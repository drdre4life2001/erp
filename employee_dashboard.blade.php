@extends('layouts.erp')

@section('page_title')
@endsection

@section('content')

    <div class="container-fluid pt-25">
        <!-- Row -->
        <br>
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"> <span style="color:white; font-weight: bold;">Dahboard</span></h3>
                        <span class="pull-right">
                                <!-- Tabs -->
                                <ul class="nav panel-tabs">
                                    <li class=""><a href="{{ url('finance/dashboard') }}">Finance</a></li>
                                    <li><a href="{{ url('finance/dashboard') }}" data-toggle="tab">Human Resources</a></li>
                                    <li><a href="{{ url('finance/dashboard') }}" data-toggle="tab">Procurement</a></li>
                                    <li class="active"><a href="{{ url('employee/dashboard') }}" data-toggle="tab">Employee</a></li>
                                </ul>
                            </span>
                    </div>
                    <br> <br>
                    <center>
                        <div class="panel-body">
                            <div class="tab-content">

                                @if(isset($total_payables))
                                    <div class="tab-pane active container" id="tab1">

                                        <div class="col-lg-6 well ">
                                            <center>
                                                <h4>
                                                    <b style="color:red;">Total Revenue</b>
                                                </h4>
                                                <h4>
                                                    &#x20A6; {{ $total_payables }}
                                                </h4>
                                            </center>
                                        </div>

                                        <div class="col-lg-6 well col-lg-offset-1">
                                            <center>
                                                <h4>
                                                    <b style="color:red;">Total Payables</b>
                                                </h4>
                                                <h4>
                                                    &#x20A6; {{ $total_payables }}
                                                </h4>
                                            </center>
                                        </div>

                                        <div class="col-lg-3 well col-lg-offset-1">
                                            <center>
                                                <h4>
                                                    <b style="color:red;">Total Expenses</b>
                                                </h4>
                                                <h4>
                                                    &#x20A6; {{ $total_expenses }}
                                                </h4>
                                        </div>
                                    </div>
                                @endif


                            </div>
                    </center>
                </div>
            </div>
        </div>
        <!-- Row -->
        <div class="row">
            <div class="col-lg-6 col-xs-12">
                <div id="weather_1" class="panel panel-default card-view">
                    <div class="panel panel-heading">
                        <div class="panel panel-title"> Latest Expense Requests</div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="datable_1" class="table table-hover table-bordered display mb-30">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Category</th>
                                            <th>Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                        </tr>
                                        <tr>
                                            <td>Garrett Winters</td>
                                            <td>Accountant</td>
                                            <td>Tokyo</td>
                                        </tr>
                                        <tr>
                                            <td>Ashton Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>San Francisco</td>
                                        </tr>
                                        <tr>
                                            <td>Cedric Kelly</td>
                                            <td>Senior Javascript Developer</td>
                                            <td>Edinburgh</td>
                                        </tr>
                                        <tr>
                                            <td>Airi Satou</td>
                                            <td>Accountant</td>
                                            <td>Tokyo</td>
                                        </tr>
                                        <tr>
                                            <td>Brielle Williamson</td>
                                            <td>Integration Specialist</td>
                                            <td>New York</td>
                                        </tr>
                                        <tr>
                                            <td>Herrod Chandler</td>
                                            <td>Sales Assistant</td>
                                            <td>San Francisco</td>
                                        </tr>
                                        <tr>
                                            <td>Rhona Davidson</td>
                                            <td>Integration Specialist</td>
                                            <td>Tokyo</td>
                                        </tr>
                                        <tr>
                                            <td>Colleen Hurst</td>
                                            <td>Javascript Developer</td>
                                            <td>San Francisco</td>
                                        </tr>
                                        <tr>
                                            <td>Donna Snider</td>
                                            <td>Customer Support</td>
                                            <td>New York</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xs-12">
                <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
            </div>
            <!-- /Row -->
        </div>

        @endsection

        @section('script')
            <script>
                Highcharts.chart('container', {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Historic World Population by Region'
                    },
                    subtitle: {
                        text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
                    },
                    xAxis: {
                        categories: ['Africa', 'America', 'Asia', 'Europe', 'Oceania'],
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Population (millions)',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' millions'
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 80,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                        shadow: true
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Year 1800',
                        data: [107, 31, 635, 203, 2]
                    }, {
                        name: 'Year 1900',
                        data: [133, 156, 947, 408, 6]
                    }, {
                        name: 'Year 2012',
                        data: [1052, 954, 4250, 740, 38]
                    }]
                });
            </script>

@endsection

