-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2017 at 11:10 AM
-- Server version: 5.7.9
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `sys_banks`
--

DROP TABLE IF EXISTS `sys_banks`;
CREATE TABLE IF NOT EXISTS `sys_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sys_banks`
--

INSERT INTO `sys_banks` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Access Bank', 2, 2, '2017-05-11 14:24:25', '2017-05-11 14:24:25', NULL),
(2, '01839364664646', 2, 2, '2017-05-11 19:16:45', '2017-05-11 19:16:45', NULL),
(3, 'Commuinty Bank', 2, 2, '2017-05-11 19:51:11', '2017-05-11 19:51:11', NULL),
(4, 'Zenith Bank', 2, 2, '2017-05-11 19:52:10', '2017-05-11 19:52:10', NULL),
(5, 'United Trust Bank', 2, 2, '2017-05-11 19:52:45', '2017-05-11 19:52:45', NULL),
(6, 'Fidelity Bank', 2, 2, '2017-05-11 20:01:45', '2017-05-11 20:01:45', NULL),
(7, 'Providus Bank', 2, 2, '2017-05-11 20:03:13', '2017-05-11 20:03:13', NULL),
(8, 'United Bank for Africa', 2, 2, '2017-05-11 20:22:57', '2017-05-11 20:22:57', NULL),
(9, 'Zenith Bank', NULL, NULL, '2017-09-25 14:05:44', '2017-09-25 14:15:39', '2017-09-25 14:15:39');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
