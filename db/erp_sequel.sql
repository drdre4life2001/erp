-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: erp
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `company_loans`
--

DROP TABLE IF EXISTS `company_loans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_loans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `amount` float DEFAULT NULL,
  `interest_rate` varchar(255) DEFAULT NULL,
  `balance` float DEFAULT '0',
  `bank` varchar(255) DEFAULT NULL,
  `description` text,
  `period` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_loans`
--

LOCK TABLES `company_loans` WRITE;
/*!40000 ALTER TABLE `company_loans` DISABLE KEYS */;
INSERT INTO `company_loans` VALUES (17,8,8000000,'0.1',0,'Skye Bank','There is no description available for this loan                                                \r\n                                            ',12,2,'2017-09-24 14:19:25','2017-09-24 12:43:23',NULL);
/*!40000 ALTER TABLE `company_loans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_repayments`
--

DROP TABLE IF EXISTS `loan_repayments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loan_repayments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loan_id` int(11) NOT NULL,
  `loan_amount` float NOT NULL,
  `total_resettlement` float NOT NULL,
  `amount_paid` float NOT NULL,
  `balance` float NOT NULL,
  `note` text NOT NULL,
  `date_paid` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_repayments`
--

LOCK TABLES `loan_repayments` WRITE;
/*!40000 ALTER TABLE `loan_repayments` DISABLE KEYS */;
INSERT INTO `loan_repayments` VALUES (27,17,8000000,0,100000,7900000,'No note available for this payment                                                            \r\n                                                        ','2017-09-24 13:48:39','2017-09-24 12:48:39','2017-09-24 12:48:39',NULL),(28,17,8000000,100000,7000000,900000,'No note this is 80% payment note                                                            \r\n                                                        ','2017-09-24 13:49:15','2017-09-24 12:49:15','2017-09-24 12:49:15',NULL),(29,17,8000000,7100000,5000,895000,'this is another note                                                            \r\n                                                        ','2017-09-24 13:49:55','2017-09-24 12:49:55','2017-09-24 12:49:55',NULL),(30,17,8000000,7105000,9000,886000,'no note   for now                                                   \r\n                                                        ','2017-09-24 14:17:23','2017-09-24 13:17:23','2017-09-24 13:17:23',NULL),(31,17,8000000,7114000,100000,786000,'completing loan                                              ','2017-09-24 14:18:28','2017-09-24 13:18:28','2017-09-24 13:18:28',NULL),(32,17,8000000,7214000,786000,0,'loan completed                                                            \r\n                                                        ','2017-09-24 14:19:24','2017-09-24 13:19:24','2017-09-24 13:19:24',NULL);
/*!40000 ALTER TABLE `loan_repayments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_organizations`
--

DROP TABLE IF EXISTS `sys_organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_organizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tin` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `id_hr_work_addresses` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK_sys_organizations_id_hr_work_addresses` (`id_hr_work_addresses`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_organizations`
--

LOCK TABLES `sys_organizations` WRITE;
/*!40000 ALTER TABLE `sys_organizations` DISABLE KEYS */;
INSERT INTO `sys_organizations` VALUES (3,'TechAdvance','001','','12323434',2,2,'2017-05-12 14:16:01','2017-09-19 15:50:22','2017-09-19 15:50:22',1),(4,'TechAdvance','002','','12323434',2,2,'2017-05-12 14:29:27','2017-05-12 14:29:27',NULL,1),(5,'TechAdvance','003','','12323434',2,2,'2017-05-12 14:34:33','2017-05-12 14:34:33',NULL,1),(6,'TechAdvance','004','','12323434',2,2,'2017-05-12 15:28:13','2017-05-12 15:28:13',NULL,1),(7,'TechAdvance','005','','12323434',2,2,'2017-05-12 16:11:56','2017-05-12 16:11:56',NULL,1),(8,'RevOPT','3','','0208383',2,2,'2017-06-14 09:35:09','2017-06-14 09:35:09',NULL,2),(9,'Gpay Africa','1001','Payment solution company','011101',2,2,'2017-09-23 17:38:36','2017-09-23 17:38:36',NULL,3);
/*!40000 ALTER TABLE `sys_organizations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-25  9:11:59
