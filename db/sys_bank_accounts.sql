-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2017 at 11:13 AM
-- Server version: 5.7.9
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `sys_bank_accounts`
--

DROP TABLE IF EXISTS `sys_bank_accounts`;
CREATE TABLE IF NOT EXISTS `sys_bank_accounts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bank_id` int(11) NOT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prevBalance` float NOT NULL,
  `currentBalance` float NOT NULL,
  `book_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sys_bank_accounts`
--

INSERT INTO `sys_bank_accounts` (`id`, `bank_id`, `account_number`, `prevBalance`, `currentBalance`, `book_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '1', 0, 0, NULL, '2017-09-25 15:19:47', '2017-09-25 16:19:10', '2017-09-25 16:19:10'),
(2, 3, '0183936466', 783009000, 782852000, NULL, '2017-09-25 15:25:33', '2017-09-25 15:25:33', NULL),
(3, 3, '1234567889', 4567, 4567, NULL, '2017-09-25 15:34:32', '2017-09-25 15:34:32', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
