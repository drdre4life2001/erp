<?php

// Home
Breadcrumbs::register('dashboard', function($breadcrumbs)
{
    $breadcrumbs->push('Dashboard', route('dashboard'));
});

// Home > About
Breadcrumbs::register('roles', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Roles', route('roles'));
});

// Home > About
Breadcrumbs::register('new-role', function($breadcrumbs)
{
    $breadcrumbs->parent('roles');
    $breadcrumbs->push('Add A New Role', route('new-role'));
});

// Home > About
Breadcrumbs::register('users', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Users', route('users'));
});

// Home > About
Breadcrumbs::register('new-user', function($breadcrumbs)
{
    $breadcrumbs->parent('users');
    $breadcrumbs->push('Add A New User', route('new-user'));
});

// Home > About
Breadcrumbs::register('update-user', function($breadcrumbs)
{
    $breadcrumbs->parent('users');
    $breadcrumbs->push('Update A User');
});

// Home > About
Breadcrumbs::register('update-role/{id}', function($breadcrumbs)
{
    $breadcrumbs->parent('roles');
    $breadcrumbs->push('Update Role');
});

//Home > Companies > Asset
Breadcrumbs::register('asset', function($breadcrumbs) {
    //$breadcrumbs->parent('roles');
    $breadcrumbs->push('View Assets');
});

// Users > Companies
Breadcrumbs::register('companies', function($breadcrumbs)
{
    $breadcrumbs->parent('users');
    $breadcrumbs->push('Companies' , route('companies'));
});

// Users > Companies
Breadcrumbs::register('new-company', function($breadcrumbs)
{
    $breadcrumbs->parent('companies');
    $breadcrumbs->push('Add Company' , route('new-company'));
});
