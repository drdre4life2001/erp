@extends('layouts.erp')

@section('content')


<h2 class="page-header">Lead</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Lead    </div>

    <div class="panel-body">
                
        <form action="{{ url('/lead'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif

            <div class="form-group">
                <label for="id" class="col-sm-3 control-label">Id</label>
                <div class="col-sm-6">
                    <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
                </div>
            </div>
            <div class="form-group">
                <label for="status" class="col-sm-3 control-label">Status</label>
                <div class="col-sm-6">
                    {{--<input type="text" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}">--}}
                    <select name="status" class="form-control" id="status">
                        <option value="{{$model['status'] or ''}}"> {{$model['status'] or 'Choose'}} </option>
                        <option value="Lead">Lead</option>
                        <option value="Open">Open</option>
                        <option value="Replied">Replied</option>
                        <option value="Oppurtunity">Oppurtunity</option>
                        <option value="Quotation">Quotation</option>
                        <option value="Lost QUotation">Lost Quotation</option>
                        <option value="Interested">Interested</option>
                        <option value="Converted">Converted</option>
                    </select>
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="person_name" class="col-sm-3 control-label">Person Name</label>
                <div class="col-sm-6">
                    <input type="text" name="person_name" id="person_name" class="form-control" value="{{$model['person_name'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="gender" class="col-sm-3 control-label">Gender</label>
                <div class="col-sm-6">
                    {{--<input type="text" name="gender" id="gender" class="form-control" value="{{$model['gender'] or ''}}">--}}
                    <select name="gender" class="form-control" id="gender">
                        <option value="{{$model['gender'] or ''}}"> {{$model['gender'] or 'Choose'}} </option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="organization_name" class="col-sm-3 control-label">Organization Name</label>
                <div class="col-sm-6">
                    <input type="text" name="organization_name" id="organization_name" class="form-control" value="{{$model['organization_name'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="source" class="col-sm-3 control-label">Source</label>
                <div class="col-sm-6">
                    <input type="text" name="source" id="source" class="form-control" value="{{$model['source'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="email_address" class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-6">
                    <input type="text" name="email_address" id="email_address" class="form-control" value="{{$model['email_address'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="lead_owner" class="col-sm-3 control-label">Lead Owner</label>
                <div class="col-sm-6">
                    {{--<input type="text" name="lead_owner" id="lead_owner" class="form-control" value="{{$model['lead_owner'] or ''}}">--}}
                    <select name="lead_owner" class="form-control" id="lead_owner">
                        <option value=""></option>
                        @foreach($lead_owners as $employee)
                            <option value="{{ $employee->id }}">{{ $employee->email }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="next_contact_date" class="col-sm-3 control-label">Next Contact Date</label>
                <div class="col-sm-3">
                    <input type="date" name="next_contact_date" id="next_contact_date" class="form-control" value="{{$model['next_contact_date'] or ''}}">
                </div>
            </div>
                                                                        <div class="form-group">
                <label for="next_contact_by" class="col-sm-3 control-label">Next Contact By</label>
                <div class="col-sm-6">
                    <input type="text" name="next_contact_by" id="next_contact_by" class="form-control" value="{{$model['next_contact_by'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="phone" class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-6">
                    <input type="text" name="phone" id="phone" class="form-control" value="{{$model['phone'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="salutation" class="col-sm-3 control-label">Salutation</label>
                <div class="col-sm-6">
                    <input type="text" name="salutation" id="salutation" class="form-control" value="{{$model['salutation'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="mobile_no" class="col-sm-3 control-label">Mobile No</label>
                <div class="col-sm-6">
                    <input type="text" name="mobile_no" id="mobile_no" class="form-control" value="{{$model['mobile_no'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="fax" class="col-sm-3 control-label">Fax</label>
                <div class="col-sm-6">
                    <input type="text" name="fax" id="fax" class="form-control" value="{{$model['fax'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="website" class="col-sm-3 control-label">Website</label>
                <div class="col-sm-6">
                    <input type="text" name="website" id="website" class="form-control" value="{{$model['website'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="territory" class="col-sm-3 control-label">Territory</label>
                <div class="col-sm-6">
                    <input type="text" name="territory" id="territory" class="form-control" value="{{$model['territory'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="lead_type" class="col-sm-3 control-label">Lead Type</label>
                <div class="col-sm-6">
                    <select name="lead_type" class="form-control" id="lead_type">
                        <option value="{{$model['lead_type'] or ''}}"> {{$model['lead_type'] or 'Choose'}} </option>
                        <option value="Client">Clinet</option>
                        <option value="Channel Partner">Channel Partner</option>
                        <option value="Consultant">Consultant</option>

                    </select>
                    {{--<input type="text" name="lead_type" id="lead_type" class="form-control" value="{{$model['lead_type'] or ''}}">--}}
                </div>
            </div>
            <div class="form-group">
                <label for="market_segment" class="col-sm-3 control-label">Market Segment</label>
                <div class="col-sm-6">
                    {{--<input type="text" name="market_segment" id="market_segment" class="form-control" value="{{$model['market_segment'] or ''}}">--}}
                    <select name="market_segment" class="form-control" id="market_segment">
                        <option value="{{$model['market_segment'] or ''}}"> {{$model['market_segment'] or 'Choose'}} </option>
                        <option value="Lower Income">Lower Income</option>
                        <option value="Middle Income">Middle Income</option>
                        <option value="Upper Income">Upper Income</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="industry" class="col-sm-3 control-label">Industry</label>
                <div class="col-sm-6">
                    {{--<input type="text" name="industry" id="industry" class="form-control" value="{{$model['industry'] or ''}}">--}}
                    <select name="industry" class="form-control" id="industry">
                        <option value="{{$model['industry'] or ''}}"> {{$model['industry'] or 'Choose'}} </option>
                        <option value="Venture Capital">Venture Capital</option>
                        <option value="Telecommunications"> Telecommunications</option>
                        <option value="Technology">Technology</option>
                        <option value="Transportation">Transportation</option>
                        <option value="Retail and Wholesale">Retail and Wholesale</option>
                        <option value="Service">Service</option>
                        <option value="News Papers and Publishers">News Papers and Publishers</option>
                        <option value="Real Estate">Real Estate</option>
                        <option value="Pharmaceuticals">Pharmaceuticals</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="request_type" class="col-sm-3 control-label">Request Type</label>
                <div class="col-sm-6">
                    {{--<input type="text" name="request_type" id="request_type" class="form-control" value="{{$model['request_type'] or ''}}">--}}
                    <select name="request_type" class="form-control" id="request_type">
                        <option value="{{$model['request_type'] or ''}}"> {{$model['request_type'] or 'Choose'}} </option>
                        <option value="Venture Capital">Product Inquiry</option>
                        <option value="Request for information"> Request for information</option>
                        <option value="Suggestions">Suggestions</option>

                        {{--<option value="Pharmaceuticals">Pharmaceuticals</option>--}}
                    </select>
                </div>
            </div>
                                                            
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/lead') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection