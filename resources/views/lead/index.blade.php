@extends('layouts.erp')

@section('content')
<h2 class="page-header">{{ ucfirst('leads') }}</h2>

{{--<div></div>--}}
<div class="panel panel-default">
    <div class="panel-heading">
        List of {{ ucfirst('leads') }}
    </div>

    <div class="col-lg-10">

    </div>

    <div class="panel-body">
        <div class="">
            <table class="table table-responsive" id="institutions-table">
              <thead>
                <tr>
                    <th>Id</th>
                    {{--<th>Status</th>--}}
                    <th>Person Name</th>
                    <th>Source</th>
                    <th>Email Address</th>
                    <th>Lead Owner</th>
                    <th>Phone</th>
                    {{--<th>Mobile No</th>--}}
                    <th>Request Type</th>
                    <th style="width:50px">Actions</th>
                    {{--<th style="width:50px"><a href="" class="fa fa-trash">Delete</a></th>--}}
                </tr>
              </thead>
              <tbody>
              <?php $num = 1; ?>
              @foreach($leads as $lead)
                  <tr>
                    <td>
                        <center>
                            {{ $num++ }}
                        </center>
                    </td>
                      {{--<td>--}}
                          {{--<center>--}}
                              {{--{{ $lead->status }}--}}
                          {{--</center>--}}
                      {{--</td>--}}
                      <td>
                          <center>
                              {{ $lead->person_name }}
                          </center>
                      </td>
                      <td>
                          <center>
                              {{ $lead->source }}
                          </center>
                      </td>
                      <td>
                          <center>
                              {{ $lead->email_address }}
                          </center>
                      </td>
                      <td>
                          <center>
                              {{ $lead->lead_owner }}
                          </center>
                      </td>
                      <td>
                          <center>
                              {{ $lead->phone }}
                          </center>
                      </td>

                      {{--<td>--}}
                          {{--<center>--}}
                              {{--{{ $lead->mobile_no }}--}}
                          {{--</center>--}}
                      {{--</td>--}}
                      <td>
                          <center>
                              {{ $lead->request_type }}
                          </center>
                      </td>
                      <td>
                          <a href="{{ url('lead/' . $lead->id . '/edit') }}" target="_blank" style="color:#000099;"  class="glyphicon glyphicon-edit">Edit</a> <br> <br>
                          <hr>
                          <a href="{{ url('lead/' . $lead->id) }}" target="_blank" style="color:#337ab7;"  class="glyphicon glyphicon-eye-open">View</a> <br> <br>
                          <hr>
                          <a href="{{ url('lead/destroy', $lead->id) }}" style="color:red;" class="glyphicon glyphicon-trash" id="a_del">Trash</a>
                      </td>
                  </tr>
              @endforeach
              </tbody>
            </table>
        </div>
        <a href="{{url('lead/create')}}" class="btn btn-primary" role="button">Add lead</a>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "responsive": true,
                "ajax": "{{url('lead/grid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/lead') }}/'+row[0]+'">'+data+'</a>';
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/lead') }}/'+row[0]+'/edit" class="btn btn-default">Update</a>';
                        },
                        "targets": 20                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="#" onclick="return doDelete('+row[0]+')" class="btn btn-danger">Delete</a>';
                        },
                        "targets": 20+1
                    },
                ]
            });
        });
        function doDelete(id) {
            if(confirm('You really want to delete this record?')) {
               $.ajax({ url: '{{ url('/lead') }}/' + id, type: 'DELETE'}).success(function() {
                theGrid.ajax.reload();
               });
            }
            return false;
        }
    </script>
@endsection