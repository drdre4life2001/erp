@extends('layouts.erp')

@section('content')
    <section class="content-header">
        <h1>
            Request Item
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($requestItem, ['route' => ['requestItems.update', $requestItem->id], 'method' => 'patch']) !!}

                        @include('request_items.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection