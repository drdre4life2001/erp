
<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Fin Request Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_fin_request_category', 'Category:') !!}
    <select class="form-control" name="id_fin_request_category">
        @if(isset($categories))
        @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
        @endForeach
        @endIf
    </select>
</div>

<!-- Unit price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('unit_price', 'Unit Price:') !!}
    {!! Form::text('unit_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Cost Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cost_type', 'Cost Type:') !!}
    <select name="cost_type" class="form-control">
        <option value="Indirect">Indirect</option>
        <option value="Direct">Direct</option>
    </select>
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textArea('description', null, ['class' => 'form-control']) !!}
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('requestItems.index') !!}" class="btn btn-default">Cancel</a>
</div>
