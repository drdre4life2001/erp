@extends('layouts.erp')

@section('page_title')
<h2>Create Request Item</h2>
@endsection

@section('content')
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    @if(isset($errors))
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}} here</li>
                            @endForeach
                        </ul>
                        @endIf
                    </div>
                    @endIf
                    {!! Form::open(['route' => 'requestItems.store']) !!}

                        @include('request_items.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
