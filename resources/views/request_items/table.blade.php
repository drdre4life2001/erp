<table id="datable_1" class="table table-hover display pb-30" >
    <thead class="panel panel-heading">
    <tr>
        <th>Name</th>
        <th>Category</th>
        <th>Description</th>
        <th>Cost Type</th>
        <th colspan="3">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php $i = 1;?>
    @foreach($requestItems as $requestItem)
        <tr>
            <td>{!! $requestItem->name !!}</td>
            <td>{!! $requestItem->category->name !!}</td>
            <td>{!! $requestItem->description !!}</td>
            <td>{!! $requestItem->cost_type !!}</td>
            <td>
                {!! Form::open(['route' => ['requestItems.destroy', $requestItem->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('requestItems.show', [$requestItem->id]) !!}"
                       class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('requestItems.edit', [$requestItem->id]) !!}"
                       class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>