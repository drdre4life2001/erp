@extends('layouts.erp')

@section('page_title')
    <div>
        <section class="content-header">
            <h4 class="pull-left">Request Sub-Categories</h4>
            <h2 class="pull-right">
                <a class="btn btn-primary " style="margin-top: -10px;margin-bottom: 5px"
                   href="{!! route('requestItems.create') !!}">Add New</a>
            </h2>
        </section>
    </div>
    <div class="clearfix"></div>
@endsection

@section('content')
    <div class="col-sm-12">
        <div class="panel panel-default card-view">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="table-wrap">
                        <div class="table-responsive">
                            @include('request_items.table')
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

