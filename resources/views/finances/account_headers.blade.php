@extends('layouts.erp')

@section('page_title')
    <h4>Account Headers </h4><br>
@endsection

@section('content')
    <div class="row">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <form action="{{ url('finance/account_header') }}" method="post">
            {{csrf_field()}}
            @if(isset($errors))
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endForeach
                        </ul>
                    </div>
                @endIf
            @endIf

            <div class="col-md-4">
                <div class="form-group">
                    <label class="">Name</label>
                    <input name="name" id="name" type="text" class="form-control" value="{{old('name')}}">

                </div>
                <div class="form-group">
                    <label class="">Effect</label><br>
                    @foreach($effects as $effect)
                    <input type="radio" value="{{$effect->id}}" name="effect">{{$effect->name}}
                    @endforeach
                </div>
                <div class="form-group">
                    <input type="submit" value="Add" class="btn btn-danger btn-sm">
                </div>
            </div>

        </form>

    </div>
    <div class="row">
        <div class="col-sm-8">
            <table class='table table-bordered' id='saveRequests'>
                <thead>
                <tr>
                    <th>S/N</th>
                    <th>Header</th>
                    <th>Effect</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($item))
                    <?php $i =  1; ?>
                    @foreach($item as $items)

                        <tr>
                            <!--Loop through all items for this procurement -->

                            <td>{{ $i++ }}</td>
                            <td>{{ $items->header }}</td>
                            <td>{{ $items->effect }}</td>
                            <td>
                                <div class='btn-group' style="display:block">
                                    <!--Edit item -->
                                    <a href="" style="margin-right: 0px;"
                                       class='btn btn-default btn-xs' data-toggle='modal' data-target='#{{$items->id}}'>
                                        <i class="glyphicon glyphicon-eye-open"> </i> Edit</a>
                                    <!--delete this item -->

                                    <a href="" style="margin-right: 0px;"
                                       class='btn btn-danger btn-xs' data-toggle="modal" data-target="#{{$items->id.'_'}}">
                                        <i class="glyphicon glyphicons-delete"> </i> Delete</a>

                                </div>

                            </td>
                            <!-- Delete Modal -->
                            <div class="modal fade" id="{{$items->id.'_'}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action = "{{url('finance/account_header/delete')}}" method="post">
                                            <div class="modal-body">
                                                Are you sure you want to delete this item? Removing headers will also remove sub-headers.
                                                <input type="hidden" name="item_id" value="{{$items->id}}">
                                            </div>
                                            <div class="modal-footer">

                                                <a class="btn btn-default btn-ok" data-dismiss="modal">No</a>
                                                <input type="submit" value="Yes"  class="btn btn-danger">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--Modal ends -->
                            <!-- Modal -->
                            <div id="{{$items->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Edit this Header</h4>
                                        </div>
                                        <form action="{{url('finance/account_header/edit')}}" method="post">
                                            <div class="modal-body">
                                                <input type="hidden" name="item_id" value="{{$items->id}}">
                                                <div class="form-group col-sm-6">
                                                    <label class="">Header</label>
                                                    <input name="name" id="name" type="text" class="form-control" value="{{$items->header}}" required/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="">Effect</label><br>
                                                    @foreach($effects as $effect)
                                                        <input type="radio" value="{{$effect->id}}"
                                                               <?php if($items->effect == $effect->name){ echo 'checked'; } ?>
                                                               name="effect">{{$effect->name}}
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" value="update" name="submit" class="btn btn-danger">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--Modal ends -->

                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#price').keyup(calculate);
            $('#quantity').keyup(calculate);
        });
        function calculate(e)
        {
            $('#total').val($('#price').val() * $('#quantity').val());
        }


        $('#saveRequests').DataTable({
            searching: false,
            "pagingType": "full_numbers",
            dom: 'Bfrtip',
            buttons: [
                'csv', 'excel','print'
            ]
        });

        //console.log(categories);
        var result = {};
        var items = {}
        var selectedCategory = "";
        //loop through the categories
        for (var i = 0; i < categories.length; i++) {
            //save the categories and categories items object
            result[categories[i].name] = categories[i].items;
            //loop through the items in the category
            for (var j = 0; j < categories[i].items.length; j++) {
                //save the items data; [items id => items obj]
                items[categories[i].items[j].id] = categories[i].items[j];
            }


        }

        //console.log(result);
        //console.log(items);

        $('select[name="category"]').change(function () { // When category select changes

            //set the unit price to default
            $('#unit_price').val('');
            $('.description').replaceWith("<div class='description col-sm-12'><p><b></b><p></div>");
            //var p = document.querySelector('.other_product_name');
            //p.innerHTML = "";
            //$('#other_product_item').fadeOut();

            /**if ($(this).val() == "Others"){
                var productfield = document.createElement('input');
                productfield.setAttribute('type', 'text');
                productfield.setAttribute('name', 'name');
                productfield.setAttribute('placeholder', "Product Eg: Dry Yam");
                productfield.classList.add('form-control');


            }else{*/
            selectedCategory = $(this).val();
            var options = '<option>Choose one!</option>';
            $.each(result[$(this).val()] || [], function (i, v) { // Cycle through each associated items
                options += '<option value="' + v.id + '">' + v.name + '</option>';
            });
            //options += '<option>' + "Other" + '</option>';
            var itemtfield = document.createElement('select');
            itemtfield.setAttribute('name', 'item');
            itemtfield.classList.add('form-control');
            itemtfield.innerHTML = options;
            /**} */

            var p = document.querySelector('.item_name');
            p.innerHTML = "";
            p.appendChild(itemtfield);
            $('#category_item').fadeIn();
            setItemListener();
//            $('select[name="product"]').html(options); // And update the role options
        });

        function setItemListener() {
            $('select[name="item"]').change(function () {

                $(this).attr('name', 'item');
                //auto-set the unit price, qty, other information
                //gets its information
                var itemObj = items[$(this).val()];
                $('#unit_price').val(itemObj.unit_price);
                $('.description').replaceWith("<div class='description col-sm-12'><p><i>" + itemObj.description + "</i><p></div>");
                //console.log(itemObj);
                //console.log();
            })
        }
    </script>
@endSection
