@extends('layouts.erp')

@section('page_title')
    Viewing <span style="color:red;">{{ $company_name }}</span>'s Payment Schedule
@endsection

@section('content')

<div class="content">
    <section class="content-header">
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    @if(isset($errors))
                    @if(count($errors) > 0)
                    <div class="col-sm-12 alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endForeach
                        </ul>

                    </div>
                    @endIf
                    @endIf

                    @if(isset($message))
                    <div class="col-sm-12 alert alert-success">
                        <ul>
                            <li>{{$message}}</li>
                        </ul>
                    </div>
                    @endIf

                    <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>
                               <center>
                                    <label for=""><span style="color:red;">{{ $company_name }}</span>'s Payment Schedule</label> 
                               </center>
                            <!-- </h4> <a style="color: blue;" href="{{ url('finance/back') }}"><<< Go-Back</a> -->
                        </div> <br> <br>
                            <!-- company id, loan id , amount of loan, amount paid so far, -->
                        <div class="card-block">

                            <div class="col-md-9 col-md-offset-2">
                                    <div class="row">
                                         <table class="table table-responsive" id="organizations-table">
                                             <thead>
                                                 <th>
                                                     <center>
                                                         Company Name
                                                     </center>
                                                 </th>
                                                 <th>
                                                     <center>
                                                         Amount To be Paid
                                                     </center>
                                                 </th>
                                                 <th>
                                                     <center>
                                                         Interest Rate
                                                     </center>
                                                 </th>
                                                 <th>
                                                     <center>
                                                         Period    
                                                     </center>
                                                 </th>
                                                 <th>
                                                     <center>
                                                         Bank
                                                     </center>
                                                 </th>
                                                 <th> 
                                                     <center>
                                                         Monthly Payment
                                                     </center>
                                                 </th>
                                             </thead>
                                             <tbody>
                                                 <tr>
                                                     <td> <center>{!! $company_name !!}</center></td>
                                                     <td>
                                                        <center>
                                                         &#8358;{!! number_format($loan->amount, 2) !!}
                                                        </center>
                                                    </td>
                                                     <td>
                                                        <center>
                                                            {!! number_format($loan->interest_rate, 2) !!}<span style="font-weight: bold;">%
                                                       </span> </center></td>
                                                     <td>
                                                        <center>
                                                            {!! $loan->period !!}
                                                        </center>
                                                    </td>
                                                     <td>
                                                        <center>
                                                         {!! $bank !!}
                                                        </center>
                                                    </td>
                                                     <td>
                                                        <center>
                                                         &#8358;{!! number_format($monthly_payment, 2) !!}<span style="color:&#8358;"> (Monthly)</span>
                                                        </center>
                                                    </td>
                                                 </tr>
                                             </tbody>
                                         </table>       
                                    </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
