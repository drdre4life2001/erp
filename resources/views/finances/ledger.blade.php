@extends('layouts.erp')

@section('page_title')
    <h4>Ledger </h4><br>
@endsection

@section('content')
    <div class="row">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

    </div>
    <div class="row">
        <div class="col-sm-12">
            <table class='table table-bordered' id='saveRequests'>
                <thead>

                <tr>
                    <th>S/N</th>
                    <th>Bank Details</th>
                    <th>Transaction type</th>
                    <th>Amount</th>
                    <th>Previous Balance</th>
                    <th>Current Balance</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($items))
                    <?php $i =  1; ?>
                    @foreach($items as $item)

                        <tr>
                            <!--Loop through all items for this procurement -->

                            <td>{{ $i++ }}</td>
                            <td>{{ $item->bank_name}}[{{$item->account_number}}]</td>
                            <td>{{ $item->effect }}</td>
                            <td>{{ number_format($item->amount) }}</td>
                            <td>{{ number_format($item->prevBalance) }}</td>
                            <td>{{ number_format($item->currentBalance) }}</td>
                            <td>
                                <div class='btn-group' style="display:block">

                                    <a href="" style="margin-right: 0px;"
                                       class='btn btn-danger btn-xs' data-toggle="modal" data-target="#{{$item->id.'_'}}">
                                        <i class="glyphicon glyphicons-delete"> </i> Delete</a>

                                </div>

                            </td>
                            <!-- Delete Modal -->
                            <div class="modal fade" id="{{$item->id.'_'}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action = "{{url('finance/banks/delete')}}" method="post">
                                            <div class="modal-body">
                                                Are you sure you want to delete this item?
                                                <input type="hidden" name="item_id" value="{{$item->id}}">
                                            </div>
                                            <div class="modal-footer">

                                                <a class="btn btn-default btn-ok" data-dismiss="modal">No</a>
                                                <input type="submit" value="Yes"  class="btn btn-danger">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--Modal ends -->


                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#price').keyup(calculate);
            $('#quantity').keyup(calculate);
        });
        function calculate(e)
        {
            $('#total').val($('#price').val() * $('#quantity').val());
        }


        $('#saveRequests').DataTable({
            searching: false,
            "pagingType": "full_numbers",
            dom: 'Bfrtip',
            buttons: [
                'csv', 'excel','print'
            ]
        });

        //console.log(categories);
        var result = {};
        var items = {}
        var selectedCategory = "";
        //loop through the categories
        for (var i = 0; i < categories.length; i++) {
            //save the categories and categories items object
            result[categories[i].name] = categories[i].items;
            //loop through the items in the category
            for (var j = 0; j < categories[i].items.length; j++) {
                //save the items data; [items id => items obj]
                items[categories[i].items[j].id] = categories[i].items[j];
            }


        }

        //console.log(result);
        //console.log(items);

        $('select[name="category"]').change(function () { // When category select changes

            //set the unit price to default
            $('#unit_price').val('');
            $('.description').replaceWith("<div class='description col-sm-12'><p><b></b><p></div>");
            //var p = document.querySelector('.other_product_name');
            //p.innerHTML = "";
            //$('#other_product_item').fadeOut();

            /**if ($(this).val() == "Others"){
                var productfield = document.createElement('input');
                productfield.setAttribute('type', 'text');
                productfield.setAttribute('name', 'name');
                productfield.setAttribute('placeholder', "Product Eg: Dry Yam");
                productfield.classList.add('form-control');


            }else{*/
            selectedCategory = $(this).val();
            var options = '<option>Choose one!</option>';
            $.each(result[$(this).val()] || [], function (i, v) { // Cycle through each associated items
                options += '<option value="' + v.id + '">' + v.name + '</option>';
            });
            //options += '<option>' + "Other" + '</option>';
            var itemtfield = document.createElement('select');
            itemtfield.setAttribute('name', 'item');
            itemtfield.classList.add('form-control');
            itemtfield.innerHTML = options;
            /**} */

            var p = document.querySelector('.item_name');
            p.innerHTML = "";
            p.appendChild(itemtfield);
            $('#category_item').fadeIn();
            setItemListener();
//            $('select[name="product"]').html(options); // And update the role options
        });

        function setItemListener() {
            $('select[name="item"]').change(function () {

                $(this).attr('name', 'item');
                //auto-set the unit price, qty, other information
                //gets its information
                var itemObj = items[$(this).val()];
                $('#unit_price').val(itemObj.unit_price);
                $('.description').replaceWith("<div class='description col-sm-12'><p><i>" + itemObj.description + "</i><p></div>");
                //console.log(itemObj);
                //console.log();
            })
        }
    </script>
@endSection
