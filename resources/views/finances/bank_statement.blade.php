@extends('layouts.erp')

@section('page_title')
    <h4> <span style="color:red;">
            @if(isset($accounts))
                @foreach($accounts as $account)
                    {{$account->account_number}}
                @endforeach
            @endif
        </span> Account Statement</h4><br>
@endsection

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <table class='table table-bordered' id='saveRequests'>
                <thead>
                <tr>
                    <th>S/N</th>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Previous Balance</th>
                    <th>Current Balance</th>
                    <!-- <th>Email</th> -->
                </tr>
                </thead>
                <tbody>
                @if(isset($item))
                    <?php $i =  1; ?>
                    @foreach($item as $items)
                    <?php $date = date('d-m-Y', strtotime($items->created_at)); ?>
                        <tr>
                            <!--Loop through all items for this procurement -->

                            <td>{{ $i++ }}</td>
                            <td>{{$date}}</td>
                            <td>{{ $items->amount }}</td>
                            <td>{{ number_format($items->prevBalance) }}</td>
                            <td>{{ number_format($items->currentBalance) }}</td>
                            {{-- <td>{{ $items->email }}</td> --}}
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#saveRequests').DataTable({
                searching: false,
                "pagingType": "full_numbers",
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel'
                ]
            });
        });
    </script>
@endSection
