@extends('layouts.erp')

@section('page_title')
    <h4>Bank Accounts</h4><br>
@endsection

@section('content')
    <div class="row">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <form action="{{ url('finance/bank_accounts') }}" method="post">
            {{csrf_field()}}
            @if(isset($errors))
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endForeach
                        </ul>
                    </div>
                @endIf
            @endIf

            <div class="col-md-4">
                <div class="form-group">
                    <label class="">Select bank</label>
                    <select name="bank_id" class="form-control"required>
                        <option>--Please select --</option>
                        @if(isset($banks))
                            @foreach($banks as $bank)
                                <option value="{{$bank->id}}">{{$bank->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <label class="">Account Number</label>
                    <input name="account_number" id="account_number" type="number" class="form-control" value="{{old('account_number')}}" required>
                </div>
                <div class="form-group">
                    <label class="">Account Balance</label>
                    <input name="account_balance" id="account_balance" type="number" class="form-control" value="{{old('account_balance')}}" required>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="">Select Subsidiary</label>
                    <select name="subsidiary_id" class="form-control" required>
                        <option value="">--Please select --</option>
                        @if(isset($organizations))
                            @foreach($organizations as $organization)
                                <option value="{{$organization->id}}">{{$organization->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <label class="">Name of Relationship Manager</label>
                    <input name="name_relationship_manager" id="name_relationship_manager" type="text" class="form-control" value="{{old('name_relationship_manager')}}" required>
                </div>
                <div class="form-group">
                    <label class="">Phone number of Relationship Manager </label>
                    <input name="phone_relationship_manager" id="phone_relationship_manager" type="text" class="form-control" value="{{old('phone_relationship_manager')}}" required>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="">Email of Relationship Manager </label>
                    <input name="email_relationship_manager" id="email_relationship_manager" type="email" class="form-control" value="{{old('email_relationship_manager')}}" required>
                </div>
                <div class="form-group">
                    <input type="submit" value="Add" class="btn btn-danger btn-sm">
                </div>
            </div>

        </form>

    </div>
    <div class="row">
        <div class="col-sm-12">
            <table class='table table-bordered' id='saveRequests'>
                <thead>
                <tr>
                    <th>S/N</th>
                    <th>Bank Name</th>
                    <th>Account Number</th>
                    <th>Previous Balance</th>
                    <th>Current Balance</th>
                    <th>Subsidiary</th>
                    <th>Relationship Manager</th>
                    <th>Relationship Manager - Email</th>
                    <th>Relationship Manager - Phone Number</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($item))
                    <?php $i =  1; ?>
                    @foreach($item as $items)

                        <tr>
                            <!--Loop through all items for this procurement -->

                            <td>{{ $i++ }}</td>
                            <td>{{ $items->bank }}</td>
                            <td>{{ $items->account }}</td>
                            <td>{{ number_format($items->prevBalance) }}</td>
                            <td>{{ number_format($items->currentBalance) }}</td>
                            <td>{{ $items->subsidiary }}</td>
                            <td>{{ $items->name_relationship_manager }}</td>
                            <td>{{ $items->email_relationship_manager }}</td>
                            <td>{{ $items->phone_relationship_manager }}</td>
                            <td>
                                <div class='btn-group' style="display:block">
                                    <!--Edit item -->
                                    <a href="" style="margin-right: 0px;"
                                       class='btn btn-default btn-xs' data-toggle='modal' data-target='#{{$items->id}}'>
                                        <i class="glyphicon glyphicon-eye-open"> </i> Edit</a>
                                    <!--delete this item -->

                                    <a href="{{url('finance/bank_accounts/statement/'.$items->id)}}" style="margin-right: 0px;"
                                       class='btn btn-danger btn-xs'>
                                        <i class="glyphicon glyphicons-delete"> </i> View</a>

                                </div>

                            </td>
                            <!-- Delete Modal -->
                            <div class="modal fade" id="{{$items->id.'_'}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action = "{{url('finance/bank/accounts/delete')}}" method="post">
                                            <div class="modal-body">
                                                Are you sure you want to delete this item?
                                                <input type="hidden" name="item_id" value="{{$items->id}}">
                                            </div>
                                            <div class="modal-footer">

                                                <a class="btn btn-default btn-ok" data-dismiss="modal">No</a>
                                                <input type="submit" value="Yes"  class="btn btn-danger">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--Modal ends -->
                            <!-- Modal -->
                            <div id="{{$items->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h5 class="modal-title">Edit this Account Number</h5>
                                        </div>
                                        <form action="{{url('finance/bank/accounts/edit')}}" method="post">
                                            <div class="modal-body">
                                                <input type="hidden" name="item_id" value="{{$items->id}}">
                                                <div class="form-group col-sm-6">
                                                    <label class="">Account Number</label>
                                                    <input name="account" id="account_number" type="text" class="form-control" value="{{$items->account}}" required/>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label class="">Select Subsidiary</label>
                                                <select name="subsidiary_id" class="form-control" required>
                                                    <option value="">--Please select --</option>
                                                    @if(isset($organizations))
                                                        @foreach($organizations as $organization)
                                                            <option
                                                                <?php
                                                                if($organization->id == $items->subsidiary_id){
                                                                    echo 'selected';
                                                                }
                                                                ?>
                                                                value="{{$organization->id}}">{{$organization->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label class="">Name of Relationship Manager</label>
                                                    <input name="name_relationship_manager" id="name_relationship_manager" type="text" class="form-control" value="{{$items->name_relationship_manager}}" required/>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label class="">Email of Relationship Manager</label>
                                                    <input name="email_relationship_manager" id="email_relationship_manager" type="text" class="form-control" value="{{$items->email_relationship_manager}}" required/>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label class="">Phone Number of Relationship Manager</label>
                                                    <input name="phone_relationship_manager" id="phone_relationship_manager" type="text" class="form-control" value="{{$items->phone_relationship_manager}}" required/>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" value="update" name="submit" class="btn btn-danger">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--Modal ends -->

                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#price').keyup(calculate);
            $('#quantity').keyup(calculate);
        });
        function calculate(e)
        {
            $('#total').val($('#price').val() * $('#quantity').val());
        }


        $('#saveRequests').DataTable({
            searching: false,
            "pagingType": "full_numbers",
            dom: 'Bfrtip',
            buttons: [
                'csv', 'excel','print'
            ]
        });

        //console.log(categories);
        var result = {};
        var items = {}
        var selectedCategory = "";
        //loop through the categories
        for (var i = 0; i < categories.length; i++) {
            //save the categories and categories items object
            result[categories[i].name] = categories[i].items;
            //loop through the items in the category
            for (var j = 0; j < categories[i].items.length; j++) {
                //save the items data; [items id => items obj]
                items[categories[i].items[j].id] = categories[i].items[j];
            }


        }

        //console.log(result);
        //console.log(items);

        $('select[name="category"]').change(function () { // When category select changes

            //set the unit price to default
            $('#unit_price').val('');
            $('.description').replaceWith("<div class='description col-sm-12'><p><b></b><p></div>");
            //var p = document.querySelector('.other_product_name');
            //p.innerHTML = "";
            //$('#other_product_item').fadeOut();

            /**if ($(this).val() == "Others"){
                var productfield = document.createElement('input');
                productfield.setAttribute('type', 'text');
                productfield.setAttribute('name', 'name');
                productfield.setAttribute('placeholder', "Product Eg: Dry Yam");
                productfield.classList.add('form-control');


            }else{*/
            selectedCategory = $(this).val();
            var options = '<option>Choose one!</option>';
            $.each(result[$(this).val()] || [], function (i, v) { // Cycle through each associated items
                options += '<option value="' + v.id + '">' + v.name + '</option>';
            });
            //options += '<option>' + "Other" + '</option>';
            var itemtfield = document.createElement('select');
            itemtfield.setAttribute('name', 'item');
            itemtfield.classList.add('form-control');
            itemtfield.innerHTML = options;
            /**} */

            var p = document.querySelector('.item_name');
            p.innerHTML = "";
            p.appendChild(itemtfield);
            $('#category_item').fadeIn();
            setItemListener();
//            $('select[name="product"]').html(options); // And update the role options
        });

        function setItemListener() {
            $('select[name="item"]').change(function () {

                $(this).attr('name', 'item');
                //auto-set the unit price, qty, other information
                //gets its information
                var itemObj = items[$(this).val()];
                $('#unit_price').val(itemObj.unit_price);
                $('.description').replaceWith("<div class='description col-sm-12'><p><i>" + itemObj.description + "</i><p></div>");
                //console.log(itemObj);
                //console.log();
            })
        }
    </script>
@endSection
