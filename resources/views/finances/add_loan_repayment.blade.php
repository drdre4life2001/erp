@extends('layouts.erp')

@section('page_title')
    Add Loan Repayment for <span style="color:&#8358;;">{{ $company_name }}</span>
@endsection

@section('content')

<div class="content">
    <section class="content-header">
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    @if(isset($errors))
                    @if(count($errors) > 0)
                    <div class="col-sm-12 alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endForeach
                        </ul>

                    </div>
                    @endIf
                    @endIf

                    @if(isset($message))
                    <div class="col-sm-12 alert alert-success">
                        <ul>
                            <li>{{$message}}</li>
                        </ul>
                    </div>
                    @endIf

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>
                                   <center>
                                       @if(!empty($loan->balance) )
                                            Company Loan
                                        @else
                                            <b>
                                                Loan Repayment Completed for {{$company_name}}
                                            </b>
                                        @endif
                                   </center>
                                </h4>
                            </div> <br> <br>

                            <div class="card-block">

                                <div class="col-md-11 col-md-offset-1">
                                    <form method="POST" action="">
                                        <div class="row">
                                           
                                            @if(!empty($loan->balance))
                                                <div class="col-lg-5 ">
                                                        <label for=""> Amount to be Paid: </label>
                                                        
                                                        <input type="text" placeholder="Enter amount here" required name="amount_paid" id="input" class="form-control" value="" >
                                                        
                                                        @if ($errors->has('amount'))<p class="help-block" style="color: red">{{ $errors->first('loan_amount') }}</p> @endif <br> 
                                                    </div>
                                                    <div class=" col-lg-5">
                                                        <label>Note</label>
                                                        <textarea class="form-control" placeholder="note here" required name="note">
                                                            
                                                        </textarea>
                                                    </div>           
                                            @endif
                                                    
                                        </div>
                                        <div class="row">
                                            <div class=" col-lg-5">
                                                        <label>Loan Amount</label>
                                                            <input type="text" required name="loan_amo" id="input" class="form-control" disabled value="{{ number_format($loan->amount, 2) }}" >
                                            </div>
                                            <div class=" col-lg-5">
                                                        <label>Balance</label>
                                                            <input type="text" required name="balance" id="input" class="form-control" disabled value="{{ number_format($loan->balance, 2) }}" >
                                            </div>
                                            <br> <br>
                                        </div> <br> <br>
                                        <div class="col-lg-2 col-lg-offset-4">
                                            @if(!empty($loan->balance))
                                                <input type="submit" value="Make Repayment" class="btn btn-success"/> <br> <br>
                                            @endif 
                                        </div>

                                    </form>   
                                     
                                </div>

                            </div>
                        </div>

                         <div class="card-block">

                            <div class="col-md-9 col-md-offset-1">
                                @if(count($repayment_histories))
                                    <div class="row">
                                        <center> <b>REPAYMENT HISTORY FOR {{ strtoupper($company_name) }} </b> <small style="color: red;">Expected Amount: &#8358;{{ number_format($loan->amount, 2) }} </small> </center>
                                         <table class="table table-responsive table-hover" id="organizations-table">
                                             <thead>
                                                <th>
                                                     <center>
                                                         S/N
                                                     </center>
                                                 </th>
                                                 <th>
                                                     <center>
                                                         Amount Paid
                                                     </center>
                                                 </th>
                                                 <!-- <th>
                                                    <center>
                                                         Total Resettlement so far   
                                                     </center>
                                                     
                                                 </th> -->
                                                 <th>
                                                     <center>
                                                         Balance
                                                     </center>
                                                 </th>
                                                 <th>
                                                     <center>
                                                         Note
                                                     </center>
                                                 </th>
                                                 <th> 
                                                     <center>
                                                         Date Paid
                                                     </center>
                                                 </th>
                                             </thead>
                                             <tbody>
                                                <?php ;$num = 1; ?>
                                                @foreach($repayment_histories as $repayment_history)
                                                 <tr>
                                                    <td>
                                                        <center>
                                                            ({{ $num++ }})
                                                        </center>
                                                    </td>
                                                     <td> 
                                                        <center>&#8358;{!! number_format($repayment_history->amount_paid, 2) !!}</center>
                                                     </td>
                                                     <!-- <td>
                                                        <center>
                                                            &#8358;{!! number_format($repayment_history->total_resettlement, 2) !!}
                                                        </center>
                                                        
                                                    </td> -->
                                                    <td>
                                                        <center>
                                                         &#8358;{!! number_format($repayment_history->balance, 2) !!}
                                                        </center>
                                                    </td>
                                                     <td>
                                                            {!! $repayment_history->note !!}<span style="font-weight: bold;">
                                                        </span> 
                                                    </td>
                                                     <td>
                                                        <center>
                                                            {{ $repayment_history->date_paid }}
                                                        </center>
                                                    </td>
                                                 </tr>

                                                @endforeach
                                             </tbody>
                                         </table>
                                        <hr>
                                        

                                        <div class="col-lg-4" style="margin-top:-30px; margin-left: -50px;">
                                            <center>
                                                <b style="font-weight: bold; color: red;">Total Resettlement = &#8358;{{ number_format($total_resettlement, 2) }} {{ empty($loan->balance) ? '(Completed)' : '' }}</b>
                                            </center>
                                        </div>     
                                        <!-- <div class="col-lg-2">
                                            <center>
                                                <b style="font-weight: bold;">SUM = </b>
                                            </center>
                                        </div>     
                                        <div class="col-lg-2">
                                            <center>
                                                <b style="font-weight: bold;">SUM = </b>
                                            </center>
                                        </div>  -->    
                                            
                                    </div>
                                @else
                                    <div class="row">
                                        <div>
                                            <center> <b>NO REPAYMENT HISTORY FOR {{ strtoupper($company_name) }} </b> <small style="color: red;">Expected Amount: &#8358;{{ number_format($loan->amount, 2) }} </small> </center>
                                        </div>
                                    </div>
                                @endif

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection
