@extends('layouts.app_finance')

@section('page_title')
<h2>Finance [Dashboard]</h2>
@endsection

@section('finance_content')


@endsection

@section('script')
<script>

    $(function () {
        Highcharts.chart('pie_chart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Employees by gender'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Male',
                    y: 56.33
                }, {
                    name: 'Female',
                    y: 24.03,
                    sliced: true,
                    selected: true
                }]
            }]
        });
    });

    $(function () {
        Highcharts.chart('column_chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Employees by Department'
            },
            subtitle: {
                text: 'Source: Taerp'
            },
            xAxis: {
                categories: [
                    'Accounts',
                    'Legal',
                    'Finance'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of employees'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} Million</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
                {
                    name: 'Male',
                    data: [49.9, 71.5, 106.4]

                },
                {
                    name: 'Female',
                    data: [83.6, 78.8, 98.5]

                },
                {
                    name: 'Others',
                    data: [48.9, 38.8, 39.3]

                }
            ]
        });
    });

    $(function () {
        Highcharts.chart('column_chart_single', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Number of Employees over the last 3 months'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Count'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} Million</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
                {
                    name: 'Employees',
                    data: [49.9, 71.5, 106.4]

                }
            ]
        });
    });

</script>

@endsection
