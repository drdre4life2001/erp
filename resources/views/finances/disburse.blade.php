@extends('layouts.erp')

@section('page_title')
    <h4>Disburse Funds</h4>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-8">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form action="{{ url('finance/process/disburse') }}" method="post">
                {{csrf_field()}}
                @if(isset($errors))
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endForeach
                            </ul>
                        </div>
                    @endIf
                @endIf

                    <?php
                    use Illuminate\Support\Facades\Auth;
                    use Illuminate\Support\Facades\DB;
                    $user_id = Auth::user()->id_hr_employee;
                    $user = DB::select("select * from hr_employee as a,  hr_grade as b where
                    a.id_hr_grade = b.id and b.id = 4 and a.id = '$user_id'");

                    ?>
                    <div class="col-md-12">
                        <div class="form-group col-sm-12">
                            <div class="">
                                <input type="hidden" name="item_id" value="{{$item_id}}">
                                @if($user) {{-- If user is a C-Level staff--}}
                                    <div class="form-group col-sm-8">
                                        <label class="">Account Sub Header</label>
                                        <select class="form-control" id="account_sub_header" name="account_sub_header" required>
                                            <option value="">--Please select --</option>
                                            @foreach($subheaders as $subheader)
                                                <option value="{{$subheader->id}}">{{$subheader->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-8">
                                        <label class="">Select Bank Account</label>
                                        <select class="form-control bank_account" name="bank_account">
                                            <option value="">--Please select--</option>
                                            @foreach($banks as $bank)
                                                <option value="{{$bank->id}}">{{$bank->name}}[{{$bank->account_number}}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @else
                                    <div class="form-group col-sm-8">
                                        <label class="">Account Sub Header</label>
                                        <select class="form-control account_sub_header" id="account_sub_header" name="account_sub_header" required>
                                            <option value="">--Please select --</option>
                                            @foreach($subheaders as $subheader)
                                                <option value="{{$subheader->id}}">{{$subheader->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <input type ="hidden" name="bank_account_id" id="bank_account_id">
                                    <div class="form-group col-sm-8">
                                        <label class="">Account Details</label>
                                        <input name="bank_details" id="bank_details" class="bank_details form-control" readonly="true">
                                    </div>

                                @endif
                                    <div class="form-group col-sm-8">
                                        <label class="">Account Balance</label>
                                        <input name="balance" id="balance" type="number"  class="form-control balance" readonly="true"/>
                                    </div>
                                    <div class="form-group col-sm-8">
                                        <label class="">Amount to be disbursed</label>
                                        @foreach($details as $detail)
                                            <?php $amount = number_format($detail->total); ?>
                                        <input name="amount" id="amount" type="number" value="{{$detail->total}}" class="form-control" readonly="true"/>
                                            @endforeach
                                    </div>

                                {{--
                                 <div class="form-group col-sm-8">
                                         <label class="">Select Bank Account</label>
                                         <select class="form-control bank_account" name="bank_account">
                                             <option value="">--Please select--</option>
                                             @foreach($banks as $bank)
                                                 <option value="{{$bank->id}}">{{$bank->name}}[{{$bank->account_number}}]</option>
                                             @endforeach
                                         </select>
                                 </div>
                                 --}}

                                <div class="form-group col-sm-12">
                                    <input type="submit" value="Disburse"  id="disburse" disabled="disabled" class="btn btn-sm btn-primary"/>
                                </div>
                            </div>
                            <div class="row">

                            </div>
                        </div>
                    </div>



            </form>
        </div>
    </div>

@endsection
@section('script')
    <script>
        var status = true;
        $('.account_sub_header').on('change', function(e){
            var token = $("input[name='_token']").val();
            var account_sub_header = e.target.value;
            //ajax
            $.ajax({
                url: "<?php echo route('get-bank-details') ?>",
                method: 'POST',
                data: {account_sub_header:account_sub_header, _token:token},
                success: function(data) {
                    console.log(data);
                    $('#balance').empty();

                    //$.each(data, function(index, subCatObj){
                        $('#balance').val(data[0].currentBalance);
                        $('#bank_account_id').val(data[0].id);
                        $('#bank_details').val(data[0].bank_name+'['+data[0].account_number+']');
                    //});
                    var balance = parseFloat($('#balance').val());
                    var amount = parseFloat($('#amount').val());
                    //console.log(amount, balance);
                    checkBalance(balance, amount);
                }
            });

            //Check balance
            function checkBalance(balance, amount) {
                if (balance >= amount) {
                    //alert('greater');
                    document.getElementById('disburse').disabled = false;
                } else {
                    document.getElementById('disburse').disabled = true;
                }
            }
            //alert(amount);
        });
        $('.bank_account').on('change', function(e){
            var token = $("input[name='_token']").val();
            var bank_account = e.target.value;
            //ajax
            $.ajax({
                url: "<?php echo route('get-balance') ?>",
                method: 'POST',
                data: {bank_account:bank_account, _token:token},
                success: function(data) {
                    console.log(data);
                    $('#balance').empty();

                    //$.each(data, function(index, subCatObj){
                    $('#balance').val(data[0].currentBalance);
                    //});
                    var balance = parseFloat($('#balance').val());
                    var amount = parseFloat($('#amount').val());
                    //console.log(amount, balance);
                    checkBalance(balance, amount);
                }
            });

            //Check balance
            function checkBalance(balance, amount) {
                if (balance >= amount) {
                    //alert('greater');
                    document.getElementById('disburse').disabled = false;
                } else {
                    document.getElementById('disburse').disabled = true;
                }
            }
            //alert(amount);
        });
        $('.account_header').on('change', function(e){
            var token = $("input[name='_token']").val();
            var account_header = e.target.value;
            //ajax
            $.ajax({
                url: "<?php echo route('get-sub-headers') ?>",
                method: 'POST',
                data: {account_header:account_header, _token:token},
                success: function(data) {
                    console.log(data);
                    $('#account_sub_header').empty();

                    $.each(data, function(index, subCatObj){
                    //$('#balance').val(data[0].currentBalance).toFixed(2);
                        $('#account_sub_header').append('<option value="'+subCatObj.id+'">'+subCatObj.name+'</option>')
                    });
                }
            });
        });
    </script>
@endsection