@extends('layouts.erp')

@section('page_title')
    <h4>Bulk Bank Accounts Upload </h4><br>
@endsection

@section('content')
    <div class="row">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <form action="{{ url('finance/bank-account/bulk-upload') }}" method="post" enctype="multipart/form-data" >
            {{csrf_field()}}
            @if(isset($errors))
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endForeach
                        </ul>
                    </div>
                @endIf
            @endIf

            <div class="col-md-4">
                <div class="form-group">
                    <label class="">Select file</label>
                    <input name="excel_file" id="name" type="file" class="form-control" value="">
                </div>
                <div class="form-group">
                    <input type="submit" value="Upload" class="btn btn-danger btn-sm">
                </div>
                <div class="form-group">
                    <p>Click to <a style="color: blue; text-decoration: underline;" href="{{ url('bulk-upload-excel-csv-download') }}">download format here</a> </p>
                </div>
            </div>

        </form>

    </div>

@endsection

