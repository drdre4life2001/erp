@extends('layouts.erp')

@section('page_title')
    Add New Company Loan
@endsection

@section('content')

<div class="content">
    <section class="content-header">
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    @if(isset($errors))
                    @if(count($errors) > 0)
                    <div class="col-sm-12 alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endForeach
                        </ul>

                    </div>
                    @endIf
                    @endIf

                    @if(isset($message))
                    <div class="col-sm-12 alert alert-success">
                        <ul>
                            <li>{{$message}}</li>
                        </ul>
                    </div>
                    @endIf

                    <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>
                               <center>
                                    Company Loan
                               </center>
                            </h4>
                        </div> <br> <br>

                        <div class="card-block">

                            <div class="col-md-11 col-md-offset-1">
                                <form method="POST">
                                    <div class="row">
                                                <div class="col-lg-5">
                                                    <label for="">Company name: </label>
                                                            <select name="company_id" required class="form-control">
                                                                <option value="">Choose Company</option>
                                                                @foreach($companies as $company)
                                                                    <option value="{{ $company->id }}">{{ $company->name }}</option>
                                                                @endforeach
                                                            </select>
                                                    </label>
                                                    @if ($errors->has('company_name'))<p class="help-block" style="color: red">{{ $errors->first('company_name') }}</p> @endif <br> 
                                                </div>
                                                <div class="col-lg-5">
                                                    
                                                    <label for="">Select Bank: </label>
                                                            <select required name="bank" class="form-control">
                                                                <option value="">Choose Bank</option>
                                                                <option value="Guaranty Trust Bank">Guaranty Trust Bank</option>
                                                                <option value="Zenith Bank">Zenith Bank</option>
                                                                <option value="Diamond Bank">Diamond Bank</option>
                                                                <option value="Skye Bank">Skye Bank</option>
                                                                <option value="First Bank">First Bank</option>
                                                            </select>
                                                    </label>
                                                    @if ($errors->has('bank'))<p class="help-block" style="color: red">{{ $errors->first('bank') }}</p> @endif <br> 

                                                </div>     
                                    </div>
                                    <div class="row">
                                                <div class="col-lg-5 ">
                                                    <label for="">Amount: </label>
                                                    
                                                    <input type="text" placeholder="Enter amount here" required name="amount" id="input" class="form-control" value="" >
                                                    
                                                    @if ($errors->has('amount'))<p class="help-block" style="color: red">{{ $errors->first('amount') }}</p> @endif <br> 
                                                </div>
                                                <div class=" col-lg-5">
                                                    <label>Interest Rate</label>
                                                    <!-- <div class="input-group mb-2 mb-sm-0"> -->
                                                            <!-- <div class="input-group-addon"> <h5>%</h5> </div> -->
                                                            <input type="text" placeholder="Interest Rate" required name="interest_rate" id="input" class="form-control" value="" >
                                                    @if ($errors->has('interest_rate'))<p class="help-block" style="color: red">{{ $errors->first('interest_rate') }}</p> @endif <br> 
                                                </div>             
                                    </div>
                                    <div class="row">
                                        <div class=" col-lg-5">
                                            <label>Description for Loan Collection</label>
                                            <textarea class="form-control" required name="description">
                                                
                                            </textarea>
                                        </div>
                                        <div class="col-lg-5 ">
                                            <label for="">Period: </label>
                                            
                                            <input type="text"  placeholder="Period" required name="period" id="input" class="form-control" value="" >
                                            @if ($errors->has('period'))<p class="help-block" style="color: red">{{ $errors->first('period') }}</p> @endif <br> <br>
                                            
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-lg-offset-4">
                                        <input type="submit" value="Create" class="btn btn-success"/> <br> <br>
                                    </div>
                                </form>    
                                   
                            </div>

                        </div>
                    </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="all_loans">
        <div class="col-md-11 col-md-offset-1">
            <table class="table table-responsive" id="organizations-table">
                <thead>
                    <th>
                        <center>
                            S/N
                        </center>
                    </th>
                    <th>
                        <center>
                            Company Name
                        </center>
                    </th>
                    <th>
                        <center>
                            Amount
                        </center>
                    </th>
                    <th>
                        <center>
                            Interest Rate    
                        </center>
                    </th>
                    <th>
                        <center>
                            Bank
                        </center>
                    </th>
                    <th> 
                        <center>
                            Description
                        </center>
                    </th>
                    <th>
                        <center>
                            Period
                        </center>
                    </th>

                    <th colspan="3">
                        <center>
                            Action
                        </center>
                    </th>
                </thead>
                <tbody>
                <?php $num = 1; ?>
                @foreach($company_loans as $company_loan)
                    <tr>

                        <td>{{ $num++ }}</td>
                        <td>{!! $company_loan->company_name !!}</td>
                        <td>&#8358;{!! number_format($company_loan->amount, 2) !!}</td>
                        <td>{!! number_format($company_loan->interest_rate, 2) !!}<span style="font-weight: bold;">%</span></td>
                        <td>{!! $company_loan->bank !!}</td>
                        <td>{!! $company_loan->description !!}</td>
                        <td>{!! $company_loan->period !!}</td>
                        <td> <a href="{{ url('finance/add-loan-repayment', $company_loan->id) }}" class="btn btn-primary btn-xs">Add Loan Repayment</a> </td>
                        <td> <a href="{{ url('finance/view-payment-schedule', $company_loan->id) }}" class="btn btn-danger btn-xs">View Payment Schedule</a> </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
</div>

@endsection
