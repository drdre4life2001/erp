@extends('layouts.erp')

@section('page_title')
    <h4>Account Sub Headers </h4><br>
@endsection

@section('content')
    <div class="row">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <form action="{{ url('finance/account_sub_header') }}" method="post">
            {{csrf_field()}}
            @if(isset($errors))
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endForeach
                        </ul>
                    </div>
                @endIf
            @endIf

            <div class="col-md-4">
                <div class="form-group">
                    <label class="">Sub-Header Name</label>
                    <input name="name" id="name" type="text" class="form-control" value="{{old('name')}}" required>
                </div>
                <div class="form-group">
                    <label class="">Select Header</label><br>
                    <select name="account_header" class="form-control account_header" required>
                        <option value="">--Please select--</option>
                        @foreach($headers as $header)
                            <option value="{{$header->id}}">{{$header->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="">Add Parent Sub-Header</label><br>
                    <select name="parent_id" class="form-control account_header">
                    <option value="">--Please select--</option>
                    @foreach($subheaders as $header)
                        <option value="{{$header->id}}">{{$header->name}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="">Select Bank Account</label><br>
                    <select name="bank_account_id" class="form-control" required>
                        <option value="">--Please select--</option>
                        @foreach($banks as $bank)
                            <option value="{{$bank->id}}">{{$bank->bank_name}}[{{$bank->account_number}}]</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" value="Add" class="btn btn-danger btn-sm">
                </div>
            </div>

        </form>

    </div>
    <div class="row">
        <div class="col-sm-8">
            <table class='table table-bordered' id='saveRequests'>
                <thead>
                <tr>
                    <th>S/N</th>
                    <th>Header</th>
                    <th>Sub-Header</th>
                    <th>Bank Details</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($item))
                    <?php $i =  1; ?>
                    @foreach($item as $items)

                        <tr>
                            <!--Loop through all items for this procurement -->

                            <td>{{ $i++ }}</td>
                            <td>{{ $items->header }}</td>
                            <td>{{ $items->sub_header }}</td>
                            <td>{{ $items->bank_name }}[{{$items->account_number}}]</td>
                            <td>
                                <div class='btn-group' style="display:block">
                                    <!--Edit item -->
                                    <a href="" style="margin-right: 0px;"
                                       class='btn btn-default btn-xs' data-toggle='modal' data-target='#{{$items->id}}'>
                                        <i class="glyphicon glyphicon-eye-open"> </i> Edit</a>
                                    <!--delete this item -->

                                    <a href="" style="margin-right: 0px;"
                                       class='btn btn-danger btn-xs' data-toggle="modal" data-target="#{{$items->id.'_'}}">
                                        <i class="glyphicon glyphicons-delete"> </i> Delete</a>

                                </div>

                            </td>
                            <!-- Delete Modal -->
                            <div class="modal fade" id="{{$items->id.'_'}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action = "{{url('finance/account_sub_header/delete')}}" method="post">
                                            <div class="modal-body">
                                                Are you sure you want to delete this item?
                                                <input type="hidden" name="item_id" value="{{$items->id}}">
                                            </div>
                                            <div class="modal-footer">

                                                <a class="btn btn-default btn-ok" data-dismiss="modal">No</a>
                                                <input type="submit" value="Yes"  class="btn btn-danger">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--Modal ends -->
                            <!-- Modal -->
                            <div id="{{$items->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Edit Sub-Header</h4>
                                        </div>
                                        <form action="{{url('finance/account_sub_header/edit')}}" method="post">
                                            <div class="modal-body">
                                                <input type="hidden" name="item_id" value="{{$items->id}}">
                                                <div class="form-group col-sm-6">
                                                    <label class="">Sub Header</label>
                                                    <input name="name" id="name" type="text" class="form-control" value="{{$items->sub_header}}" required/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="">Header</label><br>
                                                    <select name="account_header" class="form-control account_header2">
                                                        @foreach($headers as $header)
                                                            <option value="{{$header->id}}" <?php if($header->name == $items->header){ echo 'selected'; } ?>>
                                                                {{$header->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="">Add Parent Sub-Header</label><br>
                                                    <select name="account_sub_header" class="form-control" id="account_sub_header2">
                                                        <option value="{{$items->parent_id}}">If applicable</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="">Bank Details</label><br>
                                                    <select name="bank_account_id" class="form-control">
                                                            @foreach($banks as $bank)
                                                                <option value="{{$bank->id}}" <?php if($bank->id == $items->bank_id){ echo 'selected'; } ?>>
                                                                    {{$bank->bank_name}}[{{$bank->account_number}}]
                                                                </option>
                                                            @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" value="update" name="submit" class="btn btn-danger">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            @section('script')
                                                <script>
                                                    $('.account_header2').on('change', function(e){
                                                        var token = $("input[name='_token']").val();
                                                        var account_header = e.target.value;
                                                        //ajax
                                                        $.ajax({
                                                            url: "<?php echo route('get-sub-headers') ?>",
                                                            method: 'POST',
                                                            data: {account_header:account_header, _token:token},
                                                            success: function(data) {
                                                                console.log(data);
                                                                $('#account_sub_header2').empty();
                                                                $('#account_sub_header2').append('<option value="0">Not Applicable</option>')
                                                                $.each(data, function(index, subCatObj){
                                                                    //$('#balance').val(data[0].currentBalance).toFixed(2);
                                                                    $('#account_sub_header2').append('<option value="'+subCatObj.id+'">'+subCatObj.name+'</option>')
                                                                });
                                                            }
                                                        });
                                                    });
                                                </script>
                                            @endsection
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.account_header').on('change', function(e){
            var token = $("input[name='_token']").val();
            var account_header = e.target.value;
            //ajax
            $.ajax({
                url: "<?php echo route('get-sub-headers') ?>",
                method: 'POST',
                data: {account_header:account_header, _token:token},
                success: function(data) {
                    console.log(data);
                    $('#account_sub_header').empty();
                    $('#account_sub_header').append('<option value="0">Not Applicable</option>')
                    $.each(data, function(index, subCatObj){
                        //$('#balance').val(data[0].currentBalance).toFixed(2);
                        $('#account_sub_header').append('<option value="'+subCatObj.id+'">'+subCatObj.name+'</option>')
                    });
                }
            });
        });
        
        $('#saveRequests').DataTable({
            searching: false,
            "pagingType": "full_numbers",
            dom: 'Bfrtip',
            buttons: [
                'csv', 'excel','print'
            ]
        });

        //console.log(categories);
        var result = {};
        var items = {}
        var selectedCategory = "";
        //loop through the categories
        for (var i = 0; i < categories.length; i++) {
            //save the categories and categories items object
            result[categories[i].name] = categories[i].items;
            //loop through the items in the category
            for (var j = 0; j < categories[i].items.length; j++) {
                //save the items data; [items id => items obj]
                items[categories[i].items[j].id] = categories[i].items[j];
            }


        }

        //console.log(result);
        //console.log(items);

        $('select[name="category"]').change(function () { // When category select changes

            //set the unit price to default
            $('#unit_price').val('');
            $('.description').replaceWith("<div class='description col-sm-12'><p><b></b><p></div>");
            //var p = document.querySelector('.other_product_name');
            //p.innerHTML = "";
            //$('#other_product_item').fadeOut();

            /**if ($(this).val() == "Others"){
                var productfield = document.createElement('input');
                productfield.setAttribute('type', 'text');
                productfield.setAttribute('name', 'name');
                productfield.setAttribute('placeholder', "Product Eg: Dry Yam");
                productfield.classList.add('form-control');


            }else{*/
            selectedCategory = $(this).val();
            var options = '<option>Choose one!</option>';
            $.each(result[$(this).val()] || [], function (i, v) { // Cycle through each associated items
                options += '<option value="' + v.id + '">' + v.name + '</option>';
            });
            //options += '<option>' + "Other" + '</option>';
            var itemtfield = document.createElement('select');
            itemtfield.setAttribute('name', 'item');
            itemtfield.classList.add('form-control');
            itemtfield.innerHTML = options;
            /**} */

            var p = document.querySelector('.item_name');
            p.innerHTML = "";
            p.appendChild(itemtfield);
            $('#category_item').fadeIn();
            setItemListener();
//            $('select[name="product"]').html(options); // And update the role options
        });

        function setItemListener() {
            $('select[name="item"]').change(function () {

                $(this).attr('name', 'item');
                //auto-set the unit price, qty, other information
                //gets its information
                var itemObj = items[$(this).val()];
                $('#unit_price').val(itemObj.unit_price);
                $('.description').replaceWith("<div class='description col-sm-12'><p><i>" + itemObj.description + "</i><p></div>");
                //console.log(itemObj);
                //console.log();
            })
        }
    </script>
@endSection
