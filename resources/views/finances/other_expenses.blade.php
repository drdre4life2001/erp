@extends('layouts.erp')
@section('page_title')
    Viewing <span style="color:red;"> Expenses</span>
@endsection

@section('content')

    <div class="content">
        <section class="content-header">
        </section>
        <div class="content">
            <div class="box box-primary">

                <div class="box-body">
                    <div class="row">
                        @if(isset($errors))
                            @if(count($errors) > 0)
                                <div class="col-sm-12 alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endForeach
                                    </ul>

                                </div>
                            @endIf
                        @endIf

                        @if(isset($message))
                            <div class="col-sm-12 alert alert-success">
                                <ul>
                                    <li>{{$message}}</li>
                                </ul>
                            </div>
                        @endIf

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>
                                        <center>
                                            <label for=""><span style="color:red;"></span>Expenses Disbursements</label>
                                        </center>
                                    <!-- </h4> <a style="color: blue;" href="{{ url('finance/back') }}"><<< Go-Back</a> -->
                                </div>
                                <br> <br>
                                <!-- company id, loan id , amount of loan, amount paid so far, -->
                                <div class="card-block">

                                    <div class="col-md-9 col-md-offse">
                                        <div class="row">
                                            <table class="table table-responsive" id="organizations-table">
                                                <thead style="width:100%;">
                                                <th>
                                                    <center>
                                                        S/N
                                                    </center>
                                                </th>
                                                <th>
                                                    <center>
                                                        Title
                                                    </center>
                                                </th>
                                                <th>
                                                    <center>
                                                        Description
                                                    </center>
                                                </th>
                                                <th>
                                                    <center>
                                                        Requested from
                                                    </center>
                                                </th>

                                                <th>
                                                    <center>
                                                        Expense Amount
                                                    </center>
                                                </th>

                                                <th>
                                                    <center>
                                                        Account Header
                                                    </center>
                                                </th>
                                                
                                                <th>
                                                    <center>
                                                        Action
                                                    </center>
                                                </th>
                                                </thead>
                                                <tbody>
                                                <?php $num = 1; ?>
                                                @foreach($other_expenses as $expense)
                                                    <tr>
                                                        <td>
                                                            <center>{!! $num++ !!}</center>
                                                        </td>
                                                        <td>
                                                            <center>
                                                               {{ $expense->title }}
                                                            </center>
                                                        </td>
                                                        <td>
                                                            <center>
                                                                {{ $expense->description }}
                                                            </center>
                                                        </td>
                                                        <td>
                                                            <center>
                                                                {{ $expense->owner }}
                                                           </center>
                                                        </td>

                                                        <td>
                                                            <center>  &#8358;
                                                                {{ number_format($expense->amount, 2) }}
                                                            </center>
                                                        </td>

                                                        <td>
                                                            <center>
                                                                {{ $expense->category }}
                                                            </center>
                                                        </td>
                                                        <td>
                                                            <center>
                                                                @if($expense->disbursed == "false")
                                                                    <a href="#modal-expense{{$expense->id}}"
                                                                       data-toggle="modal" class="btn btn-sm btn-primary">Disburse</a>
                                                                    @else
                                                                    <a class="btn btn-sm btn-default">
                                                                        Disbursed</a>
                                                                @endif
                                                            </center>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @foreach($other_expenses as $expense)
            <div class="modal fade" id="modal-expense{{$expense->id}}" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        @if(isset($errors))
                            @if(count($errors) > 0)
                                <div class="col-sm-12 alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endForeach
                                    </ul>

                                </div>
                            @endIf
                        @endIf
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Disburse  (<span style="color:red;">{{ $expense->owner }}'s Expense</span>) </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{ url('finance/disbursement/other-expense') }}">
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Bank Account:</label>
                                    <select class="form-control bank_id" required name="bank_id" id="bank_id">

                                        <option class="">Choose Bank</option>
                                        @foreach($banks as $bank)
                                            <option class="{{ $bank->id }}"
                                                    value="{{ $bank->id }}">{{ $bank->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="div_amount" id="div_amount" style="display: none;">
                                    <div class="form-group">
                                        <label style="color: #222  !important;"> Balance: </label>
                                        {{--<input type="text" name="bank_amount" readonly>--}}
                                        <select class="form-control bank_balance" disabled id="bank_balance" name="amount_left"
                                                required="">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Account Header:</label>
                                    <select required="required" class="form-control account_header_id" name="account_header_id" id="account_header_id">

                                        <option class="">Choose Account Header</option>
                                        @foreach($account_headers as $account_header)
                                            <option class="" value="{{ $account_header->id }}">
                                                {{ $account_header->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="subheader_lists" id="subheader_lists" style="display: none;">
                                    <div class="form-group" >
                                        <label > Select Subheader: </label>
                                        <select required class="form-control subheader_id" id="subheader_id" name="subheader_id" required="">

                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="message-text" class="form-control-label">Expense Amount:</label>
                                    <input class="form-control" required readonly value="{{ $expense->amount }}" name="amount" id="message-text" type="text"/>
                                </div>

                                <input type="hidden" readonly="" name="expense_id" value="{{ $expense->id }}">

                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-primary" value="Disburse"/>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                        </form>

                    </div>
                </div>
            </div>
    @endforeach
@endsection
