@extends('layouts.erp')

@section('page_title')
    Approved Expenses
@endsection

@section('content')

    <div class="content">
        <section class="content-header">
        </section>
        <div class="content">
            <div class="box box-primary">

                <div class="box-body">
                    <div class="row">
                        @if(isset($errors))
                            @if(count($errors) > 0)
                                <div class="col-sm-12 alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endForeach
                                    </ul>

                                </div>
                            @endIf
                        @endIf

                        @if(isset($message))
                            <div class="col-sm-12 alert alert-success">
                                <ul>
                                    <li>{{$message}}</li>
                                </ul>
                            </div>
                        @endIf

                        <div class="col-sm-12">

                            <div class="card-block">

                                <div class="col-md-9 col-md-offset-1" id="all_expenses">
                                    @if(count($other_expenses))
                                        <div class="row">
                                            <center> <small style="color: red;">My Expenses: ({{ count($other_expenses) }}) </small> </center>
                                            <table class="table table-responsive table-hover" id="organizations-table">
                                                <thead>
                                                <th>
                                                    <center>
                                                        S/N
                                                    </center>
                                                </th>
                                                <th>
                                                    <center>
                                                        Title
                                                    </center>
                                                </th>
                                                <th>
                                                    <center>
                                                        Description
                                                    </center>
                                                </th>
                                                <th>
                                                    <center>
                                                        Category
                                                    </center>
                                                </th>
                                                <th>
                                                    <center>
                                                        Expected Amount
                                                    </center>
                                                </th>

                                                </thead>
                                                <tbody>
                                                <?php $num = 1; ?>
                                                @foreach($other_expenses as $expense)
                                                    <tr>
                                                        <td>
                                                            <center>
                                                                ({{ $num++ }})
                                                            </center>
                                                        </td>
                                                        <td>
                                                            <center> {{ $expense->title }} </center>
                                                        </td>
                                                        <td>
                                                            <center>
                                                                {{ $expense->description }}
                                                            </center>
                                                        </td>
                                                        <td>
                                                            {{ $expense->category }}<span style="font-weight: bold;"></span>
                                                        </td>

                                                        <td>  &#8358;
                                                            {{ number_format($expense->amount, 2) }}<span style="font-weight: bold;"></span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            <hr>

                                        </div>
                                    @else
                                        <div class="row">
                                            <div>
                                                <center> <b>None of your expense has been Approved. </b> </center>
                                            </div>
                                        </div>
                                    @endif

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

