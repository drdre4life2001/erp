@extends('layouts.erp')

@section('page_title')
    All Expenses
@endsection

@section('content')

    <div class="content">
        <section class="content-header">
        </section>
        <div class="content">
            <div class="box box-primary">

                <div class="box-body">
                    <div class="row">
                        @if(isset($errors))
                            @if(count($errors) > 0)
                                <div class="col-sm-12 alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endForeach
                                    </ul>

                                </div>
                            @endIf
                        @endIf

                        @if(isset($message))
                            <div class="col-sm-12 alert alert-success">
                                <ul>
                                    <li>{{$message}}</li>
                                </ul>
                            </div>
                        @endIf

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>
                                        <center>
                                                <b>
                                                    Create an Expense
                                                </b>
                                        </center>
                                    </h4>
                                </div> <br> <br>

                                <div class="card-block">

                                    <div class="col-md-10 col-md-offset-4">
                                        <form method="POST" action="">
                                            <div class="row">
                                                    <div class="col-lg-5">
                                                        <label for=""> Title: </label>

                                                        <input type="text" required placeholder="Expense Title" name="title" id="input" class="form-control" value="" >

                                                        @if ($errors->has('title'))<p class="help-block" style="color: red">{{ $errors->first('title') }}</p> @endif <br>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <label for=""> Choose Category: </label>

                                                    <select name="account_header_id" required id="" class="form-control">
                                                        <option value="" >Choose Category</option>
                                                        @foreach($categories as $category)
                                                            <option value="{{$category->id}}" >{{ $category->name }}</option>
                                                        @endforeach
                                                    </select>

                                                    @if ($errors->has('account_header_id'))<p class="help-block" style="color: red">{{ $errors->first('account_header_id') }}</p> @endif <br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <label>Content</label>
                                                    <textarea required name="description" rows="5" cols="2" id="input" class="form-control" value="Description goes here" > </textarea>
                                                    @if ($errors->has('content'))<p class="help-block" style="color: red">{{ $errors->first('content') }}</p> @endif <br>
                                                </div>
                                                <br> <br>
                                            </div>

                                            <div class="row">
                                                    <div class="col-lg-5">
                                                        <label for=""> Expected Amount: </label>

                                                        <input type="number" required placeholder="Expected Amount" name="amount"  id="input" class="form-control" value="" >

                                                        @if ($errors->has('amount'))<p class="help-block" style="color: red">{{ $errors->first('amount') }}</p> @endif <br>
                                                    </div>
                                            </div>
                                             <br> <br>
                                            <div class="col-lg-2 col-lg-offset-1">
                                                    <input type="submit" value="Submit Expense Request" class="btn btn-success"/> <br> <br>
                                            </div>

                                        </form>

                                    </div>

                                </div>
                            </div>

                            <div class="card-block">

                                <div class="col-md-9 col-md-offset-1" id="all_expenses">
                                        @if(count($other_expenses))
                                            <div class="row">
                                                <center> <small style="color: red;">My Expenses: ({{ count($other_expenses) }}) </small> </center>
                                                <table class="table table-responsive table-hover" id="organizations-table">
                                                    <thead>
                                                        <th>
                                                            <center>
                                                                S/N
                                                            </center>
                                                        </th>
                                                        <th>
                                                            <center>
                                                                Title
                                                            </center>
                                                        </th>
                                                        <th>
                                                            <center>
                                                                Description
                                                            </center>
                                                        </th>
                                                        <th>
                                                            <center>
                                                                Category
                                                            </center>
                                                        </th>
                                                        <th>
                                                            <center>
                                                                Expected Amount
                                                            </center>
                                                        </th>
                                                        <th>
                                                            <center>
                                                                Requested From
                                                            </center>
                                                        </th>
                                                        <th>
                                                            <center>
                                                                Status
                                                            </center>
                                                        </th>
                                                        {{-- <th>
                                                            <center>
                                                                Approved by
                                                            </center>
                                                        </th> --}}
                                                        <th>
                                                            <center>
                                                                Approve
                                                            </center>
                                                        </th>
                                                        <th>
                                                            <center>
                                                                Decline
                                                            </center>
                                                        </th>
                                                    </thead>
                                                    <tbody>
                                                    <?php $num = 1; ?>
                                                    @foreach($other_expenses as $expense)
                                                        <tr>
                                                            <td>
                                                                <center>
                                                                    ({{ $num++ }})
                                                                </center>
                                                            </td>
                                                            <td>
                                                                <center> {{ $expense->title }} </center>
                                                            </td>
                                                            <td>
                                                                <center>
                                                                    {{ $expense->description }}
                                                                </center>
                                                            </td>
                                                            <td>
                                                                {{ $expense->category }}<span style="font-weight: bold;"></span>
                                                            </td>

                                                             <td>  &#8358;
                                                                {{ number_format($expense->amount, 2) }}<span style="font-weight: bold;"></span>
                                                            </td>

                                                            <td>
                                                                {{ $expense->owner }}<span style="font-weight: bold;"></span>
                                                            </td>

                                                            <td>
                                                                {{ $expense->status }}<span style="font-weight: bold;"></span>
                                                            </td>
                                                            <td>
                                                                <center>
                                                                @if($expense->status == "pending")
                                                                        <a href="{{ url('other-expenses/approveExpense', $expense->id) }}" data-toggle="modal" class="btn btn-xs btn-primary">Approve</a>
                                                                @elseif($expense->status == "declined")
                                                                        <a class="btn btn-xs btn-danger">Declined</a>
                                                                @elseif($expense->status == "approved")
                                                                        <a class="btn btn-xs btn-success">Approved</a>
                                                                @endif
                                                                </center>
                                                            </td>
                                                            <td>
                                                                <center>
                                                                    @if($expense->status == "pending")
                                                                        <a href="{{ url('other-expenses/declineExpense', $expense->id) }}"
                                                                            class="btn btn-xs btn-primary">Decline</a>
                                                                    @elseif($expense->status == "declined")
                                                                        <a class="btn btn-xs btn-danger">
                                                                            Declined</a>
                                                                    @else
                                                                        <a class="btn btn-xs btn-success">
                                                                            Approved</a>
                                                                    @endif
                                                                </center>
                                                            </td>
                                                        </tr>

                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                <hr>

                                            </div>
                                        @else
                                            <div class="row">
                                                <div>
                                                    <center> <b>No Expense Available </b> </center>
                                                </div>
                                            </div>
                                        @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

