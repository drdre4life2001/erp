@extends('erp.layouts.master')

@section('title')
    Job Roles
@endsection

@section('sidebar')
    @include('erp.partials.sidebar')
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Job Role
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Job</li>
        </ol>
    </section>
    <section class="content">
        @if(isset($errors))
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}} here</li>
                        @endForeach
                    </ul>
                </div>
            @endIf
        @endIf
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Job
                            </b></a>
                    </li>
                </ul>
                <div class="tab-content" style="padding: 2%">
                    <div class="tab-pane active" id="tab_1">
                        <p align="right">
                            <a href="{{url('/jobs')}}" class="btn btn-large btn-purple"><i class="fa fa-eye" aria-hidden="true" style="color:white"></i>  Jobs
                            </a>
                        </p>
                        <div class="adf" style="background:#ffffff; float: left; width: 100%; ">
                            <div class="col-md-1 hidden-sm hidden-xs"></div>
                            <div class="col-md-10">
                                <form action="{{route('jobs.store')}}" method="post">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                {!! Form::label('name', 'Name:') !!}
                                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'E.g : Finance Analyst']) !!}
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <!-- Code Field -->
                                            <div class="form-group">
                                                {!! Form::label('code', 'Internal Code:') !!}
                                                {!! Form::text('code', null, ['class' => 'form-control', 'placeholder' => 'E.g : JB001']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                {!! Form::label('description', 'Description:') !!}
                                                {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'E.g : His role/description']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-lg-offset-4">
                                            <!-- Submit Field -->
                                            <div class="form-group col-sm-12">
                                                {!! Form::submit('Save', ['class' => 'btn btn-purple btn-lg']) !!}
                                                <a href="{!! route('jobs.index') !!}" class="btn btn-default btn-lg">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-1 hidden-sm hidden-xs"></div>
                        </div>
                        <hr/ style="clear: both">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript">
        $("#example1").DataTable();
    </script>
@endsection