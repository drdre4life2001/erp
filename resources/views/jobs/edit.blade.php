@extends('erp.layouts.master')

@section('title')
    Job Roles
@endsection

@section('sidebar')
    @include('erp.partials.sidebar')
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Employee
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Job</li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Job
                            </b></a>
                    </li>
                </ul>
                <div class="tab-content" style="padding: 2%">
                    <div class="tab-pane active" id="tab_1">
                        <p align="right">
                            <a href="{{url('jobs/create')}}" class="btn btn-large btn-purple"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add New Job
                            </a>
                        </p>
                        <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                            <div class="col-md-1 hidden-sm hidden-xs"></div>
                            <div class="col-md-10">
                                <form action="{{ url('jobs/edit', $job->id) }}" method="POST">

                                    <div class="form-group col-sm-6">
                                        {!! Form::label('name', 'Name:') !!}
                                        {!! Form::text('name', $job->name, ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- Code Field -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('code', 'Internal Code:') !!}
                                        {!! Form::text('code', $job->code, ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- Description Field -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('description', 'Description:') !!}
                                        {!! Form::text('description', $job->description, ['class' => 'form-control']) !!}
                                    </div>


                                    <!-- Submit Field -->
                                    <div class="form-group col-sm-12">
                                        {!! Form::submit('Update', ['class' => 'btn btn-purple']) !!}
                                        <a href="{!! route('jobs.index') !!}" class="btn btn-default">Cancel</a>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-1 hidden-sm hidden-xs"></div>
                        </div>
                        <hr/ style="clear: both">
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </section>
@endsection

@section('script')
    <script type="text/javascript">
        $("#example1").DataTable();
    </script>
@endsection
