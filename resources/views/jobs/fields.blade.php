@extends('layouts.erp')

@section('page_title')
    <h4>Create Job Role</h4>
@endsection

@section('content')
    <section class="content-header">
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    @if(isset($errors))
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}} here</li>
                                    @endForeach
                                </ul>
                            </div>
                        @endIf
                    @endIf
                        <form action="{{route('jobs.store')}}" method="post">
                            <div class="row"><!-- Name Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('name', 'Name:') !!}
                                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                </div>

                                <!-- Code Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('code', 'Internal Code:') !!}
                                    {!! Form::text('code', null, ['class' => 'form-control']) !!}
                                </div>

                                <!-- Description Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('description', 'Description:') !!}
                                    {!! Form::text('description', null, ['class' => 'form-control']) !!}
                                </div>


                                <!-- Submit Field -->
                                <div class="form-group col-sm-12">
                                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                    <a href="{!! route('jobs.index') !!}" class="btn btn-default">Cancel</a>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
@endsection