@extends('layouts.erp')

@section('page_title')
@endsection

@section('content')
<div class="content">

    @if(isset($errors))
    @if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{$error}} here</li>
            @endForeach
        </ul>

    </div>
    @endIf
    @endIf

    <div class="row">
        <div class="col-sm-6">

            <div class="card">
                <div class="card-header">
                    <strong class="pull-left">Create Permissions</strong>
                </div>

                <div class="card-block">
                    {!! Form::open(['route' => 'permissions.store']) !!}

                    @include('permissions.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
