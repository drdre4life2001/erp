




<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-12">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-12">
    {!! Form::label('level', 'Level:') !!}
    <select name="level" class="form-control">
        <option value="0">Single Level</option>
        <option value="1">Multiple Level</option>
    </select>
</div>


<!-- Id Sys Modules Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_sys_modules', 'Module:') !!}
    <select name="id_sys_modules" class="form-control">
        @if(isset($modules))
            @foreach($modules as $module)
                <option value="{{$module->id}}">{{$module->name}}</option>
            @endForeach
        @endIf
    </select>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('permissions.index') !!}" class="btn btn-default">Cancel</a>
</div>
