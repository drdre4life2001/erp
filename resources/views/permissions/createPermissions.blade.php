@extends('layouts.erp')

@section('page_title')
@endsection

@section('content')
<div class="content">

    @if(isset($errors))
    @if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{$error}} here</li>
            @endForeach
        </ul>

    </div>
    @endIf
    @endIf

    <div class="row">
        <div class="col-sm-6">

            <div class="card">
                <div class="card-header">
                    <strong class="pull-left">Create Permissions</strong>
                </div>

                <div class="card-block">
                    <form method="post" action="{{url('permissions/store')}}">
                        <div class="form-group col-sm-12">
                            <label class="">Name</label>
                            <input name="name" id="name" type="text" class="form-control"/>
                        </div>
                        <div class="form-group col-sm-12">
                            <label class=""> Description</label>
                            <textarea name="description" class="form-control"></textarea>
                        </div>
                        
                        <div class="form-group col-sm-12">
                            <input type="submit" value="Submit" class="btn btn-sm btn-primary"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
