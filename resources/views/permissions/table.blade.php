<table class="table table-responsive table-striped" id="permissions-table">
    <thead>
        <th>Name</th>
        <th>Slug</th>
        <th>Level</th>
        <th>Description</th>
        <th>Module</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($permissions as $permissions)
        <tr>
            <td>{!! $permissions->name !!}</td>
            <td>{!! $permissions->slug !!}</td>
            <td>{!! $permissions->level == 1 ? 'Multiple' : 'Single' !!}</td>
            <td>{!! $permissions->description !!}</td>
            <td>{!! $permissions->module->name !!}</td>
            <td>
                {!! Form::open(['route' => ['permissions.destroy', $permissions->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('permissions.show', [$permissions->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-eye"></i></a>
                    <a href="{!! route('permissions.edit', [$permissions->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>