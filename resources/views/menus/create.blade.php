@extends('layouts.erp')

@section('page_title')
@endsection

@section('content')
@if(isset($errors))
@if(count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{$error}} here</li>
        @endForeach
    </ul>

</div>
@endIf
@endIf

<div class="row">
    <div class="col-sm-12">

        <div class="card">
            <div class="card-header">
                <strong class="pull-left">Create Menu</strong>
            </div>
            <br/>
            <div class="card-block">

                    {!! Form::open(['route' => 'menus.store']) !!}

                        @include('menus.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
