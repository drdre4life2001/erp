<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('url', 'Url:') !!}
    {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>

<!-- Id System Menu Group Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_system_menu_group', 'Menu Group:') !!}
    <select name="menu_group" class="form-control" >
        @foreach($menuGroups as $menuGroup)
        <option value="{{$menuGroup->id}}">{{$menuGroup->name}}</option>
        @endforeach

    </select>
</div>

<!-- Permission accessible -->
<div class="form-group col-sm-6">
    {!! Form::label('permission', 'Permission Required:') !!}
    <select name="permission" class="form-control" >
        @foreach($permissions as $permission)
        <option value="{{$permission->id}}">{{$permission->name}}</option>
        @endforeach

    </select>
</div>

<!-- level accessible -->
<div class="form-group col-sm-6">
    {!! Form::label('level', 'Level Required:') !!}
    <select name="level" class="form-control" >
        @foreach($levels as $key => $level )
        <option value="{{$key}}">{{$level}}</option>
        @endforeach

    </select>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('menus.index') !!}" class="btn btn-default">Cancel</a>
</div>
