@extends('layouts.erp')

@section('page_title')
@endsection
@section('error')
    @if(isset($errors))
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}} here</li>
                        @endForeach
                </ul>
            </div>
            @endIf
            @endIf
@endsection
@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Generate Payroll</h6>
                    </div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap mt-40">
                            <form action="{{ url('generate_payroll') }}" method="post">
                                <div class="form-group">
                                    <label class="control-label mb-10">Month</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="icon-user"></i></div>
                                        <select class="form-control selectpicker" data-show-subtext="true"
                                                data-live-search="true" name="month">
                                            @foreach($months as $month)
                                                <option data-subtext="{{ $month }}"
                                                        value="{{ $month }}">{{ $month }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10">Year</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="icon-user"></i></div>
                                        <select class="form-control selectpicker" data-show-subtext="true"
                                                data-live-search="true" name="year">
                                            @foreach($years as $month)
                                                <option data-subtext="{{ $month }}"
                                                        value="{{ $month }}">{{ $month }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10">Select Company</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="icon-user"></i></div>
                                        <select class="form-control selectpicker" data-show-subtext="true"
                                                data-live-search="true" name="id_hr_company">
                                            @foreach($companies as $company)
                                                <option data-subtext="{{ $company->name }}"
                                                        value="{{ $company->id }}">{{ $company->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-15">
                                        <input class="btn btn-success btn-anim" type="submit" value="Generate Payroll for This Period">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection

@section('script')

@endsection
