 @extends('layouts.erp')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Draft Pay Roll for {{ $month }}, {{ $year }}</h6>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('export_payroll/'.$month.'/'.$year.'/draft') }}"><h6 class="btn btn-success">Download to Excel</h6></a>
                        <a href="{{ url('confirm_payroll/'.$month.'/'.$year.'/'.$company) }}" onclick="confirm('Are you sure you want to confirm this payroll data?')"><h6 class="btn btn-success">Confirm {{ $month  }}, {{ $year }} Payroll</h6></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                @include('hr.elements.payroll_data')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection