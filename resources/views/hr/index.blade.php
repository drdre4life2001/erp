@extends('layouts.app_hr')

@section('page_title')
<h2>Human Resource [Dashboard]</h2>
@endsection

@section('hr_content')

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script
    src="http://code.jquery.com/jquery-3.1.1.js"
    integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="
    crossorigin="anonymous"></script>


<div class="row">
<div class="col-md-6">
    <div id="pie_chart"
         style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
</div>
    <div class="col-md-6">
        <div id="column_chart_single"
             style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
</div>

<div>
    <div class="col-md-12">
        <div id="column_chart"
             style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
</div>
@endsection

@section('script')
<script>

$(function () {
    Highcharts.chart('pie_chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Employees by gender'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Male',
                y: 56.33
            }, {
                name: 'Female',
                y: 24.03,
                sliced: true,
                selected: true
            }]
        }]
    });
});

$(function () {
    Highcharts.chart('column_chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Employees by Department'
        },
        subtitle: {
            text: 'Source: Taerp'
        },
        xAxis: {
            categories: [
                'Accounts',
                'Legal',
                'Finance'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of employees'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} Million</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            {
                name: 'Male',
                data: [49.9, 71.5, 106.4]

            },
            {
                name: 'Female',
                data: [83.6, 78.8, 98.5]

            },
            {
                name: 'Others',
                data: [48.9, 38.8, 39.3]

            }
        ]
    });
});

$(function () {
    Highcharts.chart('column_chart_single', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Number of Employees over the last 3 months'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} Million</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            {
                name: 'Employees',
                data: [49.9, 71.5, 106.4]

            }
        ]
    });
});

</script>

@endsection
