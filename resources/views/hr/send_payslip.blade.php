@extends('layouts.erp')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Send Payslips to Employees</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <div class="form-wrap mt-40">
                                    <form action="{{ url('send_payslip') }}" method="post">
                                        <div class="form-group">lipsfefe
                                            <label class="control-label mb-10" for="exampleInputuname_1">Month</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                <select class="form-control selectpicker" data-show-subtext="true"
                                                        data-live-search="true" name="month">
                                                    @foreach($months as $month)
                                                        <option data-subtext="{{ $month }}"
                                                                value="{{ $month }}">{{ $month }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputuname_1">Year</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                <select class="form-control selectpicker" data-show-subtext="true"
                                                        data-live-search="true" name="year">
                                                    @foreach($years as $month)
                                                        <option data-subtext="{{ $month }}"
                                                                value="{{ $month }}">{{ $month }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-15">
                                                <input class="btn btn-success btn-anim" type="submit" value="Send Payslip">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection