 @extends('layouts.erp')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Payrolls History</h6>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('generate_payroll') }}"><h6 class="btn btn-primary">Generate Another Payroll</h6></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                                <div class="form-wrap mt-40">
                                    <form action="{{ url('get_payroll') }}" method="post">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputuname_1">Month</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                <select class="form-control selectpicker" data-show-subtext="true"
                                                        data-live-search="true" name="month">
                                                    @foreach($months as $month)
                                                        <option data-subtext="{{ $month }}"
                                                                value="{{ $month }}">{{ $month }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputuname_1">Year</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                <select class="form-control selectpicker" data-show-subtext="true"
                                                        data-live-search="true" name="year">
                                                    @foreach($years as $month)
                                                        <option data-subtext="{{ $month }}"
                                                                value="{{ $month }}">{{ $month }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-15">
                                                <input class="btn btn-success btn-anim" type="submit" value="View Payroll">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection