<table id="example_1" class="table table-hover table-bordered" >
    <thead class="panel panel-heading">
    <tr>
        <th>S/N</th>
        <th class="">Employee Name</th>
        <th>Basic Salary</th>
        <th>Housing Allowance</th>
        <th>Transport Allowance</th>
        <th>Entertainment Allowance</th>
        <th>Leave Allowance</th>
        <th>Utility Allowance</th>
        <th>N.H.F</th>
        <th>Life Assurance</th>
        <th>Consolidated Relief Allowance</th>
        <th>Fixed CRA</th>
        <th>Pension</th>
        <th>Arrears</th>
        <th>Salary Advance</th>
        <th>Absence</th>
        <th>Other Deductions</th>
        <th>Tax</th>
        <th>Net Pay</th>
    </tr>
    </thead>
    <tbody>
    <?php $i = 1;?>
    <?php foreach($payrolls as $bonus){ ?>
    <tr>
        <td>{{ $i++ }}</td>
        <td>{{ $bonus->employee }}</td>
        <td>&#x20A6;{{ number_format($bonus->salary,2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->housing, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->transport, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->entertainment, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->leave, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->utility, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->nhf, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->life_assurance,2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->cra, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->fixed_cra, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->pension, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->arrears, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->salary_advance, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->absence, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->other_deductions, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->tax, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->net_pay, 2) }}</td>
    </tr>
    <?php } ?>
    </tbody>
</table>