<table id="example1" class="table table-hover" >
    <thead class="panel panel-heading">
    <tr>
        <th>S/N</th>
        <th class="">Employee Name</th>
        <th>Basic Salary</th>
        <th>Housing Allowance</th>
        <th>Transport Allowance</th>
        <th>Entertainment Allowance</th>
        <th>Leave Allowance</th>
        <th>Utility Allowance</th>
        <th>N.H.F</th>
        <th>Life Assurance</th>
        <th>Consolidated Allowance</th>
        <th>Fixed CRA</th>
        <th>Pension</th>
        <th>Arrears</th>
        <th>Salary Advance</th>
        <th>Export Pay Slip</th>
        <th>View Pay Slip</th>

    </tr>
    </thead>
    <tbody>

    <?php $i = 1;?>
    <?php foreach($payrolls as $bonus){ ?>
    <tr>
        <td>{{ $i++ }}</td>
        <td>{{ @$bonus->employee->first_name }} {{ @$bonus->employee->lastname }}</td>
        <td>&#x20A6;{{ number_format($bonus->salary,2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->housing, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->transport, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->entertainment, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->leave, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->utility, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->nhf, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->life_assurance,2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->cra, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->fixed_cra, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->pension, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->arrears, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->salary_advance, 2) }}</td>
        <td>
            <a href="{{ url('export_payroll/'.$month.'/'.$year.'/confirmed'.'/'.$bonus->company_id.'/'.auth()->user()->company->uri) }}" class="btn btn-success" title="Export Payslip to {{ @$bonus->employee->first_name }} {{ @$bonus->employee->lastname }}"><i class="fa fa-envelope"></i></a>
        </td>
        <td>
            <a href="{{ url('payslip/'.$bonus->employee_id.'/'.$month.'/'.$year.'/'.auth()->user()->company->uri) }}" title="View Pay Slip for {{ @$bonus->employee->first_name }} {{ @$bonus->employee->lastname }}" class="btn btn-default" target="_blank"><i class="fa fa-eye"></i></a>
        </td>
    </tr>
    <?php } ?>
    </tbody>
</table>