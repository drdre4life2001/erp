@extends('erp.layouts.master')
  
  @section('title')
    CRM - Pipelines
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
      <section class="content-header">
         <h1>  All Pipelines    </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">All Pipelines
            </li>
         </ol>
      </section>
      <section class="content">
            <div class="col-md-12">
               <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                     <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>All Pipelines
                        </b></a>
                     </li>
                  </ul>
                  <div class="tab-content" style="padding: 2%">
                     <div class="tab-pane active" id="tab_1">
                        <p align="right">
                           {{-- <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add A New Pipeline
                           </button> --}}
                        </p>
                        <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                           <div class="col-md-1 hidden-sm hidden-xs"></div>
                           <div class="col-md-10">
                              {{-- <form>
                                 <h3 align="center"> Create Lead
                                 </h3>
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                       <label>ID
                                       </label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-building"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" placeholder="Company Name" readonly="">
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Status
                                       </label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-user"></i>
                                          </div>
                                          <select class="form-control pull-right">
                                             <option value=""> Choose </option>
                                             <option value="Lead">Lead</option>
                                             <option value="Open">Open</option>
                                             <option value="Replied">Replied</option>
                                             <option value="Oppurtunity">Oppurtunity</option>
                                             <option value="Quotation">Quotation</option>
                                             <option value="Lost QUotation">Lost Quotation</option>
                                             <option value="Interested">Interested</option>
                                             <option value="Converted">Converted</option>
                                          </select>
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Person Name
                                       </label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-user"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" placeholder="Person Name">
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Status
                                       </label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-user"></i>
                                          </div>
                                          <select class="form-control pull-right">
                                             <option value=""> Choose </option>
                                             <option value="Lead">Male</option>
                                             <option value="Open">Female</option>
                                          </select>
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Organization Name
                                       </label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-building"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" placeholder="Organization Name">
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Source
                                       </label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-building"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" placeholder="Source">
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>E-mail Address
                                       </label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-envelope"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" placeholder="Email Address">
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Lead Owner</label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-user"></i>
                                          </div>
                                          <select class="form-control pull-right">
                                             <option value=""></option>
                                             <option value="5">uchechukwu.obioha@gpayafrica.com</option>
                                             <option value="16">edmund@techadvance.ng</option>
                                             <option value="17">tunde@techadvance.ng</option>
                                             <option value="18">kiros@techadvance.ng</option>
                                             <option value="19">folawiyo.awodipe@techadvance.ng</option>
                                             <option value="20">wale.adeniji@techadvance.ng</option>
                                             <option value="21">olawale.quadri@techadvance.ng</option>
                                             <option value="22">kazeem.noibi@techadvance.ng</option>
                                             <option value="23">oladayo.raji@techadvance.ng</option>
                                             <option value="24">adetunji.akinde@techadvance.com</option>
                                             <option value="25">tosin.animashaun@techadvance.ng</option>
                                             <option value="26">tomiwa.adebayo@techadvance.ng</option>
                                             <option value="27">nnachi.sylvester@techadvance.ng</option>
                                             <option value="28">ifeoluwa.king@techadvance.ng</option>
                                             <option value="29">adetunji.adeosun@techadvance.ng</option>
                                             <option value="30">adeyemi.onafuye@techadvance.ng</option>
                                             <option value="31">lola.majekodunmi@techadvance.ng</option>
                                             <option value="32">wole.olorunleke@techadvance.ng</option>
                                             <option value="33">tolani.balogun@techadvance.ng</option>
                                             <option value="34">stephanie.ofoedu@techadvance.ng</option>
                                             <option value="35">noyoze.akenzua@techadvance.ng</option>
                                             <option value="36">ayodeji.awonuga@techadvance.ng</option>
                                          </select>
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Next Contact Date
                                       </label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-calendar"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" id="datepicker">
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Next Contact By
                                       </label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-user"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" placeholder="Next Contact By
                                             ">
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                 </div>
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                       <label>Telephone</label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-mobile"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" placeholder="Telephone">
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Salutation</label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-building"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" placeholder="Telephone">
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Mobile</label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-mobile"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" placeholder="Telephone">
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Fax</label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-print"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" placeholder="Fax">
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Website</label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-globe"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" placeholder="Website">
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Territory</label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-map"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" placeholder="Territory">
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Lead Type</label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-university"></i>
                                          </div>
                                          <select class="form-control pull-right">
                                             <option value=""> Choose </option>
                                             <option value="Client">Clinet</option>
                                             <option value="Channel Partner">Channel Partner</option>
                                             <option value="Consultant">Consultant</option>
                                          </select>
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Market Segment</label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-university"></i>
                                          </div>
                                          <select class="form-control pull-right">
                                             <option value=""> Choose </option>
                                             <option value="Lower Income">Lower Income</option>
                                             <option value="Middle Income">Middle Income</option>
                                             <option value="Upper Income">Upper Income</option>
                                          </select>
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Industry</label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-building"></i>
                                          </div>
                                          <select class="form-control pull-right">
                                             <option value=""> Choose </option>
                                             <option value="Venture Capital">Venture Capital</option>
                                             <option value="Telecommunications"> Telecommunications</option>
                                             <option value="Technology">Technology</option>
                                             <option value="Transportation">Transportation</option>
                                             <option value="Retail and Wholesale">Retail and Wholesale</option>
                                             <option value="Service">Service</option>
                                             <option value="News Papers and Publishers">News Papers and Publishers</option>
                                             <option value="Real Estate">Real Estate</option>
                                             <option value="Pharmaceuticals">Pharmaceuticals</option>
                                          </select>
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                       <label>Request Type</label>
                                       <div class="input-group date">
                                          <div class="input-group-addon">
                                             <i class="fa fa-university"></i>
                                          </div>
                                          <select class="form-control pull-right">
                                             <option value=""> Choose </option>
                                             <option value="Venture Capital">Product Inquiry</option>
                                             <option value="Request for information"> Request for information</option>
                                             <option value="Suggestions">Suggestions</option>
                                          </select>
                                       </div>
                                       <!-- /.input group -->
                                    </div>
                                 </div>
                                 <p align="center">
                                    <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    Save</button>
                                    <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                                    Cancel</a>
                                 </p>
                              </form> --}}
                           </div>
                           <div class="col-md-1 hidden-sm hidden-xs"></div>
                        </div>
                        <hr/ style="clear: both">
                        <div class="box-body table-responsive">
                           <table id="prospect" class="table table-bordered table-hover table-responsive">
                              <thead>
                                 <tr style="color:#8b8b8b">
                                    <th>S/N</th>
                                    <th>Prospect</th>
                                    <th>Date</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 @if(!empty($pipelines))
                                    <?php $i = 1; ?>
                                    @foreach($pipelines as $pipeline)
                                       <tr>
                                          <td> {{ $i++ }} </td>
                                          <td>
                                             {{ $pipeline->prospect }}
                                          </td>
                                          <td style="color:#1577c8; font-weight: bold">{{ $pipeline->activity_date }}</td>
                                          <td>
                                             {{$pipeline->title}}
                                          </td>
                                          <td>{{$pipeline->description}}.</td>
                                          <td>  
                                             <a href="#">
                                             <span class="label label-success" style="padding:12.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:;">
                                             <i class=""></i>  {{$pipeline->status}} </span></a>
                                          </td>
                                       </tr>
                                    @endforeach
                                 @endif
                                 </tfoot>
                           </table>
                        </div>
                     </div>
                  </div>
                  <!-- /.tab-content -->
               </div>
            </div>
      </section>
   @endsection

   @section('script')
    <script type="text/javascript">
       $('.adf').hide();
         $('.cdb').on('click', function(){
           $('.adf').slideToggle();
        
         });

           $('.cadfxx').on('click', function(){
              $('.adf').slideToggle();
           });

               //Date picker
           $('#datepicker').datepicker({
             autoclose: true
           });

               //Date picker
           $('#datepicker2').datepicker({
             autoclose: true
           });

            $('#reservation').daterangepicker();
           //Date range picker with time picker
           $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
           //Date range as a button

       $('#daterange-btn').daterangepicker(
               {
                 ranges: {
                   'Today': [moment(), moment()],
                   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                   'This Month': [moment().startOf('month'), moment().endOf('month')],
                   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                 },
                 startDate: moment().subtract(29, 'days'),
                 endDate: moment()
               },
               function (start, end) {
                 $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
               }
           );

       </script>
    </script>
    <script>
            $('#prospect').DataTable({
                searching: true,
                "pagingType": "full_numbers",
                dom: 'Bfrtip'
            });
            //$("#activity").datepicker();
    </script>
   @endsection

