<table id="example1" class="table table-bordered table-hover table-responsive">
   <thead>
      <tr style="color:#8b8b8b">
         <th>S/N</th>
         <th>Status</th>
         <th>Name</th>
         <th>Gender</th>
         <th>Company</th>
         <th>Source</th>
         <th>Email</th>
         <th>Lead Owner</th>
         <th>Telephone</th>
         <th>Request Type</th>
         <th>Next Contact Date</th>
         <th>Actions</th>
      </tr>
   </thead>
   <tbody>
      <?php $inc = 1; ?>
      @if(!empty($leads))
         @foreach($leads as $lead)
            <tr>
               <td>{{ $inc++ }}</td>
               <td>{{ ucfirst($lead->status) }}</td>
               <td>{{ $lead->person_name }}</td>
               <td>{{ $lead->gender }}</td>
               <td>{{ $lead->organization_name }}</td>
               <td>{{ $lead->source }}</td>
               <td>{{ $lead->email_address }}</td>
               <td>{{ $lead->lead_owner }}</td>
               <td>{{ $lead->phone }}</td>
               <td>{{ date("M d, Y",strtotime($lead->next_contact_date)) }}</td>
               <td>{{ $lead->request_type }}</td>
               <td>  
                  <a href="{{ url('/lead/'.$lead->id.'/edit') }}">
                  <span class="label label-warning" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                  <i class="fa fa-trash"></i>  Edit </span></a>
                  <a href="" id="a_del">
                  <span class="label label-danger" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                  <i class="fa fa-trash"></i>  Deactive </span></a>
               </td>
            </tr>
         @endforeach
      @endif
   </tbody>
      </tfoot>
</table>