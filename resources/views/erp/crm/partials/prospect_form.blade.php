<div class="col-md-10">
   <form method="POST" action="">
      <h3 align="center"> Create Prospect
      </h3>
      <div class="col-lg-6">
         <div class="form-group">
            <label>Company Name
            </label>
            <div class="input-group date">
               <div class="input-group-addon">
                  <i class="fa fa-building"></i>
               </div>
               <input type="text" required name="name" class="form-control pull-right" placeholder="Company Name">
               @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
            </div>
            <!-- /.input group -->
         </div>
         <div class="form-group">
            <label>Name Of Executive
            </label>
            <div class="input-group date">
               <div class="input-group-addon">
                  <i class="fa fa-user"></i>
               </div>
               <input type="text" required name="contact_person" class="form-control pull-right" placeholder="Name Of Executive">
               @if ($errors->has('contact_person')) <p class="help-block" style="color: red">{{ $errors->first('contact_person') }}</p> @endif
            </div>
            <!-- /.input group -->
         </div>
         <div class="form-group">
            <label>Position Of Executive
            </label>
            <div class="input-group date">
               <div class="input-group-addon">
                  <i class="fa fa-university"></i>
               </div>
               <input type="text" required name="contact_position" class="form-control pull-right" placeholder="Position Of Executive">
               @if ($errors->has('contact_position')) <p class="help-block" style="color: red">{{ $errors->first('contact_position') }}</p> @endif
            </div>
            <!-- /.input group -->
         </div>
         <div class="form-group">
            <label>Contract Address
            </label>
            <div class="input-group date">
               <div class="input-group-addon">
                  <i class="fa fa-map"></i>
               </div>
               <input type="text" required name="address" class="form-control pull-right" placeholder="Contract Address">
               @if ($errors->has('address')) <p class="help-block" style="color: red">{{ $errors->first('address') }}</p> @endif
            </div>
            <!-- /.input group -->
         </div>
      </div>
      <div class="col-lg-6">
         <div class="form-group">
            <label>Email Address
            </label>
            <div class="input-group date">
               <div class="input-group-addon">
                  <i class="fa fa-envelope"></i>
               </div>
               <input type="email" required" name="email" class="form-control pull-right" placeholder="Email Address
                  ">
                  @if ($errors->has('email')) <p class="help-block" style="color: red">{{ $errors->first('email') }}</p> @endif
            </div>
            <!-- /.input group -->
         </div>
         <div class="form-group">
            <label>Telephone</label>
            <div class="input-group date">
               <div class="input-group-addon">
                  <i class="fa fa-mobile"></i>
               </div>
               <input type="number" required name="phone" class="form-control pull-right" placeholder="Telephone">
               @if ($errors->has('phone')) <p class="help-block" style="color: red">{{ $errors->first('phone') }}</p> @endif
            </div>
            <!-- /.input group -->
         </div>
         <div class="form-group">
            <label>Prospect's Code</label>
            <div class="input-group date">
               <div class="input-group-addon">
                  <i class="fa fa-user"></i>
               </div>
               <input type="text" required name="code" class="form-control pull-right" placeholder="Prospect's Code">
               @if ($errors->has('code')) <p class="help-block" style="color: red">{{ $errors->first('code') }}</p> @endif
            </div>
            <!-- /.input group -->
         </div>
         <div class="form-group">
            <label>Estimated Revenue</label>
            <div class="input-group date">
               <div class="input-group-addon">
                  <i class="fa fa-bar-chart"></i>
               </div>
               <input type="text" required name="revenue" class="form-control pull-right" placeholder="Estimated Revenue">
               @if ($errors->has('revenue')) <p class="help-block" style="color: red">{{ $errors->first('revenue') }}</p> @endif
            </div>
            <!-- /.input group -->
         </div>
      </div>
      <div class="col-lg-12">
         <div class="form-group">
            <label>Assign Prospect To A Subsidiary</label>
            <div class="input-group date">
               <div class="input-group-addon">
                  <i class="fa fa-university"></i>
               </div>
               <select name="subsidiary" required="" class="form-control pull-right">
                  <option value="">--Please select--</option>
                  @foreach($organizations as $organization)
                     <option value="{{ $organization->id }}">{{ $organization->name }}</option>
                  @endforeach
               </select>
            </div>
         </div>
      </div>
      <p align="center">
         <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
         Add a New Prospect</button>
         <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
         Cancel</a>
      </p>
   </form>
</div>