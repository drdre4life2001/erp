        <form action="{{ url('/lead'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="person_name" class="col-sm-3 control-label">Id</label>
                        <div class="col-sm-6">
                            <input type="text" required="" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="gender" class="col-sm-3 control-label">Status</label>
                        <div class="col-sm-6">
                            {{--<input type="text" name="gender" id="gender" class="form-control" value="{{$model['gender'] or ''}}">--}}
                            <select required="" name="status" class="form-control" id="status">
                                <option value="{{$model['status'] or ''}}"> {{$model['status'] or 'Choose'}} </option>
                                <option value="Lead" @if($model['status'] == "Lead") selected  @endif>Lead</option>
                                <option value="Open" @if($model['status'] == "Open") selected  @endif>Open</option>
                                <option value="Replied" @if($model['status'] == "Replied") selected  @endif>Replied</option>
                                <option value="Oppurtunity" @if($model['status'] == "Oppurtunity") selected  @endif>Oppurtunity</option>
                                <option value="Quotation" @if($model['status'] == "Quotation") selected  @endif>Quotation</option>
                                <option value="Lost QUotation" @if($model['status'] == "Lost QUotation") selected  @endif>Lost Quotation</option>
                                <option value="Interested" @if($model['status'] == "Interested") selected  @endif>Interested</option>
                                <option value="Converted" @if($model['status'] == "Converted") selected  @endif>Converted</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>               
            
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="person_name" class="col-sm-3 control-label">Person Name</label>
                        <div class="col-sm-6">
                            <input type="text" required="" name="person_name" id="person_name" class="form-control" value="{{$model['person_name'] or ''}}">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="gender" class="col-sm-3 control-label">Gender</label>
                        <div class="col-sm-6">
                            {{--<input type="text" name="gender" id="gender" class="form-control" value="{{$model['gender'] or ''}}">--}}
                            <select name="gender" required="" class="form-control" id="gender">
                                <option value="{{$model['gender'] or ''}}"> {{$model['gender'] or 'Choose'}} </option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>                                                                                    
            
            
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="organization_name" class="col-sm-3 control-label">Organization Name</label>
                        <div class="col-sm-6">
                            <input type="text" required="" name="organization_name" id="organization_name" class="form-control" value="{{$model['organization_name'] or ''}}">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="source" class="col-sm-3 control-label">Source</label>
                        <div class="col-sm-6">
                            <input type="text" required="" name="source" id="source" class="form-control" value="{{$model['source'] or ''}}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="email_address" class="col-sm-3 control-label">Email Address</label>
                        <div class="col-sm-6">
                            <input type="text" required="" name="email_address" id="email_address" class="form-control" value="{{$model['email_address'] or ''}}">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="lead_owner" class="col-sm-3 control-label">Lead Owner</label>
                        <div class="col-sm-6">
                            {{--<input type="text" name="lead_owner" id="lead_owner" class="form-control" value="{{$model['lead_owner'] or ''}}">--}}
                            <select required="" name="lead_owner" class="form-control" id="lead_owner">
                                <option value="">--Select Owner--</option>
                                @foreach($lead_owners as $employee)
                                    <?php $db_sel_value = $employee->id; ?>
                                    <option @if($employee->id == $db_sel_value) selected @endif value="{{ $employee->id }}">{{ $employee->email }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="next_contact_date" class="col-sm-3 control-label">Next Contact Date {{ "Previous Date: ".$model['next_contact_date'] or ''}}</label>
                        <div class="col-sm-6">
                            <input required="" type="date" name="next_contact_date" id="next_contact_date" class="form-control" value="{{$model['next_contact_date'] or ''}}">
                        </div>
                    </div>mustaein olajobi aremu welcome
                    
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="next_contact_by" class="col-sm-3 control-label">Next Contact By</label>
                        <div class="col-sm-6">
                            <input type="text" required="" name="next_contact_by" id="next_contact_by" class="form-control" value="{{$model['next_contact_by'] or ''}}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="phone" class="col-sm-3 control-label">Phone</label>
                        <div class="col-sm-6">
                            <input type="text" required="" name="phone" id="phone" class="form-control" value="{{$model['phone'] or ''}}">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="salutation" class="col-sm-3 control-label">Salutation</label>
                        <div class="col-sm-6">
                            <input type="text" required="" name="salutation" id="salutation" class="form-control" value="{{$model['salutation'] or ''}}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="mobile_no" class="col-sm-3 control-label">Mobile No</label>
                        <div class="col-sm-6">
                            <input type="text" required="" name="mobile_no" id="mobile_no" class="form-control" value="{{$model['mobile_no'] or ''}}">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="fax" class="col-sm-3 control-label">Fax</label>
                        <div class="col-sm-6">
                            <input type="text" required="" name="fax" id="fax" class="form-control" value="{{$model['fax'] or ''}}">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="website" class="col-sm-3 control-label">Website</label>
                        <div class="col-sm-6">
                            <input type="text" required="" name="website" id="website" class="form-control" value="{{$model['website'] or ''}}">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="territory" class="col-sm-3 control-label">Territory</label>
                        <div class="col-sm-6">
                            <input type="text" required="" name="territory" id="territory" class="form-control" value="{{$model['territory'] or ''}}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                     <div class="form-group">
                         <label for="lead_type" class="col-sm-3 control-label">Lead Type</label>
                         <div class="col-sm-6">
                             <select name="lead_type" required="" class="form-control" id="lead_type">
                                 <option value="{{$model['lead_type'] or ''}}"> {{$model['lead_type'] or 'Choose'}} </option>
                                 <option value="Client" @if($model['lead_type'] == "Client") selected @endif>Clinet</option>
                                 <option value="Channel Partner" @if($model['lead_type'] == "CHannel Partner") selected @endif>Channel Partner</option>
                                 <option value="Consultant" @if($model['lead_type'] == "Consultant") selected @endif>Consultant</option>

                             </select>
                             {{--<input type="text" name="lead_type" id="lead_type" class="form-control" value="{{$model['lead_type'] or ''}}">--}}
                         </div>
                     </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="market_segment" class="col-sm-3 control-label">Market Segment</label>
                        <div class="col-sm-6">
                            {{--<input type="text" name="market_segment" id="market_segment" class="form-control" value="{{$model['market_segment'] or ''}}">--}}
                            <select required="" name="market_segment" class="form-control" id="market_segment">
                                <option value="{{$model['market_segment'] or ''}}"> {{$model['market_segment'] or 'Choose'}} </option>
                                <option value="Lower Income" @if($model['market_segment'] == "Lower Income") selected @endif>Lower Income</option>
                                <option value="Middle Income" @if($model['market_segment'] == "Middle Income") selected @endif>Middle Income</option>
                                <option value="Upper Income" @if($model['market_segment'] == "Upper Income") selected @endif>Upper Income</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="industry" class="col-sm-3 control-label">Industry</label>
                        <div class="col-sm-6">
                            {{--<input type="text" name="industry" id="industry" class="form-control" value="{{$model['industry'] or ''}}">--}}
                            <select required="" name="industry" class="form-control" id="industry">
                                <option value="{{$model['industry'] or ''}}"> {{$model['industry'] or 'Choose'}} </option>
                                <option value="Venture Capital" @if($model['industry'] == "Venture Capital") selected @endif>Venture Capital</option>
                                <option value="Telecommunications" @if($model['industry'] == "Telecommunications") selected @endif > Telecommunications</option>
                                <option value="Technology"  @if($model['industry'] == "Technology") selected @endif>Technology</option>
                                <option value="Transportation"  @if($model['industry'] == "Transportation") selected @endif>Transportation</option>
                                <option value="Retail and Wholesale"  @if($model['industry'] == "Retail and Wholesale") selected @endif>Retail and Wholesale</option>
                                <option value="Service" @if($model['industry'] == "Service") selected @endif>Service</option>
                                <option value="News Papers and Publishers" @if($model['industry'] == "News Papers and Publishers") selected @endif>News Papers and Publishers</option>
                                <option value="Pharmaceuticals" @if($model['industry'] == "Pharmaceuticals") selected @endif>Pharmaceuticals</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="request_type" class="col-sm-3 control-label">Request Type</label>
                        <div class="col-sm-6">
                            {{--<input type="text" name="request_type" id="request_type" class="form-control" value="{{$model['request_type'] or ''}}">--}}
                            <select required="" name="request_type" class="form-control" id="request_type">
                                <option value="{{$model['request_type'] or ''}}"> {{$model['request_type'] or 'Choose'}} </option>
                                <option value="Venture Capital" @if($model['request_type'] == "Venture Capital") selected @endif>Product Inquiry</option>
                                <option value="Request for information"  @if($model['request_type'] == "Request for information") selected @endif> Request for information</option>
                                <option value="Suggestions" @if($model['request_type'] == "Suggestions") selected @endif>Suggestions</option>
                                {{--<option value="Pharmaceuticals">Pharmaceuticals</option>--}}
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-lg-offset-4">
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                            <button type="submit" class="btn btn-purple">
                                <i class="fa fa-plus"></i> Save
                            </button> 
                        </div>
                    </div>                    
                </div>
            </div>
        </form>        