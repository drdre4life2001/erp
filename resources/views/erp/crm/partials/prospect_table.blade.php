<table id="example1" class="table table-bordered table-hover">
   <thead>
      <tr style="color:#8b8b8b">
         <th>S/N</th>
         <th>Date Created</th>
         <th>Company</th>
         <th>Name Executive</th>
         <th>Position of Executive</th>
         <th>Email</th>
         <th>Phone</th>
         <th>Staff Assigned To</th>
         <th>Company Assigned To</th>
         <th>Lead Status</th>
         <th>Actions</th>
      </tr>
   </thead>
   <tbody>
      @if(!empty($item))
         <?php
             $i =  1;
             //use Illuminate\Support\Facades\Auth;
             //use Illuminate\Support\Facades\DB;
             $user_id = Illuminate\Support\Facades\Auth::user()->id_hr_employee;
             $grades = Illuminate\Support\Facades\DB::select("select * from hr_employee where id = '$user_id'");
             //var_dump($grades);exit;
             foreach ($grades as $grade){
                 $grade = $grade->id_hr_grade;
             }

         ?>
         @foreach($item as $items)
            <?php $tdate = date('d-m-Y', strtotime($items->created_at)); ?>
            <tr @if($items->status == 'Troubled') style="background-color: orangered; color: white" @elseif($items->status == 8) style="background-color: green" @else style="background-color:#white; color:#222;" @endif>
             <!--Loop through all items for this procurement -->
             <td>{{ $i++ }}</td>
             <td>{{ $tdate}}</td>
             <td>{{ $items->prospect }}</td>
             <td>{{ $items->contact_person }}</td>
             <td>{{ $items->contact_position }}</td>
             <td>{{ $items->contact_email }}</td>
             <td>{{ $items->contact_phone }}</td>
             <td>{{ $items->lastname }} {{ $items->firstname }}</td>
             <td>{{ $items->subsidiary }}</td>
             <td>{{ $items->status }}</td>
             <td>
                 <div class='btn-group'>
                     <!--Edit item -->
                     <a href="{{url('crm/prospects/activity/'.$items->id)}}" class="btn btn-sm btn-success" style="margin-right: 0px;"
                       >
                         <i title="View activities of this prospect!"> </i>View </a> <br> <br>
                     @if($grade != 4)
                         @if($items->status_id != 9)
                             <a href="" style="" class="btn btn-purple btn-sm" data-toggle='modal' data-target='#{{$items->id}}'>
                             Add Activity </a> <br> <br>
                             <a href="" class="btn btn-sm btn-purple" style="" data-toggle='modal' data-target='#{{$items->id}}_'>
                                 Update Status</a>
                         @endif
                     @endif

                 </div>


             </td>
          </tr>

          {{-- Modal for adding new activity on each prospect --}}
          <div id="{{$items->id}}" class="modal fade" role="dialog">
              <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h5 class="modal-title">Add Activity</h5>
                      </div>
                      <form action="{{url('crm/prospects/activity/add')}}" method="post">
                          <div class="modal-body">
                              <input type="hidden" value="{{$items->id}}" name="prospect_id">
                              <div class="form-group">
                                  <label class="control-label mb-10" for="exampleInputuname_1">Title</label>
                                  <div class="input-group">
                                      <div class="input-group-addon"><i class="fa fa-building"></i></div>
                                      <input type="text" class="form-control" id="exampleInputuname_1"
                                             placeholder="E.g. Meeting with the Chairman" name="title" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="control-label mb-10" for="exampleInputuname_1">Description</label>
                                  <div class="input-group">
                                      <div class="input-group-addon"><i class="fa fa-pencil-square-o"></i></div>
                                      <textarea class="form-control" name="description" required></textarea>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="control-label mb-10" for="exampleInputuname_1">Date</label>
                                  <!--
                                  <div class="input-group">
                                      <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                      <input type="text" class="form-control" id="activity" name="date" required>
                                  </div>
                                  -->
                                  <input type="date" class="form-control" name="activity_date" required>
                              </div>
                              <div class="form-group">
                                  <label class="control-label mb-10" for="exampleInputuname_1">Status</label>
                                  <div class="input-group">
                                      <div class="input-group-addon"><i class="fa fa-battery-empty"></i></div>
                                      <select class="form-control" name="status" required>
                                          <option value="">--Please select--</option>
                                          @foreach($pipeline_status as $pipeline)
                                              <option value="{{$pipeline->id}}">{{$pipeline->definition}}</option>
                                          @endforeach
                                      </select>
                                  </div>
                              </div>
                              {{--
                              <div class="form-group col-md-6">
                                  <label class="">Name</label>
                                  <input name="name" id="name" type="text" class="form-control" value="{{$items->name}}">
                              </div>
                              --}}
                          </div>
                          <div class="modal-footer">
                              <input type="submit" value="Add Activity" name="submit" class="btn btn-purple">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
          <!--Modal ends -->

          {{-- MODAL FOR UPDATING LEAD STATUS --}}
          <div id="{{$items->id}}_" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Update Status</h4>
                                        </div>
                                        <form action="{{url('crm/prospects/update')}}" method="post">
                                            <div class="modal-body">
                                                <input type="hidden" name="item_id" value="{{$items->id}}">
                                                <div class="form-group col-sm-6">
                                                    <label class="">Please Select</label>
                                                    <select name="status" class="form-control" required>
                                                        @foreach($status as $stat)
                                                            <option
                                                                    <?php
                                                                    if($stat->id == $items->status_id){
                                                                        echo 'selected';
                                                                    }
                                                                    ?>
                                                                    value="{{$stat->id}}">
                                                                {{$stat->definition}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" value="update" name="submit" class="btn btn-danger">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
          @section('script')
              <script>
                  $(function() {
                      $("#activity").datepicker();
                  });
              </script>
          @endsection
         @endforeach
      @endif
      
      </tfoot>
</table>