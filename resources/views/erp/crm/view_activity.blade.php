@extends('erp.layouts.master')
  
  @section('title')
    CRM - Prospects|View Activity Log
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  	<section class="content-header">
  	   <h1>  Activity Log     </h1>
  	   <ol class="breadcrumb">
  	      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  	      <li class="active">Activity Log
  	      </li>
  	   </ol>
  	</section>
  	<section class="content">
  	   <div class="col-md-12">
	  	   <div class="nav-tabs-custom">
	  	      <ul class="nav nav-tabs">
	  	         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>Activity Log
	  	            </b></a>
	  	         </li>
	  	      </ul>
	  	      <div class="tab-content" style="padding: 2%">
	  	         <div class="tab-pane active" id="tab_1">
	  	            <p align="right">
	  	               <a href="{{url('/crm/prospects/create')}}" class="btn btn-large btn-default cdb" ><i class="fa fa-eye" aria-hidden="true" style="color:white"></i>  <<< Go Back
	  	               </a>
	  	            </p>
	  	            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
	  	               <div class="col-md-1 hidden-sm hidden-xs"></div>
	  	               <div class="col-md-1 hidden-sm hidden-xs"></div>
	  	            </div>

	  	            <hr/ style="clear: both">
	  	            <div class="box-body">
                     @if(isset($pipelines))
                        <?php $i = 1; ?>
                        <div class="row">
                          @foreach($pipelines as $pipeline)
                            <?php $date = date('d-m-Y', strtotime($pipeline->activity_date)); ?>
                            <div class="col-md-4 col-md-offset-1 well">

                                <span class="badge">{{ $i++ }}</span><br>
                                Title: <span style="color:#222;"></span>{{$pipeline->title}} <br><br>
                                Description: {{$pipeline->description}} <br><br>
                                Created: {{$date}} <br>
                                Status: {{$pipeline->status}}<br>
                                @if($pipeline->pipeline_status != 3)
                                    <a href="" style=""
                                       class='btn btn-purple btn-xs' data-toggle='modal' data-target='#{{$pipeline->id}}'>
                                        <i class="glyphicon glyphicon-upload"> </i> Update Status</a>
                                @endif

                            <!-- Update Status Modal -->
                                <div id="{{$pipeline->id}}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Update Status</h4>
                                            </div>
                                            <form action="{{url('crm/pipeline/update')}}" method="post">
                                                <div class="modal-body">
                                                    <input type="hidden" name="prospect_id" value="{{$pipeline->prospect_id}}">
                                                    <input type="hidden" name="pipeline_id" value="{{$pipeline->id}}">
                                                    <div class="form-group col-sm-6">
                                                        {{--<label class="">Please Select</label> --}}
                                                        <select name="status" class="form-control" required>
                                                            @foreach($pipeline_status as $stat)
                                                                <option
                                                                    <?php
                                                                    if ($stat->id == $pipeline->status) {
                                                                        echo 'selected';
                                                                    }
                                                                    ?>
                                                                    value="{{$stat->id}}">
                                                                    {{$stat->definition}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <input type="submit" value="update" name="submit" class="btn btn-danger">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            {{-- data chuncking bootstrap --}}
                        @endforeach
                        </div>
                        
                      @else
                      <div class="row">
                        <div class="col-lg-6 col-lg-offset-2">
                          <p>No Activity found for this Propect!</p>
                        </div>
                      </div>
                     @endif
	  	            </div>
	  	         </div>
	  	      </div>
	  	   </div>
  	   </div>
  	</section>
  @endsection


  @section('script')
  	<script type="text/javascript">
  	  $('.adf').hide();
  	  $('.cdb').on('click', function(){
  	    $('.adf').slideToggle();
  	 
  	  });

  	    $('.cadfxx').on('click', function(){
  	       $('.adf').slideToggle();
  	    });

  	    @if ($errors->has('name') || $errors->has('email') || $errors->has('contact_person') || $errors->has('contact_position') || $errors->has('revenue') || $errors->has('subsidiary') || $errors->has('code') || $errors->has('phone') || $errors->has('address'))
  	      $('.adf').slideToggle();
  	    @endif

  	        //Date picker
  	    $('#datepicker').datepicker({
  	      autoclose: true
  	    });

  	        //Date picker
  	    $('#datepicker2').datepicker({
  	      autoclose: true
  	    });

  	     $('#reservation').daterangepicker();
  	    //Date range picker with time picker
  	    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
  	    //Date range as a button

  	$('#daterange-btn').daterangepicker(
  	        {
  	          ranges: {
  	            'Today': [moment(), moment()],
  	            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
  	            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
  	            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
  	            'This Month': [moment().startOf('month'), moment().endOf('month')],
  	            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  	          },
  	          startDate: moment().subtract(29, 'days'),
  	          endDate: moment()
  	        },
  	        function (start, end) {
  	          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  	        }
  	    );

  	</script>

  @endsection