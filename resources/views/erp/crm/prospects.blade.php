@extends('erp.layouts.master')
  
  @section('title')
    CRM - Prospects
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  	<section class="content-header">
  	   <h1>  All Prospects     </h1>
  	   <ol class="breadcrumb">
  	      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  	      <li class="active">All Prospects
  	      </li>
  	   </ol>
  	</section>
  	<section class="content">
  	   <div class="col-md-12">
	  	   <div class="nav-tabs-custom">
	  	      <ul class="nav nav-tabs">
	  	         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>All Prospects
	  	            </b></a>
	  	         </li>
	  	      </ul>
	  	      <div class="tab-content" style="padding: 2%">
	  	         <div class="tab-pane active" id="tab_1">
	  	            <p align="right">
	  	               <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add A New Prospects
	  	               </button>
	  	            </p>
	  	            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
	  	               <div class="col-md-1 hidden-sm hidden-xs"></div>
	  	               @include('erp.crm.partials.prospect_form')
	  	               <div class="col-md-1 hidden-sm hidden-xs"></div>
	  	            </div>
	  	            <hr/ style="clear: both">
	  	            <div class="box-body">
	  	               @include('erp.crm.partials.prospect_table')
	  	            </div>
	  	         </div>
	  	      </div>
	  	   </div>
  	   </div>
  	</section>
  @endsection


  @section('script')
  	<script type="text/javascript">
  	  $('.adf').hide();
  	  $('.cdb').on('click', function(){
  	    $('.adf').slideToggle();
  	 
  	  });

  	    $('.cadfxx').on('click', function(){
  	       $('.adf').slideToggle();
  	    });

  	    @if ($errors->has('name') || $errors->has('email') || $errors->has('contact_person') || $errors->has('contact_position') || $errors->has('revenue') || $errors->has('subsidiary') || $errors->has('code') || $errors->has('phone') || $errors->has('address'))
  	      $('.adf').slideToggle();
  	    @endif

  	        //Date picker
  	    $('#datepicker').datepicker({
  	      autoclose: true
  	    });

  	        //Date picker
  	    $('#datepicker2').datepicker({
  	      autoclose: true
  	    });

  	     $('#reservation').daterangepicker();
  	    //Date range picker with time picker
  	    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
  	    //Date range as a button

  	$('#daterange-btn').daterangepicker(
  	        {
  	          ranges: {
  	            'Today': [moment(), moment()],
  	            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
  	            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
  	            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
  	            'This Month': [moment().startOf('month'), moment().endOf('month')],
  	            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  	          },
  	          startDate: moment().subtract(29, 'days'),
  	          endDate: moment()
  	        },
  	        function (start, end) {
  	          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  	        }
  	    );

  	</script>

  @endsection