@extends('erp.layouts.master')
@section('title')
   Dashboard
@endsection
@section('sidebar')
    @include('erp.partials.sidebar')
@endsection
@section('content')
<section class="content">
   <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
            <?php
               $financeControl = \App\Libraries\Utilities::dashboardControl(3);

               $hrControl = \App\Libraries\Utilities::dashboardControl(2);
               $procureControl = \App\Libraries\Utilities::dashboardControl(6);

               $crmControl = \App\Libraries\Utilities::dashboardControl(10);

               $employeeControl = \App\Libraries\Utilities::dashboardControl(1); //when rep employee resource accessible id from theresources table based on the authenticated user if he/she has access to the resource he is trying to access
               ?>
            @if($hrControl == true)
            <li @if($financeControl == false && $hrControl == true && $procureControl == false && $crmControl == false && $employeeControl == false) class="active" @endif><a href="#tab_1" data-toggle="tab"><i class="fa fa-user"></i> <b>Human Resource</b></a></li>
            @endif
            @if($financeControl == true)
            <li @if($financeControl == true && $hrControl == false && $procureControl == false && $crmControl == false && $employeeControl == false) class="active" @endif><a href="#tab_2"  data-toggle="tab"><i class="fa fa-university"></i> <b>Financial Report</b></a></li>
            @endif
            @if($procureControl == true && auth()->user()->roles->id != "35") 
            <li @if($financeControl == false && $hrControl == false && $procureControl == true && $crmControl == false && $employeeControl == false) class="active" @endif><a href="#tab_3" data-toggle="tab"><i class="fa fa-shopping-basket"></i> <b>Procurement</b></a></li>
            @endif
            @if($crmControl == true)
            <li @if($financeControl == true && $hrControl == false && $procureControl == false && $crmControl == false && $employeeControl == false) class="active" @endif><a href="#tab_4" data-toggle="tab"><i class="ion-person-stalker"></i> <b>CRM</b> </a></li>
            @endif

            {{-- display for logged in employee only --}}
        @if(strtolower(auth()->user()->roles->name) == "employee")
            @if($employeeControl == true)
            <li @if($financeControl == false && $hrControl == false && $procureControl == false && $crmControl == false && $employeeControl == true) class="active" @endif><a href="#tab_5" data-toggle="tab"><i class="ion-person-stalker"></i> <b>Employee</b> </a></li>
            @endif
        @endif

        @if(strtolower(auth()->user()->roles->name) == "procurement")
            @if($procureControl == true)
            <li @if($financeControl == false && $hrControl == false && $procureControl == true && $crmControl == false && $employeeControl == false) class="active" @endif><a href="#tab_3" data-toggle="tab"><i class="ion-person-stalker"></i> <b>Employee</b> </a></li>
            @endif
        @endif
         </ul>


        @if(strtolower(auth()->user()->roles->name) == "admin" || auth()->user()->roles->id == "35")
            @include('erp.dashboard.admin')
         @else
            <div class="tab-content" style="padding: 2%">

                     @if($hrControl == true)
                        @if($hrControl == true || $procureControl || false && $crmControl || false && $employeeControl || false)
                           @include('erp.dashboard.hr')
                        </div>
                        @endif
                     @endif

                     @if($financeControl == true)
                           @if($financeControl == true || $hrControl == false || $procureControl == false || $crmControl == false || $employeeControl == false)
                              @include('erp.dashboard.finance')
                           </div>
                         @endif
                     @endif

                     @if($procureControl == true)
                        @if($financeControl == false && $hrControl == false && $procureControl == true && $crmControl == false && $employeeControl == false)
                              @include('erp.dashboard.procurement')
                        </div>
                        @endif
                     @endif

                     @if($crmControl == true)
                        @if($financeControl == false && $hrControl == false && $procureControl == false && $crmControl == true && $employeeControl == false)
                              @include('erp.dashboard.crm')
                        </div>
                        @endif
                     @endif
            </div>
         @endif

         @if(strtolower(auth()->user()->roles->name) == "procurement")
            @include('erp.dashboard.procurement')
         @else
            <div class="tab-content" style="padding: 2%">
                     @if($hrControl == true)
                        @if($hrControl == true || $procureControl || false && $crmControl || false && $employeeControl || false)
                           @include('erp.dashboard.hr')
                        </div>
                        @endif
                     @endif

                     @if($financeControl == true)
                           @if($financeControl == true || $hrControl == false || $procureControl == false || $crmControl == false || $employeeControl == false)
                              @include('erp.dashboard.finance')
                           </div>
                         @endif
                     @endif

                     @if($procureControl == true)
                        @if($financeControl == false && $hrControl == false && $procureControl == true && $crmControl == false && $employeeControl == false)
                              @include('erp.dashboard.procurement')
                        </div>
                        @endif
                     @endif

                     @if($crmControl == true)
                        @if($financeControl == false && $hrControl == false && $procureControl == false && $crmControl == true && $employeeControl == false)
                              @include('erp.dashboard.crm')
                        </div>
                        @endif
                     @endif
            </div>
         @endif
         <!-- /.tab-content -->
      </div>
      <!-- nav-tabs-custom -->
   </div>
</section>
@endsection
@section('script')
   <script>
      $('ul li a').click(function(){
          $(this).addClass("active");
      });
   </script>
@endsection
