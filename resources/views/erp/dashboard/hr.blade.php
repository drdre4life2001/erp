<div class="tab-pane active " id="tab_1">
   <div class="row">
      @if(!empty($leaves))
         <div class="col-md-7">
            <div>
               <div class="box-footer">
                  <div class="row" style="padding: 2%">
                  <ul class="products-list product-list-in-box">

                        @foreach($leaves as $leave)
                           <li class="item">
                              <div class="product-img">
                                 <img src="{{ !empty($leave->picture) ? asset($leave->picture) : asset('profile.png') }}" alt="Product Image" style="border-radius:0%; height: 120px; width: 120px; margin-right: 10%;">
                              </div>
                              <div class="product-info pis">
                           <span class="product-title">{{$leave->first_name." ".$leave->lastname}}
                           </span>
                                 <div class="product-description">
                                   Leave Type: {{$leave->type}}  <br/>
                                 </div>
                                 <div class="product-description">
                                    Description: {{$leave->description}}  <br />
                                 </div>
                                 <a href="{{url('/employee_leaves')}}">
                                    <span class="label " style="padding:1.2%; margin-top: 4% !important; float: left; border:2px solid #8ea2b5; color:#fff; background:#8ea2b5"><i class="fa fa-eye"></i>  View </span></a>
                              </div>
                           </li>
                        @endforeach
                           {{--<li class="item">--}}
                              {{--<button class="btn btn-default" style="width: 100%"><i class="fa fa-"></i>View More</button>--}}
                           {{--</li>--}}
                  </ul>
               </div>
            <!-- /.row -->
            </div>
            <hr/>
         </div>
      </div>
      @endif
      <div @if(!empty($leaves)) class="col-md-5" @else class="col-md-8"  @endif>
         <div class="col-md-6">
            <!-- sm box -->
            <div class="small-box bg-white" style="border-radius:2px;">
               <div class="inner">
                  <h3>{{isset($employees[0]) ? $employees[0]->count : 0}}</h3>
                  <p>TOTAL EMPLOYEE</p>
               </div>
               <div class="icon">
               </div>
            <a href="{{url('/employee')}}" target="_blank" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <!-- ./col -->
         <div class="col-md-6">
            <!-- small box -->
            <div class="small-box bg-white" style="border-radius:2px; ">
               <div class="inner">
                  <h3>{{ isset($users[0]) ? $users[0]->count : 0 }}</h3>
                  <p>TOTAL USERS</p>
               </div>
               <div class="icon">
               </div>
               <a href="{{url('/erp-users/index')}}" target="_blank" class="small-box-footer"> <i class="fa fa-user"></i></a>
            </div>
         </div>
         <!-- ./col -->
         <div class="col-md-6">
            <!-- small box -->
            <div class="small-box bg-white" style="border-radius: 2px">
               <div class="inner">
                  <h3>&#8358; {{ $loan }}</h3>
                  <p>TOTAL LOAN </p>
               </div>
               <div class="icon">
               </div>
               <a href="{{url('/finance/company-loan/index')}}" target="_blank" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <div class="col-md-6">
            <!-- small box -->
            <div class="small-box bg-white" style="border-radius:2px; ">
               <div class="inner">
                  <h3>{{ isset($leave_requests[0]) ? $leave_requests[0]->count : 0 }}</h3>
                  <p>LEAVE REQUESTS</p>
               </div>
               <div class="icon">
               </div>
               <a href="{{url('/employee_leaves')}}" target="_blank" class="small-box-footer"> <i class="fa fa-question"></i></a>
            </div>
         </div>
         <!-- ./col -->
         <div class="col-md-12">
            <!-- small box -->
            <div class="small-box bg-white" style="border-radius: 2px; ">
               <div class="inner">
                  <h3>&#8358;{{ $total_salary }}</h3>
                  <p>TOTAL SALARY
                  </p>
               </div>
               <div class="icon">
                  <i class="ion-person-stalker red"></i>
               </div>
            </div>
         </div>
         <!-- ./col -->
      </div>
      <div class="box-body">
         <div class="row">
            @if(!is_null($all_data))
               <div class="col-md-7">
                  <div class="">
                     <!-- Sales Chart Canvas -->
                     {{--<canvas id="salesChart" style="height: 180px;"></canvas>--}}
                        <div class="col-md-10">
                           <div id="depart"></div>
                        </div>
                  </div>
               </div>
            @endif

            <div @if(!empty($leaves)) class="col-md-5" @endif >
               <div class="progress-group">
                  <span class="progress-text">Total Revenue</span>
                  <div class="progress sm">
                     <div class="progress-bar progress-bar-green" style="width: {{ number_format($total_revenue[0]->total/100 )}}%"></div>
                  </div>
               </div>
               <!-- /.progress-group -->
               <div class="progress-group">
                  <span class="progress-text">Total Payable</span>
                  <div class="progress sm">
                     <div class="progress-bar progress-bar-red" style="width: {{ $total_payable/1000 }}%"></div>
                  </div>
               </div>
               <!-- /.progress-group -->
               <div class="progress-group">
                  <span class="progress-text">Total Expenses</span>
                  <div class="progress sm">
                     <div class="progress-bar progress-bar-yellow" style="width: {{  $total_expense/1000 }}%"></div>
                  </div>
               </div>
               <!-- /.progress-group -->
            </div>
         </div>
         <!-- /.row -->
      </div>
   </div>

   @section('script')
      @include('erp.dashboard.admin_partials.hr_chart')
   @endsection

