{{-- PROCUREMENT --}}

<div class="tab-pane" id="tab_3">
   <div class="row">
      <div class="col-md-12">
         <!-- /.box-header -->
         <div class="box-body">
            <div class="row">
               <div class="col-md-12">
                  <div class="col-md-8">
                     <div class="box">
                        <div class="box">
                           <div class="box-header with-border">
                              <h3 class="box-title">Procurement Activity table</h3>
                           </div>
                           <!-- /.box-header -->
                           <div class="box-body">
                              @include('erp.expenses.partials.expense_table')
                           </div>
                           <!-- /.box-body -->
                           <div class="box-footer clearfix">
                              <ul class="pagination pagination-sm no-margin pull-right">
                                 <li><a href="#">&laquo;</a></li>
                                 <li><a href="#">1</a></li>
                                 <li><a href="#">2</a></li>
                                 <li><a href="#">3</a></li>
                                 <li><a href="#">&raquo;</a></li>
                              </ul>
                           </div>
                        </div>
                        <!-- /.box-body -->
                     </div>
                     <!-- /.box -->
                  </div>
                  <div class="col-md-4">
                     <div class="col-lg-12 col-xs-12">
                        <!-- small box -->
                        <div class="small-box bg-white" style="border-radius:2px; ">
                           <div class="inner">
                              <h3>&#8358;50,000</h3>
                              <p>TOTAL REQUESTS</p>
                           </div>
                           <div class="icon">
                              <i class="ion ion-stats-bars green"></i>
                           </div>
                           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                     </div>
                     <!-- ./col -->
                     <div class="col-lg-12 col-xs-12">
                        <!-- small box -->
                        <div class="small-box bg-white" style="border-radius: 2px;   ">
                           <div class="inner">
                              <h3>&#8358;340,500</h3>
                              <p>TOTAL APPROVED REQUEST</p>
                           </div>
                           <div class="icon">
                              <i class="ion ion-arrow-graph-up-right yellow"></i>
                           </div>
                           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                     </div>
                     <div class="box-header with-border">
                        <h3 class="box-title">Vertical Progress bars</h3>
                     </div>
                     <div class="box-body text-center" style="margin-top:4%">
                        <div class="progress vertical">
                           <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="height: 80%">
                              <span class="sr-only">80%</span>
                           </div>
                        </div>
                        <div class="progress vertical">
                           <div class="progress-bar progress-bar-aqua" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="height: 20%">
                              <span class="sr-only">20%</span>
                           </div>
                        </div>
                        <div class="progress vertical">
                           <div class="progress-bar progress-bar-yellow" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="height: 65%">
                              <span class="sr-only">65%</span>
                           </div>
                        </div>
                        <div class="progress vertical">
                           <div class="progress-bar progress-bar-red" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="height: 60%">
                              <span class="sr-only">60%</span>
                           </div>
                        </div>
                     </div>
                     <!-- /.chart-responsive -->
                  </div>
                  <!-- /.col -->
               </div>
               <!-- /.col -->
            </div>
            <!-- /.row -->
         </div>
         <!-- ./box-body -->
         <!-- /.box -->
      </div>
      <!-- ./col -->
   </div>
</div>