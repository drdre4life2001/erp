<script type="text/javascript">
    <?php
    echo "var jdata =  $all_data;";
    ?>
    
   Highcharts.chart('depart', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'A chart showing number of employees by department'
    },
    subtitle: {
        text: 'Click the columns to view versions.'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Interval'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },
    credits: {
        enabled: false
      },
    "series": [
        {
            "name": "Depertments",
            "data": jdata,
            color: '#BC96FF'
        }
    ],
});   
</script>