<div class="tab-pane" id="tab_3">
                  <div class="row">
                     <div class="col-md-12">
                        <!-- /.box-header -->
                        <div class="box-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="col-md-12">
                                    <div class="box">
                                       <div class="box">
                                          <div class="box-header with-border">
                                             <h3 class="box-title">Expense Requests</h3>
                                          </div>
                                          <!-- /.box-header -->
                                          <div class="box-body">
                                             @include('erp.expenses.partials.expense_table')
                                          </div>
                                          <!-- /.box-body -->
                                          {{-- <div class="box-footer clearfix">
                                             <ul class="pagination pagination-sm no-margin pull-right">
                                                <li><a href="#">&laquo;</a></li>
                                                <li><a href="#">1</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">&raquo;</a></li>
                                             </ul>
                                          </div> --}}
                                       </div>
                                       <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                 </div>
                                 {{-- <div class="col-md-4">
                                    <div class="col-lg-12 col-xs-12">
                                       <!-- small box -->
                                       <div class="small-box bg-white" style="border-radius:2px; ">
                                          <div class="inner">
                                             <h3>{{count($other_expenses)}}</h3>
                                             <p>TOTAL REQUESTS</p>
                                          </div>
                                          <div class="icon">
                                             <i class="ion ion-stats-bars green"></i>
                                          </div>
                                          <a href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                                       </div>
                                    </div>
                                    <!-- ./col -->
                                    <div class="col-lg-12 col-xs-12">
                                       <!-- small box -->
                                       <div class="small-box bg-white" style="border-radius: 2px;   ">
                                          <div class="inner">
                                             <h3>{{count($approved_expenses)}}</h3>
                                             <p>TOTAL APPROVED REQUEST</p>
                                          </div>
                                          <div class="icon">
                                             <i class="ion ion-arrow-graph-up-right yellow"></i>
                                          </div>
                                          <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-xs-12">
                                       <!-- small box -->
                                       <div class="small-box bg-white" style="border-radius: 2px;   ">
                                          <div class="inner">
                                             <h3>{{count($declined_expenses)}}</h3>
                                             <p>TOTAL DECLINED REQUEST</p>
                                          </div>
                                          <div class="icon">
                                             <i class="ion ion-arrow-graph-up-right yellow"></i>
                                          </div>
                                          <a href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-xs-12">
                                       <!-- small box -->
                                       <div class="small-box bg-white" style="border-radius: 2px;   ">
                                          <div class="inner">
                                             <h3>{{count($pending_expenses)}}</h3>
                                             <p>TOTAL PENDING REQUEST</p>
                                          </div>
                                          <div class="icon">
                                             <i class="ion ion-arrow-graph-up-right yellow"></i>
                                          </div>
                                          <a href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                                       </div>
                                    </div>
                                    <!-- /.chart-responsive -->
                                 </div> --}}
                                 <!-- /.col -->
                              </div>
                              <!-- /.col -->
                           </div>
                           <!-- /.row -->
                        </div>
                        <!-- ./box-body -->
                        <!-- /.box -->
                     </div>
                     <!-- ./col -->
                  </div>