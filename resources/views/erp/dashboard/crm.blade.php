<div class="tab-pane active" id="tab_4">
   <div class="row">
      <div class="col-lg-3 col-xs-6">
         <!-- small box -->
         <div class="small-box bg-white" style="border-radius:2px;">
            <div class="inner">
               <h3>{{ $total_prospects }}</h3>
               <p>Total Prospect</p>
            </div>
            <div class="icon">
               <i class="ion ion-stats-bars green"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6">
         <!-- small box -->
         <div class="small-box bg-white" style="border-radius: 2px;">
            <div class="inner">
               <h3>{{ $total_inline }}</h3>
               <p>Prospect in Pipeline</p>
            </div>
            <div class="icon">
               <i class="ion ion-arrow-graph-up-right yellow"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
      </div>
      <!-- ./col -->
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6">
         <!-- small box -->
         <div class="small-box bg-white" style="border-radius: 2px;">
            <div class="inner">
               <h3>{{ $total_close }}</h3>
               <p>Prospect Closed</p>
            </div>
            <div class="icon">
               <i class="ion-pie-graph red"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
      </div>
      <div class="col-lg-3 col-xs-6">
         <!-- small box -->
         <div class="small-box bg-white" style="border-radius: 2px;">
            <div class="inner">
               <h3>{{ number_format($total_deactivated, 2) }}</h3>
               <p>Prospect Deactivated</p>
            </div>
            <div class="icon">
               <i class="ion-pie-graph red"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <!-- /.box-header -->
         <div class="box-body">
            <div class="row">
               <div class="col-md-12">
                  <div class="col-xs-8">
                     <div class="box">
                        <div class="box-header">
                           <h3 class="box-title">Prospects</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                           @include('erp.crm.partials.prospect_table')
                        </div>
                        <!-- /.box-body -->
                     </div>
                     <!-- /.box -->
                  </div>

                  <!-- /.col -->
               </div>
               <!-- /.col -->
            </div>
            <!-- /.row -->    
         </div>
         <!-- ./box-body -->
         <!-- /.box -->
      </div>
   </div>
</div>