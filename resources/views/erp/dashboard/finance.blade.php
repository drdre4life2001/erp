<div class="tab-pane active" id="tab_2">
<div class="row">
   <div class="col-lg-4 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-white" style="border-radius:2px;">
         <div class="inner">
            <h3>&#8358;{{ number_format($total_revenue[0]->total) }}</h3>
            <p>Total Revenue</p>
         </div>
         <div class="icon">
            <i class="ion ion-stats-bars green"></i>
         </div>
      </div>
   </div>
   <!-- ./col -->
   <div class="col-lg-4 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-white" style="border-radius: 2px;">
         <div class="inner">
            <h3>&#8358;{{ number_format($total_payable, 2) }}</h3>
            <p>Total Payable</p>
         </div>
         <div class="icon">
            {{-- <i class="ion ion-arrow-graph-up-right yellow"></i> --}}
         </div>
      </div>
   </div>
   <!-- ./col -->
   <!-- ./col -->
   <div class="col-lg-4 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-white" style="border-radius: 2px;">
         <div class="inner">
            <h3>&#8358;{{ number_format($total_expense, 2) }}</h3>
            <p>Total Expenses</p>
         </div>
         <div class="icon">
            <i class="ion-pie-graph red"></i>
         </div>
      </div>
   </div>
   <div class="col-md-12">
      <!-- /.box-header -->
      <div class="box-body">
         <div class="row">
            <div class="col-md-12">
               <div class="col-xs-12">
                  <div class="box">
                     <div class="box-header">
                        <h3 class="box-title">Transaction</h3>
                        {{-- <div class="box-tools">
                           <div class="input-group input-group-sm" style="width: 150px;">
                              <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                              <div class="input-group-btn">
                                 <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                              </div>
                           </div>
                        </div> --}}
                     </div>
                     <!-- /.box-header -->
                     <div class="box-body no-padding">
                        @include('erp.finance.ledger_table')
                     </div>
                     <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
               </div>

               <!-- /.col -->
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- ./box-body -->
      <!-- /.box -->
   </div>
   <!-- ./col -->
</div>
