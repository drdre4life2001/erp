@extends('erp.layouts.master')
@section('title')
Roles Management
@endsection
@section('sidebar')
@include('erp.partials.sidebar')
@endsection
@section('content')
<section class="content-header">
   <h1>
      Roles Management
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Roles Management</li>
   </ol>
</section>
<section class="content">
   <div class="col-md-12">
      <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> User Roles </b></a></li>
         </ul>
         <div class="tab-content" style="padding: 2%">
            <div class="tab-pane active" id="tab_1">
               <p align="right">
                  <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add user role</button>
               </p>
               <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
                  <div class="col-md-10">
                     <form method="POST" action="{{ url('roles/store') }}">
                        <h3 align="center">Create User</h3>
                        <div class="col-md-6" style="padding: 0px;">
                           <div class="form-group">
                              <label>Role Name</label>
                              <div class="icon-addon addon-md">
                                 <input type="text" class="form-control" name="name" placeholder="Role Name" required>
                                 <label for="house" class="fa fa-book" rel="tooltip" title="Description"></label>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6" style="padding-right: 0px;">
                           <div class="form-group"><p>Select resource(s) viewable by user. <span style="color:#ea6c41;">you can select many by holding control + shift key the tap on each resource</span></p>
                             <select class="form-control" required name="resource[]" multiple="true" >
                                 @foreach($sql as $sqls)
                                     <option value="{{$sqls->id}}">{{$sqls->name}}</option>
                                 @endforeach
                             </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Description</label>
                           <div class="icon-addon addon-md">
                              <textarea class="form-control" required placeholder="Description" name="description"></textarea>
                              <label for="house" class="fa fa-edit" rel="tooltip" title="house"></label>
                           </div>
                        </div>
                        <p align="center">
                           <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                           Save</button>
                           <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                           Cancel</a>
                        </p>
                     </form>
                  </div>
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
               </div>
               <hr/ style="clear: both">
               <div class="box-body">
                  <table id="example1" class="table table-bordered table-hover">
                     <thead>
                        <tr style="color:#8b8b8b">
                           <th>S/N No</th>
                           <th>Name</th>
                           <th>Slug</th>
                           <th>Description</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @if(!empty($roles))
                           <?php $num = 1; ?>
                           @foreach($roles as $role)
                              <tr>
                                 <td> {{ $num++ }} </td>
                                 <td> {{ $role->name }} </td>
                                 <td> {{ $role->slug }} </td>
                                 <td> {{ $role->description }} </td>
                                 <td>  
                                    <a href="{{ url('roles/edit', $role->id) }}" target="_blank">
                                    <span class="label label-warning" style="padding:3.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                    <i class="fa fa-edit"></i>  Edit </span></a>
                                    
                                    <a id="a_del" href="{{ url('roles/destroy/'. $role->id) }}">
                                    <span class="label label-danger" style="padding:3.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                    <i class="fa fa-trash"></i>  Delete </span></a>
                                 </td>
                              </tr>
                           @endforeach
                        @endif
                      </tbody>
                        </tfoot>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section('script')
<script>
   $("#example1").DataTable();

   $('.adf').hide();
   $('.cdb').on('click', function(){
   $('.adf').slideToggle();
   });
   $('.cadfxx').on('click', function(){
     $('.adf').slideToggle();
   });
</script>
@endsection