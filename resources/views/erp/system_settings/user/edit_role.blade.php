@extends('erp.layouts.master')
@section('title')
Roles Management
@endsection
@section('sidebar')
@include('erp.partials.sidebar')
@endsection
@section('content')
<section class="content-header">
   <h1>
      Roles Management
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Roles Management</li>
   </ol>
</section>
<section class="content">
   <div class="col-md-12">
      <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> User Roles </b></a></li>
         </ul>
         <div class="tab-content" style="padding: 2%">
            <div class="tab-pane active" id="tab_1">
               <p align="right">
                  <button class="btn btn-large btn-default cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white;"></i>  Edit user role and permission </button>
               </p>
               <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
                  <div class="col-md-10">
                     <form method="POST" action="{{ url('roles/edit', $role->id) }}">
                        <h3 align="center">Update User</h3>
                        <div class="col-md-6" style="padding: 0px;">
                           <div class="form-group">
                              <label>Role Name</label>
                              <div class="icon-addon addon-md">
                                 <input type="text" value="{{ $role->name }}" class="form-control" name="name" placeholder="Role Name" required>
                                 <label for="house" class="fa fa-book" rel="tooltip" title="Description"></label>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6" style="padding-right: 0px;">
                           <div class="form-group">
                           <label>Select/Unselect Resources for this role.</label>
                             <select class="form-control" required name="resource[]" multiple="true" ><p>Select resource(s) viewable by user. <span style="color:#ea6c41;">you can select many by holding control + shift key the tap on each resource</span></p>
                                 @foreach($sql as $sqls)
                                     <option value="{{$sqls->id}}" @if(in_array($sqls->id, $db_sel_val)) selected @endif>{{$sqls->name}}</option>
                                 @endforeach
                             </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Description</label>
                           <div class="icon-addon addon-md">
                              <textarea class="form-control" required placeholder="Description" name="description">{{ $role->description }}</textarea>
                              <label for="house" class="fa fa-edit" rel="tooltip" title="house"></label>
                           </div>
                        </div>
                        <p align="center">
                           <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                           Update <i class="fa fa-upload"></i></button>
                        </p>
                     </form>
                  </div>
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
               </div>
               <hr/ style="clear: both">
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section('script')
<script>
   $("#example1").DataTable();

   $('.adf').hide();
   $('.adf').slideToggle();
   $('.cadfxx').on('click', function(){
     $('.adf').slideToggle();
   });
</script>
@endsection