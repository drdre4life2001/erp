@extends('erp.layouts.master')
@section('title')
User Management
@endsection
@section('sidebar')
@include('erp.partials.sidebar')
@endsection
@section('content')
<section class="content-header">
   <h1>
      User Management
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> User Management</li>
   </ol>
</section>
<section class="content">
   <div class="col-md-12">
      <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> User Management </b></a></li>
         </ul>
         <div class="tab-content" style="padding: 2%">
            <div class="tab-pane active" id="tab_1">
               <p align="right">
                  {{-- <button class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Invite user</button> --}}
               </p>
               <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
                  <div class="col-md-10">
                     
                  </div>
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
               </div>
               <hr/ style="clear: both">
               <div class="box-body">
                  <table id="example1" class="table table-bordered table-hover">
                     <thead>
                        <tr style="color:#8b8b8b">
                           <th>S/N No</th>
                           <th>Email</th>
                           <th>Role</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @if(!empty($users))
                           <?php $num = 1; ?>

                           @foreach($users as $user)
                               <tr @if(!is_null($user->deleted_at)) title="User deactivated activate user to update profile" @endif>
                                 <td> {{ $num++ }} </td>
                                 <td> {{ $user->email }} </td>
                                 
                                 <td> @if($user->roles->id == "35") {{"Admin"}} @else
                                        {{ $user->roles->name }}
                                      @endif
                                 </td>
                                 
                                 <td>
                                    <a href="#modal-id{{ $user->id }}" data-toggle="modal" >
                                    <span class="label label-warning" style="padding:3.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                    <i class="fa fa-edit"></i>  Edit </span></a>

                                    @if($user->status == "active")
                                        <a id="a_deactivate" href="{{ url('/erp-users/deactivate/'. auth()->user()->company->id."/".auth()->user()->company->uri."/". $user->id ) }}" onclick="return confirm('Are you sure to trash'+ <?php echo $user->email; ?> +'')" title="Deactivate {{$user->email}}">
                                        <span class="label label-danger" style="padding:3.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                        <i class="fa fa-trash"></i>  Deactivate </span></a>
                                    @elseif($user->status == 'inactive')
                                       <a href="{{ url('/erp-users/activate/'. auth()->user()->company->id."/".auth()->user()->company->uri."/". $user->id ) }}" onclick="return confirm('Are you sure to activate'+ <?php echo $user->email; ?> +'')" title="Activate {{$user->email}}">
                                       <span class="label label-success" style="padding:3.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                       <i class="fa fa-ok"></i>  Activate </span></a>
                                    @endif 
                                    {{-- null --}}
                                 </td>
                              </tr>



                              <div class="modal fade" id="modal-id{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <center>
                                          <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                                      </center>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    @if(Session::has('error'))
                                      {{ Session::get('error') }}
                                    @endif
                                    <form method="POST" action="{{url('/erp-users/update', $user->id)}}">
                                      <div class="modal-body">
                                          <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">User mail:</label>
                                            <input type="text" name="user_email" value="{{ $user->email }}" class="form-control" id="email">
                                          </div>
                                          <div class="form-group">
                                            <select class="form-control" name="role" required>
                                              <option value="">Update user role</option>`
                                                <?php $db_selected_value = $user->role; ?>
                                             @if(!empty($roles))
                                                @foreach($roles as $role)
                                                    <option value="{{ $role->id }}" @if($db_selected_value == $role->id ) selected @endif>{{ $role->name }}</option>
                                                @endforeach
                                             @endif
                                            </select>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <input type="submit" value="Update" class="btn btn-success" />
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                           @endforeach
                        @endif
                        </tfoot>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section('script')
<!-- page script -->
<script>
   $('.adf').hide();
   $('.cdb').on('click', function(){
     $('.adf').slideToggle();

   });

      @if ($errors->has('email') || $errors->has('user_role'))
           $('.adf').slideToggle();
      @endif

     $('.cadfxx').on('click', function(){
        $('.adf').slideToggle();
     });
</script>
@endsection