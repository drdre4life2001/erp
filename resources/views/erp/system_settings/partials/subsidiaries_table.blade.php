<table id="example1" class="table table-bordered table-hover" >
    <thead class="panel panel-heading">
    <tr>
        <th>S/N</th>
        <th>Subsidiary Name</th>
        <th>Sector</th>
        <th>Business Type</th>
        <th>Date Incorporated</th>
        <th>Registration Number</th>
        <th>Certificate Image</th>
        <th>View</th>
        <th>Delete</th>

    </tr>
    </thead>
    <tbody>
    <?php $num = 1; foreach($organizations as $org){ ?>
        <tr>
            <td>{{ $num++ }}</td>
            <td>{{ $org['business_name'] }}</td>
            <td>{{ $org['business_sector'] }}</td>
            <td>{{ $org['business_type'] }}</td>
            <td>{{ date("M d, Y",strtotime($org['date_incorporated'])) 
                }} 
            </td>
            <td>{{ $org['registration_number'] }}</td>
            <td><a href="{{$org['certificate_image']}}" title="Click to download">Download</a></td>
                    <td><a data-toggle="modal" data-target="#modal-id{{$org['uid']}}" @if(empty($org['accounts'])) title="This Organization has no account!" @endif class="btn btn-success"> <i class="fa fa-eye"></i> Accounts </a></td>
            <td><a href="{{url('organization/destroy/'.$org['uid'])}}" id="a_del" class="btn btn-danger table_clicked"> Delete </a></td>
        </tr>
    <?php } ?>
    </tbody>
</table>

@foreach($organizations as $org)
        @include('erp.system_settings.partials.organization_accounts')
@endforeach