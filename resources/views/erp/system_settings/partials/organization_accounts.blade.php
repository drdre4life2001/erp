@if(!empty($org['accounts']))
	
	<div class="modal fade" id="modal-id{{$org['uid']}}">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">{{$org['business_name']}}'s' Accounts</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-hover" id="example1">
						<thead>
							<tr>
								<th>
									<center>
										S/N
									</center>
								</th>
								<th>
									<center>
										Account Name
									</center>
								</th>
								<th>
									<center>
										Account Number
									</center>
								</th>
							</tr>
						</thead>
						<tbody>
							<input type="hidden" value="{{ $inc = 1 }}" name="">  
							@foreach($org['accounts'] as $acc)
								<tr>
									<td>
										<center>
											{{ $inc++ }}
										</center>
									</td>
									<td>
										<center>
											{{ $acc['account_number'] }}
										</center>
									</td>
									<td>
										<center>
											{{ $acc['name'] }}
										</center>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
@endif

