@extends('erp.layouts.master')
@section('title')
Company Subsidiaries
@endsection
@section('sidebar')
@include('erp.partials.sidebar')
@endsection

@section('content')
<section class="content-header">
   <h1>
      Company Subsidiaries
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Company Subsidiaries </li>
   </ol>
</section>
<section class="content">
   <div class="col-md-12">
      <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Company Subsidiaries </b></a></li>
         </ul>
         <div class="tab-content" style="padding: 2%">
            <div class="tab-pane active" id="tab_1">
               <p align="right">
                  <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add Subsidiaries</button>
               </p>
               <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
                  <div class="col-md-10">
                     <form method="POST" action="" enctype="multipart/form-data">
                        <h3 align="center">Company Subsidiaries</h3>
                        <div class="row">
                          <div class="col-md-6" style="padding: 0px;">
                            <div class="form-group">
                              <label>Name</label>
                              <div class="icon-addon addon-md">
                                 <input type="text" required class="form-control" name="name" value="{{old('name')}}" placeholder="Name">
                                 <label for="house" class="fa fa-user" rel="tooltip" title="Description"></label>
                              </div>
                           </div>
                         </div>

                         <div class="col-md-6">
                           <div class="form-group">
                              <label>Registration Number</label>
                              <div class="icon-addon addon-md">
                                  <input type="number" required="" class="form-control" name="registration_number" value="{{old('registration_number')}}" placeholder="Registration Number">
                                  <label for="house" class="fa fa-book" rel="tooltip" title="Description"></label>
                              </div>
                           </div>
                         </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <div class="icon-addon addon-md">
                                        <input type="number" required="" class="form-control" name="phone_number" value="{{old('phone_number')}}" placeholder="Phone Number">
                                        <label for="house" class="fa fa-book" rel="tooltip" title="Description"></label>
                                    </div>
                                </div>
                            </div>

                          <div class="col-md-6">
                            <div class="form-group">
                               <label>Email</label>
                               <div class="icon-addon addon-md">
                                  <input required="" value="{{old('email')}}" type="text" class="form-control" name="email" placeholder="Email Address">
                                  <label for="house" class="fa fa-envelope" rel="tooltip" title="Email"></label>
                               </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                               <label>Date Incorporated:</label>
                               <div class="icon-addon addon-md">
                                  <input name="date_incorporated" required="" type="text" class="form-control fromDate" placeholder="Select a date format DD/MM/YY">
                                  <label for="house" class="fa fa-pencil-square" rel="tooltip" title="Description"></label>
                               </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                               <label>Certificate Image</label>
                               <div class="addon-md">
                                   <input type="file" name="certificate_image" class="btn btn-purple">
                               </div>
                            </div>
                          </div>
                        </div>

                         <div class="row">
                             <div class="col-md-12">
                                 <div class="form-group">
                                     <label>Business Type</label>
                                     <div class="icon-addon addon-md">
                                         <select required="" name="business_type" class="form-control">
                                             <option value="">Select Organization</option>
                                             <option value="Sole Proprietorship">Sole Proprietorship</option>
                                             <option value="Partnership">Partnership</option>
                                             <option value="Limited Liability Company">Limited Liability Company</option>
                                         </select>
                                         <label for="house" class="fa fa-calendar" rel="tooltip" title="Description"></label>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <hr/ style="clear: both">
                        <p align="center">
                           <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                           Save</button>
                           <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                           Cancel</a>
                        </p>
                     </form>
                  </div>
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
               </div>
               <hr/ style="clear: both">
               <div class="box-body">
                @include('erp.system_settings.partials.subsidiaries_table')
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection


@section('script')
<script>
   $(function () {
     $("#example1").DataTable();
   
         });
  @if ($errors->has('name') || $errors->has('phone_number') || $errors->has('email') || $errors->has('date_incorporated') || $errors->has('business_type'))
        $('.adf').slideToggle();
   @endif
   
   $('.adf').hide();
   $('.cdb').on('click', function(){
     $('.adf').slideToggle();
   
   });
   
     $('.cadfxx').on('click', function(){
        $('.adf').slideToggle();
     });
</script>
@endsection


