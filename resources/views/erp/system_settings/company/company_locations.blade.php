@extends('erp.layouts.master')
@section('title')
Company Locations
@endsection
@section('sidebar')
@include('erp.partials.sidebar')
@endsection
@section('content')
<section class="content-header">
   <h1>
      Company Locations
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Company Locations </li>
   </ol>
</section>
<section class="content">
   <!-- /.
      <div class="col-md-12">
           <!-- Custom Tabs -->
   <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Office Locations </b></a></li>
      </ul>
      <div class="tab-content" style="padding: 2%">
         <div class="tab-pane active" id="tab_1">
            <p align="right">
               <button class="btn btn-large btn-purple cdb" s><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add location</button>
            </p>
            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
               <div class="col-md-1 hidden-sm hidden-xs"></div>
               <div class="col-md-10">
                  <form method="POST" action="">
                     <h3 align="center">Office Locations</h3>
                     <div class="col-md-6" style="padding: 0px;">
                        <div class="form-group">
                           <label>Name</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" value="{{old('name')}}" name="name" placeholder="Name">
                              <label for="house" class="fa fa-user" rel="tooltip" title="Description"></label>
                              @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Mobile</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="Mobile">
                              <label for="house" class="fa fa-mobile" rel="tooltip" title="Description"></label>
                              @if ($errors->has('mobile')) <p class="help-block" style="color: red">{{ $errors->first('mobile') }}</p> @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Phone</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Phone">
                              <label for="house" class="fa fa-mobile" rel="tooltip" title="Description"></label>
                              @if ($errors->has('phone')) <p class="help-block" style="color: red">{{ $errors->first('phone') }}</p> @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Email</label>
                           <div class="icon-addon addon-md">
                              <input type="email" required="" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                              <label for="house" class="fa fa-envelope" rel="tooltip" title="Description"></label>
                              @if ($errors->has('email')) <p class="help-block" style="color: red">{{ $errors->first('email') }}</p> @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Longitude:</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" name="longitude" value="{{old('longitude')}}" placeholder="Longitude">
                              <label for="house" class="fa fa-map-pin" rel="tooltip" title="Description"></label>
                              @if ($errors->has('longitude')) <p class="help-block" style="color: red">{{ $errors->first('longitude') }}</p> @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Street:</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" name="street" value="{{ old('street') }}" placeholder="Street">
                              <label for="house" class="fa fa-map" rel="tooltip" title="Description"></label>
                              @if ($errors->has('street')) <p class="help-block" style="color: red">{{ $errors->first('street') }}</p> @endif
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6" style="padding-right: 0px;">
                        <div class="form-group">
                           <label>Website:</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" name="website" value="{{ old('website') }}" placeholder="website">
                              <label for="house" class="fa fa-globe" rel="tooltip" title="Description"></label>
                              @if ($errors->has('website')) <p class="help-block" style="color: red">{{ $errors->first('website') }}</p> @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Code:</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" name="code" value="{{ old('code') }}" placeholder="Assign Code">
                              <label for="house" class="fa fa-mobile" rel="tooltip" title="Code"></label>
                              @if ($errors->has('code')) <p class="help-block" style="color: red">{{ $errors->first('code') }}</p> @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Fax:</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" name="fax" value="{{ old('fax') }}" placeholder="Fax">
                              <label for="house" class="fa fa-mobile" rel="tooltip" title="Fax"></label>
                              @if ($errors->has('fax')) <p class="help-block" style="color: red">{{ $errors->first('fax') }}</p> @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Latitude:</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" name="latitude" value="{{ old('latitude') }}" placeholder="Latitude">
                              <label for="house" class="fa fa-map-pin" rel="tooltip" title="Latitude"></label>
                              @if ($errors->has('latitude')) <p class="help-block" style="color: red">{{ $errors->first('latitude') }}</p> @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Address:</label>
                           <div class="icon-addon addon-md">
                              <textarea name="address" required="" id="inputAddress" class="form-control" rows="3" required="required">{{old('address')}}</textarea>
                              <label for="house" class="fa fa-map" rel="tooltip" title="Address"></label>
                              @if ($errors->has('address')) <p class="help-block" style="color: red">{{ $errors->first('address') }}</p> @endif
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-lg-offset-5">
                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                              Save</button>
                              <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                              Cancel</a>
                     </div>
                  </form>
               </div>
               <div class="col-md-1 hidden-sm hidden-xs"></div>
            </div>
            <hr/ style="clear: both">
            <div class="box-body">
               <table id="example1" class="table table-bordered table-hover">
                  <thead>
                     <tr style="color:#8b8b8b">
                        <th>S/N</th>
                        <th>Name</th>
                        <th>Website</th>
                        <th>Mobile</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Street</th>
                        <th>Code</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(!empty($workAddresses))
                        <?php $num = 1; ?>
                        @foreach($workAddresses as $address)
                           <tr>
                              <td>{{ $num++ }}</td>
                              <td>{{ $address->name }}</td>
                              <td> {{ $address->website }} </td>
                              <td> {{ $address->mobile }} </td>
                              <td> {{ $address->phone }} </td>
                              <td> {{ $address->email }} </td>
                              <td> {{ $address->street }} </td>
                              <td> {{ $address->code }} </td>
                              <td>
                                 <a href="#modal-id{{ $address->id }}" data-toggle="modal">
                                 <span class="label label-warning" style="padding:7.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                 <i class="fa fa-trash"></i>  Edit </span></a>
                                 <a href="{{ url('workAddresses/delete', $address->id) }}" id="a_del">
                                 <span class="label label-danger" style="padding:7.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                 <i class="fa fa-trash"></i>  Delete </span></a>
                              </td>
                           </tr>


                           <div class="modal fade" id="modal-id{{ $address->id }}">
                              <div class="modal-dialog">
                                 <div class="modal-content">
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                       <h4 class="modal-title"></h4>
                                    </div>
                                    <div class="modal-body">
                                       <form action="{{ url('/workAddress/edit') }}" method="POST" role="form">
                                          <center>
                                             <legend>Edit form</legend>
                                          </center>
                                       
                                          <div class="row">
                                             <div class="col-lg-6">
                                                <div class="form-group">
                                                   <label>Name</label>
                                                   <div class="icon-addon addon-md">
                                                      <input type="text" class="form-control" value="{{ $address->name }}" name="name" placeholder="Name">
                                                      <label for="house" class="fa fa-user" rel="tooltip" title="Description"></label>
                                                      @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                                                   </div>
                                                </div>
                                             </div>

                                             <div class="col-lg-6">
                                                <div class="form-group">
                                                   <label>Mobile</label>
                                                   <div class="icon-addon addon-md">
                                                      <input type="text" class="form-control" name="mobile" value="{{ $address->mobile }}" placeholder="Mobile">
                                                      <label for="house" class="fa fa-mobile" rel="tooltip" title="Description"></label>
                                                      @if ($errors->has('mobile')) <p class="help-block" style="color: red">{{ $errors->first('mobile') }}</p> @endif
                                                   </div>
                                                </div>
                                             </div>
                                          </div>

                                          <div class="row">
                                                <div class="col-lg-6">
                                                   <div class="form-group">
                                                      <label>Phone</label>
                                                      <div class="icon-addon addon-md">
                                                         <input type="text" class="form-control" name="phone" value="{{ $address->phone }}" placeholder="Phone">
                                                         <label for="house" class="fa fa-mobile" rel="tooltip" title="Description"></label>
                                                         @if ($errors->has('phone')) <p class="help-block" style="color: red">{{ $errors->first('phone') }}</p> @endif
                                                      </div>
                                                   </div>
                                                </div>

                                                <div class="col-lg-6">
                                                   <div class="form-group">
                                                      <label>Email</label>
                                                      <div class="icon-addon addon-md">
                                                         <input type="email" class="form-control" name="email" value="{{ $address->email }}" placeholder="Email">
                                                         <label for="house" class="fa fa-envelope" rel="tooltip" title="Description"></label>
                                                         @if ($errors->has('email')) <p class="help-block" style="color: red">{{ $errors->first('email') }}</p> @endif
                                                      </div>
                                                   </div>
                                                </div>
                                          </div>

                                          <div class="row">
                                             <div class="col-lg-6">
                                                <div class="form-group">
                                                   <label>Longitude:</label>
                                                   <div class="icon-addon addon-md">
                                                      <input type="text" class="form-control" name="longitude" value="{{ $address->longitude }}" placeholder="Longitude">
                                                      <label for="house" class="fa fa-map-pin" rel="tooltip" title="Description"></label>
                                                      @if ($errors->has('longitude')) <p class="help-block" style="color: red">{{ $errors->first('longitude') }}</p> @endif
                                                   </div>
                                                </div>
                                             </div>

                                             <div class="col-lg-6">
                                                <div class="form-group">
                                                   <label>Street:</label>
                                                   <div class="icon-addon addon-md">
                                                      <input type="text" class="form-control" name="street" value="{{ $address->street }}" placeholder="Street">
                                                      <label for="house" class="fa fa-map" rel="tooltip" title="Description"></label>
                                                      @if ($errors->has('street')) <p class="help-block" style="color: red">{{ $errors->first('street') }}</p> @endif
                                                   </div>
                                                </div>
                                             </div>
                                          </div>

                                          <div class="row">
                                                <div class="col-lg-6">
                                                   <div class="form-group">
                                                      <label>Website:</label>
                                                      <div class="icon-addon addon-md">
                                                         <input type="text" class="form-control" name="website" value="{{ $address->website }}" placeholder="website">
                                                         <label for="house" class="fa fa-globe" rel="tooltip" title="Description"></label>
                                                         @if ($errors->has('website')) <p class="help-block" style="color: red">{{ $errors->first('website') }}</p> @endif
                                                      </div>
                                                   </div>
                                                </div>

                                                <div class="col-lg-6">
                                                   <div class="form-group">
                                                      <label>Code:</label>
                                                      <div class="icon-addon addon-md">
                                                         <input type="text" class="form-control" name="code" value="{{ $address->code }}" placeholder="Assign Code">
                                                         <label for="house" class="fa fa-mobile" rel="tooltip" title="Code"></label>
                                                         @if ($errors->has('code')) <p class="help-block" style="color: red">{{ $errors->first('code') }}</p> @endif
                                                      </div>
                                                   </div>
                                                </div>
                                          </div>

                                          <div class="row">
                                             <div class="col-lg-6">
                                                <div class="form-group">
                                                   <label>Fax:</label>
                                                   <div class="icon-addon addon-md">
                                                      <input type="text" class="form-control" name="fax" value="{{ $address->fax }}" placeholder="Fax">
                                                      <label for="house" class="fa fa-mobile" rel="tooltip" title="Fax"></label>
                                                      @if ($errors->has('fax')) <p class="help-block" style="color: red">{{ $errors->first('fax') }}</p> @endif
                                                   </div>
                                                </div>
                                             </div>

                                             <div class="col-lg-6">
                                                <div class="form-group">
                                                   <label>Latitude:</label>
                                                   <div class="icon-addon addon-md">
                                                      <input type="text" class="form-control" name="latitude" value="{{ $address->latitude }}" placeholder="Latitude">
                                                      <label for="house" class="fa fa-map-pin" rel="tooltip" title="Latitude"></label>
                                                      @if ($errors->has('latitude')) <p class="help-block" style="color: red">{{ $errors->first('latitude') }}</p> @endif
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <input type="hidden" name="work_address_id" value="{{ $address->id }}">
                                          <center>
                                             <button type="submit" class="btn btn-success">Update <i class="fa fa-upload"></i></button>
                                          </center>
                                       </form>
                                    
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        @endforeach
                     @endif
                  </tbody>
                     </tfoot>
               </table>
            </div>
         </div>
      </div>
   </div>
   </div>
</section>
@endsection
@section('script')
<script>
   $(function () {
     $("#example1").DataTable();
   
         });

   @if ($errors->has('name') || $errors->has('phone') || $errors->has('street') || $errors->has('email'))
        $('.adf').slideToggle();
   @endif

   
   
   $('.adf').hide();
   $('.cdb').on('click', function(){
     $('.adf').slideToggle();
   
   });
   
     $('.cadfxx').on('click', function(){
        $('.adf').slideToggle();
     });
</script>
@endsection