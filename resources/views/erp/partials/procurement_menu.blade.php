<li class="header"> <b>PROCUREMENT</b></li>

  <li class="treeview">
  <a href="#">
    <i class="fa fa-folder"></i> <span>Parameters</span>

     <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
  </a>
  <ul class="treeview-menu">
     
      <li><a href="{{url('requestCategories')}}"><i class="fa fa-circle-o text-green"></i>Categories</a></li>
      <li><a href="{{url('add_vendor')}}"><i class="fa fa-circle-o text-red"></i>Vendor</a></li>
  </ul>
</li>

<li class="treeview">
  <a href="#">
    <i class="fa fa-folder"></i> <span>Procurement</span>

     <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
  </a>
  <ul class="treeview-menu">
   
    <li><a href="{{url('/save_requests')}}"><i class="fa fa-circle-o text-green"></i>Request</a></li>

    <li><a href="{{url('requests/lineManagerRequests')}}"><i class="fa fa-circle-o text-red"></i>Pending Request <br />Line Manager</a></li>
    <li><a href="{{url('requests/cLevelRequests')}}"><i class="fa fa-circle-o text-red"></i>Pending Request <br />C-Levels</a></li>
  </ul>
</li>

<li class="treeview">
  <a href="#">
    <i class="fa fa-folder"></i> <span>Procurement Report</span>

     <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
  </a>
  <ul class="treeview-menu">
    <li><a href="{{url('procurement/reports')}}"><i class="fa fa-circle-o text-green"></i>Procurement Report</a></li>
  </ul>
</li>