<li class="header"> <b>FINANCE & ACCOUNTING</b></li>
<li class="treeview">
   <a href="#">
   <i class="fa fa-cog"></i>
   <span>Parameter</span>
   <span class="pull-right-container">
   <i class="fa fa-angle-left pull-right"></i>
   </span>
   </a>
   <ul class="treeview-menu">
      <li>
         <a href="{{ url('requestCategories') }}"><i class="fa fa-circle-o"></i> Revenue Categories
         </a>
      </li>
   </ul>
   <ul class="treeview-menu">
      <li>
         <a href="{{ url('clients') }}"><i class="fa fa-circle-o"></i> Clients
         <span class="pull-right-container">
         </span>
         </a>
      </li>
   </ul>
   <ul class="treeview-menu">
      <li>
         <a href="#"><i class="fa fa-circle-o"></i> Expenses
         <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
         </span>
         </a>
         <ul class="treeview-menu">
            <li><a href="{{ url('finance/operating_expenses') }}"><i class="fa fa-circle-o text-green"></i> Operating Expenses</a></li>
            <li><a href="{{ url('finance/capital_expenses') }}"><i class="fa fa-circle-o text-red"></i> Capital Expenses </a></li>
         </ul>
      </li>
   </ul>
   <ul class="treeview-menu">
      <li>
         <a href="#"><i class="fa fa-circle-o"></i> Account
         <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
         </span>
         </a>
         <ul class="treeview-menu">
            <li><a href="{{ url('finance/banks') }}"><i class="fa fa-circle-o text-green"></i> Bank</a></li>
            <li><a href="{{ url('finance/bank_accounts') }}"><i class="fa fa-circle-o text-yellow"></i> Bank Accounts </a></li>
            <li><a href="{{ url('finance/account/create') }}"><i class="fa fa-circle-o text-yellow"></i> Advance Bank Accounts </a></li>
            <li><a href="{{ url('finance/account-effects') }}"><i class="fa fa-circle-o text-aqua"></i> Account Effects </a></li>
            <li><a href="{{ url('finance/account_header') }}"><i class="fa fa-circle-o text-red"></i> Account Headers </a></li>
            <li><a href="{{ url('finance/account_sub_header') }}"><i class="fa fa-circle-o text-aqua"></i> Account Sub Header </a></li>
         </ul>
      </li>
   </ul>

   <ul class="treeview-menu">
      <li>
         <a href="{{ url('finance/salvage_items') }}"><i class="fa fa-circle-o"></i> Item Salvage
         </a>
      </li>
   </ul>
   
</li>
<li class="treeview">
   <a href="{{ url('finance/revenue-streams') }}">
   <i class="ion ion-stats-bars"></i> <span>Revenue Streams</span>
   </a>
</li>
{{-- <li class="treeview">
   <a href="">
   <i class="ion ion-arrow-graph-up-right"></i> <span>Remittance</span>
   <span class="pull-right-container">
   <i class="fa fa-angle-left pull-right"></i>
   </span>
   </a>
   <ul class="treeview-menu">
      <li><a href="{{ url('finance/remittance') }}"><i class="fa fa-circle-o text-yellow"></i> PHEDC</a></li>
   </ul>
</li> --}}
<li class="treeview">
   <a href="{{ url('finance/revenue') }}">
   <i class="ion ion-cash"></i> <span>Revenue</span>
   </a>
</li>
<li class="treeview">
   <a href="#">
   <i class="fa fa-book"></i>
   <span>Accounting Book</span>
   <span class="pull-right-container">
   <i class="fa fa-angle-left pull-right"></i>
   </span>
   </a>
   <ul class="treeview-menu">
      <li>
         <a href="#"><i class="fa fa-circle-o"></i> Liquidity
         <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
         </span>
         </a>
         <ul class="treeview-menu">
            <li><a href="{{ url('finance/expense_report') }}"><i class="fa fa-circle-o text-green"></i> Expense Sheet Report</a></li>
            <li><a href="{{ url('finance/expense_pivot') }}"><i class="fa fa-circle-o text-yellow"></i> Summarized Expenses </a></li>
         </ul>
      </li>
   </ul>
</li>
<li class="treeview">
   <a href="#">
   <i class="fa fa-area-chart"></i> <span>Disbursement</span>
   <span class="pull-right-container">
   <i class="fa fa-angle-left pull-right"></i>
   </span>
   </a>
   <ul class="treeview-menu">
      <li><a href="{{ url('finance/disbursment/salaries') }}"><i class="fa fa-circle-o text-green"></i>Salary</a></li>
      <li><a href="{{ url('finance/disbursement/procurement') }}"><i class="fa fa-circle-o text-red"></i>Procurement</a></li>
   </ul>
</li>
<li class="treeview">
   <a href="#">
   <i class="fa fa-area-chart"></i> <span>Vendors</span>
   <span class="pull-right-container">
   <i class="fa fa-angle-left pull-right"></i>
   </span>
   </a>
   <ul class="treeview-menu">
      <li><a href="{{ url('add_vendor') }}"><i class="fa fa-circle-o text-green"></i>Vendors</a></li>
      <li><a href="{{ url('finance/invoice') }}"><i class="fa fa-circle-o text-red"></i>Invoices</a></li>
   </ul>
</li>
<li class="treeview">
   <a href="#">
   <i class="fa fa-pencil"></i>
   <span>Budget</span>
   <span class="pull-right-container">
   <i class="fa fa-angle-left pull-right"></i>
   </span>
   </a>
   <ul class="treeview-menu">
      <li><a href="{{url('finance/budget/create')}}"><i class="fa fa-circle-o text-green"></i>Add Budget</a></li>
      {{-- <li><a href="{{url('finance/budget/create-custom')}}"><i class="fa fa-circle-o text-green"></i>Add Custom Budget</a></li> --}}
      {{-- <li><a href="{{url('finance/budget/view/'.env('ACCOUNT_NUMBER').'/'.env('BUDGET_UUID'))}}"><i class="fa fa-circle-o text-yellow"></i>View Budget</a></li> --}}
      {{-- <li><a href="{{url('finance/budget/fixed-expense')}}"><i class="fa fa-circle-o text-green"></i>Fixed Expense</a></li>
      <li><a href="{{url('finance/budget/fixed-expense')}}"><i class="fa fa-circle-o text-yellow"></i>Variable Expense</a></li> --}}
   </ul>
</li>

<li class="treeview">
   <a href="#">
   <i class="fa fa-pencil"></i>
   <span>Accounts</span>
   <span class="pull-right-container">
   <i class="fa fa-angle-left pull-right"></i>
   </span>
   </a>
   <ul class="treeview-menu">
      <li><a href="{{url('finance/account/create')}}"><i class="fa fa-circle-o text-green"></i>Accounts</a></li>
   </ul>
</li>


<li class="treeview">
   <a href="{{ url('finance/tax') }}">
   <i class="fa fa-bar-chart"></i> <span>Tax Management</span>
   </a>
</li>
<li class="treeview">
   <a href="{{ url('finance/company-loan/index') }}">
   <i class="fa fa-bar-chart"></i> <span>Loan</span>
   </a>
</li>

