<li class="header"> <b>HR & PAYROLL </b></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>HR Parameters</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Employees</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
         <ul class="treeview-menu">
                <li>
                  <a href="{{url('/departments')}}"><i class="fa fa-circle-o"></i> Departments
                    <span class="pull-right-container">
                      <i class="fa pull-right"></i>
                    </span>
                  </a>
                </li>
              </ul>

              <ul class="treeview-menu">
                <li>
                  <a href="{{url('/employee')}}"><i class="fa fa-circle-o"></i>Manage Employees
                    <span class="pull-right-container">
                      <i class="pull-right"></i>
                    </span>
                  </a>
               
                </li>
              </ul>

               <ul class="treeview-menu">
                <li>
                  <a href="{{url('/add_employee_renumeration')}}"><i class="fa fa-circle-o"></i> Remunerations
                    <span class="pull-right-container">
                      <i class="pull-right"></i>
                    </span>
                  </a>
               
                </li>
              </ul>

            <ul class="treeview-menu">
                <li>
                    <a href="{{url('add_employee_bonus')}}"><i class="fa fa-circle-o"></i> Employees' Bonus
                        <span class="pull-right-container">
                      <i class="pull-right"></i>
                    </span>
                    </a>

                </li>
            </ul>

            <ul class="treeview-menu">
                <li>
                    <a href="{{url('jobs')}}"><i class="fa fa-circle-o"></i> Employees' Jobs
                        <span class="pull-right-container">
                      <i class="pull-right"></i>
                    </span>
                    </a>

                </li>
            </ul>

               <ul class="treeview-menu">
                <li>
                  <a href="{{url('add_employee_deduction')}}"><i class="fa fa-circle-o"></i> Employee Dedutions
                    <span class="pull-right-container">
                      <i class="pull-right"></i>
                    </span>
                  </a>
                 
                </li>
              </ul>

               <ul class="treeview-menu">
                <li>
                  <a href="{{url('add_leave_type')}}"><i class="fa fa-circle-o"></i> Leave Management
                    <span class="pull-right-container">
                      <i class="pull-right"></i>
                    </span>
                  </a>
                </li>
              </ul>

              <ul class="treeview-menu">
                <li>
                  <a href="{{url('employee_leaves')}}"><i class="fa fa-circle-o"></i> Leave Management
                    <span class="pull-right-container">
                      <i class="pull-right"></i>
                    </span>
                  </a>
                </li>
              </ul>

               <ul class="treeview-menu">
                <li>
                  <a href="{{url('employee_absences')}}"><i class="fa fa-circle-o"></i> Absences
                    <span class="pull-right-container">
                      <i class="pull-right"></i>
                    </span>
                  </a>
                  
                </li>
              </ul>

              <ul class="treeview-menu">
                <li>
                  <a href=""><i class="fa fa-circle-o"></i> Employee Loans
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{url('employee_loans')}}"><i class="fa fa-circle-o text-green"></i> Add Loan</a></li>
                     <li><a href="{{url('loan_settlements')}}"><i class="fa fa-circle-o text-yellow"></i> Loan Settlement </a></li>
                  </ul>
                </li>
              </ul>

              <ul class="treeview-menu">
                <li>
                  <a href="{{url('employee_arrears')}}"><i class="fa fa-circle-o"></i> Salary Arrears
                    <span class="pull-right-container">
                      <i class="pull-right"></i>
                    </span>
                  </a>
                </li>
              </ul>




        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-list-alt"></i>
            <span>Payroll</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('work_days')}}"><i class="fa fa-briefcase"></i> Managing Workdays <br /> <span style="margin-left:10%">& Public Holidays</span></a></li>
          </ul>

           <ul class="treeview-menu">
                <li>
                  <a href="#"><i class="fa fa-list-ul"></i> Generate Payroll
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{url('generate_payroll')}}"><i class="fa fa-circle-o text-green"></i> Generate Draft Payroll</a></li>
                    <li><a href="{{url('draft_payrolls')}}"><i class="fa fa-circle-o text-red"></i> Draft Payroll </a></li>

                    <li><a href="{{url('payrolls')}}"><i class="fa fa-circle-o text-yellow"></i> Payroll History </a></li>

                    <li><a href="{{url('send_payslip')}}"><i class="fa fa-circle-o text-aqua"></i> Send Payslips </a></li>
                     
                      
                  </ul>
                </li>
 </ul>


        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i> <span>HR Report & Analysis</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('department_report')}}"><i class="fa fa-circle-o"></i> Department Report</a></li>
            <li><a href="{{url('paye_report')}}"><i class="fa fa-circle-o"></i> PAYE Report</a></li>
            <li><a href="{{url('pension_report')}}"><i class="fa fa-circle-o"></i> Pension Report</a></li>
          </ul>
        </li>