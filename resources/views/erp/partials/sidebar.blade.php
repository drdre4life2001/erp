  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar ScrollStyle">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('erp/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>User</p>
        </div>
      </div>
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="{{url('/')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a> 
        </li>
        
        <?php
        /**$links = Session::get('links'); **/
        use Illuminate\Support\Facades\DB;
        //dlld
        //ALl these shit, just to get the role_id of either the logged in user or employee
        $role_id = \App\Libraries\Utilities::getRoleId();
        $links = DB::select("select a.link as links from sys_menu_links as a, sys_modules as b, user_permissions as c
            where
            a.module_id = b.id and
            c.resource_id = a.module_id and
            c.role_id = $role_id
            ");
                    ?>
        @if(isset($links))
            @foreach($links as $link_key => $link)
              @if(auth()->user()->roles->id == "35" && $link->links == "erp.partials.procurement_menu")
              @endif
                <li><hr class="light-grey-hr mb-10"/></li>
                @include("$link->links")
            @endforeach
        @endif
        @include('erp.partials.crm')
        @include('erp.partials.other_expenses')
        @include('erp.partials.employee')
      </ul>
    </section>
</aside>