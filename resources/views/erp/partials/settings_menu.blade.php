<li class="header"> <b>SYSTEM SETTINGS </b></li>
<li class="treeview">
  <a href="#">
    <i class="ion ion-person-stalker"></i> <span>User Management</span>
    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
  </a>
  <ul class="treeview-menu">
    <li><a href="{{ url('erp-users/index') }}"><i class="fa fa-circle-o text-yellow"></i>Users</a></li>
    <li><a href="{{ url('/roles') }}"><i class="fa fa-circle-o text-red"></i> Role</a></li>
  </ul>
</li>
  
  <li class="treeview">
  <a href="#">
    <i class="fa fa-building"></i> <span>Company Structure</span>
    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
  </a>
  <ul class="treeview-menu">
    <li><a href="{{ url('/organizations') }}"><i class="fa fa-circle-o text-yellow"></i> Company Subscidiaries</a></li>
    <li><a href="{{ url('/workAddresses') }}"><i class="fa fa-circle-o text-red"></i> Company Location</a></li>
  </ul>
</li>         