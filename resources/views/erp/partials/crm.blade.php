<li class="header"> <b>CRM</b></li>

    <li class="treeview">
        <a href="{{ url('crm/prospects/create') }}">
          <i class="fa fa-users"></i> <span>Prospects</span>
          <span class="pull-right-container">
            <i class="fa pull-right"></i>
          </span>
        </a>
        
      </li>

        <li class="treeview">
        <a href="">
          <i class="fa fa-filter"></i> <span>Leads</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('/lead') }}"><i class="fa fa-circle-o text-yellow"></i>Leads</a></li>
        </ul>
      </li>

      <li>
        <a href="{{ url('crm/pipelines') }}">
          <i class="fa fa-calendar"></i> <span>Pipeline</span>
          
        </a>
      </li>

       <li>
        <a href="{{ url('crm/report/by-user') }}">
          <i class="fa fa-book"></i> <span>Report</span>
          
        </a>
      </li>
