<li class="header"> <b>OTHER EXPENSES</b></li>
     <li class="treeview">
<a href="#">
<i class="fa fa-google-wallet"></i> <span>Expenses</span>
<span class="pull-right-container">
  <i class="fa fa-angle-left pull-right"></i>
</span>
</a>
<ul class="treeview-menu">


<li><a href="{{url('other-expenses/#all_expenses')}}"><i class="fa fa-circle-o text-red"></i>My Expenses</a></li>
 <li><a href="{{url('other-expenses/approved-expenses')}}"><i class="fa fa-circle-o text-yellow"></i>Approved Expenses</a></li>
   <li><a href="{{url('other-expenses/declined-expenses')}}"><i class="fa fa-circle-o text-aqua"></i>Declined Expenses</a></li>
    <li><a href="{{url('other-expenses/#all_expenses')}}"><i class="fa fa-circle-o text-red"></i>All Expenses</a></li>
  </ul>
</li>