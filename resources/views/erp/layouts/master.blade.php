<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  @yield('html_meta')
  <title>ERP | @yield('title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('erp/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('erp/dist/css/AdminLTE.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('erp/dist/css/skins/_all-skins.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('erp/plugins/iCheck/flat/blue.css') }}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{ asset('erp/plugins/morris/morris.css') }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('erp/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <!-- Date Picker --> 
  <link rel="stylesheet" href="{{ asset('erp/plugins/datepicker/datepicker3.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('erp/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('erp/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

  <link rel="stylesheet" href="{{ asset('erp/plugins/datatables/dataTables.bootstrap.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans|Source+Sans+Pro|Rubik" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" type="text/css"/>
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('erp/toastr.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('erp/select2.min.css') }}"/>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <style type="text/css">
    .ScrollStyle
    {
        max-height: 150px;
        overflow-y: scroll;
    }

    #loading-img {
      background: url(http://preloaders.net/preloaders/360/Velocity.gif) center center no-repeat;
      height: 100%;
      z-index: 20;
    }

    .overlay {
      background: #e9e9e9;
      display: none;
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      opacity: 0.5;
    }


  </style>

    <style>
      /*loader css begins for assessment page*/
      .loader,
      .loader:after {
        border-radius: 50%;
        width: 10em;
        height: 10em;
      }
      .loader {
        margin: 60px auto;
        font-size: 10px;
        position: relative;
        text-indent: -9999em;
        border-top: 1.1em solid rgba(255, 255, 255, 0.2);
        border-right: 1.1em solid rgba(255, 255, 255, 0.2);
        border-bottom: 1.1em solid rgba(255, 255, 255, 0.2);
        border-bottom-color: #BC96FF;
        border-left: 1.1em solid #ffffff;
        -webkit-transform: translateZ(0);
        -ms-transform: translateZ(0);
        transform: translateZ(0);
        -webkit-animation: load8 1.1s infinite linear;
        animation: load8 1.1s infinite linear;
      }
      @-webkit-keyframes load8 {
        0% {
          -webkit-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      @keyframes load8 {
        0% {
          -webkit-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      #loadingDiv {
        position:absolute;;
        top:0;
        left:0;
        width:100%;
        height:100%;
        background-color: rgba(0, 0, 0, 0.1);
        color:red;
      }
    </style>

  @yield('style')
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!-- <span class="logo-mini"><b>E</b>RP</span> -->
      <!-- logo for regular state and mobile devices -->
      <!-- <span class="logo-lg"><b>TA - </b>ERP</span> -->
     <p align="center"> <img src="{{ asset('erp/dist/img/advance-erp.png') }}"> </p>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
         
 
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset('uploads/employee_pictures/user.jpg') }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ ucfirst(auth()->user()->email) }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{ asset('erp/dist/img/male.png') }}" class="img-circle" alt="User Image">

                <p>
                  {{ ucfirst(auth()->user()->email) }}
                  <small>Member since {{-- Nov. 2012 --}}</small>
                </p>
              </li>
              <!-- Menu Body -->
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  {{-- <a href="#" class="btn btn-default btn-flat">Profile</a> --}}
                </div>
                <div class="pull-right">
                  <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
         
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('erp/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Anthony</p>
        </div>
      </div>
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a> 
        </li>
      </ul>
    </section>
  </aside>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper dashboard-wrapper">
    @yield('content')
  </div>
    @yield('sidebar')
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>

<script src="{{ asset('erp/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('erp/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('erp/toastr.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('erp/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script src="{{ asset('erp/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('erp/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('erp/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('erp/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('erp/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('erp/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('erp/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('erp/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('erp/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('erp/dist/js/app.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->

<!-- FastClick -->
<script src="{{ asset('erp/plugins/fastclick/fastclick.js') }}"></script>



<script src="{{ asset('erp/plugins/chartjs/Chart.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('erp/dist/js/pages/dashboard2.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>


@yield('script')
<script>
    //$( function() {
    // $('#example1').DataTable({
    //     dom: 'Bfrtip',
    //     buttons: [
    //         'print', 'excel', 'pdf'
    //     ],
    // });

    function ConfirmDelete()
              {
              var x = confirm("Are you sure you want to delete?");
              if (x)
                return true;
              else
                return false;
    }
    
    $("a#a_del").click(function(){
     return ConfirmDelete();
    });

    function ConfirmDeactivate()
              {
              var x = confirm("Are you sure you want to deactivate user ?");
              if (x)
                return true;
              else
                return false;
    }
    
    $("a#a_deactivate").click(function(){
     return ConfirmDeactivate();
    });

    $('.fromDate').datepicker(
        { 
          format: 'dd-mm-yy'
        }
    );
    $('.toDate').datepicker(
        { 
          format: 'dd-mm-yy'
        }
    );

    @if (session('success'))
    toastr.success("{{session('success')}}");
    @endif

    @if (session('error'))
    toastr.error("{{session('error')}}");
    @endif

    @if (session('info'))
    toastr.info("{{session('info')}}");
    @endif

    //For FinanceController$bankBalance method basically a functionality for conditional dropdown select
    $('.bank_id').change(function(e) {
        var parent = e.target.value;
        $.get('/finance/bank/'+ parent, function(data) {
            $( ".bank_balance" ).html("");
           console.log(data);
            var SelectOption;
            $.each(data, function(index,value)
            {
                if( value.currentBalance > 0 ) {
                    $(".div_amount").show();
                    SelectOption +=  "<option value='"+ value.id   +"' >" + value.currentBalance + "</option>";
                }else{
                    SelectOption +=  "<option value='"+ value.id   +"' >" + 0.00 + "</option>";
                }
            });

            $( ".bank_balance" ).html(SelectOption);

        });
    });


    //Account header
    $('.account_header_id').change(function(e) {
        var a_parent = e.target.value;
        $(".subheader_lists").show();
        $.get('/finance/account_header/'+ a_parent, function(data) {
            $( ".subheader_id" ).html("");
            console.log(data);
            var a_SelectOption ="<option  value=''>--Select Subheader--</option>";

            $.each(data, function(index,value)
            {
                a_SelectOption +=  "<option value='"+ value.header_id +"' >" + value.name + "</option>";
            });

            $( ".subheader_id" ).html(a_SelectOption);

        });
    });

    $('.employee_search').select2({
        placeholder: 'Type Employee Name and Select',
        ajax: {
            url: '/employees-ajax',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results:  $.map(data, function (item) {
                      console.log(item);
                        return {
                            text: item.first_name+" | Organization : "+ item.organization,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    $('.work_address_search').select2({
        placeholder: 'Insert Existing Work Address then Select',
        ajax: {
            url: '/work-address-ajax',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results:  $.map(data, function (item) {
                      console.log(item);
                        return {
                            text: item.name + ' | '+ item.website,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    // $('.account_effects').select2({
    //     placeholder: 'Insert Existing Work Address then Select',
    //     ajax: {
    //         url: '/work-address-ajax',
    //         dataType: 'json',
    //         delay: 250,
    //         processResults: function (data) {
    //             return {
    //                 results:  $.map(data, function (item) {
    //                   console.log(item);
    //                     return {
    //                         text: item.name + ' | '+ item.website,
    //                         id: item.id
    //                     }
    //                 })
    //             };
    //         },
    //         cache: true
    //     }
    // });

    $('.job_title_search').select2({
        placeholder: 'Insert Existing Job Title then Select',
        ajax: {
            url: '/job-location-ajax',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results:  $.map(data, function (item) {
                      console.log(item);
                        return {
                            text: item.name + ' | '+ item.code,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    $('.organization_search').select2({
        placeholder: 'Type Your Existing Organization then Select',
        ajax: {
            url: '/organizations-ajax',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results:  $.map(data, function (item) {
                        console.log(item);
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    $('.client_search').select2({
        placeholder: 'Type Your Existing Client then Select',
        ajax: {
            url: '/clients-ajax',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results:  $.map(data, function (item) {
                        console.log(item);
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    $('.revenue_stream_search').select2({
        placeholder: 'Type and select exisiting revenue stream',
        ajax: {
            url: '/revenue-stream-ajax',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results:  $.map(data, function (item) {
                        console.log(item);
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    $('.department_search').select2({
        placeholder: 'Type and select exisiting department',
        ajax: {
            url: '/departments-ajax',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results:  $.map(data, function (item) {
                        console.log(item);
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    //data table
    $("#example1").DataTable();

    $(".loader_clicked").click(function () {
        $(".overlay").show();
    });

</script>

  <script type="text/javascript">

      $(".table_clicked").click(function() {
          $('table').append('<div style="margin-top: 3%;" id="loadingDiv"><div class="loader">Loading...</div></div>');
          $(window).on('load', function(){
              setTimeout(removeLoader, 20000); //wait for page load PLUS two seconds.
          });
          function removeLoader(){
              $( "#loadingDiv" ).fadeOut(500, function() {
                  // fadeOut complete. Remove the loading div
                  $( "#loadingDiv" ).remove(); //makes page more lightweight
              });
          }
      });

  </script>

  <script type="text/javascript">

      $(".form_clicked").click(function() {
          $('form').append('<div style="margin-top: 0%; z-index:1000; cursor:wait;" id="loadingDiv"><div class="loader">Loading...</div></div>');
          $(this).css('cursor', 'wait');
          $(window).on('load', function(){
              setTimeout(removeLoader, 20000); //wait for page load PLUS two seconds.
          });

          function removeLoader(){
              $( "#loadingDiv" ).fadeOut(500, function() {
                  // fadeOut complete. Remove the loading div
                  $( "#loadingDiv" ).remove(); //makes page more lightweight
              });
          }
      });

  </script>

</body>
</body>
</html>
