
<form action="" method="post">
    <div class="form-group">
        <label class="control-label mb-10">Employee</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="icon-user"></i></div>
            <select required="" name="employee_id" class="form-control selectpicker employee_search" data-show-subtext="true"
                    data-live-search="true" >
            </select>
               @if ($errors->has('employee_id')) <p class="help-block" style="color: red">{{ $errors->first('employee_id') }}</p> @endif

        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10">Type</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="icon-user"></i></div>
            <select required="" class="form-control selectpicker" data-show-subtext="true"
                    data-live-search="true" name="leave_type_id">
                    <option value="">Select Leave Type</option>
                @foreach($leave_types as $type)
                    <option data-subtext="{{ $type->id }}"
                            value="{{ $type->id }}">{{ ucfirst($type->name) }}
                    </option>
                @endforeach
            </select>
               @if ($errors->has('leave_type_id')) <p class="help-block" style="color: red"> {{ $errors->first('leave_type_id') }}</p> @endif
        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10">Start Date</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="icon-user"></i></div>
            <input required="" type="date" name="start_date" value="{{old('start_date')}}" class="form-control" min="1" max="30">
               @if ($errors->has('start_date')) <p class="help-block" style="color: red"> {{ $errors->first('start_date') }}</p> @endif
        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10">End Date</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="icon-user"></i></div>
            <input required="" type="date" name="end_date" value="{{old('end_date')}}" class="form-control" min="1" max="30">
               @if ($errors->has('end_date')) <p class="help-block" style="color: red"> {{ $errors->first('end_date') }}</p> @endif
        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10">Number of days</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="icon-user"></i></div>
            <input required="" type="number" name="number_of_days" value="{{old('number_of_days')}}" class="form-control" min="1" max="30">
               @if ($errors->has('number_of_days')) <p class="help-block" style="color: red"> {{ $errors->first('number_of_days') }}</p> @endif
        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10">Leave Description</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="icon-user"></i></div>
            <textarea required="" name="description" class="form-control">{{old('description')}}</textarea>
               @if ($errors->has('description')) <p class="help-block" style="color: red"> {{ $errors->first('description') }}</p> @endif
        </div>
    </div>

    <div class="form-group">
        <div class="input-group mb-15">
            <input class="btn btn-success btn-anim" type="submit" value="Create Leave">
        </div>
    </div>
</form>