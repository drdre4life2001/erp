@extends('erp.layouts.master')
  
  @section('title')
    Human Resource - Employee Remuneration
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  		<section class="content-header">
  		   <h1>
  		      Employees' Renumerations
  		   </h1>
  		   <ol class="breadcrumb">
  		      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  		      <li class="active">Employees' Renumerations</li>
  		   </ol>
  		</section>
  		<section class="content">
  		   <div class="col-md-12">
	  		   <div class="nav-tabs-custom">
	  		      <ul class="nav nav-tabs">
	  		         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>Employees' Renumerations Table</b></a></li>
	  		      </ul>
	  		      <div class="tab-content" style="padding: 2%">
	  		         <div class="tab-pane active" id="tab_1">
	  		            <p align="right">
	  		               <button class="btn btn-large btn-purple cdb" style="color:white"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add Renumerations</button>
	  		            </p>
	  		            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; padding: 3% ">
	  		               <h3 align="center">Add Renumeration For An Employee</h3>
	  		               @include('erp.hr.partials.remuneration_form')
	  		           </div>
										<hr/ style="clear: both">
										<div class="table-wrap">
												<div class="table-responsive">
														@include('erp.hr.partials.remuneration_table')
													</div>			
										</div>
	  		         </div>
	  		      </div>
	  		      <!-- /.tab-content -->
     		   </div>
  		   </div>
  		   <!-- /.col -->
  		</section>
  @endsection

  @section('script')
  	<script type="text/javascript">
  		$("#example1").DataTable();

  		$('.adf').hide();
  		 $('.cdb').on('click', function(){
  		   $('.adf').slideToggle();
  		
  		});

	   $('.cadfxx').on('click', function(){
	      $('.adf').slideToggle();
	   });
  	</script>
  @endsection