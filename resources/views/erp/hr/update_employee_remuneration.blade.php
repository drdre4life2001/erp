@extends('erp.layouts.master')
  
  @section('title')
    Human Resource - Update Employee Remuneration
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  	<div class="row">
        <div class="col-md-6">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap mt-40">
                            <form action="{{ url('update_employee_renumeration/') }}" method="post">
                                <div class="form-group">
                                    <label class="control-label mb-10">Employee</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="icon-user"></i></div>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="exampleInputuname_1"
                                                    value="{{ @$employee->first_name }}, {{ @$employee->lastname }}" readonly>
                                            <input type="hidden" name="employee_id" value="{{ @$employee->id }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Basic Salary</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                                name="basic_pay" value="{{ $renumeration->basic_pay }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Housing Allowance</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               placeholder="Housing Allowance (Gross)" name="housing" value="{{ $renumeration->housing }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Transport Allowance</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               placeholder="Transport Allowance (Gross)" name="transport" value="{{ $renumeration->transport }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Leave Allowance</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               placeholder="Leave Allowance" name="leave" value="{{ $renumeration->leave_allowance }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Utility Allowance</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               placeholder="Utility Allowance" name="utility" value="{{ $renumeration->utility }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Meal Allowance</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               placeholder="Meal Allowance" name="meal" value="{{ $renumeration->meal }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Others</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               placeholder="Others" name="others" value="{{ $renumeration->others }}">
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap mt-40">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="mt-25">
                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Entertainment Allowance</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                                           placeholder="Meal Allowance" name="entertainment" value="{{ $renumeration->entertainment }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Number of Dependent Relatives</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                                           placeholder="Number of Dependent Relatives" name="dependable_relative" value="{{ $renumeration->dependable_relative }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Number of Children</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                                           placeholder="Number of Children" name="children" value="{{ $renumeration->children }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                   <div class="col-md-3">
                                                      <label class="control-label mb-10"
                                                         for="exampleInputuname_1">Compute National Housing Fund
                                                      in Payroll?</label>
                                                      <div class="input-group">
                                                         <select required="" class="form-control selectpicker"
                                                            data-show-subtext="true"
                                                            data-live-search="true" name="nhf">
                                                            <option @if($renumeration->nhf == "1") selected @endif data-subtext="Yes"
                                                               value="1">Yes
                                                            </option>
                                                            <option @if($renumeration->nhf == "0") selected @endif data-subtext="No"
                                                               value="0">No
                                                            </option>
                                                         </select>
                                                          @if ($errors->has('nhf')) <p class="help-block" style="color: red">{{ $errors->first('nhf') }}</p> @endif
                                                      </div>
                                                   </div>
                                                   <div class="col-md-3">
                                                      <div class="form-group">
                                                         <label class="control-label mb-10"
                                                            for="exampleInputuname_1">Compute Life Assurance in
                                                         Payroll?</label>
                                                         <div class="input-group">
                                                            <select required="" class="form-control selectpicker"
                                                               data-show-subtext="true"
                                                               data-live-search="true" name="life_assurance">
                                                               <option @if($renumeration->life_assurance == "1") selected @endif data-subtext="Yes"
                                                                  value="1">Yes
                                                               </option>
                                                               <option @if($renumeration->life_assurance == "0") selected @endif data-subtext="No"
                                                                  value="0">No
                                                               </option>
                                                            </select>
                                                             @if ($errors->has('life_assurance')) <p class="help-block" style="color: red">{{ $errors->first('life_assurance') }}</p> @endif
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-3">
                                                      <div class="form-group">
                                                         <label class="control-label mb-10"
                                                            for="exampleInputuname_1">Consolidated Relief
                                                         Allowance in Payroll?</label>
                                                         <div class="input-group">
                                                            <select required="" class="form-control selectpicker"
                                                               data-show-subtext="true"
                                                               data-live-search="true" name="cra">
                                                               <option @if($renumeration->cra == "1") selected @endif data-subtext="Yes"
                                                                  value="1">Yes
                                                               </option>
                                                               <option @if($renumeration->cra == "0") selected @endif data-subtext="No"
                                                                  value="0">No
                                                               </option>
                                                            </select>
                                                             @if ($errors->has('cra')) <p class="help-block" style="color: red">{{ $errors->first('cra') }}</p> @endif
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="row">
                                                   <div class="col-md-3">
                                                      <div class="form-group">
                                                         <label class="control-label mb-10"
                                                            for="exampleInputuname_1">Compute Pension in Payroll
                                                         ?</label>
                                                         <div class="input-group">
                                                            <select required="" class="form-control selectpicker"
                                                               data-show-subtext="true"
                                                               data-live-search="true" name="pension">
                                                               <option @if($renumeration->pension == "1") selected @endif data-subtext="Yes"
                                                                  value="1">Yes
                                                               </option>
                                                               <option @if($renumeration->pension == "0") selected @endif data-subtext="No"
                                                                  value="0">No
                                                               </option>
                                                            </select>
                                                             @if ($errors->has('pension')) <p class="help-block" style="color: red">{{ $errors->first('pension') }}</p> @endif
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-3">
                                                      <label class="control-label mb-10"
                                                         for="exampleInputuname_1">Does Employee have Any
                                                      Disability ?</label>
                                                      <div class="input-group">
                                                         <select required="" class="form-control selectpicker"
                                                            data-show-subtext="true"
                                                            data-live-search="true" name="disability">
                                                            <option @if($renumeration->disability == "1") selected @endif data-subtext="Yes"
                                                               value="1">Yes
                                                            </option>
                                                            <option @if($renumeration->disability == "0") selected @endif data-subtext="No"
                                                               value="0">No
                                                            </option>
                                                         </select>
                                                          @if ($errors->has('disability')) <p class="help-block" style="color: red">{{ $errors->first('disability') }}</p> @endif
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
               
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-15">
                                        <input class="btn btn-purple btn-anim" type="submit" value="Update Employee Renumeration">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @endsection