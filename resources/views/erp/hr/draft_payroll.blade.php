@extends('erp.layouts.master')
  
  @section('title')
    Human Resource - Draft Payroll
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')

      <section class="content-header">
         <h1>
            Generate Payroll
         </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Generate Payroll</li>
         </ol>
      </section>
      <section class="content">
            <div class="col-md-12">
         <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
               <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Generate Payroll
                  </b></a>
               </li>
            </ul>
            <div class="tab-content" style="padding: 2%">
               <div class="tab-pane active" id="tab_1">
                  <p align="right">
                  <a href="{{url('generate_payroll')}}" class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Generate Payroll
                     </a>
                  </p>
                  <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                     <div class="col-md-1 hidden-sm hidden-xs"></div>
                     <div class="col-md-1 hidden-sm hidden-xs"></div>
                  </div>
                  <hr/ style="clear: both">
                  <div class="box-body">
                     <table id="example1" class="table table-hover table-hover table-bordered" >
                         <thead class="panel panel-heading">
                         <tr>
                             <th>Month</th>
                             <th>Year</th>
                             <th>Actions</th>
                         </tr>
                         </thead>
                         <tbody>
                         <?php foreach($payrolls as $bonus){ ?>
                         <tr>
                             <td>{{ $bonus->month }}</td>
                             <td>{{ $bonus->year }}</td>
                             <td>
                                 <a href="{{ url('export_payroll/'.$bonus->month.'/'.$bonus->year.'/draft'.'/'.$bonus->subsidiary_id.'/'.auth()->user()->company->uri) }}"><h6 class="btn btn-danger">Download to Excel</h6></a>
                                 <a href="{{ url('confirm_payroll/'.$bonus->month.'/'.$bonus->year.'/'.$bonus->subsidiary_id.'/'.auth()->user()->company->uri) }}" id="a_del"><h6 class="btn btn-success">Confirm {{ $bonus->month  }}, {{ $bonus->year }} Payroll</h6></a>
                             </td>
                         </tr>
                         <?php } ?>
                         </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <!-- /.tab-content -->
         </div>
         <!-- nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </section>

   @endsection

@section('script')
   <script>
     $("#example1").DataTable();
     $('.adf').hide();
     $('.cdb').on('click', function(){
       $('.adf').slideToggle();
    
     });

       $('.cadfxx').on('click', function(){
          $('.adf').slideToggle();
       });


   </script>
@endsection