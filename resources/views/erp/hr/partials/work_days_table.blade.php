<table id="example1" class="table table-hover table-hover" >
    <thead class="panel panel-heading">
    <tr>
        <th>Month</th>
        <th>Year</th>
        <th>No of Days</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($workdays as $bonus){ ?>
    <tr>
        <td>{{ $bonus->month }}</td>
        <td>{{ $bonus->year }}</td>
        <td>{{ $bonus->days }} 
        </td>
        <td><a class="btn btn-danger" href="{{ url('delete_record/'.$bonus->id.'/days') }}" id="a_del">Delete</a></td>
    </tr>
    <?php } ?>
    </tbody>
</table>