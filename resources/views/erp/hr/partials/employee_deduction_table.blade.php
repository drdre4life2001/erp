<table id="example1" class="table table-hover table-bordered" >
    <thead class="panel panel-heading">
    <tr>
        <th>Employee Name</th>
        <th>Description</th>
        <th>Amount</th>
        <th>Month</th>
        <th>Year</th>
        <th>Date</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($deductions as $bonus){ ?>
    <tr>
        <td>{{ $bonus->first_name }} {{ $bonus->lastname }}</td>
        <td>{{ $bonus->description }}</td>
        <td>&#x20A6;{{ number_format($bonus->amount, 2) }}</td>
        <td>{{ $bonus->month }}</td>
        <td>{{ $bonus->year }}</td>
        <td>{{ $bonus->created_at }}</td>
        <td>
            <a class="btn btn-danger" id="a_del" href="{{ url('delete_record/'.$bonus->id.'/deduction') }}">Delete</a>
        </td>
    </tr>
    <?php } ?>
    </tbody>
</table>