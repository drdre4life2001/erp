<table id="example1" class="table table-bordered table-hover">
    <thead class="panel panel-heading">
    <tr>
        <th>Employee Name</th>
        <th>Days Absent</th>
        <th>Month</th>
        <th>Year</th>
        <th>Date</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($deductions as $bonus){ ?>
    <tr>
        <td>{{ $bonus->first_name }} {{ $bonus->lastname }}</td>
        <td>{{ number_format($bonus->days_absent) }}</td>
        <td>{{ $bonus->month }}</td>
        <td>{{ $bonus->year }}</td>
        <td>{{ $bonus->created_at }}</td>
        <td><a class="btn btn-danger" id="a_del" href="{{ url('delete_record/'.$bonus->id.'/absence') }}">Delete</a></td>
    </tr>
    <?php } ?>
    </tbody>
</table>