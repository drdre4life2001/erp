<form action="{{url('employee_loans')}}" method="post">
    <div class="form-group">
        <label class="control-label mb-10">Employee</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="icon-user"></i></div>
            <select class="form-control employee_search" style="width:100%" name="employee_id">
            </select>
            @if ($errors->has('employee_id')) <p class="help-block" style="color: red"> {{ $errors->first('employee_id') }}</p> @endif

        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10" for="exampleInputuname_1">Amount</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-money"></i></div>
            <input type="number" class="form-control" id="exampleInputuname_1"
                   placeholder="No of Days Employee was absent" name="amount">
                                    @if ($errors->has('amount')) <p class="help-block" style="color: red"> {{ $errors->first('amount') }}</p> @endif

        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10" for="exampleInputuname_1">Tenure</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-money"></i></div>
            <input type="number" class="form-control" id="exampleInputuname_1"
                   placeholder="Period (in Months) which the Loan will be paid" name="tenure">
                                    @if ($errors->has('tenure')) <p class="help-block" style="color: red"> {{ $errors->first('tenure') }}</p> @endif

        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10" for="exampleInputuname_1">Interest</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-money"></i></div>
            <input type="number" class="form-control" id="exampleInputuname_1"
                   placeholder="Loan Interest: Enter 0 if None" name="interest">
                                    @if ($errors->has('interest')) <p class="help-block" style="color: red"> {{ $errors->first('interest') }}</p> @endif

        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10" for="exampleInputuname_1">Description</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-book"></i></div>
            <textarea class="form-control" name="description">
            </textarea>
                                    @if ($errors->has('description')) <p class="help-block" style="color: red"> {{ $errors->first('description') }}</p> @endif

        </div>
    </div>
    <div class="col-lg-4 col-lg-offset-4">
        <div class="form-group">
                <div class="input-group">
                    <center>
                            <input class="btn btn-success btn-anim" type="submit" value="Add Loan for This Employee">
                    </center>
        
                </div>
        </div>
    </div>
</form>


