<form action="{{ url('add_leave_type') }}" method="post">
    <div class="form-group">
        <label class="control-label mb-10" for="exampleInputuname_1">Leave Title</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-pencil"></i></div>
            <input required="" type="text" class="form-control" id="exampleInputuname_1"
                   name="name">
                                    @if ($errors->has('name')) <p class="help-block" style="color: red"> {{ $errors->first('name') }}</p> @endif

        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10" for="exampleInputuname_1">No of Days Off</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-user"></i></div>
            <input required="" type="text" class="form-control" id="exampleInputuname_1"
                   name="days">
                                    @if ($errors->has('days')) <p class="help-block" style="color: red"> {{ $errors->first('days') }}</p> @endif
        </div>
    </div>
    <div class="form-group">
        <div class="input-group mb-15">
            <input class="btn btn-success btn-anim" type="submit" value="Add Leave Type">
        </div>
    </div>
</form>