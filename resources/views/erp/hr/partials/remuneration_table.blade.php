<table id="example1" class="table table-bordered table-hover" >
    <thead class="">
    <tr>
        <th>S/N</th>
        <th class="">Employee Name</th>
        <th>Basic Salary</th>
        <th>Housing Allowance</th>
        <th>Transport Allowance</th>
        <th>Entertainment Allowance</th>
        <th>Leave Allowance</th>
        <th>Utility Allowance</th>
        <th>N.H.F</th>
        <th>Life Assurance</th>
        <th>Consolidated Relief Allowance</th>
        <th>Fixed CRA</th>
        <th>Dependable Relatives</th>
        <th>Children</th>
        <th>Disability</th>
        <th>Date Added</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php $i = 1;?>
    <?php foreach($renumerations as $bonus){ ?>
    <tr>
        <td>{{ $i++ }}</td>
        <td>{{ $bonus->first_name }} {{ $bonus->lastname }}</td>
        <td>&#x20A6;{{ number_format($bonus->basic_pay,2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->housing, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->transport, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->entertainment, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->leave_allowance, 2) }}</td>
        <td>&#x20A6;{{ number_format($bonus->utility, 2) }}</td>
        <td>{{ $bonus->nhf == 1 ? 'Yes' : 'No' }}</td>
        <td>{{ $bonus->life_assurance == 1 ? 'Yes' : 'No' }}</td>
        <td>{{ $bonus->cra == 1 ? 'Yes' : 'No' }}</td>
        <td>{{ $bonus->fixed_cra == 1 ? 'Yes' : 'No' }}</td>
        <td>{{ $bonus->children }}</td>
        <td>{{ $bonus->dependable_relative }}</td>
        <td>{{ $bonus->disability }}</td>
        <td>{{ $bonus->created_at }}</td>
        <td><a href="{{ url('update_employee_renumeration/'.$bonus->id."/".auth()->user()->company->uri) }}"><button> <i class="fa fa-circle-o text-red"></i> Update</button></a></td>
    </tr>
    <?php } ?>
    </tbody>
</table>