<table id="example1" class="table table-bordered table-hover" >
    <thead class="panel panel-heading">
    <tr>
        <th>Employee</th>
        <th>Leave Type</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>No of Days</th>
        <th>Description</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @if(!empty($leaves))
            <?php foreach($leaves as $bonus){ ?>
            <tr>
                <td>{{ $bonus->first_name }} {{ $bonus->lastname }}</td>
                <td>{{ $bonus->type }}</td>
                <td>{{ date("M d, Y",strtotime($bonus->start)) }}</td>
                <td>{{ date("M d, Y",strtotime($bonus->end)) }}</td>
                <td>{{ $bonus->days }}</td>
                <td>{{ $bonus->description }}</td>
                <td>
                    @if($bonus->status == 1)
                        Active
                    @endif
                </td>
                <td><a href="{{ url('delete_record/'.$bonus->id.'/employee-leave') }}" id="a_del" class="btn btn-danger">Delete</a></td>
            </tr>
            <?php } ?>
        @endif
    </tbody>
</table>