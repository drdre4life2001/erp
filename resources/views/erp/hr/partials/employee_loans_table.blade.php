<table id="example1" class="table table-hover table-responsive" >
    <thead class="panel panel-heading">
    <tr>
        <th>Employee Name</th>
        <th>Amount</th>
        <th>Description</th>
        <th>Tenure</th>
        <th>Interest</th>
        <th>Total Payable</th>
        <th>Date</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
        @if(!empty($loans))
            <?php foreach($loans as $bonus){ ?>
            <tr>
                <td>{{ $bonus->first_name }} {{ $bonus->lastname }}</td>
                <td>{{ number_format($bonus->amount, 2) }}</td>
                <td>{{ $bonus->description }}</td>
                <td>{{ $bonus->tenure }}</td>
                <td>{{ $bonus->interest }}</td>
                <td>{{ $bonus->total_payable }}</td>
                <td>{{ $bonus->created_at }}</td>
                <td><a id="a_del" class="btn btn-danger" href="{{ url('delete_record/'.$bonus->id.'/loan') }}">Delete</a></td>
            </tr>
            <?php } ?>
        @endif
    </tbody>
</table>
