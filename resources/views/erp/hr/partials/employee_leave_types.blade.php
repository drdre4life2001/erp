<table id="example1" class="table table-hover" >
    <thead class="panel panel-heading">
    <tr>
        <th>Leave Title</th>
        <th>No of Days</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($leave_types as $bonus){ ?>
    <tr>
        <td>{{ $bonus->name }}</td>
        <td>{{ $bonus->days }}</td>
        <td><a href="{{ url('delete_record/'.$bonus->id.'/leave') }}"><button class="btn btn-danger" onclick="return confirm('Are you sure? This process is not reversible!')">Delete</button></a></td>
    </tr>
    <?php } ?>
    </tbody>
</table>