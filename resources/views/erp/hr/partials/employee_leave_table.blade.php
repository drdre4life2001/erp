<table id="example1" class="table table-bordered table-hover" >
    <thead class="panel panel-heading">
    <tr>
        <th>Employee</th>
        <th>Leave Type</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>No of Days</th>
        <th>Description</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($leaves as $bonus){ ?>
    <tr>
        <td>{{ $bonus->first_name }} {{ $bonus->lastname }}</td>
        <td>{{ $bonus->type }}</td>
        <td>{{ $bonus->start }}</td>
        <td>{{ $bonus->end }}</td>
        <td>{{ $bonus->days }}</td>
        <td>{{ $bonus->description }}</td>
        <td><a href="{{ url('delete_record/'.$bonus->id.'/leave') }}"><button onclick="return confirm('Are you sure? This process is not reversible!')">Delete</button></a></td>
    </tr>
    <?php } ?>
    </tbody>
</table>