<div class="row">
   <form action="{{ url('add_employee_renumeration') }}" method="post">
      <div class="col-md-6">
         <div class="panel card-view">
            <div class="panel-wrapper collapse in">
               <div class="panel-body">
                  <div class="form-wrap mt-40">
                     <div class="form-group">
                        <label class="control-label mb-10">Employee</label>
                        <div class="input-group">
                           <div class="input-group-addon"><i class="icon-user"></i></div>
                           <select required="" class="form-control selectpicker employee_search" style="width: 100%;" data-show-subtext="true"
                                   name="employee_id">
                           </select>
                            @if ($errors->has('employee_id')) <p class="help-block" style="color: red">{{ $errors->first('employee_id') }}</p> @endif
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label mb-10" for="exampleInputuname_1">Basic Salary</label>
                        <div class="input-group">
                           <div class="input-group-addon"><i class="fa fa-money"></i></div>
                           <input type="number" required="" class="form-control" id="exampleInputuname_1"
                              placeholder="Basic Salary (Gross)" name="basic_pay">
                               @if ($errors->has('basic_pay')) <p class="help-block" style="color: red">{{ $errors->first('basic_pay') }}</p> @endif
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label mb-10" for="exampleInputuname_1">Housing Allowance</label>
                        <div class="input-group">
                           <div class="input-group-addon"><i class="fa fa-money"></i></div>
                           <input type="number" required="" class="form-control" id="exampleInputuname_1"
                              placeholder="Housing Allowance (Gross)" name="housing">
                               @if ($errors->has('housing')) <p class="help-block" style="color: red">{{ $errors->first('housing') }}</p> @endif
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label mb-10" for="exampleInputuname_1">Transport Allowance</label>
                        <div class="input-group">
                           <div class="input-group-addon"><i class="fa fa-money"></i></div>
                           <input type="number" required="" class="form-control" id="exampleInputuname_1"
                              placeholder="Transport Allowance (Gross)" name="transport">
                               @if ($errors->has('transport')) <p class="help-block" style="color: red">{{ $errors->first('transport') }}</p> @endif
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label mb-10" for="exampleInputuname_1">Leave Allowance</label>
                        <div class="input-group">
                           <div class="input-group-addon"><i class="fa fa-money"></i></div>
                           <input type="number" required="" class="form-control" id="exampleInputuname_1"
                              placeholder="Leave Allowance" name="leave">
                               @if ($errors->has('leave')) <p class="help-block" style="color: red">{{ $errors->first('leave') }}</p> @endif
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label mb-10" for="exampleInputuname_1">Utility Allowance</label>
                        <div class="input-group">
                           <div class="input-group-addon"><i class="fa fa-money"></i></div>
                           <input type="number" required="" class="form-control" id="exampleInputuname_1"
                              placeholder="Utility Allowance" name="utility">
                               @if ($errors->has('utility')) <p class="help-block" style="color: red">{{ $errors->first('utility') }}</p> @endif
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label mb-10" for="exampleInputuname_1">Meal Allowance</label>
                        <div class="input-group">
                           <div class="input-group-addon"><i class="fa fa-money"></i></div>
                           <input type="number" required="" class="form-control" id="exampleInputuname_1"
                              placeholder="Meal Allowance" name="meal">
                               @if ($errors->has('meal')) <p class="help-block" style="color: red">{{ $errors->first('meal') }}</p> @endif
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label mb-10" for="exampleInputuname_1">Others</label>
                        <div class="input-group">
                           <div class="input-group-addon"><i class="fa fa-money"></i></div>
                           <input type="number" required="" class="form-control" id="exampleInputuname_1"
                              placeholder="Others" name="others">
                               @if ($errors->has('others')) <p class="help-block" style="color: red">{{ $errors->first('others') }}</p> @endif
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6">
         <div class="panel panel-default card-view">
            <div class="panel-wrapper collapse in">
               <div class="panel-body">
                  <div class="form-wrap mt-40">
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="mt-25">
                              <div class="form-group">
                                 <label class="control-label mb-10" for="exampleInputuname_1">Entertainment
                                 Allowance</label>
                                 <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                    <input type="number" required="" class="form-control" id="exampleInputuname_1"
                                       placeholder="Meal Allowance" name="entertainment">
                                        @if ($errors->has('entertainment')) <p class="help-block" style="color: red">{{ $errors->first('entertainment') }}</p> @endif
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="control-label mb-10" for="exampleInputuname_1">Number of
                                 Dependent Relatives</label>
                                 <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                    <input type="number" required="" class="form-control" id="exampleInputuname_1"
                                       placeholder="Number of Dependent Relatives"
                                       name="dependable_relative">
                                        @if ($errors->has('dependable_relative')) <p class="help-block" style="color: red">{{ $errors->first('dependable_relative') }}</p> @endif
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="control-label mb-10" for="exampleInputuname_1">Number of
                                 Children</label>
                                 <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                    <input type="number" required="" class="form-control" id="exampleInputuname_1"
                                       placeholder="Number of Children" name="children">
                                        @if ($errors->has('children')) <p class="help-block" style="color: red">{{ $errors->first('children') }}</p> @endif
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="row">
                                    <div class="col-md-3">
                                       <label class="control-label mb-10"
                                          for="exampleInputuname_1">Compute National Housing Fund
                                       in Payroll?</label>
                                       <div class="input-group">
                                          <select required="" class="form-control selectpicker"
                                             data-show-subtext="true"
                                             data-live-search="true" name="nhf">
                                             <option data-subtext="Yes"
                                                value="1">Yes
                                             </option>
                                             <option data-subtext="No"
                                                value="0">No
                                             </option>
                                          </select>
                                           @if ($errors->has('nhf')) <p class="help-block" style="color: red">{{ $errors->first('nhf') }}</p> @endif
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                       <div class="form-group">
                                          <label class="control-label mb-10"
                                             for="exampleInputuname_1">Compute Life Assurance in
                                          Payroll?</label>
                                          <div class="input-group">
                                             <select required="" class="form-control selectpicker"
                                                data-show-subtext="true"
                                                data-live-search="true" name="life_assurance">
                                                <option data-subtext="Yes"
                                                   value="1">Yes
                                                </option>
                                                <option data-subtext="No"
                                                   value="0">No
                                                </option>
                                             </select>
                                              @if ($errors->has('life_assurance')) <p class="help-block" style="color: red">{{ $errors->first('life_assurance') }}</p> @endif
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                       <div class="form-group">
                                          <label class="control-label mb-10"
                                             for="exampleInputuname_1">Consolidated Relief
                                          Allowance in Payroll?</label>
                                          <div class="input-group">
                                             <select required="" class="form-control selectpicker"
                                                data-show-subtext="true"
                                                data-live-search="true" name="cra">
                                                <option data-subtext="Yes"
                                                   value="1">Yes
                                                </option>
                                                <option data-subtext="No"
                                                   value="0">No
                                                </option>
                                             </select>
                                              @if ($errors->has('cra')) <p class="help-block" style="color: red">{{ $errors->first('cra') }}</p> @endif
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="row">
                                    <div class="col-md-3">
                                       <div class="form-group">
                                          <label class="control-label mb-10"
                                             for="exampleInputuname_1">Compute Pension in Payroll
                                          ?</label>
                                          <div class="input-group">
                                             <select required="" class="form-control selectpicker"
                                                data-show-subtext="true"
                                                data-live-search="true" name="pension">
                                                <option data-subtext="Yes"
                                                   value="1">Yes
                                                </option>
                                                <option data-subtext="No"
                                                   value="0">No
                                                </option>
                                             </select>
                                              @if ($errors->has('pension')) <p class="help-block" style="color: red">{{ $errors->first('pension') }}</p> @endif
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                       <label class="control-label mb-10"
                                          for="exampleInputuname_1">Does Employee have Any
                                       Disability ?</label>
                                       <div class="input-group">
                                          <select required="" class="form-control selectpicker"
                                             data-show-subtext="true"
                                             data-live-search="true" name="disability">
                                             <option data-subtext="Yes"
                                                value="1">Yes
                                             </option>
                                             <option data-subtext="No"
                                                value="0">No
                                             </option>
                                          </select>
                                           @if ($errors->has('disability')) <p class="help-block" style="color: red">{{ $errors->first('disability') }}</p> @endif
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="input-group mb-15">
                           <input class="btn btn-success btn-anim" type="submit"
                              value="Submit Employee Renumeration">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </form>
</div>