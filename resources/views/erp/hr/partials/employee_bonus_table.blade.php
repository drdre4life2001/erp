<table id="example1" class="table table-bordered table-hover">
    <thead class="panel panel-heading">
    <tr>
        <th>Employee Name</th>
        <th>Amount</th>
        <th>Description</th>
        <th>Month</th>
        <th>Year</th>
        <th>Date</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($bonuses as $bonus){ ?>
    <tr>
        <td>{{ $bonus->first_name }} {{ $bonus->lastname }}</td>
        <td>&#x20A6;{{ number_format($bonus->amount, 2) }}</td>
        <td>{{ $bonus->description }}</td>
        <td>{{ $bonus->month }}</td>
        <td>{{ $bonus->year }}</td>
        <td>{{ $bonus->created_at }}</td>
        <td><a id="a_del" class="btn btn-danger" href="{{ url('delete_record/'.$bonus->id.'/bonus') }}">Delete </a></td>
    </tr>
    <?php } ?>
    </tbody>
</table>