<table id="example1" class="table table-bordered table-hover">
   <thead>
      <tr style="color:#8b8b8b">
         <th>ID No</th>
         {{-- <th>Image</th> --}}
         <th>Line Manager</th>
         <th>Code</th>
         <th>Action</th>
      </tr>
   </thead>
   <tbody>
      @if(!empty($departments))
         <?php $i = 1; ?>
         @foreach($departments as $department)
            <tr>
               <td> {{$i++}} </td>
               {{-- <td>
                  <img class="img img-responsive" @if(isset($department->picture)) src="{{asset($department->picture)}}" @else src="{{asset('uploads/employee_pictures/user.jpg')}}" @endif width="10%">

               </td> --}}
               <td style="color:#333;">{{$department->line_manager_name}}</td>
               <td>{{$department->code}}</td>
               <td>
                  <a href="#">
                     <span class="label label-success " style="padding:9.2%; margin-top: 4% !important; float: left; margin-left:1%"><i class="fa fa-eye"></i>  View</span></a>
               </td>
            </tr>
         @endforeach
      @endif
      </tfoot>
</table>