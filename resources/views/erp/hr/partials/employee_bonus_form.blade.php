<form action="{{ url('add_employee_bonus') }}" method="post">
    <div class="form-group">
        <label class="control-label mb-10">Employee</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="icon-user"></i></div>
            <select required="" class="form-control selectpicker employee_search" style="width: 100%;" data-show-subtext="true"
                    name="employee_id">
            </select>
            @if ($errors->has('employee_id')) <p class="help-block" style="color: red"> {{ $errors->first('employee_id') }}</p> @endif

        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10" for="exampleInputuname_1">Amount</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-money"></i></div>
            <input required="" type="number" class="form-control" id="exampleInputuname_1"
                   placeholder="Amount" name="amount">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10" for="exampleInputuname_1">Description</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-book"></i></div>
            <textarea required="" class="form-control" name="description">
            </textarea>
            @if ($errors->has('description')) <p class="help-block" style="color: red"> {{ $errors->first('description') }}</p> @endif

        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10" for="exampleInputuname_1">Month</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <select required="" class="form-control selectpicker" data-show-subtext="true"
                    data-live-search="true" name="month">
                @foreach($months as $month)
                    <option data-subtext="{{ $month }}"
                            value="{{ $month }}">{{ $month }}</option>
                @endforeach
            </select>
            @if ($errors->has('month')) <p class="help-block" style="color: red"> {{ $errors->first('month') }}</p> @endif

        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10" for="exampleInputuname_1">Year</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <select required="" class="form-control selectpicker" data-show-subtext="true"
                    data-live-search="true" name="year">
                @foreach($years as $month)
                    <option data-subtext="{{ $month }}"
                            value="{{ $month }}">{{ $month }}</option>
                @endforeach
            </select>
            @if ($errors->has('year')) <p class="help-block" style="color: red"> {{ $errors->first('year') }}</p> @endif

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="">
                <div class="form-group">
                    <label class="control-label mb-10"
                           for="exampleInputuname_1">Is This Bonus Taxable?</label>
                    <div class="input-group">
                        <ul class=" nicescroll-bar">
                            <li class="">
                                <div class="checkbox checkbox-default">
                                    <input required="" type="checkbox" id="checkbox01" name="taxable"/>
                                    @if ($errors->has('taxable')) <p class="help-block" style="color: red"> {{ $errors->first('taxable') }}</p> @endif
                                    <label for="checkbox01">Yes</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="input-group mb-15">
            <input class="btn btn-success btn-anim" type="submit" value="Add Bonus">

        </div>
    </div>
</form>