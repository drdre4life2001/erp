<form action="" method="post">
    <div class="form-group">
        <label class="control-label mb-10" for="exampleInputuname_1">Month</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <select required="" class="form-control selectpicker" data-show-subtext="true"
                    data-live-search="true" name="month">
                @foreach($months as $month)
                    <option data-subtext="{{ $month }}"
                            value="{{ $month }}">{{ $month }}</option>
                @endforeach
            </select>
                                    @if ($errors->has('month')) <p class="help-block" style="color: red"> {{ $errors->first('month') }}</p> @endif

        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10" for="exampleInputuname_1">Year</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <select required="" class="form-control selectpicker" data-show-subtext="true"
                    data-live-search="true" name="year">
                @foreach($years as $month)
                    <option data-subtext="{{ $month }}"
                            value="{{ $month }}">{{ $month }}</option>
                @endforeach
            </select>
                                    @if ($errors->has('year')) <p class="help-block" style="color: red"> {{ $errors->first('year') }}</p> @endif

        </div>
    </div>
    <div class="form-group">
        <label class="control-label mb-10" for="exampleInputuname_1">No of Work Days</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-money"></i></div>
            <input required="" type="number" class="form-control" id="exampleInputuname_1"
                   name="days">
                                    @if ($errors->has('days')) <p class="help-block" style="color: red"> {{ $errors->first('days') }}</p> @endif

        </div>
    </div>
    <div class="form-group">
        <div class="input-group mb-15">
            <input class="btn btn-success btn-anim" type="submit" value="Add Work Day">


        </div>
    </div>
</form>