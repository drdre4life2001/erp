@extends('erp.layouts.master')
  
  @section('title')
    Human Resource - Generate Draft Payroll
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  	<section class="content-header">
  	   <h1>
  	      Generate Payroll
  	   </h1>
  	   <ol class="breadcrumb">
  	      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  	      <li class="active"> Generate Payroll </li>
  	   </ol>
  	</section>
  	<section class="content">
  	      <div class="col-md-12">
  	   <div class="nav-tabs-custom">
  	      <ul class="nav nav-tabs">
  	         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Generate Payroll
  	            </b></a>
  	         </li>
  	      </ul>
  	      <div class="tab-content" style="padding: 2%">
  	         <div class="tab-pane active" id="tab_1">
  	            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
  	               <div class="col-md-1 hidden-sm hidden-xs"></div>
  	               <div class="col-md-10">
  	                  <form action="{{ url('generate_payroll') }}" method="post">
  	                      <div class="form-group">
  	                          <label class="control-label mb-10">Month</label>
  	                          <div class="input-group">
  	                              <div class="input-group-addon"><i class="icon-user"></i></div>
  	                              <select required="" class="form-control selectpicker" data-show-subtext="true"
  	                                      data-live-search="true" name="month">
  	                                  @foreach($months as $month)
  	                                      <option data-subtext="{{ $month }}"
  	                                              value="{{ $month }}">{{ $month }}</option>
  	                                  @endforeach
  	                              </select>
  	                              @if ($errors->has('month')) <p class="help-block" style="color: red">{{ $errors->first('month') }}</p> @endif
  	                          </div>
  	                      </div>
  	                      <div class="form-group">
  	                          <label class="control-label mb-10">Year</label>
  	                          <div class="input-group">
  	                              <div class="input-group-addon"><i class="icon-user"></i></div>
  	                              <select required="" class="form-control selectpicker" data-show-subtext="true"
  	                                      data-live-search="true" name="year">
  	                                  @foreach($years as $month)
  	                                      <option data-subtext="{{ $month }}"
  	                                              value="{{ $month }}">{{ $month }}</option>
  	                                  @endforeach
  	                              </select>
  	                              @if ($errors->has('year')) <p class="help-block" style="color: red">{{ $errors->first('year') }}</p> @endif
  	                          </div>
  	                      </div>
  	                      <div class="form-group">
  	                          <label class="control-label mb-10">Select Company</label>
  	                          <div class="input-group">
  	                              <div class="input-group-addon"><i class="icon-user"></i></div>
  	                              <select required="" class="form-control organization_search" data-show-subtext="true"
  	                                      data-live-search="true" name="id_hr_company">
  	                              </select>
  	                              @if ($errors->has('id_hr_company')) <p class="help-block" style="color: red">{{ $errors->first('id_hr_company') }}</p> @endif
  	                          </div>
  	                      </div>
  	                      <div class="form-group">
  	                          <div class="input-group mb-15">
  	                              <input class="btn btn-success btn-anim" type="submit" value="Generate Payroll for This Period">

  	                          </div>
  	                      </div>
  	                  </form>
  	               </div>
  	               <div class="col-md-1 hidden-sm hidden-xs"></div>
  	            </div>
  	            <hr/ style="clear: both">
  	            <div class="box-body">
  	            </div>
  	         </div>
  	      </div>
  	   </div>
  	   </div>
  	</section>
  @endsection

  @section('script')
   <script>
     $("#example1").DataTable();
     $('.adf').hide();
	   $('.adf').slideToggle();

	   $('.cadfxx').on('click', function(){
	      $('.adf').slideToggle();
	   });


   </script>
@endsection