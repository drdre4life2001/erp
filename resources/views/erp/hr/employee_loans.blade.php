@extends('erp.layouts.master')
  
  @section('title')
    Human Resource - Employee Loan
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

   @section('content')
      <section class="content-header">
         <h1>
            Employee Loan
         </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Employee Loan</li>
         </ol>
      </section>
      <section class="content">
         <div class="col-md-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Employee Loan
                     </b></a>
                  </li>
               </ul>
               <div class="tab-content" style="padding: 2%">
                  <div class="tab-pane active" id="tab_1">
                     <p align="right">
                        <button class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add  Employee Loan
                        </button>
                     </p>
                     <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                           @include('erp.hr.partials.employee_loan_form')
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                     </div>
                     <hr/ style="clear: both">
                     <div class="box-body">
                        <div class="col-lg-12">
                           @include('erp.hr.partials.employee_loans_table')
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
@endsection

@section('script')
 <script type="text/javascript">
     $("#example1").DataTable();

   $('.adf').hide();
   $('.cdb').on('click', function(){
     $('.adf').slideToggle();
   });
   @if ($errors->has('employee_id') || $errors->has('amount') || $errors->has('interest') || $errors->has('tenure') || $errors->has('description'))
     $('.adf').slideToggle();
   @endif

     $('.cadfxx').on('click', function(){
        $('.adf').slideToggle();
     });
 </script>
@endsection
