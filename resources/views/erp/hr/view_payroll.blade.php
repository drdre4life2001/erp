@extends('erp.layouts.master')
  @section('title')
    Human Resources - Payroll
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
   <section class="content-header">
      <h1> Payroll
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active"> Payroll </li>
      </ol>
   </section>
   <section class="content">
      <div class="col-md-12">
         <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
               <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Payroll  </b></a></li>
            </ul>
            <div class="tab-content" style="padding: 2%">
           <div class="tab-pane active" id="tab_1">
              <p align="right">
              </p>
              <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                 <div class="col-md-1 hidden-sm hidden-xs"></div>
                 <div class="col-md-10">
                 </div>
                 <div class="col-md-1 hidden-sm hidden-xs"></div>
              </div>
              <hr/>
              {{--<div class="box-body">--}}
              
               <div class="table-wrap">
                  <div class="table-responsive">
                      @include('hr.elements.payrolls_data')
                  </div>
              </div>
              {{--</div>--}}
           </div>
        </div>
        </div>
      </div>
   </section>
  @endsection

  @section('script')
   <script>
       $("#example1").DataTable();
     $('.adf').hide();
     $('.cdb').on('click', function(){
       $('.adf').slideToggle();
     });
       $('.cadfxx').on('click', function(){
          $('.adf').slideToggle();
       });
   </script>
@endsection