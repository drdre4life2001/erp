@extends('erp.layouts.master')
  
  @section('title')
    Human Resource - Employee Department
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  	<section class="content-header">
  	   <h1>
  	      Departments
  	   </h1>
  	   <ol class="breadcrumb">
  	      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  	      <li class="active">Department</li>
  	   </ol>
  	</section>
  	<section class="content">
  	    <div class="col-md-12">
	  	   <div class="nav-tabs-custom">
	  	      <ul class="nav nav-tabs">
	  	         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>Department Table</b></a></li>
	  	      </ul>
	  	      <div class="tab-content" style="padding: 2%">
	  	         <div class="tab-pane active" id="tab_1">
	  	            <p align="right">
	  	               <button class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add Department</button>
	  	            </p>
	  	            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
	  	               <div class="col-md-2 hidden-sm hidden-xs"></div>
	  	               <div class="col-md-8">
	  	                  <form method="post" action="">
	  	                     <h3 align="center">Create Department</h3>
	  	                     <div class="form-group">
	  	                        <input type="text" name="name" class="form-control" placeholder="Department Name">
	  	                        @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
	  	                     </div>
	  	                     <div class="col-md-6" style="padding: 0px;">
	  	                        <div class="form-group">
	  	                           <label>Department Code Number</label>
	  	                           <input type="text" name="code" class="form-control" placeholder="Department Code">
	  	                           @if ($errors->has('code')) <p class="help-block" style="color: red">{{ $errors->first('code') }}</p> @endif
	  	                        </div>
	  	                        <div class="form-group">
	  	                           <label>Manager /  Head of Department</label>
	  	                           <select name="id_hr_employee" class="form-control" va
	  	                           >
									   <option value="">Select One</option>
								   		@if(isset($employees))
		  	                           	  @foreach($employees as $employee)
			  	                              <option value="{{$employee->id}}">{{$employee->first_name ." ".$employee->lastname}}</option>
		  	                           	  @endforeach
	  	                              @endif
	  	                           </select>
	  	                           @if ($errors->has('id_hr_employee')) <p class="help-block" style="color: red">{{ $errors->first('id_hr_employee') }}</p> @endif
	  	                        </div>
	  	                     </div>
	  	                     <div class="col-md-6" style="padding-right: 0px;">
	  	                        <div class="form-group">
	  	                           <label>Organization</label>
	  	                           <select name="id_sys_organizations" class="form-control" va
	  	                           >
									   <option value="">Select One</option>
								   @if(isset($organizaions))
		  	                           	  @foreach($organizaions as $organization)
			  	                              <option value="{{$organization->id}}">{{$organization->name}}</option>
		  	                           	  @endforeach
	  	                              @endif
	  	                           </select>
                                    @if ($errors->has('id_sys_organizations')) <p class="help-block" style="color: red"> {{ $errors->first('id_sys_organizations') }}</p> @endif
								</div>
									<div class="form-group">
	  	                           <label>Parent Department</label>
	  	                           <select name="parent_id" class="form-control">
									   <option value="">Select One</option>
									   @if(isset($parentDepartments))
										   @foreach($parentDepartments as $department)
											   <option value="{{$department->id}}">{{$department->name}}</option>
										   @endforeach
									   @endif
	  	                           </select>
                                    @if ($errors->has('parent_id')) <p class="help-block" style="color: red"> {{ $errors->first('parent_id') }}</p> @endif
	  	                        </div>
	  	                     </div>
	  	                     <div class="form-group">
	  	                        <input type="text" class="form-control" placeholder="Description">
	  	                     </div>
	  	                     <p align="center">
	  	                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
	  	                        Save</button>
	  	                        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
	  	                        Cancel</a>
	  	                     </p>
							 </div> </form>
	  	               </div>
	  	               <div class="col-md-2 hidden-sm hidden-xs"></div>
	  	            </div>
	  	            <hr style="clear: both"/>
	  	            <div class="box-body">
	  	               @include('erp.hr.partials.employee_department')
	  	            </div>
	  	         </div>
	  	      </div>
	  	   </div>
		</div>
  	   </div>
  	</section>
  @endsection

	@section('script')
		<script>
			// $(function () {
				$("#example1").DataTable(); 
			// });
			$('.adf').hide();
			$('.cdb').on('click', function(){
				$('.adf').slideToggle();
			});

			$('.cadfxx').on('click', function(){
				$('.adf').slideToggle();
			});

		</script>
	@endsection