@extends('erp.layouts.master')
  
  @section('title')
    Human Resource - Employee Deductions
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  	<section class="content-header">
  	   <h1>
  	      Employee Deduction
  	   </h1>
  	   <ol class="breadcrumb">
  	      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  	      <li class="active"> Employee Deduction</li>
  	   </ol>
  	</section>
  	<section class="content">
      <div class="col-md-12">
	  	   <div class="nav-tabs-custom">
	  	      <ul class="nav nav-tabs">
	  	         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Employee Deduction Table</b></a></li>
	  	      </ul>
	  	      <div class="tab-content" style="padding: 2%">
	  	         <div class="tab-pane active" id="tab_1">
	  	            <p align="right">
	  	               <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Create Employee Deduction</button>
	  	            </p>
	  	            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
	  	               <div class="col-md-1 hidden-sm hidden-xs"></div>
	  	               <div class="col-md-10">
	  	                  @include('erp.hr.partials.employee_deduction_form')
	  	               </div>
	  	               <div class="col-md-1 hidden-sm hidden-xs"></div>
	  	            </div>
	  	            <hr/ style="clear: both">
	  	            <div class="box-body">
	  	                  @include('erp.hr.partials.employee_deduction_table')
	  	            </div>
	  	         </div>
	  	      </div>
	  	      <!-- /.tab-content -->
	  	   </div>
  	   </div>
  	   <!-- /.col -->
  	</section>
  @endsection

  @section('script')
	<script>
	  // $(function () {
	  	// alert('helo');
		  $('#example1').DataTable( {
	          dom: 'Bfrtip',
	          buttons: [
	              {
	                  extend: 'print',
	                  exportOptions: {
	                      columns: ':visible'
	                  }
	              }
	          ],
	          columnDefs: [ {
	              visible: true
	          }]
		  });

		  $('.adf').hide();
		  $('.cdb').on('click', function(){
		    $('.adf').slideToggle();
		 
		  });

		    $('.cadfxx').on('click', function(){
		       $('.adf').slideToggle();
		    });
	  // });
	</script>
@endsection