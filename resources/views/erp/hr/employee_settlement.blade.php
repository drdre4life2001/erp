@extends('erp.layouts.master')
  
  @section('title')
    Human Resource - Employee Settlement
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

@section('content')
        <section class="content-header">
   <h1>      Settlements
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Settlements </li>
   </ol>
</section>
<section class="content">
      <div class="col-md-12">
   <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Employee Settlement  </b></a></li>
      </ul>
      <div class="tab-content" style="padding: 2%">
         <div class="tab-pane active" id="tab_1">
            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
               <div class="col-md-1 hidden-sm hidden-xs"></div>
               <div class="col-md-10">
               </div>
               <div class="col-md-1 hidden-sm hidden-xs"></div>
            </div>
            <hr/ style="clear: both">
            <div class="box-body">
               <table id="example1" class="table table-hover table-responsive" >
                   <thead class="panel panel-heading">
                   <tr>
                       <th>Employee</th>
                       <th>Amount</th>
                       <th>Amount to date</th>
                       <th>Total Payable</th>
                       <th>Outstanding Amount</th>
                       <th>Date</th>
                   </tr>
                   </thead>
                   <tbody>
                       @if(!empty($settlements))
                           <?php foreach($settlements as $s){ ?>
                           <tr>
                               <td>{{ $s->first_name }} {{ $bonus->lastname }}</td>
                               <td>{{ number_format($bonus->amount, 2) }}</td>
                               <td>{{ $s->amount }}</td>
                               <td>{{ $s->amount_to_date }}</td>
                               <td>{{ $s->total_payable }}</td>
                               <td>{{ $s->total_outstanding }}</td>
                               <td>{{ $s->created_at }}</td>
                           </tr>
                           <?php } ?>
                       @endif
                   </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   </div>
</section>
@endsection


