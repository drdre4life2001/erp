@extends('erp.layouts.master')
  
  @section('title')
    Human Resource - View History
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  <section class="content-header">
       <h1>
          Send Payslip to Employees
       </h1>
       <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"> Send pay slip</li>
       </ol>
    </section>
    <section class="content">
          <div class="col-md-12">
       <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
             <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Send pay slip
                </b></a>
             </li>
          </ul>
          <div class="tab-content" style="padding: 2%">
             <div class="tab-pane active" id="tab_1">
                <p align="right">
                </p>
                <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                   <div class="col-md-1 hidden-sm hidden-xs"></div>
                   <div class="col-md-10">
                        
                    <div class="col-md-1 hidden-sm hidden-xs"></div>
                     </div>
                     <hr/ style="clear: both">
                     <div class="col-lg-8 col-lg-offset-2">
                        <form action="{{ url('send_payslip') }}" method="post">
                            <div class="form-group">
                                <label class="control-label mb-10" for="exampleInputuname_1">Month</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <select class="form-control selectpicker" data-show-subtext="true"
                                            data-live-search="true" name="month">
                                        @foreach($months as $month)
                                            <option data-subtext="{{ $month }}"
                                                    value="{{ $month }}">{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10" for="exampleInputuname_1">Year</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <select class="form-control selectpicker" data-show-subtext="true"
                                            data-live-search="true" name="year">
                                        @foreach($years as $month)
                                            <option data-subtext="{{ $month }}"
                                                    value="{{ $month }}">{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-15">
                                    <input class="btn btn-success btn-anim form_clicked" type="submit" value="Send Payslip">
                                </div>
                            </div>
                        </form>
                     </div>
                  </div>
               </div>
               <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </section>
  @endsection


@section('script')
   <script>
     $("#example1").DataTable();
     // $('.adf').hide();
       // $('.adf').slideToggle();

       $('.cadfxx').on('click', function(){
          $('.adf').slideToggle();
       });
   </script>
@endsection