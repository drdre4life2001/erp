@extends('erp.layouts.master')
  
  @section('title')
    Human Resource - View Payroll
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
    <section class="content-header">
       <h1>
          View Payroll
       </h1>
       <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"> View Payroll </li>
       </ol>
    </section>
    <section class="content">
          <div class="col-md-12">
       <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
             <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> View Payroll
                </b></a>
             </li>
          </ul>
          <div class="tab-content" style="padding: 2%">
             <div class="tab-pane active" id="tab_1">
                <p align="right">
                   <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  View Payroll
                   </button>
                </p>
                <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                   <div class="col-md-1 hidden-sm hidden-xs"></div>
                   <div class="col-md-10">
                        <div class="">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Draft Pay Roll for {{ $month }}, {{ $year }}</h6>
                            </div>
                            <div class="pull-right">
                                <a href="{{ url('export_payroll/'.$month.'/'.$year.'/'.'draft/'.$payrolls[0]->company_id.'/'.auth()->user()->company->uri) }}"><h6 class="btn btn-success">Download to Excel</h6></a>
                                <a href="{{ url('confirm_payroll/'.$month.'/'.$year.'/'.$company.'/'.auth()->user()->company->uri) }}" onclick="confirm('Are you sure you want to confirm this payroll data?')"><h6 class="btn btn-success">Confirm {{ $month  }}, {{ $year }} Payroll</h6></a>
                            </div>
                            <br> <br>
                            <div class="clearfix"></div>
                        </div>
                        <br> <br>
                        <div class="">
                            <div class="">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        @include('hr.elements.payroll_data')
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="col-md-1 hidden-sm hidden-xs"></div>
                     </div>
                     <hr/ style="clear: both">
                  </div>
               </div>
               <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </section>
  @endsection


@section('script')
    <script>
        $("#example1").DataTable();
        // $('.adf').hide();
        $('.cdb').on('click', function(){
        $('.adf').slideToggle();
    
        });

        $('.cadfxx').on('click', function(){
            $('.adf').slideToggle();
        });
    </script>
@endsection