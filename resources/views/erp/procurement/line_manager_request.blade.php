@extends('erp.layouts.master')
  
  @section('title')
    Procurement - Line manager Request
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

@section('content')
    <section class="content-header">
       <h1>     Request
       </h1>
       <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Pending Request Item</a></li>
          <li class="active">Request</li>
       </ol>
    </section>
    <section class="content">
          <div class="col-md-12"> 
               <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">        
                  </ul>
                      <div class="tab-content" style="padding: 2%">
                        <div class="tab-pane active" id="tab_1">
                            <hr/ style="clear: both">
                            <div class="box-body">
                                <table class='table table-bordered' id='example1'>
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Title</th>
                                            <th>Category</th>
                                            <th>Request By</th>  
                                            <th colspan="1">Action</th> 
                                                          
                                        </tr>
                                    </thead>
                                    <tbody>
                                           @if(isset($details))
                                                    @foreach($details as $detail)
                                        <tr>
                                                <!--Loop through all items for this procurement -->
                                                
                                                        <td>{{ $detail->createdAt }}</td>
                                                        <td>{{ $detail->title }}</td>
                                                        <td>{{ $detail->category }}</td>
                                                        <td>{{ $detail->lastname }} {{ $detail->firstname }} </td>
                                                        <td>
                                                            <div class='btn-group'>
                                                              
                                                                <a href="{{url('/lineManagerRequests/details/'.$detail->request_id)}}"
                                                                   class='btn btn-default btn-primary btn-xs'><i
                                                                            class="glyphicon glyphicon-eye-open"></i> View</a>
                                                                <!--
                                                                <a href="" data-toggle="modal" data-target="#{{$detail->request_id}}"
                                                                   class='btn btn-default btn-xs'>
                                                                    <i class="glyphicon glyphicon-trash"></i></a>
                                                                -->
                                                            </div>
                                                          
                                                        </td>
                                                        <!--
                                                        <div class="modal fade" id="{{$detail->request_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form action = "{{url('request/delete')}}" method="post">  
                                                                            <div class="modal-body">
                                                                                Are you sure you want to delete this request?
                                                                                <input type="hidden" name="request_id" value="{{$detail->request_id}}">
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                
                                                                                <a class="btn btn-default btn-ok" data-dismiss="modal">Cancel</a>
                                                                                <input type="submit" value="Delete"  class="btn btn-danger">
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        -->
                                        </tr>
                                          
                                         @endforeach
                                         
                                                @endif
                                    </tbody>         
                                </table>
                            </div>
                        </div>
                      </div>
               </div>
           </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript">
           $("#example1").DataTable();
    </script>
@endsection