@extends('erp.layouts.master')
  
  @section('title')
    Procurement - Request
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

@section('content')
    <section class="content-header">
       <h1>     Request
       </h1>
       <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Request Item</a></li>
          <li class="active">Request</li>
       </ol>
    </section>
    <section class="content">
          <div class="col-md-12"> 
               <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">        
                  </ul>
                      <div class="tab-content" style="padding: 2%">
                        <div class="tab-pane active" id="tab_1">
                            <hr/ style="clear: both">
                            <div id="search" style="background: #eee; height: auto; padding: 2% 5%">
                               @include('erp.procurement.partials.request_form')
                            </div>
                            <div class="box-body">
                               @include('erp.procurement.partials.request_table')
                            </div>
                        </div>
                      </div>
               </div>
           </div>
    </section>
@endsection



@section('script')
    <script>
        $(document).ready(function(){
            $('#price').keyup(calculate);
            $('#quantity').keyup(calculate);
        });
        function calculate(e)
        {
            $('#total').val($('#price').val() * $('#quantity').val());
        }


        $('#saveRequests').DataTable({
                searching: false,
                "pagingType": "full_numbers",
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel','print'
                ]
        });

        //console.log(categories);
        var result = {};
        var items = {}
        var selectedCategory = "";
        //loop through the categories
        for (var i = 0; i < categories.length; i++) {
            //save the categories and categories items object
            result[categories[i].name] = categories[i].items;
            //loop through the items in the category
            for (var j = 0; j < categories[i].items.length; j++) {
                //save the items data; [items id => items obj]
                items[categories[i].items[j].id] = categories[i].items[j];
            }


        }

        //console.log(result);
        //console.log(items);

        $('select[name="category"]').change(function () { // When category select changes

            //set the unit price to default
            $('#unit_price').val('');
            $('.description').replaceWith("<div class='description col-sm-12'><p><b></b><p></div>");
            //var p = document.querySelector('.other_product_name');
            //p.innerHTML = "";
            //$('#other_product_item').fadeOut();

            /**if ($(this).val() == "Others"){
                var productfield = document.createElement('input');
                productfield.setAttribute('type', 'text');
                productfield.setAttribute('name', 'name');
                productfield.setAttribute('placeholder', "Product Eg: Dry Yam");
                productfield.classList.add('form-control');


            }else{*/
            selectedCategory = $(this).val();
            var options = '<option>Choose one!</option>';
            $.each(result[$(this).val()] || [], function (i, v) { // Cycle through each associated items
                options += '<option value="' + v.id + '">' + v.name + '</option>';
            });
            //options += '<option>' + "Other" + '</option>';
            var itemtfield = document.createElement('select');
            itemtfield.setAttribute('name', 'item');
            itemtfield.classList.add('form-control');
            itemtfield.innerHTML = options;
            /**} */

            var p = document.querySelector('.item_name');
            p.innerHTML = "";
            p.appendChild(itemtfield);
            $('#category_item').fadeIn();
            setItemListener();
//            $('select[name="product"]').html(options); // And update the role options
        });

        function setItemListener() {
            $('select[name="item"]').change(function () {

                $(this).attr('name', 'item');
                //auto-set the unit price, qty, other information
                //gets its information
                var ite
                bj = items[$(this).val()];
                $('#unit_price').val(itemObj.unit_price);
                $('.description').replaceWith("<div class='description col-sm-12'><p><i>" + itemObj.description + "</i><p></div>");
                //console.log(itemObj);
                //console.log();
            })
        }
    </script>
    @endSection