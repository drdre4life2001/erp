<table class='table table-bordered' id='saveRequests'>
     <thead>
         <tr>
             <th>Title</th>
             <th>Description</th>
             <th>Unit Price</th>  
             <th>Quantity</th>  
             <th>Total</th>
             <th colspan="2">Action</th>  
                        
         </tr>
     </thead>
     <tbody>
            @if(isset($item))
                     @foreach($item as $items)
         <tr>
                 <!--Loop through all items for this procurement -->
                         <td>{{ $items->title }}</td>
                         <td>{{ $items->description }}</td>
                         <td>{{ number_format($items->price) }}</td>
                         <td>{{ $items->quantity }}</td>
                         <td>{{ number_format($items->total) }}</td>
                         <td>
                             <div class='btn-group' style="display:block">
                                 <!--Edit item -->
                                 <a href="" style="margin-right: 0px;" 
                                    class='btn btn-default btn-xs' data-toggle='modal' data-target='#{{$items->id}}'>
                                     <i class="glyphicon glyphicon-eye-open"> </i> Edit</a>
                                     <!--delete this item -->
                                
                                 <a href="" style="margin-right: 0px;" 
                                    class='btn btn-danger btn-xs' data-toggle="modal" data-target="#{{$items->id.'_'}}">
                                     <i class="glyphicon glyphicons-delete"> </i> Delete</a>
                                    
                             </div>
                           
                         </td>
                         <!-- Delete Modal -->
                         <div class="modal fade" id="{{$items->id.'_'}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                 <div class="modal-dialog">
                                     <div class="modal-content">
                                         <form action = "{{url('request/items/delete')}}" method="post">  
                                             <div class="modal-body">
                                                 Are you sure you want to delete this item?
                                                 <input type="hidden" name="item_id" value="{{$items->id}}">
                                                 @if(isset($detail))
                                                     @foreach($detail as $details)
                                                     <input name="request_id" id="request_id" type="hidden" class="form-control" value="{{ $details->id }}"/>
                                                     @endforeach
                                                 @endif 
                                             </div>
                                             <div class="modal-footer">
                                                 
                                                 <a class="btn btn-default btn-ok" data-dismiss="modal">No</a>
                                                 <input type="submit" value="Yes"  class="btn btn-danger">
                                             </div>
                                         </form>
                                     </div>
                                 </div>
                         </div>
                         <!--Modal ends -->
                         <!-- Modal -->
                         <div id="{{$items->id}}" class="modal fade" role="dialog">
                           <div class="modal-dialog">

                             <!-- Modal content-->
                             <div class="modal-content">
                               <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 <h4 class="modal-title">Edit this Item</h4>
                               </div>
                               <form action="{{url('request/items/edit')}}" method="post">
                               <div class="modal-body">
                                         <input type="hidden" name="item_id" value="{{$items->id}}">
                                         <div class="form-group col-sm-6">
                                             <label class="">Title</label>
                                             <input name="title" id="title" type="text" class="form-control" value="{{$items->title}}" required/>
                                         </div>
                                       <div class="form-group col-sm-6">
                                           <label class="">Salvages</label>
                                           <select name="salvage" class="form-control" required>
                                               <option value="">Please select..</option>
                                               @foreach($salvages as $salvage)
                                               <option value="{{$salvage->id}}">{{$salvage->name}} [{{number_format($salvage->value)}}]</option>
                                               @endforeach
                                           </select>
                                       </div>
                                         <div class="form-group col-sm-12">
                                             <label class="">Description</label>
                                             <textarea name="description" class="form-control" required>{{$items->description}}</textarea>
                                         </div>
                                         <div class="form-group col-sm-6">
                                             <label class="">Unit Price</label>
                                             <input name="price" id="price2" type="number" class="form-control" value="{{ $items->price }}" required/>
                                         </div>
                                         <div class="form-group col-sm-6">
                                             <label class="">Quantity</label>
                                             <input name="quantity" id="quantity2" type="number" class="form-control" value="{{$items->quantity}}" required/>
                                         </div>
                                         <div class="form-group col-sm-6">
                                             <label class="">Total</label>
                                             <input name="total" id="total2" type="number" class="form-control" value="{{ $items->total }}" required/>
                                         </div>
                                         @if(isset($detail))
                                             @foreach($detail as $details)
                                         <div class="form-group col-sm-12">
                                             <input name="request_id" id="request_id" type="hidden" class="form-control" value="{{ $details->id }}"/>
                                         </div>
                                             @endforeach
                                         @endif 
                                     
                               </div>
                               <div class="modal-footer">
                                 <input type="submit" value="submit" name="submit" class="btn btn-danger">
                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                               </div>
                               </form>
                                 @section('script')
                                 <script type="text/javascript">
                                     $(document).ready(function(){
                                         $('#price2').keyup(calculate);
                                         $('#quantity2').keyup(calculate);
                                     });
                                     function calculate(e)
                                     {
                                         $('#total2').val($('#price2').val() * $('#quantity2').val());
                                     }
                                     $('#confirm-delete').on('show.bs.modal', function(e) {
                                         $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
                                     });
                                 </script>
                                 @endsection
                             </div>
                           </div>
                         </div> 
                         <!--Modal ends -->   
                         
         </tr>
          @endforeach
                 @endif
     </tbody>         





 </table>