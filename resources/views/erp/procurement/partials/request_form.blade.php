<form action="{{ url('save_requests') }}" method="post">
        @if(isset($errors))
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endForeach
                    </ul>
                </div>
                @endIf
        @endIf
       
        <script>
            var categories = <?php echo json_encode(session('categories'));  ?> ;
            var items = <?php echo json_encode(session('items'));  ?> ;
        </script>
        <div class="row">

                    <a class="btn btn-purple pull-right"  data-toggle="modal" href='#modal-salvage'>Add Salvage</a>

                    

            <div class="form-group col-sm-12">

                <div class="form-group col-sm-2">
                    <label class="">Salvages </label> <br> <br>
                    <select required="" name="salvage" class="form-control">
                        <option value="">Please select..</option>
                        @foreach($salvages as $salvage)
                        <option value="{{$salvage->id}}">{{$salvage->name}} [{{number_format($salvage->value)}}]</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-sm-2">
                    <label class="">Title</label>
                    <input required="" name="title" id="title" type="text" class="form-control" value="{{old('title')}}"/>
                </div>
                <div class="form-group col-sm-2">
                    <label class="">Description</label>
                    <textarea required="" name="description" class="form-control">{{old('description')}}</textarea>
                </div>
                <div class="form-group col-sm-2">
                    <label class="">Unit Price</label>
                    <input required="" name="price" id="price" type="number" value="{{old('price')}}" class="form-control"/>
                </div>
                <div class="form-group col-sm-2">
                    <label class="">Quantity</label>
                    <input required="" name="quantity" id="quantity" value="{{old('quantity')}}" type="number" class="form-control"/>
                </div>
                <div class="form-group col-sm-2">
                    <label class="">Total</label>
                    <input required="" name="total" id="total" value="{{old('total')}}" type="number" class="form-control"/>
                </div>
                @if(isset($detail))
                    @foreach($detail as $details)
                <div class="form-group col-sm-3">
                    <input required="" name="request_id" id="request_id" type="hidden" class="form-control" value="{{ $details->id }}"/>
                </div>
                    @endforeach
                @endif 
            </div>
        </div>
        
        <div class="form-group col-sm-12">
            <button class ="btn btn-sm btn-danger"><i class="fa fa-plus"> Add Item</i></button>
            @if(count($item) > 0) <!--Only show the Done button if an item haas been added -->
                @if(isset($detail))
                    @foreach($detail as $details)
                     {{-- <a class ="btn btn-sm btn-success" href="{{url('/request/send/'.$details->id)}}"><i class="fa fa-check"> Done</i></a> --}}
                    @endforeach
                @endif 
            @endif
        </div>
        
        <!--
        
        <div class="form-group col-sm-12">
            <input type="submit" value="Submit" class="btn btn-sm btn-primary"/>
        </div>
        -->
        
        </form>

        <div class="modal fade" id="modal-salvage">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Add salvage item</h4>
                                </div>
                                <form method="POST" action="{{url('finance/add-salvage')}}">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" placeholder="Enter salvage name" name="salvage_name" value="{{old('salvage_name')}}">
                                            </div>
                                            <div class="col-lg-4">
                                                <input type="number" class="form-control" placeholder="e.g 12" name="period" value="{{old('period')}}">
                                            </div>
                                            <div class="col-lg-4">
                                                <input type="number" class="form-control" placeholder="Amount" name="value" value="{{old('value')}}">
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="modal-footer"><br>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <input type="submit" class="btn btn-purple"></button>
                                    </div>    
                                </form>
                                
                            </div>
                        </div>
                    </div>