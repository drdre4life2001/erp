@extends('erp.layouts.master')
  
  @section('title')
    Finance - All Loans
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
     <section class="content-header">
         <h1>
            Company Loan
         </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Company Loan</li>
         </ol>
      </section>
      <section class="content">
         <div class="col-md-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Company Loan
                     </b></a>
                  </li>
               </ul>
               <div class="tab-content" style="padding: 2%">
                  <div class="tab-pane active" id="tab_1">
                     <p align="right">
                     <hr/ style="clear: both">
                     <div class="box-body">
                        <table class='table table-bordered' id='example1'>
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Request Title</th>
                                    <th>Request Category</th>
                                    <th>Requested By</th>   
                                </tr>
                            </thead>
                            <tbody>
                                   @if(isset($requests))
                                            @foreach($requests as $request)
                                            <?php $date = date('d-m-Y', strtotime($request->created)); ?>
                                <tr>
                                        <!--Loop through all items for this procurement -->
                                                <td>{{$date}}</td>
                                                <td>{{ $request->title }}</td>
                                                <td>{{ $request->category }}</td>
                                                <td>{{ $request->lastname}} {{$request->firstname}}</td>
                                </tr>
                                
                                @endforeach
                                @endif
                            </tbody>

                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
  @endsection

  @section('script')
    <script type="text/javascript">
        $("#example1").DataTable({
            searching: true,
                "pagingType": "full_numbers",
                dom: 'Bfrtip'
            });
    </script>
  @endsection