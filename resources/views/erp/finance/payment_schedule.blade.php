@extends('erp.layouts.master')
  
  @section('title')
    Finance - Payment Schedule
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
      <section class="content-header">
         <h1>
            {{ ucfirst($company_name) }}'s Payment Schedule
         </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Payment Schedule</li>
         </ol>
      </section>
      <section class="content">
         <div class="col-md-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Payment Schedule
                     </b></a>
                  </li>
               </ul>
               <div class="tab-content" style="padding: 2%">
                  <div class="tab-pane active" id="tab_1">
                     <hr/ style="clear: both">
                     <div class="box-body">
                        <center>
                              <button class="btn btn-large btn-default"><i class="fa fa-mark" aria-hidden="true" style="color:white"></i> {{ ucfirst($company_name) }}'s Payment Schedule
                              </button>
                        </center>
                        <hr/ style="clear: both">
                        <table id="example1" class="table table-bordered table-hover">
                           <thead>
                              <tr style="color:#8b8b8b">
                                 <th> <center>Company Name</center></th>
                                 <th> <center>Amout to be paid</center></th>
                                 <th> <center>Interest Rate</center></th>
                                 <th> <center>Period</center></th>
                                 <th> <center>Bank</center></th>
                                 <th> <center>Monthly Payment</center></th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                  <td> <center>{!! $company_name !!}</center></td>
                                  <td>
                                     <center>
                                      &#8358;{!! number_format($loan->amount, 2) !!}
                                     </center>
                                 </td>
                                  <td>
                                     <center>
                                         {!! number_format($loan->interest_rate, 2) !!}<span style="font-weight: bold;">%
                                    </span> </center></td>
                                  <td>
                                     <center>
                                         {!! $loan->period !!}
                                     </center>
                                 </td>
                                  <td>
                                     <center>
                                      {!! $bank !!}
                                     </center>
                                 </td>
                                  <td>
                                     <center>
                                      &#8358;{!! number_format($monthly_payment, 2) !!}<span style="color:&#8358;"> (Monthly)</span>
                                     </center>
                                 </td>
                              </tr>
                              </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
               <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </section>
  @endsection

  @section('script')
       <script type="text/javascript">
         
         $(function () {
           $("#example1").DataTable();});


         $('.adf').hide();
         $('.cdb').on('click', function(){
           $('.adf').slideToggle();
         });

         @if ($errors->has('company_id') || $errors->has('bank') || $errors->has('description') || $errors->has('interest_rate') || $errors->has('amount') || $errors->has('period'))
           $('.adf').slideToggle();
         @endif

           $('.cadfxx').on('click', function(){
              $('.adf').slideToggle();
           });

       </script>
  @endsection
