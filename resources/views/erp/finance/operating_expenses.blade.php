@extends('erp.layouts.master')
  
  @section('title')
    Finance - Operating Expenses
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  	<section class="content-header">
   <h1>      Operating Expenses  
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Operating Expenses </li>
   </ol>
</section>
<section class="content">
    <div class="col-md-12">
	   <div class="nav-tabs-custom">
	      <ul class="nav nav-tabs">
	         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Operating Expenses  </b></a></li>
	      </ul>
	      <div class="tab-content" style="padding: 2%">
	         <div class="tab-pane active" id="tab_1">
	            <p align="right">
	               <button class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add Operating Expensess</button>
	            </p>
	            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
	               <div class="col-md-1 hidden-sm hidden-xs"></div>
	               <div class="col-md-10">
	                  <form method="POST" action="">
	                     <h3 align="center">Operating Expenses</h3>
	                     <div class="form-group">
	                        <label>Name</label>
	                        <div class="icon-addon addon-md">
	                           <input type="text" required="" class="form-control" name="name" value="{{ old('name') }}" placeholder="Expense Name">
	                           @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
	                           <label for="house" class="fa fa-book" rel="tooltip" title="Description"></label>
	                        </div>
	                     </div>
	                     <p align="center">
	                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
	                        Add</button>
	                        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
	                        Cancel</a>
	                     </p>
	                  </form>
	               </div>
	               <div class="col-md-1 hidden-sm hidden-xs"></div>
	            </div>
	            <hr/ style="clear: both">
	            <div class="box-body">
	               <table id="example1" class="table table-bordered table-hover">
	                  <thead>
	                     <tr style="color:#8b8b8b">
	                        <th>S/N </th>
	                        <th>Name</th>
	                        <th>Action</th>
	                     </tr>
	                  </thead>
	                  <tbody>
	                  	<?php $inc = 1; ?>
	                     @foreach($item as $i)
	                     	<tr>
	                     	   <td>{{ $inc++ }}</td>
	                     	   <td>{{ucfirst($i->name) }}</td>
	                     	   <td>  
	                     	    <a data-toggle="modal" href='#modal-id{{ $i->id }}'>
	                     	      <span class="label label-warning" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
								   <i class="fa fa-trash"></i>  Edit </span>
								</a>
	                     	    <a id="a_del" href="{{ url('finance/operating_expenses/delete/'.$i->id) }}">
	                     	      <span class="label label-danger" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
								   <i class="fa fa-trash"></i>  Deactive </span>
								</a>
	                     	   </td>
	                     	</tr>
	                     	{{-- modal for edit --}}
	                     	<div class="modal fade" id="modal-id{{ $i->id }}">
	                     		<div class="modal-dialog">
	                     			<div class="modal-content">
	                     				<div class="modal-header">
	                     					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                     					<h4 class="modal-title">Edit- {{ ucfirst($i->name) }} Expense</h4>
	                     				</div>
	                     				<div class="modal-body">
	                     					<form method="POST" action="{{ url('finance/operating_expenses/edit') }}">
	                     					   <h3 align="center">Operating Expenses</h3>
	                     					   <div class="form-group">
	                     					      <label>Name</label>
	                     					      <input type="hidden" name="item_id" value="{{ $i->id }}">
	                     					      <div class="icon-addon addon-md">
	                     					         <input type="text" class="form-control" name="name" value="{{ $i->name }}" placeholder="Expense Name">
	                     					         @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
	                     					         <label for="house" class="fa fa-book" rel="tooltip" title="Description"></label>
	                     					      </div>
	                     					   </div>
	                     					   <p align="center">
	                     					      <button class="btn btn-large btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i>
	                     					      Update</button>
	                     					   </p>
	                     					</form>
	                     				</div>
	                     				<div class="modal-footer">
	                     					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                     				</div>
	                     			</div>
	                     		</div>
	                     	</div>
	                     @endforeach
	                  </tfoot>
	               </table>
	            </div>
	         </div>
	      </div>
	   </div>
   </div>
</section>
{{-- <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Trigger modal</a> --}}
@endsection

@section('script')
	<script>
	  // $(function () {
	  	// alert('helo');
		  $('#example1').DataTable({
	          dom: 'Bfrtip',
	          buttons: [
	              {
	                  extend: 'print',

	                //   exportOptions: {
	                //       columns: ':visible'
	                //   }
	              },
				  {
	                  extend: 'excel',

	                //   exportOptions: {
	                //       columns: ':visible'
	                //   }
	              },
	          ],
		  });

		  $('.adf').hide();
		  $('.cdb').on('click', function(){
		    $('.adf').slideToggle();
		 
		  });

		    $('.cadfxx').on('click', function(){
		       $('.adf').slideToggle();
		    });
	  // });
	</script>
@endsection