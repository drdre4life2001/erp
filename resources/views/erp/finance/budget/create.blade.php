@extends('erp.layouts.master')
  
  @section('title')
    Finance - Budget
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
      <section class="content-header">
         <h1>
            Budget
         </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Budget</li>
         </ol>
      </section>
      <section class="content">
         <div class="col-md-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Budget
                     </b></a>
                  </li>
               </ul>
               <div class="tab-content" style="padding: 2%">
                  <div class="tab-pane active" id="tab_1">
                     <p align="right">
                     </p>
                       <div class="" style="background:#ecf0f5; float: left; width: 100%;">
                          <div class="col-md-1 hidden-sm hidden-xs"></div>
                          <div class="col-md-10">
                              @if(session()->has('message'))
                                  <div class="alert alert-danger">
                                      {{session()->get('message')}}
                                  </div>
                              @endif

                             @include('erp.finance.partials.create_budget_form')
                          </div>
                          <div class="col-md-1 hidden-sm hidden-xs"></div>
                       </div>
                     <hr/ style="clear: both">
                  </div>
               </div>
            </div>
         </div>
      </section>
  @endsection



  @section('script')
       <script type="text/javascript">
         
         $(function () {
           $("#example1").DataTable();});


         $('.adf').hide();
         $('.cdb').on('click', function(){
           $('.adf').slideToggle();
         });

         @if ($errors->has('amount_paid') || $errors->has('note'))
           $('.adf').slideToggle();
         @endif

           $('.cadfxx').on('click', function(){
              $('.adf').slideToggle();
           });


 
        //INCOME SOURCE ADD MORE BUTTON FOR BUDGET 
         var i = 1;
         $('.add-income-source').click(function(){
             i++;
         var box_html = $('<div class="row well income-more"><div class="col-lg-6"><div class="form-group"><label>Name</label><div class="icon-addon addon-md"> <select name="income_name[]" id="inputIncomeName" class="form-control" required="required"><option value="">Select Source</option>@foreach($income_sources as $income)<option value="{{$income->id}}">{{$income->name}}</option>@endforeach</select><label for="house" class="fa fa-book" rel="tooltip" title="Name of budget"></label>@if ($errors->has('income_name')) <p class="help-block" style="color: red">{{ $errors->first('income_amount') }}</p> @endif</div></div></div><div class="col-lg-6"><div class="form-group"><label>Amount</label><div class="icon-addon addon-md"><input required="" type="number" required="" value="{{ old('income_amount') }}"  name="income_amount[]" class="form-control" placeholder="Amount"><label for="house" class="fa fa-book" rel="tooltip" title="Amount"></label>@if ($errors->has('income_amount')) <p class="help-block" style="color: red">{{ $errors->first('income_amount') }}</p> @endif</div></div></div> <a id="a_del" class="pull-right remove_button" style="color: red;"> <i class="fa fa-trash"></i></a></div>');
             $('.income-wrapper').append(box_html);
         });

         //FIXED EXPENSE ADD MORE BUTTON FOR BUDGET 
         var j = 1;
         $('.add-fixed').click(function(){
             j++;
         var fixed_html = $('<div class="row well"><div class="col-lg-6"><div class="form-group"><label>Name</label><div class="icon-addon addon-md"><select name="f_name[]" id="inputIncomeName" class="form-control" required="required"> <option value="">Select Source</option> @foreach($f_expenses as $f) <option value="{{$f->id}}">{{$f->name}}</option> @endforeach </select><label for="house" class="fa fa-book" rel="tooltip" title="Name of Expense"></label>@if ($errors->has('f_name')) <p class="help-block" style="color: red">{{ $errors->first('f_name') }}</p> @endif</div></div></div><div class="col-lg-6"><div class="form-group"><label>Amount</label><div class="icon-addon addon-md"><input required="" type="number" required="" value="{{ old('f_amount') }}"  name="f_amount[]" class="form-control" placeholder="Amount"><label for="house" class="fa fa-book" rel="tooltip" title="Amount"></label>@if ($errors->has('f_amount')) <p class="help-block" style="color: red">{{ $errors->first('f_amount') }}</p> @endif</div></div></div><a id="a_del" class="pull-right rm_btn" style="color: red;"> <i class="fa fa-trash"></i></a></div>');
             $('.fixed-wrapper').append(fixed_html);
             // box_html.fadeIn('slow');
             // $(document).on('click', '.btn_remove', function() {
             //   var button_id = $(this).attr("id");
             //   $('#row'+button_id+'').remove();
             // });
         });

         //VARIABLE EXPENSE ADD MORE BUTTON FOR BUDGET 
         var j = 1;
         $('.add-variable').click(function(){
             j++;
         var variable_html = $('<div class="row well"><div class="col-lg-6"><div class="form-group"><label>Name</label><div class="icon-addon addon-md"><select name="v_name[]" id="inputIncomeName" class="form-control" required="required"> <option value="">Select Source</option> @foreach($v_expenses as $v) <option value="{{$v->id}}">{{$v->name}}</option> @endforeach</select><label for="house" class="fa fa-book" rel="tooltip" title="Name of Expense"></label>@if ($errors->has('v_name')) <p class="help-block" style="color: red">{{ $errors->first('v_name') }}</p> @endif</div></div></div><div class="col-lg-6"><div class="form-group"><label>Amount</label><div class="icon-addon addon-md"><input required="" type="number" required="" value="{{ old('v_amount') }}"  name="v_amount[]" class="form-control" placeholder="Amount"><label for="house" class="fa fa-book" rel="tooltip" title="Amount"></label>@if ($errors->has('v_amount')) <p class="help-block" style="color: red">{{ $errors->first('v_amount') }}</p> @endif</div></div></div> <a id="a_del" class="pull-right r_btn" style="color: red;"> <i class="fa fa-trash"></i></a></div>');
             $('.variable-wrapper').append(variable_html);
             // box_html.fadeIn('slow');
             // $(document).on('click', '.btn_remove', function() {
             //   var button_id = $(this).attr("id");
             //   $('#row'+button_id+'').remove();
             // });
         });

         $('.income-wrapper').on('click', '.remove_button', function(e){
             e.preventDefault();
             $(this).parent('div').remove(); //Remove field html
             x--; //Decrement field counter
         });

         $('.fixed-wrapper').on('click', '.rm_btn', function(e){
             e.preventDefault();
             $(this).parent('div').remove(); //Remove field html
             x--; //Decrement field counter
         });

         $('.variable-wrapper').on('click', '.r_btn', function(e){
             e.preventDefault();
             $(this).parent('div').remove(); //Remove field html
             x--; //Decrement field counter
         });


          

       </script>
  @endsection
