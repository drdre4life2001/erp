@extends('erp.layouts.master')
  
  @section('title')
    Finance - Salvage Items
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  	<section class="content-header">
   <h1>      Revenue Streams
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
   </ol>
</section>
@if ($errors->any())
        {{ implode('', $errors->all('<div>:message</div>')) }}
      @endif
<section class="content">
      <div class="col-md-12">
   <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> All Revenue Streams
            </b></a>
         </li>
      </ul>
      
      <div class="tab-content" style="padding: 2%">
         <div class="tab-pane active" id="tab_1">
            <p align="right">
               <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add Revenue Streams
               </button>
            </p>
            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
               <div class="col-md-1 hidden-sm hidden-xs"></div>
               <div class="col-md-10">
                  <form method="POST" action="{{ url('finance/add-revenue-stream') }}">
                     <h3 align="center">Revenue Streams
                     </h3>
                     <div class="col-lg-12">
                        <div class="form-group">
                           <label>Name</label>
                           <div class="icon-addon addon-md">
                              <input type="text" class="form-control" name="name" placeholder="Name">
                              <label for="house" class="fa fa-user" rel="tooltip" title="Name"></label>
                              @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-12">
                        <div class="form-group">
                           <label>Assign Client</label>
                           <div class="">
                              <select class="form-control client_search" style="width:100%;" name="client_id">
                              </select>
                              @if ($errors->has('client_id')) <p class="help-block" style="color: red">{{ $errors->first('cllient_id') }}</p> @endif
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-12">
                        <div class="form-group">
                           <label>Search/Select Organization</label>
                           <div class="">
                              <select class="form-control organization_search" style="width:100%;" name="company_id">
                              </select>
                              @if ($errors->has('company_id')) <p class="help-block" style="color: red">{{ $errors->first('company_id') }}</p> @endif
                           </div>
                        </div>
                     </div>
                     <p align="center">
                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                        Add </button>
                        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                        Cancel</a>
                     </p>
                  </form>
               </div>
               <div class="col-md-1 hidden-sm hidden-xs"></div>
            </div>
            <hr/ style="clear: both">
            <div class="box-body">
               <table id="example1" class="table table-bordered table-hover">
                  <thead>
                     <tr style="color:#8b8b8b">
                        <th>S/N </th>
                        <th>Name</th>
                        <th>Client</th>
                        <th>Company</th>
                        <th>Total Revenue</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php $num = 1; ?>
                     @forelse($items as $item)
                        <tr>
                           <td>{{ $num++ }}</td>
                           <td> {{ $item->name}}  </td>
                           <td>{{ $item->client }} </td>
                           <td>{{ $item->company }}</td>
                           <td>&#x20A6;{{ number_format($item->revenue, 2) }}</td>
                           <td>  
                              <a data-toggle="modal" href="#modal-id{{ $item->id }}">
                              <span class="label label-warning" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                              <i class="fa fa-trash"></i>  Edit </span></a>
                              <a href="{{ url('finance/revenue_stream/delete/'. $item->id) }}" id="a_del">
                              <span class="label label-danger" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                              <i class="fa fa-trash"></i>  Delete </span></a>
                           </td>
                        </tr>

                        <div class="modal fade" id="modal-id{{ $item->id }}">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Update Revenue Stream</h4>
                                 </div>
                                 <div class="modal-body">
                                    <form method="POST" action="{{ url('finance/revenue_stream/edit') }}">
                                       <h3 align="center">Revenue Streams
                                       </h3>
                                       <div class="col-lg-12">
                                          <div class="form-group">
                                             <label>Name</label>
                                             <div class="icon-addon addon-md">
                                                <input type="text" class="form-control" name="name" value="{{ $item->name }}" placeholder="Name">
                                                <label for="house" class="fa fa-user" rel="tooltip" title="Name"></label>
                                                @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                                             </div>
                                          </div>
                                       </div>
                                       <input type="hidden" name="item_id" value="{{ $item->id }}">
                                       <div class="col-lg-6">
                                          <div class="form-group">
                                             <label>Assign Client</label>
                                             <div class="icon-addon addon-md">
                                                <select class="form-control" name="client_id">
                                                   <option value="">--Please select --</option>
                                                   <?php $db_client = $item->client_id; ?>
                                                   @forelse($clients as $client)
                                                      <option value="{{ $client->id }}" @if($client->id == $db_client) selected @endif>{{ $client->name }}</option>
                                                   @empty
                                                   @endforelse
                                                </select>
                                                <label for="house" class="fa fa-university" rel="tooltip" title="Bank Name"></label>
                                                @if ($errors->has('client_id')) <p class="help-block" style="color: red">{{ $errors->first('cllient_id') }}</p> @endif
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-lg-6">
                                          <div class="form-group">
                                             <label>Assign Client</label>
                                             <div class="icon-addon addon-md">
                                                <select class="form-control" name="company_id">
                                                   <option value="">--Please select --</option>
                                                   <?php $db_company = $item->company_id; ?>
                                                   @forelse($organizations as $org)
                                                      <option value="{{ $org->id }}" @if($org->id == $db_company) selected @endif>{{ $org->name }}</option>
                                                   @empty
                                                   @endforelse
                                                </select>
                                                <label for="house" class="fa fa-building" rel="tooltip" title="Bank Name"></label>
                                                @if ($errors->has('company_id')) <p class="help-block" style="color: red">{{ $errors->first('company_id') }}</p> @endif
                                             </div>
                                          </div>
                                       </div>
                                       <p align="center">
                                          <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                                          Update </button>
                                       </p>
                                    </form>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     @empty

                     @endforelse
                     </tfoot>
               </table>
            </div>
         </div>
      </div>
      <!-- /.tab-content -->
   </div>
   <!-- nav-tabs-custom -->
   </div>
   <!-- /.col -->
</section>
  @endsection

@section('script')
    <script>
      $(function () {
        $("#example1").DataTable(); });
      $('.adf').hide();
      $('.cdb').on('click', function(){
        $('.adf').slideToggle();
     
      });

        $('.cadfxx').on('click', function(){
           $('.adf').slideToggle();
        });

    </script>
@endsection