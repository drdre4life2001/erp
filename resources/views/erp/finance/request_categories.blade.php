@extends('erp.layouts.master')
  
  @section('title')
   Finance - Request Categories
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
    <section class="content-header">
   <h1>    Request Categories 
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Request Categories</li>
   </ol>
</section>
<section class="content">
      <div class="col-md-12">
   <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>All Request Categories
            </b></a>
         </li>
      </ul>
      <div class="tab-content" style="padding: 2%">
         <div class="tab-pane active" id="tab_1">
            <p align="right">
               <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add New
               </button>
            </p>
            <div class="" style="background:#ecf0f5; float: left; width: 100%; ">
               <div class="col-md-1 hidden-sm hidden-xs"></div>
               <div class="col-md-10">
                  <form method="post" action="{{ route('requestCategories.store') }}">
                     <h3 align="center"> Request Categories 
                     </h3>
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label>Name</label>
                           <input type="text" name="name" class="form-control">
                           @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label>Revenue Stream</label>
                           <select class="form-control revenue_stream_search" name="parent_id" >
                             <option value="">Select One</option>
                              
                           </select>
                           @if ($errors->has('parent_id')) <p class="help-block" style="color: red">{{ $errors->first('parent_id') }}</p> @endif
                        </div>
                     </div>
                     <p align="center">
                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                        Save </button>
                        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                        Cancel</a>
                     </p>
                  </form>
               </div>
               <div class="col-md-1 hidden-sm hidden-xs"></div>
            </div>
            <hr/ style="clear: both">
            <div class="box-body">
               <table id="example1" class="table table-bordered table-hover">
                  <thead>
                     <tr style="color:#8b8b8b">
                        {{-- <th>S/N</th> --}}
                        <th>Name</th>
                        <th>Parent Category</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($requestCategories as $requestCategory)
                         <tr>
                             <?php $name = \App\Models\RequestCategory::find($requestCategory->parent_id);?>

                             <td>{!! $requestCategory->name !!}</td>
                             <td>{!! !is_null($name) ? $name->name : '' !!}</td>
                             <td>
                                 {!! Form::open(['route' => ['requestCategories.destroy', $requestCategory->id], 'method' => 'delete']) !!}
                                 <div class='btn-group'>
                                     {{--
                                     <a href="{!! route('requestCategories.show', [$requestCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                     --}}
                                     <a href="{!! route('requestCategories.edit', [$requestCategory->id]) !!}" >
                                        <span class="label label-warning" style="padding:6.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                        <i class="fa fa-edit"></i>  Edit </span></a>
                                        <a href="{{url('request-category/delete/'. $requestCategory->id)}}" id="a_del">
                                        <span class="label label-danger" style="padding:6.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                        <i class="fa fa-trash"></i>  Delete </span></a>

                                  </div>
                                 {!! Form::close() !!}
                             </td>
                         </tr>
                     @endforeach
                  </tbody>
                  </tfoot>
               </table>
            </div>
         </div>
      </div>
   </div>
   </div>
</section>
@endsection

@section('script')
	<!-- page script -->
  <script type="text/javascript">
  
  @if ($errors->has('parent_id') || $errors->has('name'))
           $('.adf').slideToggle();
  @endif


	  $(function () {
	    $("#example1").DataTable();

	        });


	  $('.adf').hide();
	  $('.cdb').on('click', function(){
	    $('.adf').slideToggle();
	 
	  });

	    $('.cadfxx').on('click', function(){
	       $('.adf').slideToggle();
	    });

	        //Date picker
	    $('#datepicker').datepicker({
	      autoclose: true
	    });

	        //Date picker
	    $('#datepicker2').datepicker({
	      autoclose: true
	    });

	     $('#reservation').daterangepicker();
	    //Date range picker with time picker
	    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
	    //Date range as a button

	$('#daterange-btn').daterangepicker(
	        {
	          ranges: {
	            'Today': [moment(), moment()],
	            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	            'This Month': [moment().startOf('month'), moment().endOf('month')],
	            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	          },
	          startDate: moment().subtract(29, 'days'),
	          endDate: moment()
	        },
	        function (start, end) {
	          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	        }
	    );

	</script>	
@endsection