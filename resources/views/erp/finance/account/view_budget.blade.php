@extends('erp.layouts.master')

@section('title')
    Finance - View Budget
@endsection

@section('sidebar')
    @include('erp.partials.sidebar')
@endsection

@section('content')
    <section class="content-header">
        <h1>
            View Budget
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> View Budget</li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> View Budget</b></a></li>
                </ul>
                <div class="tab-content" style="padding: 2%">
                    <div class="tab-pane active" id="tab_1">
                        <p align="right">
                            <a href="{{url('/budget/create')}}" class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add  Budget</a>
                        </p>
                        <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                            <div class="col-md-1 hidden-sm hidden-xs"></div>
                        </div>
                        <hr/ style="clear: both">
                        <div class="box-body">
                            @include('erp.finance.partials.budget_view')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript">
        $("#example1").DataTable();

        $('.adf').hide();
        $('.cdb').on('click', function(){
            $('.adf').slideToggle();

        });

        $('.cadfxx').on('click', function(){
            $('.adf').slideToggle();
        });
    </script>
@endsection