@extends('erp.layouts.master')

@section('title')
    Finance - Account
@endsection

@section('sidebar')
    @include('erp.partials.sidebar')
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Budget
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Account</li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Account
                            </b></a>
                    </li>
                </ul>
                <div class="tab-content" style="padding: 2%">
                    <div class="tab-pane active" id="tab_1">
                        <p align="right">
{{--                             <button class="btn btn-large btn-purple cdb"><i class="" aria-hidden="true" style="color:white"></i>  Account
                            </button> --}}
                        </p>
                        <div class="" style="background:#ecf0f5; float: left; width: 100%;">
                            <div class="col-md-1 hidden-sm hidden-xs"></div>
                            <div class="col-md-10">
                                @if(session()->has('message'))
                                    <div class="alert alert-danger">
                                        {{session()->get('message')}}
                                    </div>
                                @endif

                                {{-- @include('erp.finance.partials.create_account') --}}
                            </div>
                            <div class="col-md-1 hidden-sm hidden-xs"></div>
                            <hr/>
                            <div class="box-body">
                                @include('erp.finance.partials.accounts_table')
                            </div>
                        </div>
                        <hr/ style="clear: both">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection