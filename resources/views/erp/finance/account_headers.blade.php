@extends('erp.layouts.master')
  
  @section('title')
    Finance - Account Headers
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  	<section class="content-header">
   <h1>      Account Headers
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Account Headers</li>
   </ol>
</section>
<section class="content">
      <div class="col-md-12">
   <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>  Account Headers </b></a></li>
      </ul>
      <div class="tab-content" style="padding: 2%">
         <div class="tab-pane active" id="tab_1">
            <p align="right">
               <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add  Account Headers </button>
            </p>
            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
               <div class="col-md-1 hidden-sm hidden-xs"></div>
               <div class="col-md-10">
                  <form method="post" action="">
                    {{ csrf_field() }}
                     <h3 align="center">Account Headers</h3>
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label>Name</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" name="name" placeholder="Account Number">
                              <label for="house" class="fa fa-user" rel="tooltip" title="Bank Name"></label>
                              @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label>Effect</label>
                           <div class="icon-addon addon-md">
                              <select required class="form-control" name="effect">
                                 <option value=""> Choose</option>
                                 @foreach($effects as $e)
                                   <option value="{{ $e->id }}"> {{ $e->name }}</option>
                                 @endforeach
                              </select>
                              <label for="house" class="fa fa-building" rel="tooltip" title="Bank Name"></label>
                              @if ($errors->has('effect')) <p class="help-block" style="color: red">{{ $errors->first('effect') }}</p> @endif
                           </div>
                        </div>
                     </div>
                     <p align="center">
                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                        Add Header</button>
                        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                        Cancel</a>
                     </p>
                  </form>
               </div>
               <div class="col-md-1 hidden-sm hidden-xs"></div>
            </div>
            <hr/ style="clear: both">
            <div class="box-body">
               <table id="example1" class="table table-bordered table-hover">
                  <thead>
                     <tr style="color:#8b8b8b">
                        <th>S/N </th>
                        <th>Header</th>
                        <th>Effect</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                    <?php $num = 1; ?>
                    @foreach($item as $i)
                      <tr>
                         <td>{{ $num++ }}</td>
                         <td>{{ $i->header }}</td>
                         <td>{{ $i->effect }}</td>
                         <td>  
                            <a data-toggle="modal" href='#modal-id{{ $i->id }}'>
                            <span class="label label-warning" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                            <i class="fa fa-trash"></i>  Edit </span></a>
                            <a id="a_del" href="{{ url('finance/account_header/delete/'.$i->id) }}">
                            <span class="label label-danger" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                            <i class="fa fa-trash"></i>  Delete </span></a>
                         </td>
                      </tr>

                      <div class="modal fade" id="modal-id{{ $i->id }}">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Update</h4>
                            </div>
                            <div class="modal-body">
                              <form method="post" action="{{ url('finance/account_header/edit') }}">
                                {{ csrf_field() }}
                                 <h3 align="center">Account Headers</h3>
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                       <label>Name</label>
                                       <div class="icon-addon addon-md">
                                          <input type="text" required="" class="form-control" name="name" value="{{ $i->header }}" placeholder="Account Number">
                                          <label for="house" class="fa fa-user" rel="tooltip" title="Bank Name"></label>
                                          @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                                       </div>
                                    </div>
                                 </div>
                                 <input type="hidden" name="item_id" value="{{ $i->id }}">
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                       <label>Effect</label>
                                       <div class="icon-addon addon-md">
                                          <select class="form-control" name="effect">
                                             <option value=""> Choose</option>
                                            <?php $db_selected_value = $i->id;?>
                                             @foreach($effects as $e)
                                               <option value="{{ $e->id }}" @if($db_selected_value == $e->id) selected @endif> {{ $e->name }}</option>
                                             @endforeach
                                          </select>
                                          <label for="house" class="fa fa-building" rel="tooltip" title="Bank Name"></label>
                                          @if ($errors->has('effect')) <p class="help-block" style="color: red">{{ $errors->first('effect') }}</p> @endif
                                       </div>
                                    </div>
                                 </div>
                                 <p align="center">
                                    <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    Update Bank Account</button>
                                 </p>
                              </form>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforeach
                     </tfoot>
               </table>
            </div>
         </div>
      </div>
   </div>
   </div>
</section>
@endsection

@section('script')
    <script>
      $(function () {
        $("#example1").DataTable();

            });


      $('.adf').hide();
      $('.cdb').on('click', function(){
        $('.adf').slideToggle();
     
      });

        $('.cadfxx').on('click', function(){
           $('.adf').slideToggle();
        });

        //get sub-headers dynamically
        //onchange country display states related
        
    </script>
@endsection