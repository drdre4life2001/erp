@extends('erp.layouts.master')
  
  @section('title')
    Finance - Banks
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  	<section class="content-header">
   <h1>      Banks
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Banks </li>
   </ol>
</section>
<section class="content">
  <div class="col-md-12">
   <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Banks  </b></a></li>
      </ul>
      <div class="tab-content" style="padding: 2%">
         <div class="tab-pane active" id="tab_1">
            <p align="right">
               <button class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add Banks </button>
            </p>
            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
               <div class="col-md-1 hidden-sm hidden-xs"></div>
               <div class="col-md-10">
                  <form method="POST" action="">
                    {{ csrf_field() }}
                     <h3 align="center">Banks </h3>
                     <div class="form-group">
                        <label>Bank Name</label>
                        <div class="icon-addon addon-md">
                           <input type="text" class="form-control" name="name" placeholder="Name">
                           <label for="house" class="fa fa-university" rel="tooltip" title="Bank Name"></label>
                        </div>
                     </div>
                     <p align="center">
                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                        Add Bank</button>
                        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                        Cancel</a>
                     </p>
                  </form>
               </div>
               <div class="col-md-1 hidden-sm hidden-xs"></div>
            </div>
            <hr/ style="clear: both">
            <div class="box-body">
               <table id="example1" class="table table-bordered table-hover">
                  <thead>
                     <tr style="color:#8b8b8b">
                        <th>S/N </th>
                        <th>Bank Name</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                    <?php $num = 1; ?>
                     @foreach($banks as $bank)
                      <tr>
                         <td>{{ $num++ }}</td>
                         <td>
                            {{-- <div class="product-img">
                               <img src="{{ asset('erp/dist/img/fcmb.jpg') }}" style="float:left; border-radius:10%; height: 40px; width: 40px; margin-right: 5%; margin-top:1%">
                            </div> --}}
                            <span style="float:left; margin-top:2%; font-weight: bold">{{ ucfirst($bank->name) }}</span>
                         </td>
                         <td>  
                            <a data-toggle="modal" href='#modal-id{{ $bank->id }}'>
                            <span class="label label-warning" style="padding:2.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                            <i class="fa fa-trash"></i>  Edit </span></a>
                            <a id="a_del" href="{{ url('finance/banks/delete/'.$bank->id) }}">
                            <span class="label label-danger" style="padding:2.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                            <i class="fa fa-trash"></i>  Deactive </span></a>
                         </td>
                      </tr>

                      <div class="modal fade" id="modal-id{{ $bank->id }}">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Edit- {{ ucfirst($bank->name) }} </h4>
                              </div>
                              <div class="modal-body">
                                <form method="POST" action="{{ url('finance/banks/edit') }}">
                                   <h3 align="center">Bank</h3>
                                   <div class="form-group">
                                      <label>Name</label>
                                      <input type="hidden" name="bank_id" value="{{ $bank->id }}">
                                      <div class="icon-addon addon-md">
                                         <input type="text" class="form-control" name="name" value="{{ $bank->name }}" placeholder="Bank Name">
                                         @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                                         <label for="house" class="fa fa-book" rel="tooltip" title="Bank Name"></label>
                                      </div>
                                   </div>
                                   <p align="center">
                                      <button class="btn btn-large btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                                      Update</button>
                                   </p>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                     @endforeach
                     </tfoot>
               </table>
            </div>
         </div>
      </div>
      <!-- /.tab-content -->
   </div>
  </div>
</section>
@endsection


@section('script')
    <script>
    
    </script>
@endsection