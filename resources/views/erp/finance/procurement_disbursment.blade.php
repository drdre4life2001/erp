@extends('erp.layouts.master')
  
  @section('title')
    Finance - Procurement Disbursement
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  	<section class="content-header">
  	   <h1>     Procurement Items
  	   </h1>
  	   <ol class="breadcrumb">
  	      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  	      <li class="active">Procurement Items</li>
  	   </ol>
  	</section>
  	<section class="content">
  	   <div class="col-md-12">
  	      <div class="nav-tabs-custom">
  	         <ul class="nav nav-tabs">
  	            <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>procurement
  	               </b></a>
  	            </li>
  	         </ul>
  	         <div class="tab-content" style="padding: 2%">
  	            <div class="tab-pane active" id="tab_1">
  	               <hr/ style="clear: both">
  	               <div class="box-body">
  	                  <table id="example1" class="table table-bordered table-hover">
  	                     <thead>
  	                        <tr style="color:#8b8b8b">
                               <th>S/N</th>
  	                           <th>Title</th>
  	                           <th>Description</th>
  	                           <th>Price</th>
  	                           <th>Quantiity</th>
  	                           <th>Total</th>
  	                           <th>Line Manager Remark</th>
  	                           <th>C-Level Remark</th>
  	                           <th>Action</th>
  	                        </tr>
  	                     </thead>
  	                     <tbody>
                          <?php $inc = 1; ?>
                          @forelse($items as $item)
                            <tr>
                            <!--Loop through all items for this procurement -->
                            <td>{{ $inc++ }}</td>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->description }}</td>
                            <td>{{ number_format($item->price) }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ number_format($item->total) }}</td>
                            <td>{{ $item->line_manager_status }}</td>
                            <td>{{ $item->c_level_status }}</td>
                            <td>

                                <div class='btn-group'>
                                    <?php 
                                    $getUserStatus = \App\Libraries\Utilities::getUserStatus($item->id);

                                    if($getUserStatus == 'procurement'){
                                        if($item->rfq_status == 2){
                                            echo "<a href='#>
                              <span class='label label-success' style='padding:12.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid 'eee; color:#ddd'>
                              <i class='fa fa-close'></i>  Funds Disbursed! </span></a>";
                                        }else if($item->rfq_status == 1){
                                            echo "<a class='btn btn-default btn-success btn-xs'>RFQ Sent</a>";
                                        }else if(($item->line_manager_status_id == 2) && ($item->c_level_status_id == 4) && ($item->rfq_status != 1) && ($item->rfq_status != 2) ){
                                            echo "<a class='btn btn-default btn-primary btn-xs bootstrap-modal-form-open' data-toggle='modal' data-target='#{{$item->id}}'>Generate RFQ</a>";
                                        }else{

                                            echo "<a href='#>
                              <span class='label label-danger' style='padding:12.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid 'eee; color:#ddd'>
                              <i class='fa fa-close'></i>  Not Approve </span></a>";
                                        }
                                    }else if($getUserStatus == 'finance'){ //Finance/CLevel or other staff with acess to finance module
                                    if($item->rfq_status == 2){
                                        echo "<a class='btn btn-default btn-success btn-xs' style='color:white;'>Funds Disbursed! </a>";
                                    }else if($item->rfq_status == 1){
                                    ?>
                                    <a class='btn btn-default btn-success btn-xs' href="{{url('finance/disburse/'.$item->id)}}">Disburse Funds</a>
                                    <?php
                                    }else if(($item->line_manager_status_id == 2) && ($item->c_level_status_id == 4) && ($item->rfq_status != 1) && ($item->rfq_status != 2) ){
                                        echo "<a class='btn btn-default btn-primary btn-xs bootstrap-modal-form-open'>RFQ Not Sent</a>";
                                    }else{
                                        echo "<a class='btn btn-default btn-danger btn-xs'>Not Approved</a>";
                                    }
                                    }else if($getUserStatus == 'others'){
                                        if($item->rfq_status == 2){
                                            echo "<a class='btn btn-default btn-success btn-xs'>Funds Disbursed</a>";
                                        }else if($item->rfq_status == 1){
                                            echo "<a class='btn btn-default btn-success btn-xs'>RFQ Sent</a>";
                                        }else if(($item->line_manager_status_id == 2) && ($item->c_level_status_id == 4) && ($item->rfq_status != 1) && ($item->rfq_status != 2) ){
                                            echo "<a class='btn btn-default btn-primary btn-xs bootstrap-modal-form-open'>No Acess</a>";
                                        }else{
                                            echo "<a class='btn btn-default btn-danger btn-xs'>Not Approved</a>";
                                        }
                                    }
                                    ?>
                                </div>
                            </td>
                            <div class="modal fade" id="{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title"></h4>
                                        </div>
                                        <div class="modal-body">
                                            <form action = "{{url('requests/rfq/send')}}" method="post" class="bootstrap-modal-form">
                                                <input type="hidden" name="item_id" value="{{$item->id}}">
                                                @if(isset($vendors))
                                                    <div class="form-group col-sm-12">
                                                        <label class="">Select Vendor</label>
                                                        <select name="vendor_id" class="form-control" id="vendor">
                                                            <option value="">Select...</option>
                                                            @foreach($vendors as $vendor)
                                                                <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                @endif
                                                <div class="form-group col-sm-12">
                                                    <label class="">Required Response Time</label>
                                                    <input name="response_date" id="datepicker" type="text" class="form-control"/>
                                                </div>
                                                <div class="form-group col-sm-12">
                                                    <label class="">RFQ Send Options</label><br>
                                                    <input type="radio" name="send_option" value="1" class=""> Send mail to vendor
                                                    <input type="radio" name="send_option" value="2"checked="checked" > Generate Excel sheet for vendor
                                                </div>
                                        </div>
                                        <div class="modal-footer">

                                            <a class="btn btn-default btn-ok" data-dismiss="modal">Cancel</a>
                                            <input type="submit" value="submit"  class="btn btn-danger">
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </tr>
                          @empty
                          @endforelse
  	                        </tfoot>
  	                  </table>
  	               </div>
  	            </div>
  	         </div>
  	         <!-- /.tab-content -->
  	      </div>
  	      <!-- nav-tabs-custom -->
  	   </div>
  	   <!-- /.col -->
  	</section>
  @endsection

  @section('script')
       <script>
         $(function () {
           $("#example1").DataTable();});


         $('.adf').hide();
         $('.cdb').on('click', function(){
           $('.adf').slideToggle();
        
         });

           $('.cadfxx').on('click', function(){
              $('.adf').slideToggle();
           });

       </script>
  @endsection
