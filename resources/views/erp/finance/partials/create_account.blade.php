<h3 align="center" style="color:rgb(171, 135, 236);">Create Account
</h3>
<form method="POST" action="">
    <div class="fixed-wrapper well">
        <div class="row well">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Name</label>
                    <div class="icon-addon addon-md">
                        <select name="account_category" id="inputIncomeName" class="form-control" required="required">
                            <option value="">Select Account Category</option>
                            <option value="1">Savings</option>
                            <option value="2">Current</option>
                            <option value="3">Fixed Deposit</option>
                        </select>
                        <label for="house" class="fa fa-book" rel="tooltip" title="Category"></label>
                        @if ($errors->has('account_category')) <p class="help-block" style="color: red">{{ $errors->first('account_category') }}</p> @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Account Name</label>
                    <div class="icon-addon addon-md">
                        <input required="" type="text" required="" value="{{ old('name') }}"  name="name" class="form-control" placeholder="Account Name">
                        <label for="house" class="fa fa-book" rel="tooltip" title="Name"></label>
                        @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p align="center">
        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
            Create</button>
        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
            Cancel</a>
    </p>
</form>