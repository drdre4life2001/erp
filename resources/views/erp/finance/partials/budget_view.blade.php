@extends('erp.layouts.master')

@section('title')
    Finance - Budget View
@endsection

@section('sidebar')
    @include('erp.partials.sidebar')
@endsection

@section('content')
    <hr>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body" style="background-color: white;">
                        <div class="form-wrap">
                            <div class="pull-left">
                                <a href="{{ url('finance/budget/create') }}" class="btn btn-purple"> <i class="fa fa-plus"></i> Add New</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ url('finance/budget/trash/'.$budget['account']."/".$budget['uid']) }}" id="a_del" class="btn btn-danger">Delete</a>
                            </div>
                            <br> <br>
                            <hr>
                            <form action="{{url('finance/budget-update')}}/{{$budget['account']}}/{{$budget['uid']}}" method="post">
                                <input type="hidden" name="_method" value="PATCH">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputuname_1">Account Number</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                <input disabled type="text" required value="{{$budget['account']}}" class="form-control" id="exampleInputuname_1"
                                                       name="account_number">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputuname_1">Budget Name</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                <input type="text" value="{{$budget['name']}}" class="form-control" id="exampleInputuname_1"
                                                       name="name" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/> <br/>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputuname_1">Income</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                <input type="text" value="{{$budget['income']}}" class="form-control" id="exampleInputuname_1"
                                                       name="income" >
                                                @if ($errors->has('income')) <p class="help-block" style="color: red">{{ $errors->first('income') }}</p> @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputuname_1">Start Date</label>
                                            <span for="">{{$budget['start_date']}}</span>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                <input type="date" value="" required class="form-control" id="exampleInputuname_1"
                                                       name="start_date" value="">
                                                @if ($errors->has('start_dat')) <p class="help-block" style="color: red">{{ $errors->first('start_date') }}</p> @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputuname_1">End Date</label>
                                            <span for="">{{$budget['end_date']}}</span>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                <input type="date" value="" required class="form-control" id="exampleInputuname_1"
                                                       name="end_date" value="">
                                                @if ($errors->has('end_date')) <p class="help-block" style="color: red">{{ $errors->first('end_date') }}</p> @endif

                                            </div>
                                        </div>
                                    </div>
                                </div> <br> <br>

                                {{-- display income sources --}}
                                @if(is_array($budget['income_sources']) ? count($budget['income_sources']) : null)
                                    <div class="well">
                                        <center>
                                            <h4>Income Sources</h4>
                                        </center>
                                        @foreach($budget['income_sources'] as $b)
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="exampleInputuname_1">Name</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                            <select name="income_name[]" id="inputIncomeName" class="form-control" required="required">
                                                                <option value="">Select Source</option>

                                                                @foreach($income_sources as $i)
                                                                    <option value="{{$i->id}}" @if($i->id == $b['id']) selected @endif>{{$i->name}}</option>
                                                                @endforeach

                                                            </select>
                                                            @if ($errors->has('income_name')) <p class="help-block" style="color: red">{{ $errors->first('income_name') }}</p> @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="exampleInputuname_1">Amount</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                            <input type="text" required value="{{number_format($b['amount'])}}" class="form-control" id="exampleInputuname_1"
                                                                   name="income_amount[]" value="">

                                                            @if ($errors->has('income_amount')) <p class="help-block" style="color: red">{{ $errors->first('income_amount') }}</p> @endif

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif

                                @if(is_array($budget['fixed_expenses']) ? count($budget['fixed_expenses']) : null)
                                    <div class="well">
                                        <center>
                                            <h4>Fixed Expenses</h4>
                                        </center>
                                        @foreach($budget['fixed_expenses'] as $f)
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="exampleInputuname_1">Name</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                            <select name="f_name[]" id="inputIncomeName" class="form-control" required="required">
                                                                <option value="">Select Source</option>
                                                                @foreach($f_expenses as $f_exp)
                                                                    <option value="{{$f_exp->id}}" @if($f_exp->id == $f['id']) selected @endif>{{$f_exp->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('f_name')) <p class="help-block" style="color: red">{{ $errors->first('f_name') }}</p> @endif

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="exampleInputuname_1">Amount</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                            <input type="text" required value="{{number_format($f['amount'])}}" class="form-control" id="exampleInputuname_1"
                                                                   name="f_amount[]" value="">
                                                            @if ($errors->has('f_amount')) <p class="help-block" style="color: red">{{ $errors->first('f_amount') }}</p> @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif

                                @if(is_array($budget['variable_expenses']) ? count($budget['variable_expenses']) : null)
                                    <div class="well">
                                        <center>
                                            <h4>Variable Expenses</h4>
                                        </center>
                                        @foreach($budget['variable_expenses'] as $v)
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="exampleInputuname_1">Name</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                            <select name="v_name[]" id="inputIncomeName" class="form-control" required="required">
                                                                <option value="">Select Source</option>

                                                                @foreach($v_expenses as $v_exp)
                                                                    <option value="{{$v_exp->id}}" @if($v_exp->id == $v['id']) selected @endif>{{$v_exp->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('v_name')) <p class="help-block" style="color: red">{{ $errors->first('v_name') }}</p> @endif

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="exampleInputuname_1">Amount</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                            <input type="text" value="{{number_format($v['amount'])}}" class="form-control" id="exampleInputuname_1"
                                                                   name="v_amount[]" required value="">
                                                            @if ($errors->has('v_amount')) <p class="help-block" style="color: red">{{ $errors->first('v_amount') }}</p> @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                                <br/> <br/>
                                <center>
                                    <input type="submit" class="btn btn-purple btn-lg" value="Update Budget">
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection