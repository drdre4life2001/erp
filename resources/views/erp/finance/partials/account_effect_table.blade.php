<div class="box-body">
    <table id="example1" class="table table-bordered table-hover">
       <thead>
          <tr style="color:#8b8b8b">
             <th>S/N </th>
             <th>Account Effect</th>
             <th>Value</th>
          </tr>
       </thead>
       <tbody>
         <?php $num = 1; ?>
          @foreach($effects as $effect)
           <tr>
              <td>{{ $num++ }}</td>
              <td>
                 {{-- <div class="product-img">
                    <img src="{{ asset('erp/dist/img/fcmb.jpg') }}" style="float:left; border-radius:10%; height: 40px; width: 40px; margin-right: 5%; margin-top:1%">
                 </div> --}}
                 <span style="float:left; margin-top:2%; font-weight: bold">{{ ucfirst($effect->name) }}</span>
              </td>
              <td>  
                 <a data-toggle="modal" href='#modal-id{{ $effect->id }}'>
                 <span class="label label-warning" style="padding:2.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                 <i class="fa fa-trash"></i>  Edit </span></a>
                 <a id="a_del" href="{{ url('finance/effect/delete/'.$effect->id) }}">
                 <span class="label label-danger" style="padding:2.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                 <i class="fa fa-trash"></i>  Deactive </span></a>
              </td>
           </tr>

           <div class="modal fade" id="modal-id{{ $effect->id }}">
               <div class="modal-dialog">
                 <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 class="modal-title">Edit- {{ ucfirst($effect->name) }} </h4>
                   </div>
                   <div class="modal-body">
                     <form method="POST" action="{{ url('finance/effects/edit') }}">
                        <h3 align="center">Effect</h3>
                        <div class="form-group">
                            <label>Name</label>
                           <input type="hidden" name="effect_id" value="{{ $effect->id }}">
                           <div class="icon-addon addon-md">
                              <input type="text" class="form-control" name="name" value="{{ $effect->name }}" placeholder="Effect Name">
                              @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                              <label for="house" class="fa fa-book" rel="tooltip" title="Effect Name"></label>
                           </div> <br> <br>
                           
                           <div class="icon-addon addon-md">
                            <label>Value</label>
                            <input type="text" class="form-control" name="value" value="{{ $effect->value }}" placeholder="Effect Value">
                            @if ($errors->has('value')) <p class="help-block" style="color: red">{{ $errors->first('value') }}</p> @endif
                            <label for="house" class="fa fa-book" rel="tooltip" title="Effect Value"></label>
                         </div>
                        </div>
                        <p align="center">
                           <button class="btn btn-large btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                           Update</button>
                        </p>
                     </form>
                   </div>
                   <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                   </div>
                 </div>
               </div>
             </div>
          @endforeach
          </tfoot>
    </table>
 </div>