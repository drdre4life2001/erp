<table id="example1" class="table table-bordered table-hover" >
    <thead class="panel panel-heading">
    <tr>
        <th>S/N</th>
        <th>Account Name</th>
        <th>Account Number</th>
        <th>Ledger Balance Date</th>
        <th>Minimum Balance</th>
        <th>Account Category</th>
        <th>Status</th>
        <th>View</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php $num = 1; foreach($accounts as $account){ ?>
    <tr>
        <td>{{ $num++ }}</td>
        <td>{{ $account['account_name'] }}</td>
        <td>{{ $account['account_number'] }}</td>
        <td>{{ $account['ledger_balance'] }}</td>
        <td>{{ $account['minimum_balance'] }}</td>
        <td>{{ $account['account_category'] }}</td>
        <td>{{ $account['status'] }}</td>
        <td><a href="{{url('finance/accounts/view/budget/'.$account['account_number'])}}" class="btn btn-success table_clicked"> View Budget </a></td>
        <td><a href="{{url('finance/account/delete/'.$account['account_number'])}}" id="a_del" class="btn btn-danger table_clicked"> Delete </a></td>
    </tr>
    <?php } ?>
    </tbody>
</table>