   <h3 align="center" style="color:rgb(171, 135, 236);">Create Budget
   </h3>
<form method="POST" action="">
   <div class="row">
      <div class="col-md-4" style="padding: 0px;">
         <div class="form-group">
            <label>Select Account Number</label>
            <div class="icon-addon addon-md">
               <select name="account_number" id="inputAccount_number" class="form-control" required="required">
                  <option value="">Select Account</option>
                  @foreach($accounts as $acc)
                     <option value="{{ $acc['account_number'] }}">{{ $acc['account_number'] }} | {{ $acc['account_name'] }}</option>
                  @endforeach
               </select>
               <label for="house" class="fa fa-money" rel="tooltip" title="house"></label>
               @if ($errors->has('account_number')) <p class="help-block" style="color: red">{{ $errors->first('account_number') }}</p> @endif
            </div>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label>Name of Budget</label>
            <div class="icon-addon addon-md">
               <input required="" type="text" required="" value="{{ old('name') }}"  name="name" class="form-control" placeholder="Enter Value">
               <label for="house" class="fa fa-book" rel="tooltip" title="Name of budget"></label>
               @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
            </div>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label>Income</label>
            <div class="icon-addon addon-md">
               <input required="" type="number" required="" value="{{ old('income') }}"  name="income" class="form-control" placeholder="Total income amount">
               <label for="house" class="fa fa-book" rel="tooltip" title="Income amount in total"></label>
               @if ($errors->has('income')) <p class="help-block" style="color: red">{{ $errors->first('income') }}</p> @endif
            </div>
         </div>
      </div>
   </div>
   <center style="color:rgb(171, 135, 236);">
      <h4>Add Income Source</h4x>
   </center>
   <div class="income-wrapper well">
      <div class="row well">
            <div class="col-lg-6">
                <div class="form-group">
                   <label>Name</label>
                   <div class="icon-addon addon-md">
                      <select name="income_name[]" id="inputIncomeName" class="form-control" required="required">
                          <option value="">Select Source</option>
                          @foreach($income_sources as $income)
                              <option value="{{$income->id}}">{{$income->name}}</option>
                         @endforeach
                      </select>
                      <label for="house" class="fa fa-book" rel="tooltip" title="Name of budget"></label>
                      @if ($errors->has('income_name')) <p class="help-block" style="color: red">{{ $errors->first('income_amount') }}</p> @endif
                   </div>
                </div>        
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                   <label>Amount</label>
                   <div class="icon-addon addon-md">
                      <input required="" type="number" required="" value="{{ old('income_amount') }}"  name="income_amount[]" class="form-control" placeholder="Amount">
                      <label for="house" class="fa fa-book" rel="tooltip" title="Amount"></label>
                      @if ($errors->has('income_amount')) <p class="help-block" style="color: red">{{ $errors->first('income_amount') }}</p> @endif
                   </div>
                </div>        
            </div>
            <span class="pull-right add-income-source" style="color: blue;">Add more <i class="fa fa-plus"></i></span>
      </div>
   </div>

   <center style="color:rgb(171, 135, 236);">
      <h4>Add Fixed Expense</h4>
   </center>
   <div class="fixed-wrapper well">
     <div class="row well">
           <div class="col-lg-6">
               <div class="form-group">
                  <label>Name</label>
                  <div class="icon-addon addon-md">
                      <select name="f_name[]" id="inputIncomeName" class="form-control" required="required">
                          <option value="">Select Source</option>
                          @foreach($f_expenses as $f)
                              <option value="{{$f->id}}">{{$f->name}}</option>
                          @endforeach
                      </select>
                     <label for="house" class="fa fa-book" rel="tooltip" title="Name of Expense"></label>
                     @if ($errors->has('f_name')) <p class="help-block" style="color: red">{{ $errors->first('f_name') }}</p> @endif
                  </div>
               </div>        
           </div>
           <div class="col-lg-6">
               <div class="form-group">
                  <label>Amount</label>
                  <div class="icon-addon addon-md">
                     <input required="" type="number" required="" value="{{ old('f_amount') }}"  name="f_amount[]" class="form-control" placeholder="Amount">
                     <label for="house" class="fa fa-book" rel="tooltip" title="Amount"></label>
                     @if ($errors->has('f_amount')) <p class="help-block" style="color: red">{{ $errors->first('f_amount') }}</p> @endif
                  </div>
               </div>        
           </div>
           <span class="pull-right add-fixed" style="color: blue;">Add more <i class="fa fa-plus"></i></span>
     </div> 
   </div>


{{--    class TestStringConcatenation2{
 public static void main(String args[]){
 
   
    int balqis = 0;
    int count = 0;
    for (int her_age = 1; her_age <= 10; her_age++) {
        balqis += 1;
        System.out.println("The sum of "+balqis+" plus 1"+ is :"+balqis);
    }
  
 }
}
 --}}
   
   <center style="color:rgb(171, 135, 236);">
      <h4>Add Variable Expense</h4>
   </center>
   <div class="variable-wrapper well">
      <div class="row well">
            <div class="col-lg-6">
                <div class="form-group">
                   <label>Name</label>
                   <div class="icon-addon addon-md">
                       <select name="v_name[]" id="inputIncomeName" class="form-control" required="required">
                           <option value="">Select Source</option>
                           @foreach($v_expenses as $v)
                               <option value="{{$v->id}}">{{$v->name}}</option>
                           @endforeach
                       </select>
                      <label for="house" class="fa fa-book" rel="tooltip" title="Name of budget"></label>
                      @if ($errors->has('v_name')) <p class="help-block" style="color: red">{{ $errors->first('v_name') }}</p> @endif
                   </div>
                </div>        
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                   <label>Amount</label>
                   <div class="icon-addon addon-md">
                      <input required="" type="number" required="" value="{{ old('v_amount') }}"  name="v_amount[]" class="form-control" placeholder="Amount">
                      <label for="house" class="fa fa-book" rel="tooltip" title="Amount"></label>
                      @if ($errors->has('v_amount')) <p class="help-block" style="color: red">{{ $errors->first('v_amount') }}</p> @endif
                   </div>
                </div>        
            </div>
           <span class="pull-right add-variable" style="color: blue;">Add more <i class="fa fa-plus"></i></span>
      </div>   
   </div>

   <div class="row well">
         <center>
            <h3>Date</h3>
         </center>
         <div class="col-lg-6">
             <div class="form-group">
                <label>Start Date</label>
                <div class="icon-addon addon-md">
                   <input required="" type="text" required="" name="start_date" class="form-control fromDate" placeholder="Start Date">
                   <label for="house" class="fa fa-book" rel="tooltip" title="Start Date"></label>
                   @if ($errors->has('start_date')) <p class="help-block" style="color: red">{{ $errors->first('start_date') }}</p> @endif
                </div>
             </div>        
         </div>
         <div class="col-lg-6">
             <div class="form-group">
                <label>End Date</label>
                <div class="icon-addon addon-md">
                   <input required="" type="text" required="" name="end_date" class="form-control toDate" placeholder="End Date">
                   <label for="house" class="fa fa-book" rel="tooltip" title="End Date"></label>
                   @if ($errors->has('end_date')) <p class="help-block" style="color: red">{{ $errors->first('end_date') }}</p> @endif
                </div>
             </div>        
         </div>
   </div>
   
   
   <p align="center">
      <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
      Add</button>
      <a href="" class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
      Cancel</a>
   </p>
</form>