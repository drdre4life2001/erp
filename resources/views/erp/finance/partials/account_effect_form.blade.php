<div class="col-md-10">
    <form method="POST" action="">
      {{ csrf_field() }}
       <h3 align="center">Account Effect</h3>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Name</label>
                    <div class="icon-addon addon-md">
                       <input type="text" class="form-control" name="name" placeholder="Name">
                       <label for="house" class="fa fa-university" rel="tooltip" title="Effect Name"></label>
                       @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                    </div>
                 </div>
            </div>

            

            <div class="col-lg-6">
                <div class="form-group">
                    <label>Value</label>
                    <div class="icon-addon addon-md">
                       <input type="number" class="form-control" name="value" placeholder="Number">
                       <label for="house" class="fa fa-university" rel="tooltip" title="Effect Value"></label>
                       @if ($errors->has('value')) <p class="help-block" style="color: red">{{ $errors->first('value') }}</p> @endif
                    </div>
                 </div>
            </div>
        </div>
       
       <p align="center">
          <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
          Add <i class="fa fa-plus"></i></button>
          <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
          Cancel</a>
       </p>
    </form>
 </div>