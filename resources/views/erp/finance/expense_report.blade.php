@extends('erp.layouts.master')
  
  @section('title')
    Finance - Expense Report
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

@section('content')
   <section class="content-header">
      <h1>     Expense Sheet Report
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Expense Sheet Report</li>
      </ol>
   </section>
   <section class="content">
   <div class="col-md-12">
      <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>Expense Sheet Report
               </b></a>
            </li>
         </ul>
         <div class="tab-content" style="padding: 2%">
            <div class="tab-pane active" id="tab_1">
               <hr/ style="clear: both">
               <div id="search" style="background: #eee; height: auto; padding: 2% 5%">
                  <form method="POST" action="{{ url('finance/expense_report') }}">
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label>From:</label>
                           <div class="input-group">
                              <div class="input-group-addon">
                                 <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right fromDate" name="from" id="datepicker">
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label>To:</label>
                           <div class="input-group">
                              <div class="input-group-addon">
                                 <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right toDate" name="to" id="datepicker2">
                           </div>
                        </div>
                     </div>
                     <p align="center">
                        <button class="btn btn-large btn-danger form_clicked"><i class="fa fa-search" aria-hidden="true"></i>
                        Generate Expense Report </button>
                     </p>
                  </form>
               </div>
               <div class="box-body">
                  <table id="example1" class="table table-bordered table-hover">
                     <thead>
                        <tr style="color:#8b8b8b">
                           <th>S/N </th>
                           <th>Narration</th>
                           <th>Category</th>
                           <th>Sub-Category</th>
                           <th>Day</th>
                           <th>Amount</th>
                           {{-- <th>Action</th> --}}
                        </tr>
                     </thead>
                     <tbody>
                        <?php $num = 1; ?>
                        @forelse($result as $r)
                           <tr>
                              <?php $date = date('d-m-Y', strtotime($r->created_at)); ?>
                              <td>{{$num++ }}</td>
                              <td> {{ $r->title }} </td>
                              <td> {{ $r->category }} </td>
                              <td> {{ $r->sub_category}} </td>
                              <td> {{ $date }} </td>
                              <td> {{ number_format($r->total, 2) }} </td>
                           </tr>
                        @empty
                        @endforelse
                     </tbody>
                        </tfoot>
                  </table>
               </div>
            </div>
         </div>
      </div>
      </div>
   </section>
@endsection

@section('script')
    <script>
      $(function () {
        $("#example1").DataTable();});


      $('.adf').hide();
      $('.cdb').on('click', function(){
      $('.adf').slideToggle();});

    $('.cadfxx').on('click', function(){
       $('.adf').slideToggle();
    });

    </script>
@endsection