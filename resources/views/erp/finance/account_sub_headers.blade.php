@extends('erp.layouts.master')
  
  @section('title')
    Finance - Account Sub-Headers
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
   <section class="content-header">
      <h1>      Account Sub Headers
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Account Sub Headers</li>
      </ol>
   </section>
   <section class="content">
      <div class="col-md-12">
         <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
               <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Account Sub Headers </b></a></li>
            </ul>
            <div class="tab-content" style="padding: 2%">
               <div class="tab-pane active" id="tab_1">
                  <p align="right">
                     <button class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add  Account Sub Headers</button>
                  </p>
                  <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                     <div class="col-md-1 hidden-sm hidden-xs"></div>
                     <div class="col-md-10">
                        <form method="POST" action="">
                           {{ csrf_field() }}
                           <h3 align="center">Account Sub Headers</h3>
                           <div class="col-lg-6">
                              <div class="form-group">
                                 <label>Sub-Header Name</label>
                                 <div class="icon-addon addon-md">
                                    <input type="text" class="form-control" name="name" placeholder="Sub-Header Name">
                                    <label for="house" class="fa fa-user" rel="tooltip" title="Bank Name"></label>
                                    @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label>Select Header</label>
                                 <div class="icon-addon addon-md">
                                    <select class="form-control" name="account_header">
                                       <option value=""> Please Select</option>
                                       @foreach($headers as $header)
                                          <option value="{{ $header->id }}">{{ $header->name }}</option>
                                       @endforeach
                                    </select>
                                    <label for="house" class="fa fa-building" rel="tooltip" title="Bank Name"></label>
                                    @if ($errors->has('account_header')) <p class="help-block" style="color: red">{{ $errors->first('account_header') }}</p> @endif

                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group">
                                 <label>Select Bank Account</label>
                                 <div class="icon-addon addon-md">
                                    <select class="form-control" name="bank_account_id">
                                       <option value="">--Please select--</option>
                                       @foreach($banks as $bank)
                                           <option value="{{$bank->id}}">{{$bank->bank_name}}[{{$bank->account_number}}]</option>
                                       @endforeach
                                    </select>
                                    <label for="house" class="fa fa-university" rel="tooltip" title="Bank Name"></label>
                                    @if ($errors->has('bank_account_id')) <p class="help-block" style="color: red">{{ $errors->first('bank_account_id') }}</p> @endif
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                                <p align="center">
                                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                                        Add Sub Header</button>
                                        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                                        Cancel</a>
                                     </p>
                           </div>
                           
                        </form>
                     </div>
                     <div class="col-md-1 hidden-sm hidden-xs"></div>
                  </div>
                  <hr/ style="clear: both">
                  <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                           <tr style="color:#8b8b8b">
                              <th>S/N </th>
                              <th>Header</th>
                              <th>Sub Header</th>
                              <th>Bank Details</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                        @if(!empty($item))
                            <?php $i =  1; ?>
                            @foreach($item as $items)
                              <tr>
                                 <td>{{ $i++ }}</td>
                                 <td>{{ $items->header }}</td>
                                 <td>{{ $items->sub_header }}</td>
                                 <td>{{ $items->bank_name }}[{{$items->account_number}}]</td>
                                 <td>  
                                    <a data-toggle="modal" href='#modal-id{{ $items->id }}'>
                                    <span class="label label-warning" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                    <i class="fa fa-trash"></i>  Edit </span></a>
                                    <a href="{{ url('finance/account_sub_header/delete/'.$items->id) }}" id="a_del">
                                    <span class="label label-danger" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                    <i class="fa fa-trash"></i>  Delete </span></a>
                                 </td>
                              </tr>

                              {{-- Modal to update subheaders --}}
                              <div class="modal fade" id="modal-id{{ $items->id }}">
                                 <div class="modal-dialog">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                          <h4 class="modal-title">Update Subheader {{ $items->sub_header }}</h4>
                                       </div>
                                       <div class="modal-body">
                                          <form method="POST" action="{{ url('finance/account_sub_header/edit') }}">
                                             {{ csrf_field() }}
                                             <h3 align="center">Account Sub Headers</h3>
                                             <div class="col-lg-6">
                                                <div class="form-group">
                                                   <label>Sub-Header Name</label>
                                                   <div class="icon-addon addon-md">
                                                      <input type="text" class="form-control" value="{{ $items->sub_header }}" name="name" placeholder="Sub-Header Name">
                                                      <label for="house" class="fa fa-user" rel="tooltip" title="Bank Name"></label>
                                                      @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                                                   </div>
                                                </div>
                                                <div class="form-group">
                                                   <label>Select Header</label>
                                                   <div class="icon-addon addon-md">
                                                      <select class="form-control account_header" name="account_header">
                                                         <option value=""> Please Select</option>
                                                         <?php $db_val = $items->header_id; ?>
                                                         @foreach($headers as $header)
                                                            <option value="{{ $header->id }}" @if($header->id == $db_val) selected @endif>{{ $header->name }}</option>
                                                         @endforeach
                                                      </select>
                                                      <label for="house" class="fa fa-building" rel="tooltip" title="Bank Name"></label>
                                                      @if ($errors->has('account_header')) <p class="help-block" style="color: red">{{ $errors->first('account_header') }}</p> @endif

                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-lg-6">
                                                <div class="form-group">
                                                   <label>Add Parent Sub-Header</label>
                                                   <div class="icon-addon addon-md">
                                                      <select class="form-control account_sub_header" name="parent_id">
                                                      </select>
                                                      <label for="house" class="fa fa-building" rel="tooltip" title="Bank Name"></label>
                                                      {{-- @if ($errors->has('parent_id')) <p class="help-block" style="color: red">{{ $errors->first('parent_id') }}</p> @endif --}}
                                                   </div>
                                                </div>
                                                <div class="form-group">
                                                   <label>Select Bank Account</label>
                                                   <div class="icon-addon addon-md">
                                                      <select class="form-control" name="bank_account_id">
                                                         <option value="">--Please select--</option>
                                                         <?php $bank_db_val = $items->bank_account_id; ?>
                                                         @foreach($banks as $bank)
                                                             <option value="{{$bank->id}}" @if($bank_db_val == $bank->id) selected @endif>{{$bank->bank_name}}[{{$bank->account_number}}]</option>
                                                         @endforeach
                                                      </select>
                                                      <label for="house" class="fa fa-university" rel="tooltip" title="Bank Name"></label>
                                                      @if ($errors->has('bank_account_id')) <p class="help-block" style="color: red">{{ $errors->first('bank_account_id') }}</p> @endif
                                                   </div>
                                                </div>
                                             </div>
                                             <p align="center">
                                                <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                                                Update</button>
                                             </p>
                                          </form>
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           @endforeach
                        @endif
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
@endsection

@section('script')
    <script>
      $(function () {
        $("#example1").DataTable();

            });


      $('.adf').hide();
      $('.cdb').on('click', function(){
        $('.adf').slideToggle();
     
      });

        $('.cadfxx').on('click', function(){
           $('.adf').slideToggle();
        });

        $('.account_header').change(function(e) {
            var parent = e.target.value;
               // alert(parent);
            $.get('/finance/get-sub-headers/'+ parent, function(data) {
                  console.log(data);
                var SelectOption ="<option  value=''>--Select Sub Header--</option>";
                if (data.length < 1) {
                    alert('Oops! No sub header available.');
                    $(".account_header").focus();
                    $(".account_sub_header").empty();
                }else{

                    $.each(data, function(index,value)
                    {
                        SelectOption +=  "<option value='"+ value.id   +"'>" + value.name + "</option>";
                    });
                }
                $( ".account_sub_header" ).html(SelectOption); //});
            });
        });
    </script>
@endsection
