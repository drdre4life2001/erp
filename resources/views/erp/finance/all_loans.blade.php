@extends('erp.layouts.master')
  
  @section('title')
    Finance - All Loans
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
      <section class="content-header">
         <h1>
            Company Loan
         </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Company Loan</li>
         </ol>
      </section>
      <section class="content">
         <div class="col-md-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Company Loan
                     </b></a>
                  </li>
               </ul>
               <div class="tab-content" style="padding: 2%">
                  <div class="tab-pane active" id="tab_1">
                     <p align="right">
                        <button class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add  Company Loan
                        </button>
                     </p>
                     <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                        <div class="col-md-10">
                           <form method="POST">
                              <h3 align="center">Create Company Loan
                              </h3>
                              <div class="col-md-6" style="padding: 0px;">
                                 <div class="form-group">
                                    <label>Company name:</label>
                                    <div class="icon-addon addon-md">
                                       <select class="form-control organization_search" style="width:100%;" name="company_id" required="" >
                                       </select>
                                       <label for="house" class="fa fa-university" rel="tooltip" title="Company"></label>
                                       @if ($errors->has('company_id')) <p class="help-block" style="color: red">{{ $errors->first('company_id') }}</p> @endif
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label>Amount </label>
                                    <div class="icon-addon addon-md">
                                       <input type="number" required="" class="form-control" placeholder="Amount" name="amount">
                                       <label for="house" class="fa fa-money" rel="tooltip" title="house"></label>
                                       @if ($errors->has('amount')) <p class="help-block" style="color: red">{{ $errors->first('amount') }}</p> @endif
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label>Description for Loan Collection</label>
                                    <div class="icon-addon addon-md">
                                       <input type="text" required="" name="description" class="form-control" placeholder="Description for Loan Collection">
                                       <label for="house" class="fa fa-book" rel="tooltip" title="Description"></label>
                                       @if ($errors->has('description')) <p class="help-block" style="color: red">{{ $errors->first('description') }}</p> @endif
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6" style="padding-right: 0px;">
                                 <div class="form-group">
                                    <label>Select Bank:</label>
                                    <div class="icon-addon addon-md">
                                       <select class="form-control" name="bank" required="">
                                          <option value="">Choose Bank</option>
                                          @if(!empty($banks))
                                             @forelse($banks as $bank)
                                                <option value="{{ $bank->name }}"> {{ $bank->name }} </option>
                                             @empty
                                             @endforelse
                                          @endif
                                       </select>
                                       <label for="house" class="fa fa-university" rel="tooltip" title="Description"></label>
                                       @if ($errors->has('bank')) <p class="help-block" style="color: red">{{ $errors->first('bank') }}</p> @endif
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label>Interest Rate</label>
                                    <div class="icon-addon addon-md">
                                       <input type="number" name="interest_rate" required class="form-control" placeholder="Interest Rate in Percentage  (%)">
                                       <label for="house" class="fa fa-book" rel="tooltip" title="Period"></label>
                                       @if ($errors->has('interest_rate')) <p class="help-block" style="color: red">{{ $errors->first('interest_rate') }}</p> @endif
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label>Period</label>
                                    <div class="icon-addon addon-md">
                                       <input type="number" name="period" class="form-control" placeholder="Period in Months e.g 3">
                                       <label for="house" class="fa fa-book" rel="tooltip" title="Period"></label>
                                       @if ($errors->has('period')) <p class="help-block" style="color: red">{{ $errors->first('period') }}</p> @endif
                                    </div>
                                 </div>
                              </div>
                              <p align="center">
                                 <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                                 Save</button>
                                 <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                                 Cancel</a>
                              </p>
                           </form>
                        </div>
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                     </div>
                     <hr/ style="clear: both">
                     <div class="box-body">
                        <table id="example1" class="table table-bordered table-hover">
                           <thead>
                              <tr style="color:#8b8b8b">
                                 <th>S/N</th>
                                 <th>Company Name</th>
                                 <th>Amout</th>
                                 <th>Interest Rate</th>
                                 <th>Bank</th>
                                 <th>Description</th>
                                 <th>Period</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php $num = 1; ?>
                              @forelse($company_loans as $loan)
                                 <tr>
                                    <td> {{ $num++ }} </td>
                                    <td>{{ $loan->company_name }}</td>
                                    <td>&#x20a6;{{ number_format($loan->amount, 2) }}</td>
                                    <td>
                                       {{ $loan->interest_rate }}
                                       <div class="progress progress-xs">
                                          <div class="progress-bar progress-bar-warning" style="width: {{ $loan->interest_rate*100 }}%"></div>
                                       </div>
                                    </td>
                                    <td>{{ $loan->bank }}</td>
                                    <td>7
                                    </td>
                                    <td>{{ $loan->period }}</td>
                                    <td>  
                                       <a target="_blank" href="{{ url('finance/add-loan-repayment', $loan->id) }}">
                                       <span class="label label-success" style="padding:2.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                       <i class="fa fa-money"></i>  Add Loan Repayment </span></a>
                                       <a target="_blank" href="{{ url('finance/view-payment-schedule', $loan->id) }}">
                                       <span class="label label-danger" style="padding:2.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                       <i class="fa fa-money"></i>  View Payment Schedule </span></a>
                                    </td>
                                 </tr>

                              @empty
                              @endforelse
                              </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
               <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </section>
  @endsection

  @section('script')
       <script type="text/javascript">
         
         $(function () {
           $("#example1").DataTable();});


         $('.adf').hide();
         $('.cdb').on('click', function(){
           $('.adf').slideToggle();
         });

         @if ($errors->has('company_id') || $errors->has('bank') || $errors->has('description') || $errors->has('interest_rate') || $errors->has('amount') || $errors->has('period'))
           $('.adf').slideToggle();
         @endif

           $('.cadfxx').on('click', function(){
              $('.adf').slideToggle();
           });

       </script>
  @endsection
