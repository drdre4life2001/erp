@extends('erp.layouts.master')
  
  @section('title')
    Finance - Salvage Items
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')

  	<section class="content-header">
  	   <h1>      Item Salvage / Depreciation And Amortization Schedules
  	   </h1>
  	   <ol class="breadcrumb">
  	      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  	      <li class="active">Depreciation And Amortization Schedules</li>
  	   </ol>
  	</section>
  	<section class="content">
  	  <div class="col-md-12">
  	   <div class="nav-tabs-custom">
  	      <ul class="nav nav-tabs">
  	         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Depreciation And Amortization Schedules </b></a></li>
  	      </ul>
  	      <div class="tab-content" style="padding: 2%">
  	         <div class="tab-pane active" id="tab_1">
  	            <p align="right">
  	               <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add  Depreciation And Amortization Schedules</button>
  	            </p>
  	            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
  	               <div class="col-md-1 hidden-sm hidden-xs"></div>
  	               <div class="col-md-10">
  	                  <form method="POST" action="">
  	                     <h3 align="center">Depreciation And Amortization Schedules</h3>
  	                     <div class="col-lg-6">
  	                        <div class="form-group">
  	                           <label>Name</label>
  	                           <div class="icon-addon addon-md">
  	                              <input type="text" class="form-control" name="name" placeholder="Name">
  	                              <label for="house" class="fa fa-user" rel="tooltip" title="Bank Name"></label>
                                  @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
  	                           </div>
  	                        </div>
  	                     </div>
  	                     <div class="col-lg-6">
  	                        <div class="form-group">
  	                           <label>Value</label>
  	                           <div class="icon-addon addon-md">
  	                              <input type="number" class="form-control" name="value" placeholder="Value">
  	                              <label for="house" class="fa fa-money" rel="tooltip" title="Bank Name"></label>
                                  @if ($errors->has('value')) <p class="help-block" style="color: red">{{ $errors->first('value') }}</p> @endif
  	                           </div>
  	                        </div>
  	                     </div>
  	                     <div class="col-md-12">
  	                        <div class="form-group">
  	                           <label>Period (in months)</label>
  	                           <div class="icon-addon addon-md">
  	                              <input type="number" class="form-control" name="period" placeholder="Period (in months)">
  	                              <label for="house" class="fa fa-money" rel="tooltip" title="Bank Name"></label>
                                  @if ($errors->has('period')) <p class="help-block" style="color: red">{{ $errors->first('period') }}</p> @endif
  	                           </div>
  	                        </div>
  	                     </div>
  	                     <p align="center">
  	                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
  	                        Add </button>
  	                        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
  	                        Cancel</a>
  	                     </p>
  	                  </form>
  	               </div>
  	               <div class="col-md-1 hidden-sm hidden-xs"></div>
  	            </div>
  	            <hr/ style="clear: both">
  	            <div class="box-body">
  	               <table id="example1" class="table table-bordered table-hover">
  	                  <thead>
  	                     <tr style="color:#8b8b8b">
  	                        <th>S/N </th>
  	                        <th>Name</th>
  	                        <th>Period</th>
  	                        <th>Value</th>
  	                        <th>Action</th>
  	                     </tr>
  	                  </thead>
  	                  <tbody>
                        @if(!empty($item))
                        <?php $i = 1; ?>
                          @foreach($item as $items)
                            <tr>
                               <td>{{ $i++ }}</td>
                              <td>{{ $items->name }}</td>
                              <td>{{ $items->period }} months</td>
                              <td>{{ number_format($items->value) }}</td>
                               <td>  
                                  <a data-toggle="modal" href="#modal-id{{ $items->id }}">
                                  <span class="label label-warning" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                  <i class="fa fa-trash"></i>  Edit </span></a>
                                  <a href="{{ url('finance/salvage_items/delete/'.$items->id) }}" id="a_del">
                                  <span class="label label-danger" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                  <i class="fa fa-trash"></i>  Delete </span></a>
                               </td>
                            </tr>

                            <div class="modal fade" id="modal-id{{ $items->id }}">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Update Salvage Item</h4>
                                  </div>
                                  <div class="modal-body">
                                    <form method="POST" action="{{ url('/finance/salvage_items/edit') }}">
                                       <h3 align="center">Depreciation And Amortization Schedules</h3>
                                       <div class="col-lg-6">
                                          <div class="form-group">
                                             <label>Name</label>
                                             <div class="icon-addon addon-md">
                                                <input type="text" class="form-control" name="name" value="{{ $items->name }}" placeholder="Name">
                                                <label for="house" class="fa fa-user" rel="tooltip" title="Bank Name"></label>
                                                @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                                             </div>
                                          </div>
                                       </div>
                                       <input type="hidden" name="item_id" value="{{ $items->id }}">
                                       <div class="col-lg-6">
                                          <div class="form-group">
                                             <label>Value</label>
                                             <div class="icon-addon addon-md">
                                                <input type="number" class="form-control" name="value" value="{{ $items->value }}" placeholder="Value">
                                                <label for="house" class="fa fa-money" rel="tooltip" title="Bank Name"></label>
                                                @if ($errors->has('value')) <p class="help-block" style="color: red">{{ $errors->first('value') }}</p> @endif
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <label>Period (in months)</label>
                                             <div class="icon-addon addon-md">
                                                <input type="number" class="form-control" name="period" value="{{ $items->period }}" placeholder="Period (in months)">
                                                <label for="house" class="fa fa-money" rel="tooltip" title="Bank Name"></label>
                                                @if ($errors->has('period')) <p class="help-block" style="color: red">{{ $errors->first('period') }}</p> @endif
                                             </div>
                                          </div>
                                       </div>
                                       <p align="center">
                                          <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                                          Update </button>
                                       </p>
                                    </form>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          @endforeach
                        @endif
                        
  	                     </tfoot>
  	               </table>
  	            </div>
  	         </div>
  	      </div>
  	   </div>
  	   </div>
  	</section>
  @endsection

  @section('script')
    <script>
      $(function () {
        $("#example1").DataTable(); });
      $('.adf').hide();
      $('.cdb').on('click', function(){
        $('.adf').slideToggle();
     
      });

        $('.cadfxx').on('click', function(){
           $('.adf').slideToggle();
        });

    </script>
@endsection