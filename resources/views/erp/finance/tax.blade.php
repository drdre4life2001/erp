@extends('erp.layouts.master')
  
  @section('title')
    Finance - Tax Management
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
    <section class="content-header">
   <h1>
      Tax
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Tax Type</li>
   </ol>
</section>
<section class="content">
      <div class="col-md-12">
   <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Tax Type </b></a></li>
      </ul>
      <div class="tab-content" style="padding: 2%">
         <div class="tab-pane active" id="tab_1">
            <p align="right">
               <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white;"></i>  Add Tax Type</button>
            </p>
            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
               <div class="col-md-1 hidden-sm hidden-xs"></div>
               <div class="col-md-10">
                  <form method="POST" action="">
                     <h3 align="center">Tax Type</h3>
                     <div class="col-md-12" style="padding: 0px;">
                        <div class="form-group">
                           <label>Tax Name</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name">
                              @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                              <label for="name" class="fa fa-user" rel="tooltip" title="Name"></label>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12" style="padding-right: 0px;">
                        <div class="form-group">
                           <label>Rate</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" name="rate" value="{{ old('rate') }}" placeholder="Rate">
                              <label for="house" class="fa fa-book" rel="tooltip" title="Rate"></label>
                              @if ($errors->has('rate')) <p class="help-block" style="color: red">{{ $errors->first('rate') }}</p> @endif
                           </div>
                        </div>
                     </div>
                     <p align="center">
                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                        Add New Item</button>
                        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                        Cancel</a>
                     </p>
                  </form>
               </div>
               <div class="col-md-1 hidden-sm hidden-xs"></div>
            </div>
            <hr/ style="clear: both">
            <div class="box-body">
               <table id="example1" class="table table-bordered table-hover">
                  <thead>
                     <tr style="color:#8b8b8b">
                        <th>S/N</th>
                        <th>Name</th>
                        <th>Rate</th>
                        <th>Date Added</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php $inc = 1; ?>
                     @foreach($items as $item)
                        <tr>
                           <td>{{ $inc++ }}</td>
                           <td>{{ ucfirst($item->name) }}</td>
                           <td>{{ $item->rate }}</td>
                           <td>{{ date("M d, Y",strtotime($item->created_at)) }}</td>
                           <td>  
                              <a data-toggle="modal" href='#modal-id{{ $item->id }}'>
                              <span class="label label-warning" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                              <i class="fa fa-trash"></i>  Edit </span></a>
                              <a href="{{ url('finance/tax/delete/'.$item->id) }}" id="a_del">
                              <span class="label label-danger" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                              <i class="fa fa-trash"></i>  Trash </span></a>
                           </td>
                        </tr>

                        <div class="modal fade" id="modal-id{{ $item->id }}">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Edit Item ({{ ucfirst($item->name)}})</h4>
                              </div>
                              <div class="modal-body">
                                <form method="POST" action="{{ url('finance/tax/edit') }}">
                                   <h3 align="center">Item</h3>
                                   <div class="col-md-12" style="padding: 0px;">
                                      <div class="form-group">
                                         <label>Item Name</label>
                                         <div class="icon-addon addon-md">
                                            <input type="text" class="form-control" name="name" value="{{ $item->name }}" placeholder="Name">
                                            @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                                            <label for="name" class="fa fa-user" rel="tooltip" title="Name"></label>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="col-md-12" style="padding-right: 0px;">
                                      <div class="form-group">
                                         <label>Rate</label>
                                         <div class="icon-addon addon-md">
                                            <input type="text" class="form-control" name="rate" value="{{ $item->rate }}" placeholder="Tax Telephone">
                                            @if ($errors->has('rate')) <p class="help-block" style="color: red">{{ $errors->first('rate') }}</p> @endif
                                            <label for="house" class="fa fa-mobile" rel="tooltip" title="Telephone"></label>
                                         </div>
                                      </div>
                                   </div>
                                   <input type="hidden" name="tax_id" value="{{ $item->id }}">
                                   <p align="center">
                                      <button class="btn btn-large btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                                      Update Item</button>
                                   </p>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                     @endforeach
                     </tfoot>
               </table>
            </div>
         </div>
      </div>
   </div>
   </div>
</section>

{{-- edit modal --}}

@endsection

@section('script')
    <script>
      $(function () {
        $("#example1").DataTable();
      });


      $('.adf').hide();
      $('.cdb').on('click', function(){
        $('.adf').slideToggle();
     
      });
      @if ($errors->has('name') || $errors->has('rate'))
        $('.adf').slideToggle();
      @endif

        $('.cadfxx').on('click', function(){
           $('.adf').slideToggle();
        });


    </script>
@endsection