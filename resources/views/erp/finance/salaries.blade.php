@extends('erp.layouts.master')
  
  @section('title')
    Finance - Salaries Disbursement
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
      <section class="content-header">
         <h1>     Disburse Payroll Salaries/Tax
         </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Disburse Payroll Salaries/Tax</li>
         </ol>
      </section>
      <section class="content">
      <div class="col-md-12">
         <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
               <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>
                  </b></a>
               </li>
            </ul>
            <div class="tab-content" style="padding: 2%">
               <div class="tab-pane active" id="tab_1">
                  <hr/ style="clear: both">
                  <div class="box-body">
                     <table id="example1" class="table table-bordered table-hover">
                        <thead>
                           <tr style="color:#8b8b8b">
                              <th>S/N </th>
                              <th>Tax Amount 1</th>
                              <th>Salary Amount</th>
                              <th>Month</th>
                              <th>Year</th>
                              <th>Status</th>
                              <th>Generated At</th>
                              <th>Salaries</th>
                              <th>Tax</th>
                           </tr>
                        </thead>
                        <tbody>
                          <?php $inc = 1; ?>
                          @forelse($payroll_summaries as $salary)
                            <tr>
                               <td>{{ $inc++ }} </td>
                               <td>&#x20a6;{{ number_format($salary->tax, 2) }} </td>
                               <td>
                                  &#x20a6;{{ number_format($salary->amount, 2) }}
                                  <div class="progress progress-xs">
                                  </div>
                               </td>
                               <td>{{ $salary->month }}</td>
                               <td>{{ $salary->year }}</td>
                               <td>{{ $salary->status }}</td>
                               <td>{{ $salary->created_at }}</td>
                               <td>  
                                @if($salary->status == "paid")
                                  <a href="#" title="Paid!" disabled>
                                  <span class="label label-danger" style="padding:12.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                  <i class="fa fa-money"></i>  Paid </span></a>
                                @else
                                  <a href="#modal-salaries{{$salary->id}}"
                                                                       data-toggle="modal">
                                  <span class="label label-success" style="padding:12.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                  <i class="fa fa-money"></i>  Pay Salary </span></a>
                                @endif
                               </td>
                               <td>  
                                @if($salary->tax_status == "pending" || $salary->tax_status == NULL)
                                  <a href="#modal-tax{{$salary->id}}" data-toggle="modal">
                                  <span class="label label-success" style="padding:12.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                  <i class="fa fa-money"></i>  Pay Tax </span></a>
                                @else
                                  <a href="#">
                                  <span class="label label-danger" style="padding:12.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                  <i class="fa fa-money"></i>  Tax Paid </span></a>
                                @endif
                               </td>
                               <td>  
                            </tr>

                            {{-- modal for salary payment --}}
                            <div class="modal fade" id="modal-salaries{{$salary->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Pay Salaries for (<span style="color:red;">{{ $salary->month }} {{ $salary->year }}</span>) </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="{{ url('finance/account_header', $salary->id) }}">
                                                <div class="form-group">
                                                    <label for="recipient-name" class="form-control-label">Bank Account:</label>
                                                    <select class="form-control bank_id" required name="bank_id" id="bank_id">

                                                        <option class="">Choose Bank</option>
                                                        @foreach($banks as $bank)
                                                            <option class="{{ $bank->id }}"
                                                                    value="{{ $bank->id }}">{{ $bank->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="div_amount" id="div_amount" style="display: none;">
                                                    <div class="form-group">
                                                        <label style="color: #222  !important;"> Balance: </label>
                                                        {{--<input type="text" name="bank_amount" readonly>--}}
                                                        <select class="form-control bank_balance" disabled id="bank_balance" name="amount_left"
                                                                required="">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="recipient-name" class="form-control-label">Account Header:</label>
                                                    <select required="required" class="form-control account_header_id" name="account_header_id" id="account_header_id">

                                                        <option class="">Choose Account Header</option>
                                                        @foreach($account_headers as $account_header)
                                                            <option class="" value="{{ $account_header->id }}">
                                                                {{ $account_header->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="subheader_lists" id="subheader_lists" style="display: none;">
                                                    <div class="form-group" >
                                                        <label > Select Subheader: </label>
                                                        <select required class="form-control subheader_id" id="subheader_id" name="subheader_id" required="">

                                                        </select>
                                                    </div>
                                                </div>

                                                <input type="hidden" name="payroll_reference" readonly value="{{ $salary->payroll_reference }}">

                                                <div class="form-group">
                                                    <label for="message-text" class="form-control-label">Amount:</label>
                                                    <input class="form-control" required readonly value="{{ $salary->amount }}" name="amount" id="message-text" type="text"/>
                                                </div>

                                        </div>
                                        <div class="modal-footer">
                                            <input type="submit" class="btn btn-success" value="Disburse"/>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            {{-- modal for tax payment --}}
                            <div class="modal fade" id="modal-tax{{$salary->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Pay Tax for (<span style="color:red;">{{ $salary->month }} {{ $salary->year }}</span>)</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="{{ url('finance/disbursment/tax') }}">
                                                <div class="form-group">
                                                    <label for="recipient-name" class="form-control-label">Bank Account:</label>
                                                    <select class="form-control bank_id" required name="bank_id" id="bank_id">

                                                        <option class="">Choose Bank</option>
                                                        @foreach($banks as $bank)
                                                            <option class="{{ $bank->id }}"
                                                                    value="{{ $bank->id }}">{{ $bank->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="div_amount" id="div_amount" style="display: none;">
                                                    <div class="form-group">
                                                        <label style="color: #222  !important;"> Balance: </label>
                                                        {{--<input type="text" name="bank_amount" readonly>--}}
                                                        <select class="form-control bank_balance" disabled id="bank_balance" name="amount_left"
                                                                required="">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="recipient-name" class="form-control-label">Account Header:</label>
                                                    <select required="required" class="form-control account_header_id" name="account_header_id" id="account_header_id">

                                                        <option class="">Choose Account Header</option>
                                                        @foreach($account_headers as $account_header)
                                                            <option class="" value="{{ $account_header->id }}">
                                                                {{ $account_header->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="subheader_lists" id="subheader_lists" style="display: none;">
                                                    <div class="form-group" >
                                                        <label > Select Subheader: </label>
                                                        <select required class="form-control subheader_id" id="subheader_id" name="subheader_id" required="">

                                                        </select>
                                                    </div>
                                                </div>

                                                <input type="hidden" name="payroll_reference" readonly value="{{ $salary->payroll_reference }}">

                                                <div class="form-group">
                                                    <label for="message-text" class="form-control-label">Tax Amount:</label>
                                                    <input class="form-control" required readonly value="{{ $salary->tax }}" name="tax" id="message-text" type="text"/>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input type="submit" class="btn btn-primary" value="Disburse"/>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                          @empty
                          @endforelse
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
            <!-- /.tab-content -->
         </div>
         <!-- nav-tabs-custom -->
      </div>
      </section>
  @endsection

  @section('script')
       <script>
         $(function () {
           $("#example1").DataTable();

               });
         $('.adf').hide();
         $('.cdb').on('click', function(){
           $('.adf').slideToggle();
        
         });

           $('.cadfxx').on('click', function(){
              $('.adf').slideToggle();
           });

       </script>
  @endsection
