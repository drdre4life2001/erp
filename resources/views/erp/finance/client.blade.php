@extends('erp.layouts.master')
  
  @section('title')
    Finance - Clients
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
    <section class="content-header">
   <h1>
      Clients
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Clients</li>
   </ol>
</section>
<section class="content">
      <div class="col-md-12">
   <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Clients  </b></a></li>
      </ul>
      <div class="tab-content" style="padding: 2%">
         <div class="tab-pane active" id="tab_1">
            <p align="right">
               <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white;"></i>  Add Clients</button>
            </p>
            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
               <div class="col-md-1 hidden-sm hidden-xs"></div>
               <div class="col-md-10">
                  <form method="POST" action="{{ url('add_client') }}">
                     <h3 align="center">Clients</h3>
                     <div class="col-md-6" style="padding: 0px;">
                        <div class="form-group">
                           <label>Client Name</label>
                           <div class="icon-addon addon-md">
                              <input required="" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name">
                              @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                              <label for="name" class="fa fa-user" rel="tooltip" title="Name"></label>
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Client Telephone</label>
                           <div class="icon-addon addon-md">
                              <input required="" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Client Telephone">
                              @if ($errors->has('phone')) <p class="help-block" style="color: red">{{ $errors->first('phone') }}</p> @endif
                              <label for="house" class="fa fa-mobile" rel="tooltip" title="Telephone"></label>
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Client Address</label>
                           <div class="icon-addon addon-md">
                              <input required="" type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="Client Address">
                              @if ($errors->has('address')) <p class="help-block" style="color: red">{{ $errors->first('address') }}</p> @endif
                              <label for="house" class="fa fa-map" rel="tooltip" title="Address"></label>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6" style="padding-right: 0px;">
                        <div class="form-group">
                           <label>Country</label>
                           <div class="icon-addon addon-md">
                              <select class="form-control countryId"  name="country" data-show-subtext="true"
                                 data-live-search="true" name="country" required>
                                <option>Select Country</option>
                                  @forelse($countries as $key => $value)
                                   <option data-subtext="Zimbabwe"
                                      value="{{ $key }}" @if (old('country') == $key) selected="selected" @endif>{{ $value }} 
                                   </option>
                                  @empty
                                  @endforelse
                              </select>
                              @if ($errors->has('country')) <p class="help-block" style="color: red">{{ $errors->first('country') }}</p> @endif
                              <label for="house" class="fa fa-globe" rel="tooltip" title="Select Country"></label>
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Select State</label>
                           <div class="icon-addon addon-md">
                              <select required="" class="form-control states" name="state" value="{{ old('state') }}">
                              </select>
                              <label for="house" class="fa fa-globe" rel="tooltip" title="Select State"></label>
                              @if ($errors->has('state')) <p class="help-block" style="color: red">{{ $errors->first('state') }}</p> @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Select City</label>
                           <div class="icon-addon addon-md">
                              <select required="" class="form-control cities" name="city" id="cities" value="{{ old('city') }}">
                              </select>
                              <label for="house" class="fa fa-map" rel="tooltip" title="Select Cities"></label>
                              @if ($errors->has('city')) <p class="help-block" style="color: red">{{ $errors->first('city') }}</p> @endif
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label>Description</label>
                        <div class="icon-addon addon-md">
                           <input required="" type="text" class="form-control" name="description" value="{{ old('description') }}" placeholder="Description">
                           <label for="house" class="fa fa-book" rel="tooltip" title="Description"></label>
                           @if ($errors->has('description')) <p class="help-block" style="color: red">{{ $errors->first('description') }}</p> @endif
                        </div>
                     </div>
                     <p align="center">
                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                        Add New Client</button>
                        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                        Cancel</a>
                     </p>
                  </form>
               </div>
               <div class="col-md-1 hidden-sm hidden-xs"></div>
            </div>
            <hr/ style="clear: both">
            <div class="box-body">
               <table id="example1" class="table table-bordered table-hover">
                  <thead>
                     <tr style="color:#8b8b8b">
                        <th>S/N</th>
                        <th>Client Name</th>
                        <th>Phone</th>
                        <th>Description</th>
                        <th>Address</th>
                        <th>TIN</th>
                        <th>Date Added</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php $inc = 1; ?>
                     @foreach($clients as $client)
                        <tr>
                           <td>{{ $inc++ }}</td>
                           <td>{{ ucfirst($client->name) }}</td>
                           <td>{{ $client->phone }}</td>
                           <td>{{ $client->description }}</td>
                           <td>{{ $client->address }}</td>
                           <td>{{ $client->tin }}</td>
                           <td>{{ date("M d, Y",strtotime($client->created_at)) }}</td>
                           <td>  
                              <a data-toggle="modal" href='#modal-id{{ $client->id }}'>
                              <span class="label label-warning" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                              <i class="fa fa-trash"></i>  Edit </span></a>
                              <a href="{{ url('delete-client/'.$client->id.'/client') }}" id="a_del">
                              <span class="label label-danger" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                              <i class="fa fa-trash"></i>  Deactive </span></a>
                           </td>
                        </tr>

                        <div class="modal fade" id="modal-id{{ $client->id }}">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Edit client ({{ ucfirst($client->name)}})</h4>
                              </div>
                              <div class="modal-body">
                                <form method="POST" action="{{ url('update_client') }}">
                                   <h3 align="center">Clients</h3>
                                   <div class="col-md-6" style="padding: 0px;">
                                      <div class="form-group">
                                         <label>Client Name</label>
                                         <div class="icon-addon addon-md">
                                            <input type="text" class="form-control" name="name" value="{{ $client->name }}" placeholder="Name">
                                            @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                                            <label for="name" class="fa fa-user" rel="tooltip" title="Name"></label>
                                         </div>
                                      </div>
                                      <div class="form-group">
                                         <label>Client Telephone</label>
                                         <div class="icon-addon addon-md">
                                            <input type="text" class="form-control" name="phone" value="{{ $client->phone }}" placeholder="Client Telephone">
                                            @if ($errors->has('phone')) <p class="help-block" style="color: red">{{ $errors->first('phone') }}</p> @endif
                                            <label for="house" class="fa fa-mobile" rel="tooltip" title="Telephone"></label>
                                         </div>
                                      </div>
                                      <div class="form-group">
                                         <label>Client Address</label>
                                         <div class="icon-addon addon-md">
                                            <input type="text" class="form-control" name="address" value="{{ $client->address }}" placeholder="Client Address">
                                            @if ($errors->has('address')) <p class="help-block" style="color: red">{{ $errors->first('address') }}</p> @endif
                                            <label for="house" class="fa fa-map" rel="tooltip" title="Address"></label>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right: 0px;">
                                      <div class="form-group">
                                         <label>Country</label>
                                         <div class="icon-addon addon-md">
                                            <select class="form-control countries countryId"  name="country" value="{{ old('country') }}" data-show-subtext="true"
                                               data-live-search="true" name="country" required>
                                              <?php $db_selected_value = $client->country; ?>
                                              @foreach($countries as $key => $value)
                                               <option data-subtext=""
                                                  value="{{ $key }}" @if($db_selected_value == $key ) selected @endif>{{ $value }}
                                               </option>
                                              @endforeach
                                            </select>
                                            @if ($errors->has('country')) <p class="help-block" style="color: red">{{ $errors->first('country') }}</p> @endif
                                            <label for="house" class="fa fa-globe" rel="tooltip" title="Select Country"></label>
                                         </div>
                                      </div>
                                      <div class="form-group">
                                         <label>Select State</label>
                                         <div class="icon-addon addon-md">
                                            <select class="form-control states" name="state" value="{{ old('state') }}">
                                            </select>
                                            <label for="house" class="fa fa-globe" rel="tooltip" title="Select State"></label>
                                            @if ($errors->has('state')) <p class="help-block" style="color: red">{{ $errors->first('state') }}</p> @endif
                                         </div>
                                      </div>
                                      <div class="form-group">
                                         <label>Select City</label>
                                         <div class="icon-addon addon-md">
                                            <select class="form-control cities" name="city" value="{{ old('city') }}">
                                            </select>
                                            <label for="house" class="fa fa-map" rel="tooltip" title="Select Cities"></label>
                                            @if ($errors->has('city')) <p class="help-block" style="color: red">{{ $errors->first('city') }}</p> @endif
                                         </div>
                                      </div>
                                   </div>
                                   <input type="hidden" name="client_id" value="{{ $client->id }}">
                                   <div class="form-group">
                                      <label>Description</label>
                                      <div class="icon-addon addon-md">
                                         <input type="text" required="" class="form-control" name="description" value="{{ old('description') }}" placeholder="Description">
                                         <label for="house" class="fa fa-book" rel="tooltip" title="Description"></label>
                                         @if ($errors->has('description')) <p class="help-block" style="color: red">{{ $errors->first('description') }}</p> @endif
                                      </div>
                                   </div>
                                   <p align="center">
                                      <button class="btn btn-large btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                                      Update Client</button>
                                      {{-- <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                                      Cancel</a> --}}
                                   </p>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                     @endforeach
                     </tfoot>
               </table>
            </div>
         </div>
      </div>
      <!-- /.tab-content -->
   </div>
   <!-- nav-tabs-custom -->
   </div>
   <!-- /.col -->
</section>

{{-- edit modal --}}

{{-- <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Trigger modal</a> --}}
@endsection

@section('script')
    <script>
      $(function () {
        $("#example1").DataTable();
      });


      $('.adf').hide();
      $('.cdb').on('click', function(){
        $('.adf').slideToggle();
     
      });
      @if ($errors->has('name') || $errors->has('phone') || $errors->has('address') || $errors->has('country') || $errors->has('state') || $errors->has('cities'))
        $('.adf').slideToggle();
      @endif

        $('.cadfxx').on('click', function(){
           $('.adf').slideToggle();
        });

        //onchange country display states related
        $('.countryId').change(function(e) {
          // alert('slkd');
          var parent = e.target.value;
          $.get('/states/'+ parent, function(data) {
                // console.log(data);
              var SelectOption ="<option  value=''>--Select State--</option>";
              if (data.length < 1) {
                  alert('Oops! No state available in the selected country.');
                  // document.getElementById("countryId")
                  $(this).focus();
              }else{
                  $.each(data, function(index,value)
                  {
                      SelectOption +=  "<option value='"+ value.id   +"'>" + value.name + "</option>";
                  });
              }
              $( ".states" ).html(SelectOption); //});
          });
        });
        
        //onchange country display states related
        $('.states').change(function(e) {
            var parent = e.target.value;
            $.get('/cities/'+ parent, function(data) {
                  // console.log(data);
                var SelectOption ="<option  value=''>--Select City--</option>";
                if (data.length < 1) {
                    alert('Oops! No city available in the selected state.');
                    document.getElementById("states").focus();
                }else{

                    $.each(data, function(index,value)
                    {

                        SelectOption +=  "<option value='"+ value.id   +"'>" + value.name + "</option>";
                    });
                }
                $( ".cities" ).html(SelectOption); //});
            });
        });

    </script>
@endsection