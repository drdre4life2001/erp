@extends('erp.layouts.master')
  
  @section('title')
    Finance - Loan Repayments
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
      <section class="content-header">
         <h1>
            Company Loan
         </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Company Loan</li>
         </ol>
      </section>
      <section class="content">
         <div class="col-md-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Company Loan
                     </b></a>
                  </li>
               </ul>
               <div class="tab-content" style="padding: 2%">
                  <div class="tab-pane active" id="tab_1">
                     <p align="right">
                        @if(!empty($loan->balance))
                             <button class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add Loan Repayment
                             </button>
                         @else
                             <b>
                                 <a href="#" class="btn btn-large btn-default"><i style="color: #af85f8;" class="fa fa-stop" aria-hidden="true" style="color:white"></i>  Loan repayment completedfor {{$company_name}}
                                 </a> 
                             </b>
                         @endif
                        
                     </p>
                     @if(!empty($loan->balance) )
                       <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                          <div class="col-md-1 hidden-sm hidden-xs"></div>
                          <div class="col-md-10">
                             <form method="POST">
                                <h3 align="center">Create Company Loan
                                </h3>
                                <div class="col-md-6" style="padding: 0px;">
                                   <div class="form-group">
                                      <label>Amount </label>
                                      <div class="icon-addon addon-md">
                                         <input required="" type="number" required="" value="{{ old('amount_paid') }}" class="form-control" placeholder="Amount" name="amount_paid">
                                         <label for="house" class="fa fa-money" rel="tooltip" title="house"></label>
                                         @if ($errors->has('amount_paid')) <p class="help-block" style="color: red">{{ $errors->first('amount') }}</p> @endif
                                      </div>
                                   </div>
                                   <div class="form-group">
                                      <label>Description</label>
                                      <div class="icon-addon addon-md">
                                         <input required="" type="text" required="" value="{{ old('note') }}"  name="note" class="form-control" placeholder="Description for Loan Collection">
                                         <label for="house" class="fa fa-book" rel="tooltip" title="Description"></label>
                                         @if ($errors->has('note')) <p class="help-block" style="color: red">{{ $errors->first('description') }}</p> @endif
                                      </div>
                                   </div>
                                </div>
                                <div class="col-md-6" style="padding-right: 0px;">
                                   <div class="form-group">
                                      <label>Loan Amount: </label>
                                      <div class="icon-addon addon-md">
                                         <input required="" type="text" value="{{ number_format($loan->amount, 2) }}" class="form-control" disabled name="amount">
                                         <label for="house" class="fa fa-money" rel="tooltip" title="house"></label>
                                         @if ($errors->has('amount')) <p class="help-block" style="color: red">{{ $errors->first('amount') }}</p> @endif
                                      </div>
                                   </div>
                                   <div class="form-group">
                                      <label>Balance </label>
                                      <div class="icon-addon addon-md">
                                         <input required="" type="text" value="{{ number_format($loan->balance, 2) }}" class="form-control"  disabled name="amount">
                                         <label for="house" class="fa fa-money" rel="tooltip" title="house"></label>
                                         @if ($errors->has('amount')) <p class="help-block" style="color: red">{{ $errors->first('amount') }}</p> @endif
                                      </div>
                                   </div>
                                </div>
                                <p align="center">
                                   <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                                   Make Repayment</button>
                                   <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                                   Cancel</a>
                                </p>
                             </form>
                          </div>
                          <div class="col-md-1 hidden-sm hidden-xs"></div>
                       </div>
                     @endif
                     <hr/ style="clear: both">
                     <div class="box-body">
                        @if(count($repayment_histories))
                                    <div class="row">
                                        <center> <b>REPAYMENT HISTORY FOR {{ strtoupper($company_name) }} </b> <small style="color: red;">Expected Amount: &#8358;{{ number_format($loan->amount, 2) }} </small> </center>
                                         <table id="example1" class="table table-responsive table-bordered table-hover" id="organizations-table">
                                             <thead>
                                                <th>
                                                     <center>
                                                         S/N
                                                     </center>
                                                 </th>
                                                 <th>
                                                     <center>
                                                         Amount Paid
                                                     </center>
                                                 </th>
                                                 <!-- <th>
                                                    <center>
                                                         Total Resettlement so far   
                                                     </center>
                                                     
                                                 </th> -->
                                                 <th>
                                                     <center>
                                                         Balance
                                                     </center>
                                                 </th>
                                                 <th>
                                                     <center>
                                                         Note
                                                     </center>
                                                 </th>
                                                 <th> 
                                                     <center>
                                                         Date Paid
                                                     </center>
                                                 </th>
                                             </thead>
                                             <tbody>
                                                <?php ;$num = 1; ?>
                                                @foreach($repayment_histories as $repayment_history)
                                                 <tr>
                                                    <td>
                                                        <center>
                                                            ({{ $num++ }})
                                                        </center>
                                                    </td>
                                                     <td> 
                                                        <center>&#8358;{!! number_format($repayment_history->amount_paid, 2) !!}</center>
                                                     </td>
                                                     <!-- <td>
                                                        <center>
                                                            &#8358;{!! number_format($repayment_history->total_resettlement, 2) !!}
                                                        </center>
                                                        
                                                    </td> -->
                                                    <td>
                                                        <center>
                                                         &#8358;{!! number_format($repayment_history->balance, 2) !!}
                                                        </center>
                                                    </td>
                                                     <td>
                                                            <center>
                                                              {!! $repayment_history->note !!}<span style="font-weight: bold;">
                                                            </center>
                                                        </span> 
                                                    </td>
                                                     <td>
                                                        <center>
                                                            <center>
                                                              {{ date("M d, Y",strtotime($repayment_history->date_paid)) }}
                                                            </center>
                                                        </center>
                                                    </td>
                                                 </tr>

                                                @endforeach
                                             </tbody>
                                         </table>
                                        <hr> <hr> <br>
                                        <div class="col-lg-4" style="margin-top:-30px; margin-left: -50px;">
                                            <center>
                                                <b style="font-weight: bold; color: red;">Total Resettlement = &#8358;{{ number_format($total_resettlement, 2) }} {{ empty($loan->balance) ? '(Completed)' : '' }}</b>
                                            </center>
                                        </div>     
                                    </div>
                                @else

                                    <div class="row">
                                        <div>
                                            <center> <b>NO REPAYMENT HISTORY FOR {{ strtoupper($company_name) }} </b> <small style="color: red;">Expected Amount: &#8358;{{ number_format($loan->amount, 2) }} </small> </center>
                                        </div>
                                    </div>
                                @endif
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
  @endsection

  @section('script')
       <script type="text/javascript">
         
         $(function () {
           $("#example1").DataTable();});


         $('.adf').hide();
         $('.cdb').on('click', function(){
           $('.adf').slideToggle();
         });

         @if ($errors->has('amount_paid') || $errors->has('note'))
           $('.adf').slideToggle();
         @endif

           $('.cadfxx').on('click', function(){
              $('.adf').slideToggle();
           });

       </script>
  @endsection
