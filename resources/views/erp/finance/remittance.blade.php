@extends('erp.layouts.master')
  
  @section('title')
    Finance - Account Headers
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  	<section class="content-header">
  	   <h1>     PHEDC Remittance
  	   </h1>
  	   <ol class="breadcrumb">
  	      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  	      <li class="active">PHEDC Remittance</li>
  	   </ol>
  	</section>
  	<section class="content">
  	   <!-- /.
  	      <div class="col-md-12">
  	           <!-- Custom Tabs -->
  	   <div class="nav-tabs-custom">
  	      <ul class="nav nav-tabs">
  	         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> PHEDC Remittance
  	            </b></a>
  	         </li>
  	      </ul>
  	      <div class="tab-content" style="padding: 2%">
  	         <div class="tab-pane active" id="tab_1">
  	            <p align="right">
  	               <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add PHEDC Remittance
  	               </button>
  	            </p>
  	            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
  	               <div class="col-md-1 hidden-sm hidden-xs"></div>
  	               <div class="col-md-10">
  	                  <form>
  	                     <h3 align="center">Revenue Streams
  	                     </h3>
  	                     <div class="col-lg-12">
  	                        <div class="form-group">
  	                           <label>(Bulk Upload) Attach File</label>
  	                           <input type="file" name="">
  	                        </div>
  	                     </div>
  	                     <div class="col-lg-6">
  	                        <div class="form-group">
  	                           <label>Date:</label>
  	                           <div class="input-group date">
  	                              <div class="input-group-addon">
  	                                 <i class="fa fa-calendar"></i>
  	                              </div>
  	                              <input type="text" class="form-control pull-right" id="datepicker">
  	                           </div>
  	                           <!-- /.input group -->
  	                        </div>
  	                     </div>
  	                     <div class="col-lg-6">
  	                        <div class="form-group">
  	                           <label>Enter Amount</label>
  	                           <div class="icon-addon addon-md">
  	                              <input type="number" class="form-control" name="">
  	                              <label for="house" class="fa fa-money" rel="tooltip" title="Enter Amount"></label>
  	                           </div>
  	                        </div>
  	                     </div>
  	                     <p align="center">
  	                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
  	                        Add </button>
  	                        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
  	                        Cancel</a>
  	                     </p>
  	                  </form>
  	               </div>
  	               <div class="col-md-1 hidden-sm hidden-xs"></div>
  	            </div>
  	            <hr/ style="clear: both">
  	            <div class="box-body">
  	               <table id="example1" class="table table-bordered table-hover">
  	                  <thead>
  	                     <tr style="color:#8b8b8b">
  	                        <th>S/N </th>
  	                        <th>Date</th>
  	                        <th>Amount</th>
  	                        <th>Action</th>
  	                     </tr>
  	                  </thead>
  	                  <tbody>
  	                     <tr>
  	                        <td>1</td>
  	                        <td>17-10-2017 </td>
  	                        <td>&#x20A6;1000</td>
  	                        <td>  
  	                           <a href="#">
  	                           <span class="label label-warning" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
  	                           <i class="fa fa-trash"></i>  Edit </span></a>
  	                           <a href="#">
  	                           <span class="label label-danger" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
  	                           <i class="fa fa-trash"></i>  Delete </span></a>
  	                        </td>
  	                     </tr>
  	                     <tr>
  	                        <td>2</td>
  	                        <td>18-10-2017 </td>
  	                        <td>&#x20A6;73836</td>
  	                        <td>  
  	                           <a href="#">
  	                           <span class="label label-warning" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
  	                           <i class="fa fa-trash"></i>  Edit </span></a>
  	                           <a href="#">
  	                           <span class="label label-danger" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
  	                           <i class="fa fa-trash"></i>  Delete </span></a>
  	                        </td>
  	                     </tr>
  	                     <tr>
  	                        <td>3</td>
  	                        <td>31-10-2017   </td>
  	                        <td>&#x20A6;7900</td>
  	                        <td>  
  	                           <a href="#">
  	                           <span class="label label-warning" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
  	                           <i class="fa fa-trash"></i>  Edit </span></a>
  	                           <a href="#">
  	                           <span class="label label-danger" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
  	                           <i class="fa fa-trash"></i>  Delete </span></a>
  	                        </td>
  	                     </tr>
  	                     <tr>
  	                        <td>4</td>
  	                        <td>21-10-2017   </td>
  	                        <td>&#x20A6;1200800</td>
  	                        <td>  
  	                           <a href="#">
  	                           <span class="label label-warning" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
  	                           <i class="fa fa-trash"></i>  Edit </span></a>
  	                           <a href="#">
  	                           <span class="label label-danger" style="padding:4.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
  	                           <i class="fa fa-trash"></i>  Delete </span></a>
  	                        </td>
  	                     </tr>
  	                     </tfoot>
  	               </table>
  	            </div>
  	         </div>
  	      </div>
  	      <!-- /.tab-content -->
  	   </div>
  	   <!-- nav-tabs-custom -->
  	   </div>
  	   <!-- /.col -->
  	</section>
  	<!-- Main content -->
  @endsection

  @section('script')
  	<script type="text/javascript">
  	  $(function () {
  	    $("#example1").DataTable();

  	        });


  	  $('.adf').hide();
  	  $('.cdb').on('click', function(){
  	    $('.adf').slideToggle();
  	 
  	  });

  	    $('.cadfxx').on('click', function(){
  	       $('.adf').slideToggle();
  	    });

  	        //Date picker
  	    $('#datepicker').datepicker({
  	      autoclose: true
  	    });


  	</script>

  @endsection