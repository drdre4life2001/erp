@extends('erp.layouts.master')
  
  @section('title')
    Finance - Bank Accounts
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
  	<section class="content-header">
   <h1>      Bank Accounts
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Bank Accounts </li>
   </ol>
</section>
<section class="content">
      <div class="col-md-12">
   <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Bank Accounts  </b></a></li>
      </ul>
      <div class="tab-content" style="padding: 2%">
         <div class="tab-pane active" id="tab_1">
            <p align="right">
               <button class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add Bank Accounts </button>
            </p>
            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
               <div class="col-md-1 hidden-sm hidden-xs"></div>
               <div class="col-md-10">
                  <form method="post" action="">
                     <h3 align="center">Bank Accounts </h3>
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label>Bank Name</label>
                           <div class="icon-addon addon-md">
                              <select class="form-control" name="bank_id" required="">
                                 <option value="">Choose Bank</option>
                                 @foreach($banks as $bank)
                                    <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                                 @endforeach
                              </select>
                              @if ($errors->has('bank_id')) <p class="help-block" style="color: red">{{ $errors->first('bank_id') }}</p> @endif
                              <label for="house" class="fa fa-university" rel="tooltip" title="Bank Name"></label>
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Account Number</label>
                           <div class="icon-addon addon-md">
                              <input type="number" class="form-control" name="account_number" value="{{ old('account_number') }}" placeholder="Account Number" required="">
                              @if ($errors->has('account_number')) <p class="help-block" style="color: red">{{ $errors->first('account_number') }}</p> @endif
                              <label for="house" class="fa fa-university" rel="tooltip" title="Bank Name"></label>
                            </div>
                        </div>
                        
                        <div class="form-group">
                           <label>Account Balance</label>
                           <div class="icon-addon addon-md">
                              <input type="number" class="form-control" name="account_balance" value="{{ old('account_balance') }}" placeholder="Account Number" required="">
                              @if ($errors->has('account_balance')) <p class="help-block" style="color: red">{{ $errors->first('account_balance') }}</p> @endif
                              <label for="house" class="fa fa-university" rel="tooltip" title="Bank Name"></label>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label>Select Subsidiary</label>
                           <div class="icon-addon addon-md">
                              <select class="form-control organization_search" name="subsidiary_id" required="">
                              </select>
                              @if ($errors->has('subsidiary_id')) <p class="help-block" style="color: red">{{ $errors->first('subsidiary_id') }}</p> @endif
                              <label for="house" rel="tooltip" title="Subsidiary Name"></label>
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Name of Relationship Manager</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" name="name_relationship_manager" value="{{ old('name_relationship_manager') }}" placeholder="Name of Relationship Manager">
                              @if ($errors->has('name_relationship_manager')) <p class="help-block" style="color: red">{{ $errors->first('name_relationship_manager') }}</p> @endif
                              <label for="house" class="fa fa-user" rel="tooltip" title="Bank Name"></label>
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Phone number of Relationship Manager</label>
                           <div class="icon-addon addon-md">
                              <input type="text" required="" class="form-control" name="phone_relationship_manager" value="{{ old('phone_relationship_manager') }}" placeholder="Phone number of Relationship Manager">
                              @if ($errors->has('phone_relationship_manager')) <p class="help-block" style="color: red">{{ $errors->first('phone_relationship_manager') }}</p> @endif
                              <label for="house" class="fa fa-user" rel="tooltip" title="Bank Name"></label>
                           </div>
                        </div>
                     </div>

                      <div class="form-group">
                          <label>Account Name</label>
                          <div class="icon-addon addon-md">
                            <input type="text" class="form-control" name="account_name" value="{{ old('account_name') }}" placeholder="Account Name" required="">
                            @if ($errors->has('account_name')) <p class="help-block" style="color: red">{{ $errors->first('account_name') }}</p> @endif
                            <label for="house" class="fa fa-envelope" rel="tooltip" title="Account Name"></label>
                          </div>
                      </div>
                        <div class="form-group">
                            <label>Email of Relationship Manager</label>
                            <div class="icon-addon addon-md">
                              <input type="text" class="form-control" name="email_relationship_manager" value="{{ old('email_relationship_manager') }}" placeholder="Email of Relationship Manager" required="">
                              @if ($errors->has('email_relationship_manager')) <p class="help-block" style="color: red">{{ $errors->first('email_relationship_manager') }}</p> @endif
                              <label for="house" class="fa fa-envelope" rel="tooltip" title="Bank Name"></label>
                            </div>
                        </div>

                        <p align="center">
                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                        Add Bank Account</button>
                        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                        Cancel</a>
                     </p>
                  </form>
               </div>
               <div class="col-md-1 hidden-sm hidden-xs"></div>
            </div>
            <hr/ style="clear: both">
            <div class="box-body">
               <table id="example1" class="table table-bordered table-hover">
                  <thead>
                     <tr style="color:#8b8b8b">
                        <th>S/N </th>
                        <th>Bank Name</th>
                        <th>Account No.</th>
                        <th>Previous Balance</th>
                        <th>Current Balance</th>
                        <th>Subsidiary</th>
                        <th>Relationship Manager</th>
                        <th>RM Email</th>
                        <th>RM Phone</th>
                        {{-- <th>Action</th> --}}
                     </tr>
                  </thead>
                  <tbody>
                     <?php $num = 1; ?>
                     @foreach($items as $i)
                        <tr>
                           <td>{{ $num++ }}</td>
                           <td>
                              <span style="float:left; margin-top:2%; font-weight: bold">{{ $i->bank}}</span>
                           </td>
                           <td>{{ $i->account }}</td>
                           <td>&#x20a6;{{ number_format($i->prevBalance) }}</td>
                           <td>&#x20a6;{{ number_format($i->currentBalance) }}</td>
                           <td>{{ $i->subsidiary }}</td>
                           <td>{{ $i->name_relationship_manager }}</td>
                           <td>{{ $i->email_relationship_manager }}</td>
                           <td>{{ $i->phone_relationship_manager }}</td>
                           {{-- <td>  
                              <a href="#">
                              <span class="label label-warning" style="padding:6.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                              <i class="fa fa-trash"></i>  Edit </span></a>
                              <a href="#">
                              <span class="label label-danger" style="padding:6.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                              <i class="fa fa-trash"></i>  Deactive </span></a>
                           </td> --}}
                        </tr>
                     @endforeach
                  </tfoot>
               </table>
            </div>
         </div>
      </div>
   </div>
   </div>
</section>
  @endsection

@section('script')
    <script>
        $("#example1").DataTable();

            });


      $('.adf').hide();
      $('.cdb').on('click', function(){
        $('.adf').slideToggle();
     
      });

        $('.cadfxx').on('click', function(){
           $('.adf').slideToggle();
        });

    </script>
@endsection