@extends('erp.layouts.master')
  
  @section('title')
    Finance - Vendor Management
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
    <section class="content-header">
   <h1>
      Vendor
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Add Vendor</li>
   </ol>
</section>
<section class="content">
      <div class="col-md-12">
   <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Vendor </b></a></li>
      </ul>
      <div class="tab-content" style="padding: 2%">
         <div class="tab-pane active" id="tab_1">
            <p align="right">
               <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white;"></i>  Vendor</button>
            </p>
            <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
               <div class="col-md-1 hidden-sm hidden-xs"></div>
               <div class="col-md-10">
                  <form method="POST" action="">
                     <h3 align="center">Vendor</h3>
                     <div class="row">
                       <div class="col-md-6" style="padding: 0px;">
                          <div class="form-group">
                             <label>Vendors Name </label>
                             <div class="icon-addon addon-md">
                                <input required="" type="text" required="" value="{{ old('name') }}" class="form-control" placeholder="Name" name="name">
                                <label for="house" class="fa fa-money" rel="tooltip" title="house"></label>
                                @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                             </div>
                          </div>
                        </div>

                        <div class="col-lg-6">
                          <div class="form-group">
                             <label>Vendors Email</label>
                             <div class="icon-addon addon-md">
                                <input required="" type="email" required="" value="{{ old('email') }}"  name="email" class="form-control" placeholder="Vendors Email">
                                <label for="house" class="fa fa-book" rel="tooltip" title="Description"></label>
                                @if ($errors->has('email')) <p class="help-block" style="color: red">{{ $errors->first('email') }}</p> @endif
                             </div>
                          </div>
                        </div>
                     </div>

                      <div class="row">
                        <div class="col-md-6" style="padding-right: 0px;">
                           <div class="form-group">
                              <label>Description: </label>
                              <div class="icon-addon addon-md">
                                 <textarea name="description" id="inputDescription" class="form-control" rows="3" required="required">{{ old('description') }}</textarea>
                                 <label for="house" class="fa fa-money" rel="tooltip" title="description"></label>
                                 @if ($errors->has('description')) <p class="help-block" style="color: red">{{ $errors->first('description') }}</p> @endif
                              </div>
                           </div>
                         </div>

                         <div class="col-lg-6">
                           <div class="form-group">
                              <label>Phone </label>
                              <div class="icon-addon addon-md">
                                 <input required="" type="number" value="{{old('phone')}}" class="form-control" name="phone">
                                 <label for="house" class="fa fa-money" rel="tooltip" title="phone"></label>
                                 @if ($errors->has('phone')) <p class="help-block" style="color: red">{{ $errors->first('phone') }}</p> @endif
                              </div>
                           </div>
                         </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                             <label>Vendors Tax Identification Number: </label>
                             <div class="icon-addon addon-md">
                                <input required="" type="number" name="tin" class="form-control" value="{{ old('tin') }}">
                                <label for="house" class="fa fa-money" rel="tooltip" title="tin"></label>
                                @if ($errors->has('tin')) <p class="help-block" style="color: red">{{ $errors->first('tin') }}</p> @endif
                             </div>
                          </div>
                        </div>

                        <div class="col-lg-6">
                          <div class="form-group">
                             <label>Address </label>
                             <div class="icon-addon addon-md">
                                <textarea name="address" id="inputAddress" class="form-control" rows="3" required="required"></textarea>
                                <label for="house" class="fa fa-money" rel="tooltip" title="address"></label>
                                @if ($errors->has('address')) <p class="help-block" style="color: red">{{ $errors->first('address') }}</p> @endif
                             </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                             <label>Country: </label>
                             <div class="icon-addon addon-md">
                                <select name="country" id="countryId" class="form-control countryId" required="required">
                                  <option value="">Select Country</option>
                                  @if(!empty($countries))
                                    @foreach($countries as $country)
                                      <option value="{{ $country->id }}">{{ ucfirst($country->name) }}</option>
                                    @endforeach
                                  @endif
                                </select>
                                <label for="house" class="fa fa-money" rel="tooltip" title="country"></label>
                                @if ($errors->has('country')) <p class="help-block" style="color: red">{{ $errors->first('country') }}</p> @endif
                             </div>
                          </div>
                        </div>

                        <div class="col-lg-6">
                          <div class="form-group">
                             <label>State </label>
                             <div class="icon-addon addon-md">
                                <select name="state" class="states form-control" required="required">
                                  <option value=""></option>
                                </select>
                                <label for="house" class="fa fa-money" rel="tooltip" title="state"></label>
                                @if ($errors->has('state')) <p class="help-block" style="color: red">{{ $errors->first('state') }}</p> @endif
                             </div>
                          </div>
                        </div>
                      </div>
                     
                     <p align="center">
                        <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                        Add Vendor</button>
                        <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                        Cancel</a>
                     </p>
                  </form>
               </div>
               <div class="col-md-1 hidden-sm hidden-xs"></div>
            </div>
            <hr/ style="clear: both">
            <div class="box-body">
               <table id="example1" class="table table-bordered table-hover">
                  <thead>
                     <tr style="color:#8b8b8b">
                        <th>S/N</th>
                        <th>Vendor Name</th>
                        <th>Phone</th>
                        <th>Description</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>TIN</th>
                        <th>Date Added</th>
                        <th>Date Updated</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php $inc = 1; ?>
                     @if(!empty($vendors))
                      @foreach($vendors as $vendor)
                        <tr>
                           <td>{{ $inc++ }} </td>

                           <td>{{ ucfirst($vendor->name) }} </td>
                           <td>{{ $vendor->phone }}</td>
                           <td>{{ $vendor->description }}</td>
                           <td>{{ $vendor->address }}</td>
                           <td>{{ $vendor->tin }}</td>
                           <td>{{ date("M d, Y",strtotime($vendor->created_at)) }}</td>
                           <td>{{ date("M d, Y",strtotime($vendor->updated_at)) }}</td>
                           <td>  
                              <a href="#modal-id{{$vendor->id}}" data-toggle="modal">
                              <span class="label label-warning" style="padding:6.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                              <i class="fa fa-edit"></i>  Edit </span></a>
                              <a href="{{ url('delete_record/'.$vendor->id.'/vendor') }}" id="a_del">
                              <span class="label label-danger" style="padding:6.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                              <i class="fa fa-trash"></i>  Delete </span></a>
                        </tr>

                        <div class="modal fade" id="modal-id{{ $vendor->id }}">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Edit Item ({{ ucfirst($vendor->name)}})</h4>
                              </div>
                              <div class="modal-body">
                                <form method="POST" action="{{ url('/vendor/edit') }}">
                                   <h3 align="center">Vendor</h3>
                                   <div class="col-md-6" style="padding: 0px;">
                                      <div class="form-group">
                                         <label>Vendor Name</label>
                                         <div class="icon-addon addon-md">
                                            <input type="text" class="form-control" name="name" value="{{ $vendor->name }}" placeholder="Name">
                                            @if ($errors->has('name')) <p class="help-block" style="color: red">{{ $errors->first('name') }}</p> @endif
                                            <label for="name" class="fa fa-user" rel="tooltip" title="Name"></label>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding-right: 0px;">
                                      <div class="form-group">
                                         <label>Phone</label>
                                         <div class="icon-addon addon-md">
                                            <input type="text" class="form-control" name="phone" value="{{ $vendor->phone }}" placeholder="Vendor Telephone">
                                            @if ($errors->has('phone')) <p class="help-block" style="color: red">{{ $errors->first('phone') }}</p> @endif
                                            <label for="house" class="fa fa-mobile" rel="tooltip" title="Phone"></label>
                                         </div>
                                      </div>
                                   </div>

                                   <div class="col-md-6" style="padding-right: 0px;">
                                      <div class="form-group">
                                         <label>Description</label>
                                         <div class="icon-addon addon-md">
                                            <textarea name="description" id="inputAddress" class="form-control" rows="3" required="required">{{ $vendor->description }}</textarea>
                                            @if ($errors->has('description')) <p class="help-block" style="color: red">{{ $errors->first('description') }}</p> @endif
                                            <label for="house" class="fa fa-mobile" rel="tooltip" title="Description"></label>
                                         </div>
                                      </div>
                                   </div>

                                   <div class="col-md-6" style="padding-right: 0px;">
                                      <div class="form-group">
                                         <label>Address</label>
                                         <div class="icon-addon addon-md">
                                            <textarea name="address" id="inputAddress" class="form-control" rows="3" required="required">{{ $vendor->address }}</textarea>
                                            @if ($errors->has('address')) <p class="help-block" style="color: red">{{ $errors->first('address') }}</p> @endif
                                            <label for="house" class="fa fa-mobile" rel="tooltip" title="Phone"></label>
                                         </div>
                                      </div>
                                   </div>

                                   <div class="col-md-6" style="padding: 0px;">
                                      <div class="form-group">
                                         <label>TIN</label>
                                         <div class="icon-addon addon-md">
                                            <input type="number" class="form-control" name="tin" value="{{ $vendor->tin }}" placeholder="Name">
                                            @if ($errors->has('tin')) <p class="help-block" style="color: red">{{ $errors->first('tin') }}</p> @endif
                                            <label for="name" class="fa fa-number" rel="tooltip" title="T.I.N"></label>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="col-md-6" style="padding: 0px;">
                                      <div class="form-group">
                                         <label>Email</label>
                                         <div class="icon-addon addon-md">
                                            <input type="email" class="form-control" name="email" value="{{ $vendor->email }}" placeholder="Name">
                                            @if ($errors->has('email')) <p class="help-block" style="color: red">{{ $errors->first('email') }}</p> @endif
                                            <label for="email" class="fa fa-envelope" rel="tooltip" title="Email"></label>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="col-lg-6">
                                     <div class="form-group">
                                        <label>Country: </label>
                                        <div class="icon-addon addon-md">
                                           <select name="country" id="countryId" class="form-control countryId" required="required">
                                             <option value="">Select Country</option>
                                             @if(!empty($countries))
                                                <?php $db_country_val = $vendor->country; ?>
                                               @foreach($countries as $country)
                                                 <option value="{{ $country->id }}" @if($country->id == $db_country_val) selected @endif>{{ ucfirst($country->name) }}</option>
                                               @endforeach
                                             @endif
                                           </select>
                                           <label for="house" class="fa fa-money" rel="tooltip" title="country"></label>
                                           @if ($errors->has('country')) <p class="help-block" style="color: red">{{ $errors->first('country') }}</p> @endif
                                        </div>
                                     </div>
                                   </div>

                                   <div class="col-lg-6">
                                     <div class="form-group">
                                        <label>State--Selected:
                                          @if(!empty($states))
                                             <?php $db_state_val = $vendor->state; ?>
                                            @foreach($states as $state)
                                              @if($state->id == $db_state_val) <span style="color:orange;">
                                                {{ ucfirst($state->name) }}
                                                </span> 
                                              @endif
                                            @endforeach
                                          @endif
                                        </label>
                                        <div class="icon-addon addon-md">
                                           <select name="state" class="states form-control" required="required">
                                             <option value=""></option>
                                           </select>
                                           <label for="house" class="fa fa-money" rel="tooltip" title="state"></label>
                                           @if ($errors->has('state')) <p class="help-block" style="color: red">{{ $errors->first('state') }}</p> @endif
                                        </div>
                                     </div>
                                   </div>
                                   <input type="hidden" name="vendor_id" value="{{ $vendor->id }}">
                                   <p align="center">
                                      <button class="btn btn-large btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                                      Update Vendor</button>
                                   </p>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      @endforeach
                     @endif
                     </tfoot>
               </table>
            </div>
         </div>
      </div>
   </div>
   </div>
</section>

{{-- edit modal --}}

@endsection

@section('script')
    <script>
      $(function () {
        $("#example1").DataTable();
      });


      $('.adf').hide();
      $('.cdb').on('click', function(){
        $('.adf').slideToggle();
     
      });
      @if ($errors->has('name') || $errors->has('rate'))
        $('.adf').slideToggle();
      @endif

        $('.cadfxx').on('click', function(){
           $('.adf').slideToggle();
        });

    //onchange country display states related
    $('.countryId').change(function(e) {
      var parent = e.target.value;
      $.get('/states/'+ parent, function(data) {
            // console.log(data);
          var SelectOption ="<option  value=''>--Select State--</option>";
          if (data.length < 1) {
              alert('Oops! No state available in the selected country.');
              // document.getElementById("countryId")
              $(this).focus();
          }else{
              $.each(data, function(index,value)
              {
                  SelectOption +=  "<option value='"+ value.id   +"'>" + value.name + "</option>";
              });
          }
          $( ".states" ).html(SelectOption); //});
      });
    });
    </script>
@endsection