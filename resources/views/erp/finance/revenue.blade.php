@extends('erp.layouts.master')
  
  @section('title')
    Finance - Revenue
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
   <section class="content-header">
      <h1>     All Revenue
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">All Revenue</li>
      </ol>
   </section>
   <section class="content">
         <div class="col-md-12">
      <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>All Revenue
               </b></a>
            </li>
         </ul>
         <div class="tab-content" style="padding: 2%">
            <div class="tab-pane active" id="tab_1">
               <p align="right">
                  <button class="btn btn-large btn-purple cdb" ><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add Revenue
                  </button>
               </p>
               <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
                  <div class="col-md-10">
                     <form method="POST" action="{{ url('finance/add-revenue') }}">
                        <h3 align="center">Revenue
                        </h3>
                        <div class="col-lg-12">
                           <div class="form-group">
                              <label>Revenue Stream</label>
                              <select class="form-control" name="stream_id">
                                 <option value="">--Please select --</option>
                                 @forelse($streams as $stream)
                                    <option value="{{ $stream->id }}">{{ $stream->name }}</option>
                                 @empty

                                 @endforelse
                              </select>
                              @if ($errors->has('stream_id')) <p class="help-block" style="color: red">{{ $errors->first('stream_id') }}</p> @endif
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group">
                              <label>To:</label>
                              <div class="input-group date">
                                 <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                 </div>
                                 <input type="text" class="form-control pull-right toDate" name="to" >
                                 @if ($errors->has('to')) <p class="help-block" style="color: red">{{ $errors->first('to') }}</p> @endif
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group">
                              <label>From:</label>
                              <div class="input-group date">
                                 <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                 </div>
                                 <input type="text" class="form-control pull-right fromDate" name="from" >
                                 @if ($errors->has('from')) <p class="help-block" style="color: red">{{ $errors->first('from') }}</p> @endif
                              </div>
                           </div>
                        </div>
                        <p align="center">
                           <button class="btn btn-large btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                           Add </button>
                           <a class="btn btn-large btn-default cadfxx"><i class="fa fa-window-close" aria-hidden="true"></i>
                           Cancel</a>
                        </p>
                     </form>
                  </div>
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
               </div>
               <hr/ style="clear: both">
               <div id="search" style="background: #eee; height: auto; padding: 2% 5%;">
               </div>
               <div class="box-body">
                  <table id="example1" class="table table-bordered table-hover">
                     <thead>
                        <tr style="color:#8b8b8b">
                           <th>S/N </th>
                           <th>Date</th>
                           <th>Client</th>
                           <th>Company</th>
                           <th>Revenue Stream</th>
                           <th>Total Collections</th>
                           <th>Total Commission</th>
                           {{-- <th>Action</th> --}}
                        </tr>
                     </thead>
                     <tbody>
                        <?php $num = 1; ?>
                        @forelse($items as $item)
                           <tr>
                              <?php $date = date('d-m-Y', strtotime($item->tdate)); ?>
                              <td>{{$num++ }}</td>
                              <td> {{ $date }} </td>
                              <td> {{ $item->client }} </td>
                              <td> {{ $item->company}} </td>
                              <td> {{ $item->revenue_stream}} </td>
                              <td> {{ number_format($item->total_collection, 2) }}</td>
                              <td> {{ number_format($item->total_commission, 2) }}</td>
                           </tr>
                        @empty
                        @endforelse
                        </tfoot>
                  </table>
               </div>
            </div>
         </div>
      </div>
      </div>
   </section>
  @endsection

@section('script')
    <script>
     $(function () {
        $("#example1").DataTable();
     });
     $('.adf').hide();
     $('.cdb').on('click', function(){
     $('.adf').slideToggle();
     });
     $('.cadfxx').on('click', function(){
        $('.adf').slideToggle();
     });
    </script>
@endsection