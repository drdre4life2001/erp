<table class='table table-bordered table-hover' id="example1">
    <thead>
    <tr>
        <th>S/N</th>
        <th>Bank Details</th>
        <th>Transaction type</th>
        <th>Amount</th>
        <th>Previous Balance</th>
        <th>Current Balance</th>
        {{-- <th >Action</th> --}}
    </tr>
    </thead>
    <tbody>
    @if(isset($ledgers))
        <?php $i =  1; ?>
        @foreach($ledgers as $item)

            <tr>
                <!--Loop through all items for this procurement -->

                <td>{{ $i++ }}</td>
                <td>{{ $item->bank_name}}[{{$item->account_number}}]</td>
                <td>{{ $item->effect }}</td>
                <td>{{ number_format($item->amount) }}</td>
                <td>{{ number_format($item->prevBalance) }}</td>
                <td>{{ number_format($item->currentBalance) }}</td>
                <td>
                    <div class='btn-group' style="display:block">

                        {{-- <a href="" style="margin-right: 0px;"
                           class='btn btn-danger btn-xs' data-toggle="modal" data-target="#{{$item->id.'_'}}">
                            <i class="glyphicon glyphicons-delete"> </i> Delete
                        </a> --}}

                    </div>

                </td>
                <!-- Delete Modal -->
                <div class="modal fade" id="{{$item->id.'_'}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action = "{{url('finance/ledger/delete', $item->id)}}" method="get">
                                <div class="modal-body">
                                    Are you sure you want to delete this item?
                                    <input type="hidden" name="item_id" value="{{$item->id}}">
                                </div>
                                <div class="modal-footer">

                                    <a class="btn btn-default btn-ok" data-dismiss="modal">No</a>
                                    <input type="submit" value="Yes"  class="btn btn-danger">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--Modal ends -->


            </tr>
        @endforeach
    @endif
    </tbody>
</table>