@extends('erp.layouts.master')
  
  @section('title')
    Finance - Expense Pivot
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

@section('content')
	<section class="content-header">
	   <h1>     Expense Pivot
	   </h1>
	   <ol class="breadcrumb">
	      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	      <li class="active">Expense Pivot</li>
	   </ol>
	</section>
	<section class="content">
	  <div class="col-md-12"> 
	   <div class="nav-tabs-custom">
	      <ul class="nav nav-tabs">
	         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>Expense Pivot
	            </b></a>
	         </li>
	      </ul>
	      <div class="tab-content" style="padding: 2%">
	         <div class="tab-pane active" id="tab_1">
	            <hr/ style="clear: both">
	            <div id="search" style="background: #eee; height: auto; padding: 2% 5%">
	               <form method="POST" action="">
	               	{{ csrf_field() }}
	                  <div class="col-lg-6">
	                     <div class="form-group">
	                        <label>From:</label>
	                        <div class="input-group">
	                           <div class="input-group-addon">
	                              <i class="fa fa-calendar"></i>
	                           </div>
	                           <input type="text" name="from" required="" class="form-control fromDate pull-right" id="datepicker">
	                        </div>
	                     </div>
	                  </div>
	                  <div class="col-lg-6">
	                     <div class="form-group">
	                        <label>To:</label>
	                        <div class="input-group">
	                           <div class="input-group-addon">
	                              <i class="fa fa-calendar"></i>
	                           </div>
	                           <input type="text" required="" name="to" class="form-control pull-right toDate" id="datepicker2">
	                        </div>
	                     </div>
	                  </div>
	                  <p align="center">
	                     <button class="btn btn-large btn-danger"><i class="fa fa-search" aria-hidden="true"></i>
	                     Generate Expense Report </button>
	                  </p>
	               </form>
	            </div>
	            <div class="box-body">
	               <table id="example1" class="table table-bordered table-hover">
	                  <thead>
	                     <tr style="color:#8b8b8b">
	                        <th>Category </th>
	                        <th>Week 1</th>
	                        <th>Week 2</th>
	                        <th>Week 3</th>
	                        <th>Week 4</th>
	                        <th>Week 5</th>
	                        <th>Week 6</th>
	                        <th>Total</th>
	                     </tr>
	                  </thead>
	                  <tbody>
	                  	<?php $inc = 1 ?>
	                  	@forelse($result as $res)
                          <tr>
                             <td>{{ $inc++ }}</td>
                             <td>{{ number_format($res['week_one']) }}</td>
                             <td>{{ number_format($res['week_two']) }}</td>
                             <td>{{ number_format($res['week_three']) }}</td>
                             <td>{{ number_format($res['week_four']) }}</td>
                             <td>{{ number_format($res['week_five']) }}</td>
                             <td>{{ number_format($res['week_six']) }}</td>
                             <td>{{ number_format($res['total']) }}</td>
                          </tr>
                        @empty
                        @endforelse
	                  </tbody>
	                     </tfoot>
	               </table>
	            </div>
	         </div>
	      </div>
	   </div>
	   </div>
	</section>
@endsection

@section('script')
    <script>
      $(function () {
        $("#example1").DataTable();});


      $('.adf').hide();
      $('.cdb').on('click', function(){
      $('.adf').slideToggle();});

    $('.cadfxx').on('click', function(){
       $('.adf').slideToggle();
    });

    </script>
@endsection