@if(count($other_expenses))
    <div class="">
        <center> <small style="color: red;">Expenses: ({{ count($other_expenses) }}) </small> </center>
        <table class="table table-responsive table-hover" id="example1">
            <thead>
                <th>
                    <center>
                        S/N
                    </center>
                </th>
                <th>
                    <center>
                        Title
                    </center>
                </th>
                <th>
                    <center>
                        Description
                    </center>
                </th>
                <th>
                    <center>
                        Category
                    </center>
                </th>
                <th>
                    <center>
                        Expected Amount
                    </center>
                </th>
                <th>
                    <center>
                        Requested From
                    </center>
                </th>
                <th>
                    <center>
                        Status
                    </center>
                </th>
                {{-- <th>
                    <center>
                        Approved by
                    </center>
                </th> --}}
                <th>
                    <center>
                        Approve
                    </center>
                </th>
                <th>
                    <center>
                        Decline
                    </center>
                </th>
            </thead>
            <tbody>
            <?php $num = 1; ?>
            @foreach($other_expenses as $expense)
                <tr>
                    <td>
                        <center>
                            ({{ $num++ }})
                        </center>
                    </td>
                    <td>
                        <center> {{ $expense->title }} </center>
                    </td>
                    <td>
                        <center>
                            {{ $expense->description }}
                        </center>
                    </td>
                    <td>
                        {{ $expense->category }}<span style="font-weight: bold;"></span>
                    </td>

                     <td>  &#8358;
                        {{ number_format($expense->amount, 2) }}<span style="font-weight: bold;"></span>
                    </td>

                    <td>
                        {{ isset($expense->owner) ? $expense->owner : null }}<span style="font-weight: bold;"></span>
                    </td>

                    <td>
                        {{ $expense->status }}<span style="font-weight: bold;"></span>
                    </td>
                    <td>
                        <center>
                        @if($expense->status == "pending")
                                <a href="{{ url('other-expenses/approveExpense', $expense->id) }}" data-toggle="modal" class="btn btn-xs btn-primary">Approve</a>
                        @elseif($expense->status == "declined")
                                <a class="btn btn-xs btn-danger">Declined</a>
                        @elseif($expense->status == "approved")
                                <a class="btn btn-xs btn-success">Approved</a>
                        @endif
                        </center>
                    </td>
                    <td>
                        <center>
                            @if($expense->status == "pending")
                                <a href="{{ url('other-expenses/declineExpense', $expense->id) }}"
                                    class="btn btn-xs btn-primary">Decline</a>
                            @elseif($expense->status == "declined")
                                <a class="btn btn-xs btn-danger">
                                    Declined</a>
                            @else
                                <a class="btn btn-xs btn-success">
                                    Approved</a>
                            @endif
                        </center>
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>
        <hr>

    </div>
@else
    <div class="row">
        <div>
            <center> <b>No Expense Available </b> </center>
        </div>
    </div>
@endif