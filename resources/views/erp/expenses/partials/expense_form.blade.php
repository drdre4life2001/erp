<form method="POST" action="{{url('other-expenses')}}">
    <div class="row">
            <div class="col-lg-5">
                <label for=""> Title: </label>

                <input type="text" required placeholder="Expense Title" name="title" id="input" class="form-control" value="" >

                @if ($errors->has('title'))<p class="help-block" style="color: red">{{ $errors->first('title') }}</p> @endif <br>
            </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <label for=""> Choose Category: </label>

            <select name="account_header_id" required id="" class="form-control">
                <option value="" >Choose Category</option>
                @foreach($categories as $category)
                    <option value="{{$category->id}}" >{{ $category->name }}</option>
                @endforeach
            </select>

            @if ($errors->has('account_header_id'))<p class="help-block" style="color: red">{{ $errors->first('account_header_id') }}</p> @endif <br>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <label>Content</label>
            <textarea required name="description" rows="5" cols="2" id="input" class="form-control" value="Description goes here" > </textarea>
            @if ($errors->has('content'))<p class="help-block" style="color: red">{{ $errors->first('content') }}</p> @endif <br>
        </div>
        <br> <br>
    </div>

    <div class="row">
            <div class="col-lg-5">
                <label for=""> Expected Amount: </label>

                <input type="number" required placeholder="Expected Amount" name="amount"  id="input" class="form-control" value="" >

                @if ($errors->has('amount'))<p class="help-block" style="color: red">{{ $errors->first('amount') }}</p> @endif <br>
            </div>
    </div>
     <br> <br>
    <div class="col-lg-2 col-lg-offset-1">
            <input type="submit" value="Submit Expense Request" class="btn btn-success"/> <br> <br>
    </div>

</form>