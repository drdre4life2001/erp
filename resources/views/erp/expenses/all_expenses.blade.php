@extends('erp.layouts.master')
  
  @section('title')
    Finance - Expense Pivot
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

@section('content')
	<section class="content-header">
	   <h1>     Expense Pivot
	   </h1>
	   <ol class="breadcrumb">
	      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	      <li class="active">Expense Pivot</li>
	   </ol>
	</section>
	<section class="content">
	  <div class="col-md-12"> 
	   <div class="nav-tabs-custom">
	      <ul class="nav nav-tabs">
	         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>Expense Pivot
	            </b></a>
	         </li>
	      </ul>
	      <div class="tab-content" style="padding: 2%">
	         <div class="tab-pane active" id="tab_1">
	            <hr/ style="clear: both">
	            <div id="search" style="background: #eee; height: auto; padding: 2% 5%">
	               {{-- @include('erp.expenses.partials.expense_form') --}}
	            </div>
	            <div class="box-body">
	               @include('erp.expenses.partials.expense_table')
	            </div>
	         </div>
	      </div>
	   </div>
	   </div>
	</section>
@endsection

@section('script')
    <script>
      $(function () {
        $("#example1").DataTable();});


      $('.adf').hide();
      $('.cdb').on('click', function(){
      $('.adf').slideToggle();});

    $('.cadfxx').on('click', function(){
       $('.adf').slideToggle();
    });

    </script>
@endsection