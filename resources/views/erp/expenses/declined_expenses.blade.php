@extends('erp.layouts.master')
  
  @section('title')
    Expenses
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

@section('content')
	<section class="content-header">
	   <h1>     Expense Pivot
	   </h1>
	   <ol class="breadcrumb">
	      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	      <li class="active">Expense Pivot</li>
	   </ol>
	</section>
	<section class="content">
	  <div class="col-md-12"> 
	   <div class="nav-tabs-custom">
	      <ul class="nav nav-tabs">
	         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>Expense Pivot
	            </b></a>
	         </li>
	      </ul>
	      <div class="tab-content" style="padding: 2%">
	         <div class="tab-pane active" id="tab_1">
	            <hr/ style="clear: both">
	            <div id="search" style="background: #eee; height: auto; padding: 2% 5%">
	               @include('erp.expenses.partials.expense_form')
	            </div>
	            <div class="box-body">
	               @if(count($other_expenses))
	                   <div class="row">
	                       <center> <small style="color: red;">My Expenses: ({{ count($other_expenses) }}) </small> </center>
	                       <table class="table table-responsive table-hover" id="organizations-table">
	                           <thead>
	                           <th>
	                               <center>
	                                   S/N
	                               </center>
	                           </th>
	                           <th>
	                               <center>
	                                   Title
	                               </center>
	                           </th>
	                           <th>
	                               <center>
	                                   Description
	                               </center>
	                           </th>
	                           <th>
	                               <center>
	                                   Category
	                               </center>
	                           </th>
	                           <th>
	                               <center>
	                                   Expected Amount
	                               </center>
	                           </th>

	                           </thead>
	                           <tbody>
	                           <?php $num = 1; ?>
	                           @foreach($other_expenses as $expense)
	                               <tr>
	                                   <td>
	                                       <center>
	                                           ({{ $num++ }})
	                                       </center>
	                                   </td>
	                                   <td>
	                                       <center> {{ $expense->title }} </center>
	                                   </td>
	                                   <td>
	                                       <center>
	                                           {{ $expense->description }}
	                                       </center>
	                                   </td>
	                                   <td>
	                                       {{ $expense->category }}<span style="font-weight: bold;"></span>
	                                   </td>

	                                   <td>  &#8358;
	                                       {{ number_format($expense->amount, 2) }}<span style="font-weight: bold;"></span>
	                                   </td>
	                               </tr>
	                           @endforeach
	                           </tbody>
	                       </table>
	                       <hr>

	                   </div>
	               @else
	                   <div class="row">
	                       <div>
	                           <center> <b>None of your expense has been Approved. </b> </center>
	                       </div>
	                   </div>
	               @endif
	            </div>
	         </div>
	      </div>
	   </div>
	   </div>
	</section>
@endsection

@section('script')
    <script>
      $(function () {
        $("#example1").DataTable();});


      $('.adf').hide();
      $('.cdb').on('click', function(){
      $('.adf').slideToggle();});

    $('.cadfxx').on('click', function(){
       $('.adf').slideToggle();
    });

    </script>
@endsection