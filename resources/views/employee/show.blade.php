@extends('erp.layouts.master')
  
  @section('title')
    Human Resource - Employee
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
      <section class="content-header">
         <h1>
            Employee
         </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Employee</li>
         </ol>
      </section>
      <section class="content">
         <div class="col-md-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>Employee
                     </b></a>
                  </li>
               </ul>
               <div class="tab-content" style="padding: 2%">
                  <div class="tab-pane active" id="tab_1">
                     <p align="right">
                        {{-- <button class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>
                        </button> --}}
                     </p>
                     <div class="adf">
                        @if(isset($errors))
                            @if(count($errors) > 0)
                                 <div class="alert alert-info">
                                     <ul>
                                         @foreach($errors->all() as $error)
                                             <li>{{$error}}</li>
                                             @endForeach
                                     </ul>
                                 </div>
                            @endIf
                        @endIf
                     </div>
                     
                     @if(Session::has('success'))
                         <div class="alert alert-success">
                             <ul>
                                     <li>{{Session::get('success')}}</li>
                             </ul>
                         </div>
                     @endif
                     <div class="" style="background:#ecf0f5; float: left; width: 100%; ">
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                        <div class="col-md-10">
                           <section class="row text-left">
                               <div class="form-group col-sm-3 ">
                                   <?php $pix = isset($employee->picture) ? URL::to(''.$employee->picture) : '/uploads/employee_pictures/user.jpg'?>
                                   <p>
                                      <a data-toggle="modal"
                                         data-target="#pixModal">
                                          <img class="img img-responsive img-rounded" src="{{ $pix }}" alt="{{$employee->picture}}"
                                               width="100" height="100"/>
                                      </a>
                                   </p>
                                   <p>Click on picture to update it.</p>
                               </div>
                               <div class="col-sm-9 text-center">
                                           <p>
                                           <h4>{!! $employee->lastname !!} {!! $employee->first_name !!} {!! $employee->other_name !!}</h4></p>
                                           <h5>#{!! $employee->identification_number !!}</h5>
                                           <!--
                                           <p><a href="{{URL::to('employee/account/'.$employee->id)}}" class="btn  btn-primary">Create User
                                                   Account</a></p>
                                           -->
                               </div>
                           <div class="row" style="padding-left: 20px">
                               @include('employee.show_fields')
                           </div>
                           </section>
                        </div>
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                     </div>
                     <hr/ style="clear: both">
                     <div class="box-body">
                     </div>
                  </div>
               </div>
               <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </section>
  @endsection

  @section('script')
       <script type="text/javascript">
         
         $("#example1").DataTable();


         // $('.adf').hide();
         // $('.cdb').on('click', function(){
         //   $('.adf').slideToggle();
         // });

         @if ($errors->has('company_id') || $errors->has('bank') || $errors->has('description') || $errors->has('interest_rate') || $errors->has('amount') || $errors->has('period'))
           $('.adf').slideToggle();
         @endif

           // $('.cadfxx').on('click', function(){
           //    $('.adf').slideToggle();
           // });

       </script>
  @endsection
