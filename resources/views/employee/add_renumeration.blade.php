@extends('layouts.erp')

@section('page_title')
@endsection
@section('error')
    @if(isset($errors))
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endForeach
                </ul>

            </div>
            @endIf
            @endIf
@endsection
@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Add Renumeration for An Employee</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <form action="{{ url('add_employee_renumeration') }}" method="post">
        <div class="col-md-6">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap mt-40">
                            <div class="form-group">
                                <label class="control-label mb-10">Employee</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="icon-user"></i></div>
                                    <select class="form-control selectpicker" data-show-subtext="true"
                                            data-live-search="true" name="employee_id">
                                        @foreach($employees as $employee)
                                            <option data-subtext="{{ $employee->first_name }} {{ $employee->lastname }}"
                                                    value="{{ $employee->id }}">{{ $employee->first_name }}
                                                , {{ $employee->lastname }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10" for="exampleInputuname_1">Basic Salary</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                           placeholder="Basic Salary (Gross)" name="basic_pay">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10" for="exampleInputuname_1">Housing Allowance</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                           placeholder="Housing Allowance (Gross)" name="housing">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10" for="exampleInputuname_1">Transport Allowance</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                           placeholder="Transport Allowance (Gross)" name="transport">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10" for="exampleInputuname_1">Leave Allowance</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                           placeholder="Leave Allowance" name="leave">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10" for="exampleInputuname_1">Utility Allowance</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                           placeholder="Utility Allowance" name="utility">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10" for="exampleInputuname_1">Meal Allowance</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                           placeholder="Meal Allowance" name="meal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10" for="exampleInputuname_1">Others</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                           placeholder="Others" name="others">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                            <div class="form-wrap mt-40">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="mt-25">
                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Entertainment
                                                    Allowance</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                                           placeholder="Meal Allowance" name="entertainment">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Number of
                                                    Dependent Relatives</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                                           placeholder="Number of Dependent Relatives"
                                                           name="dependable_relative">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Number of
                                                    Children</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                                           placeholder="Number of Children" name="children">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Compute National Housing Fund
                                                            in Payroll?</label>
                                                        <div class="input-group">
                                                            <select class="form-control selectpicker"
                                                                    data-show-subtext="true"
                                                                    data-live-search="true" name="nhf">
                                                                <option data-subtext="Yes"
                                                                        value="1">Yes
                                                                </option>
                                                                <option data-subtext="No"
                                                                        value="0">No
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label mb-10"
                                                                   for="exampleInputuname_1">Compute Life Assurance in
                                                                Payroll?</label>
                                                            <div class="input-group">
                                                                <select class="form-control selectpicker"
                                                                        data-show-subtext="true"
                                                                        data-live-search="true" name="life_assurance">
                                                                    <option data-subtext="Yes"
                                                                            value="1">Yes
                                                                    </option>
                                                                    <option data-subtext="No"
                                                                            value="0">No
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label mb-10"
                                                                   for="exampleInputuname_1">Consolidated Relief
                                                                Allowance in Payroll?</label>
                                                            <div class="input-group">
                                                                <select class="form-control selectpicker"
                                                                        data-show-subtext="true"
                                                                        data-live-search="true" name="cra">
                                                                    <option data-subtext="Yes"
                                                                            value="1">Yes
                                                                    </option>
                                                                    <option data-subtext="No"
                                                                            value="0">No
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label mb-10"
                                                                   for="exampleInputuname_1">Compute Pension in Payroll
                                                                ?</label>
                                                            <div class="input-group">
                                                                <select class="form-control selectpicker"
                                                                        data-show-subtext="true"
                                                                        data-live-search="true" name="pension">
                                                                    <option data-subtext="Yes"
                                                                            value="1">Yes
                                                                    </option>
                                                                    <option data-subtext="No"
                                                                            value="0">No
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Does Employee have Any
                                                            Disability ?</label>
                                                        <div class="input-group">
                                                            <select class="form-control selectpicker"
                                                                    data-show-subtext="true"
                                                                    data-live-search="true" name="disability">
                                                                <option data-subtext="Yes"
                                                                        value="1">Yes
                                                                </option>
                                                                <option data-subtext="No"
                                                                        value="0">No
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-15">
                                        <input class="btn btn-success btn-anim" type="submit"
                                               value="Submit Employee Renumeration">
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    <!-- /Row -->
@endsection

@section('script')

@endsection
