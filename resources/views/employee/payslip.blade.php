@extends('erp.layouts.master')

@section('title')
    Human Resource - Pay Slip
@endsection

@section('sidebar')
    @include('erp.partials.sidebar')
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Payslip</h6>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('download_payslip/'.$payslip->employee_id.'/'.$payslip->month.'/'.$payslip->year) }}"><h6 class="btn btn-purple">Download to PDF</h6></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <!-- Section:Biography -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-block text-xs-left">
                                    <h2 class="card-title" style="color:#a17be5"><i
                                                class="fa fa-user fa-fw"></i>{{ $payslip->employee->first_name }}
                                        , {{ $payslip->employee->lastname }}</h2>
                                    <h3 class="card-title" style="color:#a17be5"><i
                                                class="fa fa-calendar fa-fw"></i> {{ $payslip->month }}
                                        , {{ $payslip->year }} Payslip</h3>
                                </div>
                            </div>
                        </div>
                        <!-- End:Biography -->
                        <div class="panel panel-default">
                            <div class="panel panel-heading">
                                <h3>Benefits & Allowances</h3>
                            </div>
                            <div class="panel panel-body">
                                <div class="panel panel-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card card-block">
                                                <table class="table table-bordered table-hover">
                                                    <thead class="thead-default">
                                                    <tr>
                                                        <th>Item</th>
                                                        <th>Amount</th>
                                                        <th>Amount to Date</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Basic Salary</td>
                                                        <td>{{ number_format($payslip->salary, 2) }}</td>
                                                        <td>{{ number_format($basic_to_date, 2) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Transport Allowance</td>
                                                        <td>{{ number_format($payslip->transport, 2) }}</td>
                                                        <td>{{ number_format($transport_to_date, 2) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Leave Allowance</td>
                                                        <td>{{ number_format($payslip->leave, 2) }}</td>
                                                        <td>{{ number_format($leave_to_date, 2) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Housing Allowance</td>
                                                        <td>{{ number_format($payslip->housing, 2) }}</td>
                                                        <td>{{ number_format($housing_to_date, 2) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Meal Allowance</td>
                                                        <td>{{ number_format($payslip->meal, 2) }}</td>
                                                        <td>{{ number_format($meal_to_date, 2) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Entertainment Allowance</td>
                                                        <td>{{ number_format($payslip->entertainment, 2) }}</td>
                                                        <td>{{ number_format($entertainment_to_date, 2) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Entertainment Allowance</td>
                                                        <td>{{ number_format($payslip->entertainment, 2) }}</td>
                                                        <td>{{ number_format($entertainment_to_date, 2) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Other Allowances</td>
                                                        <td>{{ number_format($payslip->other_taxable_benefits, 2) }}</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h5><b>Total Gross Payable (a)</b></h5></td>
                                                        <td><h5>
                                                                <b>{{ number_format($payslip->total_gross_payable, 2) }}</b>
                                                            </h5></td>
                                                        <td><h5><b>{{ number_format($grosspay_to_date, 2) }}</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h5><b>Less: Total Tax Reliefs</b></h5></td>
                                                        <td><h5>
                                                                <b>{{ number_format($payslip->total_gross_payable - $payslip->taxable_pay, 2) }}</b>
                                                            </h5></td>
                                                        <td><h5><b>{{ number_format($tax_reliefs_to_date, 2) }}</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h5><b>Total Taxable Pay</b></h5></td>
                                                        <td><h5><b>{{ number_format($payslip->taxable_pay, 2) }}</b>
                                                            </h5></td>
                                                        <td><h5><b>{{ number_format($taxable_pay_to_date, 2) }}</b></h5>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel panel-heading">
                                <h3>Deductions</h3>
                            </div>
                            <div class="panel panel-body">
                                <div class="panel panel-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card card-block">
                                                <table class="table table-bordered table-hover">
                                                    <thead class="thead-default">
                                                    <tr>
                                                        <th>Item</th>
                                                        <th>Amount</th>
                                                        <th>Amount to Date</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody style="color: #a17be5">
                                                    <tr>
                                                        <td><b>PAYE Tax</b></td>
                                                        <td><b>{{ number_format($payslip->tax, 2) }}</b></td>
                                                        <td><b>{{ number_format($tax_to_date, 2) }}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Pension</b></td>
                                                        <td><b>{{ number_format($payslip->pension, 2) }}</b></td>
                                                        <td><b>{{ number_format($pension_to_date, 2) }}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>NHF Contribution</b></td>
                                                        <td><b>{{ number_format($payslip->nhf, 2) }}</b></td>
                                                        <td><b>{{ number_format($nhf_to_date, 2) }}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Salary Advance / Loan</b></td>
                                                        <td><b>{{ number_format($payslip->salary_advance, 2) }}</b></td>
                                                        <td><b>{{ number_format($salary_advance_to_date, 2) }}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Other Deductions</b></td>
                                                        <td><b>{{ number_format($payslip->other_deductions, 2) }}</b>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr style="color: red">
                                                        <td><h5><b>Total Deductions</b></h5></td>
                                                        <?php
                                                        $t = $payslip->tax;
                                                        $p = $payslip->pension;
                                                        $n = $payslip->nhf;
                                                        $od = $payslip->other_deductions;
                                                        $sa = $payslip->salary_advance;
                                                        $total = $t + $p + $n + $od + $sa;
                                                        ?>
                                                        <td><h5><b>{{ number_format($total, 2) }}</b></h5></td>
                                                        <?php $total_deductions = $tax_to_date + $pension_to_date + $nhf_to_date + $salary_advance_to_date; ?>
                                                        <td><h5> {{ number_format($total_deductions, 2) }} </h5></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel panel-heading">
                                <h3>Net Pay</h3>
                            </div>
                            <div class="panel panel-body">
                                <div class="panel panel-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card card-block">
                                                <table class="table table-bordered table-hover">
                                                    <thead class="thead-default">
                                                    <tr>
                                                        <th>Item</th>
                                                        <th>Amount</th>
                                                        <th>Amount to Date</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><h3>Net Salary</h3></td>
                                                        <td><h3>{{ number_format($payslip->net_pay, 2) }}</h3></td>
                                                        <td><h3>{{ number_format($netpay_to_date, 2) }}</h3></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection