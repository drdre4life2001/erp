 @extends('layouts.erp')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Employees' Deductions</h6>
                    </div>
                    <div class="pull-right">
                        <h6 class="btn btn-primary"><a href="{{ url('add_employee_deduction') }}">Add Deduction</a></h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_2" class="table table-hover display  pb-30" >
                                    <thead class="panel panel-heading">
                                    <tr>
                                        <th>Employee Name</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                        <th>Month</th>
                                        <th>Year</th>
                                        <th>Date</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($deductions as $bonus){ ?>
                                    <tr>
                                        <td>{{ $bonus->first_name }} {{ $bonus->lastname }}</td>
                                        <td>{{ $bonus->description }}</td>
                                        <td>&#x20A6;{{ number_format($bonus->amount, 2) }}</td>
                                        <td>{{ $bonus->month }}</td>
                                        <td>{{ $bonus->year }}</td>
                                        <td>{{ $bonus->created_at }}</td>
                                        <td><a href="{{ url('delete_record/'.$bonus->id.'/deduction') }}"><button onclick="return confirm('Are you sure? This process is not reversible!')">Delete</button></a></td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection