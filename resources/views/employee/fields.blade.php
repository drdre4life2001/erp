<div class="panel-body">
    <div class="row">
        <div class="col-sm-12 col-xs-12">
            <div class="form-wrap">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-purple">
                            <div class="panel panel-header">
                                <strong>Bio-Data</strong>
                            </div>
                            <div class="panel panel-body">
                            <div class="panel panel-content">
                                <div class="row">
                                    <!-- First Name Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="first_name">First Name:</label>
                                        <input class="form-control" name="first_name" type="text" id="first_name"
                                    value="{{ old('first_name') }} @if(isset($employee->first_name) ? $employee->first_name : null) @endif">
                                    </div>

                                    <!-- Lastname Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="lastname">Lastname:</label>
                                        <input class="form-control" name="lastname" type="text" id="lastname"
                                               value="{{ old('lastname') }}">
                                    </div>

                                    <!-- Other Name Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="other_name">Other Name:</label>
                                        <input class="form-control" name="other_name" type="text" id="other_name"
                                               value="{{ old('other_name') }}">
                                    </div>

                                    <!-- Date Of Birth Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="date_of_birth">Date Of Birth:</label>
                                        <input class="form-control" name="date_of_birth" type="date" id="date_of_birth"
                                               value="{{ old('date_of_birth') }}">
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- City Of Birth Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="city_of_birth">State Of Birth:</label>
                                        <input class="form-control" name="city_of_birth" type="text" id="city_of_birth"
                                               value="{{ old('city_of_birth') }}">
                                    </div>

                                    <!-- Disability Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="disability">Disability:</label>
                                        <input class="form-control" name="disability" type="text" id="disability"
                                               value="{{ old('disability')  }}">
                                    </div>

                                    <!-- Code Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="code">Gender:</label>
                                        <select name="code" class="form-control">
                                            @if(isset($gender))
                                                @foreach($gender as $eachGender)
                                                    <option value="{{$eachGender->id}}">{{$eachGender->name}} </option>
                                                @endforeach
                                                @endIf
                                        </select>
                                    </div>

                                    <!-- No Of Children Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="no_of_children">No Of Children:</label>
                                        <input class="form-control" name="no_of_children" type="number"
                                               id="no_of_children"
                                               value="{{ old('no_of_children') }}">
                                    </div>
                                </div>

                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="panel panel-purple">
                            <div class="panel panel-header">
                                <strong>Contact Information</strong>
                            </div>
                            <div class="panel panel-body">
                                <div class="row">
                                    <!-- First Name Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="first_name">E-Mail Address:</label>
                                        <input class="form-control" name="email" type="text" id="email"
                                               value="{{ old('email') }}">
                                    </div>

                                    <!-- Lastname Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="phone">Phone Number:</label>
                                        <input class="form-control" name="phone" type="text" id="phone"
                                               value="{{ old('phone') }}">
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- Code Sys Country Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="code_sys_country">Country:</label>
                                        <select name="code_sys_country" class="form-control">
                                            @if(isset($countries))
                                                @foreach($countries as $country)
                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endforeach
                                                @endIf
                                        </select>
                                    </div>

                                    <!-- Permanent Address Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="permanent_address">Permanent Address:</label>
                                        <input class="form-control" name="permanent_address" type="text"
                                               id="permanent_address"
                                               value="{{ old('permanent_address') }}">
                                    </div>

                                    <!-- Home Address Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="home_address">Home Address:</label>
                                        <input class="form-control" name="home_address" type="text" id="home_address"
                                               value="{{ old('home_address') }}">
                                    </div>
                                    <!-- Code Field -->

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">

                        <div class="panel panel-purple">
                            <div class="panel panel-header">
                                <strong>Work Information</strong>
                            </div>

                            <div class="panel panel-body">
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="identification_number">Identification Number:</label>
                                        <input class="form-control" name="identification_number" type="text"
                                               id="identification_number"
                                               value="{{ old('identification_number') }}">
                                    </div>

                                    <!-- Passport No Field -->



                                    <!-- Date First Hired Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="date_first_hired">Date First Hired:</label>
                                        <input class="form-control" name="date_first_hired" type="date"
                                               id="date_first_hired"
                                               value="{{ old('date_first_hired') }}">
                                    </div>

                                    <!-- Id Hr Work Addresses Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="id_hr_work_addresses">Office Location</label>
                                        <select name="id_hr_work_addresses" style="width:100%;" @if(empty($workAddresses)) title="Please create work address" @endif required class="form-control work_address_search"
                                                id="id_hr_work_addresses">
                                        </select>
                                    </div>

                                    <!-- Id Hr Grade Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="id_hr_grade">Select Grade:</label>
                                        <select name="id_hr_grade" class="form-control">
                                            @if(isset($grades))
                                                @foreach($grades as $eachGrade)
                                                    <option value="{{$eachGrade->id}}">{{$eachGrade->name}}
                                                        [{{$eachGrade->code}}]
                                                    </option>
                                                @endforeach
                                                @endIf
                                        </select>
                                    </div>

                                    <!-- Id Hr G Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="id_hr_company">Select Company:</label>
                                        <select name="id_hr_company" style="width:100%;" class="form-control organization_search">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">

                        <div class="panel panel-purple">
                            <div class="panel panel-header">
                                <strong>Bank Information</strong>
                            </div>

                            <div class="panel panel-body">
                                <!-- Id Sys Banks Field -->
                                <div class="form-group col-sm-6">
                                    <label for="id_sys_banks">Select Bank:</label>
                                    <select name="id_sys_banks" class="form-control">
                                        <option value="">Bank</option>
                                        @if(isset($banks))
                                            @foreach($banks as $bank)
                                                <option value="{{$bank->id}}"> {{$bank->name}}</option>
                                            @endforeach
                                            @endIf
                                    </select>
                                </div>

                                <!-- Account No Field -->
                                <div class="form-group col-sm-6">
                                    <label for="account_no">Account No:</label>
                                    <input class="form-control" name="account_no" type="text" id="account_no"
                                           value="{{ old('account_no') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">

                        <div class="panel panel-purple">
                            <div class="panel panel-header">
                                <strong>Job Role</strong>
                            </div>

                            <div class="panel panel-body">
                                <div class="row">
                                    <!-- Id Hr Job Title Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="id_hr_job_title">Job Title:</label>
                                        <select name="id_hr_job_title" style="width:100%;" class="form-control job_title_search" required id="id_hr_job_title">
                                            
                                        </select>
                                    </div>


                                    <!-- Note Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="note">Job Note:</label>
                                        <textarea class="form-control" name="job_note">
                                                {{ old('job_note') }}
                                        </textarea>

                                    </div>

                                    <!-- Id Sys Departments Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="id_sys_departments">Departments:</label>
                                        <select name="id_sys_departments" class="form-control" id="id_sys_departments">
                                            <option value="">Select One</option>
                                            @if(isset($departments))
                                                @foreach($departments as $department)
                                                    <option value="{{$department->id}}">{{$department->name}}
                                                        [{{$department->code}}]
                                                    </option>
                                                @endforeach
                                                @endIf
                                        </select>
                                    </div>

                                    <!-- Note Field -->
                                    <div class="form-group col-sm-6">
                                        <label for="note">Department Note:</label>
                                        <textarea class="form-control" name="department_note">
                                            {{ old('department_note') }}
                                        </textarea>

                                    </div>
                                    <!-- Assign a line manager -->
                                    <div class="form-group col-sm-6">
                                        <label for="line_manager">Line Manager:</label>
                                        <select name="line_manager" style="width:100%;" class="form-control employee_search" id="line_manager">
                                        </select>
                                    </div>
                                    <!--Assign a permission level -->
                                    <div class="form-group col-sm-6">
                                        <label for="role">Permission Level</label>
                                        <select name="role" class="form-control" id="role">
                                            <option value="">Select...</option>
                                            @if(isset($roles))
                                                @foreach($roles as $role)
                                                    <option value="{{$role->id}}">
                                                    {{$role->name}}
                                                    </option>
                                                @endforeach
                                            @endIf
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12 col-lg-4 col-lg-offset-4">
                        <?php if(isset($employee)){ ?>
                        <input type="hidden" name="employee_id" value="{{$employee->id}}">
                        <?php } ?>
                        <input class="btn btn-purple btn-lg" type="submit" value="Save">
                        <a href="{{URL::to('employee')}}" class="btn btn-default btn-lg">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>