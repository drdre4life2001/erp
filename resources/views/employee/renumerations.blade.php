 @extends('layouts.erp')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Employees' Renumerations</h6>
                    </div>
                    <div class="pull-right">
                        <h6 class="btn btn-primary"><a href="{{ url('add_employee_renumeration') }}">Add Renumeration</a></h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_2" class="table table-hover display pb-30" >
                                    <thead class="panel panel-heading">
                                    <tr>
                                        <th>S/N</th>
                                        <th class="">Employee Name</th>
                                        <th>Basic Salary</th>
                                        <th>Housing Allowance</th>
                                        <th>Transport Allowance</th>
                                        <th>Entertainment Allowance</th>
                                        <th>Leave Allowance</th>
                                        <th>Utility Allowance</th>
                                        <th>N.H.F</th>
                                        <th>Life Assurance</th>
                                        <th>Consolidated Relief Allowance</th>
                                        <th>Fixed CRA</th>
                                        <th>Dependable Relatives</th>
                                        <th>Children</th>
                                        <th>Disability</th>
                                        <th>Date Added</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1;?>
                                    <?php foreach($renumerations as $bonus){ ?>
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $bonus->first_name }} {{ $bonus->lastname }}</td>
                                        <td>&#x20A6;{{ number_format($bonus->basic_pay,2) }}</td>
                                        <td>&#x20A6;{{ number_format($bonus->housing, 2) }}</td>
                                        <td>&#x20A6;{{ number_format($bonus->transport, 2) }}</td>
                                        <td>&#x20A6;{{ number_format($bonus->entertainment, 2) }}</td>
                                        <td>&#x20A6;{{ number_format($bonus->leave_allowance, 2) }}</td>
                                        <td>&#x20A6;{{ number_format($bonus->utility, 2) }}</td>
                                        <td>{{ $bonus->nhf == 1 ? 'Yes' : 'No' }}</td>
                                        <td>{{ $bonus->life_assurance == 1 ? 'Yes' : 'No' }}</td>
                                        <td>{{ $bonus->cra == 1 ? 'Yes' : 'No' }}</td>
                                        <td>{{ $bonus->fixed_cra == 1 ? 'Yes' : 'No' }}</td>
                                        <td>{{ $bonus->children }}</td>
                                        <td>{{ $bonus->dependable_relative }}</td>
                                        <td>{{ $bonus->disability }}</td>
                                        <td>{{ $bonus->created_at }}</td>
                                        <td><a href="{{ url('update_employee_renumeration/'.$bonus->id) }}"><button onclick="return confirm('Are you sure? This process is not reversible!')">Update</button></a></td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection