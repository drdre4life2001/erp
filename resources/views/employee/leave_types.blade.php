 @extends('layouts.erp')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Leave Types</h6>
                    </div>
                    <div class="pull-right">
                        <h6 class="btn btn-primary"><a href="{{ url('add_leave_type') }}">Add Leave Type</a></h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_2" class="table table-hover display  pb-30" >
                                    <thead class="panel panel-heading">
                                    <tr>
                                        <th>Leave Title</th>
                                        <th>No of Days</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($leave_types as $bonus){ ?>
                                    <tr>
                                        <td>{{ $bonus->name }}</td>
                                        <td>{{ $bonus->days }}</td>
                                        <td><a href="{{ url('delete_record/'.$bonus->id.'/leave') }}"><button onclick="return confirm('Are you sure? This process is not reversible!')">Delete</button></a></td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection