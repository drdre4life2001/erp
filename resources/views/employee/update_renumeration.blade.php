@extends('layouts.erp')

@section('page_title')
@endsection
@section('error')
    @if(isset($errors))
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}} here</li>
                        @endForeach
                </ul>

            </div>
            @endIf
            @endIf
@endsection
@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Update Renumeration for An Employee</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap mt-40">
                            <form action="{{ url('update_employee_renumeration') }}" method="post">
                                <div class="form-group">
                                    <label class="control-label mb-10">Employee</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="icon-user"></i></div>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="exampleInputuname_1"
                                                    value="{{ $employee->first_name }}, {{ $employee->lastname }}" readonly>
                                            <input type="hidden" name="employee_id" value="{{ $employee->id }}">

                                            <select class="form-control selectpicker" data-show-subtext="true"
                                               data-live-search="true" name="employee_id">
                                               @if(!empty($employees))
                                                    @foreach($employees as $employee)
                                                        <?php $db_val = $employee->id; ?>
                                                        <option @if($employee->id == $db_val) selected="" @endif data-subtext="{{ $employee->first_name }} {{ $employee->lastname }}"
                                                           value="{{ $employee->id }}">{{ $employee->first_name }}
                                                           , {{ $employee->lastname }}
                                                        </option>
                                                    @endforeach
                                               @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Basic Salary</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                                name="basic_pay" value="{{ $renumeration->basic_pay }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Housing Allowance</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               placeholder="Housing Allowance (Gross)" name="housing" value="{{ $renumeration->housing }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Transport Allowance</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               placeholder="Transport Allowance (Gross)" name="transport" value="{{ $renumeration->transport }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Leave Allowance</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               placeholder="Leave Allowance" name="leave" value="{{ $renumeration->leave_allowance }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Utility Allowance</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               placeholder="Utility Allowance" name="utility" value="{{ $renumeration->utility }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Meal Allowance</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               placeholder="Meal Allowance" name="meal" value="{{ $renumeration->meal }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Others</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               placeholder="Others" name="others" value="{{ $renumeration->others }}">
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap mt-40">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="mt-25">
                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Entertainment Allowance</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                                           placeholder="Meal Allowance" name="entertainment" value="{{ $renumeration->entertainment }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Number of Dependent Relatives</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                                           placeholder="Number of Dependent Relatives" name="dependable_relative" value="{{ $renumeration->dependable_relative }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Number of Children</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                                    <input type="number" class="form-control" id="exampleInputuname_1"
                                                           placeholder="Number of Children" name="children" value="{{ $renumeration->children }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Compute National Housing Fund in Payroll?</label>
                                                        <div class="input-group">
                                                            <ul class="todo-list nicescroll-bar">
                                                                <li class="todo-item">
                                                                    <div class="checkbox checkbox-default">
                                                                        @if( $renumeration->nhf == 1)
                                                                        <input type="checkbox" id="checkbox01" name="nhf" value="1" checked/>
                                                                        @else
                                                                        <input type="checkbox" id="checkbox01" name="nhf" value="1"/>
                                                                        @endif
                                                                        <label for="checkbox01">Yes</label>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label mb-10"
                                                                   for="exampleInputuname_1">Compute Life Assurance in Payroll?</label>
                                                            <div class="input-group">
                                                                <ul class="todo-list nicescroll-bar">
                                                                    <li class="todo-item">
                                                                        <div class="checkbox checkbox-default">
                                                                            @if( $renumeration->life_assurance == 1)
                                                                            <input type="checkbox" id="checkbox01" name="life_assurance" value="1" checked/>
                                                                            @else
                                                                            <input type="checkbox" id="checkbox01" name="life_assurance" value="1"/>
                                                                            @endif
                                                                            <label for="checkbox01">Yes</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label mb-10"
                                                                   for="exampleInputuname_1">Consolidated Relief Allowance in Payroll?</label>
                                                            <div class="input-group">
                                                                <ul class="todo-list nicescroll-bar">
                                                                    <li class="todo-item">
                                                                        <div class="checkbox checkbox-default">
                                                                            @if( $renumeration->fixed_cra == 1)
                                                                            <input type="checkbox" id="checkbox01" name="fixed_cra" value="1" checked/>
                                                                            @else
                                                                            <input type="checkbox" id="checkbox01" name="fixed_cra" value="1"/>
                                                                            @endif
                                                                            <label for="checkbox01">Yes</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label mb-10"
                                                                   for="exampleInputuname_1">Compute Pension in Payroll ?</label>
                                                            <div class="input-group">
                                                                <ul class="todo-list nicescroll-bar">
                                                                    <li class="todo-item">
                                                                        <div class="checkbox checkbox-default">
                                                                            @if( $renumeration->pension == 1)
                                                                                <input type="checkbox" id="checkbox01" name="pension" value="1" checked/>
                                                                            @else
                                                                                <input type="checkbox" id="checkbox01" name="pension" value="1"/>
                                                                            @endif
                                                                            <label for="checkbox01">Yes</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Does Employee have Any Disability ?</label>
                                                        <div class="input-group">
                                                            <ul class="todo-list nicescroll-bar">
                                                                <li class="todo-item">
                                                                    <div class="checkbox checkbox-default">
                                                                        @if( $renumeration->pension == 1)
                                                                            <input type="checkbox" id="checkbox01" name="disability" value="1" checked/>
                                                                        @else
                                                                            <input type="checkbox" id="checkbox01" name="disability" value="1"/>
                                                                        @endif
                                                                        <label for="checkbox01">Yes</label>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-15">
                                        <input class="btn btn-success btn-anim" type="submit" value="Update Employee Renumeration">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection

@section('script')

@endsection
