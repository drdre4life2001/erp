 @extends('layouts.erp')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Employees' Loan Settlements</h6>
                    </div>
                    {{--<div class="pull-right">
                        <h6 class="btn btn-primary"><a href="{{ url('add_employee_loan') }}">Add Loan for An Employee</a></h6>
                    </div>--}}
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_1" class="table table-hover display  pb-30" >
                                    <thead class="panel panel-heading">
                                    <tr>
                                        <th>Employee</th>
                                        <th>Amount</th>
                                        <th>Amount to Date</th>
                                        <th>Total Payable</th>
                                        <th>Amount Outstanding</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($settlements as $bonus){ ?>
                                    <tr>
                                        <td>{{ $bonus->first_name }} {{ $bonus->lastname }}</td>
                                        <td>{{ number_format($bonus->amount, 2) }}</td>
                                        <td>{{ number_format($bonus->amount_to_date, 2) }}</td>
                                        <td>{{ number_format($bonus->total_payable, 2) }}</td>
                                        <td>{{ number_format($bonus->total_outstanding, 2) }}</td>
                                        <td>{{ $bonus->created_at }}</td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection