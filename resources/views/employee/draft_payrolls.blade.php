 @extends('layouts.erp')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Draft Payrolls</h6>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('generate_payroll') }}"><h6 class="btn btn-primary">Generate Draft Payroll</h6></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_1" class="table table-hover display  pb-30" >
                                    <thead class="panel panel-heading">
                                    <tr>
                                        <th>Month</th>
                                        <th>Year</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($payrolls as $bonus){ ?>
                                    <tr>
                                        <td>{{ $bonus->month }}</td>
                                        <td>{{ $bonus->year }}</td>
                                        <td>
                                            <a href="{{ url('export_payroll/'.$bonus->month.'/'.$bonus->year.'/draft') }}"><h6 class="btn btn-success">Download to Excel</h6></a>
                                            <a href="{{ url('confirm_payroll/'.$bonus->month.'/'.$bonus->year.'/'.$bonus->subsidiary_id.'') }}" onclick="confirm('Are you sure you want to confirm this payroll data?')"><h6 class="btn btn-success">Confirm {{ $bonus->month  }}, {{ $bonus->year }} Payroll</h6></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection