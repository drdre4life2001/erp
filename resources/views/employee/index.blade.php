@extends('erp.layouts.master')
  
  @section('title')
    Human Resource - Employee
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
      <section class="content-header">
         <h1>
            Employee
         </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Employee</li>
         </ol>
      </section>
      <section class="content">
         <div class="col-md-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Employee
                     </b></a>
                  </li>
               </ul>
               <div class="tab-content" style="padding: 2%">
                  <div class="tab-pane active" id="tab_1">
                     <p align="right">
                        <a href="{{url('employee/create')}}" class="btn btn-large btn-purple"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add New Employee
                        </a>
                     </p>
                     <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                        <div class="col-md-10">
                           
                        </div>
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                     </div>
                     <hr/ style="clear: both">
                     <div class="box-body">
                        <table class="table table-hover" id="example1">
                                    <thead>
                                    <th>S/N</th>
                                    <th>Identification Number</th>
                                    <th>Last Name</th>
                                    <th>First Name</th>
                                    <th>Other Name</th>
                                    <th>Date Hired</th>
                                    <th>Work Location</th>
                                    <th>Gender</th>
                                    <th>Grade Level</th>
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                        <?php $num = 1; ?>
                                    @foreach($employees as $employee)
                                        <tr>
                                            <td>{{$num++}}</td>
                                            <td>{!! $employee->identification_number !!}</td>
                                            <td>{!! $employee->lastname !!}</td>
                                            <td>{!! $employee->first_name !!}</td>
                                            <td>{!! $employee->other_name !!}</td>
                                            <td>{!! $employee->date_first_hired !!}</td>
                                            <td>{!! $employee->workAddress->name !!}</td>
                                            <td>{!! $employee->gender->name !!}</td>
                                            <td>{!! $employee->grade->name !!}</td>
                                            <td>
                                                <div class='btn-group'>
                                                    <a href="{!! url('show-employee/'. $employee->id."/". auth()->user()->company->uri )!!}"
                                                       class='btn btn-success btn-xs'><i class="fa fa-eye"></i>View</a></div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                        </table>
                     </div>
                  </div>
               </div>
               <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </section>
  @endsection

  @section('script')
       <script type="text/javascript">
           $("#example1").DataTable();
       </script>
  @endsection
