<style>
    .move{
        margin-top:-1%
    }
</style>

<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <strong>Bio Information</strong>
            <span class="pull-right move" >
                <a class="btn btn-purple btn-sm" data-toggle="modal"
                   data-target="#bioModal">Update Data
                </a>
            </span>
        </div>

        <div class="panel panel-body">
            <div class="panel panel-content">
                <div class="row" style="padding-left: 10px; padding-right: 10px">
                    <!-- Gender Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('gender', 'Gender:') !!}
                        <p>{!! @$employee->gender->name !!}</p>
                    </div>

                    <!-- Date Of Birth Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('date_of_birth', 'Date Of Birth:') !!}
                        <p>{!! $employee->date_of_birth !!}</p>
                    </div>

                    <!-- City Of Birth Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('city_of_birth', 'State Of Birth:') !!}
                        <p>{!! $employee->city_of_birth !!}</p>
                    </div>

                    <!-- Id Sys Banks Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('phone', ' Phone Number:') !!}
                        <p>{!! $employee->phone !!}</p>
                    </div>

                    <!-- Id Sys Banks Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('email', ' E-Mail:') !!}
                        <p>{!! $employee->email !!}</p>
                    </div>


                    <!-- Home Address Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('home_address', 'Home Address:') !!}
                        <p>{!! $employee->home_address !!}</p>
                    </div>

                    <!-- Permanent Address Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('permanent_address', 'Permanent Address:') !!}
                        <p>{!! $employee->permanent_address !!}</p>
                    </div>

                    <!-- Code Sys Country Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('code_sys_country', 'Country:') !!}
                        <p>{!! @$employee->country->name !!}</p>
                    </div>

                    <!-- Disability Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('disability', 'Disability:') !!}
                        <p>{!! $employee->disability !!}</p>
                    </div>


                    <!-- No Of Children Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('no_of_children', 'No Of Children:') !!}
                        <p>{!! $employee->no_of_children !!}</p>
                    </div>

                    <!-- Passport No Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('passport_no', 'Passport No:') !!}
                        <p>{!! $employee->passport_no !!}</p>
                    </div>

                    <!-- Date First Hired Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('date_first_hired', 'Date Hired:') !!}
                        <p>{!! $employee->date_first_hired !!}</p>
                    </div>
00
                    <!-- Id Sys Banks Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('id_sys_banks', ' Bank:') !!}
                        <p>{!! @$employee->bank->name !!}</p>
                    </div>

                    <!-- Account No Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('account_no', 'Account No:') !!}
                        <p>{!! @$employee->account_no !!}</p>
                    </div>


                    <!-- Id Hr Work Addresses Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('id_hr_work_addresses', 'Assigned Work Addresses:') !!}
                        <p>{!! @$employee->workAddress->name !!}</p>
                    </div>

                    <!-- Id Hr Grade Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('id_hr_grade', 'Grade Level:') !!}
                        <p>{!! @$employee->grade->name !!}</p>
                    </div>

                    <div class="form-group col-sm-4">
                        {!! Form::label('id_hr_company', 'Company Assigned:') !!}

                        @foreach($companies as $company)
                            <p>{!! $company->company !!}</p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <strong>Line Manager</strong>
        </div>
        <div class="panel panel-body">
            <div class="row">
                <div class="form-group col-sm-12">
                    <table class="table table-responsive table-striped table-bordered ">
                        @if(isset($managers))
                            <tr>
                                <th>Last Name</th>
                                <th>First Name</th>
                                <th>Identification Number</th>
                                <th>Date Assigned</th>
                                <th>Action</th>
                            </tr>
                            @foreach($managers as $manager)
                                <tr>
                                    <td>{{$manager->lastname}}</td>
                                    <td>{{$manager->first_name}}</td>
                                    <td>{{$manager->identification_number}}</td>
                                    <td>{{$manager->created_at}}</td>
                                    <td><a class="btn btn-sm btn-danger" data-toggle="modal"
                                           data-target="#managerModal">Change Line Manager</a></td>
                                </tr>
                                @endForeach
                                @endIf
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <strong>Job Role</strong>
            <span class="pull-right move">
                <a class="btn btn-purple btn-sm" data-toggle="modal" data-target="#jobModal">Change Job Role</a></span>
        </div>
        <div class="panel panel-body">
            <div class="row">
                <div class="form-group col-sm-12">
                    <table class="table table-responsive table-striped table-bordered">
                        @if(isset($employee->jobTitles))
                            <tr>
                                <th>Name</th>
                                <th>Remarks</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Action</th>
                            </tr>
                            @foreach($employee->jobTitles as $jobTitle)
                                <tr>
                                    <td>{{$jobTitle->job->name}}</td>
                                    <td>{{$jobTitle->note}}</td>
                                    <td>{{$jobTitle->start_date}}</td>
                                    <td>{{$jobTitle->end_date}}</td>
                                    <td><a class="btn btn-sm btn-danger">Terminate</a></td>
                                </tr>
                                @endForeach
                                @endIf
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <strong>Department</strong>
            <span class="pull-right move">
                <a class="btn btn-purple btn-sm" data-toggle="modal"
                   data-target="#departmentModal">Change Department</a></span>
        </div>
        <div class="panel panel-body">
            <div class="row">
                <div class="form-group col-sm-12">
                    <table class="table table-responsive table-striped table-bordered">
                        @if(isset($employee->employeeDepartments))
                            <tr>
                                <th>Name</th>
                                <th>Remarks</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Action</th>
                            </tr>
                            @forelse($employee->employeeDepartments as $department)
                                <tr>
                                    <td>{{@$department->department->name}}</td>
                                    <td>{{$department->note}}</td>
                                    <td>{{$department->start_date}}</td>
                                    <td>{{$department->end_date}}</td>
                                    <td><a class="btn btn-sm btn-danger">Terminate</a></td>
                                </tr>
                            @empty
                            @endforelse
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <strong>Education</strong>
            <span class="pull-right move">
                <a class="btn btn-purple btn-sm" data-toggle="modal"
                   data-target="#educationModal">Add Education</a></span>
        </div>
        <div class="panel panel-body">
            <div class="row">
                <div class="form-group col-sm-12">
                    <table class="table table-responsive table-striped table-bordered">
                        @if(isset($employee->employeeEducation))
                            <tr>
                                <th>Institution</th>
                                <th>Department</th>
                                <th>Degree</th>
                                <th>Grade</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Action</th>
                            </tr>
                            @foreach($employee->employeeEducation as $education)
                                <tr>
                                    <td>{{$education->institution->name}}</td>
                                    <td>{{$education->institutionDepartment->name}}</td>
                                    <td>{{$education->mode_of_education}}</td>
                                    <td>{{$education->grade}}</td>
                                    <td>{{$education->startDate}}</td>
                                    <td>{{$education->endDate}}</td>
                                    <td><a class="btn btn-sm btn-danger">Remove</a></td>
                                </tr>
                                @endForeach
                                @endIf
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <strong>Work Experience</strong>
            <span class="pull-right move">
                <a class="btn btn-purple btn-sm" data-toggle="modal"
                   data-target="#experienceModal">Add Experience</a></span>
        </div>
        <div class="panel panel-body">
            <div class="row">
                <div class="form-group col-sm-12">
                    <table class="table table-responsive table-striped table-bordered">
                        @if(isset($employee->employeeExperiences))
                            <tr>
                                <th>Company</th>
                                <th>Designation</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Action</th>
                            </tr>
                            @foreach($employee->employeeExperiences as $experience)
                                <tr>
                                    <td>{{ $experience->organization_name }}</td>
                                    <td>{{ $experience->designation }}</td>
                                    <td>{{ $experience->work_start_date }}</td>
                                    <td>{{ $experience->work_end_date }}</td>
                                    <td><a class="btn btn-sm btn-danger" href="{{ url('delete_experience/'.$experience->id) }}" onclick="return confirm('Are you sure?')">Remove</a></td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <strong>Referee</strong>
            <span class="pull-right move">
                <a class="btn btn-purple btn-sm" data-toggle="modal"
                   data-target="#refereeModal">Add Referee</a></span>
        </div>
        <div class="panel panel-body">
            <div class="row">
                <div class="form-group col-sm-12">
                    <table class="table table-responsive table-striped table-bordered">
                        @if(isset($employee->employeeReferees))
                            <tr>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>E-Mail</th>
                                <th>Relationship</th>
                                <th>Action</th>
                            </tr>
                            @foreach($employee->employeeReferees as $experience)
                                <tr>
                                    <td>{{ $experience->firstname }}, {{ $experience->lastname }}</td>
                                    <td>{{ $experience->address }}</td>
                                    <td>{{ $experience->phone }}</td>
                                    <td>{{ $experience->email }}</td>
                                    <td>{{ $experience->relationship }}</td>
                                    <td><a class="btn btn-sm btn-danger" href="{{ url('delete_referee/'.$experience->id) }}" onclick="return confirm('Are you sure?')">Remove</a></td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <strong>Attachments</strong>
            <span class="pull-right move">
                <a class="btn btn-purple btn-sm" data-toggle="modal"
                   data-target="#attachmentModal">Add Files</a></span>
        </div>
        <div class="panel panel-body">
            <div class="row">
                <div class="form-group col-sm-12">
                    <table class="table table-responsive table-striped table-bordered">
                        @if(isset($employee->employeeAttachments))
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                            @foreach($employee->employeeAttachments as $experience)
                                <tr>
                                    <td>{{ $experience->name }}</td>
                                    <td>{{ $experience->description }}</td>
                                    <td><a class="btn btn-sm btn-danger" href="{{ url('delete_attachment/'.$experience->id) }}" onclick="return confirm('Are you sure?')">Remove</a>
                                        <a class="btn btn-sm btn-danger" href="{{ url('download_attachment/'.$experience->id) }}" onclick="return confirm('Are you sure?')">Download</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
    <!-- Modals  -->
    <div id="departmentModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Department</h4>
                </div>
                {!! Form::open(['route' => ['employeeDepartments.store'], 'method' => 'post']) !!}
                <div class="modal-body">
                    <div class="form-group ">
                        <input type="hidden" name="employee_id" value="{{$employee->id}}">

                        <label for="id_hr_job_title">Departments:</label>
                        <select name="id_sys_departments" class="form-control" id="id_sys_departments">
                            @if(isset($departments))
                                @foreach($departments as $department)
                                    <option value="{{$department->id}}">{{$department->name}} [{{$department->code}}]
                                    </option>
                                @endforeach
                                @endIf
                        </select>
                    </div>

                    <!-- Note Field -->
                    <div class="form-group ">
                        <label for="department_note">Remark:</label>
                        <textarea class="form-control" name="department_note">

                    </textarea>

                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::button('<i class="glyphicon glyphicon-book"></i> Change',
                    ['type' => 'submit', 'class' => 'btn btn-default btn-purple', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>

    <div id="jobModal" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Job Role</h4>
                </div>
                {!! Form::open(['route' => ['employeeJobs.store'], 'method' => 'post']) !!}
                <div class="modal-body">
                    <div class="form-group ">
                        <input type="hidden" name="employee_id" value="{{$employee->id}}">

                        <label for="id_hr_job_title">Job Role:</label>
                        <select name="id_hr_job_title" class="form-control" id="id_hr_job_title">
                            @if(isset($jobs))
                                @foreach($jobs as $job)
                                    <option value="{{$job->id}}">{{$job->name}} [{{$job->code}}]</option>
                                @endforeach
                                @endIf
                        </select>
                    </div>

                    <!-- Note Field -->
                    <div class="form-group ">
                        <label for="note">Job Remark:</label>
                        <textarea class="form-control" name="job_note">

                    </textarea>

                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::button('<i class="glyphicon glyphicon-book"></i> Change',
                    ['type' => 'submit', 'class' => 'btn btn-default btn-purple', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>

    <div id="managerModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Assign Line Manager</h4>
                </div>
                {!! Form::open(['route' => ['lineManager.store'], 'method' => 'post']) !!}
                <div class="modal-body">
                    <div class="form-group ">
                        <input type="hidden" name="employee_id" value="{{$employee->id}}">

                        <label for="manager_id">Manager:</label>
                        <select name="manager_id" class="form-control" id="manager_id">
                            @if(isset($allManagers))
                                @foreach($allManagers as $allManager)
                                    <option value="{{$allManager->id}}"> {{$allManager->identification_number}}
                                        [{{$allManager->lastname.' '.$allManager->first_name}}]
                                    </option>
                                @endforeach
                                @endIf
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    {!! Form::button('<i class="glyphicon glyphicon-book"></i> Assign',
                    ['type' => 'submit', 'class' => 'btn btn-default btn-purple', 'onclick' => "return confirm('Are you sure ?')"]) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>

    <div id="educationModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Education</h4>
                </div>
                {!! Form::open(['route' => ['employeeEducations.store'], 'method' => 'post']) !!}
                <div class="modal-body">
                    <!-- Specialization Field -->
                    <div class="row">
                        <!-- Id Hr Institutions Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_hr_institutions', 'Institution Attended:') !!}
                            <select name="id_hr_institutions" class="form-control" id="id_hr_institutions">
                                @if(isset($institutions))
                                    @foreach($institutions as $institution)
                                        <option value="{{$institution->id}}"> {{$institution->name}} </option>
                                    @endforeach
                                    @endIf
                            </select>
                        </div>

                        <!-- Id Hr Institution Departments Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_hr_institution_departments', 'Department:') !!}
                            <select name="id_hr_institution_departments" class="form-control" id="id_hr_institutions">
                                @if(isset($institutionDepartments))
                                    @foreach($institutionDepartments as $institutionDepartment)
                                        <option value="{{$institutionDepartment->id}}"> {{$institutionDepartment->name}} </option>
                                    @endforeach
                                    @endIf
                            </select>
                        </div>

                        <!-- Grade Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('grade', 'Grade:') !!}
                            {!! Form::text('grade', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Mode Of Education Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('mode_of_education', 'Degree:') !!}
                            {!! Form::text('mode_of_education', null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group col-sm-6">
                            {!! Form::label('specialization', 'Specialization:') !!}
                            {!! Form::text('specialization', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Notes Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('notes', 'Remarks:') !!}
                            {!! Form::text('notes', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Startdate Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('startDate', 'Start Date:') !!}
                            {!! Form::date('startDate', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Enddate Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('endDate', 'End Date:') !!}
                            {!! Form::date('endDate', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Comments Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::label('comments', 'Extra Information:') !!}
                            {!! Form::text('comments', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Id Hr Employee Field -->
                        <div class="form-group col-sm-6">

                            <input type="hidden" name="employee_id" value="{{$employee->id}}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::button('<i class="glyphicon glyphicon-book"></i> Assign',
                    ['type' => 'submit', 'class' => 'btn btn-default btn-purple', 'onclick' => "return confirm('Are you sure ?')"]) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>

    <div id="experienceModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Experience</h4>
                </div>
                {!! Form::open(['route' => ['save_experience'], 'method' => 'post']) !!}
                <div class="modal-body">
                    <!-- Specialization Field -->
                    <div class="row">
                        <!-- Grade Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('organization_name', 'Company Name:') !!}
                            {!! Form::text('organization_name', null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group col-sm-12">
                            {!! Form::label('comments', 'Contact Information:') !!}
                            {!! Form::textarea('last_manager', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Mode Of Education Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('designation', 'Designation:') !!}
                            {!! Form::text('designation', null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group col-sm-6">
                            {!! Form::label('department', 'Department:') !!}
                            {!! Form::text('department', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Startdate Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('startDate', 'Start Date:') !!}
                            {!! Form::date('work_start_date', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Enddate Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('endDate', 'End Date:') !!}
                            {!! Form::date('work_end_date', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Id Hr Employee Field -->
                        <div class="form-group col-sm-6">
                            <input type="hidden" name="id_hr_employee" value="{{$employee->id}}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::button('<i class="glyphicon glyphicon-book"></i> Assign',
                    ['type' => 'submit', 'class' => 'btn btn-default btn-purple', 'onclick' => "return confirm('Are you sure ?')"]) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>

    <div id="refereeModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Referee</h4>
                </div>
                {!! Form::open(['route' => ['save_referee'], 'method' => 'post']) !!}
                <div class="modal-body">
                    <!-- Specialization Field -->
                    <div class="row">
                        <!-- Grade Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('firstname', 'First Name:') !!}
                            {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group col-sm-6">
                            {!! Form::label('lastname', 'Last Name:') !!}
                            {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Mode Of Education Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('address', 'Address:') !!}
                            {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group col-sm-6">
                            {!! Form::label('phone', 'Phone:') !!}
                            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Startdate Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('email', 'E-Mail:') !!}
                            {!! Form::text('email', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Enddate Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('relationship', 'Relationship:') !!}
                            {!! Form::text('relationship', null, ['class' => 'form-control', 'Placeholder' => 'e.g. Father, Brother, Wife']) !!}
                        </div>

                        <!-- Id Hr Employee Field -->
                        <div class="form-group col-sm-6">
                            <input type="hidden" name="id_hr_employee" value="{{$employee->id}}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::button('<i class="glyphicon glyphicon-book"></i> Assign',
                    ['type' => 'submit', 'class' => 'btn btn-default btn-purple', 'onclick' => "return confirm('Are you sure ?')"]) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>

    <div id="attachmentModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add File</h4>
                </div>
                {!! Form::open(['route' => ['save_attachment'], 'method' => 'post', 'files'=>'true']) !!}
                <div class="modal-body">
                    <!-- Specialization Field -->
                    <div class="row">
                        <!-- Grade Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('title', 'Title:') !!}
                            {!! Form::text('title', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Mode Of Education Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('address', 'Description:') !!}
                            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group col-sm-6">
                            {!! Form::label('phone', 'File:') !!}
                            {!! Form::file('attachment', null, ['class' => 'form-control', 'accept' => 'application/pdf']) !!}
                        </div>

                        <!-- Id Hr Employee Field -->
                        <div class="form-group col-sm-6">
                            <input type="hidden" name="id_hr_employee" value="{{$employee->id}}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::button('<i class="glyphicon glyphicon-book"></i> Assign',
                    ['type' => 'submit', 'class' => 'btn btn-default btn-purple', 'onclick' => "return confirm('Are you sure ?')"]) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>

    <div id="pixModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Picture</h4>
                </div>
                {!! Form::open(['route' => ['change_picture'], 'method' => 'post', 'files'=>'true']) !!}
                <div class="modal-body">
                    <!-- Specialization Field -->
                    <div class="row">
                        <!-- Grade Field -->

                        <div class="form-group col-sm-6">
                            {!! Form::file('picture', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Id Hr Employee Field -->
                        <div class="form-group col-sm-6">
                            <input type="hidden" name="employee_id" value="{{$employee->id}}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::button('<i class="glyphicon glyphicon-book"></i> Assign',
                    ['type' => 'submit', 'class' => 'btn btn-default btn-purple', 'onclick' => "return confirm('Are you sure ?')"]) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>

    <div id="bioModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Bio Data</h4>
                </div>
                {!! Form::open(['route' => ['update_bio'], 'method' => 'post', 'files'=>'true']) !!}
                <div class="modal-body">
                    <!-- Specialization Field -->
                    <div class="row">
                        @include('employee.fields')
                        <!-- Id Hr Employee Field -->
                        <div class="form-group col-sm-6">
                            <input type="hidden" name="employee_id" value="{{$employee->id}}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::button('<i class="glyphicon glyphicon-book"></i> Assign',
                    ['type' => 'submit', 'class' => 'btn btn-default btn-purple', 'onclick' => "return confirm('Are you sure ?')"]) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>

@endsection