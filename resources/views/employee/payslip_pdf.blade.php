<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
</head>
<body style=" height: 100%;">
<div style=" position: relative;">
    <div style="width: 100%; /*overflow: auto;*/ position: relative;height: auto; margin-top: -10px;">
        <span style="float: left !important;"><img src="" alt="COMPANIES-LOGO"></span>
        <center>
            <h2 style="color:#af85f8 !important;">{{ isset(auth()->user()->company->name) ? ucfirst(auth()->user()->company->name ." ENTERPRISE RESOURSE PLANNING PLATFORM EMPLOYEE PAY SLIP") : "ENTERPRISE RESOURSE PLANNING PLATFORM EMPLOYEE PAY SLIP"  }}</h2>
            {{ $payslip->employee->first_name }}, {{ $payslip->employee->lastname }}'s PAYSLIP<br>
            <span>Payslip for : {{ $payslip->year }}, {{ $payslip->month }}.</span> <br>
        </center>
        <br> <br>
        <h4>Benefit & Allowances</h4>
        <table class="table table-bordered table-hover">
            <thead class="thead-default">
            <tr>
                <th>Item |</th>
                <th>Amount |</th>
                <th>Amount to Date |</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Basic Salary: </td>
                <td> {{ number_format($payslip->salary, 2) }}</td>
                <td> {{ number_format($basic_to_date, 2) }}</td>
            </tr>
            <tr>
                <td>Transport Allowance: </td>
                <td> {{ number_format($payslip->transport, 2) }}</td>
                <td> {{ number_format($transport_to_date, 2) }}</td>
            </tr>
            <tr>
                <td>Leave Allowance: </td>
                <td> {{ number_format($payslip->leave, 2) }}</td>
                <td> {{ number_format($leave_to_date, 2) }}</td>
            </tr>
            <tr>
                <td>Housing Allowance: </td>
                <td> {{ number_format($payslip->housing, 2) }}</td>
                <td> {{ number_format($housing_to_date, 2) }}</td>
            </tr>
            <tr>
                <td>Meal Allowance: </td>
                <td> {{ number_format($payslip->meal, 2) }}</td>
                <td> {{ number_format($meal_to_date, 2) }}</td>
            </tr>
            <tr>
                <td>Entertainment Allowance: </td>
                <td> {{ number_format($payslip->entertainment, 2) }}</td>
                <td> {{ number_format($entertainment_to_date, 2) }}</td>
            </tr>
            <tr>
                <td>Other Allowances: </td>
                <td> {{ number_format($payslip->other_taxable_benefits, 2) }}</td>
                <td></td>
            </tr>
            <tr>
                <td><h5><b>Total Gross Payable (a)</b></h5></td>
                <td><h5>
                        <b> {{ number_format($payslip->total_gross_payable, 2) }}</b>
                    </h5></td>
                <td><h5><b> {{ number_format($grosspay_to_date, 2) }}</b></h5>
                </td>
            </tr>
            <tr>
                <td><h5><b>Less: Total Tax Reliefs</b></h5></td>
                <td><h5>
                        <b> {{ number_format($payslip->total_gross_payable - $payslip->taxable_pay, 2) }}</b>
                    </h5></td>
                <td><h5><b> {{ number_format($tax_reliefs_to_date, 2) }}</b></h5>
                </td>
            </tr>
            <tr>
                <td><h5><b>Total Taxable Pay</b></h5></td>
                <td><h5><b> {{ number_format($payslip->taxable_pay, 2) }}</b>
                    </h5></td>
                <td><h5><b> {{ number_format($taxable_pay_to_date, 2) }}</b></h5>
                </td>
            </tr>
            </tbody>
        </table>
        <h4>Deductions</h4>
        <table class="table table-bordered table-hover">
            <thead class="thead-default">
            <tr>
                <th>Item |</th>
                <th>Amount |</th>
                <th>Amount to Date |</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><b style="color: #d9534f">PAYE Tax: </b></td>
                <td><b> {{ number_format($payslip->tax, 2) }}</b></td>
                <td><b> {{ number_format($tax_to_date, 2) }}</b></td>
            </tr>
            <tr>
                <td><b style="color: #d9534f">Pension: </b></td>
                <td><b> {{ number_format($payslip->pension, 2) }}</b></td>
                <td><b> {{ number_format($pension_to_date, 2) }}</b></td>
            </tr>
            <tr>
                <td><b style="color: #d9534f">NHF Contribution: </b></td>
                <td><b> {{ number_format($payslip->nhf, 2) }}</b></td>
                <td><b> {{ number_format($nhf_to_date, 2) }}</b></td>
            </tr>
            <tr>
                <td><b style="color: #d9534f">Salary Advance / Loan: </b></td>
                <td><b> {{ number_format($payslip->salary_advance, 2) }}</b></td>
                <td><b> {{ number_format($salary_advance_to_date, 2) }}</b></td>
            </tr>
            <tr>
                <td><b style="color: #d9534f">Other Deductions: </b></td>
                <td><b> {{ number_format($payslip->other_deductions, 2) }}</b>
                </td>
                <td></td>
            </tr>
            <tr>
                <td><h5><b style="color: #d9534f">Total Deductions: </b></h5></td>
                <?php
                $t = $payslip->tax;
                $p = $payslip->pension;
                $n = $payslip->nhf;
                $od = $payslip->other_deductions;
                $sa = $payslip->salary_advance;
                $total = $t + $p + $n + $od + $sa;
                ?>
                <td><h5><b> {{ number_format($total, 2) }}</b></h5></td>
                <?php $total_deductions = $tax_to_date + $pension_to_date + $nhf_to_date + $salary_advance_to_date; ?>
                <td><h5> {{ number_format($total_deductions, 2) }} </h5></td>
            </tr>
            </tbody>
        </table>
        <h4>Net Pay</h4>
        <table class="table table-bordered table-hover">
            <thead class="thead-default">
            <tr>
                <th>Item |</th>
                <th>Amount |</th>
                <th>Amount to Date |</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><h3>Net Salary</h3></td>
                <td><h3> {{ number_format($payslip->net_pay, 2) }}</h3></td>
                <td><h3> {{ number_format($netpay_to_date, 2) }}</h3></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div style="
   position:fixed;
   float: right !important;
   bottom:0px;
   color:#af85f8;
   ">

    <b>Date Sent :</b> {{ date("M d, Y",strtotime(date("Y-m-d"))) }}
</div>
</body>
</html>
