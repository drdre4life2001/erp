@extends('erp.layouts.master')
  
  @section('title')
    Human Resource - Employee
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
      <section class="content-header">
         <h1>
            Employee
         </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Employee</li>
         </ol>
      </section>
      <section class="content">
         <div class="col-md-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Employee
                     </b></a>
                  </li>
               </ul>
               <div class="tab-content" style="padding: 2%">
                  <div class="tab-pane active" id="tab_1">
                        @if(isset($errors))
                            @if(count($errors) > 0)
                                <div class="alert alert-danger" style="color:white;">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}} here</li>
                                            @endForeach
                                    </ul>

                                </div>
                            @endIf
                        @endIf
                     <p align="right">
                        <a href="{{url('/employee')}}" class="btn btn-large btn-purple"><i class="fa fa-eye-open" aria-hidden="true" style="color:white"></i> View Employees
                        </a>
                     </p>
                     <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                        <div class="col-md-10">
                           {!! Form::open(['route' => 'employee.store']) !!}
                            @include('employee.create_field')
                           {!! Form::close() !!}
                        </div>
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                     </div>
                     <hr/ style="clear: both">

                         <table id="datable_1" class="table table-hover display  pb-30" >
                             <thead class="panel panel-heading">
                             <tr>
                                 <th>Employee</th>
                                 <th>Leave Type</th>
                                 <th>Start Date</th>
                                 <th>End Date</th>
                                 <th>No of Days</th>
                                 <th>Description</th>
                                 <th>Status</th>
                             </tr>
                             </thead> 
                             <tbody>
                             <?php foreach($leaves as $bonus){ ?>
                             <tr>
                                 <td>{{ $bonus->first_name }} {{ $bonus->lastname }}</td>
                                 <td>{{ $bonus->type }}</td>
                                 <td>{{ $bonus->start }}</td>
                                 <td>{{ $bonus->end }}</td>
                                 <td>{{ $bonus->days }}</td>
                                 <td>{{ $bonus->description }}</td>
                                 <td><a href="{{ url('delete_record/'.$bonus->id.'/leave') }}"><button onclick="return confirm('Are you sure? This process is not reversible!')">Delete</button></a></td>
                             </tr>
                             <?php } ?>
                             </tbody>
                         </table>
                  </div>
               </div>
               <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </section>
  @endsection

  @section('script')
       <script type="text/javascript">
           $("#example1").DataTable();
         @if ($errors->has('company_id') || $errors->has('bank') || $errors->has('description') || $errors->has('interest_rate') || $errors->has('amount') || $errors->has('period'))
           $('.adf').slideToggle();
         @endif

           $('.cadfxx').on('click', function(){
              $('.adf').slideToggle();
           });
       </script>
  @endsection

