<table class="table table-responsive" id="employees-table">
    <thead>
        <th>Identification Number</th>
        <th>Account No</th>
        <th>Date Of Birth</th>
        <th>Home Address</th>
        <th>First Name</th>
        <th>Lastname</th>
        <th>Other Name</th>
        <th>Passport No</th>
        <th>Picture</th>
        <th>City Of Birth</th>
        <th>Disability</th>
        <th>Permanent Address</th>
        <th>No Of Children</th>
        <th>Date First Hired</th>
        <th>Id Hr Work Addresses</th>
        <th>Id Sys Banks</th>
        <th>Gender</th>
        <th>Id Hr Grade</th>
        <th>Code Sys Country</th>
        <th>Created By</th>
        <th>Updated By</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($employees as $employee)
        <tr>
            <td>{!! $employee->identification_number !!}</td>
            <td>{!! $employee->account_no !!}</td>
            <td>{!! $employee->date_of_birth !!}</td>
            <td>{!! $employee->home_address !!}</td>
            <td>{!! $employee->first_name !!}</td>
            <td>{!! $employee->lastname !!}</td>
            <td>{!! $employee->other_name !!}</td>
            <td>{!! $employee->passport_no !!}</td>
            <td>{!! $employee->picture !!}</td>
            <td>{!! $employee->city_of_birth !!}</td>
            <td>{!! $employee->disability !!}</td>
            <td>{!! $employee->permanent_address !!}</td>
            <td>{!! $employee->no_of_children !!}</td>
            <td>{!! $employee->date_first_hired !!}</td>
            <td>{!! $employee->id_hr_work_addresses !!}</td>
            <td>{!! $employee->id_sys_banks !!}</td>
            <td>{!! $employee->gender !!}</td>
            <td>{!! $employee->id_hr_grade !!}</td>
            <td>{!! $employee->code_sys_country !!}</td>
            <td>{!! $employee->created_by !!}</td>
            <td>{!! $employee->updated_by !!}</td>
            <td>
                {!! Form::open(['route' => ['employees.destroy', $employee->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('employees.show', [$employee->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('employees.edit', [$employee->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>