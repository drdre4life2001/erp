@extends('layouts.erp')

@section('page_title')
@endsection
@section('error')
    @if(isset($errors))
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}} here</li>
                        @endForeach
                </ul>

            </div>
            @endIf
            @endIf
@endsection
@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Add Leave Type</h6>
                    </div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap mt-40">
                            <form action="{{ url('add_leave_type') }}" method="post">
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Leave Title</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-pencil"></i></div>
                                        <input type="text" class="form-control" id="exampleInputuname_1"
                                               name="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">No of Days Off</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                        <input type="text" class="form-control" id="exampleInputuname_1"
                                               name="days">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-15">
                                        <input class="btn btn-success btn-anim" type="submit" value="Add Leave Type">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection

@section('script')

@endsection
