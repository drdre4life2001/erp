@extends('layouts.app_settings')

@section('page_title')
<h2>Gender</h2>
@endsection

@section('settings_content')
    <section class="content-header">
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'gender.store']) !!}
                    @if(isset($errors))
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}} here</li>
                            @endForeach
                        </ul>
                        @endIf
                    </div>
                    @endIf
                        @include('gender.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
