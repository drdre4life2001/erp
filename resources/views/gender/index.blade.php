@extends('layouts.app_settings')

@section('page_title')
<h2>Gender</h2>
@endsection

@section('settings_content')
    <section class="content-header">
    </section>
    <div class="content">
        <div class="clearfix"></div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('gender.table')
            </div>
        </div>
    </div>
@endsection

