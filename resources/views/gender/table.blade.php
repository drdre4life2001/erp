<table class="table table-responsive" id="genders-table">
    <thead>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($genders as $gender)
        <tr>
            <td>{!! $gender->name !!}</td>
            <td>
                {!! Form::open(['route' => ['gender.destroy', $gender->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('gender.show', [$gender->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('gender.edit', [$gender->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>