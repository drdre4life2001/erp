@extends('layouts.app')

@section('content')
    <section class="content-header">
   </section>
   <div class="content">
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($gender, ['route' => ['genders.update', $gender->id], 'method' => 'patch']) !!}

                        @include('gender.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection