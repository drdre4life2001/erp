@extends('layouts.erp')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Expense Sheet Report</h6>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('finance/expense_pivot') }}"><h6 class="btn btn-primary">Expense Pivot</h6></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-wrap mt-40">
                                    <form action="{{ url('finance/expense_report') }}" method="post">
                                        <div class="form-group">
                                            <div class="form-group col-md-4">
                                                <label class="">From</label>
                                                <input name="start_date" id="dt1" type="text" placeholder="Select date" class="form-control" value="{{old('from')}}" required>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="">To</label>
                                                <input name="end_date" id="dt2" type="text" placeholder="Select date" class="form-control" value="{{old('to')}}" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-15">
                                                <input class="btn btn-success btn-anim" type="submit" value="Generate Expense Report">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                                @if(isset($result))
                                    @include('reports.elements.expense_results')
                                @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection