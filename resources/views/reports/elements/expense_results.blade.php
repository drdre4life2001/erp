<div class="card">
    <div class="card-content">
        <div class="row">
            <div class="col-md-12">
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table id="datable_1" class="table table-hover display  pb-30">
                            <thead class="panel panel-heading">
                            <tr>
                                <th>S/N</th>
                                <th>Narration</th>
                                <th>Category</th>
                                <th>Sub-Category</th>
                                <th>Day</th>
                                <th>Week Number</th>
                                <th>Month</th>
                                <th>Date</th>
                                <th>Expenses</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; foreach($result as $bonus){ ?>
                            <?php
                            $dayofweek = date('w', strtotime($bonus->created_at));
                            $day = date('Y-m-d', strtotime(($bonus->created_at - $dayofweek).' day', strtotime($bonus->created_at)));
                            $ddate = $bonus->created_at;
                            $date = new DateTime($ddate);
                            $week = $date->format("W");
                            $month = date_parse_from_format("Y-m-d", $ddate);
                            ?>
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $bonus->title }}</td>
                                <td>{{ $bonus->category }}</td>
                                <td>{{ $bonus->sub_category }}</td>
                                <td>{{ $month["day"]  }}</td>
                                <td>{{ $week }}</td>
                                <td>{{ $month["month"] }}</td>
                                <td>{{ $bonus->created_at }}</td>
                                <td>{{ number_format($bonus->total, 2) }}</td>
                            </tr>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>S/N</th>
                                <th>Narration</th>
                                <th>Category</th>
                                <th>Sub-Category</th>
                                <th>Day</th>
                                <th>Week Number</th>
                                <th>Month</th>
                                <th>Date</th>
                                <th>Expenses</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            {{--<div class="col-md-3">
                <div class="panel panel-default card-view panel-refresh">
                    <div class="refresh-container">
                        <div class="la-anim-1"></div>
                    </div>
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Analytics</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div id="container" style="min-width: 300px; height: 350px; max-width: 600px; margin: 0 auto"></div>
                        </div>
                    </div>
                </div>
            </div>--}}
        </div>
    </div>
</div>
@section('script')
    <script>
        $( function() {
            $("#dt1").datepicker({
                dateFormat: "yy-mm-dd",

                onSelect: function (date) {
                    var dt2 = $('#dt2');
                    var startDate = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('getDate');
                    dt2.datepicker('setDate', minDate);
                    startDate.setDate(startDate.getDate() + 60);
                    //sets dt2 maxDate to the last day of 30 days window
                    dt2.datepicker('option', 'maxDate', startDate);
                    dt2.datepicker('option', 'minDate', minDate);
                    $(this).datepicker('option', 'minDate', minDate);
                }
            });
            $('#dt2').datepicker(
                {dateFormat: "yy-mm-dd"}
            );
        } );
    </script>

@endsection
