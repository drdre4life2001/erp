<div class="table-wrap">
    <div class="table-responsive">
        <table id="datable_1" class="table table-hover display  pb-30" >
            <thead class="panel panel-heading">
            <tr>
                <th>S/N</th>
                <th>Employee Name</th>
                <th>Employee Contribution</th>
                <th>Employer Contribution</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=1; foreach($result as $bonus){ ?>
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $bonus->first_name }} {{ $bonus->lastname }}</td>
                <td>{{ number_format($bonus->pension, 2) }}</td>
                <td>{{ number_format($bonus->pension, 2) }}</td>
            </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <td></td>
                <td><h3>Total</h3></td>
                <td><h3>{{ number_format($total[0]->total * 2, 2) }}</h3></td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
