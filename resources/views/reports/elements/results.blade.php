<div class="table-wrap">
    <div class="table-responsive">
        <table id="example1" class="table table-hover display  pb-30" >
            <thead class="panel panel-heading">
            <tr>
                <th>S/N</th>
                <th>Employee Name</th>
                <th>Tax Payable</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=1; foreach($result as $bonus){ ?>
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $bonus->first_name }} {{ $bonus->lastname }}</td>
                <td>{{ number_format($bonus->tax, 2) }}</td>
            </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <td></td>
                <td><h3>Total</h3></td>
                <td><h3>{{ number_format($total[0]->total, 2) }}</h3></td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
