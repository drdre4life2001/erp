<div class="row">
    <div class="col-lg-12">
        <div class="table-wrap">
            <div class="table-responsive">
                <table id="example1" class="table table-hover display  pb-30">
                    <thead class="panel panel-heading">
                    <tr>
                        <th>S/N</th>
                        <th>Department</th>
                        <th>Tax Payable</th>
                        <th>Total Salary</th>
                        <th>Total Pension</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; foreach($result as $bonus){ ?>
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $bonus->department }}</td>
                        <td>{{ number_format($bonus->tax, 2) }}</td>
                        <td>{{ number_format($bonus->net_pay, 2) }}</td>
                        <td>{{ number_format($bonus->pension, 2) }}</td>
                    </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td><h3>Total</h3></td>
                        <td></td>
                        <td><h3>{{ number_format($total[0]->total, 2) }}</h3></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-default card-view panel-refresh">
            <div class="refresh-container">
                <div class="la-anim-1"></div>
            </div>
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Analytics</h6>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div id="container" style="min-width: 300px; height: 350px; max-width: 600px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('script')
    <script>
        Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Departmental Salaries'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'BLACK'
                        }
                    }
                }
            },
            series: [{
                name: 'Departments',
                colorByPoint: true,
                data: [
                        @foreach($depts as $dept)
                    {
                        'name': '{{$dept['name']}}',
                        'y': {{ $dept['y'] }}
                    },
                    @endforeach
                ]
            }]
        });
    </script>
@endsection
