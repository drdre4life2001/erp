<!-- Row -->
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
        <div class="panel panel-default card-view pt-0">
            <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                    <div class="sm-data-box bg-white">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                    <span class="txt-dark block counter"> &#x20A6;<span class="counter-anim">15,678</span></span>
                                    <span class="block"><span
                                                class="weight-500 uppercase-font txt-grey font-13">TOTAL EXPENSES ({{ date('Y') }}
                                            )</span><i
                                                class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default card-view pt-0">
            <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                    <div class="sm-data-box bg-white">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                    <span class="txt-dark block counter"> &#x20A6;<span class="counter-anim">15,678</span></span>
                                    <span class="block"><span
                                                class="weight-500 uppercase-font txt-grey font-13">TOTAL EXPENSES ({{ date('M') }}
                                            - {{ date('Y') }})</span><i
                                                class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
        <div class="panel panel-default card-view pt-0">
            <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                    <div class="sm-data-box bg-white">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                    <span class="txt-dark block counter">&#x20A6;<span class="counter-anim">15,678</span></span>
                                    <span class="block"><span
                                                class="weight-500 uppercase-font txt-grey font-13">RECIEVABLES ({{ date('Y') }}
                                            )</span><i
                                                class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default card-view pt-0">
            <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                    <div class="sm-data-box bg-white">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                    <span class="txt-dark block counter"> &#x20A6;<span class="counter-anim">15,678</span></span>
                                    <span class="block"><span
                                                class="weight-500 uppercase-font txt-grey font-13">TOTAL REVENUE ({{ date('Y') }}
                                            )</span><i
                                                class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
        <div class="panel panel-default card-view pt-0">
            <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                    <div class="sm-data-box bg-white">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                    <span class="txt-dark block counter"><span class="counter-anim">15,678</span></span>
                                    <span class="block"><span
                                                class="weight-500 uppercase-font txt-grey font-13">TOTAL EMPLOYEES</span>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default card-view pt-0">
            <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                    <div class="sm-data-box bg-white">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                    <span class="txt-dark block counter"><span class="counter-anim">15,678</span></span>
                                    <span class="block"><span
                                                class="weight-500 uppercase-font txt-grey font-13">TOTAL PROJECTS </span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
        <div class="panel panel-default card-view pt-0">
            <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                    <div class="sm-data-box bg-white">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                    <span class="txt-dark block counter" style="text-transform: capitalize">{{ $quarter }} Quarter</span>
                                    <span class="block"><span
                                                class="weight-500 uppercase-font txt-grey font-13">{{ date('Y') }}</span>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default card-view pt-0">
            <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                    <div class="sm-data-box bg-white">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                    <span class="txt-dark block counter"><span class="counter-anim">15,678</span></span>
                                    <span class="block"><span
                                                class="weight-500 uppercase-font txt-grey font-13">TOTAL PROJECTS </span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Row -->
<!-- Row -->
<div class="row">
    <div class="col-lg-6 col-xs-12">
        <div id="weather_1" class="panel panel-default card-view">
            <div class="panel panel-heading">
                <div class="panel panel-title"> Latest Expense Requests</div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="table-wrap">
                        <div class="table-responsive">
                            <table id="datable_1" class="table table-hover table-bordered display mb-30">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Category</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Tiger Nixon</td>
                                    <td>System Architect</td>
                                    <td>Edinburgh</td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td>Accountant</td>
                                    <td>Tokyo</td>
                                </tr>
                                <tr>
                                    <td>Ashton Cox</td>
                                    <td>Junior Technical Author</td>
                                    <td>San Francisco</td>
                                </tr>
                                <tr>
                                    <td>Cedric Kelly</td>
                                    <td>Senior Javascript Developer</td>
                                    <td>Edinburgh</td>
                                </tr>
                                <tr>
                                    <td>Airi Satou</td>
                                    <td>Accountant</td>
                                    <td>Tokyo</td>
                                </tr>
                                <tr>
                                    <td>Brielle Williamson</td>
                                    <td>Integration Specialist</td>
                                    <td>New York</td>
                                </tr>
                                <tr>
                                    <td>Herrod Chandler</td>
                                    <td>Sales Assistant</td>
                                    <td>San Francisco</td>
                                </tr>
                                <tr>
                                    <td>Rhona Davidson</td>
                                    <td>Integration Specialist</td>
                                    <td>Tokyo</td>
                                </tr>
                                <tr>
                                    <td>Colleen Hurst</td>
                                    <td>Javascript Developer</td>
                                    <td>San Francisco</td>
                                </tr>
                                <tr>
                                    <td>Donna Snider</td>
                                    <td>Customer Support</td>
                                    <td>New York</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-xs-12">
        <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
    </div>
    <!-- /Row -->
</div>