@extends('layouts.erp')

@section('page_title')
@endsection
@section('error')
    @if(isset($errors))
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}} here</li>
                        @endForeach
                </ul>

            </div>
            @endIf
            @endIf
@endsection
@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Add A New Project</h6>
                    </div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap mt-40">
                            <form action="{{ url('add_project') }}" method="post">
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Project Title</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-building"></i></div>
                                        <input type="text" class="form-control" id="exampleInputuname_1"
                                               placeholder="Project Title" name="name" value="{{ isset($project->name) ?  $project->name : ''}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Description</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-book"></i></div>
                                        <textarea class="form-control" name="description" required>
                                                {{ isset($project->description) ?  $project->description : ''}}
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Client</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <select class="form-control selectpicker" data-show-subtext="true"
                                                data-live-search="true" name="client_id" id="countryId" required>
                                            <?php $i = 0; ?>
                                            @foreach($clients as $client)
                                                <option data-subtext="{{ $client->name }}"
                                                        value="{{ $client->id }}">{{ $client->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Project Manager</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <select class="form-control selectpicker" data-show-subtext="true"
                                        data-live-search="true" name="manager" id="countryId" required>
                                            <?php $i = 0; ?>
                                            @foreach($employees as $employee)
                                                <option data-subtext="{{ $employee->first_name.', '.$employee->lastname }}"
                                                        value="{{ $employee->id }}">{{ $employee->first_name.', '.$employee->lastname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-15">
                                        <input class="btn btn-success btn-anim" type="submit" value="Add A New Project">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection
