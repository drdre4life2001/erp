@extends('layouts.erp')

@section('content')
    <div class="row">
        @if ( session()->has('message') )
            <div class="alert alert-danger alert-dismissable">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">All  Projects</h6>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_2" class="table table-hover display  pb-30" >
                                    <thead class="panel panel-heading">
                                    <tr>
                                        <th>Project Name</th>
                                        <th>Team Lead</th>
                                        <th>Project Type</th>
                                        <th>Issues</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>

                                        <?php
                                        if(isset($projects)){
                                        foreach($projects as $project){
                                        ?>
                                            <td>{{ $project['project'] }}</td>
                                            <td>{{ $project['lead'] }}</td>
                                            <td>{{ $project['type'] }}</td>

                                        <td>

                                            <form action="{{url('project/issues')}}" method="post">
                                                <input type="hidden" name="key" value="{{$project['key']}}">
                                                <button class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                            </form>

                                        </td>
                                    </tr>
                                    <?php } }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection