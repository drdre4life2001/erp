@extends('layouts.erp')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">All  Issues</h6>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-danger" href="{{url('projects')}}">Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_2" class="table table-hover display  pb-30" >
                                    <thead class="panel panel-heading">
                                    <tr>
                                        <th>Issue Type</th>
                                        <th>Description</th>
                                        <th>Summary</th>
                                        <th>Due date</th>
                                        <th>Created by</th>
                                        <th>Created on</th>
                                        <th>Status</th>
                                        <th>Progress Level</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>

                                        <?php foreach($issues as $issue){
                                            $created = date('d-m-Y', strtotime($issue->created));
                                        ?>

                                        <td>{{ $issue->issueType }}</td>
                                        <td>{{ $issue->description }}</td>
                                        <td>{{ $issue->summary }}</td>
                                            <td>{{ $issue->duedate }}</td>
                                            <td>{{ $issue->creator }}</td>
                                            <td>{{ $created }}</td>
                                            <td>{{ $issue->status }}</td>
                                            <td>{{ $issue->progress }}</td>

                                    </tr>
                                    <?php  } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection