@extends('layouts.erp')

@section('page_title')
@endsection

@section('content')

<div class="content">

    <div class="col-sm-12">

        <div class="card">
            <div class="card-header">
                <strong class="pull-left">Menu Groups</strong>
                    <span class="pull-right">
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('menuGroups.create') !!}">Add New</a></span>
            </div>

            <div class="card-block">
                @include('menu_groups.table')
            </div>
        </div>
    </div>

</div>
@endsection

