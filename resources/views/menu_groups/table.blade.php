<table class="table table-responsive" id="menuGroups-table">
    <thead>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($menuGroups as $menuGroup)
        <tr>
            <td>{!! $menuGroup->name !!}</td>
            <td>
                {!! Form::open(['route' => ['menuGroups.destroy', $menuGroup->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('menuGroups.show', [$menuGroup->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-eye"></i></a>
                    <a href="{!! route('menuGroups.edit', [$menuGroup->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>