<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Assign Code:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Tin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tin', 'TIN:') !!}
    {!! Form::text('tin', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Hr Work Addresses Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_hr_work_addresses', 'Location') !!}
    <select name="id_hr_work_addresses" class="form-control" >
        @foreach($addresses as $address)
        <option value="{{$address->id}}">{{$address->name}}</option>
        @endforeach

    </select>
    <p><a href="{{URL::to('workAddresses/create')}}"><i>Create Work address</i></a></p>
</div>

<!-- Parent organization -->
<div class="form-group col-sm-6">
    {!! Form::label('parent_id', 'Parent Organization (if any):') !!}
    <select name="parent_id" class="form-control" >
        <option value="0">Select Parent Organization..</option>
        @foreach($parentOrganizations as $organization)
        <option value="{{$organization->id}}">{{$organization->name}}</option>
        @endforeach

    </select>
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('organizations.index') !!}" class="btn btn-default">Cancel</a>
</div>
