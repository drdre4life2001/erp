<table class="table table-responsive" id="datable_2">
    <thead>
        <th>Name</th>
        <th>Code</th>
        <th>Description</th>
        <th>Tin</th>
        <th>Parent Organization</th>
        <th>Location</th>
        <th>Action</th>
    </thead>
    <tbody>
    @foreach($organizations as $organization)
        <tr>
            <td>{!! $organization->name !!}</td>
            <td>{!! $organization->code !!}</td>
            <td>{!! $organization->description !!}</td>
            <td>{!! $organization->tin !!}</td>
            <td></td>
            <td>@if(isset($organization->workAddress))
                {{$organization->workAddress->name}} [ {{$organization->workAddress->email}}, {{$organization->workAddress->mobile}} ]
            @endIf</td>
            <td>
                {!! Form::open(['route' => ['organizations.destroy', $organization->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {{--<a href="{!! route('organizations.show', [$organization->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>--}}
                    {{--<a href="{!! route('organizations.edit', [$organization->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>--}}
{{--                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}--}}
                    <a id="a_del" href="{{ url('/organitazion/destroy/'. $organization->id) }}" onclick="return confirm('Are you sure to trash'+ <?php echo $organization->name; ?> +'')" class="btn btn-xs btn-danger"  title="Trash Role"> <i class="fa fa-trash"></i></a>
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>