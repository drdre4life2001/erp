@extends('layouts.erp')

@section('page_title')
<h2>Create Subsidiary</h2>
@endsection

@section('content')
    <section class="content-header">
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'organizations.store']) !!}
                    @if(isset($errors))
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}} here</li>
                            @endForeach
                        </ul>
                    </div>
                    @endIf
                    @endIf
                        @include('organizations.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
