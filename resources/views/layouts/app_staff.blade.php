
@extends('layouts.app')

@section('links')

<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/')}}"><i class="icon-speedometer"></i> Dashboard <span class="badge badge-info">NEW</span></a>
</li>
@foreach(Session::get('menuGroups') as $menuGroup)
<li class="nav-title">
    {{$menuGroup['name']}}

</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-puzzle"></i>
        {{$menuGroup['name']}}
        <span class="caret"></span></a>


    <ul class="nav-dropdown-items">
        @foreach($menuGroup['menus'] as $menu)
        <li class="nav-link"><a class="nav-link" href="{{URL::to($menu['url'])}}"><i class="icon-puzzle"></i>
                {{$menu['name']}}</a></li>
        @endforeach

    </ul>

</li>
@endforeach
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1" style="">
            @yield('page_title')
            <hr />
                @yield('finance_content')
        </div>
    </div>
</div>
@endsection

