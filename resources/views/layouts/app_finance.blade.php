@extends('layouts.app')

@section('links')
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('home')}}"><i class="icon-speedometer"></i> Dashboard <span class="badge badge-info">NEW</span></a>
</li>

<li class="nav-title">
    Finance
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-puzzle"></i>Receivables & Payables
        <span class="caret"></span></a>
    <ul class="nav-dropdown-items">
        <li class="nav-link"><a class="nav-link" href="{{URL::to('payables/create')}}"><i class="icon-puzzle"></i> Make a request</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('payables')}}"><i class="icon-puzzle"></i> View all requests</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-puzzle"></i> Settings
        <span class="caret"></span></a>
    <ul class="nav-dropdown-items">
        <li class="nav-link"><a class="nav-link" href="{{URL::to('requestCategories')}}"><i class="icon-puzzle"></i> Request Category</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('requestItems')}}"><i class="icon-puzzle"></i> Request Items</a></li>
    </ul>
</li>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1" style="">
            @yield('page_title')
            <hr />
                @yield('finance_content')
        </div>
    </div>
</div>
@endsection

