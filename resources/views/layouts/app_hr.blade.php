@extends('layouts.app')

@section('links')
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('home')}}"><i class="icon-speedometer"></i> Dashboard <span class="badge badge-info">NEW</span></a>
</li>

<li class="nav-title">
    Human Resource
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-puzzle"></i> Employees
        <span class="caret"></span></a>
    <ul class="nav-dropdown-items">
        <li class="nav-link"><a class="nav-link" href="{{URL::to('employee/create')}}"> <i class="icon-puzzle"></i> Create</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('employee')}}"><i class="icon-puzzle"></i> View</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-puzzle"></i> Departments
        <span class="caret"></span></a>
    <ul class="nav-dropdown-items">
        <li class="nav-link"><a class="nav-link" href="{{URL::to('departments/create')}}"><i class="icon-puzzle"></i> Create</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('departments')}}"><i class="icon-puzzle"></i> View</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-puzzle"></i> Grades
        <span class="caret"></span></a>
    <ul class="nav-dropdown-items">
        <li class="nav-link"><a class="nav-link" href="{{URL::to('grades/create')}}"><i class="icon-puzzle"></i> Create</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('grades')}}"><i class="icon-puzzle"></i> View</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-puzzle"></i>Jobs
        <span class="caret"></span></a>
    <ul class="nav-dropdown-items">
        <li class="nav-link"><a class="nav-link" href="{{URL::to('jobs/create')}}"><i class="icon-puzzle"></i> Create</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('jobs')}}"><i class="icon-puzzle"></i> View</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-puzzle"></i> Institutions
        <span class="caret"></span></a>
    <ul class="nav-dropdown-items">
        <li class="nav-link"><a class="nav-link" href="{{URL::to('institutions/create')}}"><i class="icon-puzzle"></i> Create</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('institutions')}}"><i class="icon-puzzle"></i> View</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('institutionDepartments/create')}}"><i class="icon-puzzle"></i> Create Institution's Department</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('institutionDepartments')}}"><i class="icon-puzzle"></i> View Institution's Department</a></li>
    </ul>
</li>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 " style="">
            @yield('page_title')

                @yield('hr_content')
        </div>
    </div>
</div>
@endsection

