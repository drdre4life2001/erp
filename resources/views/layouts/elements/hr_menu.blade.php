<li class="navigation-header">
    <span>HR & Payroll</span>
    <i class="zmdi zmdi-more"></i>
</li>

<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><span class="right-nav-text">Employee Management</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="pages_dr" class="collapse collapse-level-1 two-col-list">
        <li>
            <a class="active" href="{{URL::to('departments')}}">Create Company/Department Units</a>
        </li>
        <li>
            <a class="active" href="{{URL::to('grades')}}">Create Level</a>
        </li>
        <li>
            <a class="active" href="{{URL::to('jobs')}}">Create Job-Role</a>
        </li>
        <li>
            <a class="active" href="{{ url('/employee/create') }}">Create Employee</a>
        </li>
        <li>
            <a class="active" href="{{ url('/employee') }}">View Employees</a>
        </li>
        <li>
            <a class="active" href="{{ url('/employee_loans') }}">Employee Loans</a>

        </li>
    </ul>
</li>

<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#payrol_drp"><div class="pull-left"><span class="right-nav-text">Payroll Management</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="payrol_drp" class="collapse collapse-level-1 two-col-list">
        <li>
            <a class="active" href="{{ url('/generate_payroll') }}">Payslip Generation</a>
        </li>
        <li>
            <a class="active" href="{{url('/send_payslip')}}">View/Download/Print Payslip</a>
        </li>
    </ul>
</li>

<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#leave_drp"><div class="pull-left"><span class="right-nav-text">Leave Management</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="leave_drp" class="collapse collapse-level-1 two-col-list">
        <li>
            <a class="active" href="{{ url('/add_leave_type') }}">Add Leave Type</a>
        </li>
        <li>
            <a class="active" href="{{ url('/employee_leaves') }}">View Leave Request</a>
        </li>
    </ul>
</li>

<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#att_drp"><div class="pull-left"><span class="right-nav-text">Attendance Management</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="att_drp" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#abs">Abscence Management<div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="abs" class="collapse collapse-level-2">
                <li>
                    <a href="{{URL::to('employee_absences')}}">List All</a>
                </li>
                <li>
                    <a href="{{URL::to('add_employee_absence')}}">Add Absence for an Employee</a>
                </li>
            </ul>
        </li>

        <li>
            <a class="active" href="{{ url('/') }}">Manage Public Holidays</a>
        </li>
    </ul>
</li>

<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#report_drp"><div class="pull-left"><span class="right-nav-text">Reports</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="report_drp" class="collapse collapse-level-1 two-col-list">
        <li>
            <a class="" href="{{ url('/department_report') }}">Department Report</a>
        </li>

        <li>
            <a class="" href="{{ url('/paye_report') }}">Paye Report</a>
        </li>

        <li>
            <a class="" href="{{ url('/pension_report') }}">Pension Report</a>
        </li>
    </ul>
</li>
