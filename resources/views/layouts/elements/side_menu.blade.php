<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li class="navigation-header">
            <span><h5><i class="fa fa-dashboard"></i> <a href="{{ url('/') }}">Dashboard</a></h5></span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <?php
        /**$links = Session::get('links'); **/
        use Illuminate\Support\Facades\DB;
        //dlld
        //ALl these shit, just to get the role_id of either the logged in user or employee
        $role_id = \App\Libraries\Utilities::getRoleId();
        $links = DB::select("select a.link as links from sys_menu_links as a, sys_modules as b, user_permissions as c
            where
            a.module_id = b.id and
            c.resource_id = a.module_id and
            c.role_id = $role_id
            ");
        ?>
        @if(isset($links))
            @foreach($links as $link)
                <li><hr class="light-grey-hr mb-10"/></li>
                @include("$link->links")
            @endforeach
        @endif
        <li class="navigation-header">
            <span>Leave Management</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#1ex_rq"><div class="pull-left"><span class="right-nav-text">Leave</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="1ex_rq" class="collapse collapse-level-1 two-col-list">
                <li>
                    <a href="{{URL::to('leave/request/create')}}">Apply for a leave Request</a>
                </li>
            </ul>
        </li>
        <li><hr class="light-grey-hr mb-10"/>
        </li>
    </ul>
</div>