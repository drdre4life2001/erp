<li class="navigation-header">
    <span>Procurement</span>
    <i class="zmdi zmdi-more"></i>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#pr_dr"><div class="pull-left"><span class="right-nav-text">Parameters</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="pr_dr" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{URL::to('requestCategories')}}">Categories</a>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#dropdown_dr_lv32">Vendors<div class="pull-right"></div><div class="clearfix"></div></a>
            <ul id="dropdown_dr_lv32" class="collapse collapse-level-2">
                <li>
                    <a href="{{URL::to('vendors')}}">List All Vendors</a>
                </li>
                <li>
                    <a href="{{URL::to('add_vendor')}}">Create A Vendor</a>
                </li>
            </ul>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#ex_rq"><div class="pull-left"><span class="right-nav-text">Procurements</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="ex_rq" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{URL::to('requests/create')}}">New Request</a>
        </li>
        <li>
            <a href="{{URL::to('requests/showRequests')}}">Requests</a>
        </li>
        <li>
            <a href="{{URL::to('requests/lineManagerRequests')}}">Pending Requests (Line Manager)</a>
            <a href="{{URL::to('requests/cLevelRequests')}}">Pending Requests (C-Level)</a>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#rp_pr"><div class="pull-left"><span class="right-nav-text">Procurement Reports</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="rp_pr" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{URL::to('payables/create')}}">Procurement Reports</a>
            <a href="{{URL::to('procurement/reports')}}">List Procurement Reports</a>  
        </li>
    </ul>
</li>