<li class="navigation-header">
    <span>Finance & Accounting</span>
    <i class="zmdi zmdi-more"></i>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr"><div class="pull-left"><span class="right-nav-text">Parameters</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="ui_dr" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{URL::to('requestCategories')}}">Revenue Categories</a>
        </li>

        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#dropdown_dr_lv22">Clients<div class="pull-right"></div><div class="clearfix"></div></a>
            <ul id="dropdown_dr_lv22" class="collapse collapse-level-2">
                <li>
                    <a href="{{URL::to('clients')}}">List All Clients</a>
                </li>
                <li>
                    <a href="{{URL::to('add_client')}}">Add A Client</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#dropdown_dr_lv50">Expenses<div class="pull-right"></div><div class="clearfix"></div></a>
            <ul id="dropdown_dr_lv50" class="collapse collapse-level-2">
                <li>
                    <a href="{{url('finance/operating_expenses')}}">Operating Expenses</a>
                </li>
                <li>
                    <a href="{{url('finance/capital_expenses')}}">Capital Expenses</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#dropdown_dr_lv51">Accounts<div class="pull-right"></div><div class="clearfix"></div></a>
            <ul id="dropdown_dr_lv51" class="collapse collapse-level-2">
                <li>
                    <a href="{{url('finance/banks')}}">Banks</a>
                </li>
                <li>
                    <a href="{{url('finance/bank_accounts')}}">Bank Accounts</a>
                </li>
                <li>
                    <a href="{{url('finance/bank-account/bulk-upload')}}">Bank Accounts Bulk Upload</a>
                </li>
                <li>
                    <a href="{{url('finance/account_header')}}">Account Headers</a>
                </li>
                <li>
                    <a href="{{url('finance/account_sub_header')}}">Account Sub Headers</a>
                </li>
            </ul>
        </li>

        <li>
            <a href="{{url('finance/salvage_items')}}">Item Savages</a>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#ex_dr"><div class="pull-left"><span class="right-nav-text">Revenue Streams</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="ex_dr" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{url('finance/add-revenue-stream')}}">Add Revenue Stream</a>
        </li>
        <li>
            <a href="{{ url('finance/revenue-streams') }}">All Revenue Streams</a>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#ex_dr_001"><div class="pull-left"><span class="right-nav-text">Remittances</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="ex_dr_001" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{url('finance/remittance')}}">PHEDC</a>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#ex_dr2"><div class="pull-left"><span class="right-nav-text">Revenue</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="ex_dr2" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{url('finance/add-revenue')}}">Add Revenue</a>
        </li>
        <li>
            <a href="{{ url('finance/revenue') }}">All Revenue</a>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#rp_dr2"><div class="pull-left"><span class="right-nav-text">Accounting Books</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="rp_dr2" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#dropdown_dr_lv30">Liquidity<div class="pull-right"></div><div class="clearfix"></div></a>
            <ul id="dropdown_dr_lv30" class="collapse collapse-level-2">
                <li>
                    <a href="{{URL::to('finance/expense_report')}}">Expense Sheet</a>
                </li>
                <li>
                    <a href="{{URL::to('finance/expense_pivot')}}">Summarized Expenses</a>
                </li>
            </ul>
        </li>
    </ul>
</li>

<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#disbursment"><div class="pull-left"><span class="right-nav-text">Disbursement</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="disbursment" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{URL::to('finance/disbursment/salaries')}}">Salaries</a>
            <a href="{{URL::to('finance/disbursement/procurement')}}">Procurement</a>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#loan"><div class="pull-left"><span class="right-nav-text">Loans</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="loan" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{URL::to('finance/company-loan/index')}}">Create Company Loan</a>
        </li>
        <li>
            <a href="{{URL::to('finance/company-loan/index#all_loans')}}">View Company Loans</a>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#tax"><div class="pull-left"><span class="right-nav-text">Tax Management</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="tax" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{URL::to('finance/tax/create')}}">Add Tax Type</a>
        </li>
        <li>
            <a href="{{URL::to('finance/tax')}}">List Tax Types</a>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#vendor"><div class="pull-left"><span class="right-nav-text">Vendor Management</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="vendor" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{URL::to('vendors')}}">List All Vendors</a>
        </li>
        <li>
            <a href="{{URL::to('add_vendor')}}">Create A Vendor</a>
        </li>
        <li>
            <a href="{{URL::to('finance/invoice')}}">All Invoice</a>
        </li>
        <li>
            <a href="{{URL::to('finance/invoice/create')}}">Add Invoice</a>
        </li>
    </ul>
</li>