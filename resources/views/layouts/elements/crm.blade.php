<li class="navigation-header">
    <span>CRM</span>
    <i class="zmdi zmdi-more"></i>
</li>

<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#ex_crm_1"><div class="pull-left"><span class="right-nav-text">Prospects</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="ex_crm_1" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{url('/crm/prospects/create')}}">Create Prospect</a>
        </li>
        <li>
            <a href="{{url('/crm/prospects')}}">List Prospects</a>
        </li>
    </ul>
</li>

<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#lead"><div class="pull-left"><span class="right-nav-text">Leads</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="lead" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{url('/lead/create')}}">Add New Lead</a>
        </li>
        <li>
            <a href="{{url('/lead/')}}">List All Leads</a>
        </li>
        <li>
            <a href="{{url('/lead/')}}">Update Leads</a>
        </li>
    </ul>
</li>

<li>
    <a href="{{url('crm/pipelines')}}"><div class="pull-left"><span class="right-nav-text">Pipelines</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#ex_crm_2"><div class="pull-left"> <span class="right-nav-text">Reports</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="ex_crm_2" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{url('crm/report/by-user')}}">Sales Activated (by User)</a>
        </li>
    </ul>
</li>

