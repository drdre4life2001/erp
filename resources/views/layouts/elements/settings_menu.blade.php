<li class="navigation-header">
    <span>System Settings</span>
    <i class="zmdi zmdi-more"></i>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><span class="right-nav-text">User Management</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="dashboard_dr" class="collapse collapse-level-1">
        <li>
            <a  href="{{URL::to('erp-users/index')}}">Create User</a>
        </li>
        <li>
            <a  href="{{URL::to('erp-users/show')}}">View All Users</a>
        </li>
        <li>
            <a href="{{URL::to('roles')}}">Roles</a>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_dr"><div class="pull-left"><span class="right-nav-text">Company Structure</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="ecom_dr" class="collapse collapse-level-1">
        <li>
            <a href="{{URL::to('organizations')}}">Company Subsidiaries</a>
        </li>
        <li>
            <a href="{{URL::to('organizations/create')}}">Add A Subsidiary</a>
        </li>
        <li>
            <a href="{{URL::to('workAddresses')}}">Company Locations</a>
        </li>
        <li>
            <a href="{{URL::to('workAddresses/create')}}">Add A Location</a>
        </li>
    </ul>
</li>