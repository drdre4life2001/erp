<li class="navigation-header">
    <span>Other Expenses</span>
    <i class="zmdi zmdi-more"></i>
</li>
<li>
    <a href="javascript:void(0);" data-toggle="collapse" data-target="#expense_ui_dr"><div class="pull-left"><span class="right-nav-text">Expenses</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
    <ul id="expense_ui_dr" class="collapse collapse-level-1 two-col-list">
        <li>
            <a href="{{URL::to('other-expenses/')}}">New Expense Request</a>
        </li>
        <li>
            <a href="{{URL::to('other-expenses/#all_expenses')}}">My Expenses</a>
        </li>
        <li>
            <a href="{{URL::to('other-expenses/approved-expenses')}}">Approved Expenses</a>
        </li>
        <li>
            <a href="{{URL::to('other-expenses/declined-expenses')}}">Declined Expenses</a>
        </li>

        @if(auth()->user()->role == 7)
            <li>
                <a href="{{URL::to('other-expenses/all')}}">All Expenses</a>
            </li>
        @endif
    </ul>
</li>
