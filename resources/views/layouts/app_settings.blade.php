@extends('layouts.app')

@section('links')
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('home')}}"><i class="icon-speedometer"></i> Dashboard <span class="badge badge-info">NEW</span></a>
</li>

<li class="nav-title">
    Settings
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#">  <i class="icon-puzzle"></i> Banks
        <span class="caret"></span></a>
    <ul class="nav-dropdown-items">
        <li class="nav-link"><a class="nav-link" href="{{URL::to('bank/create')}}"> <i class="icon-puzzle"></i> Create</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('bank')}}"> <i class="icon-puzzle"></i> View</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-puzzle"></i> Country
        <span class="caret"></span></a>
    <ul class="nav-dropdown-items">
        <li class="nav-link"><a class="nav-link" href="{{URL::to('country/create')}}"> <i class="icon-puzzle"></i> Create</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('country')}}"> <i class="icon-puzzle"></i> View</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-puzzle"></i> Gender
        <span class="caret"></span></a>
    <ul class="nav-dropdown-items">
        <li class="nav-link"><a class="nav-link" href="{{URL::to('gender/create')}}"> <i class="icon-puzzle"></i> Create</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('gender')}}"> <i class="icon-puzzle"></i> View</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-puzzle"></i> Organization
        <span class="caret"></span></a>
    <ul class="nav-dropdown-items">
        <li class="nav-link"><a class="nav-link" href="{{URL::to('organizations/create')}}"> <i class="icon-puzzle"></i> Create</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('organizations')}}"> <i class="icon-puzzle"></i> View</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('workAddresses/create')}}"> <i class="icon-puzzle"></i> Create Work Address</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('workAddresses')}}"> <i class="icon-puzzle"></i> View Work Address</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-puzzle"></i> User role & Access
        <span class="caret"></span></a>
    <ul class="nav-dropdown-items">
        <li class="nav-link"><a class="nav-link" href="{{URL::to('permissions')}}"> <i class="icon-puzzle"></i> Permissions</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('roles')}}"> <i class="icon-puzzle"></i> Access Roles</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('')}}"> <i class="icon-puzzle"></i> View</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-puzzle"></i> Menus
        <span class="caret"></span></a>
    <ul class="nav-dropdown-items">
        <li class="nav-link"><a class="nav-link" href="{{URL::to('menuGroups')}}"> <i class="icon-puzzle"></i> Menu Groups</a></li>
        <li class="nav-link"><a class="nav-link" href="{{URL::to('menus')}}"> <i class="icon-puzzle"></i> Menus</a></li>
    </ul>
</li>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12" style="">
            @yield('page_title')
                @yield('settings_content')
        </div>
    </div>
</div>
@endsection

