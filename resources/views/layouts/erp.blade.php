<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Taerp">
    <meta name="author" content="Method main">
    <meta name="keyword" content="Enterprise Resource Planning System">
    <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->

    <title>T.A ERP</title>
    <!-- Datepicker CSS -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <!-- Icons -->
    <link href="{{asset('assets/css/doodle/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/doodle/simple-line-icons.css')}}" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="{{asset('assets/css/doodle/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/bower_components/morris.js/morris.css')}}" rel="stylesheet">

    <!--Data table-->




    <!-- Chartist CSS -->
    <link href="{{asset('assets/vendors/bower_components/chartist/dist/chartist.min.css')}}" rel="stylesheet">

    <!-- Data table CSS -->
    <link href="{{asset('assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css')}}"
          rel="stylesheet">

    <link href="{{asset('assets/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css')}}"
          rel="stylesheet">
    <!-- bootstrap-select CSS -->
    <link href="{{asset('assets/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css')}}"
          rel="stylesheet">
    <!-- switchery CSS -->
    <link href="{{asset('assets/vendors/bower_components/switchery/dist/switchery.min.css')}}" rel="stylesheet">

    <!-- vector map CSS -->
    <link href="{{asset('assets/vendors/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet">

    <!-- Bootstrap Colorpicker CSS -->
    <link href="{{ asset('assets/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}"
          rel="stylesheet" type="text/css"/>

    <!-- select2 CSS -->
    <link href="{{ asset('assets/vendors/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet"
          type="text/css"/>

    <!-- bootstrap-tagsinput CSS -->
    <link href="{{ asset('assets/vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}"
          rel="stylesheet" type="text/css"/>

    <!-- bootstrap-touchspin CSS -->
    <link href="{{ asset('assets/vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}"
          rel="stylesheet" type="text/css"/>

    <!-- multi-select CSS -->
    <link href="{{ asset('assets/vendors/bower_components/multiselect/css/multi-select.css') }}" rel="stylesheet"
          type="text/css"/>

    <!-- Bootstrap Switches CSS -->
    <link href="{{ asset('assets/vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}"
          rel="stylesheet" type="text/css"/>


    <!-- Bootstrap Datetimepicker CSS -->
    <link href="{{ asset('assets/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/dt/dt-1.10.15/fc-3.2.2/fh-3.1.2/datatables.min.css"/>

    <!-- Custom CSS -->
    <link href="{{asset('assets/css/doodle/style.css')}}" rel="stylesheet">
    <style>
        .grafico {
            min-width: 310px;
            max-width: 400px;
            height: 280px;
            margin: 0 auto
        }

        .main-header {
            font-size: x-large;
            color: #888;
            font-family: Verdana;
            margin-bottom: 20px;
        }

        .destaque {
            color: #f88;
            font-weight: bolder;
        }

        .highcharts-tooltip h3 {
            margin: 0.3em 0;
        }

        /*Panel tabs*/
        .panel-tabs {
            position: relative;
            bottom: 30px;
            clear:both;
            border-bottom: 1px solid transparent;
        }

        .panel-tabs > li {
            float: left;
            margin-bottom: -1px;
        }

        .panel-tabs > li > a {
            margin-right: 2px;
            margin-top: 4px;
            line-height: .85;
            border: 1px solid transparent;
            border-radius: 4px 4px 0 0;
            color: #ffffff;
        }

        .panel-tabs > li > a:hover {
            border-color: transparent;
            color: #ffffff;
            background-color: transparent;
        }

        .panel-tabs > li.active > a,
        .panel-tabs > li.active > a:hover,
        .panel-tabs > li.active > a:focus {
            color: #fff;
            cursor: default;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border-radius: 2px;

    </style>

</head>

<body>
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<div class="wrapper theme-1-active box-layout pimary-color-red">
    @include('layouts.elements.top_menu')
    @include('layouts.elements.side_menu') 
    
    <div class="page-wrapper">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <div class="panel panel-body">
                    <span> @yield('error')</span> <br>
                    @if(Session::has('success'))
                        <center>
                            <span class="alert alert-success">{{ Session::has('success') ? Session::get('success') : "" }}</span>
                        </center>
                    @endif
                    @if(Session::has('error'))
                        <center>
                            <span class="alert alert-danger">{{ Session::has('error') ? Session::get('error') : "" }}</span>
                        </center>
                    @endif
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                    <div class="panel panel-body">
                        @yield('page_title')
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer container-fluid pl-30 pr-30">
            <div class="row">
                <div class="col-sm-12">
                    <p>{{ date('Y') }} &copy; TA-ERP. Pampered by Tech Team</p>
                </div>
            </div>
        </footer>
    </div>
</div>

<!-- GenesisUI main scripts -->

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="{{asset('assets/js/app.js')}}"></script>
<script src="{{asset('assets/js/dashboard.js')}}"></script>
<script src="{{asset('assets/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/vendors/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
<script src="{{asset('assets/vendors/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{asset('assets/js/doodle/vectormap-data.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/jQuery.print-master/jQuery.print.js')}}" />

<!-- Data table JavaScript -->

<script type="text/javascript"
        src="https://cdn.datatables.net/v/dt/dt-1.10.15/fc-3.2.2/fh-3.1.2/datatables.min.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/vendors/bower_components/Flot/excanvas.min.js')}}"></script>
<script src="{{asset('assets/vendors/bower_components/Flot/jquery.flot.js')}}"></script>
<script src="{{asset('assets/vendors/bower_components/Flot/jquery.flot.pie.js')}}"></script>
<script src="{{asset('assets/vendors/bower_components/Flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('assets/vendors/bower_components/Flot/jquery.flot.time.js')}}"></script>
<script src="{{asset('assets/vendors/bower_components/Flot/jquery.flot.stack.js')}}"></script>
<script src="{{asset('assets/vendors/bower_components/Flot/jquery.flot.crosshair.js')}}"></script>
<script src="{{asset('assets/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset('assets/js/doodle/flot-data.js')}}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{asset('assets/js/doodle/jquery.slimscroll.js')}}"></script>

<!-- simpleWeather JavaScript -->
<script src="{{asset('assets/vendors/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('assets/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js')}}"></script>
<script src="{{asset('assets/js/doodle/simpleweather-data.js')}}"></script>

<!-- Progressbar Animation JavaScript -->
<script src="{{asset('assets/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('assets/vendors/bower_components/jquery.counterup/jquery.counterup.min.js')}}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{asset('assets/js/doodle/dropdown-bootstrap-extended.js')}}"></script>

<!-- Sparkline JavaScript -->
<script src="{{asset('assets/vendors/jquery.sparkline/dist/jquery.sparkline.min.js')}}"></script>

<!-- Owl JavaScript -->
<script src="{{asset('assets/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- ChartJS JavaScript -->
<script src="{{asset('assets/vendors/chart.js/Chart.min.js')}}"></script>

<!-- Chartist JavaScript -->
<script src="{{asset('assets/vendors/bower_components/chartist/dist/chartist.min.js')}}"></script>

<!-- EasyPieChart JavaScript -->
<script src="{{asset('assets/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{asset('assets/vendors/bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('assets/vendors/bower_components/morris.js/morris.min.js')}}"></script>
<script src="{{asset('assets/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>

<!-- Switchery JavaScript -->
<script src="{{asset('assets/vendors/bower_components/switchery/dist/switchery.min.js')}}"></script>

<!-- Bootstrap Switch JavaScript -->
<script src="{{asset('assets/vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js')}}"></script>


<!-- Bootstrap Select JavaScript -->
<script src="{{asset('assets/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>

<!-- Select2 JavaScript -->
<script src="{{asset('assets/vendors/bower_components/select2/dist/js/select2.full.js')}}"></script>

<!-- Init JavaScript -->

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script src="{{asset('assets/js/doodle/init.js')}}"></script>
<script src="{{asset('assets/js/tableHeadFixer.js')}}"></script>
<script src="{{asset('assets/js/jquery.table2excel.js')}}"></script>
<script src="{{asset('assets/js/doodle/dashboard2-data.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>


<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>



<script >


    $(document).ready(function () {
        $('#institutions-table').DataTable( {
            dom: 'Bfrtip',
            buttons: [


                'excel', 'pdf', 'print'
            ]
        } );

        $('#datable_2').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                 'excel', 'pdf', 'print'
            ]


        } );

    });


</script>

<script type="text/javascript">
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
    $("a#a_del").click(function(){
        return ConfirmDelete();
    });

    //For FinanceController$bankBalance method basically a functionality for conditional dropdown select
    $('.bank_id').change(function(e) {
        var parent = e.target.value;
        $.get('/finance/bank/'+ parent, function(data) {
            $( ".bank_balance" ).html("");
           console.log(data);
            var SelectOption;
            $.each(data, function(index,value)
            {
                console.log(value.currentBalance);
                if( value.currentBalance > 0 ) {
                    $(".div_amount").show();
                    SelectOption +=  "<option value='"+ value.id   +"' >" + value.currentBalance + "</option>";
                }else{
                    SelectOption +=  "<option value='"+ value.id   +"' >" + 0.00 + "</option>";
                }
            });

            $( ".bank_balance" ).html(SelectOption);            //});

        });
    });


    //Account header
    $('.account_header_id').change(function(e) {
        var a_parent = e.target.value;
        $(".subheader_lists").show();
        $.get('/finance/account_header/'+ a_parent, function(data) {
            $( ".subheader_id" ).html("");
            console.log(data);
            var a_SelectOption ="<option  value=''>--Select Subheader--</option>";

            $.each(data, function(index,value)
            {
                a_SelectOption +=  "<option value='"+ value.header_id +"' >" + value.name + "</option>";
            });

            $( ".subheader_id" ).html(a_SelectOption);

        });
    });


</script>
@yield('script')

</body>

</html>