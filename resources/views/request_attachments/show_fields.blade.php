<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $requestAttachment->id !!}</p>
</div>

<!-- Id Fin Request Group Field -->
<div class="form-group">
    {!! Form::label('id_fin_request_group', 'Id Fin Request Group:') !!}
    <p>{!! $requestAttachment->id_fin_request_group !!}</p>
</div>

<!-- Size Field -->
<div class="form-group">
    {!! Form::label('size', 'Size:') !!}
    <p>{!! $requestAttachment->size !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $requestAttachment->description !!}</p>
</div>

<!-- Url Field -->
<div class="form-group">
    {!! Form::label('url', 'Url:') !!}
    <p>{!! $requestAttachment->url !!}</p>
</div>

