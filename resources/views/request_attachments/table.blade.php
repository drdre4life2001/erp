<table class="table table-responsive" id="requestAttachments-table">
    <thead>
        <th>Id Fin Request Group</th>
        <th>Size</th>
        <th>Description</th>
        <th>Url</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($requestAttachments as $requestAttachment)
        <tr>
            <td>{!! $requestAttachment->id_fin_request_group !!}</td>
            <td>{!! $requestAttachment->size !!}</td>
            <td>{!! $requestAttachment->description !!}</td>
            <td>{!! $requestAttachment->url !!}</td>
            <td>
                {!! Form::open(['route' => ['requestAttachments.destroy', $requestAttachment->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('requestAttachments.show', [$requestAttachment->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('requestAttachments.edit', [$requestAttachment->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>