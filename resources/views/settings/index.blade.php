@extends('layouts.app_settings')

@section('page_title')
<h2>Settings</h2>
@endsection

@section('hr_content')
<div class="row">
    <div class="col-sm-12">
        <p>
            Follow the links above any edit the system settings.
        </p>
    </div>

    <div class="form-group col-sm-12">
        <label for="note" class="">System Name:</label>
        <input class="form-control" placeholder=""  type="text">

    </div>

    <div class="form-group col-sm-12">
        <label for="note" class="">Year:</label>
        <input class="form-control" placeholder=""  type="text">

    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        <input class="btn btn-primary" type="submit" value="Save">
    </div>
</div>
@endsection

@section('script')
@endsection
