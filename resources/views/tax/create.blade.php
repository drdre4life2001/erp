@extends('layouts.erp')

@section('page_title')
    <h5>Add Tax Rate</h5>
@endsection

@section('content')
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                @if(isset($errors))
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endForeach
                            </ul>
                            @endIf
                        </div>
                    @endIf
                    {!! Form::open(['route' => 'finance.tax.store']) !!}

                    <!-- Name Field -->
                        <div class="form-group col-sm-6">
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Name" value="{{old('name')}}">
                            <label>Rate</label>
                            <input type="text" class="form-control" name="rate" placeholder="Enter rate" value="{{old('rate')}}">
                        </div>

                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('finance.tax.index') !!}" class="btn btn-default">Cancel</a>
                        </div>


                        {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
