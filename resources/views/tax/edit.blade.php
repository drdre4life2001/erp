@extends('layouts.erp')

@section('page_title')
    <h5>Edit Tax Rate</h5>
@endsection

@section('content')
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                @if(isset($errors))
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endForeach
                            </ul>
                            @endIf
                        </div>
                    @endIf
                    {{ Form::model($data, array('route' => array('finance.tax.update', $data->id), 'method' => 'PUT')) }}

                <!-- Name Field -->
                    <div class="form-group col-sm-6">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Name" value="{{$data->name}}" required>
                        <label>Rate</label>
                        <input type="text" class="form-control" name="rate" placeholder="Enter rate" value="{{$data->rate}}" required>
                    </div>

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('finance.tax.index') !!}" class="btn btn-default">Cancel</a>
                    </div>


                    {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
