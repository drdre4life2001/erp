@extends('layouts.erp')

@section('page_title')
    <div>
        <h2 class="pull-left">Tax Rates</h2>
        <h2 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('finance.tax.create') !!}">
                Add Tax Rate</a>
        </h2>
    </div>

    <div class="clearfix"></div>
@endsection

@section('content')
    <section class="content-header">

    </section>
    <div class="content">
        <div class="clearfix"></div>


        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <table class="table table-responsive" id="requests-table">
                    <thead>
                    <th>S/N</th>
                    <th>Name</th>
                    <th>Rate</th>
                    <th colspan="2">Action</th>
                    </thead>
                    <tbody>
                    <?php $i = 0; ?>
                    @foreach($items as $item)
                        <?php $i++; ?>
                        <tr>
                            <td>{!! $i !!}</td>
                            <td>{!! $item->name !!}</td>
                            <td>{!! $item->rate !!}%</td>
                            <td>
                                {!! Form::open(['route' => ['finance.tax.destroy', $item->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    <a href="{!! route('finance.tax.edit', [$item->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

