@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">{{ ucfirst('leads') }}</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        List of {{ ucfirst('leads') }}
    </div>

    <div class="panel-body">
        <div class="">
            <table class="table table-striped" id="thegrid">
              <thead>
                <tr>
                                        <th>Id</th>
                                        <th>Status</th>
                                        <th>Person Name</th>
                                        <th>Gender</th>
                                        <th>Organization Name</th>
                                        <th>Source</th>
                                        <th>Email Address</th>
                                        <th>Lead Owner</th>
                                        <th>Next Contact Date</th>
                                        <th>Next Contact By</th>
                                        <th>Phone</th>
                                        <th>Salutation</th>
                                        <th>Mobile No</th>
                                        <th>Fax</th>
                                        <th>Website</th>
                                        <th>Territory</th>
                                        <th>Lead Type</th>
                                        <th>Market Segment</th>
                                        <th>Industry</th>
                                        <th>Request Type</th>
                                        <th style="width:50px"></th>
                    <th style="width:50px"></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        <a href="{{url('leads/create')}}" class="btn btn-primary" role="button">Add lead</a>
    </div>
</div>




@endsection



@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "responsive": true,
                "ajax": "{{url('leads/grid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/leads') }}/'+row[0]+'">'+data+'</a>';
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/leads') }}/'+row[0]+'/edit" class="btn btn-default">Update</a>';
                        },
                        "targets": 20                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="#" onclick="return doDelete('+row[0]+')" class="btn btn-danger">Delete</a>';
                        },
                        "targets": 20+1
                    },
                ]
            });
        });
        function doDelete(id) {
            if(confirm('You really want to delete this record?')) {
               $.ajax({ url: '{{ url('/leads') }}/' + id, type: 'DELETE'}).success(function() {
                theGrid.ajax.reload();
               });
            }
            return false;
        }
    </script>
@endsection