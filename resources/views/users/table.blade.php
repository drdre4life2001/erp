<table class="table table-responsive" id="datable_2">
    <thead>
        <th>S/N</th>
        <th>Email</th>
        <th>Role</th>
        <th>Action</th>
        <th>Update Status</th>
    </thead>
    <tbody>
    <?php $num = 1; ?>
    @foreach($users as $user)
        <tr @if(!is_null($user->deleted_at)) title="User deactivated activate user to update profile" @endif >
            <td>{!! $num++ !!}</td>
            <td>{!! $user->email !!}</td>
            <td>{{ isset($user->role_name) ? $user->role_name : 'No role assigned to this user' }}</td>
            <td>
                <div class='btn-group'>
                    
                    <a data-toggle="modal" @if(is_null($user->deleted_at))) data-target="#edit{{$user->id}}" @endif class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>

                    <!-- {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!} -->
                </div>
            </td>
            <td>
                @if(is_null($user->deleted_at))
                    <a id="a_del" href="{{ url('/erp-users/destroy/'. $user->id) }}" onclick="return confirm('Are you sure to trash'+ <?php echo $user->email; ?> +'')" class="btn btn-xs btn-danger"  title="Deactivate {{$user->email}}"> Deactivate</a>
                @else
                    <a href="{{ url('/erp-users/activate/'. $user->id) }}" onclick="return confirm('Are you sure to activate'+ <?php echo $user->email; ?> +'')" class="btn btn-xs btn-success"  title="Activate {{$user->email}}"> Activate</a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>