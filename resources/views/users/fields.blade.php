<div class="col-md-8 col-md-offset-2">
    <div class="row">
        <form method="POST" action=" {{ url('erp-users/index') }} ">
            <!-- Email Field -->
            <div class="row">
                <div class="col-lg-6 ">
                    <label for="">Email: </label>
                    
                    <input type="email" placeholder="mail address here" required name="email" id="input" class="form-control" value="" >
                    @if ($errors->has('email'))<p class="help-block" style="color: red">{{ $errors->first('email') }}</p> @endif <br> 
                    
                </div>
                <div class=" col-lg-6">
                    <label>Select User Role</label>
                    @if(isset($roles))
                        <select name="user_role" class="form-control">
                            @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                            @endforeach
                        </select>
                    @endIf
                </div>             
            </div>
            <center>
                <input type="submit" value="Add User" class="btn btn-success"/>
            </center> <br> <br> 
        </form>    
    </div>    
</div>
