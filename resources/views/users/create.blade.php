@extends('layouts.erp')

@section('page_title')
@endsection

@section('content')

<div class="content">
    <section class="content-header">
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    @if(isset($errors))
                    @if(count($errors) > 0)
                    <div class="col-sm-12 alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endForeach
                        </ul>

                    </div>
                    @endIf
                    @endIf

                    @if(isset($message))
                    <div class="col-sm-12 alert alert-success">
                        <ul>
                            <li>{{$message}}</li>
                        </ul>
                    </div>
                    @endIf

                    <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>
                               <center>
                                    Invite User
                               </center>
                            </h4>
                        </div> <br> <br>

                        <div class="card-block">

                            @include('users.fields')

                        </div>
                    </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
