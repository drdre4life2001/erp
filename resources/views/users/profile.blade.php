@extends('layouts.erp')
@section('content')
    <div class="col-lg-11 col-lg-offset-1">
        <div class="clearfix"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-10" style="background-color: white;">
                    <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <center>
                            <h2>Profile Details</h2> <br> <br>
                        </center>


                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="firstName" class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" required id="firstName" readonly="" name="email" value="{{ auth()->user()->email }}" placeholder="Email Address" class="form-control" autofocus>
                                        @if ($errors->has('email')) <p class="help-block" style="color: red">{{ $errors->first('email') }}</p> @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="firstName" class="col-sm-3 control-label">Current Role</label>
                                    <div class="col-sm-9">
                                        <select name="" id="" class="form-control" disabled>
                                            <option value="" >{{ ucfirst(auth()->user()->assignedRole->name) }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="firstName" class="col-sm-3 control-label">Password <span style="color:red;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="password" required id="firstName" name="password" placeholder="" class="form-control" autofocus>
                                        @if ($errors->has('password')) <p class="help-block" style="color: red">{{ $errors->first('password') }}</p> @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="firstName" class="col-sm-3 control-label">Confirm Password <span style="color:red;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="password" required id="firstName" name="confirm_password" placeholder="" class="form-control" autofocus>
                                        @if ($errors->has('confirm_password')) <p class="help-block" style="color: red">{{ $errors->first('confirm_password') }}</p> @endif
                                    </div>
                                </div>
                            </div>
                            <center>
                                <input type="submit" value="Update" class="btn btn-success"> <br> <br>
                            </center>
                        </div>
                    </form>
                </div>
                <div class="col-md-4" style="margin-top: 60px;">
                </div>
            </div>
        </div>
        </section>
    </div>
    </div>
@endsection

