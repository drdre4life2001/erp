@extends('layouts.erp')

@section('page_title')
@endsection

@section('content')

<div class="col-lg-11 col-lg-offset-1">
    <section class="content-header">
        <h1 class="pull-left">Users</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! url('/erp-users/index') !!}">Add New</a>
        </h1>
    </section>
    <div class="clearfix"></div>


    <div class="content">
        <div class="content">
            <div class="clearfix"></div>

            <div class="clearfix"></div>
            <div class="box box-primary">
                <div class="box-body">
                        @include('users.table')
                </div>
            </div>
        </div>
    </div>    
</div>

<!-- modal for editing user -->
    @foreach($users as $user)
        <div class="modal fade" id="edit{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <center>
                    <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                </center>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @if(Session::has('error'))
                {{ Session::get('error') }}
              @endif
              <form method="POST" action="{{url('/erp-users/update', $user->id)}}">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="recipient-name" class="form-control-label">User mail:</label>
                      <input type="text" name="user_email" value="{{ $user->email }}" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                      <!-- <label for="message-text" class="form-control-label">User role:</label> -->
                      <select class="form-control" name="role">
                        <option selected>Update user role</option>
                          <?php $db_selected_value = $user->role; ?>
                        @foreach($roles as $role)
                            <option value="{{ $role->id }}" @if($db_selected_value == $role->id ) selected @endif>{{ $role->name }}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="modal-footer">
                  <input type="submit" value="Update" class="btn btn-primary" />
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>    
              </form>
            </div>
          </div>
        </div>
    @endforeach
@endsection

