@extends('layouts.erp')

@section('page_title')
    <h4>Procurement Reports</h4><br>
@endsection

@section('content')
     @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endForeach
            </ul>
        </div>
    @endIf
    <div class="row">
        <div class="col-sm-12">
            
           <table class='table table-bordered' id='reports'>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Request Title</th>
                        <th>Request Category</th>
                        <th>Requested By</th>   
                    </tr>
                </thead>
                <tbody>
                       @if(isset($requests))
                                @foreach($requests as $request)
                                <?php $date = date('d-m-Y', strtotime($request->created)); ?>
                    <tr>
                            <!--Loop through all items for this procurement -->
                                    <td>{{$date}}</td>
                                    <td>{{ $request->title }}</td>
                                    <td>{{ $request->category }}</td>
                                    <td>{{ $request->lastname}} {{$request->firstname}}</td>
                    </tr>
                    
                    @endforeach
                    @endif
                </tbody>

            </table>
                       
        </div>
    </div>
@endsection

@section('script')
    <script>
        //$(function(){
            
            $('#reports').DataTable({
                searching: true,
                "pagingType": "full_numbers",
                dom: 'Bfrtip'          
            });
        //});
        

        
    </script>
    @endSection