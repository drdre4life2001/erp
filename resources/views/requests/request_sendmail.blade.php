@extends('layouts.payslip')

@section('content')
   
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Dear {{ isset($details->first_name) ? $details->first_nam : null  }}, {{ isset($details->lastname) ? $details->lastname : null }}</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <!-- Section:Biography -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-block text-xs-left">
                                    <h3 class="card-title" style="color:#009688"><i
                                                class="fa fa-calendar fa-fw"></i> </h3>
                                    <p>
                                       
                                        Please find attached a new procurement request.<br>
                                        Please review and then login to edit, approve or decline.
                                        
                                    </p>
                                    <p>
                                        Best Regards, Tech Advance.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  

@endsection