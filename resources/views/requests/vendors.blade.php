@extends('layouts.erp')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Vendors</h6>
                    </div>
                    <div class="pull-right">
                        <h6 class="btn btn-primary"><a href="{{ url('add_vendor') }}">Add Vendor</a></h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_1" class="table table-hover display  pb-30" >
                                    <thead class="panel panel-heading">
                                    <tr>
                                        <th>Vendor Name</th>
                                        <th>Phone</th>
                                        <th>Description</th>
                                        <th>Address</th>
                                        <th>TIN</th>
                                        <th>Date Added</th>
                                        <th>Date Updated</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($vendors as $bonus){ ?>
                                    <tr>
                                        <td>{{ $bonus->name }}</td>
                                        <td>{{ $bonus->phone  }}</td>
                                        <td>{{ $bonus->description }}</td>
                                        <td>{{ $bonus->address }}</td>
                                        <td>{{ $bonus->tin }}</td>
                                        <td>{{ $bonus->created_at }}</td>
                                        <td>{{ $bonus->updated_at }}</td>
                                        <td>
                                            <a href="{{ url('delete_record/'.$bonus->id.'/vendor') }}"><button onclick="return confirm('Are you sure? This process is not reversible!')" class="btn btn-danger"><i class="fa fa-trash"></i></button></a>
                                            <a href="{{ url('update_vendor/'.$bonus->id) }}"><button onclick="return confirm('Are you sure? This process is not reversible!')" class="btn btn-primary"><i class="fa fa-pencil"></i></button></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection