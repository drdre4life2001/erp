<table class="table table-responsive" id="requests-table">
    <thead>
        <th>Request Item</th>
        <th>Qty</th>
        <th>Unit Price</th>
        <th>Total Price</th>
        <th>Info</th>
        <th>Requested By</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($requests as $requests)
        <tr>
            <td>{!! $requests->item->name !!}</td>
            <td>{!! $requests->qty !!}</td>
            <td>{!! $requests->unit_price !!}</td>
            <td>{!! $requests->total_price !!}</td>
            <td>{!! $requests->info !!}</td>
            <td>{!! $requests->created_by !!}</td>
            <td>
                {!! Form::open(['route' => ['requests.destroy', $requests->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('requests.show', [$requests->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('requests.edit', [$requests->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>