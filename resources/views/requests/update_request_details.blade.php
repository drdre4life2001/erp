@extends('layouts.erp')

@section('page_title')
    <h4>Update Request Details</h4>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-8">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form action="{{ url('request_update') }}" method="post">
            @if(isset($errors))
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endForeach
                        </ul>
                    </div>
                    @endIf
                    @endIf
                   
                    @foreach($details as $detail)
                    <div class="col-md-12">
                        <div class="form-group col-sm-12">
                            <div class="row">
                                <input type="hidden" value="{{$detail->salvage_id}}" name="salvage_id">
                                <div class="form-group col-sm-6">
                                    <label class="">Title</label>
                                    <input name="title" id="title" type="text" class="form-control" value="{{$detail->title}}" readonly="true"/>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="">Description</label>
                                    <textarea name="description" class="form-control">{{$detail->description}}</textarea>
                                </div>
                                <div class="form-group col-sm-3">
                                    <label class="">Unit Price</label>
                                    <input name="price" id="price" type="number" value="{{$detail->price}}" class="form-control"/>
                                </div>
                                <div class="form-group col-sm-3">
                                    <label class="">Quantity</label>
                                    <input name="quantity" id="quantity" value="{{$detail->quantity}}" type="number" class="form-control"/>
                                </div>
                                <div class="form-group col-sm-3">
                                    <label class="">Total</label>
                                    <input name="total" id="total" type="number" value="{{$detail->total}}" class="form-control"/>
                                    <input type="hidden" name="item_id" value="{{$detail->id}}">
                                    <input type="hidden" name="request_id" value="{{$request_id}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <input type="submit" value="Update" class="btn btn-sm btn-primary"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                    
                    
                    </form>
        </div>
    </div>
    
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $('#price').keyup(calculate);
            $('#quantity').keyup(calculate);
        });
        function calculate(e)
        {
            $('#total').val($('#price').val() * $('#quantity').val());
        }
    </script>
@endsection