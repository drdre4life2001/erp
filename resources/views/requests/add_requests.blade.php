@extends('layouts.erp')

@section('page_title')
    <h4>Add Procurement items for: </h4><br>
    <h5> 
        @if(isset($detail))
            @foreach($detail as $details)
                {{ $details->title }}
                @endforeach
        @endif 
    </h5>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-8">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
                <form action="{{ url('save_requests') }}" method="post">
                        @if(isset($errors))
                            @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endForeach
                                    </ul>
                                </div>
                                @endIf
                        @endIf
                       
                        <script>
                            var categories = <?php //echo json_encode(session('categories'));  ?> ;
                            var items = <?php //echo json_encode(session('items'));  ?> ;
                        </script>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label class="">Salvages</label>
                                    <select name="salvage" class="form-control">
                                        <option value="">Please select..</option>
                                        @foreach($salvages as $salvage)
                                        <option value="{{$salvage->id}}">{{$salvage->name}} [{{number_format($salvage->value)}}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-sm-2">
                                    <label class="">Title</label>
                                    <input name="title" id="title" type="text" class="form-control" value="{{old('title')}}"/>
                                </div>
                                <div class="form-group col-sm-2">
                                    <label class="">Description</label>
                                    <textarea name="description" class="form-control">{{old('description')}}</textarea>
                                </div>
                                <div class="form-group col-sm-2">
                                    <label class="">Unit Price</label>
                                    <input name="price" id="price" type="number" value="{{old('price')}}" class="form-control"/>
                                </div>
                                <div class="form-group col-sm-2">
                                    <label class="">Quantity</label>
                                    <input name="quantity" id="quantity" value="{{old('quantity')}}" type="number" class="form-control"/>
                                </div>
                                <div class="form-group col-sm-2">
                                    <label class="">Total</label>
                                    <input name="total" id="total" value="{{old('total')}}" type="number" class="form-control"/>
                                </div>
                                @if(isset($detail))
                                    @foreach($detail as $details)
                                <div class="form-group col-sm-3">
                                    <input name="request_id" id="request_id" type="hidden" class="form-control" value="{{ $details->id }}"/>
                                </div>
                                    @endforeach
                                @endif 
                            </div>
                        </div>
                        
                        <div class="form-group col-sm-12">
                            <button class ="btn btn-sm btn-danger"><i class="fa fa-plus"> Add Item</i></button>
                            @if(count($item) > 0) <!--Only show the Done button if an item haas been added -->
                                @if(isset($detail))
                                    @foreach($detail as $details)
                                   
                                     <a class ="btn btn-sm btn-success" href="{{url('/request/send/'.$details->id)}}"><i class="fa fa-check"> Done</i></a>
                                    @endforeach
                                @endif 
                            @endif
                        </div>
                        <!--
                        
                        <div class="form-group col-sm-12">
                            <input type="submit" value="Submit" class="btn btn-sm btn-primary"/>
                        </div>
                        -->
                        
                        </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
           <table class='table table-bordered' id='saveRequests'>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Unit Price</th>  
                        <th>Quantity</th>  
                        <th>Total</th>
                        <th colspan="2">Action</th>  
                                   
                    </tr>
                </thead>
                <tbody>
                       @if(isset($item))
                                @foreach($item as $items)
                    <tr>
                            <!--Loop through all items for this procurement -->
                            
                          
                                    <td>{{ $items->title }}</td>
                                    <td>{{ $items->description }}</td>
                                    <td>{{ number_format($items->price) }}</td>
                                    <td>{{ $items->quantity }}</td>
                                    <td>{{ number_format($items->total) }}</td>
                                    <td>
                                        <div class='btn-group' style="display:block">
                                            <!--Edit item -->
                                            <a href="" style="margin-right: 0px;" 
                                               class='btn btn-default btn-xs' data-toggle='modal' data-target='#{{$items->id}}'>
                                                <i class="glyphicon glyphicon-eye-open"> </i> Edit</a>
                                                <!--delete this item -->
                                           
                                            <a href="" style="margin-right: 0px;" 
                                               class='btn btn-danger btn-xs' data-toggle="modal" data-target="#{{$items->id.'_'}}">
                                                <i class="glyphicon glyphicons-delete"> </i> Delete</a>
                                               
                                        </div>
                                      
                                    </td>
                                    <!-- Delete Modal -->
                                    <div class="modal fade" id="{{$items->id.'_'}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form action = "{{url('request/items/delete')}}" method="post">  
                                                        <div class="modal-body">
                                                            Are you sure you want to delete this item?
                                                            <input type="hidden" name="item_id" value="{{$items->id}}">
                                                            @if(isset($detail))
                                                                @foreach($detail as $details)
                                                                <input name="request_id" id="request_id" type="hidden" class="form-control" value="{{ $details->id }}"/>
                                                                @endforeach
                                                            @endif 
                                                        </div>
                                                        <div class="modal-footer">
                                                            
                                                            <a class="btn btn-default btn-ok" data-dismiss="modal">No</a>
                                                            <input type="submit" value="Yes"  class="btn btn-danger">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                    </div>
                                    <!--Modal ends -->
                                    <!-- Modal -->
                                    <div id="{{$items->id}}" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Edit this Item</h4>
                                          </div>
                                          <form action="{{url('request/items/edit')}}" method="post">
                                          <div class="modal-body">
                                                    <input type="hidden" name="item_id" value="{{$items->id}}">
                                                    <div class="form-group col-sm-6">
                                                        <label class="">Title</label>
                                                        <input name="title" id="title" type="text" class="form-control" value="{{$items->title}}" required/>
                                                    </div>
                                                  <div class="form-group col-sm-6">
                                                      <label class="">Salvages</label>
                                                      <select name="salvage" class="form-control" required>
                                                          <option value="">Please select..</option>
                                                          @foreach($salvages as $salvage)
                                                          <option value="{{$salvage->id}}">{{$salvage->name}} [{{number_format($salvage->value)}}]</option>
                                                          @endforeach
                                                      </select>
                                                  </div>
                                                    <div class="form-group col-sm-12">
                                                        <label class="">Description</label>
                                                        <textarea name="description" class="form-control" required>{{$items->description}}</textarea>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label class="">Unit Price</label>
                                                        <input name="price" id="price2" type="number" class="form-control" value="{{ $items->price }}" required/>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label class="">Quantity</label>
                                                        <input name="quantity" id="quantity2" type="number" class="form-control" value="{{$items->quantity}}" required/>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label class="">Total</label>
                                                        <input name="total" id="total2" type="number" class="form-control" value="{{ $items->total }}" required/>
                                                    </div>
                                                    @if(isset($detail))
                                                        @foreach($detail as $details)
                                                    <div class="form-group col-sm-12">
                                                        <input name="request_id" id="request_id" type="hidden" class="form-control" value="{{ $details->id }}"/>
                                                    </div>
                                                        @endforeach
                                                    @endif 
                                                
                                          </div>
                                          <div class="modal-footer">
                                            <input type="submit" value="submit" name="submit" class="btn btn-danger">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                          </form>
                                            @section('script')
                                            <script>
                                                $(document).ready(function(){
                                                    $('#price2').keyup(calculate);
                                                    $('#quantity2').keyup(calculate);
                                                });
                                                function calculate(e)
                                                {
                                                    $('#total2').val($('#price2').val() * $('#quantity2').val());
                                                }
                                                $('#confirm-delete').on('show.bs.modal', function(e) {
                                                    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
                                                });
                                            </script>
                                            @endsection
                                        </div>
                                      </div>
                                    </div> 
                                    <!--Modal ends -->   
                                    
                    </tr>
                     @endforeach
                            @endif
                </tbody>         
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#price').keyup(calculate);
            $('#quantity').keyup(calculate);
        });
        function calculate(e)
        {
            $('#total').val($('#price').val() * $('#quantity').val());
        }


        $('#saveRequests').DataTable({
                searching: false,
                "pagingType": "full_numbers",
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel','print'
                ]
        });

        //console.log(categories);
        var result = {};
        var items = {}
        var selectedCategory = "";
        //loop through the categories
        for (var i = 0; i < categories.length; i++) {
            //save the categories and categories items object
            result[categories[i].name] = categories[i].items;
            //loop through the items in the category
            for (var j = 0; j < categories[i].items.length; j++) {
                //save the items data; [items id => items obj]
                items[categories[i].items[j].id] = categories[i].items[j];
            }


        }

        //console.log(result);
        //console.log(items);

        $('select[name="category"]').change(function () { // When category select changes

            //set the unit price to default
            $('#unit_price').val('');
            $('.description').replaceWith("<div class='description col-sm-12'><p><b></b><p></div>");
            //var p = document.querySelector('.other_product_name');
            //p.innerHTML = "";
            //$('#other_product_item').fadeOut();

            /**if ($(this).val() == "Others"){
                var productfield = document.createElement('input');
                productfield.setAttribute('type', 'text');
                productfield.setAttribute('name', 'name');
                productfield.setAttribute('placeholder', "Product Eg: Dry Yam");
                productfield.classList.add('form-control');


            }else{*/
            selectedCategory = $(this).val();
            var options = '<option>Choose one!</option>';
            $.each(result[$(this).val()] || [], function (i, v) { // Cycle through each associated items
                options += '<option value="' + v.id + '">' + v.name + '</option>';
            });
            //options += '<option>' + "Other" + '</option>';
            var itemtfield = document.createElement('select');
            itemtfield.setAttribute('name', 'item');
            itemtfield.classList.add('form-control');
            itemtfield.innerHTML = options;
            /**} */

            var p = document.querySelector('.item_name');
            p.innerHTML = "";
            p.appendChild(itemtfield);
            $('#category_item').fadeIn();
            setItemListener();
//            $('select[name="product"]').html(options); // And update the role options
        });

        function setItemListener() {
            $('select[name="item"]').change(function () {

                $(this).attr('name', 'item');
                //auto-set the unit price, qty, other information
                //gets its information
                var itemObj = items[$(this).val()];
                $('#unit_price').val(itemObj.unit_price);
                $('.description').replaceWith("<div class='description col-sm-12'><p><i>" + itemObj.description + "</i><p></div>");
                //console.log(itemObj);
                //console.log();
            })
        }
    </script>
    @endSection