@extends('layouts.payslip')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="">
                            
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel panel-heading">
                                <h5>TECHADVANCE REQUEST FOR QUOTE (RFQ)</h5>
                            </div>
                            <div class="panel panel-body">
                                <div class="panel panel-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card card-block">
                                                <table class="table table-bordered table-hover">
                                                    <!--
                                                    <thead class="thead-default">
                                                    <tr>
                                                        <th>Title</th>
                                                        <th>Description</th>
                                                        <th>Price</th>
                                                        <th>Quantity</th>
                                                        <th>Total</th>
                                                    </tr>
                                                    </thead>
                                                    -->
                                                    <tbody>
                                                        <tr>
                                                            <td><h5>Name of Agency</h5></td>
                                                            <td><h5>TechAdvance Limited</h5></td>
                                                        </tr>
                                                        @foreach($request as $requests)
                                                        <tr>
                                                            <td><h5>Agency file reference</h5></td>
                                                            <td><h5>TA{{$requests->rfq_id}}</h5></td>
                                                        </tr>
                                                        <tr>
                                                            <td><h5>RFQ Number</h5></td>
                                                            <td><h5>TA{{$requests->item_id}}</h5></td>
                                                        </tr>
                                                        <tr>
                                                            <td><h5>Date of RFQ</h5></td>
                                                            <td><h5>
                                                            <?php
                                                            $time = strtotime($requests->request_date);
                                                            $myFormatForView = date("d/m/y", $time);
                                                            echo $myFormatForView;
                                                            ?>
                                                            </h5></td>
                                                        </tr>
                                                        <tr>
                                                            <td><h5>Agency Project Officer (Procurement)</h5></td>
                                                            <td><h5>{{$requests->lastname}} {{$requests->first_name}}</h5></td>
                                                        </tr>
                                                        <tr>
                                                            <td><h5>Required Response Time For Quote</h5></td>
                                                            <td><h5>
                                                            <?php
                                                            $response = strtotime($requests->response_date);
                                                            $responseFormat = date("d/m/y", $response);
                                                            echo $responseFormat;
                                                            ?>
                                                            </h5></td>
                                                        </tr>
                                                        <tr>
                                                            <td><h5>RFQ Description</h5></td>
                                                            <td><h5>All items shall be properly marked and duly inspected.</h5></td>
                                                        </tr>
                                                        <tr>
                                                            <td><h5>Shipment Type or Mode</h5></td>
                                                            <td><h5>AIR/SEA/LAND FREIGHT</h5></td>
                                                        </tr>
                                                        <tr>
                                                            <td><h5>Freight Charge Notification</h5></td>
                                                            <td><h5></h5></td>
                                                        </tr>
                                                        @endforeach
                                                        
                                                        
                                                    </tbody>
                                                </table>
                                                <p>ITEM DETAILS</p>
                                                <table class="table table-bordered table-hover">
                                              
                                                    <thead class="thead-default">
                                                        <tr>
                                                            <th>Title</th>
                                                            <th>Description</th>
                                                            <th>Unit Price</th>
                                                            <th>Quantity</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($item as $items)
                                                        <tr>
                                                            <td>{{$items->title}}</td>
                                                            <td>{{$items->description}}</td>
                                                            <td>{{NUMBER_FORMAT($items->price)}}</td>
                                                            <td>{{$items->quantity}}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                <table class="table table-bordered table-hover">
                                                    <tbody>
                                                        <tr>
                                                            <td>Confidential Information</td>
                                                            <td>
                                                            Include details in table below or insert Not Applicable Information on confidentiality provisions is available at: http://www.finance.gov.au/procurement/procurement-policy-and-guidance/buying/contract-issues/confidentiality-procurement- cycle/principles.html
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                         
                                                                <td></td>
                                                                <td>Agency  Confidential information</td>
                                                                <td>Period of Confidentiality</td>
                                                
                                                        </tr>
                                                        <tr>
                                                            
                                                    
                                                                <td></td>
                                                                <td>Agency data</td>
                                                                <td>Indefinitely</td>
                                                            
                                                        </tr>
                                                        <tr>
                                                           
                                                                <td></td>
                                                                <td>Any Personal Information held by </td>
                                                                <td>Indefinitely</td>
                                                          
                                                        </tr>
                                                        <tr>
                                                           
                                                                <td></td>
                                                                <td>Security Classified Information</td>
                                                                <td>Indefinitely</td>
                                                            
                                                        </tr>
                                                        <tr>
                                                          
                                                                <td></td>
                                                                <td>The Service Provider Confidential information</td>
                                                                <td>Period of Confidentiality</td>
                                                           
                                                        </tr>
                                                        <tr>
                                                        
                                                                <td></td>
                                                                <td>Information on the Service Providers  performance</td>
                                                                <td>Indefinitely</td>
                                                         
                                                        </tr>
                                                        <tr>
                                                       
                                                                <td></td>
                                                                <td>List Rates (excluding the total value of the Contract)</td>
                                                                <td>Indefinitely</td>
                                                      
                                                        </tr>
                                                        <tr>
                                                            <td>Additional Security requirements</td>
                                                            <td>
                                                            State any additional security requirements to the requirements contained in the Default Terms and Conditions, or that apply to particular aspects of work (e.g. Protected security clearance required) or insert Not Applicable>  
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Additional Insurance Requirements</td>
                                                            <td>
                                                            Insert additional requirements (if any) for relevant insurance e.g. Professional indemnity insurance for an insured amount of [$ insert amount] per occurrence and not less than [$ insert amount] in aggregate; Public liability insurance for an insured amount of [$ insert amount] per occurrence and not less than [$ insert amount] in aggregate; and Workers compensation insurance as required by law or insert Not Applicable. If Not Applicable is specified, the insurance requirements under the Deed will apply.>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                           <tr>
                                                            <td>Other Requirements</td>
                                                            <td>
                                                            Proposed Personnel performing the Legal Services may be required to sign a Deed and acknowledgements relating to confidentiality, security, moral rights, intellectual property and other relevant matters as required by the Agency. Any Contract will be conditional on this occurring. Insert any other relevant details, if any, for example
                                                            - Special conditions
                                                            -Reporting
                                                            -Value Add Services
                                                            -Performance  and evaluation
                                                            - particular requirements concerning activities that must be undertaken jointly with other existing Agency contractors>

                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection