@extends('layouts.erp')

@section('page_title')
    <h4>Request Details</h4><br>

@endsection

@section('content')
    
    <div class="row">
        <div class="col-sm-12">

            @if(session('status'))
            <div class="alert alert-danger">
                <ul>
                    <li>{{session('status')}}</li>
                </ul>
            </div>
            @endif
           <table class='table table-bordered' id='saveRequests'>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Quantity</th>  
                        <th>Total</th> 
                        <th>Line Manager Remark</th> 
                        <th>C-Level Remark</th>
                        <th colspan="1">Action</th>
                                      
                    </tr>
                </thead>
                <tbody>
                       @if(isset($items))
                                @foreach($items as $item)
                    <tr>
                            <!--Loop through all items for this procurement -->
                            
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->description }}</td>
                                    <td>{{ number_format($item->price) }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ number_format($item->total) }}</td>
                                    <td>{{ $item->line_manager_status }}</td>
                                    <td>{{ $item->c_level_status }}</td>
                                    <td>
                                       
                                        <div class='btn-group'>
                                            <?php
                                                //$disburse = \App\Libraries\Utilities::disburseStatus($item->id);
                                                //$rfqStatus = \App\Libraries\Utilities::rfqStatus($item->id);
                                                $getUserStatus = \App\Libraries\Utilities::getUserStatus($item->id);

                                                //var_dump($getUserStatus);exit;
                                                if($getUserStatus == 'procurement'){
                                                    if($item->rfq_status == 2){
                                                        echo "<a class='btn btn-default btn-success btn-xs'>Funds Disbursed</a>";
                                                    }else if($item->rfq_status == 1){
                                                        echo "<a class='btn btn-default btn-success btn-xs'>RFQ Sent</a>";
                                                    }else if(($item->line_manager_status_id == 2) && ($item->c_level_status_id == 4) && ($item->rfq_status != 1) && ($item->rfq_status != 2) ){
                                                        echo "<a class='btn btn-default btn-primary btn-xs bootstrap-modal-form-open' data-toggle='modal' data-target='#{{$item->id}}'>Generate RFQ</a>";
                                                    }else{
                                                        echo "<a class='btn btn-default btn-danger btn-xs'>Not Approved</a>";
                                                    }
                                                }else if($getUserStatus == 'finance'){ //Finance/CLevel or other staff with acess to finance module
                                                    if($item->rfq_status == 2){
                                                        echo "<a class='btn btn-default btn-success btn-xs'>Funds Disbursed</a>";
                                                    }else if($item->rfq_status == 1){
                                                        ?>
                                                    <a class='btn btn-default btn-success btn-xs' href="{{url('finance/disburse/'.$item->id)}}">Disburse Funds</a>
                                                    <?php
                                                    }else if(($item->line_manager_status_id == 2) && ($item->c_level_status_id == 4) && ($item->rfq_status != 1) && ($item->rfq_status != 2) ){
                                                        echo "<a class='btn btn-default btn-primary btn-xs bootstrap-modal-form-open'>RFQ Not Sent</a>";
                                                    }else{
                                                        echo "<a class='btn btn-default btn-danger btn-xs'>Not Approved</a>";
                                                    }
                                                }else if($getUserStatus == 'others'){
                                                    if($item->rfq_status == 2){
                                                        echo "<a class='btn btn-default btn-success btn-xs'>Funds Disbursed</a>";
                                                    }else if($item->rfq_status == 1){
                                                        echo "<a class='btn btn-default btn-success btn-xs'>RFQ Sent</a>";
                                                    }else if(($item->line_manager_status_id == 2) && ($item->c_level_status_id == 4) && ($item->rfq_status != 1) && ($item->rfq_status != 2) ){
                                                        echo "<a class='btn btn-default btn-primary btn-xs bootstrap-modal-form-open'>No Acess</a>";
                                                    }else{
                                                        echo "<a class='btn btn-default btn-danger btn-xs'>Not Approved</a>";
                                                    }
                                                }
                                            ?>
                                                    {{--
                                                    @if($disburse)
                                                        <a class='btn btn-default btn-success btn-xs' href="{{url('finance/disburse/'.$item->id)}}">Disburse Funds</a>
                                                    @elseif($rfqStatus)
                                                            <a class='btn btn-default btn-success btn-xs'>RFQ Sent</a>
                                                            @elseif(($item->line_manager_status_id == 2) && ($item->c_level_status_id == 4) && ($item->rfq_status != 1))
                                                <a class='btn btn-default btn-primary btn-xs bootstrap-modal-form-open' data-toggle="modal" data-target="#{{$item->id}}">Generate RFQ</a>
                                                @else
                                                <a href="" class='btn btn-default btn-danger btn-xs'>Not Approved</a>
                                                @endif
                                                --}}
                                        </div>
                                    </td>
                                    <div class="modal fade" id="{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title"></h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <form action = "{{url('requests/rfq/send')}}" method="post" class="bootstrap-modal-form"> 
                                                        <input type="hidden" name="item_id" value="{{$item->id}}"> 
                                                        @if(isset($vendors))
                                                        <div class="form-group col-sm-12">
                                                                <label class="">Select Vendor</label>
                                                                <select name="vendor_id" class="form-control" id="vendor"> 
                                                                <option value="">Select...</option>
                                                                @foreach($vendors as $vendor)
                                                                <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                                                @endforeach
                                                                </select> 
                                                        </div> 
                                                        @endif                                        
                                                        <div class="form-group col-sm-12">
                                                            <label class="">Required Response Time</label>
                                                            <input name="response_date" id="datepicker" type="text" class="form-control"/>
                                                        </div>
                                                        <div class="form-group col-sm-12">
                                                            <label class="">RFQ Send Options</label><br>
                                                            <input type="radio" name="send_option" value="1" class=""> Send mail to vendor
                                                            <input type="radio" name="send_option" value="2"checked="checked" > Generate Excel sheet for vendor
                                                        </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            
                                                            <a class="btn btn-default btn-ok" data-dismiss="modal">Cancel</a>
                                                            <input type="submit" value="submit"  class="btn btn-danger">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                    </div>
                                   
                                    </tr>
                                    @section('script')
                                        <script type="text/javascript">
                                            
                                            $(document).ready(function() {
                                                //Initiate Datepicker
                                                $( function() {
                                                    $( "#datepicker" ).datepicker({
                                                        minDate : 0
                                                        });
                                                });
                                                //Validate form using JS
                                                $('.bootstrap-modal-form').on('submit',function(event){
                                                    var response_time = $('#datepicker').val();
                                                    var vendor = $('#vendor').val();
                                                    
                                                    if (response_time == '') {
                                                        alert("A response time is needed");
                                                        event.preventDefault();
                                                    }else if(vendor == '' ){
                                                        alert("A vendor is needed");
                                                        //alert(response_time);
                                                        event.preventDefault();
                                                    }else{}
                                                    
                                                });

                                            });  
                                    </script>
                                    @endsection
                                   

                     @endforeach
                            @endif
                   
                </tbody>         
            </table>
        </div>
    </div>
@endsection

@section('script')

    <script>
        
        $('#showRequests').DataTable({
                searching: false,
                "pagingType": "full_numbers",
                dom: 'Bfrtip'          
        });

        
    </script>
    @endSection