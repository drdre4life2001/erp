@extends('layouts.erp')

@section('page_title')
    <h4>Pending Procurement C-LEVEL Requests</h4>
@endsection

@section('content')
  
    <div class="row">
        <div class="col-sm-12">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
           <table class='table table-bordered' id='saveRequests'>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Request By</th>  
                        <th colspan="1">Action</th> 
                                      
                    </tr>
                </thead>
                <tbody>
                       @if(isset($details))
                                @foreach($details as $detail)
                    <tr>
                            <!--Loop through all items for this procurement -->
                            
                                    <td>{{ $detail->createdAt }}</td>
                                    <td>{{ $detail->title }}</td>
                                    <td>{{ $detail->category }}</td>
                                    <td>{{ $detail->lastname }} {{ $detail->firstname }} </td>
                                    <td>
                                        <div class='btn-group'>
                                          
                                            <a href="{{url('/cLevelRequests/details/'.$detail->request_id)}}"
                                               class='btn btn-default btn-primary btn-xs'><i
                                                        class="glyphicon glyphicon-eye-open"></i> View</a>
                                            <!--
                                            <a href="" data-toggle="modal" data-target="#{{$detail->request_id}}"
                                               class='btn btn-default btn-xs'>
                                                <i class="glyphicon glyphicon-trash"></i></a>
                                            -->
                                        </div>
                                      
                                    </td>
                                    <!--
                                    <div class="modal fade" id="{{$detail->request_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form action = "{{url('request/delete')}}" method="post">  
                                                        <div class="modal-body">
                                                            Are you sure you want to delete this request?
                                                            <input type="hidden" name="request_id" value="{{$detail->request_id}}">
                                                        </div>
                                                        <div class="modal-footer">
                                                            
                                                            <a class="btn btn-default btn-ok" data-dismiss="modal">Cancel</a>
                                                            <input type="submit" value="Delete"  class="btn btn-danger">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                    </div>
                                    -->
                    </tr>
                      
                     @endforeach
                     
                            @endif
                </tbody>         
           </table>
        </div>
    </div>
@endsection

@section('script')
    <!--
    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
        $('#showRequests').DataTable({
                searching: false,
                "pagingType": "full_numbers",
                dom: 'Bfrtip'          
        });

        
    </script>
    -->
    @endSection