@extends('layouts.app_finance')

@section('page_title')
<div>
<h2 class="pull-left">Requests</h2>
<h2 class="pull-right">
    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('requests.create') !!}">
        Make A Request</a>
</h2>
</div>

<div class="clearfix"></div>
@endsection

@section('finance_content')
    <section class="content-header">

    </section>
    <div class="content">
        <div class="clearfix"></div>


        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('requests.table')
            </div>
        </div>
    </div>
@endsection

