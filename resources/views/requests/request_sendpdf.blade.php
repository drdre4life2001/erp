@extends('layouts.payslip')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="">
                            @foreach($request as $requests)
                                <h4> Posted on: {{$requests->createdAt}} </h4><br>
                                <h4> Title: {{$requests->title}}</h4><br>
                                <h4> Category: {{$requests->category }}</h4><br>
                                <h4> Requested By: {{ $requests->lastname }} {{ $requests->firstname }} </h4><br>
                            @endforeach
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel panel-heading">
                                <h3>Items for this request</h3>
                            </div>
                            <div class="panel panel-body">
                                <div class="panel panel-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card card-block">
                                                <table class="table table-bordered table-hover">
                                                    <thead class="thead-default">
                                                    <tr>
                                                        <th>Title</th>
                                                        <th>Description</th>
                                                        <th>Price</th>
                                                        <th>Quantity</th>
                                                        <th>Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        @foreach($items as $item)
                                                        <td><h3>{{$item->title}}</h3></td>
                                                        <td><h3>{{$item->description}}</h3></td>
                                                        <td><h3>{{number_format($item->price)}}</h3></td>
                                                        <td><h3>{{$item->quantity}}</h3></td>
                                                        <td><h3>{{number_format($item->total)}}</h3></td>
                                                        @endforeach
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection