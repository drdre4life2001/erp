@extends('layouts.erp')

@section('page_title')
@endsection
@section('error')
    @if(isset($errors))
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}} here</li>
                        @endForeach
                </ul>

            </div>
            @endIf
            @endIf
@endsection
@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Update Vendor Details</h6>
                    </div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap mt-40">
                            <form action="{{ url('update_vendor') }}" method="post">
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Vendor's Name</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-building"></i></div>
                                        <input type="text" class="form-control" id="exampleInputuname_1"
                                               value="{{ $client->name }}" name="name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Vendor's Tax Identification Number</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-building"></i></div>
                                        <input type="text" class="form-control" id="exampleInputuname_1"
                                               value="{{ $client->tin }}" name="tin">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Description</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-book"></i></div>
                                        <textarea class="form-control" name="description" required>
                                            {{ $client->description }}
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Vendor's Phone
                                        Number</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               name="phone" value="{{ $client->phone }}" required>
                                        <input type="hidden" class="form-control" id="exampleInputuname_1"
                                               name="vendor_id" value="{{ $client->id }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Address</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-book"></i></div>
                                        <textarea class="form-control" name="address"  required>
                                                {{ $client->address }}
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Country</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <select class="form-control countries" data-show-subtext="true"
                                                data-live-search="true" name="country" id="countryId" required>
                                                <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">State</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <select class="form-control states" data-show-subtext="true"
                                                data-live-search="true" name="state" id="stateId" required>
                                            <option value="">Select State</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">State</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <select class="form-control cities" data-show-subtext="true"
                                                data-live-search="true" name="state" id="cityId" required>
                                            <option value="">Select City</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-15">
                                        <input class="btn btn-success btn-anim" type="submit" value="Update Vendor Details">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection

@section('script')
    <script src="{{asset('assets/js/location.js')}}"></script>
@endsection
