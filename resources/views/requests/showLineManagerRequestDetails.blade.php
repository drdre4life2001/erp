@extends('layouts.erp')

@section('page_title')
    <h4>Request Details</h4><br>
@endsection

@section('content')
     @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endForeach
            </ul>
        </div>
    @endIf
    <div class="row">
        <div class="col-sm-12">
            
           <table class='table table-bordered' id='saveRequests'>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Quantity</th>  
                        <th>Total</th> 
                        <th colspan="3">Action</th> 
                                      
                    </tr>
                </thead>
                <tbody>
                       @if(isset($items))
                                @foreach($items as $item)
                               
                    <tr>
                            <!--Loop through all items for this procurement -->
                                
                                    <td>{{ $item->item_title }}</td>
                                    <td>{{ $item->description }}</td>
                                    <td>{{ $item->price }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ $item->total }}</td>
                                    <td>
                                        <div class='btn-group' style="display:block">
                                            
                                            <a href="{{url('update_request_details/'.$item->request_id.'/'.$item->main_id)}}" style="margin-right: 10px;" 
                                               class='btn btn-default btn-xs'>
                                                <i class="glyphicon glyphicon-eye-open"></i>Edit</a>
                                            <form action="{{url('lineManagerUpdateRequests')}}" method="post">
                                            
                                            <input type="radio" name="item_id[{{$item->request_id}}]" value="2" checked="checked"> Approve
                                            <input type="radio" name="item_id[{{$item->request_id}}]" value="6"> Decline
                                            <input type="hidden" name="request_id" value="{{$request_id}}">
                                            
                                        </div>
                                      
                                    </td>    
                    </tr>
                    
                    @endforeach
                    @endif
                </tbody>

            </table>
            
            <input type="submit" name="submit" class="btn btn-sm btn-danger">
            </form>
               
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#showRequests').DataTable({
                searching: false,
                "pagingType": "full_numbers",
                dom: 'Bfrtip'          
        });

        
    </script>
    @endSection