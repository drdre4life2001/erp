@extends('layouts.erp')

@section('page_title')
    <h4>Make a Procurement Request </h4>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-6">
            {!! Form::open(['route' => 'create_request']) !!}
            @if(isset($errors))
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endForeach
                        </ul>
                    </div>
                    @endIf
                    @endIf
                    <script>
                        var categories = <?php echo json_encode(session('categories'));  ?> ;
                        var items = <?php echo json_encode(session('items'));  ?> ;
                    </script>
                    <div class="form-group col-sm-12">
                        <label class="">Title</label>
                        <input name="title" id="title" type="text" class="form-control"/>
                    </div>
                    <div class="form-group col-sm-12">
                        <label class=""> Category</label>
                        <select name="category" class="form-control category" id="input-category">
                            <option value=""> --- Please Select ---</option>
                            @foreach(session('categories') as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-12">
                        <label class="">Sub Category</label>
                        <select name="subsidiary" class="form-control" id="subsidiary_id">
                                <option value=""> --- Please Select ---</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-12">
                        <label class="">Company Subsidiary</label>
                        <select name="company_id" class="form-control" id="input-category">
                            <option value=""> --- Please Select ---</option>
                            @foreach($organizations as $organization)
                                <option value="{{$organization->id}}">{{$organization->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-12">
                        <input type="submit" value="Submit" class="btn btn-sm btn-primary"/>
                    </div>
                    {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.category').on('change', function(e){
            //alert('hi');
            //console.log(e);
            var token = $("input[name='_token']").val();
            var category = e.target.value;
            //ajax
            $.ajax({
                url: "<?php echo route('select-subsidiary') ?>",
                method: 'POST',
                data: {category:category, _token:token},
                success: function(data) {
                    $('#subsidiary_id').empty();
                    $.each(data, function(index, subCatObj){
                        $('#subsidiary_id').append('<option value="'+subCatObj.id+'">'+subCatObj.name+'</option>')
                    });
                }
            });

        });

    </script>
    @endSection