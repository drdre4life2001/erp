 @extends('layouts.erp')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Clients</h6>
                    </div>
                    <div class="pull-right">
                        <h6 class="btn btn-primary"><a href="{{ url('add_client') }}">Add Client</a></h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_1" class="table table-hover display  pb-30" >
                                    <thead class="panel panel-heading">
                                    <tr>
                                        <th>Client Name</th>
                                        <th>Phone</th>
                                        <th>Description</th>
                                        <th>Address</th>
                                        <th>TIN</th>
                                        <th>Date Added</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($clients as $bonus){ ?>
                                    <tr>
                                        <td>{{ $bonus->name }}</td>
                                        <td>{{ $bonus->phone  }}</td>
                                        <td>{{ $bonus->description }}</td>
                                        <td>{{ $bonus->address }}</td>
                                        <td>{{ $bonus->tin }}</td>
                                        <td>{{ $bonus->created_at }}</td>
                                        <td>
                                            <a href="{{ url('delete_record/'.$bonus->id.'/client') }}"><button onclick="return confirm('Are you sure? This process is not reversible!')" class="btn btn-danger">Deactivate</button></a>
                                            <a href="{{ url('update_client/'.$bonus->id) }}"><button onclick="return confirm('Are you sure? This process is not reversible!')" class="btn btn-primary">Edit</button></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection