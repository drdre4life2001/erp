@extends('layouts.erp')

@section('page_title')
@endsection

@section('content')
    
    <div class="container-fluid pt-25">
        <!-- Row -->


    <div class="row">
		<div class="col-lg-12" style= " margin:0; padding:5px; " >
            <div class="panel with-nav-tabs "  style="background-color:#d6e9c6;" >
                <div class="panel-heading"  >
                        <ul class="nav nav-tabs ">
                            <li ><a href="{{URL::to('/home')}}" >Finance Dashboard</a></li>
                            <li><a href="{{URL::to('hr_dashboard')}}" >HR Dashboard</a></li>
                            <li class="active"><a href="{{URL::to('procurement_dashboard')}}" >Procurement Dashboard</a></li>
                            <li><a href="{{URL::to('project_dashboard')}}" >Project Dashboard</a></li>
                            <li><a href="{{URL::to('employees_dashboard')}}" >Employees Dashboard</a></li>
                        </ul>
                </div>
                
            </div>
        </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
                <div class="panel panel-default card-view pt-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box bg-white">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"> &#x20A6;<span class="counter-anim">{{$total_request}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">TOTAL REQUEST ({{ date('Y') }}
                                                    )</span><i
                                                        class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
                    <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
                <div class="panel panel-default card-view pt-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box bg-white">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                        <?php
                                         foreach( $unapproved_request as $unappr){
                                                  $unapprove = $unappr->unapproved;
                                                  }
                                               $live2 =($unapprove);

                                        ?>
                                       

                                            <span class="txt-dark block counter">&#x20A6;<span class="counter-anim">{{$total_approved}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">TOTAL APPROVED</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
            <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
                <div class="panel panel-default card-view pt-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box bg-white">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                       
                                            <span class="txt-dark block counter">&#x20A6;<span class="counter-anim">{{$unapprovedCash}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">TOTAL Unapproved</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        
        </div>
        <!-- Row -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-6 col-xs-12">
                <div id="" class="panel panel-default card-view">
                    <div class="panel panel-heading">
                        <div class="panel panel-title"> Latest Procurement Requests</div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="" class="table table-hover table-bordered display mb-30">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Decription</th>
                                            <th>Cost</th>
                                            <th>Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                         @foreach( $procurement_requests as  $procurement_request)
                                        <tr>
                                            <td>{{$procurement_request->title }}</td>
                                            <td>{{$procurement_request->description }}</td>
                                            <td>{{$procurement_request->price }}</td>
                                            <td>{{$procurement_request->total}}</td>
                                        </tr>
                                           @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xs-12">
            <?php
foreach( $approved_request as $appr){
    $approve = $appr->approved;
   
   
}

foreach( $unapproved_request as $unappr){
    $unapprove = $unappr->unapproved;
   
   
}

 $live1 =($approve);
 $live2 =($unapprove);

?>
                <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
            </div>
            
            <!-- /Row -->
        </div>

@endsection




@section('script')
            <script>

var approved_request = <?php echo $live1; ?>;
var unapproved_request = <?php echo $live2; ?>;
                    
               Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    credits: {
    enabled: false
},
    title: {
        text: 'Request Satatus'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Request',
        colorByPoint: true,
        data: [  {
            name: 'Approved',
            y: approved_request
        }, {
            name: 'Unapproved',
            y: unapproved_request
        }]
    }]
});
            </script>

@endsection



