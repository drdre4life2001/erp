
<div class="container">
    <div class="row"><br>
        <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
            <div class="panel panel-default card-view pt-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-white">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                        <center>
                                            <span class="txt-dark block counter"> <span class="counter-anim">{{$users[0]->count}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">ALL USERS
                                                </span></span>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
            <div class="panel panel-default card-view pt-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-white">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                        <center>
                                            <span class="txt-dark block counter"> <span class="counter-anim">{{$employees[0]->count}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">EMPLOYEES
                                                </span></span>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <div id="depart"></div>
        </div>
    </div>
</div>

