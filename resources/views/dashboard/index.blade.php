@extends('layouts.erp')
<link href="{{asset('assets/css/dashboard.css')}}" rel="stylesheet">
@section('content')
<?php
?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel tabbed-panel compact-panel panel-primary">
                <div class="panel-heading clearfix">
                    <div class="panel-title pull-left"></div>
                    <div class="pull-right">
                        <ul class="nav nav-tabs">
                            <?php
                            $financeControl = \App\Libraries\Utilities::dashboardControl(3);

                            $hrControl = \App\Libraries\Utilities::dashboardControl(2);
                            $procureControl = \App\Libraries\Utilities::dashboardControl(6);

                            $crmControl = \App\Libraries\Utilities::dashboardControl(10);
                            ?>
                            @if($financeControl == true)<li @if($url == 'finance_dashboard') class="active" @endif><a href="{{url('/finance_dashboard')}}" >Finance</a></li>@endif
                            @if($hrControl == true)
                                <li @if($url == 'hr_dashboard') class="active" @endif>
                                    <a href="{{url('/hr_dashboard')}}">Human Resource</a></li>
                            @endif
                            @if($procureControl == true)
                                <li @if($url == 'procure_dashboard') class="active" @endif> <a href="{{url('/procure_dashboard')}}">Procurement</a> </li>
                            @endif
                            @if($crmControl == true)
                                <li @if($url == 'crm_dashboard') class="active" @endif> <a href="{{url('/crm_dashboard')}}">CRM</a> </li>
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab-default-1">
                            @if($financeControl == true && $url == 'finance_dashboard' && auth()->user()->role)
                                    @include('dashboard.finance')
                            @elseif($hrControl == true && $url == 'hr_dashboard')
                                @include('dashboard.hr')
                            @elseif($procureControl == true && $url == 'procure_dashboard')
                                @include('dashboard.procurement')
                            @elseif($crmControl == true && $url == 'crm_dashboard')
                                @include('dashboard.crm')
                            @elseif($url == '/' && $financeControl == true && $hrControl == true && $procureControl == true)
                                @include('dashboard.admin') 
                            @elseif($financeControl == true)
                                @include('dashboard.finance')
                            @elseif($hrControl == true)
                                @include('dashboard.hr')
                            @elseif($procureControl == true)
                                @include('dashboard.procurement')
                            @elseif($crmControl == true)
                                @include('dashboard.crm')
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('ul li a').click(function(){
            $(this).addClass("active");
        });
    </script>
@endsection