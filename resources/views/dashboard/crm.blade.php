
<?php
$total_prospects = \App\Libraries\Utilities::totalProspects();
$total_inline =\App\Libraries\Utilities::totaInLineProspects();
$total_close = \App\Libraries\Utilities::totalClosedProspects();
$total_deactivated = \App\Libraries\Utilities::totalDeactivatedProspects();
$data =\App\Libraries\Utilities::employeeByDepartment();
?>
<div class="container">
    <div class="row"><br>
        <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
            <div class="panel panel-default card-view pt-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-white">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                        <center>
                                            <span class="txt-dark block counter"> <span class="counter-anim">{{$total_prospects}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">TOTAL PROSPECTS
                                                </span></span>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
            <div class="panel panel-default card-view pt-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-white">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                        <center>
                                            <span class="txt-dark block counter"> <span class="counter-anim">{{$total_inline}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">TOTAL PROSPECT IN-PIPELINE
                                                </span></span>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
            <div class="panel panel-default card-view pt-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-white">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                        <center>
                                            <span class="txt-dark block counter"> <span class="counter-anim">{{$total_close}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">TOTAL PROSPECT CLOSED
                                                </span></span>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-6 col-sm-5 col-xs-12">
            <div class="panel panel-default card-view pt-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-white">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                        <center>
                                            <span class="txt-dark block counter"> <span class="counter-anim">{{$total_deactivated}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">TOTAL PROSPECT DEACTIVATED
                                                </span></span>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

    </div>
</div>

