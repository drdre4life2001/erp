@extends('layouts.erp')

@section('page_title')
@endsection

@section('content')

    <div class="container-fluid pt-25">
        <!-- Row -->
        <br>
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"> <span style="color:white; font-weight: bold;">Dashboard</span></h3>
                        <span class="pull-right">
                                <!-- Tabs -->
                                <ul class="nav panel-tabs">
                                    <li class="active"><a href="{{ url('finance/dashboard') }}">Finance</a></li>
                                    <li><a href="{{ url('finance/dashboard') }}" >Human Resources</a></li>
                                    <li><a href="{{ url('finance/dashboard') }}" >Procurement</a></li>
                                    <li><a href="{{ url('employee/dashboard') }}" >Employee</a></li>
                                </ul>
                            </span>
                    </div>
                    <br> <br>
                    <center>
                        <div class="panel-body">
                            <div class="tab-content">

                                    <div class="tab-pane active container" id="tab1">


                                        <div class="col-lg-3 well ">
                                            <center>
                                                <h4>
                                                    <b style="color:red;">Total Revenue</b>
                                                </h4>
                                                <h4>
                                                    &#x20A6; 1,000,000
                                                </h4>
                                            </center>
                                        </div>

                                        <div class="col-lg-3 well col-lg-offset-1">
                                            <center>
                                                <h4>
                                                    <b style="color:red;">Total Payables</b>
                                                </h4>
                                                <h4>
                                                    &#x20A6; {{ number_format($total_payables, 2) }}
                                                </h4>
                                            </center>
                                        </div>

                                        <div class="col-lg-3 well col-lg-offset-1">
                                            <center>
                                                <h4>
                                                    <b style="color:red;">Total Expenses</b>
                                                </h4>
                                                <h4>
                                                    &#x20A6; {{ number_format($total_expenses, 2) }}
                                                </h4>
                                        </div>
                                    </div>


                            </div>
                    </center>
                </div>
            </div>
        </div>
        <!-- Row -->
        <div class="row">
            <div class="col-lg-6 col-xs-12">
                <div id="weather_1" class="panel panel-default card-view">
                    <div class="panel panel-heading">
                        <div class="panel panel-title"> Latest Expense Requests</div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="datable_1" class="table table-hover table-bordered display mb-30">
                                        <thead>
                                        <tr>
                                            <th>
                                                <center>
                                                    S/N
                                                </center>
                                            </th>
                                            <th>
                                                <center>
                                                    Title
                                                </center>
                                            </th>
                                            <th>
                                                <center>
                                                    Description
                                                </center>
                                            </th>
                                            <th>
                                                <center>
                                                    Category
                                                </center>
                                            </th>
                                            <th>
                                                <center>
                                                    Expected Amount
                                                </center>
                                            </th>
                                            <th>
                                                <center>
                                                    Status
                                                </center>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $num = 1; ?>
                                                    @foreach($other_expenses as $expense)
                                                        <tr>
                                                            <td>
                                                                <center>
                                                                    ({{ $num++ }})
                                                                </center>
                                                            </td>
                                                            <td>
                                                                <center> {{ $expense->title }} </center>
                                                            </td>
                                                            <td>
                                                                <center>
                                                                    {{ $expense->description }}
                                                                </center>
                                                            </td>
                                                            <td>
                                                                {{ $expense->category }}<span style="font-weight: bold;"></span>
                                                            </td>

                                                            <td>    
                                                                &#8358;
                                                                {{ number_format($expense->amount, 2) }}<span style="font-weight: bold;"></span>
                                                            </td>

                                                            <td>
                                                                {{ $expense->status }}<span style="font-weight: bold;"></span>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 ">
                <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto">

                </div>
            </div>
            <!-- /Row -->
        </div>
        <?php  $expense = floatval($total_expenses/100); ?>
        <?php  $payables = floatval($total_payables/100); ?>


        @endsection
    @section('script')
            <script>
                var expense = '<?php echo $expense; ?>';
                var payables = '<?php echo $payables; ?>'

                Highcharts.chart('container', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Analysis'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                            name: 'Total Payables',
                            y: {{ $payables }}
                        }, {
                            name: 'Total Expenses',
                            y: {{ $expense }},
                            sliced: true,
                            selected: true
                        }, {
                            name: 'Total Revenue',
                            y: 10.38
                        }]
                    }]
                });
            </script>

@endsection
