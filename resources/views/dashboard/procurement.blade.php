
<?php
$total_requests = \App\Libraries\Utilities::totalRequests();
$total_approved_requests =\App\Libraries\Utilities::totalApprovedRequests();
?>
<div class="container">
    <div class="row"><br>
        <div class="col-lg-5 col-md-6 col-sm-5 col-xs-12">
            <div class="panel panel-default card-view pt-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-white">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                        <center>
                                            <span class="txt-dark block counter"> &#x20A6; <span class="counter-anim">{{number_format($total_requests)}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">TOTAL REQUESTS
                                                </span></span>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-5 col-xs-12 col-lg-offset-1">
            <div class="panel panel-default card-view pt-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-white">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                        <center>
                                            <span class="txt-dark block counter"> &#x20A6; <span class="counter-anim">{{number_format($total_approved_requests)}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">TOTAL APPROVED REQUEST
                                                </span></span>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row">

    </div>
</div>


