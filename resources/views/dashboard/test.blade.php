@extends('layouts.erp')
<?php
$total_employees = \App\Libraries\Utilities::totalEmployee();
$total_loan =\App\Libraries\Utilities::totalLoans();
$total_salary = \App\Libraries\Utilities::totalSalary();
$total_leave_request = \App\Libraries\Utilities::totalLeaveRequest();
$data =\App\Libraries\Utilities::employeeByDepartment();
//var_dump($data);exit;
?>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
                <div class="panel panel-default card-view pt-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box bg-white">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"> <span class="counter-anim">{{$total_employees}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">TOTAL EMPLOYEE
                                                </span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
                <div class="panel panel-default card-view pt-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box bg-white">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"> <span class="counter-anim">{{$total_leave_request}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">TOTAL LEAVE REQUEST
                                                </span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
                <div class="panel panel-default card-view pt-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box bg-white">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"> &#x20A6;<span class="counter-anim">{{$total_loan}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">TOTAL LOAN
                                                </span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
                <div class="panel panel-default card-view pt-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box bg-white">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"> &#x20A6;<span class="counter-anim">{{$total_salary}}</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">TOTAL SALARY
                                                </span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!--Number of employees based on departments-->
            <div class="col-md-6">
                <div id="depart"></div>
            </div>
        </div>
    </div>


    <script>
        <?php
        //$data =\App\Libraries\Utilities::employeeByDepartment();
        echo "var jdata =  $data ;";
        ?>
        console.log(jdata);

    </script>
    <?php
    // var_dump($data);exit;
    ?>
@endsection
@section('script')
    <script type="text/javascript">
        //$(function () {
        //var data_click = 4;
        //var data_viewer = 5;
        Highcharts.chart('depart', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Number of employee by department'
            },
            xAxis: {
                categories: jdata.department, //['2013','2014','2015', '2016']
            },
            yAxis: {
                title: {
                    text: 'Prospects initiated'
                }
            },
            series: [{
                name: 'Prospects',
                data: jdata.count
            }]
        });
        //});
    </script>

@endsection


