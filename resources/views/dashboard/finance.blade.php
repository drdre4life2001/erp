<?php
$revenue = \App\Libraries\Utilities::totalRevenue(); //Fetch all revenue
foreach ($revenue as $revenue){
    $total = $revenue->total;
}
$total_payable = \App\Libraries\Utilities::totalPayable();
$total_expenses = \App\Libraries\Utilities::totalExpense();
?>
<div class="container">
    <div class="row">
        <div class="col-lg-4 well">
                <div class="row">
                    <div class="col-md-3">
                        <span style="font-size: 30px;">
                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="col-md-9">
                        <h6>
                            <b style="color:red;">Total Revenue</b>
                        </h6>
                        <h4>
                            &#x20A6;{{number_format($total)}}
                        </h4>
                    </div>
                </div>
        </div>

        <div class="col-lg-4 well">
            <div class="row">
                <div class="col-md-3">
                        <span style="font-size: 30px;">
                            <i class="fa fa-university" aria-hidden="true"></i>
                        </span>
                </div>
                <div class="col-md-9">
                    <h6>
                        <b style="color:red;">Total Payable</b>
                    </h6>
                    <h4>
                        &#x20A6;{{number_format($total_payable)}}
                    </h4>
                </div>
            </div>
        </div>
        <div class="col-lg-4 well">
                <div class="row">
                    <div class="col-md-3">
                        <span style="font-size: 30px;">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="col-md-9">
                        <h6>
                            <b style="color:red;">Total Expenses</b>
                        </h6>
                        <h4>
                            &#x20A6;{{number_format($total_expenses)}}
                        </h4>
                    </div>
                </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto">
            </div>
        </div>
    </div>
</div>
<?php  $total_revenue = floatval($total/100); ?>
<?php  $total_payable = floatval($total_payable/100); ?>
<?php  $total_expense = floatval($total_expenses/100); ?>
@section('script')
    <script>
        var expense = '<?php echo $total_expense; ?>';
        var payable = '<?php echo $total_payable; ?>';
        var revenue = '<?php echo $total_revenue; ?>';
        Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Total Payables',
                    y: {{ $total_payable }}
                }, {
                    name: 'Total Expenses',
                    y: {{ $total_expense }},
                    sliced: true,
                    selected: true
                }, {
                    name: 'Total Revenue',
                    y: {{ $total_revenue }}
                }]
            }]
        });
    </script>

@endsection