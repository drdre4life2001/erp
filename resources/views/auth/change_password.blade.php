<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Taerp">
    <meta name="author" content="Method main">
    <meta name="keyword" content="Enterprise Resource Planning System">
    <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->

    <title>Taerp</title>

    <!-- Icons -->
    <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/simple-line-icons.css')}}" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

</head>

<body class="app flex-row align-items-center">
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-group mb-0">
                <div class="card p-4">
                    <div class="card-block">
                        <h1>Verify Account</h1>
                        <p class="text-muted">Provide password for your account</p>
                        <form class="form-horizontal" role="form" method="POST" action="">
                            {{ csrf_field() }}

                            @if(isset($errors))
                            @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                    @endForeach
                                </ul>

                            </div>
                            @endIf
                            @endIf

                            <div class="input-group mb-3">
                                <span class="input-group-addon"><i class="icon-lock"></i>
                                </span>

                                <input id="password" type="password" placeholder="password" class="form-control" name="password">
                            </div>
                            <div class="input-group mb-4">
                                <span class="input-group-addon"><i class="icon-lock"></i>
                                </span>
                                <input id="password" type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation">

                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary px-4">Verify</button>
                                </div>
                                <div class="col-6 text-right">
                                    <a href="{{url('/')}}">Already have an account? login</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card card-inverse card-primary py-5 d-md-down-none" style="width:44%">
                    <div class="card-block text-center">
                        <div>
                            <h2>TAERP</h2>
                            <p>A system of integrated applications to manage business process</p>
                            <button type="button" class="btn btn-primary active mt-3">Learn how it works!</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap and necessary plugins -->
<!-- Bootstrap and necessary plugins -->
<script src="{{asset('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/bower_components/tether/dist/js/tether.min.js')}}"></script>
<script src="{{asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>



</body>

</html>
