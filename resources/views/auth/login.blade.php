<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Taerp">
    <meta name="author" content="Method main">
    <meta name="keyword" content="Enterprise Resource Planning System">
    <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->

    <title>Advance | Enterprise Resource Planning</title>

    <!-- Icons -->
    <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/simple-line-icons.css')}}" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

</head>

<body class="app flex-row align-items-center">
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-group mb-0">
                <div class="card p-4">
                    <div class="card-block">
                        <h1>Login</h1>
                        <p class="text-muted">Sign In to your account</p>
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}
                            @if ($errors->has('email'))
                                <div class="alert alert-danger">
                                    <span class="help-block ">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                </div>
                            @endif

                            @if (session()->has('error'))
                                <div class="alert alert-danger">
                                    <span class="help-block ">
                                            <strong>{{ session()->get('error') }}</strong>
                                        </span>
                                </div>
                            @endif

                            @if ($errors->has('password'))
                                <div class="alert alert-danger">
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                </div>
                            @endif

                            <div class="input-group mb-3">
                                <span class="input-group-addon"><i class="icon-user"></i>
                                </span>
                                <input class="form-control" id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email or Phone">
                            </div>
                            <div class="input-group mb-4">
                                <span class="input-group-addon"><i class="icon-lock"></i>
                                </span>
                                <input id="password" type="password" placeholder="password" class="form-control" name="password">

                            </div>
                            <div class="row form-group ">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary px-4" style="background-color: #BC96FF; border:#BC96FF;">Login</button>
                                </div>
                                <div class="col-6 text-right">
                                    <a href="{{ url('/password/reset') }}" class="btn btn-link px-0">Forgot password?</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card card-inverse card-primary py-5 d-md-down-none" style="width:44%; background-color:#BC96FF;" >
                    <div class="card-block text-center">
                        <div>
                            <h2>ADVANCE-ERP</h2>
                            <p>A system of integrated applications to manage business process</p>
                            <button type="button" class="btn btn-primary active mt-3" style="background-color: #ffffff; color:#BC96FF; font-weight: bold; border: #BC96FF;">Learn how it works!</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap and necessary plugins -->
<!-- Bootstrap and necessary plugins -->
<script src="{{asset('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/bower_components/tether/dist/js/tether.min.js')}}"></script>
<script src="{{asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>



</body>

</html>
