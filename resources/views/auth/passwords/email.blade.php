<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Taerp">
    <meta name="author" content="Method main">
    <meta name="keyword" content="Enterprise Resource Planning System">
    <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->

    <title>Taerp</title>

    <!-- Icons -->
    <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/simple-line-icons.css')}}" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

</head>

<body class="app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-md-offset-4">

            <!DOCTYPE html>
            <html lang="en">

            <head>

                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <meta name="description" content="Taerp">
                <meta name="author" content="Method main">
                <meta name="keyword" content="Enterprise Resource Planning System">
                <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->

                <title>Taerp</title>

                <!-- Icons -->
                <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
                <link href="{{asset('assets/css/simple-line-icons.css')}}" rel="stylesheet">

                <!-- Main styles for this application -->
                <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

            </head>

            <body class="app flex-row align-items-center">
            <div class="container">

                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="">
                            <div class="card p-4">
                                <div class="card-block">
                                    <h4 class="text-center"></h4>
                                    @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                    @endif

                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                                            <div class="col-md-12">
                                                <input id="email" type="email" placeholder="admin@taerp.com" class="form-control" name="email" value="{{ old('email') }}">

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Bootstrap and necessary plugins -->
            <!-- Bootstrap and necessary plugins -->
            <script src="{{asset('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
            <script src="{{asset('assets/bower_components/tether/dist/js/tether.min.js')}}"></script>
            <script src="{{asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>



            </body>

            </html>


        </div>
</div>
</body>
</html>

