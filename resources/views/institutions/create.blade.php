@extends('layouts.erp')

@section('page_title')
<h2>Create Institution</h2>
@endsection

@section('content')
    <section class="content-header">
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    @if(isset($errors))
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}} here</li>
                            @endForeach
                        </ul>
                        @endIf
                    </div>
                    @endIf
                    {!! Form::open(['route' => 'institutions.store']) !!}

                        @include('institutions.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
