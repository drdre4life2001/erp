<div class="panel-body">
    <div class="row">
        <div class="col-sm-9 col-xs-9">
            <div class="form-wrap">
                <div class="row"><!-- Code Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::label('code', 'Internal Code:') !!}
                        {!! Form::text('code', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Name Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::label('name', 'Name:') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('institutions.index') !!}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
