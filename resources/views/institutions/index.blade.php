@extends('layouts.erp')

@section('page_title')
<h4>Employees' Institutions</h4>
@endsection

@section('content')
    <section class="content-header">
        <span style="float: right"><a href="{{ url('institutions/create') }}"><button class="btn btn-success">Add Institution</button></a></span>
    </section>

    <div class="content">
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('institutions.table')
            </div>
        </div>
    </div>
@endsection

