@extends('layouts.erp')

@section('page_title')
    <h2>Edit Institution</h2>
@endsection

@section('content')
    <section class="content-header">
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    @if(isset($errors))
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}} here</li>
                                    @endForeach
                                </ul>
                                @endIf
                            </div>
                        @endIf
                        <form action="{{ url('institutions/edit', $institution->id) }}" method="POST">

                            <div class="form-group col-sm-12">
                                {!! Form::label('code', 'Internal Code:') !!}
                                {!! Form::text('code', $institution->code, ['class' => 'form-control']) !!}
                            </div>

                            <!-- Name Field -->
                            <div class="form-group col-sm-12">
                                {!! Form::label('name', 'Name:') !!}
                                {!! Form::text('name', $institution->name, ['class' => 'form-control']) !!}
                            </div>

                            <!-- Submit Field -->
                            <div class="form-group col-sm-12">
                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                <a href="{!! route('institutions.index') !!}" class="btn btn-default">Cancel</a>
                            </div>

                        </form>
                </div>
            </div>
        </div>
    </div>
@endsection