<table class="table table-responsive" id="institutions-table">
    <thead>
    <th>Name</th>
        <th>Code</th>
        <th >Action</th>
    </thead>
    <tbody>
    @foreach($institutions as $institution)
        <tr>
            <td>{!! $institution->name !!}</td>
            <td>{!! $institution->code !!}</td>
            <td>
                {!! Form::open(['route' => ['institutions.destroy', $institution->id], 'method' => 'delete']) !!}
                <div class='btn-group'>

                    <a href="{!! url('institutions/edit', [$institution->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>

                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>