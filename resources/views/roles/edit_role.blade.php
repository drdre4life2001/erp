@extends('layouts.erp')

@section('page_title')
    <h2>Update Roles</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Roles & Permissions</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">
                                    @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                    @if(isset($errors))
                                        @if(count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach($errors->all() as $error)
                                                        <li>{{$error}} here</li>
                                                    @endForeach
                                                </ul>
                                                @endIf
                                            </div>
                                        @endIf
                                        <div class="card">
                                            <div class="card-block">

                                                <form action="{{url('/roles/edit', $role->id)}}" method="post">

                                                    <!-- Name Field -->
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="name">Role Name</label>
                                                        <div class="input-group">
                                                            {!! Form::text('name', $role->name, ['id' => 'name' , 'class' => 'form-control',  ]) !!}
                                                        </div>
                                                    </div>

                                                    <!-- Description Field -->
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="description">Description</label>
                                                        <div class="input-group">
                                                            {!! Form::textarea('description', $role->description, ['id' => 'description' , 'class' => 'form-control']) !!}
                                                        </div>
                                                    </div>

                                                    <p>Select resource(s) viewable by user. <span style="color:#ea6c41;">you can select many by holding control + shift key the tap on each resource</span></p>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for=""></label>
                                                        <div class="input-group">
                                                            <select class="form-control" name="resource[]" multiple="true">

                                                                {{--@foreach($roles as $role)--}}
                                                                    {{--<option value="{{ $role->id }}" @if($db_selected_value == $role->id ) selected @endif>{{ $role->name }}</option>--}}
                                                                {{--@endforeach--}}

                                                                @foreach($sql as $sqls)
                                                                    <option value="{{$sqls->id}}" @if(in_array($role->id, $db_sel_val)) selected @endif>{{$sqls->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!-- Submit Field -->
                                                    <div class="form-group col-sm-12">
                                                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                                        <a href="{{ url('roles') }}" class="btn btn-default">Cancel</a>
                                                    </div>


                                                <!-- {!! Form::close() !!}-->
                                                </form>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
