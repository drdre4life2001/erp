@extends('layouts.app_settings')

@section('page_title')
@endsection

@section('settings_content')
    <section class="content-header">
    </section>
<div class="content">

    @if(isset($errors))
        @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endForeach
            </ul>

        </div>
        @endIf
    @endIf

    <div class="card">
        <div class="card-header">
            <strong class="">View Permission</strong>
        </div>

        <div class="card-block">
            <div class="row">
                <!-- Name Field -->
                <div class="form-group col-sm-3">
                    {!! Form::label('name', 'Name:') !!}
                    <p>{!! $roles->name !!}</p>
                </div>

                <!-- Slug Field -->
                <div class="form-group col-sm-3">
                    {!! Form::label('slug', 'Slug:') !!}
                    <p>{!! $roles->slug !!}</p>
                </div>

                <!-- Description Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('description', 'Description:') !!}
                    <p>{!! $roles->description !!}</p>
                </div>
            </div>
            <div><hr /></div>

            {!! Form::model($permissions, ['route' => ['roles.update', $roles->id], 'method' => 'patch']) !!}
            <div class="row">
                @foreach($permissions as $permission)
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::hidden($permission->slug, '0') !!}
                            {!! Form::checkbox($permission->slug, '1', isset($rolePermissions[$permission->id]) ? true : false) !!}
                            {!! Form::label($permission->slug, $permission->name) !!}
                        </div>
                        @if($permission->level)
                        <div class="col-md-6">
                            {!! Form::select('level-'.$permission->id, ['all' => 'All', 'organization' => 'Organization', 'department' => 'Department', 'self' => 'Individual'], isset($rolePermissions[$permission->id]) ? $rolePermissions[$permission->id] : null, ['class' => 'form-control input-sm']) !!}
                        </div>
                        @endif
                    </div>
                </div>
                @endforeach
                <div class="pull-right">
                    {!! Form::submit('Save', ['class' => 'btn btn-success btn-flat']) !!}
                    <a href="{{ url('roles')}}" type="button" class="btn btn-default btn-flat">Cancel</a>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

    <div class="row" style="padding-left: 20px">
        @include('roles.show_fields')
        <a href="{!! route('roles.index') !!}" class="btn btn-default">Back</a>
    </div>
</div>
@endsection
