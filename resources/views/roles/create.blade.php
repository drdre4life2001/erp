@extends('layouts.erp')

@section('page_title')
    <h2>Create Roles</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Roles & Permissions</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">
                                     @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                    @if(isset($errors))
                                        @if(count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach($errors->all() as $error)
                                                        <li>{{$error}} here</li>
                                                        @endForeach
                                                </ul>
                                                @endIf
                                            </div>
                                            @endIf
                                            <div class="card">
                                                <div class="card-block">
                                            
                                                    <form action="{{url('roles/store')}}" method="post">

                                                    @include('roles.fields')

                                                    <!-- {!! Form::close() !!}-->
                                                    </form>
                                                </div>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
