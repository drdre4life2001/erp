@extends('layouts.erp')

@section('page_title')
@endsection

@section('content')
    <div class="card-header">
        <strong class="pull-left">Roles</strong>
        <span class="pull-right">
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
           href="{{url('roles/create')}}">Add New</a></span>
    </div>

    <div class="card-block">
        @include('roles.table')
    </div>

@endsection

