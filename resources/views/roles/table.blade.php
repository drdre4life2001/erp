<table class="table table-hover display  pb-30"  id="datable_2">
    <thead>
        <th>Name</th>
        <th>Slug</th>
        <th>Description</th>
        <th>Action</th>
    </thead>
    <tbody>
    @foreach($roles as $roles)
        <tr>

            <td>{!! $roles->name !!}</td>
            <td>{!! $roles->slug !!}</td>
            <td>{!! $roles->description !!}</td>
            <td>


                <form action="{{url('roles/destroy')}}" method="delete">

                <div class='btn-group'>
                    {{--<a href="{{url('roles/show/')}}" class='btn btn-default btn-xs'><i class="fa fa-eye"></i></a>--}}
                    <a class='btn btn-default btn-xs' href="{{ url('/roles/edit', $roles->id) }}" target="_blank"><i class="glyphicon glyphicon-edit"></i></a>
{{--                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}--}}
                    <a id="a_del" href="{{ url('/roles/destroy/'. $roles->id) }}" onclick="return confirm('Are you sure to trash'+ <?php echo $roles->name; ?> +'')" class="btn btn-xs btn-danger"  title="Trash Role"> <i class="fa fa-trash"></i></a>
                </div>

                </form>
            </td>

        </tr>
    @endforeach
    </tbody>
</table>
