<!-- Name Field -->
<div class="form-group">
    <label class="control-label mb-10" for="name">Role Name</label>
    <div class="input-group">
        {!! Form::text('name', null, ['id' => 'name' , 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Description Field -->
<div class="form-group">
    <label class="control-label mb-10" for="description">Description</label>
    <div class="input-group">
        {!! Form::textarea('description', null, ['id' => 'description' , 'class' => 'form-control']) !!}
    </div>
</div>

<p>Select resource(s) viewable by user</p>
<div class="form-group">
    <label class="control-label mb-10" for=""></label>
    <div class="input-group">
        <select class="form-control" name="resource[]" multiple="true">
            @foreach($sql as $sqls)
                <option value="{{$sqls->id}}">{{$sqls->name}}</option>
            @endforeach
        </select>
    </div>
</div>
<!--
@foreach($sql as $sql)
<div class="form-group">
    <label class="control-label mb-10" for="">{{$sql->name}}</label>
    <div class="input-group">
        <input type="checkbox" name="resources[{{$sql->id}}][]" value="c">Create
        <input type="checkbox" name="resources[{{$sql->id}}][]" value="r">Read
        <input type="checkbox" name="resources[{{$sql->id}}][]" value="u">Update
        <input type="checkbox" name="resources[{{$sql->id}}][]" value="d">Delete
    </div>
</div>
@endforeach
-->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ url('roles') }}" class="btn btn-default">Cancel</a>
</div>
