@extends('layouts.erp')

@section('page_title')
<h4>Office Locations</h4>
@endsection

@section('content')
    <section class="content-header">
        <span style="float: right"><button class="btn btn-primary"><a href="{{ url('workAddresses/create') }}">Add A Location</a></button></span>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('work_addresses.table')
            </div>
        </div>
    </div>
@endsection

