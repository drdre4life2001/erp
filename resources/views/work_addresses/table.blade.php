<table class="table table-responsive" id="datable_2">
    <thead>
        <th>Name</th>
        <th>Website</th>
        <th>Mobile</th>
        <th>Phone</th>
        <th>Email</th>
        {{--<th>Fax</th>
        <th>Longitude</th>
        <th>Latitutde</th>--}}
        <th>Street</th>
        <th>Code</th>
        <th>Action</th>
    </thead>
    <tbody>
    @foreach($workAddresses as $workAddress)
        <tr>
            <td>{!! $workAddress->name !!}</td>
            <td>{!! $workAddress->website !!}</td>
            <td>{!! $workAddress->mobile !!}</td>
            <td>{!! $workAddress->phone !!}</td>
            <td>{!! $workAddress->email !!}</td>
            {{--<td>{!! $workAddress->fax !!}</td>
            <td>{!! $workAddress->longitude !!}</td>
            <td>{!! $workAddress->latitutde !!}</td>--}}
            <td>{!! $workAddress->street !!}</td>
            <td>{!! $workAddress->code !!}</td>
            <td>
                {!! Form::open(['route' => ['workAddresses.destroy', $workAddress->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('workAddresses.show', [$workAddress->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('workAddresses.edit', [$workAddress->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>