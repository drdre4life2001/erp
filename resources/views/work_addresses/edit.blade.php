@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Work Address
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($workAddress, ['route' => ['workAddresses.update', $workAddress->id], 'method' => 'patch']) !!}

                        @include('work_addresses.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection