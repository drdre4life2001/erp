@extends('layouts.erp')

@section('page_title')
    <h4>Create Location</h4>
@endsection

@section('content')
        {!! Form::open(['route' => 'workAddresses.store']) !!}
        @if(isset($errors))
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}} here</li>
                            @endForeach
                    </ul>
                    @endIf
                </div>
                @endIf
                @include('work_addresses.fields')

                {!! Form::close() !!}
@endsection
