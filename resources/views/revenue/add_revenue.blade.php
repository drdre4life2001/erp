@extends('layouts.erp')

@section('page_title')
    <h4>Add Revenue</h4><br>
@endsection

@section('content')
    <div class="row">
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        <form action="{{ url('finance/add-revenue') }}" method="post">
            {{csrf_field()}}
            @if(isset($errors))
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endForeach
                        </ul>
                    </div>
                @endIf
            @endIf

            <div class="col-md-4">

                <div class="form-group">
                    <label class="">Revenue Stream</label>
                    <select name="stream_id" class="form-control" required>
                        <option value="">--Please select --</option>
                        @if(isset($streams))
                            @foreach($streams as $stream)
                                <option value="{{$stream->id}}">{{$stream->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <label class="">From</label>
                    <input name="from" id="dt1" type="text" placeholder="Select date" class="form-control" value="{{old('from')}}" required>
                </div>
                <div class="form-group">
                    <label class="">To</label>
                    <input name="to" id="dt2" type="text" placeholder="Select date" class="form-control" value="{{old('to')}}" required>
                </div>

                <div class="form-group">
                    <input type="submit" value="Add" class="btn btn-danger btn-sm">
                </div>
            </div>

        </form>

    </div>

@endsection

@section('script')
    <script>
        $( function() {
            $("#dt1").datepicker({
                dateFormat: "yy-mm-dd",

                onSelect: function (date) {
                    var dt2 = $('#dt2');
                    var startDate = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('getDate');
                    dt2.datepicker('setDate', minDate);
                    startDate.setDate(startDate.getDate() + 60);
                    //sets dt2 maxDate to the last day of 30 days window
                    dt2.datepicker('option', 'maxDate', startDate);
                    dt2.datepicker('option', 'minDate', minDate);
                    $(this).datepicker('option', 'minDate', minDate);
                }
            });
            $('#dt2').datepicker(
                {dateFormat: "yy-mm-dd"}
            );
        } );
        //console.log(categories);
        var result = {};
        var items = {}
        var selectedCategory = "";
        //loop through the categories
        for (var i = 0; i < categories.length; i++) {
            //save the categories and categories items object
            result[categories[i].name] = categories[i].items;
            //loop through the items in the category
            for (var j = 0; j < categories[i].items.length; j++) {
                //save the items data; [items id => items obj]
                items[categories[i].items[j].id] = categories[i].items[j];
            }


        }

        //console.log(result);
        //console.log(items);

        $('select[name="category"]').change(function () { // When category select changes

            //set the unit price to default
            $('#unit_price').val('');
            $('.description').replaceWith("<div class='description col-sm-12'><p><b></b><p></div>");
            //var p = document.querySelector('.other_product_name');
            //p.innerHTML = "";
            //$('#other_product_item').fadeOut();

            /**if ($(this).val() == "Others"){
                var productfield = document.createElement('input');
                productfield.setAttribute('type', 'text');
                productfield.setAttribute('name', 'name');
                productfield.setAttribute('placeholder', "Product Eg: Dry Yam");
                productfield.classList.add('form-control');


            }else{*/
            selectedCategory = $(this).val();
            var options = '<option>Choose one!</option>';
            $.each(result[$(this).val()] || [], function (i, v) { // Cycle through each associated items
                options += '<option value="' + v.id + '">' + v.name + '</option>';
            });
            //options += '<option>' + "Other" + '</option>';
            var itemtfield = document.createElement('select');
            itemtfield.setAttribute('name', 'item');
            itemtfield.classList.add('form-control');
            itemtfield.innerHTML = options;
            /**} */

            var p = document.querySelector('.item_name');
            p.innerHTML = "";
            p.appendChild(itemtfield);
            $('#category_item').fadeIn();
            setItemListener();
//            $('select[name="product"]').html(options); // And update the role options
        });

        function setItemListener() {
            $('select[name="item"]').change(function () {

                $(this).attr('name', 'item');
                //auto-set the unit price, qty, other information
                //gets its information
                var itemObj = items[$(this).val()];
                $('#unit_price').val(itemObj.unit_price);
                $('.description').replaceWith("<div class='description col-sm-12'><p><i>" + itemObj.description + "</i><p></div>");
                //console.log(itemObj);
                //console.log();
            })
        }
    </script>
@endSection
