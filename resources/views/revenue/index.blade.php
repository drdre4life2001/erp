@extends('layouts.erp')

@section('page_title')
    <h4>All Revenue</h4><br>
@endsection

@section('content')
    <div class="row">
        @if(isset($errors))
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endForeach
                    </ul>
                </div>
            @endIf
        @endIf
        <div style="background-color: cornsilk;">
            <h5>Filter results</h5>
            <form action="{{url('finance/revenue')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                    <div class="form-group col-md-4">
                        <label class="">From</label>
                        <input name="from" id="dt1" type="text" placeholder="Select date" class="form-control" value="{{old('from')}}" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="">To</label>
                        <input name="to" id="dt2" type="text" placeholder="Select date" class="form-control" value="{{old('to')}}" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="">Revenue Stream</label>
                        <select name="stream_id" class="form-control">
                            <option value="">--Please select --</option>
                            @if(isset($streams))
                                @foreach($streams as $stream)
                                    <option value="{{$stream->id}}">{{$stream->name}}</option>
                                @endforeach
                            @endif
                            <option value="0">See All</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <input type="submit" value="Search" class="btn btn-danger btn-sm">
                    </div>
                </div>
            </form>
        </div>
        <div class="col-xs-12" style="margin-top: 5px;">
                <table class="col-xs-6">
                    <thead><b><th class="col-xs-4">&nbsp;</th><th class="col-xs-4">Before Filter</th><th class="col-xs-4">After Filter</th></b></thead>
                    <tr><td colspan="3">&nbsp;</td></tr>
                    <tr><td class="col-xs-4"><b>Total Commission (N)</b></td><td  class="col-xs-4"><button  id="amount" class="btn btn-primary"></button></td><td class="col-xs-4"><button  id="amountt" class="btn btn-primary">k</button></td></tr>
                    <tr><td colspan="3">&nbsp;</td></tr>
                    <tr><td class="col-xs-4"><b>Total Collections (N)</b></td><td class="col-xs-4"><button  id="no" class="btn btn-primary"></button></td><td class="col-xs-4"><button  id="not" class="btn btn-primary"></button></td></tr>
                </table>
        </div>
        <div class="col-sm-12">
            <table class='table table-bordered' id='revenue'>
                <thead>
                <tr>
                    <th>S/N</th>
                    <th>Date</th>
                    <th>Client </th>
                    <th>Company</th>
                    <th>Revenue Stream</th>
                    <th>Total Collections</th>
                    <th>Total Commission</th>
                    <th colspan="1">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($item))
                    <?php $i =  1; ?>
                    @foreach($item as $items)

                        <tr>
                            <!--Loop through all items for this procurement -->

                            <td>{{ $i++ }}</td>
                            <td>{{ $items->tdate }}</td>
                            <td>{{ $items->client }}</td>
                            <td>{{ $items->company }}</td>
                            <td>{{ $items->revenue_stream }}</td>
                            <td>{{ $items->total_collection }}</td>
                            <td>{{ $items->total_commission }}</td>
                            <td>
                                <div class='btn-group' style="display:block">
                                    <!--Edit item -->
                                    <a href="" style="margin-right: 0px;"
                                       class='btn btn-default btn-xs' data-toggle='modal' data-target='#{{$items->id}}'>
                                        <i class="glyphicon glyphicon-eye-open"> </i> Update</a>
                                    <!--delete this item -->
                                    {{--
                                    <a style="margin-right: 0px;" data-toggle='modal' data-target='#{{$items->id.'_'}}'
                                       class='btn btn-danger btn-xs'>
                                        <i class="glyphicon glyphicons-delete"> </i> Delete</a>
                                        --}}

                                </div>

                            </td>
                            <div id="{{$items->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h5 class="modal-title">Update Revenue</h5>
                                        </div>
                                        <form action="{{url('finance/revenue/update')}}" method="post">
                                            <div class="modal-body">
                                                <p>Are you sure you want to update revenue?</p>
                                                <input type="hidden" name="revenue_id" value="{{$items->id}}">
                                                <input type="hidden" name="stream_id" value="{{$items->stream_id}}">
                                                <input type="hidden" name="tdate" value="{{$items->tdate}}">
                                                {{--
                                                <div class="form-group col-md-6">
                                                    <label class="">Name</label>
                                                    <input name="name" id="name" type="text" class="form-control" value="{{$items->name}}">
                                                </div>
                                                --}}

                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" value="Yes" name="submit" class="btn btn-danger">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $( function() {
            $("#dt1").datepicker({
                dateFormat: "yy-mm-dd",

                onSelect: function (date) {
                    var dt2 = $('#dt2');
                    var startDate = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('getDate');
                    dt2.datepicker('setDate', minDate);
                    startDate.setDate(startDate.getDate() + 60);
                    //sets dt2 maxDate to the last day of 30 days window
                    dt2.datepicker('option', 'maxDate', startDate);
                    dt2.datepicker('option', 'minDate', minDate);
                    $(this).datepicker('option', 'minDate', minDate);
                }
            });
            $('#dt2').datepicker(
                {dateFormat: "yy-mm-dd"}
            );
        } );
        $(document).ready(function(){
            $('#revenue').DataTable({
                searching: true,
                "pagingType": "full_numbers",
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel'
                ],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total collections over all pages
                    total = api
                        .column( 5 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    // Total collections over this page
                    pageTotal = api
                        .column( 5, {"filter": "applied"} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total commission over all pages
                    totalNo = api
                        .column( 6 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total no over this page
                    pageTotalNo = api
                        .column( 6, {"filter": "applied"} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    pageTotalf = totalNo.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
                    totalf = pageTotalNo.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");

                    pageTotalNo = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
                    totalNo = pageTotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");

                    // Update footer
                    /*$( api.column( 2 ).footer() ).html(
                        '#'+pageTotalf +' / #'+ totalf
                    );
                    $( api.column( 3 ).footer() ).html(
                        pageTotalNo +' / '+ totalNo
                    );*/
                    document.getElementById('amount').innerHTML=pageTotalf;
                    document.getElementById('amountt').innerHTML=totalf;

                    document.getElementById('no').innerHTML=pageTotalNo;
                    document.getElementById('not').innerHTML=totalNo;
                }
            });
        });
    </script>
@endSection
