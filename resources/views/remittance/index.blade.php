@extends('layouts.erp')

@section('page_title')
    <h4>PHEDC Remittance</h4><br>
@endsection

@section('content')
    <div class="row">
        @if(isset($errors))
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endForeach
                    </ul>
                </div>
            @endIf
        @endIf
        <div style="background-color: cornsilk;">
            <h5></h5>
            <form action="{{url('finance/upload/remittance')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <h5>Bulk  Upload</h5>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label class="">Attach File</label>
                            <input name="excel_file"  type="file"  class="form-control"  required>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="">Click <a href="{{url('/finance/download/remittance')}}">here</a> to download sample excel document </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <input type="submit" value="Upload" class="btn btn-danger btn-sm">
                        </div>
                    </div>
            </form>
        </div>
        <div style="background-color: cornsilk;">
            <h5></h5>
            <form action="{{url('finance/remittance')}}" method="post">
                {{csrf_field()}}
                <h5>Add  Remittance</h5>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label class="">Select Date</label>
                        <input name="date" id="dt2" type="text" placeholder="Select date" class="form-control" value="{{old('date')}}" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="">Enter Amount</label>
                        <input name="amount"  type="number" placeholder="E.g. 1300293" class="form-control" value="{{old('amount')}}" required>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <input type="submit" value="Add" class="btn btn-danger btn-sm">
                    </div>
                </div>
            </form>
        </div>
        <div class="col-xs-12" style="margin-top: 5px;">
            <table class="col-xs-6">
                <thead><b><th class="col-xs-4">&nbsp;</th><th class="col-xs-4">Before Filter</th><th class="col-xs-4">After Filter</th></b></thead>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td class="col-xs-4"><b>Total Remitted (N)</b></td><td  class="col-xs-4"><button  id="amount" class="btn btn-primary"></button></td><td class="col-xs-4"><button  id="amountt" class="btn btn-primary">k</button></td></tr>
            </table>
        </div>
        <div class="col-sm-12">
            <table class='table table-bordered' id='revenue'>
                <thead>
                <tr>
                    <th>S/N</th>
                    <th>Date</th>
                    <th>Amount</th>
                    <th colspan="1">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($item))
                    <?php $i =  1; ?>
                    @foreach($item as $items)
                        <?php $remit_date = date('d-m-Y', strtotime($items->remittance_date)); ?>
                        <tr>
                            <!--Loop through all items for this procurement -->

                            <td>{{ $i++ }}</td>
                            <td>{{ $remit_date }}</td>
                            <td>{{ $items->amount }}</td>
                            <td>
                                <div class='btn-group' style="display:block">
                                    <!--Edit item -->
                                    <a href="" style="margin-right: 0px;"
                                       class='btn btn-default btn-xs' data-toggle='modal' data-target='#{{$items->id}}'>
                                        <i class="glyphicon glyphicon-eye-open"> </i> Edit</a>
                                    <!--delete this item -->
                                    {{--
                                    <a style="margin-right: 0px;" data-toggle='modal' data-target='#{{$items->id.'_'}}'
                                       class='btn btn-danger btn-xs'>
                                        <i class="glyphicon glyphicons-delete"> </i> Delete</a>
                                        --}}

                                </div>

                            </td>
                            <!-- Delete Modal -->
                        {{--
                        <div class="modal fade" id="{{$items->id.'_'}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action = "{{url('finance/revenue_stream/delete')}}" method="post">
                                        <div class="modal-body">
                                            Are you sure you want to delete this revenue stream?
                                            <input type="hidden" name="item_id" value="{{$items->id}}">
                                        </div>
                                        <div class="modal-footer">

                                            <a class="btn btn-default btn-ok" data-dismiss="modal">No</a>
                                            <input type="submit" value="Yes"  class="btn btn-danger">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        --}}
                        <!--Modal ends -->
                            <!-- Modal -->
                            <div id="{{$items->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h5 class="modal-title">Edit Remittance</h5>
                                        </div>
                                        <form action="{{url('finance/remittance/update')}}" method="post">
                                            <div class="modal-body">
                                                <input type="hidden" name="remittance_id" value="{{$items->id}}">
                                                <div class="form-group col-md-6">
                                                    <label class="">Amount</label>
                                                    <input name="amount" id="amount" type="amount" class="form-control" value="{{$items->amount}}" required>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" value="Update" name="submit" class="btn btn-danger">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--Modal ends -->

                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $( function() {
            $("#dt1").datepicker({
                dateFormat: "yy-mm-dd",

                onSelect: function (date) {
                    var dt2 = $('#dt2');
                    var startDate = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('getDate');
                    dt2.datepicker('setDate', minDate);
                    startDate.setDate(startDate.getDate() + 60);
                    //sets dt2 maxDate to the last day of 30 days window
                    dt2.datepicker('option', 'maxDate', startDate);
                    dt2.datepicker('option', 'minDate', minDate);
                    $(this).datepicker('option', 'minDate', minDate);
                }
            });
            $('#dt2').datepicker(
                {dateFormat: "yy-mm-dd"}
            );
        } );
        $(document).ready(function(){
            $('#revenue').DataTable({
                searching: true,
                "pagingType": "full_numbers",
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel'
                ],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total collections over all pages
                    total = api
                        .column( 3 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    // Total collections over this page
                    pageTotal = api
                        .column( 3, {"filter": "applied"} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total commission over all pages
                    totalNo = api
                        .column( 2 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total no over this page
                    pageTotalNo = api
                        .column( 2, {"filter": "applied"} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    pageTotalf = totalNo.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
                    totalf = pageTotalNo.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");

                    pageTotalNo = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
                    totalNo = pageTotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");

                    // Update footer
                    /*$( api.column( 2 ).footer() ).html(
                        '#'+pageTotalf +' / #'+ totalf
                    );
                    $( api.column( 3 ).footer() ).html(
                        pageTotalNo +' / '+ totalNo
                    );*/
                    document.getElementById('amount').innerHTML=pageTotalf;
                    document.getElementById('amountt').innerHTML=totalf;

                    document.getElementById('no').innerHTML=pageTotalNo;
                    document.getElementById('not').innerHTML=totalNo;
                }
            });
        });
    </script>
@endSection
