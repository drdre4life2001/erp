@extends('layouts.erp')

@section('page_title')
    <h4>All Pipelines</h4><br>
@endsection

@section('content')
    <div class="row">
        @if(isset($errors))
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endForeach
                    </ul>
                </div>
            @endIf
        @endIf
        @if (session('message'))
            <div class="alert alert-danger">
                {{ session('message') }}
            </div>
        @endif
        <div class="col-sm-12">
            <table class='table table-bordered' id='prospect'>
                <thead>
                <tr>
                    <th>S/N</th>
                    <th>Prospect </th>
                    <th>Date</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($item))
                    <?php $i = 1; ?>
                    @foreach($item as $items)
                        <?php $tdate = date('d-m-Y', strtotime($items->activity_date)); ?>
                        <tr>
                            <!--Loop through all items for this procurement -->

                            <td>{{ $i++ }}</td>
                            <td>{{ $items->prospect }}</td>
                            <td>{{ $tdate}}</td>
                            <td>{{ $items->title }}</td>
                            <td>{{ $items->description }}</td>
                            <td>{{ $items->status }}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function(){
            $('#prospect').DataTable({
                searching: true,
                "pagingType": "full_numbers",
                dom: 'Bfrtip'
            });
            //$("#activity").datepicker();
        });
    </script>
@endSection
