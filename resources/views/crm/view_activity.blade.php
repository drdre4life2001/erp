@extends('layouts.erp')

@section('page_title')
    <h4>Activity Log</h4><br>
    <a href="{{route('prospects')}}" class="btn btn-sm btn-danger">Go Back</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                @if(isset($pipelines))
                    <?php $i = 1; ?>
                    @foreach($pipelines as $pipeline)
                        <?php $date = date('d-m-Y', strtotime($pipeline->activity_date)); ?>
                        <div class="col-md-4 well">

                            <span class="badge">{{ $i++ }}</span><br>
                            Title: {{$pipeline->title}} <br><br>
                            Description: {{$pipeline->description}} <br><br>
                            Created: {{$date}} <br>
                            Status: {{$pipeline->status}}<br>
                            @if($pipeline->pipeline_status != 3)
                                <a href="" style=""
                                   class='btn btn-primary btn-xs' data-toggle='modal' data-target='#{{$pipeline->id}}'>
                                    <i class="glyphicon glyphicon-plus"> </i> Update Status</a>
                        @endif

                        <!-- Update Status Modal -->
                            <div id="{{$pipeline->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Update Status</h4>
                                        </div>
                                        <form action="{{url('crm/pipeline/update')}}" method="post">
                                            <div class="modal-body">
                                                <input type="hidden" name="prospect_id" value="{{$pipeline->prospect_id}}">
                                                <input type="hidden" name="pipeline_id" value="{{$pipeline->id}}">
                                                <div class="form-group col-sm-6">
                                                    {{--<label class="">Please Select</label> --}}
                                                    <select name="status" class="form-control" required>
                                                        @foreach($pipeline_status as $stat)
                                                            <option
                                                                <?php
                                                                if ($stat->id == $pipeline->status) {
                                                                    echo 'selected';
                                                                }
                                                                ?>
                                                                value="{{$stat->id}}">
                                                                {{$stat->definition}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" value="update" name="submit" class="btn btn-danger">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                            <!-- updating end-->
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    {{--
    <div class="col-xs-6 col-md-3">
                <div class="thumbnail">
                    <span class="badge">{{ $i++ }}</span><br>
                    Title: {{$pipeline->title}} <br><br>
                    Description: {{$pipeline->description}} <br><br>
                    Status: {{$pipeline->status}}<br>
                    Date: {{$date}}

                </div>

            </div>
      --}}
@endsection


