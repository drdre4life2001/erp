@extends('layouts.erp')

@section('page_title')
    <h4>Sales activated by Users</h4><br>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <div class="panel-body">
                        <div id="byuser"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    <?php

        echo "var jdata =  $data ;";
    ?>
    console.log(jdata);

</script>
<?php
       // var_dump($data);exit;
?>
@section('script')
    <script type="text/javascript">
        $(function () {
            //var data_click = 4;
            //var data_viewer = 5;
            Highcharts.chart('byuser', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Sales activated by users'
                },
                xAxis: {
                    categories: jdata.employee, //['2013','2014','2015', '2016']
                },
                yAxis: {
                    title: {
                        text: 'Prospects initiated'
                    }
                },
                series: [{
                    name: 'Prospects',
                    data: jdata.count
                }]
            });
        });
    </script>

@endsection