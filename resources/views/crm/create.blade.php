 @extends('layouts.erp')

@section('page_title')
    <h4>Create Prospect</h4>
@endsection
@section('error')
    @if(isset($errors))
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endForeach
                </ul>

            </div>
        @endIf
    @endIf
@endsection
@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Add A New Prospect</h6>
                    </div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap mt-40">
                            <form action="{{ url('crm/prospects/create') }}" method="post">
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Company Name</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-building"></i></div>
                                        <input type="text" class="form-control" id="exampleInputuname_1"
                                               placeholder="e.g ABC-123 Nigeria Limited" name="name" value="{{old('name')}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Name of Executive</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-building"></i></div>
                                        <input type="text" class="form-control" id="exampleInputuname_1"
                                               placeholder="John Doe" name="contact_person" value="{{old('contact_person')}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Position of Executive</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-building"></i></div>
                                        <input type="text" class="form-control" id="exampleInputuname_1"
                                               placeholder="E.g. CEO" name="contact_position" value="{{old('contact_position')}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Contact Address</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                        <textarea class="form-control" name="address" required>{{old('address')}}
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Email Address</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                                        <input type="email" class="form-control" id="exampleInputuname_1"
                                               placeholder="admin@admin.com"  value="{{old('email')}}" name="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Phone
                                        Number</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                                        <input type="text" class="form-control" id="exampleInputuname_1"
                                               placeholder="e.g +234089762626" value="{{old('phone')}}" name="phone" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Prospect's Code</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                        <input type="text" class="form-control" id="exampleInputuname_1"
                                               placeholder="" value="{{old('code')}}" name="code" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Assign Prospect to a Subsidiary</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-building-o"></i></div>
                                        <select class="form-control" name="subsidiary" required>
                                            <option value="">--Please select--</option>
                                            @foreach($organizations as $organization)
                                                <option value="{{$organization->id}}">{{$organization->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">Estimated Revenue</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                        <input type="number" class="form-control" id="exampleInputuname_1"
                                               placeholder="E.g. 12938838" value="{{old('revenue')}}" name="revenue" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-15">
                                        <input class="btn btn-success btn-anim" type="submit" value="Add A New Prospect">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection

@section('script')
    <script src="{{asset('assets/js/location.js')}}"></script>
@endsection
