@extends('erp.layouts.master')

  @section('title')
    Finance - Invoice
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

@section('content')
    <section class="content-header">
    <h1>
      Invoice
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Invoice</li>
    </ol>
    </section>
    <section class="content">
    <div class="col-md-12">
      <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Invoice
               </b></a>
            </li>
         </ul>
         <div class="tab-content" style="padding: 2%">
            <div class="tab-pane active" id="tab_1">
               <p align="right">
                  <a href="{!! route('finance.invoice.create') !!}" class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add  Invoice
                  </a>
               </p>
               <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
                     {{-- @include('erp.hr.partials.employee_loan_form') --}}
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
               </div>
               <hr/ style="clear: both">
               <div class="box-body">
                  <div class="">
                     <table class="table table-responsive" id="example1">
                         <thead>
                             <th>S/N</th>
                             <th>Customer</th>
                             <th>Amount</th>
                             <th>Invoice Date</th>
                             <th>Revenue Stream</th>
                             <th>Due Date</th>
                             <th>Status</th>
                             <th >Action</th>
                         </thead>
                         <tbody>
                         <?php $i = 0; ?>
                         @foreach($details as $detail)
                             <?php
                                 $i++;
                                 $invoice_date = date("l jS \, F Y", strtotime($detail->invoice_date));
                                 $due_date = date("l jS \, F Y", strtotime($detail->due_date));
                             ?>
                             <tr>
                                 <td>{!! $i !!}</td>
                                 <td>{!! $detail->client !!}</td>
                                 <td>	&#8358;{!! number_format($detail->grand_total, 2) !!}</td>
                                 <td>{{$invoice_date}}</td>
                                 <td>{{ $detail->revenue_stream_name }}</td>
                                 <td>{{$due_date}}</td>
                                 <td>{{$detail->status}}</td>
                                 <td>
                                     {!! Form::open(['route' => ['finance.invoice.destroy', $detail->id], 'method' => 'delete']) !!}
                                     <div class='btn-group'>
                                         <a href="{!! route('finance.invoice.show', [$detail->id]) !!}" class='btn btn-warning btn-xs'><i class="fa fa-eye"></i></a>
                                         <br>{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                     </div>
                                     {!! Form::close() !!}
                                 </td>
                             </tr>
                         @endforeach
                         </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>
    </section>
@endsection


@section('script')
 <script type="text/javascript">
     $("#example1").DataTable();

   $('.adf').hide();
   $('.cdb').on('click', function(){
     $('.adf').slideToggle();
   });
   @if ($errors->has('employee_id') || $errors->has('amount') || $errors->has('interest') || $errors->has('tenure') || $errors->has('description'))
     $('.adf').slideToggle();
   @endif

     $('.cadfxx').on('click', function(){
        $('.adf').slideToggle();
     });
 </script>


