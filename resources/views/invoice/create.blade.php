@extends('erp.layouts.master')
  @section('title')
    Finance - Invoice
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

@section('content')
    <section class="content-header">
    <h1>
      Invoice
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Invoice</li>
    </ol>
    </section>
    <section class="content">
    <div class="col-md-12">
      <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Invoice
               </b></a>
            </li>
         </ul>
         <div class="tab-content" style="padding: 2%">
            <div class="tab-pane active" id="tab_1">
               <p align="right">
                  <a href="{!! route('finance.invoice.create') !!}" class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add  Invoice
                  </a>
               </p>
               <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
                     {!! Form::open(['route' => 'finance.invoice.store']) !!}
                     <div class="box-body">
                         <div class="form-group col-md-12">
                             <div class="col-md-4">
                             <label>Select Revenue Stream <a title="" target="_blank" class="btn btn-xs btn-purple" href="{{url('/finance/revenue-streams')}}"><i class="fa fa-plus"></i> Add Revenue Stream</a></label>
                                 <select class="form-control revenue_stream_search" style="width:100%;" name="revenue_stream">
                                </select>
                             </div>
                             {{--
                             <div class="col-md-4">
                                 <label>Select Subsidiary</label>
                                 <select name="subsidiary" class="form-control" required>
                                     <option value="">--Please select--</option>
                                     @foreach($subsidiaries as $subsidiary)
                                         <option value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                     @endforeach
                                 </select>
                             </div>
                             --}}
                             <div class="col-md-4 col-md-offset-4" >
                                 <label>Invoice Date</label>
                                 <input type="date" value="{{old('invoice_date')}}" id="invoiced_at" name="invoice_date" class="form-control" required>
                             </div>

                         </div>
                         <div class="form-group col-md-12">
                             <div class="col-md-4">

                                 <label>Due Date</label>
                                 <input type="date" value="{{old('due_date')}}" id="due_at" name="due_date" class="form-control" required>
                             </div>
                             <div class="col-md-4">
                                 <label>Invoice Number</label>
                                 <input type="text" value="{{old('invoice_number')}}" name="invoice_number" class="form-control" placeholder="Enter Invoice Number" required>
                             </div>
                             <div class="col-md-4">
                                 <label>Order Number</label>
                                 <input type="text" value="{{old('order_number')}}" name="order_number" class="form-control" placeholder="Enter Order Number" required>
                             </div>

                         </div>

                         <div class="form-group col-md-12">
                             {!! Form::label('items', 'Items', ['class' => 'control-label']) !!}
                             <div class="table-responsive">
                                 <table class="table table-bordered" id="items">
                                     <thead>
                                     <tr style="background-color: #f9f9f9;">
                                         <th width="5%"  class="text-center">Actions</th>
                                         <th width="40%" class="text-left">Name</th>
                                         <th width="5%" class="text-center">Quantity</th>
                                         <th width="10%" class="text-right">Price</th>
                                         <th width="15%" class="text-right">Tax</th>
                                         <th width="10%" class="text-right">Total</th>
                                     </tr>
                                     </thead>
                                     <tbody>
                                     <?php $item_row = 0; ?>
                                     <tr id="item-row-{{ $item_row }}">
                                         <td class="text-center" style="vertical-align: middle;">
                                             <button type="button" onclick="$(this).tooltip('destroy'); $('#item-row-{{ $item_row }}').remove(); totalItem();" data-toggle="tooltip" title="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                         </td>
                                         <td>
                                             <input class="form-control typeahead" required="required" placeholder="Enter name" name="item[{{ $item_row }}][name]" type="text" id="item-name-{{ $item_row }}">
                                             <input  name="item[{{ $item_row }}][item_id]" type="hidden" id="item-id-{{ $item_row }}" >
                                         </td>
                                         <td>
                                             <input  class="form-control text-center"  required="required" name="item[{{ $item_row }}][quantity]" onkeyup="totalItem()" type="text" id="item-quantity-{{ $item_row }}">

                                         </td>
                                         <td>
                                             <input class="form-control text-right" required="required" name="item[{{ $item_row }}][price]" onkeyup="totalItem()" type="text" id="item-price-{{ $item_row }}">
                                         </td>
                                         <td>
                                             <select name="item[{{ $item_row }}][tax_id]" class="form-control iop" id='item-tax-'. {{$item_row}} onchange="totalItem()">
                                                 <option value="">--Please select--</option>
                                                 @foreach($taxes as $tax)
                                                     <option value="{{$tax->id}}">{{$tax->name}}</option>
                                                 @endforeach
                                             </select>
                                         </td>
                                         <td class="text-right" style="vertical-align: middle;">
                                             <span id="item-total-{{ $item_row }}">0</span>
                                             {{-- <input type="text" id="item-total-{{ $item_row }}" value="item-total-{{ $item_row }}" > --}}
                                         </td>
                                     </tr>
                                     <?php $item_row++; ?>
                                     <tr id="addItem">
                                         <td class="text-center"><button type="button" onclick="addItem();" data-toggle="tooltip" title="" class="btn btn-xs btn-purple" data-original-title=""><i class="fa fa-plus"></i></button></td>
                                         <td class="text-right" colspan="5"></td>
                                     </tr>
                                     <tr>
                                         <td class="text-right" colspan="5"><strong>Subtotal</strong></td>
                                         <td class="text-right"><span id="sub-total">0</span></td>
                                     </tr>
                                     <tr>
                                         <td class="text-right" colspan="5"><strong>Tax</strong></td>
                                         <td class="text-right"><span id="tax-total">0</span></td>
                                     </tr>
                                     <tr>
                                         <td class="text-right" colspan="5"><strong>Total</strong></td>
                                         <td class="text-right">
                                             <span id="grand-total">0</span>

                                         </td>
                                     </tr>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                         <div class="form-group col-md-12">
                             <label>Notes</label>
                             <textarea class="form-control" name="notes"></textarea>
                         </div>


                     </div>
                     <!-- /.box-body -->

                     <div class="box-footer">
                        <div class="form-group">
                            @if(empty($company_bank_account))
                                <a data-toggle="modal" href='#modal-id' class="btn btn-purple" target="_blank">Add Bank Account</a>
                                <a href="{!! route('finance.invoice.index') !!}" class="btn btn-default">Cancel</a>

                                
                            @else
                                {!! Form::submit('Save', ['class' => 'btn btn-purple']) !!}
                                <a href="{!! route('finance.invoice.index') !!}" class="btn btn-default">Cancel</a>
                            @endif
                        </div>
                    </div>

                    
                     <!-- /.box-footer -->

                     {!! Form::close() !!}
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
               </div>
               <hr/ style="clear: both">
               <div class="box-body">
               </div>
            </div>
         </div>
      </div>
    </div>

    <div class="modal fade" id="modal-id">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Bank Account</h4>
                    </div>
                    <div class="modal-body">
                    <form action="{{url('finance/bank/add_bank_account')}}" method="post">
                        {{csrf_field()}}
                                <div class="form-group">
                                    <label for="input" class=" control-label">Banks:</label>
                                    <div class="">
                                        <select name="account_name" id="input" class="form-control" required="required">
                                            <option value="">Select Bank</option>
                                            @foreach($banks as $bank)
                                                <option value="{{$bank->name}}">{{$bank->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input" class=" control-label">Account Number:</label>
                                    <div class="">
                                        <input class="form-control" type="number" name="account_number" value="Enter account number in digits">
                                    </div>
                                </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-success" value="Save Account"/>
                    </div>
            </form>

                </div>

            </div>
        </div>
    </section>
@endsection


@section('script')
    <script type="text/javascript">
        var item_row = '{{ $item_row }}';

        function addItem() {
            html  = '<tr id="item-row-' + item_row + '">';
            html += '  <td class="text-center" style="vertical-align: middle;">';
            html += '      <button type="button" onclick=" $(\'#item-row-' + item_row + '\').remove(); totalItem();" data-toggle="tooltip" title="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>';
            html += '  </td>';
            html += '  <td>';
            html += '      <input class="form-control typeahead" required="required" placeholder="Enter Name" name="item[' + item_row + '][name]" type="text" id="item-name-' + item_row + '">';
            html += '      <input name="item[' + item_row + '][item_id]" type="hidden" id="item-id-' + item_row + '">';
            html += '  </td>';
            html += '  <td>';
            html += '      <input onkeyup="totalItem()" class="form-control text-center" required="required" name="item[' + item_row + '][quantity]" type="text" id="item-quantity-' + item_row + '">';
            html += '  </td>';
            html += '  <td>';
            html += '      <input onkeyup="totalItem()" class="form-control text-right" required="required" name="item[' + item_row + '][price]" type="text" id="item-price-' + item_row + '">';
            html += '  </td>';
            html += '  <td>';
            html += '      <select onchange="totalItem()" class="form-control select2" name="item[' + item_row + '][tax_id]" id="item-tax-' + item_row + '">';
           // html += '         <option selected="selected" value="">--Please select--</option>';
            @foreach($taxes as $tax)
                html += '         <option value="">--Please select--</option>';
                html += '         <option value="{{ $tax->id }}">{{ $tax->name }}</option>';
            @endforeach
                html += '      </select>';
            html += '  </td>';
            html += '  <td class="text-right" style="vertical-align: middle;">';
            html += '      <span id="item-total-' + item_row + '">0</span>';
            //html += ' <input type="text" id="item-total-' + item_row + '" value="item-total-' + item_row + '">';
            html += '  </td>';

            $('#items tbody #addItem').before(html);
            //$('[rel=tooltip]').tooltip();

            $('[data-toggle="tooltip"]').tooltip('hide');

            $('#item-row-' + item_row + ' .select2').select2({
                placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.taxes', 1)]) }}"
            });

            item_row++;
        }

        $(document).ready(function(){
            //Date picker
            $('#invoiced_at').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });

            //Date picker
            $('#due_at').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });

            $(".select2").select2({
                placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.taxes', 1)]) }}"
            });

            $("#customer_id").select2({
                placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.customers', 1)]) }}"
            });

            $("#currency_code").select2({
                placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.currencies', 1)]) }}"
            });


            var autocomplete_path = "{{ url('items/items/autocomplete') }}";

            $(document).on('click', '.form-control.typeahead', function() {
                input_id = $(this).attr('id').split('-');

                item_id = parseInt(input_id[input_id.length-1]);

                $(this).typeahead({
                    minLength: 3,
                    displayText:function (data) {
                        return data.name;
                    },
                    source: function (query, process) {
                        $.ajax({
                            url: autocomplete_path,
                            type: 'GET',
                            dataType: 'JSON',
                            data: 'query=' + query + '&type=invoice&currency_code=' + $('#currency_code').val(),
                            success: function(data) {
                                return process(data);
                            }
                        });
                    },
                    afterSelect: function (data) {
                        $('#item-id-' + item_id).val(data.item_id);
                        $('#item-quantity-' + item_id).val('1');
                        $('#item-price-' + item_id).val(data.sale_price);
                        $('#item-tax-' + item_id).val(data.tax_id);

                        // This event Select2 Stylesheet
                        $('#item-tax-' + item_id).trigger('change');

                        $('#item-total-' + item_id).html(data.total);

                        totalItem();
                    }
                });
            });

            $(document).on('change', '.iop', function(){
                 //alert('kk')
                totalItem();
            });

            $(document).on('keyup', '#items tbody .form-control', function(){
                 //alert('is');
                totalItem();
                //alert('get total');
            });

            $(document).on('change', '#customer_id', function (e) {
                $.ajax({
                    url: '{{ url("incomes/customers/currency") }}',
                    type: 'GET',
                    dataType: 'JSON',
                    data: 'customer_id=' + $(this).val(),
                    success: function(data) {
                        $('#currency_code').val(data.currency_code);

                        // This event Select2 Stylesheet
                        $('#currency_code').trigger('change');
                    }
                });
            });
        });

        function totalItem() {
            // alert('tgrthrthrh');
            $.ajax({
                url: '/finance/items/totalItemsAjax',
                type: 'POST',
                dataType: 'JSON',
                data: $('#items input[type=\'text\'],#items input[type=\'hidden\'], #items textarea, #items select'),

                success: function(data) {
                    console.log(data);
                    if (data) {
                        $.each(data.mydata.items, function (key, value) {

                            //$('#item-total-' + key).html(value);
                            console.log(key);
                            console.log(value);
                            console.log('#item-total-' + key);
                            $('#item-total-' + key).text(value);
                        });
                        $('#sub-total').text(data.mydata.sub_total);
                        $('#tax-total').text(data.mydata.tax_total);
                        $('#grand-total').text(data.mydata.grand_total);
                    }

                }, error:function (xhr, ajaxOptions, thrownError){
                }
            });
        }
    </script>


    @if(empty($company_bank_account))
        <script type="text/javascript">
            $('#modal-id').modal('show');
        </script>
    @endif
@endsection
