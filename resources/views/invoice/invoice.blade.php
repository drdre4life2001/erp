<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
</head>
<body style=" height: 100%;">
<div style=" position: relative;">
    <div style="width: 100%; /*overflow: auto;*/ position: relative;height: auto; margin-top: -10px;">
            <span style="float:left; !important; color:#af85f8; font-weight:bold; font-size:30px;">{{ ucfirst(auth()->user()->company->name) }}</span>
        <span style="float:right;">
            <h3 style="color:#af85f8 !important;">INVOICE</h3>
            {{-- <h2 style="color:#af85f8 !important;">Invoice Bill For : {{ isset($details[0]->client) ? ucfirst($details[0]->client) : "CUSTOMER INVOICE"  }}</h2> --}}
            <br>
            <span>Invoice Number : {{ $details[0]->invoice_number }}</span> <br>
            <span>Purchase Order (P.O) Number  : {{ $details[0]->order_number }}</span> <br>
            <span>Invoice Date : {{ date('d-m-Y', strtotime($details[0]->invoice_date)) }}</span> <br>
            <span>Due Date : {{ date('d-m-Y', strtotime($details[0]->due_date)) }}</span> <br>

        </span>
        <br> <br>
        <br> <br>
        <br> <br>
        <br> <br>
        <br> <br>

        <center><h4>INVOICE DETAILS</h4></center>
        <table class="table table-bordered table-hover">
            <thead class="thead-default">
            
            <tr>
                <th>Name         </th>
                <th>Quantity     |</th>
                <th>Price        |</th>
                <th>Total        </th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td> {{ $item->name }}</td>
                    <td> {{ $item->quantity }}</td>
                    <td> {{ number_format($item->price, 2) }}</td>
                    <td> {{ number_format($item->total, 2) }}</td>
                </tr>
            @endforeach
            
                <tr >
                    <td><h5 ><b style="color:#af85f8;">Sub Total : </b></h5></td>
                    <td><h5><b> {{ number_format($details[0]->sub_total, 2) }}</b>
                        </h5></td>
                </tr>
                
                <tr >
                    <td><h5 ><b style="color:#af85f8;">Tax : </b></h5></td>
                    <td><h5><b> {{ number_format($details[0]->tax, 2) }}</b>
                        </h5></td>
                </tr>
    
                <tr >
                    <td><h5 ><b style="color:#af85f8;">Grand Total : </b></h5></td>
                    <td><h5><b> {{ number_format($details[0]->grand_total, 2) }}</b>
                        </h5></td>
                </tr>
    
            
            </tbody>
        </table>
    </div>
</div>

<div style="
    position:fixed;
    float: left !important;
    bottom:0%;
   ">
    <b style="color:#af85f8;">Invoice Note :</b> {{ $details[0]->notes }}
</div>


<div style="position:fixed;float: right !important;bottom:0px;color:#af85f8;">
    <b>Date Downloaded :</b> {{ date("M d, Y",strtotime(date("Y-m-d"))) }}
</div>
</body>
</html>









