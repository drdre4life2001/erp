@extends('layouts.payslip')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        @foreach($details as $detail)
                            <h6 class="panel-title txt-dark">Dear {{ $detail->client }}, </h6>
                        @endforeach
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <!-- Section:Biography -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-block text-xs-left">
                                    <h3 class="card-title" style="color:#009688"><i
                                                class="fa fa-calendar fa-fw"></i> </h3>
                                    <p>

                                        Please find attached an invoice <br>
                                        Please review and get back to us within the stipulated time.

                                    </p>
                                    <p>
                                        Best Regards, Tech Advance.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection