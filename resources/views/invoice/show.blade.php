@extends('erp.layouts.master')
  
  @section('title')
    Finance - Invoice Details
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

@section('content')
    <section class="content-header">
    <h1>
      Invoice
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Invoice</li>
    </ol>
    </section>
    <section class="content">
    <div class="col-md-12">
      <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Invoice
               </b></a>
            </li>
         </ul>
         <div class="tab-content" style="padding: 2%">
            <div class="tab-pane active" id="tab_1">
               <p align="right">
                  <a href="{!! route('finance.invoice.create') !!}" class="btn btn-large btn-purple cdb"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add  Invoice
                  </a>
               </p>
               <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
                     {{-- @include('erp.hr.partials.employee_loan_form') --}}
                  <div class="col-md-1 hidden-sm hidden-xs"></div>
               </div>
               <hr/ style="clear: both">
               <div class="box-body">
                  <div class="">
                     @foreach($details as $detail)
                             <div class="row " style="margin-bottom: 10px;">
                                 <div class="col-md-6">
                                     <!--DO nothing -->
                                 </div>
                                 <div class="col-md-3 col-md-offset-3">
                                    <span style="color:fuchsia;"> Subsidiary: </span> {{$detail->subsidiary}}
                                 </div>
                             </div>

                             <div class="row" style="margin-bottom: 10px;">
                                 <div class="col-md-6">
                                        <span style="color:fuchsia;"> Bill To: </span><br>
                                     {{$detail->client}}
                                 </div>
                                 <div class="col-md-3 col-md-offset-3">
                                     <?php
                                     $invoice_date = date('d-m-Y', strtotime($detail->invoice_date));
                                     $due_date = date('d-m-Y', strtotime($detail->due_date));
                                     ?>
                                     Invoice Number: {{$detail->invoice_number}} <br>
                                     Invoice Date: {{$invoice_date}} <br>
                                     Payment Due: {{$due_date}}
                                     Revenue Stream : {{$detail->revenue_stream_name}}
                                 </div>
                             </div>
                             <div class="row" >
                                 <table class="table table-bordered" id="items">
                                     <thead>
                                     <tr style="background-color: #f9f9f9;">
                                         <th width="40%" class="text-left">Name</th>
                                         <th width="5%" class="text-center">Quantity</th>
                                         <th width="10%" class="text-right">Price</th>
                                         <th width="10%" class="text-right">Total</th>
                                     </tr>
                                     </thead>
                                     <tbody>
                                         @foreach($items as $item)
                                             <tr>
                                                 <td>
                                                         {{$item->name}}
                                                 </td>
                                                 <td>
                                                        <center>
                                                                {{$item->price}}
                                                        </center>
                                                 </td>
                                                 <td>
                                                    <center>
                                                            {{$item->price}}
                                                    </center>
                                                </td>
                                                 <td>
                                                     <center>
                                                            {{number_format($item->total, 2)}}
                                                     </center>
                                                 </td>
                                             </tr>
                                         @endforeach
                                     </tbody>
                                 </table>
                             </div>
                             <div class="row " style="margin-bottom: 10px;">
                                 <div class="col-md-6">
                                     <!--DO nothing -->
                                 </div>
                             </div>
                             <div class="row">
                                        <div class="col-lg-4 well">
                                            <div class="card">
                                                <h4 style="color:#a17be5;">
                                                <b>{{strtoupper(@$company_bank_account->bank->name)}}</b>  
                                                </h4> <br>
                                                <span style="color:#ccc;"> ACCOUNT NUMBER</span><br>
                                                <b>{{ @$company_bank_account->account_number }}</b>
            
                                                <br> <br>
                                                <span style="color:#ccc;"> ACCOUNT NAME</span><br>
                                                <b>{{strtoupper(@$company_bank_account->account_name)}}</b>`
                                            </div>
                                        </div> 
                                        <div class="col-lg-4 col-lg-offset-4 well">
                                            <div class="card">
                                                    <h4 style="color:#a17be5;">
                                                            <b>INVOICE DETAILS</b>  
                                                        </h4> <br>
                                                    Subtotal: {{number_format($detail->sub_total, 2)}} <br>
                                                    Tax: {{number_format($detail->tax, 2)}}<br>
                                                    Total: {{number_format($detail->grand_total,2 )}} <br>
                                                    Paid: {{number_format($detail->paid, 2)}} <br>
                                                    Balance: {{number_format($detail->balance, 2)}} <br>
                                                    Status: <span class="badge @if($detail->status == "pending") btn-warning @elseif($detail->status == "completed") btn-success @endif ">{{$detail->status}}</span><br>
                                            </div>
                                        </div>
                             </div> <hr>
                             <div class="row ">
                                <div class="text-justify well">
                                    {{$detail->notes}}
                                </div>
                             </div>
                             <div class="row">

                                

                                 {{-- Only edit when payment has not been made --}}
                                 @if($detail->status_id == 1)
                                 <a href="{!! route('finance.invoice.edit', [$detail->id]) !!}" class="btn btn-purple"><i class="fa fa-pencil"></i>Edit</a>
                                 @endif
                                 
                                 {{-- <a class="btn btn-success"> <i class="fa fa-download"></i> Download as PDF</a> --}}
                                 <a class="btn btn-success" href="{{ url('finance/invoice/print_pdf/'.$detail->id.'/'.auth()->user()->company->uri.'/download') }}" <i class="fa fa-print"></i> Download & Print PDF</a>

                                 <a  data-toggle="modal" data-target="#myModal" class="btn btn-default">Send Mail</a>
                                 <div id="myModal" class="modal fade" role="dialog">
                                     <div class="modal-dialog">

                                         <!-- Modal content-->
                                         <div class="modal-content">
                                             <div class="modal-header">
                                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                 <h4 class="modal-title">Send Mail</h4>
                                             </div>
                                             <form action="{{url('finance/invoice/sendPdf/'.$detail->id)}}" method="post">
                                                 {{ csrf_field() }}
                                             <div class="modal-body">
                                                 <input type="text" name="email" class="form-control" placeholder="E.g. info@techadvance.ng">
                                             </div>
                                             <div class="modal-footer">
                                                 <input type="submit" value="Send Mail" class="btn btn-purple" >
                                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                             </div>
                                             </form>
                                         </div>

                                     </div>
                                 </div>
                                 {{--
                                <a href="{{url('finance/invoice/printPdf/'.$detail->id) }}" class="btn btn-success">Download PDF</a>
                                --}}
                                 @if($detail->status_id != 3)
                                 <button class="btn btn-default" data-toggle="modal" data-target="#myModalPayment"><i class="fa fa-plus"></i>Add Payment</button>
                                 @endif
                                 <!--Modal starts -->
                                 <div id="myModalPayment" class="modal fade" role="dialog">
                                     <div class="modal-dialog">

                                         <!-- Modal content-->
                                         <div class="modal-content">
                                             <div class="modal-header">
                                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                 <h4 class="modal-title">Add Payment</h4>
                                             </div>
                                             <form action="{{url('finance/invoice/payment')}}" method="post">
                                                 <div class="modal-body">
                                                     <div class="form-group col-md-12">
                                                         <div class="col-md-6">
                                                             <label>Date</label>
                                                             <input type="text" id="payment_date" name="payment_date" class="form-control" placeholder="Enter Payment Date" >
                                                             <input type="hidden" name="invoice_id" value="{{$detail->id}}">
                                                             <input type="hidden" name="balance" value="{{$detail->balance}}">
                                                             <input type="hidden" name="revenue_stream_id" value="{{$detail->revenue_stream_id}}">
                                                             <input type="hidden" name="client_id" value="{{$detail->client_id}}">
                                                             <input type="hidden" name="subsidiary_id" value="{{$detail->company_id}}">


                                                         </div>
                                                         <div class="col-md-6">
                                                             <label>Amount</label>
                                                         <input type="number" value="{{old('amount')}}"  name="amount" class="form-control" placeholder="Enter Amount" >
                                                             <p><em>Amount can not be greater than {{number_format($detail->balance, 2)}}</em></p>
                                                         </div>
                                                     </div>
                                                     <div class="form-group col-md-12">
                                                         <div class="col-md-6">
                                                             <label>Bank Account</label>
                                                             <select name="bank_account" class="form-control">
                                                                 @if(isset($banks))
                                                                     @foreach($banks as $bank)
                                                                     <option value="{{$bank->id}}">{{$bank->name}}[{{$bank->account_number}}]</option>
                                                                     @endforeach
                                                                 @endif
                                                             </select>
                                                         </div>
                                                         <div class="col-md-3 col-md-offset-3">

                                                         </div>
                                                     </div>
                                                     <div class="form-group col-md-12">
                                                         <div class="col-md-12">
                                                             <label>Description</label>
                                                             <textarea class="form-control" name="description"></textarea>
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <div class="modal-footer">
                                                     <input type="submit" class="btn btn-success"  value="Add">
                                                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                 </div>
                                             </form>
                                         </div>

                                     </div>
                                 </div>
                                 <!-- Modal ends -->
                             </div>
                             @endforeach
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        $('#payment_date').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
        $('#print').click(function () {
            $("#main").print({
                //Use Global styles
                globalStyles : false,
                //Add link with attrbute media=print
                mediaPrint : false,
                //Custom stylesheet
                stylesheet : "http://fonts.googleapis.com/css?family=Inconsolata",
                //Print in a hidden iframe
                iframe : false,
                //Don't print this
                noPrintSelector : ".avoid-this",
                 //Log to console when printing is done via a deffered callback
                deferred: $.Deferred().done(function() { console.log('Printing done', arguments); })
            });
        });
    </script>
@endsection

