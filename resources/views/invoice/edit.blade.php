@extends('erp.layouts.master')
  
  @section('title')
    Finance - Invoice
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

@section('content')
    <!-- Default box -->
    <div class="box box-success">
            @if(isset($errors))
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endForeach
                        </ul>
                        @endIf
                    </div>
                @endIf
            <section class="content">
               <div class="col-md-12">
                   <div class="nav-tabs-custom">
                      <ul class="nav nav-tabs">
                         <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b>Invoice</b></a></li>
                      </ul>
                      <div class="tab-content" style="padding: 2%">
                         <div class="tab-pane active" id="tab_1">
                            <p align="right">
                               <a class="btn btn-large btn-purple cdb" href="{!! route('finance.invoice.create') !!}" style="color:white"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Invoice</a>
                            </p>
                            <div class="adf" style="background:#ffffff; float: left; width: 100%; padding: 3% ">
                               @foreach($details as $detail)
                                   <form action="{{url('finance/invoice/update/'.$detail->id)}}" method="post">
                                       <div class="box-body">
                                           <div class="form-group col-md-12">

                                               <div class="col-md-4">
                                                   <label>Select Revenue Stream</label>
                                                   <select name="revenue_stream" class="form-control" required>
                                                       <option value="">--Please select--</option>
                                                       @foreach($revenue_streams as $revenue_stream)
                                                           <option value="{{$revenue_stream->id}}"
                                                           <?php if($detail->revenue_stream_id == $revenue_stream->id){ echo 'selected'; } ?>
                                                           >{{$revenue_stream->name}}</option>
                                                       @endforeach
                                                   </select>
                                               </div>
                                               {{--
                                               <div class="col-md-4">
                                                   <label>Select Subsidiary</label>
                                                   <select name="subsidiary" class="form-control" required>
                                                       <option value="">--Please select--</option>
                                                       @foreach($subsidiaries as $subsidiary)
                                                           <option value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                                       @endforeach
                                                   </select>
                                               </div>
                                               --}}
                                               <div class="col-md-4 col-md-offset-4" >
                                                   <label>Invoice Date</label>
                                                   <input type="text" value="{{$detail->invoice_date}}" id="invoiced_at" name="invoice_date" class="form-control" required>
                                               </div>

                                           </div>
                                           <div class="form-group col-md-12">
                                               <div class="col-md-4">

                                                   <label>Due Date</label>
                                                   <input type="text" value="{{$detail->due_date}}" id="due_at" name="due_date" class="form-control" required>
                                               </div>
                                               <div class="col-md-4">
                                                   <label>Invoice Number</label>
                                                   <input type="text" value="{{$detail->invoice_number}}" name="invoice_number" class="form-control" placeholder="Enter Invoice Number" required>
                                               </div>
                                               <div class="col-md-4">
                                                   <label>Order Number</label>
                                                   <input type="text" value="{{$detail->order_number}}" name="order_number" class="form-control" placeholder="Enter Order Number" required>
                                               </div>

                                           </div>
                                           @endforeach

                                           <div class="form-group col-md-12">
                                               {!! Form::label('items', 'Items', ['class' => 'control-label']) !!}
                                               <div class="table-responsive">
                                                   <table class="table table-bordered well table-bordered" id="items">
                                                       <thead>
                                                       <tr style="background-color: #f9f9f9;">
                                                           <th width="5%"  class="text-center">Actions</th>
                                                           <th width="40%" class="text-left">Name</th>
                                                           <th width="5%" class="text-center">Quantity</th>
                                                           <th width="10%" class="text-right">Price</th>
                                                           <th width="15%" class="text-right">Tax</th>
                                                           <th width="10%" class="text-right">Total</th>
                                                       </tr>
                                                       </thead>
                                                       <tbody>
                                                       <?php $item_row = 0; ?>
                                                       <!-- Fetch all items on this invoice -->
                                                       @foreach($items as $item)
                                                           <tr id="item-row-{{ $item_row }}">
                                                               <td class="text-center" style="vertical-align: middle;">
                                                                   <button type="button" onclick="$(this).tooltip('destroy'); $('#item-row-{{ $item_row }}').remove(); totalItem();" data-toggle="tooltip" title="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                               </td>
                                                               <td>
                                                                   <input class="form-control typeahead" required="required" placeholder="Enter name" value="{{$item->name}}" name="item[{{ $item_row }}][name]" type="text" id="item-name-{{ $item_row }}">
                                                                   <input  name="item[{{ $item_row }}][item_id]" type="text" value="{{$item->id}}" id="item-id-{{ $item_row }}" >
                                                               </td>
                                                               <td>
                                                                   <input  class="form-control text-center"  required="required" name="item[{{ $item_row }}][quantity]" value="{{$item->quantity}}" onkeyup="totalItem()" type="text" id="item-quantity-{{ $item_row }}">

                                                               </td>
                                                               <td>
                                                                   <input class="form-control text-right" required="required" name="item[{{ $item_row }}][price]" value="{{$item->price}}"  onkeyup="totalItem()" type="text" id="item-price-{{ $item_row }}">
                                                               </td>
                                                               <td>
                                                                   <select name="item[{{ $item_row }}][tax_id]" class="form-control iop" id='item-tax-'. {{$item_row}} onchange="totalItem()" >
                                                                       <option value="">--Please select--</option>
                                                                       @foreach($taxes as $tax)
                                                                           <option value="{{$tax->id}}"
                                                                           <?php if($tax->id == $item->tax_id){ echo 'selected'; } ?>
                                                                           >{{$tax->name}}</option>
                                                                       @endforeach
                                                                   </select>
                                                               </td>
                                                               <td class="text-right" style="vertical-align: middle;">
                                                                   <span id="item-total-{{ $item_row }}">0</span>
                                                                   {{-- <input type="text" id="item-total-{{ $item_row }}" value="item-total-{{ $item_row }}" > --}}
                                                               </td>
                                                           </tr>
                                                           <?php $item_row++; ?>
                                                       @endforeach
                                                       <!-- ends -->
                                                       @if(empty($items))
                                                       <tr id="item-row-{{ $item_row }}">
                                                           <td class="text-center" style="vertical-align: middle;">
                                                               <button type="button" onclick="$(this).tooltip('destroy'); $('#item-row-{{ $item_row }}').remove(); totalItem();" data-toggle="tooltip" title="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                           </td>
                                                           <td>
                                                               <input class="form-control typeahead" required="required" placeholder="Enter name" name="item[{{ $item_row }}][name]" type="text" id="item-name-{{ $item_row }}">
                                                               <input  name="item[{{ $item_row }}][item_id]" type="text" id="item-id-{{ $item_row }}" >
                                                           </td>
                                                           <td>
                                                               <input  class="form-control text-center"  required="required" name="item[{{ $item_row }}][quantity]" onkeyup="totalItem()" type="text" id="item-quantity-{{ $item_row }}">

                                                           </td>
                                                           <td>
                                                               <input class="form-control text-right" required="required" name="item[{{ $item_row }}][price]" onkeyup="totalItem()" type="text" id="item-price-{{ $item_row }}">
                                                           </td>
                                                           <td>
                                                               <select name="item[{{ $item_row }}][tax_id]" class="form-control iop" id='item-tax-'. {{$item_row}} onchange="totalItem()">
                                                                   <option value="">--Please select--</option>
                                                                   @foreach($taxes as $tax)
                                                                       <option value="{{$tax->id}}">{{$tax->name}}</option>
                                                                   @endforeach
                                                               </select>
                                                           </td>
                                                           <td class="text-right" style="vertical-align: middle;">
                                                               <span id="item-total-{{ $item_row }}">0</span>
                                                               {{-- <input type="text" id="item-total-{{ $item_row }}" value="item-total-{{ $item_row }}" > --}}
                                                           </td>
                                                       </tr>
                                                       @endif
                                                       <?php $item_row++; ?>
                                                       <tr id="addItem">
                                                           <td class="text-center"><button type="button" onclick="addItem();" data-toggle="tooltip" title="" class="btn btn-xs btn-primary" data-original-title=""><i class="fa fa-plus"></i></button></td>
                                                           <td class="text-right" colspan="5"></td>
                                                       </tr>
                                                       <tr>
                                                           <td class="text-right" colspan="5"><strong>Subtotal</strong></td>
                                                           <td class="text-right"><span id="sub-total">0</span></td>
                                                       </tr>
                                                       <tr>
                                                           <td class="text-right" colspan="5"><strong>Tax</strong></td>
                                                           <td class="text-right"><span id="tax-total">0</span></td>
                                                       </tr>
                                                       <tr>
                                                           <td class="text-right" colspan="5"><strong>Total</strong></td>
                                                           <td class="text-right">
                                                               <span id="grand-total">0</span>

                                                           </td>
                                                       </tr>
                                                       </tbody>
                                                   </table>
                                               </div>
                                           </div>
                                           <div class="form-group col-md-12">
                                               <label>Notes</label>
                                               @foreach($details as $detail)
                                                   <textarea class="form-control" name="notes">{{$detail->notes}}</textarea>
                                               @endforeach
                                           </div>


                                       </div>
                                       <!-- /.box-body -->

                                       <div class="box-footer">
                                           <div class="form-group">
                                               <input type="submit" class="btn btn-primary" value="Save">
                                               <a href="{!! route('finance.invoice.index') !!}" class="btn btn-default">Cancel</a>
                                           </div>
                                       </div>
                                   </form>
                           </div>
                            <hr/ style="clear: both">
                            
                         </div>
                      </div>
                      <!-- /.tab-content -->
                   </div>
               </div>
               <!-- /.col -->
            </section>
    </div>
@endsection


@section('script')
    <script type="text/javascript">
        var item_row = '{{ $item_row }}';

        function addItem() {
            html  = '<tr id="item-row-' + item_row + '">';
            html += '  <td class="text-center" style="vertical-align: middle;">';
            html += '      <button type="button" onclick=" $(\'#item-row-' + item_row + '\').remove(); totalItem();" data-toggle="tooltip" title="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>';
            html += '  </td>';
            html += '  <td>';
            html += '      <input class="form-control typeahead" required="required" placeholder="Enter Name" name="item[' + item_row + '][name]" type="text" id="item-name-' + item_row + '">';
            html += '      <input name="item[' + item_row + '][item_id]" type="hidden" id="item-id-' + item_row + '">';
            html += '  </td>';
            html += '  <td>';
            html += '      <input onkeyup="totalItem()" class="form-control text-center" required="required" name="item[' + item_row + '][quantity]" type="text" id="item-quantity-' + item_row + '">';
            html += '  </td>';
            html += '  <td>';
            html += '      <input onkeyup="totalItem()" class="form-control text-right" required="required" name="item[' + item_row + '][price]" type="text" id="item-price-' + item_row + '">';
            html += '  </td>';
            html += '  <td>';
            html += '      <select onchange="totalItem()" class="form-control select2" name="item[' + item_row + '][tax_id]" id="item-tax-' + item_row + '">';
            // html += '         <option selected="selected" value="">--Please select--</option>';
            @foreach($taxes as $tax)
                html += '         <option value="">--Please select--</option>';
            html += '         <option value="{{ $tax->id }}">{{ $tax->name }}</option>';
            @endforeach
                html += '      </select>';
            html += '  </td>';
            html += '  <td class="text-right" style="vertical-align: middle;">';
            html += '      <span id="item-total-' + item_row + '">0</span>';
            //html += ' <input type="text" id="item-total-' + item_row + '" value="item-total-' + item_row + '">';
            html += '  </td>';

            $('#items tbody #addItem').before(html);
            //$('[rel=tooltip]').tooltip();

            $('[data-toggle="tooltip"]').tooltip('hide');

            $('#item-row-' + item_row + ' .select2').select2({
                placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.taxes', 1)]) }}"
            });

            item_row++;
        }

        $(document).ready(function(){
            totalItem();
            //Date picker
            $('#invoiced_at').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });

            //Date picker
            $('#due_at').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });

            $(".select2").select2({
                placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.taxes', 1)]) }}"
            });

            $("#customer_id").select2({
                placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.customers', 1)]) }}"
            });

            $("#currency_code").select2({
                placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.currencies', 1)]) }}"
            });


            var autocomplete_path = "{{ url('items/items/autocomplete') }}";

            $(document).on('click', '.form-control.typeahead', function() {
                input_id = $(this).attr('id').split('-');

                item_id = parseInt(input_id[input_id.length-1]);

                $(this).typeahead({
                    minLength: 3,
                    displayText:function (data) {
                        return data.name;
                    },
                    source: function (query, process) {
                        $.ajax({
                            url: autocomplete_path,
                            type: 'GET',
                            dataType: 'JSON',
                            data: 'query=' + query + '&type=invoice&currency_code=' + $('#currency_code').val(),
                            success: function(data) {
                                return process(data);
                            }
                        });
                    },
                    afterSelect: function (data) {
                        $('#item-id-' + item_id).val(data.item_id);
                        $('#item-quantity-' + item_id).val('1');
                        $('#item-price-' + item_id).val(data.sale_price);
                        $('#item-tax-' + item_id).val(data.tax_id);

                        // This event Select2 Stylesheet
                        $('#item-tax-' + item_id).trigger('change');

                        $('#item-total-' + item_id).html(data.total);

                        totalItem();
                    }
                });
            });

            $(document).on('change', '.iop', function(){
                //alert('kk')
                totalItem();
            });

            $(document).on('keyup', '#items tbody .form-control', function(){
                //alert('is');
                totalItem();
                //alert('get total');
            });

            $(document).on('change', '#customer_id', function (e) {
                $.ajax({
                    url: '{{ url("incomes/customers/currency") }}',
                    type: 'GET',
                    dataType: 'JSON',
                    data: 'customer_id=' + $(this).val(),
                    success: function(data) {
                        $('#currency_code').val(data.currency_code);

                        // This event Select2 Stylesheet
                        $('#currency_code').trigger('change');
                    }
                });
            });
        });

        function totalItem() {
            //alert('tgrthrthrh')
            $.ajax({
                url: '{{ url("finance/items/totalItem") }}',
                type: 'POST',
                dataType: 'JSON',
                data: $('#items input[type=\'text\'],#items input[type=\'hidden\'], #items textarea, #items select'),
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },

                success: function(data) {
                    console.log(data);
                    if (data) {
                        $.each(data.mydata.items, function (key, value) {

                            //$('#item-total-' + key).html(value);
                            console.log(key);
                            console.log(value);
                            console.log('#item-total-' + key);
                            $('#item-total-' + key).text(value);
                        });
                        $('#sub-total').text(data.mydata.sub_total);
                        $('#tax-total').text(data.mydata.tax_total);
                        $('#grand-total').text(data.mydata.grand_total);
                    }

                }, error:function (xhr, ajaxOptions, thrownError){
                    alert("Error : "+thrownError);
                }
            });
        }


    </script>
@endsection


@section('script')
 <script type="text/javascript">
     $("#example1").DataTable();

   $('.adf').hide();
   $('.cdb').on('click', function(){
     $('.adf').slideToggle();
   });
   @if ($errors->has('employee_id') || $errors->has('amount') || $errors->has('interest') || $errors->has('tenure') || $errors->has('description'))
     $('.adf').slideToggle();
   @endif

     $('.cadfxx').on('click', function(){
        $('.adf').slideToggle();
     });
 </script>
@endsection
