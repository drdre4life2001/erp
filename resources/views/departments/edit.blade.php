

@extends('erp.layouts.master')
  @section('title')
    Human Resource - Departments
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
      <section class="content-header">
         <h1>
            Department
         </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Department</li>
         </ol>
      </section>
      <section class="content">
         <div class="col-md-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Department
                     </b></a>
                  </li>
               </ul>
               <div class="tab-content" style="padding: 2%">
                  <div class="" id="">
                     <p align="right">
                        <a href="{{url('departments')}}" class="btn btn-large btn-purple"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Department
                        </a>
                     </p>
                     <div class="" style="">
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                        <div class="col-md-10">
                            @if(isset($errors))
                                @if(count($errors) > 0)
                                    <div class="alert alert-info">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{$error}} here</li>
                                                @endForeach
                                        </ul>
                                        @endIf
                                    </div>
                            @endIf
                            <form action="{{ url('departments/edit', $dept->id) }}" method="POST">
                                                    <div class="form-group col-sm-6">
                                                    {!! Form::label('name', 'Name:') !!}
                                                    {!! Form::text('name', $dept->name, ['class' => 'form-control']) !!}

                                                    <!-- Code Field -->
                                                        <div class="form-group col-sm-6">
                                                            {!! Form::label('code', 'Code:') !!}
                                                            {!! Form::text('code', $dept->code,['readonly'], ['class' => 'form-control']) !!}

                                                        </div>

                                                        <!-- Id Sys Organizations Field -->
                                                        <div class="form-group col-sm-6">
                                                            {!! Form::label('id_sys_organizations', 'Organization:') !!}
                                                            <select name="id_sys_organizations" class="form-control">
                                                                <option value="0">Select Organization..</option>
                                                                @if(isset($organizations))
                                                                    @foreach($organizations as $organization)
                                                                        <option value="{{$organization->id}}">{{$organization->name}}</option>
                                                                    @endforeach
                                                                @endIf

                                                            </select>
                                                        </div>


                                                        <!-- Id Hr Employee Field -->
                                                        <div class="form-group col-sm-6">
                                                            {!! Form::label('id_hr_employee', 'Manager') !!}
                                                            <select name="id_hr_employee" class="form-control">
                                                                <option value="0">Select Departmental Head..</option>
                                                                @if(isset($employees))
                                                                    @foreach($employees as $employee)
                                                                        <option value="{{$employee->id}}"> {{$employee->identification_number}}
                                                                            [{{$employee->first_name.'  '.$employee->lastname}}]
                                                                        </option>
                                                                    @endforeach
                                                                @endIf
                                                            </select>
                                                        </div>

                                                        <!-- Id parent department -->
                                                        <div class="form-group col-sm-6">
                                                            {!! Form::label('parent_id', 'Parent Department') !!}
                                                            <select name="parent_id" class="form-control">
                                                                <option value="0">Select Parent Department..</option>
                                                                @if(isset($parentDepartments))
                                                                    @foreach($parentDepartments as $department)
                                                                        <option value="{{$department->id}}">{{$department->name}}
                                                                            [{{$department->code}}
                                                                            ]
                                                                        </option>
                                                                    @endforeach
                                                                @endIf
                                                            </select>
                                                        </div>

                                                        <!-- Description Field -->
                                                        <div class="form-group col-sm-6">
                                                            {!! Form::label('description', 'Description:') !!}
                                                            {!! Form::text('description', $dept->description, ['class' => 'form-control']) !!}
                                                        </div>

                                                        <!-- Submit Field -->
                                                        <div class="form-group col-sm-12">
                                                            {!! Form::submit('Save', ['class' => 'btn btn-purple']) !!}
                                                            <a href="{!! route('departments.index') !!}" class="btn btn-default">Cancel</a>
                                                        </div>
                                                    </div>
                                                </form>
                        </div>
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                     </div>
                     <hr/ style="clear: both">
                     <div class="box-body">
                     </div>
                  </div>
               </div>
               <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </section>
  @endsection
