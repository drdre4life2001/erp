@extends('erp.layouts.master')
  @section('title')
    Human Resource - Departments
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
      <section class="content-header">
         <h1>
            Department
         </h1>
         <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Department</li>
         </ol>
      </section>
      <section class="content">
         <div class="col-md-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Department
                     </b></a>
                  </li>
               </ul>
               <div class="tab-content" style="padding: 2%">
                  <div class="" id="">
                     <p align="right">
                        <a href="{{url('departments/create')}}" class="btn btn-large btn-purple"><i class="fa fa-plus" aria-hidden="true" style="color:white"></i>  Add Department
                        </a>
                     </p>
                     <div class="" style="">
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                        <div class="col-md-10">
                        </div>
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                     </div>
                     <hr/ style="clear: both">
                     <div class="box-body">
                        @include('departments.table')
                     </div>
                  </div>
               </div>
               <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </section>
  @endsection

  @section('script')
       <script type="text/javascript">
         
           $("#example1").DataTable();


         $('.adf').hide();
         $('.cdb').on('click', function(){
           $('.adf').slideToggle();
         });

         @if ($errors->has('company_id') || $errors->has('bank') || $errors->has('description') || $errors->has('interest_rate') || $errors->has('amount') || $errors->has('period'))
           $('.adf').slideToggle();
         @endif

           $('.cadfxx').on('click', function(){
              $('.adf').slideToggle();
           });

       </script>
  @endsection
