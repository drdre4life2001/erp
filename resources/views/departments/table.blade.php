<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">
<div class="panel-wrapper collapse in">
    <div class="panel-body">
        <div class="table-wrap">
            <div class="table-responsive" style="">
                <table id="example1" class="table table-hover table-bordered display mb-30">

                    <thead>
                    <th>Description</th>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Organization</th>
                    <th>Manager</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach($departments as $department)
                        <tr>
                            <td>{!! $department->description !!}</td>
                            <td>{!! $department->name !!}</td>
                            <td>{!! $department->code !!}</td>
                            <td>
                                @if(isset($department->organization_name))
                                    {{$department->organization_name}}
                                @endIf

                            </td>
                            <td>
                                @if(isset($department->line_manager_name))
                                    {{$department->line_manager_name}}
                                @endIf
                            </td>
                            <td >
                               {!! Form::open(['route' => ['departments.destroy', $department->id], 'method' => 'delete']) !!}
                                <div class='btn -group'>
                                    {{-- <a href="{!! route('departments.show', [$department->id]) !!}"
                                       ></a>
                                    <a href="{!! url('departments/edit', [$department->id]) !!}"
                                       class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>

                                       <a id="a_del" href="{!! url('departments/destroy', [$department->id]) !!}"
                                        class='btn btn-default btn-xs'><i class="fa fa-trash"></i></a> --}}
                                    <a href="{!! url('departments/edit', [$department->id]) !!}" >
                                        <span class="label label-warning" style="padding:6.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                        <i class="fa fa-edit"></i>  Edit </span></a>
                                 

                                        <a href="{{ url('trash/department/'. $department->id.'/'.auth()->user()->company->uri) }}" id="a_del">
                                        <span class="label label-danger" style="padding:6.2%; margin-top: 4% !important; float: left; margin-left:1%; border:1px solid #eee; color:#ddd">
                                        <i class="fa fa-trash"></i>  Delete </span>
                                    </a>
                                </div>
                                {!! Form::close() !!}
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
