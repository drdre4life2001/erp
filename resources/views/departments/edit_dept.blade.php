<div class="panel-body">
    <div class="row">
        <div class="col-sm-12 col-xs-12">
            <div class="form-wrap">
                <div class="row">
                    <!-- Name Field -->
                    <form action="{{ url('departments/edit', $dept->id) }}" method="POST">
                        <div class="form-group col-sm-6">
                    {!! Form::label('name', 'Name:') !!}
                    {!! Form::text('name', 'name', $dept->name, ['class' => 'form-control']) !!}

                    <!-- Code Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('code', 'Code:') !!}
                            {!! Form::text('code', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Id Sys Organizations Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_sys_organizations', 'Organization:') !!}
                            <select name="id_sys_organizations" class="form-control">
                                <option value="0">Select Organization..</option>
                                @if(isset($organizations))
                                    @foreach($organizations as $organization)
                                        <option value="{{$organization->id}}">{{$organization->name}}</option>
                                    @endforeach
                                @endIf

                            </select>
                        </div>


                        <!-- Id Hr Employee Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_hr_employee', 'Manager') !!}
                            <select name="parent_id" class="form-control">
                                <option value="0">Select Departmental Head..</option>
                                @if(isset($employees))
                                    @foreach($employees as $employee)
                                        <option value="{{$employee->id}}"> {{$employee->identification_number}}
                                            [{{$employee->first_name.'  '.$employee->lastname}}]
                                        </option>
                                    @endforeach
                                @endIf
                            </select>
                        </div>

                        <!-- Id parent department -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('parent_id', 'Parent Department') !!}
                            <select name="parent_id" class="form-control">
                                <option value="0">Select Parent Department..</option>
                                @if(isset($parentDepartments))
                                    @foreach($parentDepartments as $department)
                                        <option value="{{$department->id}}">{{$department->name}}
                                            [{{$department->code}}
                                            ]
                                        </option>
                                    @endforeach
                                @endIf
                            </select>
                        </div>

                        <!-- Description Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('description', 'Description:') !!}
                            {!! Form::text('description', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('departments.index') !!}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
