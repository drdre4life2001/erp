@extends('layouts.erp')

@section('page_title')
    <h5>Request Categories</h5>
@endsection

@section('content')
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                @if(isset($errors))
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}} here</li>
                                @endForeach
                            </ul>
                            @endIf
                        </div>
                        @endIf
                        {!! Form::open(['route' => 'requestCategories.store']) !!}

                        @include('request_categories.fields')

                        {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
