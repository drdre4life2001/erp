<table class="table table-responsive" id="requestCategories-table">
    <thead>
        <th>Name</th>
        <th>Parent Category</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($requestCategories as $requestCategory)
        <tr>
            <?php $name = \App\Models\RequestCategory::find($requestCategory->parent_id);?>

            <td>{!! $requestCategory->name !!}</td>
            <td>{!! !is_null($name) ? $name->name : '' !!}</td>
            <td>
                {!! Form::open(['route' => ['requestCategories.destroy', $requestCategory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {{--
                    <a href="{!! route('requestCategories.show', [$requestCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    --}}
                    <a href="{!! route('requestCategories.edit', [$requestCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>