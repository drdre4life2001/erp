<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::text('name', null, ['class' => 'form-control','placeholder'=> 'Name']) !!}
    <select class="form-control" name="parent_id">
        <option>Select Parent Category (if applicable)</option>
        @foreach($cats as $cat)
            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
        @endforeach
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('requestCategories.index') !!}" class="btn btn-default">Cancel</a>
</div>
