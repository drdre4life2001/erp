@extends('erp.layouts.master')
  
  @section('title')
    Request Category
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
    <section class="content-header">
       <h1>
        Request Category
       </h1>
       <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
          <li class="active"> Request Category</li>
       </ol>
    </section>
    <section class="content">
       <div class="col-md-12">
          <div class="nav-tabs-custom">
             <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-building"></i> <b> Request Category
                   </b></a>
                </li>
             </ul>
             <div class="tab-content" style="padding: 2%">
                <div class="tab-pane active" id="tab_1">
                   <p align="right">
                   </p>
                    <div class="adf" style="background:#ecf0f5; float: left; width: 100%; ">
                      <div class="col-md-1 hidden-sm hidden-xs"></div>
                        <div class="col-md-10">
                                @if(isset($errors))
                                 @if(count($errors) > 0)
                                     <div class="alert alert-danger">
                                         <ul>
                                             @foreach($errors->all() as $error)
                                                 <li>{{$error}}</li>
                                             @endForeach
                                         </ul>
                                         @endIf
                                     </div>
                                @endIf
                             {{ Form::model($data, array('route' => array('requestCategories.update', $data->id), 'method' => 'PUT')) }}
                                 <!-- Name Field -->
                                     <div class="form-group col-sm-6">
                                         <label class="control-label">Name</label>
                                          <input type = 'text' name="name" class = 'form-control' placeholder = 'Name' value = {{$data->name}} required>

                                         <label class="control-label">Parent Category</label>
                                         <select class="form-control" name="parent_id" required>
                                             <option value="">--Please select--</option>
                                             @foreach($cats as $cat)
                                             <option value="{{ $cat->id }}" <?php if($cat->id == $data->parent_id){ print 'selected'; }  ?>>{{ $cat->name }}</option>
                                         @endforeach
                                     </select>
                                 </div>

                                 <!-- Submit Field -->
                                 <div class="form-group col-sm-12">
                                     {!! Form::submit('Save', ['class' => 'btn btn-purple']) !!}
                                     <a href="{!! route('requestCategories.index') !!}" class="btn btn-default">Cancel</a>
                                 </div>
                             {!! Form::close() !!}
                          </div>
                      </div>
                    </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </section>
@endsection