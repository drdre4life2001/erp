@extends('layouts.erp')

@section('page_title')
<div>
<section>
<h4 class="pull-left">Expense Categories</h4>
<h2 class="pull-right">
    <a class="btn btn-primary " style="margin-top: -10px;margin-bottom: 5px" href="{!! route('requestCategories.create') !!}">Add New</a>
</h2>
    </section>
</div>
<div class="clearfix"></div>
@endsection

@section('content')
    <div class="content">
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('request_categories.table')
            </div>
        </div>
    </div>
@endsection

