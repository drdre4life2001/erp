@extends('erp.layouts.master')
  
  @section('title')
    Request Category
  @endsection

  @section('sidebar')
    @include('erp.partials.sidebar')
  @endsection

  @section('content')
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('request_categories.show_fields')
                    <a href="{!! route('requestCategories.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
  @endsection
@endsection


