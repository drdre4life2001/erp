@extends('layouts.app_hr')

@section('page_title')
    <h2>Edit Institution Departments</h2>
@endsection

@section('hr_content')
    <section class="content-header">
    </section>
    <div class="content">

        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    @if(isset($errors))
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}} here</li>
                                    @endForeach
                                </ul>
                                @endIf
                            </div>
                        @endIf
                    <form action="{{ url('institution_departments/edit', $institutionDepartment->id) }}" method="POST">
                        <!-- Name Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('name', 'Name:') !!}
                                {!! Form::text('name', $institutionDepartment->name, ['class' => 'form-control']) !!}
                            </div>

                            <!-- Description Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('description', 'Description:') !!}
                                {!! Form::text('description', $institutionDepartment->description, ['class' => 'form-control']) !!}
                            </div>


                            <!-- Submit Field -->
                            <div class="form-group col-sm-12">
                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                <a href="{!! route('institutionDepartments.index') !!}" class="btn btn-default">Cancel</a>
                            </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
