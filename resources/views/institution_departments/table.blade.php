<table class="table table-responsive" id="datable_2">
    <thead>
        <th>Name</th>
        <th>Description</th>
        <th>Action</th>
    </thead>
    <tbody>
    @foreach($institutionDepartments as $institutionDepartment)
        <tr>
            <td>{!! $institutionDepartment->name !!}</td>
            <td>{!! $institutionDepartment->description !!}</td>
            <td>
                {!! Form::open(['route' => ['institutionDepartments.destroy', $institutionDepartment->id], 'method' => 'delete']) !!}
                <div class='btn-group'>

                    <a href="{!! url('institution_departments/edit', [$institutionDepartment->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>

                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>