@extends('layouts.erp')

@section('page_title')
<h4>Institution Departments</h4>
@endsection

@section('content')
    <section class="content-header">
    </section>
    <section class="content-header">
        <span style="float: right"><a href="{{ url('institutionDepartments/create') }}"><button class="btn btn-success">Add Institution Department</button></a></span>
    </section>

    <div class="content">
        <div class="clearfix"></div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('institution_departments.table')
            </div>
        </div>
    </div>
@endsection

