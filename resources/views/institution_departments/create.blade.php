@extends('layouts.app_hr')

@section('page_title')
<h2>Create Institution Departments</h2>
@endsection

@section('hr_content')
    <section class="content-header">
    </section>
    <div class="content">

        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    @if(isset($errors))
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}} here</li>
                            @endForeach
                        </ul>
                        @endIf
                    </div>
                    @endIf
                    {!! Form::open(['route' => 'institutionDepartments.store']) !!}

                        @include('institution_departments.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
