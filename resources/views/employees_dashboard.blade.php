@extends('layouts.erp')

@section('page_title')
@endsection

@section('content')
    
    <div class="container-fluid pt-25">
        <!-- Row -->


    <div class="row">
		<div class="col-lg-12" style= " margin:0; padding:5px; " >
            <div class="panel with-nav-tabs "  style="background-color:#d6e9c6;" >
                <div class="panel-heading"  >
                        <ul class="nav nav-tabs ">
                            <li ><a href="{{URL::to('/home')}}" >Finance Dashboard</a></li>
                            <li><a href="{{URL::to('hr_dashboard')}}" >HR Dashboard</a></li>
                            <li ><a href="{{URL::to('procurement_dashboard')}}" >Procurement Dashboard</a></li>
                            <li ><a href="{{URL::to('project_dashboard')}}" >Project Dashboard</a></li>
                            <li class="active"><a href="{{URL::to('employees_dashboard')}}" >Employees Dashboard</a></li>
                        
                        </ul>
                </div>
                
            </div>
        </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
                <div class="panel panel-default card-view pt-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box bg-white">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"> &#x20A6;<span class="counter-anim">15,678</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">MY REQUESTS ({{ date('Y') }}
                                                    )</span><i
                                                        class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
                <div class="panel panel-default card-view pt-0 ">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box ">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter">&#x20A6;<span class="counter-anim">15,678</span></span>
                                            <span class="block"><span
                                                         class="weight-500 uppercase-font text-grey font-13">TOTAL COMPLETED </span></span>
                                                    )</span><i
                                                        class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
            <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
                <div class="panel panel-default card-view pt-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box bg-white">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">15,678</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">MY TASKS</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
    
        </div>
        <!-- Row -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-6 col-xs-12">
                <div id="weather_1" class="panel panel-default card-view">
                    <div class="panel panel-heading">
                        <div class="panel panel-title"> Latest Porjects Information</div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="" class="table table-hover table-bordered display mb-30">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Category</th>
                                            <th>Amount</th>
                                        </tr>
                                        </thead>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xs-12">
                <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
            </div>
            <!-- /Row -->
        </div>

@endsection


@section('script')
            <script>

                Highcharts.chart('container', {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Historic World Population by Region'
                    },
                    subtitle: {
                        text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
                    },
                    xAxis: {
                        categories: ['Africa', 'America', 'Asia', 'Europe', 'Oceania'],
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Population (millions)',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' millions'
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 80,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                        shadow: true
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Year 1800',
                        data: [107, 31, 635, 203, 2]
                    }, {
                        name: 'Year 1900',
                        data: [133, 156, 947, 408, 6]
                    }, {
                        name: 'Year 2012',
                        data: [1052, 954, 4250, 740, 38]
                    }]
                });
            </script>

@endsection

