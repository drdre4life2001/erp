@extends('layouts.app_settings')

@section('page_title')
<h2>Bank(s)</h2>
@endsection

@section('settings_content')
<div class="row">


    <table class="table table-responsive" id="banks-table">
        <thead>
        <th>Name</th>
        <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($banks as $bank)
        <tr>
            <td>{!! $bank->name !!}</td>
            <td>
                {!! Form::open(['route' => ['bank.destroy', $bank->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('bank.show', [$bank->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('bank.edit', [$bank->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('script')
@endsection
