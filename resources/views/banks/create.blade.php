@extends('layouts.app_settings')

@section('page_title')
<h2>Bank(s)</h2>
@endsection

@section('settings_content')
<div class="row">


    <form method="POST" action="{{URL::to('bank')}}" >
        @if(isset($errors))
        @if(count($errors) > 0)
        <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{$error}} here</li>
            @endForeach
        </ul>
        @endIf
        </div>
        @endIf
        <!-- Name Field -->
        <div class="form-group col-sm-6">
            <label for="name">Name:</label>
            <input class="form-control" name="name" type="text" id="name">
        </div>

        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <input class="btn btn-primary" type="submit" value="Save">
        </div>


    </form>
</div>
@endsection

@section('script')
@endsection
