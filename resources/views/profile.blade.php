@extends('layouts.erp')
<link href="{{asset('assets/css/profile.css')}}" rel="stylesheet">
@section('page_title')
    <h4>My Profile</h4><br>
@endsection

@section('content')
    <div class="row">
        @if(isset($errors))
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endForeach
                    </ul>
                </div>
            @endIf
        @endIf
             @if ( session()->has('status') )
                <div class="alert alert-success alert-dismissable">{{ session()->get('status') }}</div>
            @endif
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
            <div class="panel panel-primary">
                @foreach($items as $item)
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> {{$item->lastname}} {{$item->first_name}}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">

                                <tbody>
                                <tr>
                                    <td>Department:</td>
                                    <td>{{$item->department}}</td>
                                </tr>
                                <tr>
                                    <td>Hire date:</td>
                                    <td>{{$item->date_first_hired}}</td>
                                </tr>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td>{{$item->date_of_birth}}</td>
                                </tr>

                                <tr>
                                <tr>
                                    <td>Home Address</td>
                                    <td>{{$item->permanent_address}}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td>JIRA</td>
                                    <td>
                                        @if(count($details) == 0)
                                            <a href="" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myModal">Add credentials</a>
                                        @else
                                            <a href="" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myModal2">Update credentials</a>
                                        @endif
                                    </td>
                                </tr>
                                <!--Update JIRA credentials -->
                                <div id="myModal2" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h5 class="modal-title">JIRA Credentials</h5>
                                            </div>
                                            <form action="{{url('/profile/update-jira')}}" method="post">
                                                <div class="modal-body">
                                                    @foreach($details as $detail)
                                                        <input type="hidden" name="user_id" value="{{$detail->id}}">
                                                        <div class="form-group col-md-6">
                                                            <label class="">Username</label>
                                                            <input name="username" id="name" type="text" class="form-control" value="{{$detail->username}}" required>
                                                        </div>
                                                    @endforeach
                                                    <div class="form-group col-md-6">
                                                        <label class="">Password</label>
                                                        <input name="password" id="name" type="password" class="form-control" value="" required>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <input type="submit" value="Update" name="submit" class="btn btn-danger">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--Update ends -->
                                <!--Add credentials -->
                                <!-- Modal -->
                                <div id="myModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h5 class="modal-title">JIRA Credentials</h5>
                                        </div>
                                        <form action="{{url('/profile/add-jira')}}" method="post">
                                            <div class="modal-body">
                                                <div class="form-group col-md-6">
                                                    <label class="">Username</label>
                                                    <input name="username" id="name" type="text" class="form-control" value="" required>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="">Password</label>
                                                    <input name="password" id="name" type="password" class="form-control" value="" required>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" value="Add" name="submit" class="btn btn-danger">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                    </div>
                                </div>

                                <!--Add credentials ends -->
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection