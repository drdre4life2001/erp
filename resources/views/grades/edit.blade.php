@extends('layouts.erp')

@section('page_title')
    <h4>Edit Employee Level</h4>
@endsection

@section('content')
    <div class="row">
        @if(isset($errors))
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}} here</li>
                        @endForeach
                    </ul>
                    @endIf
                </div>
            @endIf
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-wrapper collapse in">
                            <form  action="{{ url('grades/edit', $grade->id) }}" method="POST">

                                <div class="form-group col-sm-6">
                                    {!! Form::label('name', 'Name:') !!}
                                    {!! Form::text('name',$grade->name, ['class' => 'form-control']) !!}
                                </div>

                                <!-- Code Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('code', 'Internal Code:') !!}
                                    {!! Form::text('code', $grade->code, ['class' => 'form-control']) !!}
                                </div>

                                <!-- Description Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('description', 'Description:') !!}
                                    {!! Form::text('description', $grade->description, ['class' => 'form-control']) !!}
                                </div>


                                <!-- Submit Field -->
                                <div class="form-group col-sm-12">
                                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                    <a href="{!! route('grades.index') !!}" class="btn btn-default">Cancel</a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div></div>
@endsection
