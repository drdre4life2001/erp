<div class="panel-body" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <form action="post">
        <div class="col-sm-12 col-xs-12">
            <div class="form-wrap">
                <div class="row"><!-- Name Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('name', 'Name:') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Code Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('code', 'Internal Code:') !!}
                        {!! Form::text('code', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Description Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('description', 'Description:') !!}
                        {!! Form::text('description', null, ['class' => 'form-control']) !!}
                    </div>


                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('grades.index') !!}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>