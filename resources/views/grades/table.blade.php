<div class="panel-wrapper collapse in">
    <div class="panel-body">
        <div class="table-wrap">
            <div class="table-responsive">
                <table id="datable_2" class="table table-hover table-bordered display mb-30">
                    <thead>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach($grades as $grade)
                        <tr>
                            <td>{!! $grade->name !!}</td>
                            <td>{!! $grade->code !!}</td>
                            <td>{!! $grade->description !!}</td>
                            <td>
                                {!! Form::open(['route' => ['grades.destroy', $grade->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>

                                    <a href="{!! url('/edit', [$grade->id]) !!}" class='btn btn-default btn-xs'><i
                                                class="glyphicon glyphicon-edit"></i></a>
                                    {{--
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                    --}}
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>