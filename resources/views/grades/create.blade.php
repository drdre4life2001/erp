@extends('layouts.erp')

@section('page_title')
    <h4>Create A New Employee Level</h4>
@endsection

@section('content')
    <div class="row">
        @if(isset($errors))
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}} here</li>
                            @endForeach
                    </ul>
                    @endIf
                </div>
                @endIf
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-wrapper collapse in">
                                {!! Form::open(['route' => 'grades.store']) !!}

                                @include('grades.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div></div>
@endsection
