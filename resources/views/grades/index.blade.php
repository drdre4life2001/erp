@extends('layouts.erp')

@section('page_title')
<h2>Employees' Levels</h2>
@endsection

@section('content')
    <section class="content-header">
    </section>
    <div class="content">
        <div class="clearfix"></div>
        <section class="content-header">
            <span style="float: right"><a href="{{ url('grades/create') }}"><button class="btn btn-success">Add Level</button></a></span>
        </section>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('grades.table')
            </div>
        </div>
    </div>
@endsection

