<div class="row">
    <div class="col-sm-12">
        <div class="panel-wrapper collapse in">
            <div class="panel-body">
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table class="table table-responsive" id="requestGroups-table">
                            <thead>
                            <th>Period</th>
                            <th>Request By</th>
                            <th>Remarks</th>
                            <th>Total Amount</th>
                            <th>Status</th>
                            <th colspan="3">Action</th>
                            </thead>
                            <tbody>
                            @foreach($requestGroups as $requestGroup)
                                <tr>
                                    <td>{!! $requestGroup->start_date !!} to {!! $requestGroup->end_date !!}</td>
                                    <td>{!! $requestGroup->employee->lastname.' '.$requestGroup->employee->first_name !!}</td>
                                    <td>{!! $requestGroup->remarks !!}</td>
                                    <td>&#x20A6; {!! number_format($requestGroup->total_amount_requesting) !!}</td>
                                    <td>{!! $requestGroup->status !!}</td>
                                    <td>
                                        {!! Form::open(['route' => ['payables.destroy', $requestGroup->id], 'method' => 'delete']) !!}
                                        <div class='btn-group'>
                                            <a href="{!! route('payables.show', [$requestGroup->id]) !!}"
                                               class='btn btn-default btn-primary btn-xs'><i
                                                        class="glyphicon glyphicon-eye-open"></i> View</a>
                                            <a href="{!! route('payables.edit', [$requestGroup->id]) !!}"
                                               class='btn btn-default btn-xs'><i
                                                        class="glyphicon glyphicon-edit"></i></a>
                                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
