

<div class="col-sm-6">

    <div class="card">
        <div class="card-header">

        </div>

        <div class="card-block">
            <table class="table table-responsive">
                <tr>
                    <th>User</th>
                    <td>{!! $requestGroup->employee->lastname.' '.$requestGroup->employee->first_name !!}</td>
                </tr>

                <tr>
                    <th>Period</th>
                    <td>From {!! $requestGroup->start_date !!}  to  {!! $requestGroup->end_date !!}</td>
                </tr>

                <tr>
                    <th>Amount</th>
                    <td>NGN {!! $requestGroup->total_amount_requesting !!}</td>
                </tr>

                <tr>
                    <th>Validation Date</th>
                    <td>{!! $requestGroup->validation_date !!}</td>
                </tr>

                <tr>
                    <th>Request Date</th>
                    <td>{!! $requestGroup->created_at !!}</td>
                </tr>

                <tr>
                    <th>User Responsible for approval</th>
                    <td>{!! $requestGroup->expectedApprovalUser->lastname.' '.$requestGroup->expectedApprovalUser->first_name !!}</td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="col-sm-6">
    <p>
        Status:
    </p>

    <div class="card">
        <div class="card-header">

        </div>

        <div class="card-block">
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>Ref. Payment</th>
                        <th>Date</th>
                        <th>Type</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Already Paid</td>
                    <td>NGN 0.00</td>
                </tr>

                <tr>
                    <td></td>
                    <td></td>
                    <td>Amount Claimed</td>
                    <td>NGN {!! $requestGroup->total_amount_requesting !!}</td>
                </tr>

                <tr>
                    <td></td>
                    <td></td>
                    <td>Remaining Unpaid</td>
                    <td>NGN 0.00</td>
                </tr>
            </table>

            <div class="text-left pull-right">
                @if($requestGroup->status == 0)
                <a href="{{URL::to('submit/request/'.$requestGroup->id)}}" class="btn btn-sm btn-primary">Submit</a>
                @else

                @if($requestGroup->expected_approval_user == Auth::user()->id)
                <a class="btn btn-sm btn-primary">Approve</a>
                <a href="" class="btn btn-sm btn-danger">Deny</a>
                @endIf

                @endIf
                <a href="" class="btn btn-sm btn-warning">Clone</a>
                <a href="" class="btn btn-sm btn-danger">Delete</a>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">

    <div class="card">
        <div class="card-header">

        </div>

        <div class="card-block">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Total Price</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @if(isset($requestGroup->requests))
                        @foreach($requestGroup->requests as $request)
                    <tr>
                        <td>{{$request->created_at}}</td>
                        <td>{{$request->item->name}}</td>
                        <td>{{$request->info}}</td>
                        <td>{{$request->qty}}</td>
                        <td>{{$request->unit_price}}</td>
                        <td>{{$request->total_price}}</td>
                        <td><a class="">Edit</a></td>
                    </tr>
                        @endForeach
                    @endIf

                    <tr>
                        {!! Form::open(['route' => 'requests.store']) !!}
                            <td>{{ date('Y-m-d H:i:s')}}
                            <input type="hidden" name="groupId" value="{{$requestGroup->id}}">
                            </td>
                            <td><select name="item" class="form-control" id="input-item">
                                    <option value=""> --- Please Select Item ---</option>
                                    @foreach($items as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select></td>
                            <td><textarea  name="info" class="form-control">

                                </textarea></td>
                            <td><input name="qty" type="text" class="form-control" /></td>
                            <td><input name="unit_price" id="unit_price" type="text" class="form-control" /></td>
                            <td> </td>
                            <td>
                                <input type="submit"  value="Add" class="btn btn-sm btn-primary"/></td>
                        {!! Form::close() !!}
                    </tr>
                </tbody>

            </table>
        </div>
    </div>
</div>


<div class="col-sm-12">

    <div class="card">
        <div class="card-header">
            <strong class="pull-left">Attachments</strong>
            <span class="pull-right"><a href="" class="btn btn-success btn-sm label">Add attachment</a></span>
        </div>

        <div class="card-block">
        </div>
    </div>
</div>

