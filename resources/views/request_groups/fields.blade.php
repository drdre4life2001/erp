<div>
    <hr/>
</div>

<div class="row">
    <!-- Start Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('start_date', 'Start Date:') !!}
        {!! Form::date('start_date', null, ['class' => 'form-control']) !!}
    </div>

    <!-- End Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('end_date', 'End Date:') !!}
        {!! Form::date('end_date', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="row">
    <!-- Id Hr Employee Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('id_hr_employee', 'Employee Requesting') !!}
        <select name="id_hr_employee" class="form-control">
            @foreach($employees as $employee)
                <option value="{{$employee->id}}">{{$employee->lastname}} {{$employee->first_name}}</option>
            @endforeach
        </select>
    </div>
    <!-- Remarks Field -->
    <div class="form-group col-sm-6 col-lg-6">
        {!! Form::label('remarks', 'Request Remarks:') !!}
        {!! Form::textarea('remarks', null, ['class' => 'form-control', 'rows'=>1]) !!}
    </div>

</div>
<div class="row">
    <div class="col-sm-12 pull-left">
        <b><h5>Request Item</h5></b>
    </div>
</div>

<div>
    <hr/>
</div>
<div class="row">
    <div class="form-group col-sm-6 col-lg-6">
        {!! Form::label('remarks', 'Request Title:') !!}
        {!! Form::text('title', null, ['class' => 'form-control', 'rows'=>1]) !!}
    </div>
</div>


<div class="row">
    <div class="form-group col-sm-12 required" id="category_item">
        <label class="col-sm-12 control-label" for="input-item">Item</label>
        <div class="col-sm-12 item_name">
            <select name="item" class="form-control" id="input-item">
                <option value=""> --- Please Select Item ---</option>
                @foreach($items as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="description">
            <p></p>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">

    <div class="form-group col-sm-6">
        <label class="">Quantity</label>
        <input name="qty" type="text" class="form-control" value="1"/>
    </div>

    <div class="form-group col-sm-6">
        <label class="">Unit Price</label>
        <input name="unit_price" id="unit_price" type="text" class="form-control"/>
    </div>

</div>

<div class="row">
    <div class="form-group col-sm-12">
        <label>Item Description</label>
        <textarea name="info" class="form-control">

        </textarea>
    </div>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('payables.index') !!}" class="btn btn-default">Cancel</a>
</div>


