@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Request Group
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($requestGroup, ['route' => ['requestGroups.update', $requestGroup->id], 'method' => 'patch']) !!}

                        @include('request_groups.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection