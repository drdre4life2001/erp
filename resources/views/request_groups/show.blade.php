@extends('layouts.erp')

@section('breadcrumb')
@endsection

@section('content')
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @if(isset($errors))
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}} here</li>
                            @endForeach
                        </ul>

                    </div>
                    @endIf
                    @endIf
                    @include('request_groups.show_fields')
                    <a href="{!! route('payables.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
