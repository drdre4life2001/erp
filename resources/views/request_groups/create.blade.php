
@extends((Session::get('role') == "admin" ? 'layouts.erp' : 'layouts.erp' ))
@section('breadcrumb')
@endsection

@section('content')

    <div class="row">
        <div class="col-sm-12">

        <div class="card">
            <div class="card-header">
                <h5>Request Ticket</h5>
            </div>

            <div class="card-block">
                    {!! Form::open(['route' => 'payables.store']) !!}

                        @include('request_groups.fields')

                    {!! Form::close() !!}
            </div>
        </div>
        </div>
    </div>
@endsection


@section('script')
<script>
    var categories = <?php echo json_encode(session('categories'));  ?> ;
</script>
<script>
    //console.log(categories);

    var result = {};
    var items = {}
    var selectedCategory = "";
    //loop through the categories
    for (var i = 0; i < categories.length; i++) {
        //save the categories and categories items object
        result[categories[i].name] = categories[i].items;
        //loop through the items in the category
        for (var j = 0; j < categories[i].items.length; j++) {
            //save the items data; [items id => items obj]
            items[categories[i].items[j].id] = categories[i].items[j];
        }


    }

    //console.log(result);
    //console.log(items);

    /*$('select[name="category"]').change(function () { // When category select changes

        //set the unit price to default
        $('#unit_price').val('');
        $('.description').replaceWith("<div class='description col-sm-12'><p><b></b><p></div>");
        //var p = document.querySelector('.other_product_name');
        //p.innerHTML = "";
        //$('#other_product_item').fadeOut();


        selectedCategory = $(this).val();
        var options = '<option>Choose one!</option>';
        $.each(result[$(this).val()] || [], function (i, v) { // Cycle through each associated items
            options += '<option value="' + v.id + '">' + v.name + '</option>';
        });
        //options += '<option>' + "Other" + '</option>';
        var itemtfield = document.createElement('select');
        itemtfield.setAttribute('name', 'item');
        itemtfield.classList.add('form-control');
        itemtfield.innerHTML = options;


        var p = document.querySelector('.item_name');
        p.innerHTML = "";
        p.appendChild(itemtfield);
        $('#category_item').fadeIn();
        setItemListener();
//            $('select[name="product"]').html(options); // And update the role options
    });*/


    function setItemListener() {
        $('select[name="item"]').change(function () {

            $(this).attr('name', 'item');
            //auto-set the unit price, qty, other information
            //gets its information
            var itemObj = items[$(this).val()];
            $('#unit_price').val( itemObj.unit_price);
            $('.description').replaceWith("<div class='description col-sm-12'><p><i>"+itemObj.description+"</i><p></div>");
            //console.log(itemObj);
            //console.log();
        })
    }

    setItemListener();
</script>
@endSection