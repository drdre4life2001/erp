<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id');
            $table->float('amount');
            $table->integer('type');
            $table->float('prevBalance');
            $table->float('currentBalance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bank_activities');
    }
}
