<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHrEmployeeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hr_employee', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('identification_number')->nullable()->unique('identification_number');
			$table->string('account_no')->nullable();
			$table->date('date_of_birth')->nullable();
			$table->string('home_address')->nullable();
			$table->string('first_name')->nullable();
			$table->string('lastname')->nullable();
			$table->string('other_name')->nullable();
			$table->string('passport_no')->nullable();
			$table->string('picture')->nullable();
			$table->string('city_of_birth', 50)->nullable();
			$table->string('disability', 25)->nullable();
			$table->string('permanent_address')->nullable();
			$table->integer('no_of_children')->nullable();
			$table->date('date_first_hired')->nullable();
			$table->integer('id_hr_work_addresses')->nullable()->index('FK_hr_employee_id_hr_work_addresses');
			$table->integer('id_sys_banks')->nullable()->index('FK_hr_employee_id_sys_banks');
			$table->string('code', 25)->nullable()->index('FK_hr_employee_code');
			$table->integer('id_hr_grade')->nullable()->index('FK_hr_employee_id_hr_grade');
			$table->string('code_sys_country')->nullable()->index('FK_hr_employee_code_sys_country');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hr_employee');
	}

}
