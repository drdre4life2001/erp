<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSysAssignedRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sys_assigned_roles', function(Blueprint $table)
		{
			$table->foreign('id_sys_roles', 'FK_sys_assigned_roles_id_sys_roles')->references('id')->on('sys_roles')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_sys_user', 'FK_sys_assigned_roles_id_sys_user')->references('id')->on('sys_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sys_assigned_roles', function(Blueprint $table)
		{
			$table->dropForeign('FK_sys_assigned_roles_id_sys_roles');
			$table->dropForeign('FK_sys_assigned_roles_id_sys_user');
		});
	}

}
