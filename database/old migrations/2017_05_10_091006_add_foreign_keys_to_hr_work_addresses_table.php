<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHrWorkAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hr_work_addresses', function(Blueprint $table)
		{
			$table->foreign('code', 'FK_hr_work_addresses_code')->references('code')->on('sys_country')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hr_work_addresses', function(Blueprint $table)
		{
			$table->dropForeign('FK_hr_work_addresses_code');
		});
	}

}
