<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHrEmployeeTerminationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hr_employee_termination', function(Blueprint $table)
		{
			$table->foreign('id_hr_employee_education', 'FK_hr_employee_termination_id_hr_employee_education')->references('id')->on('hr_employee_education')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hr_employee_termination', function(Blueprint $table)
		{
			$table->dropForeign('FK_hr_employee_termination_id_hr_employee_education');
		});
	}

}
