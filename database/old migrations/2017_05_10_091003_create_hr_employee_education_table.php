<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHrEmployeeEducationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hr_employee_education', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('specialization')->nullable();
			$table->string('notes')->nullable();
			$table->date('startDate')->nullable();
			$table->date('endDate')->nullable();
			$table->string('mode_of_education')->nullable();
			$table->string('grade')->nullable();
			$table->string('comments')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
			$table->integer('id_hr_institutions')->nullable()->index('FK_hr_employee_education_id_hr_institutions');
			$table->integer('id_hr_institution_departments')->nullable()->index('FK_hr_employee_education_id_hr_institution_departments');
			$table->integer('id_hr_employee')->nullable()->index('FK_hr_employee_education_id_hr_employee');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hr_employee_education');
	}

}
