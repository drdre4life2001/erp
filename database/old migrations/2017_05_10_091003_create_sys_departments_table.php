<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSysDepartmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sys_departments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('description')->nullable();
			$table->string('name')->nullable();
			$table->string('code')->nullable()->unique('code');
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('id_sys_organizations')->nullable()->index('FK_sys_departments_id_sys_organizations');
			$table->integer('id_hr_employee')->nullable()->index('FK_sys_departments_id_hr_employee');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sys_departments');
	}

}
