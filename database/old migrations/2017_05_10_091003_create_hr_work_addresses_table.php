<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHrWorkAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hr_work_addresses', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name')->nullable();
			$table->string('website')->nullable();
			$table->string('mobile')->nullable();
			$table->string('phone')->nullable();
			$table->string('email')->nullable();
			$table->string('fax')->nullable();
			$table->string('longitude', 25)->nullable();
			$table->string('latitutde', 25)->nullable();
			$table->string('street', 25)->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->string('code')->nullable()->index('FK_hr_work_addresses_code');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hr_work_addresses');
	}

}
