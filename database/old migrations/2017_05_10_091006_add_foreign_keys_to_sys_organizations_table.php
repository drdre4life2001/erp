<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSysOrganizationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sys_organizations', function(Blueprint $table)
		{
			$table->foreign('id_hr_work_addresses', 'FK_sys_organizations_id_hr_work_addresses')->references('id')->on('hr_work_addresses')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sys_organizations', function(Blueprint $table)
		{
			$table->dropForeign('FK_sys_organizations_id_hr_work_addresses');
		});
	}

}
