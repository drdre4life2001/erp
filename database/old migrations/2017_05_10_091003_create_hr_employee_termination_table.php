<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHrEmployeeTerminationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hr_employee_termination', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('new_employer')->nullable();
			$table->string('reason')->nullable();
			$table->integer('status')->nullable();
			$table->date('date_of_notification')->nullable();
			$table->date('projected_last_date')->nullable();
			$table->date('actual_last_date')->nullable();
			$table->date('accepted_date')->nullable();
			$table->integer('accepted_by_employee')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
			$table->integer('id_hr_employee_education')->nullable()->index('FK_hr_employee_termination_id_hr_employee_education');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hr_employee_termination');
	}

}
