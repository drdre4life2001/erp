<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHrEmployeeExperienceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hr_employee_experience', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('organization_name', 25)->nullable();
			$table->string('designation', 25)->nullable();
			$table->string('employee_number', 25)->nullable();
			$table->string('department', 25)->nullable();
			$table->date('work_start_date')->nullable();
			$table->date('work_end_date')->nullable();
			$table->string('last_manager', 50)->nullable();
			$table->float('last_drawn_salary', 10, 0)->nullable();
			$table->string('communication_details')->nullable();
			$table->string('project_details')->nullable();
			$table->string('remarks')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('id_hr_employee')->nullable()->index('FK_hr_employee_experience_id_hr_employee');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hr_employee_experience');
	}

}
