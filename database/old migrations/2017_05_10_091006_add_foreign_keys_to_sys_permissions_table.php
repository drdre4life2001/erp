<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSysPermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sys_permissions', function(Blueprint $table)
		{
			$table->foreign('id_sys_modules', 'FK_sys_permissions_id_sys_modules')->references('id')->on('sys_modules')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sys_permissions', function(Blueprint $table)
		{
			$table->dropForeign('FK_sys_permissions_id_sys_modules');
		});
	}

}
