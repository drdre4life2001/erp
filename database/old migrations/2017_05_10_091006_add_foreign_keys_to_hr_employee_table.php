<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHrEmployeeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hr_employee', function(Blueprint $table)
		{
			$table->foreign('code', 'FK_hr_employee_code')->references('code')->on('sys_gender')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('code_sys_country', 'FK_hr_employee_code_sys_country')->references('code')->on('sys_country')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_hr_grade', 'FK_hr_employee_id_hr_grade')->references('id')->on('hr_grade')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_hr_work_addresses', 'FK_hr_employee_id_hr_work_addresses')->references('id')->on('hr_work_addresses')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_sys_banks', 'FK_hr_employee_id_sys_banks')->references('id')->on('sys_banks')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hr_employee', function(Blueprint $table)
		{
			$table->dropForeign('FK_hr_employee_code');
			$table->dropForeign('FK_hr_employee_code_sys_country');
			$table->dropForeign('FK_hr_employee_id_hr_grade');
			$table->dropForeign('FK_hr_employee_id_hr_work_addresses');
			$table->dropForeign('FK_hr_employee_id_sys_banks');
		});
	}

}
