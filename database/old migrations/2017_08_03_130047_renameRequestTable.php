<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema::table('requeststatus', function (Blueprint $table) {
            //
            Schema::rename('requeststatus', 'request_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requeststatus', function (Blueprint $table) {
            //
            Schema::drop('request_status');
        });
    }
}
