<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHrEmployeeRefereeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hr_employee_referee', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('lastname')->nullable();
			$table->string('firstname')->nullable();
			$table->string('address')->nullable();
			$table->string('phone', 25)->nullable();
			$table->string('email')->nullable();
			$table->string('relationship')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
			$table->integer('id_hr_employee')->nullable()->index('FK_hr_employee_referee_id_hr_employee');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hr_employee_referee');
	}

}
