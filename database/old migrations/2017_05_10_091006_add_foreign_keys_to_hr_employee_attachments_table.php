<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHrEmployeeAttachmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hr_employee_attachments', function(Blueprint $table)
		{
			$table->foreign('id_hr_employee', 'FK_hr_employee_attachments_id_hr_employee')->references('id')->on('hr_employee')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hr_employee_attachments', function(Blueprint $table)
		{
			$table->dropForeign('FK_hr_employee_attachments_id_hr_employee');
		});
	}

}
