<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSysAssignedRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sys_assigned_roles', function(Blueprint $table)
		{
			$table->integer('id', true);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
			$table->integer('id_sys_roles')->nullable()->index('FK_sys_assigned_roles_id_sys_roles');
			$table->integer('id_sys_user')->nullable()->index('FK_sys_assigned_roles_id_sys_user');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sys_assigned_roles');
	}

}
