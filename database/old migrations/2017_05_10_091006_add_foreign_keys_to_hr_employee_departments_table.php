<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHrEmployeeDepartmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hr_employee_departments', function(Blueprint $table)
		{
			$table->foreign('id_hr_employee', 'FK_hr_employee_departments_id_hr_employee')->references('id')->on('hr_employee')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_sys_departments', 'FK_hr_employee_departments_id_sys_departments')->references('id')->on('sys_departments')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hr_employee_departments', function(Blueprint $table)
		{
			$table->dropForeign('FK_hr_employee_departments_id_hr_employee');
			$table->dropForeign('FK_hr_employee_departments_id_sys_departments');
		});
	}

}
