<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHrEmployeeJobTitleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hr_employee_job_title', function(Blueprint $table)
		{
			$table->foreign('id_hr_employee', 'FK_hr_employee_job_title_id_hr_employee')->references('id')->on('hr_employee')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_hr_job_title', 'FK_hr_employee_job_title_id_hr_job_title')->references('id')->on('hr_job_title')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hr_employee_job_title', function(Blueprint $table)
		{
			$table->dropForeign('FK_hr_employee_job_title_id_hr_employee');
			$table->dropForeign('FK_hr_employee_job_title_id_hr_job_title');
		});
	}

}
