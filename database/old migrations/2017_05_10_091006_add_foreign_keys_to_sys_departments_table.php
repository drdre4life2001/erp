<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSysDepartmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sys_departments', function(Blueprint $table)
		{
			$table->foreign('id_hr_employee', 'FK_sys_departments_id_hr_employee')->references('id')->on('hr_employee')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_sys_organizations', 'FK_sys_departments_id_sys_organizations')->references('id')->on('sys_organizations')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sys_departments', function(Blueprint $table)
		{
			$table->dropForeign('FK_sys_departments_id_hr_employee');
			$table->dropForeign('FK_sys_departments_id_sys_organizations');
		});
	}

}
