<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHrEmployeeDepartmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hr_employee_departments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->date('start_date')->nullable();
			$table->date('end_date')->nullable();
			$table->string('note')->nullable();
			$table->integer('id_hr_employee')->nullable()->index('FK_hr_employee_departments_id_hr_employee');
			$table->integer('id_sys_departments')->nullable()->index('FK_hr_employee_departments_id_sys_departments');

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hr_employee_departments');
	}

}
