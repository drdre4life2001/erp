<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysRfqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_rfq', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('procurement_officer_id');
            $table->dateTime('request_date');
            $table->dateTime('response_date');
            $table->integer('vendor_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_rfq');
    }
}
