<?php
namespace App\Libraries;
use App\Models\UserPermission;
use App\Models\Employee;
use App\Models\Company;
use App\Models\EmployeeAbsence;
use App\Models\EmployeeArrear;
use App\Models\EmployeeBonus;
use App\Models\EmployeeDeduction;
use App\Models\EmployeeLoan;
use App\Models\EmployeeRenumeration;
use App\Models\MonthDay;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Ledger;
use App\Models\PayrollSummary;
use App\Models\BankActivity;
use App\Models\OtherExpense;
use App\Models\RequestItems;
use App\Models\ApiLog;
use phpDocumentor\Reflection\Types\Self_;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\BadResponseException;
use App\Models\EmployeePayroll;
use Cloudder;



/**
 * Created by PhpStorm.
 * User: noibilism
 * Date: 7/13/17
 * Time: 10:21 AM
 */

class Utilities
{
    public function pullEmployees($employee_id = null, $table = null, $subsidiary_id = null, $advance_company_id = null, $advance_company_uri = null){
        $result = DB::select("SELECT * FROM '$table' WHERE employee_id = '$employee_id' AND subsidiary_id = '$subsidiary_id' AND advance_company_id = '$advance_company_id' AND advance_company_uri = '$advance_company_uri'");
    }

    public static function companyOrganizations($advance_company_id = null, $advance_company_uri = null)
    {
        return DB::SELECT("SELECT * FROM sys_organizations WHERE company_id = '$advance_company_id' AND company_uri = '$advance_company_uri'");
    }

   public static function checkExistingAdvanceOrganizationDuplicate($organization_uid)
    {
        $adv_company_id = auth()->user()->company->id;
        $adv_company_uri = auth()->user()->company->uri;
        $data = DB::SELECT("SELECT * FROM sys_organizations WHERE uid = '$organization_uid' AND company_id = '$adv_company_id' AND company_uri = '$adv_company_uri'");
        return $data;
    }

    public static function guzzleClient(){
        try{
            $api_key = "09c5579446424ca1aeb23e2f964a25b5";

            $key = env('API_KEY');


            $client = new Client(['headers' => [
                    'Content-Type' => 'application/json', 
                    'Accept' => 'application/json',
                    'api-key' => isset($key) ? $key :$api_key]
            ]);
            // dd("details hayjay");

            
        } catch(\GuzzleHttp\Exception\RequestException $e) {
            if ($ex->hasResponse()) {
                $result = json_decode($ex->getResponse()->getBody()->getContents(), true);
                return back()->with('error', $result['message']);
            }
        }
        return $client;
    }

    public static function uploadImageToCloudinary($field_name){
        $image_name = !empty($field_name) ? $field_name->getRealPath() : null;
        Cloudder::upload($image_name, null);
        list($width, $height) = getimagesize($image_name);
        $image_url= Cloudder::show(Cloudder::getPublicId(), ["width" => 200, "height"=>200]);
        return $image_url;
    }


    public static function getEmployeePayroll($month, $year, $adv_company_id_column, $adv_company_uri_column){
        return EmployeePayroll::where('month', $month)
                ->where(['year' => $year,
                        $adv_company_id_column => auth()->user()->company->id,
                        $adv_company_uri_column => auth()->user()->company->uri,
                ])->with(['employee'])->get();

    }

    public static function checkCompanyExist($company_uri){
        $company = Company::where('uri', $company_uri)->first();
        if($company){
            return $company;
        }else{
            return false;
        }
    }

    public static function returnCompanyDetails(){
        return [
                'advance_company_id' => auth()->user()->company->id,
                'advance_company_uri' => auth()->user()->company->uri
        ];
    }

    public static function alternativeCompanyDetails(){
        $data = [
                'advance_company_id' => auth()->user()->company->id,
                'advance_company_uri' => auth()->user()->company->uri
        ];
        
        return $data;
    }

    public static function cleanDate($date){
        return str_replace('\/', '-', $date);
    }

    //Access control for dashboard
    public static function getRoleId(){
        $id_hr_employee= Auth::user()->id_hr_employee;
        $user_id = Auth::user()->id;
        $employee_role = DB::select("SELECT role from sys_user where id_hr_employee = '$id_hr_employee'");
        if(count($employee_role) > 0){
            //echo 'true';
            foreach ($employee_role as $employee_role){
                $employee_role = $employee_role->role;
            }
        }else{
            $employee_role = 'null';
        }

        $user_role = DB::select("SELECT role from sys_user where id = '$user_id'");
        if(count($user_role) > 0){
            foreach ($user_role as $user_role){
                $user_role = $user_role->role;
            }
        }else{
            $user_role = 'null';
        }

        $role_id = DB::select("select COALESCE($employee_role, $user_role) as role");
        foreach ($role_id as $role_id){
            $role_id = $role_id->role;
        }
        return $role_id;
    }

    public static function otherExpenses($status = null)
    {
        $adv_company_id = auth()->user()->company->id;
        $adv_company_uri = auth()->user()->company->uri;
        
        $other_expenses = null;
        if(is_null($status)){
            $other_expenses = \DB::select("SELECT id, title, amount, description, status, created_by, approved_by, created_at, updated_at, account_header_id,
                                      (SELECT name FROM account_headers WHERE id = account_header_id ) as category, 
        (SELECT first_name FROM hr_employee WHERE id = approved_by) as approved_by  FROM other_expenses WHERE advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri'");
        }else{
            $other_expenses = \DB::select("SELECT id, title, amount, description, status, created_by, approved_by, created_at, updated_at, account_header_id,
                                      (SELECT name FROM account_headers WHERE id = account_header_id ) as category, 
                                      (SELECT first_name FROM hr_employee WHERE id = approved_by) as approved_by  FROM other_expenses WHERE status = '$status'");
        }
        return $other_expenses;
    }
    public static function crmPipeline()
    {
        $pipeline_status = DB::select("select * from pipeline_status where id != 1");
        return $pipeline_status;
    }

    public static function crmStatus()
    {
        $status = DB::select("select * from prospects_status where id != 1"); //Don't pick 'initiated' since prospect is already initiated during creation
        return $status;
    }

    public static function dashboardControl($resource_id){
            $role_id = self::getRoleId();
            // dd($resource_id);

            //print_r($role_id);exit;
            $sql = DB::select("select * from user_permissions where resource_id = $resource_id
        and role_id = $role_id
        
        ");
            if (count($sql) == 1) {
                return true;
            } else {
                return false;
            }
       // }
    }

    public static function crm_prospects()
    {
        $user_id = auth()->user()->id_hr_employee;
        $grade = DB::select("select * from hr_employee where id_hr_grade = 4 and id = '$user_id'");
        $item = null;
        if(count($grade) == 1){
            //if c level, let him see all prospects and remove 'add activity' button
            $item = DB::select("select contact_person, contact_position, p.phone as contact_phone, p.email as contact_email, p.revenue as contact_revenue, p.id as id, p.name as prospect, p.created_at as created_at, b.id as status_id,  b.definition as status, 
            c.first_name as firstname, c.lastname as lastname, d.name as subsidiary from prospects as p, prospects_status as b, 
            hr_employee as c, sys_organizations as d  where p.subsidiary_id = d.id and p.status = b.id and p.created_by = 
            c.id order by p.created_at desc");
        }else{
            //else only show prospects created by this user
            $item = DB::select("select contact_person, contact_position, p.phone as contact_phone, p.email as contact_email, p.revenue as contact_revenue, p.id as id, p.name as prospect, p.created_at as created_at, b.id as status_id, b.definition as status, 
            c.first_name as firstname, c.lastname as lastname, d.name as subsidiary from prospects as p, prospects_status as b, 
            hr_employee as c, sys_organizations as d where p.subsidiary_id = d.id and p.status = b.id and p.created_by = 
            c.id and p.created_by = '$user_id' order by p.created_at desc");
        }

        return $item;
    }

    //Utility methods for Employee dashboard - prospects, in-pipe, close, deactivated
    public static function totalProspects(){
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $user_id = Auth::user()->id_hr_employee;
        $grade = DB::table('hr_employee')->where(['id_hr_grade' => 4, 'id' => $user_id])->count();
        //if c level staff
        if($grade == 1){
            $prospects = DB::table('prospects')->where('advance_company_id', $company_id)->count();
        }else{
            //Employee
            $prospects = DB::table('prospects')->where('created_by', $user_id)->where('advance_company_id', $company_id)->count();
        }
        return $prospects;
    }
    public static function totaInLineProspects(){
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;
        $user_id = Auth::user()->id_hr_employee;

        $grade = DB::table('hr_employee')->where(['id_hr_grade' => 4, 'id' => $user_id])->where('company_id', $company_id)->count();
        //if c level staff
        if($grade == 1){
            $inline = DB::table('prospects')->where('status', 2)->where('advance_company_id', $company_id)->where('company_uri', $company_uri)->count();
        }else{
            //Employee
            $inline = DB::table('prospects')->where(['created_by' => $user_id, 'status' => 2])->where('advance_company_id', $company_id)->where('advance_company_uri', $company_uri)->count();
        }
        return $inline;
    }
    public static function totalClosedProspects(){
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $user_id = Auth::user()->id_hr_employee;
        $grade = DB::table('hr_employee')->where(['id_hr_grade' => 4, 'id' => $user_id])->count();
        //if c level staff
        if($grade == 1){
            $closed = DB::table('prospects')->where('status', 3)->where('advance_company_id', $company_id)->where('advance_company_uri', $company_uri)->count();
        }else{
            //Employee
            $closed = DB::table('prospects')->where(['created_by' => $user_id, 'status' => 3])->where('advance_company_uri', $company_uri)->count();
        }
        return $closed;
    }
    public static function totalDeactivatedProspects(){
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $user_id = Auth::user()->id_hr_employee;
        $grade = DB::table('hr_employee')->where(['id_hr_grade' => 4, 'id' => $user_id])->count();
        //if c level staff
        if($grade == 1){
            $deactivated = DB::table('prospects')->where('status', 4)->where('advance_company_id', $company_id)->where('advance_company_uri', $company_uri)->count();
        }else{
            //Employee
            $deactivated = DB::table('prospects')->where(['created_by' => $user_id, 'status' => 4])->where('advance_company_uri', $company_uri)->count();
        }
        return $deactivated;
    }
    //Utility methods for Procurement dashboard
    public static function totalRequests(){

        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $total_requests =  DB::table('request_items')->where('advance_company_id', $company_id)->sum('total');
        return $total_requests;
    }
    public static function totalApprovedRequests(){
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $total_approved_requests =  DB::table('request_items')->where('rfq_status', 1)->where('advance_company_id', $company_id)->sum('total');
        return $total_approved_requests;
    }

    //Utility methods for HR dashboard
    public static function employeeByDepartment(){
        $company_id = auth()->user()->company->id;

        $employee = DB::select("select a.name as department, a.advance_company_id as company_id, a.advance_company_uri as company_uri, count(a.id) as count from
        sys_departments as a, hr_employee_departments as b, hr_employee as c WHERE
        c.id = b.id_hr_employee and b.id_sys_departments = a.id and a.advance_company_id = '$company_id' 
        group by a.id
        ");
        $department = [];
        $count = [];

        foreach ($employee as $employee){

            $department[] = $employee->department;

            $count[] = $employee->count;

        }
        $data = ['department' => $department, 'count' => $count];
        $data = json_encode($data);
        return $data;
    }
    public static function totalEmployee(){
        //show total number of employees
        $employee =  DB::table('hr_employee')->count();
        return $employee;
    }
    public static function totalLoans(){
        //Total loans
        $loans = DB::table('hr_employee_loans')->sum('total_payable');
        $loan = number_format($loans);
        return $loan;
    }
    public static function totalSalary(){

        //Total Salaries
        $total_salaries = DB::table('hr_employee_renumerations')->where('company_id', auth()->user()->company->id)->where('company_uri', auth()->user()->company->uri)->sum('basic_pay');
        $total_salary = number_format($total_salaries);
        return $total_salary;
    }
    public static function totalLeaveRequest(){
        $leave_requests = DB::table('hr_employee_leaves')->count();
        return $leave_requests;
    }
    //Utility methods for finance dashboard
    public static function totalRevenue(){
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $revenue = DB::select("select sum(total_commission) as total from revenues where advance_company_id = '$company_id' and advance_company_uri = '$company_uri'");
        return $revenue;
    }
    public static function totalPayable(){
        //Sum up payroll + procurement + other expense yet to be disbursed
        //Fetch payroll
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;
        $payroll = DB::select("select sum(amount) as total from payroll_summaries where status = 'pending' and advance_company_id = '$company_id' and advance_company_uri = '$company_uri'");
        foreach ($payroll as $payroll){
            $payroll = $payroll->total;
        }
        //Fetch procurement
        $procurement = DB::select("select sum(total) as total from request_items where rfq_status = 1 and advance_company_id = '$company_id' and advance_company_uri = '$company_uri'");
        foreach ($procurement as $procurement){
            $procurement = $procurement->total;
        }
        //Fetch expenses
        $other_expenses = DB::select("select sum(amount) as total from other_expenses where disbursed = 'false' and advance_company_id = '$company_id' and advance_company_uri = '$company_uri'");
        foreach ($other_expenses as $other_expenses){
            $other_expenses = $other_expenses->total;
        }
        $total_payable = $payroll + $procurement + $other_expenses;
        return $total_payable;
    }
    public static function totalExpense(){
        //Sum up payroll + procurement + expenses already disbursed
        //Fetch payroll
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;
        $payroll = DB::select("select sum(amount) as total from payroll_summaries where status = 'paid' and advance_company_id = '$company_id' and advance_company_uri = '$company_uri'");
        foreach ($payroll as $payroll){
            $payroll = $payroll->total;
        }
        //Fetch procurement
        $procurement = DB::select("select sum(total) as total from request_items where rfq_status = 2  and advance_company_id = '$company_id' and advance_company_uri = '$company_uri'");
        foreach ($procurement as $procurement){
            $procurement = $procurement->total;
        }
        //Fetch expenses
        $other_expenses = DB::select("select sum(amount) as total from other_expenses where disbursed = 'true'  and advance_company_id = '$company_id' and advance_company_uri = '$company_uri'");
        foreach ($other_expenses as $other_expenses){
            $other_expenses = $other_expenses->total;
        }
        $total_expenses = $payroll + $procurement + $other_expenses;
        return $total_expenses;
    }
    //methods end
    public static function switch_db($db){
        switch ($db){
            case 'bus':
                return DB::connection('bus');
            case 'phedc':
                return DB::connection('phedc');
            case 'jedc':
                return DB::connection('jedc');
            default:
                return null;
                break;
        }
    }
    public static function insLedger($bank_account_id, $amount, $current_balance, $newBalance, $account_sub_header,
                                     $item_id, $type, $user_id)
    {
        Ledger::create([
            'bank_id' => $bank_account_id,
            'amount' => $amount,
            'prevBalance' => $current_balance,
            'currentBalance' => $newBalance,
            'sub_header_id' => $account_sub_header,
            'trans_id' => $item_id,
            'type' => $type,
            'user_id' => $user_id
        ]);
    }

    public static function insBankActivity($bank_account_id, $amount, $type, $prevBalance, $currentBalance){
                    BankActivity::create([
                        'bank_id' => $bank_account_id,
                        'amount' => $amount,
                        'type' => $type, //Gotten from account_effects table 0- debit, 1-credit
                        'prevBalance' => $prevBalance,
                        'currentBalance' => $currentBalance
                    ]);
    }
    public static function disburseStatus($item_id){
        //Confirm item status for Finance team
        $id_hr_employee= Auth::user()->id_hr_employee;
        $sql = DB::select("select * from user_permissions where (resource_id = 3)
        and role_id = (SELECT role from sys_user where id_hr_employee = $id_hr_employee)
        ");
        $query = DB::select("select * from request_items where id = '$item_id' and rfq_status = 1");
        if(count($sql) >= 1 && count($query) == 1){
            return true;
        }else{
            return false;
        }
    }

     public static function getOtherExpenses()
    {
       $other_expenses = \DB::select('SELECT id, title, amount, description, status, created_by, requested_by, approved_by, created_at, updated_at, account_header_id,
                                      (SELECT name FROM account_headers WHERE id = account_header_id ) as category, 
                                      (SELECT first_name FROM hr_employee WHERE id = approved_by) as approved_by,
                                      (SELECT email FROM sys_user WHERE id = requested_by) as owner
                                        FROM other_expenses ');
       return $other_expenses;
    }

    public static function allExpenses(){
        $other_expenses = \DB::select('SELECT id, title, amount, description, status, created_by, approved_by, created_at, updated_at, account_header_id,
                              (SELECT name FROM account_headers WHERE id = account_header_id ) as category, 
                              (SELECT first_name FROM hr_employee WHERE id = approved_by) as approved_by  FROM other_expenses');
        return $other_expenses;
    }

    public static function myExpenses()
    {
        $other_expenses = \DB::select('SELECT id, title, amount, description, status, created_by, approved_by, created_at, updated_at, account_header_id,
                                      (SELECT name FROM account_headers WHERE id = account_header_id ) as category, 
                                      (SELECT first_name FROM hr_employee WHERE id = approved_by) as approved_by  FROM other_expenses LIMIT 6 WHERE created_by = '.auth()->user()->id);
        return $other_expenses;
    }



    //get true or false for procurement/finance guy
    public static function getUserStatus($item_id){
        $user_id= Auth::user()->id_hr_employee;
        $query = DB::select("select * from request_items as a, pr_request as b where a.request_id = b.id and 
                 b.user_id = '$user_id' and a.id = '$item_id'");
        $sql = DB::select("select * from user_permissions where (resource_id = 3)
        and role_id = (SELECT role from sys_user where id_hr_employee = $user_id)
        ");
        if(count($query) >= 1){
            return 'procurement'; //procurement
        }else if(count($sql)){
            return 'finance'; //other users
        }else{
            return 'others';
        }
    }
    public static function rfqStatus($item_id){
        //Confirm item status for Procurement team
        $id_hr_employee= Auth::user()->id_hr_employee;
        $sql = DB::select("select * from user_permissions where (resource_id = 6 AND resource_id != 3)
        and role_id = (SELECT role from sys_user where id_hr_employee = $id_hr_employee)
        ");
        $query = DB::select("select * from request_items where id = '$item_id' and rfq_status = 1");
        if(count($sql) == 1 && count($query) == 1){
            return true;
        }else{
            return false;
        }
    }

    //Check if a user have right to view a resource
    public static function  checkResourcePermission($resource_id, $role_id, $permission_id){ 
        $sql = DB::select("select permissions from user_permissions where role_id = $role_id and resource_id = $resource_id");
        foreach ($sql as $permission) {
            $res = $permission->permissions;
        }
        $result = $res;
        $data =  explode(',',$result);
        foreach ($data as $key => $value) {
            if($permission_id == $value){
                return true;
            }else{
                return back();
            }
            
        }   
    }
    function createFullWordOrdinal($num)
    {
        $number = intval($num);
        $ord1 = array(1 => "first", 2 => "second", 3 => "third", 5 => "fifth", 8 => "eight", 9 => "ninth", 11 => "eleventh", 12 => "twelfth", 13 => "thirteenth", 14 => "fourteenth", 15 => "fifteenth", 16 => "sixteenth", 17 => "seventeenth", 18 => "eighteenth", 19 => "nineteenth");
        $num1 = array("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eightteen", "nineteen");
        $num10 = array("zero", "ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety");
        $places = array(2 => "hundred", 3 => "thousand", 6 => "million", 9 => "billion", 12 => "trillion", 15 => "quadrillion", 18 => "quintillion", 21 => "sextillion", 24 => "septillion", 27 => "octillion");
        $number = array_reverse(str_split($number));
        if ($number[0] == 0) {
            if ($number[1] >= 2)
                $out = str_replace("y", "ieth", $num10[$number[1]]);
            else
                $out = $num10[$number[1]] . "th";
        } else if (isset($number[1]) && $number[1] == 1) {
            $out = $ord1[$number[1] . $number[0]];
        } else {
            if (array_key_exists($number[0], $ord1))
                $out = $ord1[$number[0]];
            else
                $out = $num1[$number[0]] . "th";
        }
        if ((isset($number[0]) && $number[0] == 0) || (isset($number[1]) && $number[1] == 1)) {
            $i = 2;
        } else {
            $i = 1;
        }
        while ($i < count($number)) {
            if ($i == 1) {
                $out = $num10[$number[$i]] . " " . $out;
                $i++;
            } else if ($i == 2) {
                $out = $num1[$number[$i]] . " hundred " . $out;
                $i++;
            } else {
                if (isset($number[$i + 2])) {
                    $tmp = $num1[$number[$i + 2]] . " hundred ";
                    $tmpnum = $number[$i + 1] . $number[$i];
                    if ($tmpnum < 20)
                        $tmp .= $num1[$tmpnum] . " " . $places[$i] . " ";
                    else
                        $tmp .= $num10[$number[$i + 1]] . " " . $num1[$number[$i]] . " " . $places[$i] . " ";

                    $out = $tmp . $out;
                    $i += 3;
                } else if (isset($number[$i + 1])) {
                    $tmpnum = $number[$i + 1] . $number[$i];
                    if ($tmpnum < 20)
                        $out = $num1[$tmpnum] . " " . $places[$i] . " " . $out;
                    else
                        $out = $num10[$number[$i + 1]] . " " . $num1[$number[$i]] . " " . $places[$i] . " " . $out;
                    $i += 2;
                } else {
                    $out = $num1[$number[$i]] . " " . $places[$i] . " " . $out;
                    $i++;
                }
            }
        }
        return $out;
    }

    public static function getMonths()
    {
        $month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        return $month_names;
    }

    public static function getYears()
    {
        $current_year = date('Y');
        $date_range = range($current_year, $current_year - 10);
        return $date_range;
    }

    public static function soft_delete($id, $table)
    {
        $result = DB::table($table)
            ->where('id', $id)
            ->update(['active' => 0, 'deleted_at' => date('Y-m-d H:i:s')]);
        return $result;
    }

    //could serve as middleware ---- maybe later
    public static function checkCompanyAuthorizationForARequest($response_company_id, $response_company_uri)
    {
        $advance_user = $response_company_id === auth()->user()->company->id && $response_company_uri === auth()->user()->company->uri;

        switch($advance_user) {

            case true:
                return true;
                
                break;
            case false:
                return redirect('/departments');
                break;
          }
        
    }

    public static function generate_payroll($month, $yr, $id_hr_company)
    {
        
        $adv_company_id = auth()->user()->company->id;
        
        $adv_company_uri = auth()->user()->company->uri;

        $employees = Employee::where(['active' => 1, 'id_hr_company' => $id_hr_company, 'company_id' => auth()->user()->company->id, 'company_uri' => auth()->user()->company->uri])->get();
        if(empty($employees)){
            return back()->with('error', 'No employee found for the selected subsidiary...please create an employee');
        }
        $checkduplicate = DB::table('hr_payrolls')->where('month', $month)->where(['year' => $yr, 'company_id' => $id_hr_company, 'advance_company_id' => auth()->user()->company->id, 'advance_company_uri' => auth()->user()->company->uri])->get();
        $reference = self::generateReference();
        if (!empty($checkduplicate)) {
            return false;
        } else {
            $result = [];
            if(!$employees->isEmpty()){
                foreach ($employees as $employeee) {
                    $employee = EmployeeRenumeration::where(['employee_id' => $employeee->id, 'company_id' => auth()->user()->company->id, 'company_uri' => auth()->user()->company->uri])->first();
                    $deductions = EmployeeDeduction::where('employee_id', $employeee->id)->where('month', $month)->where(['year' => $yr, 'advance_company_id' => auth()->user()->company->id, 'advance_company_uri' => auth()->user()->company->uri])->get();
                    $deduction = 0;
                    foreach ($deductions as $ded) {
                        $deduction = $ded->amount + $deduction;
                    }
                    $loans = EmployeeLoan::where(['employee_id' => $employeee->id, 'company_id' => auth()->user()->company->id, 'company_uri' => auth()->user()->company->uri])->where('active', 1)->first();
                    if ($loans) {
                        $loan = $loans->total_payable / $loans->tenure;
                    } else {
                        $loan = 0;
                    }
                    $taxable_bonus = EmployeeBonus::where('employee_id', $employeee->id)->where('month', $month)->where(['year' => $yr, 'company_id' => auth()->user()->company->id, 'company_uri' => auth()->user()->company->uri])->where('taxable', 1)->get();
                    $t_bonus = 0;
                    foreach ($taxable_bonus as $bonus) {
                        $t_bonus = $bonus->amount + $t_bonus;
                    }
                    $non_taxable_bonus = EmployeeBonus::where('employee_id', $employeee->id)->where('month', $month)->where(['year' => $yr, 'company_id' => auth()->user()->company->id, 'company_uri' => auth()->user()->company->uri])->where('taxable', 0)->get();
                    $n_bonus = 0;
                    foreach ($non_taxable_bonus as $bonus) {
                        $n_bonus = $bonus->amount + $n_bonus;
                    }
                    if($employee){ //check if employee remuneration has been created
                        $id = $employee->employee_id;
                        //pull out the currently iterated employee department
                        $dpt = DB::select("SELECT (SELECT id FROM sys_departments WHERE id = id_sys_departments) as department_id FROM hr_employee_departments WHERE id_hr_employee = '$id' AND advance_company_uri = '$adv_company_uri' AND advance_company_id = '$adv_company_id'"); 
                        $dept = isset($dpt[0]->department_id) ? $dpt[0]->department_id : null;
                        $annualsalary = $employee->basic_pay;
                        $monthlysalary = $annualsalary / 12;
                        $transport = $employee->transport;
                        $trn = $transport / 12;
                        $leave = $employee->leave;
                        $lve = $leave / 12;
                        $housing = $employee->housing;
                        $hsn = $housing / 12;
                        $enter = $employee->entertainment;
                        $ent = $enter / 12;
                        $utility = $employee->utility;
                        $uti = $utility / 12;
                        $meals = $employee->meal;
                        $meal = $meals / 12;
                        $life_assure = $employee->life_assurance;
                        $la = $life_assure / 12;
                        $days_the_year = MonthDay::where('month', $month)->where('year', $yr)->where('advance_company_id', auth()->user()->company->id)->first();
                        $days_the_year = $days_the_year->days;
                        $lve_per_day = $lve / $days_the_year;
                        $gross_per_day = $monthlysalary / $days_the_year;
                        $housing_per_day = $hsn / $days_the_year;
                        $ent_per_day = $ent / $days_the_year;
                        $utility_per_day = $uti / $days_the_year;
                        $meal_per_day = $meal / $days_the_year;
                        $trn_per_day = $trn / $days_the_year;
                        $absent_days = EmployeeAbsence::where('employee_id', $employee->employee_id)->where('month', $month)->where(['year' => $yr, 'advance_company_id' => $adv_company_id, 'advance_company_uri' => $adv_company_uri])->first();
                        $absent_days = isset($absent_days->days_absent) ? $absent_days->days_absent : 0;
                        $arrear_days = EmployeeArrear::where('employee_id', $employee->employee_id)->where('month', $month)->where(['year' => $yr, 'advance_company_id' => $adv_company_id, 'advance_company_uri' => $adv_company_uri])->first();
                        // dd($arrear_days);
                        $arrear_days = isset($arrear_days->amount) ? $arrear_days->amount: 0;
                        $worked_days = $days_the_year - $absent_days;
                        $division = $worked_days / $days_the_year;
                        $basic_pay = $monthlysalary * $division;
                        $leave_monthly = $lve * $division;
                        $housing_monthly = $hsn * $division;
                        $ent_monthly = $ent * $division;
                        $utility_monthly = $uti * $division;
                        $meal_monthly = $meal * $division;
                        $trn_monthly = $trn * $division;
                        $pension_status = $employee->pension;
                        $cra_status = $employee->cra;
                        //$la_status = $this->ClientsEmployee->getlifeassurance($id);
                        $fixedcra_status = $employee->fixed_cra;
                        $dr_status = $employee->dependable_relative;
                        $chd_status = $employee->children;
                        $nhf_status = $employee->nhf;
                        $disability_status = $employee->disability;
                        $other_employee_deductions = $deduction;
                        $salary_advance_gangan = $loan;
                        $other_taxable_benefits = $t_bonus;
                        $other_nontaxable_benefits = $n_bonus;
                        if (isset($salary_advance_gangan)) {
                            $salary_advance = $salary_advance_gangan;
                        } else {
                            $salary_advance = 0;
                        }
                        if (isset($absent_days)) {
                            $absence_unit_deductions = $lve_per_day + $gross_per_day + $housing_per_day + $ent_per_day + $utility_per_day + $meal_per_day + $trn_per_day;
                            $total_absence_deductions = $absence_unit_deductions * $absent_days;
                        } else {
                            $total_absence_deductions = 0;
                        }
                        if (isset($arrear_days)) {
                            $arrear_unit_payments = $lve_per_day + $gross_per_day + $housing_per_day + $ent_per_day + $utility_per_day + $meal_per_day;
                            $total_arrear_payments = $arrear_unit_payments * $arrear_days;
                        } else {
                            $total_arrear_payments = 0;
                        }
                        $other_deductions = $other_employee_deductions + $salary_advance;
                        $tf = 25 / 100;
                        $tpf = 2.5 / 100;
                        $spf = 7.5 / 100;
                        $onepct = 1 / 100;
                        $twtpct = 20 / 100;
                        $annualbenefit = $annualsalary + $transport + $leave + $housing + $meals + $enter + $utility;
                        $total_benefits = $basic_pay + $leave_monthly + $housing_monthly + $ent_monthly + $utility_monthly + $meal_monthly + $trn_monthly + $other_taxable_benefits + $total_arrear_payments;
                        $totalbenefits = $total_benefits - $other_deductions;
                        if ($fixedcra_status == 1) {
                            if ($annualbenefit <= 20000000) {
                                $fixed_cra_monthly = 200000 / 12;
                                $fixed_cra = $fixed_cra_monthly * $division;
                            } else {
                                $one_pct = $onepct / $annualbenefit;
                                $fixed_cra_mth = $one_pct / 12;
                                $fixed_cra = $fixed_cra_mth * $division;
                            }
                        } else {
                            $fixed_cra = 0;
                        }

                        if ($nhf_status == 1) {
                            $nhf = $tpf * $monthlysalary;
                        } else {
                            $nhf = 0;
                        }

                        if ($pension_status == 1) {
                            $htb = $monthlysalary + $hsn + $trn;
                            $pension_init = $spf * $htb;
                            $pension = $pension_init * $division;
                        } else {
                            $pension = 0;
                        }
                        if ($cra_status == 1) {
                            $cra = $twtpct * $total_benefits;
                        } else {
                            $cra = 0;
                        }
                        if ($dr_status == 1) {
                            $dependent_relative_init = 4000 / 12;
                            $dependent_relative = $dependent_relative_init * $division;
                        } else {
                            $dependent_relative = 0;
                        }
                        if ($chd_status == 1) {
                            $children_init = 10000 / 12;
                            $children = $children_init * $division;
                        } else {
                            $children = 0;
                        }
                        if ($disability_status == 1) {
                            $disability_init = $tf * $totalbenefits;
                            $disability = $disability_init * $division;
                        } else {
                            $disability = 0;
                        }

                        $tax_reliefs = $life_assure + $cra + $pension + $fixed_cra + $disability + $dependent_relative + $children;
                        $taxable_pay = $total_benefits - $tax_reliefs;
                        $sevenpercent = 7 / 100;
                        $elevenpercent = 11 / 100;
                        $fifteenpercent = 15 / 100;
                        $nineteenpercent = 19 / 100;
                        $twentyonepercent = 21 / 100;
                        $twentyfourpercent = 24 / 100;
                        $sevenpercentoftwentyfivethousand = $sevenpercent * 25000;
                        $elevenpercentoftwentyfivethousand = $elevenpercent * 25000;
                        $fifteenpercentoffortyonethousand = $fifteenpercent * 41666.67;
                        $sevenpercentoffortyonethousand = $sevenpercent * 41666.67;
                        $twentyonepercentofonethreethree = $twentyonepercent * 133333.33;
                        $value_1 = 25000 * $division;
                        $value_2 = 41666.67 * $division;
                        $value_3 = 133333.33 * $division;
                        $value_4 = 266666.67 * $division;
                        $sevenpercentofvalueone = $sevenpercent * $value_1;
                        if ($taxable_pay > $value_1) {
                            $first = $taxable_pay - $value_1;
                            $tax_1 = $sevenpercentofvalueone;
                        } else {
                            $tax_1 = $taxable_pay * $sevenpercent;
                            $first = 0;
                        }
                        if ($first < $value_1) {
                            $tax_2 = $elevenpercent * $first;
                            $second = 0;
                        } else {
                            $tax_2 = $elevenpercent * $value_1;
                            $second = $first - $value_1;
                        }
                        if ($second < $value_2) {
                            $tax_3 = $fifteenpercent * $second;
                            $third = 0;
                        } else {
                            $third = $second - $value_2;
                            $tax_3 = $fifteenpercent * $value_2;
                        }
                        if ($third < $value_2) {
                            $tax_4 = $nineteenpercent * $third;
                            $fourth = 0;
                        } else {
                            $fourth = $third - $value_2;
                            $tax_4 = $nineteenpercent * $value_2;
                        }
                        if ($fourth < $value_3) {

                            $tax_5 = $twentyonepercent * $fourth;
                            $fifth = 0;
                        } else {
                            $fifth = $fourth - $value_3;
                            $tax_5 = $twentyonepercent * $value_3;
                        }
                        if ($taxable_pay > $value_4) {
                            $tax_6 = $twentyfourpercent * $fifth;
                        } else {
                            $tax_6 = 0;
                        }
                        $onepctoftotalbenefit = $onepct * $totalbenefits;
                        $total_tax = $tax_1 + $tax_2 + $tax_3 + $tax_4 + $tax_5 + $tax_6;
                        //$total_tax = $total_tax_init * $division;
                        if ($onepctoftotalbenefit < $total_tax) {
                            $tax_payable = $total_tax;
                        } else {
                            $tax_payable = $onepctoftotalbenefit;
                        }
                        $totaldeductions = $pension + $nhf + $tax_payable + $salary_advance + $other_employee_deductions;
                        
                        $result[] = [
                            'net_pay' => $total_benefits + $other_nontaxable_benefits - $totaldeductions,
                            'employee_id' => $id,
                            'salary' => $basic_pay,
                            'transport' => $trn_monthly,
                            'leave' => $leave_monthly,
                            'housing' => $housing_monthly,
                            'year' => $yr,
                            'month' => $month,
                            'life_assurance' => $la,
                            'cra' => $cra,
                            'pension' => $pension,
                            'nhf' => $nhf,
                            'dependable_relative' => $dependent_relative,
                            'children' => $children,
                            'disability' => $disability,
                            'fixed_cra' => $fixed_cra,
                            'total_tax_reliefs' => $tax_reliefs,
                            'taxable_pay' => $taxable_pay,
                            'total_gross_payable' => $totalbenefits,
                            'meal' => $meal_monthly,
                            'utility' => $utility_monthly,
                            'entertainment' => $ent_monthly,
                            'payroll_reference' => $reference,
                            'tax' => $tax_payable,
                            'department_id' => $dept,
                            'company_id' => $id_hr_company,
                            'arrears' => $total_arrear_payments,
                            'salary_advance' => $salary_advance,
                            'absence' => $total_absence_deductions,
                            'other_deductions' => $other_employee_deductions,
                            'other_taxable_benefits' => $other_taxable_benefits + $total_arrear_payments,
                            'other_non_taxable_benefits' => $other_nontaxable_benefits,
                            'advance_company_id' => $adv_company_id,
                            'advance_company_uri' => $adv_company_uri
                        ];
                    }
                }
                return json_encode($result, true);
            }else{
                return null;
            }
                
        }
    }

    public static function pendingPayroll(){
        $pending_payroll = PayrollSummary::where('deleted_at', NULL)->where('status', 'pending')->sum('amount');
        $pending_payroll = $pending_payroll;
        return $pending_payroll;
    }

    public static function undisbursedExpenses(){
        $undisbursed_expenses = OtherExpense::where('deleted_at', NULL)->where('disbursed', 'false')->sum('amount');
        $undisbursed_expenses = $undisbursed_expenses;
        return $undisbursed_expenses;
    }

    public static function pendingProcurement(){
        $undisbursed_procurement = RequestItems::where('rfq_status', '1')->where('state', 'active')->sum('total');
        $undisbursed_procurement = $undisbursed_procurement;
        return $undisbursed_procurement;
    }

    public static function disbursedPayroll(){
        $disbursed_payroll = PayrollSummary::where('deleted_at', NULL)->where('status', 'paid')->sum('amount');
        $disbursed_payroll_payroll = $disbursed_payroll;
        return $disbursed_payroll;
    }

    public static function disbursedExpenses(){
        $undisbursed_expenses = OtherExpense::where('deleted_at', NULL)->where('disbursed', 'true')->sum('amount');
        $undisbursed_expenses = $undisbursed_expenses;
        return $undisbursed_expenses;
    }

    public static function disbursedProcurement(){
        $undisbursed_procurement = RequestItems::where('rfq_status', '0')->where('state', 'active')->sum('total'); //here, state column serve as deleted_at #softDelete technique
        $undisbursed_procurement = $undisbursed_procurement;
        return $undisbursed_procurement;
    }

    //Dashboard total payables method
    public function totalPayables(){
        //summ all undisursed procurement and undisbursed expenses payroll deyve not paid
        $pending_payroll = self::pendingPayroll();
        $pending_expenses = self::undisbursedExpenses();
        $undisbursed_procurement = self::pendingProcurement();
        $total_payables = $pending_payroll + $pending_expenses + $undisbursed_procurement;
        return $total_payables;
    }

    //Dashboard total expenses method
    public function totalExpenses(){
        //summ all undisursed procurement and undisbursed expenses payroll deyve paid
        $disbursed_payroll = self::disbursedPayroll();
        $disbursed_expenses = self::disbursedExpenses();
        $disbursed_procurement = self::disbursedProcurement();
        $total_expenses = $disbursed_payroll + $disbursed_expenses + $disbursed_procurement;
        return $total_expenses;
    }

    public static function generateReference()
    {
        $date = strtotime(date("d.m.y"));
        $reference = $date.mt_rand(10000000, 99000000);
        return $reference;
    }

    public static function cleanData($a) {

        if(preg_match("/^[0-9,]+$/", $a)){
            $a = str_replace(',','',$a);
        }
        return $a;

    }

    // Fetch all countries list
    public static function getCountries() {
        try {
            $query = "SELECT id, name FROM countries";
            $results = DB::select($query);
            if(!$results) {
                throw new exception("Country not found.");
            }
            $res = array();
            foreach ($results as $result){
                $res[$result->id] = $result->name;
            }
            $data = array('status'=>'success', 'tp'=>1, 'msg'=>"Countries fetched successfully.", 'result'=>$res);
        } catch (Exception $e) {
            $data = array('status'=>'error', 'tp'=>0, 'msg'=>$e->getMessage());
        } finally {
            return $data;
        }
    }

     public static function countries() {
        try {
            $query = "SELECT id, name FROM countries";
            $results = DB::select($query);
            if(!$results) {
                throw new exception("Country not found.");
            }
            $data = $results;
        } catch (Exception $e) {
            $data = null;
        } finally {
            return $data;
        }
    }


    public function approvedOrDeclinedExpense(Request $request, $approved_or_declined){
            $method = $request->isMethod('post');
            $categories = AccountHeader::all();
            $other_expenses = \DB::select('SELECT id, title, amount, description, status, created_by, approved_by, created_at, updated_at, account_header_id,
                                      (SELECT name FROM account_headers WHERE id = account_header_id ) as category, 
                                      (SELECT first_name FROM hr_employee WHERE id = approved_by) as approved_by  FROM other_expenses WHERE status = "approved" AND created_by = '.auth()->user()->id);
    }

    // Fetch all states list by country id
    public static function getStates($countryId) {
        try {
            $query = "SELECT id, name FROM states WHERE country_id=".$countryId;
            $results = DB::select($query);
            $data = array('status'=>'success', 'tp'=>1, 'msg'=>"States fetched successfully.", 'result'=>$results);
        } catch (Exception $e) {
            $data = array('status'=>'error', 'tp'=>0, 'msg'=>$e->getMessage());
        } finally {
            return $data;
        }
    }

    // Fetch all cities list by state id
    public static function getCities($stateId) {
        try {
            $query = "SELECT id, name FROM cities WHERE state_id=".$stateId;
            $results = DB::select($query);
            $data = array('status'=>'success', 'tp'=>1, 'msg'=>"Cities fetched successfully.", 'result'=>$results);
        } catch (Exception $e) {
            $data = array('status'=>'error', 'tp'=>0, 'msg'=>$e->getMessage());
        } finally {
            return $data;
        }
    }

    public static function lastnweeks($past_weeks = 7, $date = null) {
        $relative_time = strtotime($date);
        $weeks = array();
        for($week_count=0; $week_count<=$past_weeks; $week_count++) {
            $monday = strtotime("last Monday", $relative_time);
            $sunday = strtotime("Sunday", $monday);
            $weeks[] = array(
                date("Y-m-d", $monday),
                date("Y-m-d", $sunday),
            );
            $relative_time = $monday;
        }
        return $weeks;
    }


}