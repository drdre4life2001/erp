<?php
namespace App\Libraries;
use App\Models\UserPermission;
use App\Models\Employee;
use App\Models\EmployeeAbsence;
use App\Models\EmployeeArrear;
use App\Models\EmployeeBonus;
use App\Models\EmployeeDeduction;
use App\Models\EmployeeLoan;
use App\Models\EmployeeRenumeration;
use App\Models\MonthDay;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Ledger;
use App\Models\PayrollSummary;
use App\Models\BankActivity;
use App\Models\OtherExpense;
use App\Models\RequestItems;
use App\Models\ApiLog;
use phpDocumentor\Reflection\Types\Self_;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\BadResponseException;
use App\Libraries\Utilities;

/**
 * Created by PhpStorm.
 * User: nurudeen
 * Date: 16/July/17
 * Time: 10:21 AM
 */ 

class ApiServices
{

    public static function callAPI($post_data = null, $uri = null, $message = null, $method = null){
        $client = Utilities::guzzleClient();
        $result = null;
        $app_api_uri = env("API_URI"); 

        if(isset($app_api_uri)){
            $app_api_uri = env("API_URI");
        }else{
            $app_api_uri = "http://104.131.174.54:7171/api/v1.0";
        }
        $uri = $app_api_uri.$uri;

        try{

            switch ($method){

                case "POST" :
                    $response = $client->post($uri, [
                        'body' => json_encode($post_data)
                    ]);

                    $result = json_decode($response->getBody()->getContents(), true);
                    self::saveToApiLogs($post_data, $result, $result['code'], $result['message'], $result['api_ref'], $method, $uri);
                    return $result;
                    break;
                    
                case "GET":
                    $response = $client->get($uri);
                    $result = json_decode($response->getBody()->getContents(), true);
                    self::saveToApiLogs($post_data, $result, $result['code'], $result['message'], $result['api_ref'], $method, $uri);

                    return $result;
                    break;

                case "DELETE":
                    $response = $client->delete($uri);
                    self::saveToApiLogs($post_data, $result, $result['code'], $result['message'], $result['api_ref'], $method, $uri);
                    return $response->getStatusCode();
                    break;

                case "PATCH":
                    $request = $client->patch($uri, [
                        'body' => json_encode($post_data)
                    ]);
                    $result = json_decode($request->getBody()->getContents(), true);
                    return $result;
                    self::saveToApiLogs($post_data, $result, $result['code'], $result['message'], $result['api_ref'], $method, $uri);
                    return $response->getStatusCode();
                    break;

                case false:
                default;
            }

        } catch(BadResponseException $ex) {

            if ($ex->hasResponse()) {
                $result = json_decode($ex->getResponse()->getBody()->getContents(), true);
                self::saveToApiLogs($post_data, $result, $result['code'], $result['message'], $result['api_ref'], $method, $uri);
                return $result;
            }

        }
    }

    public static function saveToApiLogs($request = [], $response = [], $status_code = null, $message = null, $api_ref = null, $method = null, $end_point = null){
        $save = ApiLog::create([
            'request' => json_encode($request),
            'response' => json_encode($response),
            'status_code' => $status_code,
            'message' => $message,
            'api_ref' => $api_ref,
            'method' => $method,
            'end_point' => $end_point
        ]);
        return $save;
    }

    public static function getIncomeSources($account_type_id){ //get income source by account type. 2 for Organizations, 1 for personal
        $income_sources = self::callAPI(null, "/income-sources?account_type=$account_type_id", NULL, "GET");
        return $income_sources;
    }

    public static function getExpenses($account_type_id, $expense_category){ //get income source by account type. 2 for Organizations, 1 for personal
        $expenses = self::callAPI(null, "/expense-items?account_type=$account_type_id&expense_category=$expense_category", NULL, "GET");
        return $expenses;
    }

    public static function incomeSources($id = null){
        $sources = null;
        if(is_null($id)){
            $sources = DB::select("SELECT * FROM income_sources");
        }else{
            $sources = DB::select("SELECT name FROM income_sources WHERE id = $id");
            $sources = $sources[0]->name;
        }
        
        return $sources;
    }

    public static function expenses($type, $id = null){
        $expenses = null;
        if(is_null($id)){
            if($type == "FIXED"){
                $expenses = DB::SELECT("SELECT * FROM fixed_expenses");
            }elseif($type == "VARIABLE"){
                $expenses = DB::SELECT("SELECT * FROM variable_expenses");
            }
        }else{
            if($type == "FIXED"){
                $expenses = DB::SELECT("SELECT * FROM fixed_expenses WHERE id = $id");
            }elseif($type == "VARIABLE"){
                $expenses = DB::SELECT("SELECT * FROM variable_expenses WHERE id = $id");
            }
            $expenses = $expenses[0]->name;
        }
        
        return $expenses;
    }

    public static function getAccounts($customer_uid)
    {
        $customer_accounts = self::callApi(null, "/customers/".$customer_uid."?expand=accounts",  null, "GET");

        $customer_accounts = (object) $customer_accounts['your_response'];

        $accounts = [];

        foreach ($customer_accounts->accounts as $acc) {
            $accounts[] = [
                'account_name' => $acc['name'],
                'account_number' => $acc['account_number'],
                'ledger_balance' => number_format($acc['ledger_balance'], 2),
                'minimum_balance' => number_format($acc['minimum_balance'], 2),
                'account_category' => $acc['category']['name'],
                'status' => $acc['status']
            ];
        }

        return $accounts;
    }

    public static function getOrganizations($customer_id = null, $organization_id = null){

    }



}

?>