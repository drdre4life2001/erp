<?php
use Illuminate\Support\Facades\DB;
/**
 * MmuoDev
 *
 * To use the checkPermission middleware, simply pass the resource id to the middleware.
 * Hence, the user can only see routes (of which he has permission to view)
 * The ids of the resource are gotten from sys_modules table
 * You need to call auth middleware first, as it will be needed by checkPermission.
 *
 */
use App\Models\Invoice;
// Authentication Routes...
Route::get('login', 'Auth\CustomAuthController@showLoginForm');
Route::post('login', 'Auth\CustomAuthController@authenticate');
Route::get('logout', 'Auth\CustomAuthController@logout');
// Registration Routes...
Route::get('register', 'Auth\CustomRegistrationController@showRegistrationForm');
Route::post('register', 'Auth\CustomRegistrationController@register');
// Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\CustomPasswordController@showResetForm');
Route::post('password/email', 'Auth\CustomPasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\CustomPasswordController@reset');
Route::get('/test', function(){

});
Route::get('trash/department/{id}/{company_uri}', 'DepartmentController@destroy');

 Route::get('/cities/{state_id}', 'RequestController@getStateCities');
Route::get('/states/{country_id}', 'RequestController@getCountryStates');

Route::group(['prefix' => 'profile'],  function(){
    Route::get('/', 'ProfileController@index')->name('profile');
    Route::post('/add-jira', 'ProfileController@add_jira');
    Route::post('/update-jira', 'ProfileController@update_jira');
    ROute::match(['get', 'post'], '/user-details', 'ProfileController@viewProfile');
});

//Routes for the dashboard
Route::get('/', 'HomeController@home');
Route::get('/home', 'HomeController@home')->name('home');
Route::get('/hr_dashboard',  'HomeController@hr_new')->
middleware(['auth','checkPermission:2']);
Route::get('/procure_dashboard',  'HomeController@procure_new')->
middleware(['auth','checkPermission:6']);
Route::get('/finance_dashboard', 'HomeController@finance_new')->
middleware(['auth','checkPermission:3']);
Route::get('/crm_dashboard', 'HomeController@crm')->
middleware(['auth','checkPermission:10']);

Route::get('procurement_dashboard', 'HomeController@procurement');
Route::get('project_dashboard', 'HomeController@project');
Route::get('employees_dashboard', 'HomeController@employee');

//Route::auth();
//Custom login routes


//edit departments

Route::get('/employees-ajax', 'EmployeeController@employeeAjax');

Route::get('/department-ajax', 'ReportsController@departmentAjax');

Route::get('/work-address-ajax', 'EmployeeController@workAddressAjax');

Route::get('/job-location-ajax', 'EmployeeController@jobLocationAjax');

Route::match(['get', 'post'], 'departments/edit/{id}','DepartmentController@editDepartment');

//edit grades

Route::match(['get', 'post'], 'grades/edit/{id}','GradeController@editGrade');

//edit jobs
Route::match(['get', 'post'], 'jobs/edit/{id}','JobController@editJob');

//edit institutions
Route::match(['get', 'post'], 'institutions/edit/{id}','InstitutionController@editInstitution');

//edit institutionsDepartments
Route::match(['get', 'post'], 'institution_departments/edit/{id}','InstitutionDepartmentController@editInstitutionDepartment');

//Routes for CRM Controller
Route::resource('/lead', 'LeadController');
// Route::get('/lead', 'LeadController@index');

Route::get('/lead/destroy/{id}', 'LeadController@destroy');
Route::get('/lead/grid', 'LeadController@grid');

Route::group(['prefix' => 'crm'], function(){

    Route::match(['post', 'get'], 'prospects/create', ['uses' => 'CRMController@create', 'as' => 'crm_create']);
    Route::get('/prospects', 'CRMController@index')->name('prospects');

    Route::get('/leads/grid', 'LeadsController@grid');
    Route::get('/lead/grid', 'LeadController@grid');
    Route::post('/prospects/activity/add', 'CRMController@store_activity');
    Route::get('/prospects/activity/{id}', 'CRMController@view_activity')->name('view_activity');
    Route::post('/prospects/update', 'CRMController@update_prospect');
    Route::get('/pipelines', 'CRMController@pipelines');
    Route::post('/pipeline/update', 'CRMController@pipeline_update');
    Route::get('/report/by-user', 'CRMController@by_user');
});

//Routes end

Route::group(['prefix' => 'other-expenses'], function(){
    Route::group(['middleware' => ['auth','checkPermission:4']],  function() {
        Route::match(['get', 'post'], '/all', 'OtherExpenseController@allExpenses');
    });
    Route::match(['get','post'], '/', 'OtherExpenseController@index');
    Route::post('/update/{id}', 'OtherExpenseController@update');
    Route::get('approveExpense/{expense_id}', 'OtherExpenseController@approveExpense');
    Route::get('declineExpense/{expense_id}', 'OtherExpenseController@declineExpense');
    Route::get('/approved-expenses', 'OtherExpenseController@approvedExpenses');
    Route::get('/declined-expenses', 'OtherExpenseController@declinedExpenses');
});

//Routes for finance controller
Route::group(['middleware' => ['auth','checkPermission:3']], function(){
    Route::get('clients', 'RequestController@clients')->name('clients');
    Route::get('add_client', 'RequestController@add_client')->name('add_client');
    Route::post('add_client', 'RequestController@add_client')->name('add_client');
    Route::post('update_client', 'RequestController@update_client')->name('update_client');
    Route::get('update_client/{id}', 'RequestController@update_client')->name('update_client');
    Route::get('delete-client/{id}/{type}', 'HumanResourceController@delete_record')->name('delete_record');
});


Route::get('bulk-upload-excel-csv-download', function(){
    $headers = array( 'Content-Type' => 'application/vnd.ms-excel.sheet.macroEnabled.12');
    return Response::download('downloads/bulk_bank_accounts_upload.xlsx', 'bulk_upload.xlsx', $headers);
});

Route::group(['prefix' => 'finance', 'middleware' => ['auth','checkPermission:3']],  function(){
    Route::post('/add-salvage', 'RequestController@addSalvage');
    
    Route::post('/invoice/payment', 'InvoiceController@add_payment');

    Route::post('items/totalItemsAjax', 'ItemController@totalItemAjax');
    Route::post('invoice/sendPdf/{id}', 'InvoiceController@sendPdf');
    Route::get('invoice/print_pdf/{id}/{company_uri}/{download}', 'InvoiceController@printInvoice')->name('htmltopdfview');
    Route::resource('invoice', 'InvoiceController');
    Route::match(['get', 'post'], '/budget/create', 'BudgetController@create')->name('create-budget');
    Route::match(['get', 'post'], '/budget/view/{account_number}/{budget_uuid}', 'BudgetController@viewBudget');
    Route::match(['get', 'post'], '/account/create', 'AccountController@createAccount');
    Route::match(['get', 'post'], '/account-effects', 'AccountController@createEffect');
    Route::patch('budget-update/{account_number}/{budget_uid}', 'BudgetController@updateBudget');
    Route::get('/accounts/view/budget/{account_number}', 'AccountController@viewAccountBudget');
    Route::get('/account/delete/{account_number}', 'AccountController@deleteAccount')->name('delete-account');
    
    Route::get('/budget/trash/{account_number}/{budget_uuid}', 'BudgetController@deleteBudget');
    Route::post('/invoice/update/{id}', 'InvoiceController@updateInvoice');
    Route::resource('tax', 'TaxController');
    Route::match(['get', 'post'], '/tax', 'TaxController@index');
    Route::post('/tax/edit', 'TaxController@updateTax');
    Route::get('/tax/delete/{id}', 'TaxController@destroy');
    Route::match(['post', 'get'], '/bank-account/bulk-upload', 'FinanceController@bulk_upload');
    Route::get('/download/bulk-upload', 'FinanceController@download_bulk_template');
    Route::get('/download/remittance', 'FinanceController@download_remittance');
    Route::post('/upload/remittance', 'FinanceController@upload_remittance');
    Route::match(['post', 'get'], '/remittance', ['uses' => 'FinanceController@remittance', 'as' => 'remittance']);
    Route::post('/remittance/update', 'FinanceController@update_remittance');
    Route::match(['post', 'get'], '/add-revenue', ['uses' => 'FinanceController@add_revenue']);
    Route::match(['post', 'get'], '/revenue', ['uses' => 'FinanceController@revenue', 'as' => 'revenue']);
    Route::post('/revenue/update', 'FinanceController@revenue_update');
    Route::get('/dashboard', 'FinanceController@dashboard');
    Route::get('bank_accounts/statement/{id}', 'FinanceController@bank_statement');
    Route::get('ledger', 'FinanceController@ledger')->name('ledger');
    Route::post('get-balance', 'FinanceController@get_balance')->name('get-balance');
    Route::post('get-bank-details', 'FinanceController@get_bank_details')->name('get-bank-details');
    Route::get('get-sub-headers/{header_id}', 'FinanceController@get_sub_headers')->name('get-sub-headers');
    Route::get('/disburse/{id}', 'FinanceController@disburse')->name('disburse');
    Route::post('/process/disburse', 'FinanceController@processDisburse');
    Route::match(['post', 'get'], '/banks', ['uses' => 'FinanceController@banks', 'as' => 'banks']);
    Route::post('/bank/add_bank_account', 'FinanceController@add_bank_account');
    Route::post('/bank/create_bank', 'FinanceController@addBankAccount');
    
    Route::post('/banks/edit', 'FinanceController@banks_edit');
    Route::post('/effects/edit', 'AccountController@effects_edit');
    Route::get('/effect/delete/{id}', 'AccountController@effects_delete');

    Route::get('/banks/delete/{id}', 'FinanceController@banks_delete');
    Route::get('/ledger/delete/{id}', 'FinanceController@ledger_delete');
    Route::post('/ledger/delete', 'FinanceController@banks_edit');
    Route::get('disbursement/procurement', 'FinanceController@approved_procurement_items');
    Route::match(['get', 'post'], 'disbursement/other-expense', 'FinanceController@disburseOtherExpense');

    Route::match(['post', 'get'], '/bank_accounts', ['uses' => 'FinanceController@bank_accounts', 'as' => 'bank_accounts']);
    Route::post('/bank/accounts/edit', 'FinanceController@bank_accounts_edit');
    Route::post('/bank/accounts/delete', 'FinanceController@bank_accounts_delete');
    Route::post('/expense_report', 'ReportsController@expense_sheet');
    Route::get('/expense_report', 'ReportsController@expense_sheet');
    Route::post('/expense_pivot', 'ReportsController@expense_pivot');
    Route::get('/expense_pivot', 'ReportsController@expense_pivot');

    Route::match(['post', 'get'], '/account_header', ['uses' => 'FinanceController@account_header', 'as' => 'account_header']);
    Route::post('/account_header/edit', 'FinanceController@account_header_edit');
    Route::get('/account_header/delete/{id}', 'FinanceController@account_header_delete');

    Route::match(['post', 'get'], '/account_sub_header', ['uses' => 'FinanceController@account_sub_header', 'as' => 'account_sub_header']);
    Route::post('/account_sub_header/edit', 'FinanceController@account_sub_header_edit');
    Route::get('/account_sub_header/delete/{id}', 'FinanceController@account_sub_header_delete');

    Route::match(['post','get'], '/operating_expenses', ['uses' => 'FinanceController@operate', 'as' => 'operating_expenses']);
    Route::post('/operating_expenses/edit', 'FinanceController@operate_edit');
    Route::get('/operating_expenses/delete/{id}', 'FinanceController@operate_delete');

    Route::match(['post','get'], '/salvage_items', ['uses' => 'FinanceController@salvage', 'as' => 'salvage_items']);
    Route::post('/salvage_items/edit', 'FinanceController@salvage_edit');
    Route::get('/salvage_items/delete/{id}', 'FinanceController@salvage_delete');

    Route::match(['post','get'], '/capital_expenses', ['uses' => 'FinanceController@capital', 'as' => 'capital_expenses']);
    Route::post('/capital_expenses/edit', 'FinanceController@capital_edit');
    Route::get('/capital_expenses/delete/{id}', 'FinanceController@capital_delete');

    Route::match(['get', 'post'], '/company-loan/index', ['uses' => 'FinanceController@createCompanyLoan']);
    Route::get('view-payment-schedule/{loan_id}', 'FinanceController@viewPaymentSchedule');
    Route::match(['get', 'post'], 'add-loan-repayment/{loan_id}', 'FinanceController@addLoanRepayment');
    Route::get('disbursment/salaries', 'FinanceController@disbursementSalary');
    Route::match(['get', 'post'], 'disbursment/tax', 'FinanceController@payTax');
    Route::get('/bank/{id}', 'FinanceController@bankBalance');
    Route::match(['get', 'post'], '/account_header/{id}', 'FinanceController@accountSubHeader');

    Route::match(['post', 'get'], '/add-revenue-stream/', ['uses' => 'FinanceController@add_revenue_stream']);
    Route::get('/revenue-streams', 'FinanceController@revenue_stream')->name('revenue_streams');
    Route::post('revenue_stream/edit', 'FinanceController@revenue_stream_edit');
    Route::get('revenue_stream/delete/{id}', 'FinanceController@revenue_stream_delete');
});

//Routes for Procurement

Route::group(['middleware' => ['auth','checkPermission:6']],  function(){
    Route::post('select-subsidiary', 'RequestController@select_subsidiary')->name('select-subsidiary');
    Route::get('add_requests', 'RequestController@new_requests')->name('add_requests');
    Route::match(['get', 'post'], '/save_requests', 'RequestController@save_requests')->name('save_requests');
    Route::get('requests/create', 'RequestController@create')->name('create'); //Create a procurement
    Route::get('requests/showRequests', 'RequestController@showRequests')->name('showRequests');
    Route::post('create_request', 'RequestController@store')->name('create_request');
    Route::get('request/details/{id}', 'RequestController@request_details')->name('request_details'); //Show all items for this request
    Route::post('request/items/edit', 'RequestController@edit_item');
    Route::post('request/items/delete', 'RequestController@delete_item');

    Route::get('requests/lineManagerRequests', 'RequestController@lineManagerRequests')->name('lineRequests'); //Pending requests for a line manager
    Route::get('lineManagerRequests/details/{id}', 'RequestController@lineManagerRequestsDetails')->name('lineManagerRequests');
    Route::post('request_update', 'RequestController@request_update');
    Route::get('update_request_details/{id}/{request_id}', 'RequestController@update_request_details');
    Route::post('request/delete', 'RequestController@request_delete')->name('request_delete'); //soft delete a request including her items
    Route::get('request/send/{id}', 'RequestController@request_send')->name('request_send'); //Send email notifications to concerned parties
    Route::post('lineManagerUpdateRequests', 'RequestController@lineManagerUpdateRequests');
    Route::post('/getCountries', 'RequestController@getCountries')->name('getCountries');
    Route::post('/getStates/{id}', 'RequestController@getStates')->name('getStates');
    Route::post('/getCities/{id}', 'RequestController@getCities')->name('getCities');
    Route::get('requests/cLevelRequests', 'RequestController@cLevelRequests')->name('cLevelRequests'); //Pending requests for a c-level manager
    Route::get('cLevelRequests/details/{id}', 'RequestController@cLevelRequestsDetails')->name('cLevelRequestsDetails');
    Route::get('clevel_update_request/{id}/{request_id}', 'RequestController@clevel_update_request');
    Route::post('clevel_request_update', 'RequestController@clevel_request_update');
    Route::post('cLevelUpdateRequests', 'RequestController@cLevelUpdateRequests');
    Route::post('requests/rfq/send', 'RequestController@send_rfq');
    Route::match(['post', 'get'], 'procurement/reports', 'RequestController@viewReports');
    Route::get('vendors', 'RequestController@vendors')->name('vendors');
    Route::post('add_vendor', 'RequestController@add_vendor')->name('add_vendor');
    Route::post('/vendor/edit', 'RequestController@update_vendor');
    Route::get('request-category/delete/{id}', 'RequestCategoryController@destroy');
    Route::get('add_vendor', 'RequestController@add_vendor')->name('add_vendor');
    Route::get('update_vendor/{id}', 'RequestController@update_vendor')->name('update_vendor');
    Route::post('update_vendor', 'RequestController@update_vendor')->name('update_vendor');


});
//Routes  for Procurement ends

//Routes for System Settings
Route::group(['middleware' => ['auth','checkPermission:1']],  function() {
    Route::get('permissions', 'UserPermissionsController@index');
    Route::get('permissions/create', 'UserPermissionsController@createPermissions');
    Route::post('permissions/store', 'UserPermissionsController@storePermissions');

    Route::get('roles', 'RoleController@index');
    Route::get('roles/create','RoleController@create');
    Route::match(['get', 'post'], 'roles/edit/{id}','RoleController@editRole');

    Route::post('roles/store', 'RoleController@store');
    Route::get('roles/destroy/{id}', 'RoleController@destroy');
});
//Routes for HR
Route::group(['middleware' => ['auth','checkPermission:2']],  function() {
    Route::get('employee/dashboard', 'EmployeeController@dashboard');
    Route::get('/organizations-ajax', 'OrganizationController@organizationAjax');
    Route::get('/clients-ajax', 'OrganizationController@clientAjax');
    Route::get('/revenue-stream-ajax', 'FinanceController@streamAjax');
    Route::post('save_experience', 'EmployeeController@save_experience')->name('save_experience');
    Route::get('employee_bonuses', 'HumanResourceController@employee_bonuses')->name('employee_bonuses');
    Route::post('add_employee_bonus', 'HumanResourceController@add_employee_bonus')->name('add_employee_bonus');
    Route::post('add_employee_renumeration', 'HumanResourceController@add_employee_renumeration')->name('add_employee_renumeration');
    Route::get('add_employee_renumeration', 'HumanResourceController@add_employee_renumeration')->name('add_employee_renumeration');
    Route::get('update_employee_renumeration/{id}/{company_id}', 'HumanResourceController@update_employee_renumeration')->name('update_employee_renumeration');
    Route::post('update_employee_renumeration', 'HumanResourceController@update_employee_renumeration')->name('update_employee_renumeration');
    Route::get('add_employee_bonus', 'HumanResourceController@add_employee_bonus')->name('add_employee_bonus');
    Route::get('add_work_days', 'HumanResourceController@add_work_days')->name('add_work_days');
    Route::post('add_work_days', 'HumanResourceController@add_work_days')->name('add_work_days');
    Route::post('employee_bonus/{id}', 'HumanResourceController@employee_bonuses')->name('employee_bonus');
    Route::get('employee_deductions', 'HumanResourceController@employee_deductions')->name('employee_deductions');
//    Route::get('work_days', 'HumanResourceController@work_days')->name('work_days');

    Route::match(['get', 'post'], 'work_days', 'HumanResourceController@work_days')->name('work_days');


    Route::get('employee_renumerations', 'HumanResourceController@employee_renumerations')->name('employee_renumerations');
    Route::get('payslip/{employee_id}/{month}/{year}/{advance_company_uri}', 'HumanResourceController@payslip')->name('payslip');
    Route::get('download_payslip/{employee_id}/{month}/{year}', 'HumanResourceController@download_payslip')->name('download_payslip');
    Route::match(['get', 'post'], 'employee_absences', 'HumanResourceController@add_employee_absence')->name('employee_absences');
    Route::match(['get', 'post'], 'employee_loans', 'HumanResourceController@employee_loans')->name('employee_loans');
    // Route::get('employee_arrears', 'HumanResourceController@employee_arrears')->name('employee_arrears');
    Route::post('add_employee_deduction', 'HumanResourceController@add_employee_deduction')->name('add_employee_deduction');
    Route::get('add_employee_arrears', 'HumanResourceController@add_employee_arrears')->name('add_employee_arrears');
    // Route::post('add_employee_arrears', 'HumanResourceController@add_employee_arrears')->name('add_employee_arrears');
    Route::get('generate_payroll', 'HumanResourceController@generate_payroll')->name('generate_payroll');
    Route::post('generate_payroll', 'HumanResourceController@generate_payroll')->name('generate_payroll');
    Route::post('add_employee_absence', 'HumanResourceController@add_employee_absence')->name('add_employee_absence');
    Route::match(['get', 'post'], 'employee_arrears', 'HumanResourceController@employee_arrears')->name('employee_loans');
 
    // Route::post('add_employee_loan', 'HumanResourceController@add_employee_loan')->name('add_employee_loan');
    // Route::get('add_employee_loan', 'HumanResourceController@add_employee_loan')->name('add_employee_loan');
    Route::post('send_payslip', 'HumanResourceController@send_payslip')->name('send_payslip');
    Route::get('send_payslip', 'HumanResourceController@send_payslip')->name('send_payslip');
    Route::post('paye_report', 'ReportsController@paye_report')->name('paye_report');
    Route::get('paye_report', 'ReportsController@paye_report')->name('paye_report');
    Route::post('department_report', 'ReportsController@department_report')->name('department_report');
    Route::get('department_report', 'ReportsController@department_report')->name('department_report');
    Route::post('pension_report', 'ReportsController@pension_report')->name('pension_report');
    Route::get('pension_report', 'ReportsController@pension_report')->name('pension_report');
    Route::get('add_employee_deduction', 'HumanResourceController@add_employee_deduction')->name('add_employee_deduction');
    Route::get('add_employee_absence', 'HumanResourceController@add_employee_absence')->name('add_employee_absence');
    Route::get('delete_record/{id}/{type}', 'HumanResourceController@delete_record')->name('delete_record');
    Route::post('save_referee', 'EmployeeController@save_referee')->name('save_referee');
    Route::post('update_bio', 'EmployeeController@update_bio_data')->name('update_bio');
    Route::post('change_picture', 'EmployeeController@change_picture')->name('change_picture');
    Route::get('export_payroll/{month}/{year}/{type}/{subsidiary_id}/{advance_user_uri}', 'HumanResourceController@export_payroll')->name('export_payroll');
    Route::post('save_attachment', 'EmployeeController@save_attachment')->name('save_attachment');
    Route::get('delete_experience/{id}', 'EmployeeController@delete_experience')->name('delete_experience');
    Route::get('loan_settlements', 'HumanResourceController@employee_loan_settlements')->name('loan_settlements');
    Route::get('payrolls', 'HumanResourceController@payrolls')->name('payrolls');
    Route::get('leave_types', 'HumanResourceController@leave_types')->name('leave_types');
    Route::get('confirm_payroll/{month}/{year}/{subsidiary_id}/{advance_company_uri}', 'HumanResourceController@confirm_payroll')->name('confirm_payroll');
    // Route::get('employee_leaves', 'HumanResourceController@employee_leaves')->name('employee_leaves');
    Route::match(['get', 'post'], '/employee_leaves', 'HumanResourceController@employee_leaves')->name('employee_leaves');
    Route::get('draft_payrolls', 'HumanResourceController@draft_payrolls')->name('draft_payrolls');
    Route::post('get_payroll', 'HumanResourceController@payrolls')->name('get_payroll');
    Route::get('view/payroll/{id}', 'HumanResourceController@viewPayroll');
    Route::post('add_leave_type', 'HumanResourceController@add_leave_type')->name('add_leave_type');
    Route::get('add_leave_type', 'HumanResourceController@add_leave_type')->name('add_leave_type');
    Route::get('delete_attachment/{id}', 'EmployeeController@delete_attachment')->name('delete_attachment');
    Route::get('download_attachment/{id}', 'EmployeeController@download_attachment')->name('download_attachment');
    Route::get('delete_referee/{id}', 'EmployeeController@delete_referee')->name('delete_referee');
    Route::get('organization/destroy/{id}', 'OrganizationController@destroy');
    Route::resource('employee', 'EmployeeController');
    Route::get('show-employee/{id}/{company_id}', 'EmployeeController@show');
});
//Routes for HR ends


//Routes for Project Manager
Route::group(['middleware' => ['auth','checkPermission:4']],  function() {
    Route::get('projects', 'ProjectsController@projects')->name('projects');
    Route::post('project/issues', 'ProjectsController@issues');
    Route::post('add_project', 'ProjectsController@add_project')->name('add_project');
    Route::get('add_project', 'ProjectsController@add_project')->name('add_project');
    Route::post('update_project', 'ProjectsController@update_project')->name('update_project');
    Route::get('update_project/{id}', 'ProjectsController@update_project')->name('update_project');
});

//Routes for Project Manager ends
Route::resource('hr', 'HumanResourceController');
Route::resource('settings', 'SettingsController');
Route::resource('bank', 'BankController');
Route::resource('country', 'CountryController');
Route::resource('gender', 'GenderController');
Route::resource('organizations', 'OrganizationController');
Route::match(['get', 'post'], '/organizations', 'OrganizationController@index');

// Route::resource('workAddresses', 'WorkAddressController');
Route::match(['get', 'post'], '/workAddresses', 'WorkAddressController@index');
Route::get('/workAddresses/delete/{id}', 'WorkAddressController@destroy');
Route::post('/workAddress/edit', 'WorkAddressController@updateWorkAddress');

// Route::match(['get', 'post'], '/departments', ['uses' => 'DepartmentCon/troller@index']);

Route::resource('departments', 'DepartmentController');


// Route::resource('departments', 'DepartmentController');
Route::resource('grades', 'GradeController');
Route::resource('jobs', 'JobController');
Route::resource('employeeJobs', 'EmployeeJobController');
Route::resource('employeeDepartments', 'EmployeeDepartmentController');
Route::resource('lineManager', 'LineManagerController');
Route::resource('institutions', 'InstitutionController');
Route::resource('institutionDepartments', 'InstitutionDepartmentController');
Route::resource('employeeEducations', 'EmployeeEducationController');
Route::resource('finances', 'FinanceController');
Route::resource('requestCategories', 'RequestCategoryController');
Route::resource('requestItems', 'RequestItemController');
Route::resource('rolePermissions', 'RolePermissionController');
Route::group(['prefix' => 'erp-users'], function() {
    Route::match(['get', 'post'], '/index', ['uses' => 'UserController@create']);
    Route::match(['get', 'post'], '/show', ['uses' => 'UserController@index']);
    Route::get('/destroy/{id}', ['uses' => 'UserController@destroy']);
    Route::get('/activate/{company_id}/{company_uri}/{user_id}', ['uses' => 'UserController@activate']);
    Route::get('/deactivate/{company_id}/{company_uri}/{user_id}', ['uses' => 'UserController@deactivate']);
    Route::post('/update/{id}', ['uses' => 'UserController@update']);
});
Route::get('finance/back', 'FinanceController@goBack');
Route::resource('payables', 'PayablesController');
Route::get('employee/account/{employeeId}', 'UserController@createUserAccountForEmployee');
Route::controller('payables', 'PayablesController');
Route::get('submit/request/{requestGroupId}', 'PayablesController@submitRequest');
Route::get('approve/request/{requestGroupId}', 'PayablesController@');
Route::get('deny/request/{requestGroupId}', 'PayablesController@denyRequest');
Route::post('register/verify/{email}/{token}', 'UserController@verify');

Route::get('register/verify/{email}/{token}', 'UserController@changePassword');
Route::match(['get', 'post'], 'new-request', ['uses' => 'RequestController@create', 'as' => 'new-request']);

Route::resource('menuGroups', 'MenuGroupController');
Route::resource('menus', 'MenuController');

Route::group(['prefix' => 'leave'], function(){
    Route::match(['get', 'post'], '/request/create', 'LeaveController@create');
});
