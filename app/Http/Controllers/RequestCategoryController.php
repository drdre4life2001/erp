<?php

namespace App\Http\Controllers;

use App\Models\RequestCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use DB;

class RequestCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requestCategories = RequestCategory::where(['advance_company_id' => auth()->user()->company->id, 'advance_company_uri' => auth()->user()->company->uri])->orderBy('created_at', 'DESC')->get();
        return view('erp.finance.request_categories')->with(['requestCategories'=>$requestCategories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = RequestCategory::orderBy('created_at', 'DESC')->get();
        return view('request_categories.create')->with(['cats'=>$cats]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'parent_id' => 'required'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            RequestCategory::create([
                'name' => $request->get('name'),
                'parent_id' => $request->get('parent_id'),
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id,
                'advance_company_id' => auth()->user()->company->id,
                'advance_company_uri' => auth()->user()->company->uri,
            ]);
            return back()->with('success', 'Category added successfully');

        }catch (\Exception $ex){
            return back()->withErrors($ex)->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Fetch details for this particular request id
        $data = RequestCategory::find($id);
        $cats = RequestCategory::where(['advance_company_id' => auth()->user()->company->id, 'advance_company_uri' => auth()->user()->company->uri])->orderBy('created_at', 'DESC')->get();
        return view ('request_categories.edit', compact('cats', 'data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'parent_id' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        //Update the request category
        DB::table('fin_request_category')->where('id', $id)->update([
            'name' => $request->name,
            'parent_id' => $request->parent_id
        ]);
        return redirect('requestCategories')->with('info', 'Request Category Updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use soft delete
        RequestCategory::find($id)->delete();
        return redirect('requestCategories')->with('info', 'Request Category Trashed!');

    }
}
