<?php

namespace App\Http\Controllers;

use App\Libraries\Utilities;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Organization;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ReportsController extends Controller
{
    /**
     * Display PAYE Reports from hr_payrolls table.
     *
     * @return \Illuminate\Http\Response
     */
    public function paye_report(Request $request)
    {
        $adv_company_id = auth()->user()->company->id;
        $adv_company_uri = auth()->user()->company->uri;

        $method = $request->isMethod('post');
        $months = Utilities::getMonths();
        $years = Utilities::getYears();
        if ($method) {
            $mth = $request->month;
            $yr = $request->year;

            $result = DB::select("SELECT (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name, 
                                (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, 
                                tax FROM hr_payrolls WHERE month = '$mth' AND year = '$yr' AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri'");
            $total = DB::select("SELECT SUM(tax) as total FROM hr_payrolls WHERE month = '$mth' AND year = '$yr' AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri'");
            return view('erp.hr.paye_report')->with(['result' => $result, 'total' => $total, 'months' => $months, 'years' => $years]);
        } else {
            return view('erp.hr.paye_report')->with(['months' => $months, 'years' => $years]);
        }
    }

    /**
     * Display Departmental Reports from hr_payrolls table.
     *
     * @return \Illuminate\Http\Response
     */

    public function departmenteAjax(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $company = auth()->user()->company->id;
            $data = DB::table("sys_departments")
                ->select("id","first_name", "lastname")
                ->where('company_id', "$company")
                ->where('first_name','LIKE',"%$search%")
                ->get();
        }

        return response()->json($data);
    }

    public function department_report(Request $request)
    {
        $adv_company_id = auth()->user()->company->id;
        $adv_company_uri = auth()->user()->company->uri;

        $method = $request->isMethod('post');
        $months = Utilities::getMonths();
        $years = Utilities::getYears();
        $departments = Department::where([
            'advance_company_id' => auth()->user()->company->id,
            'advance_company_uri' => auth()->user()->company->uri
        ])->get();
        //var_dump($departments);exit;
        if ($method) {
            $mth = $request->month;
            $yr = $request->year;
            $department = $request->department;
            //Get result and total based on the department selected
            if($department == 'see all'){ //All departments
                $result = DB::select("SELECT (SELECT name FROM sys_departments WHERE id = department_id) as department, 
                                SUM(tax) as tax, SUM(net_pay) as net_pay, SUM(pension) as pension FROM hr_payrolls WHERE month = '$mth' AND year = '$yr' AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri' GROUP BY department");
                $total = DB::select("SELECT SUM(net_pay) as total FROM hr_payrolls WHERE month = '$mth' AND year = '$yr' AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri'");
            }else{ //else per department selected
                $result = DB::select("SELECT (SELECT name FROM sys_departments WHERE id = '$department') as department, 
                                SUM(tax) as tax, SUM(net_pay) as net_pay, SUM(pension) as pension FROM hr_payrolls WHERE department_id = '$department' and month = '$mth' AND year = '$yr' AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri' -- GROUP BY department");
                $total = DB::select("SELECT SUM(net_pay) as total FROM hr_payrolls WHERE department_id = '$department' and month = '$mth' AND year = '$yr' AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri'");
            }


            $depts = [];
            $tax = [];
            foreach ($result as $res) {
                $depts[] = [
                    'name' => (string)$res->department,
                    'y' => (int)$res->net_pay
                ];
            }

            $tax = json_encode($tax);

            return view('erp.hr.department_report')->with(['result' => $result, 'total' => $total, 'months' => $months, 'years' => $years, 'depts' => $depts, 'tax' => $tax, 'departments' => $departments]);
        } else {
            return view('erp.hr.department_report')->with(['months' => $months, 'years' => $years, 'departments' => $departments]);
        }
    }

    /**
     * Display Pension Reports from hr_payrolls table.
     *
     * @return \Illuminate\Http\Response
     */
    public function pension_report(Request $request)
    {
        $adv_company_id = auth()->user()->company->id;
        $adv_company_uri = auth()->user()->company->uri;

        $method = $request->isMethod('post');
        $months = Utilities::getMonths();
        $years = Utilities::getYears();
        if ($method) {
            $mth = $request->month;
            $yr = $request->year;
            $result = DB::select("SELECT (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name, 
                                (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, 
                                pension FROM hr_payrolls WHERE month = '$mth' AND year = '$yr' AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri'");
            $total = DB::select("SELECT SUM(pension) as total FROM hr_payrolls WHERE month = '$mth' AND year = '$yr' AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri'");
            return view('erp.hr.pension_report')->with(['result' => $result, 'total' => $total, 'months' => $months, 'years' => $years]);
        } else {
            return view('erp.hr.pension_report')->with(['months' => $months, 'years' => $years]);
        }
    }

    /**
     * Display Pension Reports from hr_payrolls table.
     *
     * @return \Illuminate\Http\Response
     */
    public function expense_sheet(Request $request)
    {
        $adv_company_id = auth()->user()->company->id;
        $adv_company_uri = auth()->user()->company->uri;

        $method = $request->isMethod('post');
        $start_date = date("Y-m-d", strtotime("first day of previous month"));
        $start_date = $start_date . ' 00:00:00';
        $end_date = date("Y-m-d H:i:s");

        if ($method) {
            $start_date = $request->start_date;
            $end_date = $request->end_date;
            $query = "SELECT (SELECT name FROM fin_request_category WHERE id = (SELECT category FROM pr_request WHERE id = request_id)) as category, 
                                (SELECT name FROM account_sub_headers WHERE id = (SELECT sub_category_id FROM pr_request WHERE id = request_id)) as sub_category, 
                                title, total, created_at FROM request_items WHERE (created_at BETWEEN '$start_date' AND '$end_date') AND rfq_status = 2 AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri'";
            $result = DB::select($query);
            return view('erp.finance.expense_report')->with(['result' => $result, 'start_date' => $start_date, 'end_date' => $end_date]);
        } else {
            $query = "SELECT (SELECT name FROM fin_request_category WHERE id = (SELECT category FROM pr_request WHERE id = request_id)) as category, 
                                (SELECT name FROM account_sub_headers WHERE id = (SELECT sub_category_id FROM pr_request WHERE id = request_id)) as sub_category, 
                                title, total, created_at FROM request_items WHERE (created_at BETWEEN '$start_date' AND '$end_date') AND rfq_status = 2 AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri'";
            $result = DB::select($query);


            return view('erp.finance.expense_report')->with(['start_date' => $start_date, 'end_date' => $end_date, 'result' => $result]);
        }
    }

    /**
     * Display Pension Reports from hr_payrolls table.
     *
     * @return \Illuminate\Http\Response
     */

    public function str_reverse($str, $i)
    {
        return implode($i, array_reverse(explode($i, $str)));
    }
    public function expense_pivot(Request $request)
    {
        
        $adv_company_id = auth()->user()->company->id;
        $adv_company_uri = auth()->user()->company->uri;

        $method = $request->isMethod('post');
        $start_date = date("Y-m-d", strtotime("first day of previous month"));
        $start_date = $start_date . ' 00:00:00';
        $end_date = date("Y-m-d H:i:s");
        if ($method) {
            $start_date = $request->from;
            $end_date = $request->to;
            $query = "SELECT 
                            (SELECT name FROM account_sub_headers WHERE id = (SELECT sub_category_id FROM pr_request WHERE id = request_id)) as sub_category, 
                                 (SELECT id FROM account_sub_headers WHERE id = (SELECT sub_category_id FROM pr_request WHERE id = request_id)) as sub_category_id, 
                                    SUM(total) as total, created_at FROM request_items WHERE (DATE(created_at) BETWEEN '$start_date' AND '$end_date') 
                                    AND rfq_status = 2 AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri' GROUP BY sub_category, request_id";
           $result = DB::select($query);
            $array = [];
            foreach ($result as $res) {
                $last_seven_weeks = Utilities::lastnweeks(7, $res->created_at);
                $week_one = self::get_total_for_a_category($res->sub_category_id,$last_seven_weeks[0][0],$last_seven_weeks[0][1]);
                $week_two = self::get_total_for_a_category($res->sub_category_id,$last_seven_weeks[1][0],$last_seven_weeks[1][1]);
                $week_three = self::get_total_for_a_category($res->sub_category_id,$last_seven_weeks[2][0],$last_seven_weeks[2][1]);
                $week_four = self::get_total_for_a_category($res->sub_category_id,$last_seven_weeks[3][0],$last_seven_weeks[3][1]);
                $week_five = self::get_total_for_a_category($res->sub_category_id,$last_seven_weeks[4][0],$last_seven_weeks[4][1]);
                $week_six = self::get_total_for_a_category($res->sub_category_id,$last_seven_weeks[5][0],$last_seven_weeks[5][1]);

                $array[] = [
                    'sub_category' => $res->sub_category,
                    'week_one' => $week_one->total,
                    'week_two' => $week_two->total,
                    'week_three' => $week_three->total,
                    'week_four' => $week_four->total,
                    'week_five' => $week_five->total,
                    'week_six' => $week_six->total,
                    'total' => $res->total
                ];
            }
            if(empty($result)){
                return back()->with('error', 'No sheet available for the selected date range!');
            }
            return view('erp.finance.expense_pivot')->with(['start_date' => $start_date, 'end_date' => $end_date, 'result' => $array]);
        } else {
            $query = "SELECT 
                            (SELECT name FROM account_sub_headers WHERE id = (SELECT sub_category_id FROM pr_request WHERE id = request_id)) as sub_category, 
                                 (SELECT id FROM account_sub_headers WHERE id = (SELECT sub_category_id FROM pr_request WHERE id = request_id)) as sub_category_id, 
                                    SUM(total) as total, created_at FROM request_items WHERE (created_at BETWEEN '$start_date' AND '$end_date') 
                                    AND rfq_status = 2 AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri' GROUP BY sub_category, request_id";
            $result = DB::select($query);
            $array = [];
            foreach ($result as $res) {
                $last_seven_weeks = Utilities::lastnweeks(7, $res->created_at);
                $week_one = self::get_total_for_a_category($res->sub_category_id,$last_seven_weeks[0][0],$last_seven_weeks[0][1]);
                $week_two = self::get_total_for_a_category($res->sub_category_id,$last_seven_weeks[1][0],$last_seven_weeks[1][1]);
                $week_three = self::get_total_for_a_category($res->sub_category_id,$last_seven_weeks[2][0],$last_seven_weeks[2][1]);
                $week_four = self::get_total_for_a_category($res->sub_category_id,$last_seven_weeks[3][0],$last_seven_weeks[3][1]);
                $week_five = self::get_total_for_a_category($res->sub_category_id,$last_seven_weeks[4][0],$last_seven_weeks[4][1]);
                $week_six = self::get_total_for_a_category($res->sub_category_id,$last_seven_weeks[5][0],$last_seven_weeks[5][1]);

                $array[] = [
                    'sub_category' => $res->sub_category,
                    'week_one' => $week_one->total,
                    'week_two' => $week_two->total,
                    'week_three' => $week_three->total,
                    'week_four' => $week_four->total,
                    'week_five' => $week_five->total,
                    'week_six' => $week_six->total,
                    'total' => $res->total
                ];
            }


            // dd($array);
            return view('erp.finance.expense_pivot')->with(['start_date' => $start_date, 'end_date' => $end_date, 'result' => $array]);
        }
    }

    public function get_total_for_a_category($category_id, $start, $end)
    {
        $query = "SELECT (SELECT (SELECT id FROM account_sub_headers WHERE id = 
                        (SELECT sub_category_id FROM pr_request WHERE sub_category_id = '$category_id'))) as sub_category_id, 
                              SUM(total) as total FROM request_items WHERE (created_at BETWEEN '$start' AND '$end') AND 
                                rfq_status = 2 AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri'";
        $result = DB::select($query);
        return $result[0];
    }
}
