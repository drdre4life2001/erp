<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\EmployeeEducation;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EmployeeEducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $validator = Validator::make($request->all(), [
                'id_hr_institutions' => 'required|max:255',
                'id_hr_institution_departments' => 'required|max:255',
                'employee_id' => 'required|max:255',
                'startDate' => 'required|max:255',
                'endDate' => 'required|max:255',
                'grade' => 'required|max:255',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $employee = Employee::find($request->get('employee_id'));
            if(empty($employee)){
                return "Employee Resource not found";
            }

            EmployeeEducation::create([
                'specialization'=> $request->get('specialization'),
                'notes'=> $request->get('notes'),
                'startDate'=> $request->get('startDate'),
                'endDate'=> $request->get('endDate'),
                'mode_of_education'=> $request->get('mode_of_education'),
                'grade'=> $request->get('grade'),
                'comments'=> $request->get('comments'),
                'id_hr_institutions'=> $request->get('id_hr_institutions'),
                'id_hr_institution_departments'=> $request->get('id_hr_institution_departments'),
                'id_hr_employee'=> $request->get('employee_id'),
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id
            ]);

            return back()->withErrors('Employee Education changed successfully');

        }catch (\Exception $ex){
            return back()->withErrors($ex->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
