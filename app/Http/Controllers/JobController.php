<?php

namespace App\Http\Controllers;

use App\Models\Job;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $jobs = Job::where('advance_company_id', auth()->user()->company->id)->orderBy('created_at', 'DESC')->get();
        return view('jobs.index')->with(['jobs'=>$jobs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('jobs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'code' => 'required|unique:hr_job_title',

            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            Job::create([
                'name' => $request->get('name'),
                'code'=>$request->get('code'),
                'description'=>$request->get('description'),
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id,
                'advance_company_id' => auth()->user()->company->id,
                'advance_company_uri' => auth()->user()->company->uri
            ]);

            return redirect('jobs');

        }catch (\Exception $ex){
            return back()->with('error', $ex->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function editJob(Request $request, $id)
    {
        $method = $request->isMethod('post');
        $job = Job::findOrFail($id);
        switch ($method){
            case true :
                $job->update([
                    'name' => $request->name,
                    'code' => $request->code,
                    'description' => $request->description,
                ]);

                return redirect()->route('jobs.index')->with('success', 'The information has been updated!');

            case false:
                return view('jobs.edit', compact('job'));
        }

        //
    }
}
