<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\UserPermission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class UserPermissionsController extends Controller
{
    //
	public function __construct(){
		$this->middleware('auth');
	}
    public function index(){
    	$permissions = DB::select('select * from user_permissions');
    	return view('permissions.showPermissions')->with('permissions', $permissions);
    }
    public function createPermissions(){
    	return view('permissions.createPermissions');
    }
    public function storePermissions(Request $request){
    	$validator = Validator::make($request->all(), [
    			'name' => 'required|max:255',
    			'description' => 'required|max:255'
    	]);
    	if($validator->fails()){
    		return back()->withErrors($validator)->withInput();
    	}
    	$permissions = new UserPermission;
    	$permissions->name = $request->name;
    	$permissions->description = $request->description;

    	if($permissions->save()){
    		return back()->with('status', 'Permission successfully added');
    	}
    }
}
