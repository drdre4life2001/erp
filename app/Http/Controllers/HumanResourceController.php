<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use App\Libraries\Utilities;
use App\Models\DraftPayroll;
use App\Models\Employee;
use App\Models\EmployeeAbsence;
use App\Models\EmployeeArrear;
use App\Models\EmployeeBonus;
use App\Models\EmployeeDeduction;
use App\Models\EmployeeLoan;
use App\Models\EmployeeLeave;
use App\Models\EmployeePayroll;
use App\Models\EmployeeRenumeration;
use App\Models\LeaveType;
use App\Models\PayrollSummary;
use App\Models\MonthDay;
use PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;
use Psy\Exception\ErrorException;

class HumanResourceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('hr.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function employee_bonuses($id = null)
    {
        //$bonuses = EmployeeBonus::where('active', 1)->with('employee');
        $bonuses = DB::select('SELECT (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name, (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, 
                                bonus_type, month, id, amount, year, created_at, description FROM hr_employee_bonuses WHERE active = 1');
        return view('employee.bonuses')->with(['bonuses' => $bonuses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function employee_arrears(Request $request, $id = null)
    {
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $bonuses = DB::select("SELECT (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name, (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, 
                                month, id, amount, year, created_at FROM hr_employee_arrears WHERE active = 1 AND advance_company_id = '$company_id' AND advance_company_uri = '$company_uri'");
        $method = $request->isMethod('post');
        if ($method) {
            $validator = Validator::make($request->all(), [
                'employee_id' => 'required|max:255',
                'amount' => 'required|max:255',
                'description' => 'required|max:255',
                'month' => 'required|max:255',
                'pay_month' => 'required|max:255',
                'year' => 'required|max:255'
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $bonus = new EmployeeArrear();
            $bonus->employee_id = $request->employee_id;
            $bonus->amount = $request->amount;
            $bonus->description = $request->description;
            $bonus->month = $request->month;
            $bonus->year = $request->year;
            $bonus->advance_company_id = auth()->user()->company->id;
            $bonus->advance_company_uri = auth()->user()->company->uri;
            $bonus->created_by = Auth::user()->id;
            if ($bonus->save()) {
                return back()->with('success', 'Employee Arrears added!');
            }
        } else {
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('erp.hr.employee_arrears')->with(['bonuses' => $bonuses, 'months' => $months, 'years' => $years]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function draft_payrolls()
    {
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $payrolls = DB::select("SELECT * FROM hr_draft_payroll WHERE `status` = '1' AND advance_company_id = '$company_id' AND advance_company_uri = '$company_uri'");
        return view('erp.hr.draft_payroll')->with(['payrolls' => $payrolls]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function employee_deductions($id = null)
    {
        $deductions = DB::select('SELECT (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name, (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, 
                                description, id, month, amount, year, created_at FROM hr_employee_deductions WHERE active = 1');
        return view('employee.deductions')->with(['deductions' => $deductions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function employee_absences(Request $request, $id = null)
    {
        $deductions = DB::select('SELECT (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name, (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, description, id, month, days_absent, year, created_at FROM hr_employee_absences WHERE active = 1');
        $method = $request->isMethod('post');
        switch ($method) {
            case true:

                break;
            case false:

                return view('erp.hr.employee_absence')->with(['deductions' => $deductions]);
            default:
                break;
        }


        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function employee_loans(Request $request)
    {
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $method = $request->isMethod('post');
        $loans = DB::select("SELECT (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name, (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, 
                                description, id, amount, tenure, interest, total_payable, created_at FROM hr_employee_loans WHERE active = 1 AND advance_company_id = '$company_id' AND advance_company_uri = '$company_uri'");
        if ($method) {
            $validator = Validator::make($request->all(), [
                'employee_id' => 'required|max:255',
                'amount' => 'required|max:30',
                'interest' => 'required|max:30',
                'tenure' => 'required|max:30',
                'description' => 'required'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $interest = ($request->interest / 100) * $request->amount;
            $total_payable = $interest + $request->amount;
            $bonus = new EmployeeLoan();
            $bonus->employee_id = $request->employee_id;
            $bonus->amount = $request->amount;
            $bonus->total_payable = $total_payable;
            $bonus->tenure = $request->tenure;
            $bonus->interest = $request->interest;
            $bonus->description = $request->description;
            $bonus->created_by = Auth::user()->id;
            $bonus->advance_company_id = Auth::user()->company->id;
            $bonus->advance_company_uri = Auth::user()->company->uri;
            if ($bonus->save()) {
                return back()->with('success', 'Employee loan recorded!');
            }
        } else {
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('erp.hr.employee_loans')->with(['months' => $months, 'years' => $years, 'loans' => $loans]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function employee_renumerations($id = null)
    {
        $renumerations = DB::select('SELECT (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name, (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, basic_pay, id, transport,leave_allowance, housing, others, utility, nhf, life_assurance, cra, fixed_cra, pension, dependable_relative, children, disability, meal, entertainment,created_at FROM hr_employee_renumerations WHERE active = 1;');
        return view('employee.renumerations')->with(['renumerations' => $renumerations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function employee_loan_settlements($id = null)
    {
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $settlements = DB::select("SELECT (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name,
                                     (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, amount, id,
                                      amount_to_date,total_payable, total_outstanding, created_at FROM hr_employee_loan_settlements WHERE advance_company_id = '$company_id' AND advance_company_uri = '$company_uri'");
        return view('erp.hr.employee_settlement')->with(['settlements' => $settlements]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function employee_leaves(Request $request)
    {
        $method = $request->isMethod('post');
        switch ($method) {
            case true:
                $this->validate($request, [
                    'start_date' => 'required|max:255',
                    'end_date' => 'required|max:255',
                    'employee_id' => 'required',
                    'leave_type_id' => 'required',
                    'number_of_days' => 'required'
                ]);
                $emp = new EmployeeLeave();
                $emp->start = $request->start_date;
                $emp->end = $request->end_date;
                $emp->days = $request->number_of_days;
                $emp->employee_id = $request->employee_id;
                $emp->type = $request->leave_type_id;
                $emp->description = $request->description;
                $emp->created_by = auth()->user()->id;
                $emp->company_id = $this->companyId();
                $emp->company_uri = $this->companyUri();
                if ($emp->save()) {
                    return back()->with('success', 'Leave Record Added!');
                }
                break;

            case false:
            
            default:
                break;
        }
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $leave_types = DB::select('SELECT * FROM hr_leave_types WHERE active = 1;');
        $leaves = DB::select("SELECT id, (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name,
                                            (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname,
                                              (SELECT name FROM hr_leave_types WHERE id = type) as type, start, `end`, days, description, status, created_at
                                                FROM hr_employee_leaves WHERE deleted_at IS NULL AND company_id = '$company_id' AND company_uri = '$company_uri'");
        return view('erp.hr.employee_leaves')->with(['leaves' => $leaves, 'leave_types' => $leave_types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function work_days(Request $request, $id = null)
    {
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $work_days = DB::select("SELECT * FROM hr_employee_month_days WHERE active = 1 AND advance_company_id = '$company_id' AND advance_company_uri = '$company_uri'");

        $method = $request->isMethod('post');
        if ($method) {
            $validator = Validator::make($request->all(), [
                'month' => 'required|max:255',
                'year' => 'required|max:255',
                'days' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $duplicate = MonthDay::where('month', $request->month)->where('year', $request->year)->where('active', 1)->where('advance_company_id', auth()->user()->company->id)->first();
            if ($duplicate) {
                return back()->with('error', 'Duplicate record found!')->withInput();
            }
            $month_day = new MonthDay();
            $month_day->month = $request->month;
            $month_day->year = $request->year;
            $month_day->days = $request->days;
            $month_day->advance_company_id = auth()->user()->company->id;
            $month_day->advance_company_uri = auth()->user()->company->uri;
            if ($month_day->save()) {
                return back()->with('success', 'Record added!');
            }
        } else {
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('erp.hr.work_days')->with(['months' => $months, 'years' => $years, 'workdays' => $work_days]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function leave_types()
    {
        $leave_types = DB::select('SELECT * FROM hr_leave_types WHERE active = 1');
        return view('erp.hr.leave_types')->with(['leave_types' => $leave_types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_employee_bonus(Request $request)
    {
        $method = $request->isMethod('post');
        $company = auth()->user()->company->id;
        $bonuses = DB::select("SELECT (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name, 
                                  (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, bonus_type,
                                      month, id, amount, year, created_at, description FROM hr_employee_bonuses WHERE active = 1 AND company_id = $company");
        if ($method) {
            $validator = Validator::make($request->all(), [
                'employee_id' => 'required|max:255',
                'amount' => 'required|max:255',
                'description' => 'required|max:255',
                'month' => 'required|max:255',
                'year' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
           
            $taxable = isset($request->taxable) ? 1 : 0;
            $bonus = new EmployeeBonus();
            $bonus->employee_id = $request->employee_id;
            $bonus->amount = $request->amount;
            $bonus->description = $request->description;
            $bonus->month = $request->month;
            $bonus->year = $request->year;
            $bonus->created_by = Auth::user()->id;
            $bonus->company_id = Auth::user()->company->id;
            $bonus->company_uri = Auth::user()->company->uri;
            $bonus->taxable = $taxable;
            if ($bonus->save()) {
                return back()->with('success', 'Employee bonus added!');
            }
        } else {
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('erp.hr.employee_bonus')->with(['months' => $months, 'years' => $years, 'bonuses' => $bonuses]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_employee_arrears(Request $request)
    {
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $method = $request->isMethod('post');
        if ($method) {
            $validator = Validator::make($request->all(), [
                'employee_id' => 'required|max:255',
                'amount' => 'required|max:255',
                'description' => 'required|max:255',
                'month' => 'required|max:255',
                'pay_month' => 'required|max:255',
                'year' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $bonus = new EmployeeArrear();
            $bonus->employee_id = $request->employee_id;
            $bonus->amount = $request->amount;
            $bonus->description = $request->description;
            $bonus->month = $request->month;
            $bonus->year = $request->year;
            $bonus->created_by = Auth::user()->id;
            $bonus->advance_company_id = Auth::user()->company->id;
            $bonus->advance_company_uri = Auth::user()->company->uri;
            if ($bonus->save()) {
                return redirect(route('employee_arrears'))->with('success', 'Employee Arrears added!');
            }
        } else {
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('employee.add_arrears')->with(['months' => $months, 'years' => $years]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_employee_renumeration(Request $request)
    {
        $method = $request->isMethod('post');
        $company = auth()->user()->company->id;
        $renumerations = DB::select("SELECT 
                                        (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name, 
                                          (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, basic_pay,
                                            id, transport,leave_allowance, housing, others, utility, nhf, life_assurance, cra, fixed_cra,
                                             pension, dependable_relative, children, disability, meal, entertainment,created_at
                                              FROM hr_employee_renumerations WHERE active = 1 AND company_id = $company;");
        if ($method) {
            $validator = Validator::make($request->all(), [
                'employee_id' => 'required|max:255',
                'basic_pay' => 'required|max:255',
                'transport' => 'required|max:255',
                'leave' => 'required|max:255',
                'housing' => 'required|max:255',
                'others' => 'required|max:255',
                'meal' => 'required|max:255',
                'entertainment' => 'required|max:255',
                'utility' => 'required|max:255',
                'dependable_relative' => 'required|max:255',
                'children' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $duplicate = EmployeeRenumeration::where('employee_id', $request->employee_id)->first();
            if ($duplicate) {
                return back()->with('success', 'Renumeration for this employee already exist!')->withInput();
            }
            $fixed_cra = $request->cra == 1 ? 1 : 0;
            $renumeration = new EmployeeRenumeration();
            $renumeration->employee_id = $request->employee_id;
            $renumeration->basic_pay = $request->basic_pay;
            $renumeration->transport = $request->transport;
            $renumeration->leave_allowance = $request->leave;
            $renumeration->housing = $request->housing;
            $renumeration->others = $request->others;
            $renumeration->meal = $request->meal;
            $renumeration->entertainment = $request->entertainment;
            $renumeration->utility = $request->utility;
            $renumeration->nhf = $request->nhf;
            $renumeration->life_assurance = $request->life_assurance;
            $renumeration->cra = $request->cra;
            $renumeration->fixed_cra = $fixed_cra;
            $renumeration->pension = $request->pension;
            $renumeration->dependable_relative = $request->dependable_relative;
            $renumeration->children = $request->children;
            $renumeration->disability = $request->disability;
            $renumeration->created_by = Auth::user()->id;
            $renumeration->company_id = auth()->user()->company->id;
            $renumeration->company_uri = auth()->user()->company->uri;
            if ($renumeration->save()) {
                return back()->with('success', 'Employee Renumeration added!');
            }
        } else {
            $employees = Employee::all();
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('erp.hr.remuneration')->with(['employees' => $employees, 'renumerations' => $renumerations ,'months' => $months, 'years' => $years]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update_employee_renumeration(Request $request, $id = null, $company_uri = null)
    {
        $method = $request->isMethod('post');
        $e_renumeration = EmployeeRenumeration::find($id);
        if ($method) {
            $validator = Validator::make($request->all(), [
                'employee_id' => 'required|max:255',
                'basic_pay' => 'required|max:255',
                'transport' => 'required|max:255',
                'leave' => 'required|max:255',
                'housing' => 'required|max:255',
                'others' => 'required|max:255',
                'meal' => 'required|max:255',
                'entertainment' => 'required|max:255',
                'utility' => 'required|max:255',
                'dependable_relative' => 'required|max:255',
                'children' => 'required|max:255'
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $renumeration_data = EmployeeRenumeration::where('employee_id', $request->employee_id)->first();
            $renumeration = EmployeeRenumeration::find($renumeration_data->id);
            $disability = isset($request->disability) ? $request->disability : 0;
            $pension = isset($request->pension) ? $request->pension : 0;
            $nhf = isset($request->nhf) ? $request->nhf : 0;
            $life_assurance = isset($request->life_assurance) ? $request->life_assurance : 0;
            $cra = isset($request->cra) ? $request->cra : 0;
            $fixed_cra = isset($request->cra) ? 1 : 0;
            $renumeration->employee_id = $request->employee_id;
            $renumeration->basic_pay = $request->basic_pay;
            $renumeration->transport = $request->transport;
            $renumeration->leave_allowance = $request->leave;
            $renumeration->housing = $request->housing;
            $renumeration->others = $request->others;
            $renumeration->meal = $request->meal;
            $renumeration->entertainment = $request->entertainment;
            $renumeration->utility = $request->utility;
            $renumeration->nhf = $nhf;
            $renumeration->life_assurance = $life_assurance;
            $renumeration->cra = $cra;
            $renumeration->fixed_cra = $fixed_cra;
            $renumeration->pension = $pension;
            $renumeration->dependable_relative = $request->dependable_relative;
            $renumeration->children = $request->children;
            $renumeration->disability = $disability;
            $renumeration->created_by = Auth::user()->id;
            $renumeration->company_id = Auth::user()->company->id;
            $renumeration->company_uri = Auth::user()->company->uri;

            if ($renumeration->save()) {
                return redirect(route('add_employee_renumeration'))->with('success', 'Employee Renumeration added!');
            }
        } else {
            $employee = Employee::find($e_renumeration->employee_id);
            $employees = Employee::all();
            $years = Utilities::getYears();
            return view('erp.hr.update_employee_remuneration')->with(['employee' => $employee, 'employees' => $employees, 'renumeration' => $e_renumeration, 'years' => $years]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_employee_absence(Request $request)
    {
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $absences = DB::select("SELECT (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name, (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, 
                                description, id, month, days_absent, year, created_at FROM hr_employee_absences WHERE active = 1 AND advance_company_id = '$company_id' AND advance_company_uri = '$company_uri'");
        $method = $request->isMethod('post');
        if ($method) {
            $validator = Validator::make($request->all(), [
                'employee_id' => 'required|max:255',
                'days_absent' => 'required|max:30',
                'description' => 'required|max:255',
                'month' => 'required|max:255',
                'year' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $abs = new EmployeeAbsence();
            $abs->employee_id = $request->employee_id;
            $abs->days_absent = $request->days_absent;
            $abs->description = $request->description;
            $abs->month = $request->month;
            $abs->year = $request->year;
            $abs->created_by = Auth::user()->id;
            $abs->advance_company_id = $company_id;
            $abs->advance_company_uri = $company_uri;
            if ($abs->save()) {
                return back()->with('success', 'Employee absence recorded!');
            }
        } else {
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('erp.hr.employee_absence')->with(['months' => $months, 'years' => $years, 'deductions' => $absences]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_employee_loan(Request $request)
    {
        $method = $request->isMethod('post');
        if ($method) {
            $validator = Validator::make($request->all(), [
                'employee_id' => 'required|max:255',
                'amount' => 'required|max:30',
                'interest' => 'required|max:30',
                'tenure' => 'required|max:30',
                'description' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $interest = ($request->interest / 100) * $request->amount;
            $total_payable = $interest + $request->amount;
            $bonus = new EmployeeLoan();
            $bonus->employee_id = $request->employee_id;
            $bonus->amount = $request->amount;
            $bonus->total_payable = $total_payable;
            $bonus->tenure = $request->tenure;
            $bonus->interest = $request->interest;
            $bonus->description = $request->description;
            $bonus->created_by = Auth::user()->id;
            if ($bonus->save()) {
                return redirect(route('employee_loans'))->with('success', 'Employee loan recorded!');
            }
        } else {
            $employees = Employee::all();
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('employee.add_loan')->with(['employees' => $employees, 'months' => $months, 'years' => $years]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_work_days(Request $request)
    {
        $method = $request->isMethod('post');
        if ($method) {
            $validator = Validator::make($request->all(), [
                'month' => 'required|max:255',
                'year' => 'required|max:255',
                'days' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $duplicate = MonthDay::where('month', $request->month)->where('year', $request->year)->where('advance_company_id', auth()->user()->company->id)->where('active', 1)->first();
            if ($duplicate) {
                return back()->withErrors('Duplicate record found!')->withInput();
            }
            $month_day = new MonthDay();
            $month_day->month = $request->month;
            $month_day->year = $request->year;
            $month_day->days = $request->days;
            $month_day->advance_company_id = auth()->user()->company->id;
            $month_day->advance_company_uri = auth()->user()->company->uri;
            if ($month_day->save()) {
                return redirect(route('work_days'))->with('success', 'Record added!');
            }
        } else {
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('employee.add_working_days')->with(['months' => $months, 'years' => $years]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_leave_type(Request $request)
    {
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;
        $leave_types = DB::select("SELECT * FROM hr_leave_types WHERE active = 1 AND advance_company_id = '$company_id' AND advance_company_uri = '$company_uri'");

        $method = $request->isMethod('post');
        if ($method) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'days' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $duplicate = LeaveType::where('name', $request->name)->where(['days' => $request->days, 'advance_company_id' => $company_id, 'advance_company_uri' => $company_uri])->where('active', 1)->first();
            if ($duplicate) {
                return back()->withErrors('Duplicate record found!')->withInput();
            }
            $month_day = new LeaveType();
            $month_day->days = $request->days;
            $month_day->name = $request->name;
            $month_day->advance_company_id = $company_id;
            $month_day->advance_company_uri = $company_uri;
            
            if ($month_day->save()) {
                return back()->with('success', 'Record added!');
            }

        } else {
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('erp.hr.leave_types', compact('leave_types'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function send_payslip(Request $request)
    {
        $advance_company_id = auth()->user()->company->id;
        $advance_company_uri = auth()->user()->company->uri;

        $method = $request->isMethod('post');
        if ($method) {
            try {
                $validator = Validator::make($request->all(), [
                    'month' => 'required|max:255',
                    'year' => 'required|max:255'
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                }

                $payroll = Utilities::getEmployeePayroll($request->month, $request->year, 'advance_company_id', 'advance_company_uri');
                if (count($payroll) <= 0) {
                    return back()->with('error', 'Payroll record not found for the specified month and year!')->withInput();
                } else {
                    $i = 0;
                    //employee payslip is the sum of all the employee parameters like 
                    //(deductions, loans, basic pay, leave allowance) in the specified year
                    foreach ($payroll as $payslip) {
                        $employee_id = $payslip->employee_id;
                        $year = $request->year;
                        $month = $request->month;
                        $email = $payslip->employee->email;
                        $la_user = $payslip->employee->first_name . ', ' . $payslip->employee->lastname;
                        $basic_to_date = EmployeePayroll::basic_to_date($employee_id, $year);
                        $transport_to_date = EmployeePayroll::transport_to_date($employee_id, $year);
                        $leave_to_date = EmployeePayroll::leave_to_date($employee_id, $year);
                        $housing_to_date = EmployeePayroll::housing_allowance_to_date($employee_id, $year);
                        $netpay_to_date = EmployeePayroll::net_pay_to_date($employee_id, $year);
                        $meal_to_date = EmployeePayroll::meal_allowance_to_date($employee_id, $year);
                        $grosspay_to_date = EmployeePayroll::gross_payable_to_date($employee_id, $year);
                        $tax_reliefs_to_date = EmployeePayroll::tax_relief_to_date($employee_id, $year);
                        $taxable_pay_to_date = EmployeePayroll::taxable_pay_to_date($employee_id, $year);
                        $tax_to_date = EmployeePayroll::tax_to_date($employee_id, $year);
                        $entertainment_to_date = EmployeePayroll::entertainment_allowance_to_date($employee_id, $year);
                        $pension_to_date = EmployeePayroll::pension_to_date($employee_id, $year);
                        $nhf_to_date = EmployeePayroll::nhf_to_date($employee_id, $year);
                        $salary_advance_to_date = EmployeePayroll::salary_advance_to_date($employee_id, $year);
                        
                        $pdf = PDF::loadView('employee.payslip_pdf', compact('payslip', 'basic_to_date', 'transport_to_date',
                            'leave_to_date', 'housing_to_date', 'netpay_to_date', 'meal_to_date', 'grosspay_to_date',
                            'tax_reliefs_to_date', 'taxable_pay_to_date', 'tax_to_date', 'entertainment_to_date', 'pension_to_date', 'salary_advance_to_date', 'nhf_to_date'));
                        $pdf = $pdf->output();
                            $send_email = Mail::send('employee.payslip_email', 
                            compact('payslip', 'basic_to_date', 'transport_to_date',
                                'leave_to_date', 'housing_to_date', 'netpay_to_date', 'meal_to_date', 'grosspay_to_date',
                                'tax_reliefs_to_date', 'taxable_pay_to_date', 'tax_to_date', 'entertainment_to_date', 'pension_to_date', 'salary_advance_to_date', 'nhf_to_date'),
                             function ($message) use ($email, $la_user, $year, $month, $pdf) {
                            $message->to($email, $la_user)
                            ->from('admin@taerp.com', 'TA Human Resources')
                            ->subject($la_user . ' ' . $month . ',' . $year . ' Payslip')
                            ->attachData($pdf, 'pay_slip_' . $month . '_' . $year . '.pdf', []);
                        });
                        if ($send_email) {
                            $i++;
                        }
                    }
                }
                return back()->with('success', 'Payslips sent to ' . $i . ' Employees');
            } catch (ErrorException $exception) {
                return back()->with('error', $exception->getMessage());
            }
        } else {
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('erp.hr.send_pay_slip')->with(['months' => $months, 'years' => $years]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generate_payroll(Request $request)
    {
        $adv_company_id = auth()->user()->company->id;
        $adv_company_uri = auth()->user()->company->uri;
        $method = $request->isMethod('post');
        if ($method) {
            $validator = Validator::make($request->all(), [
                'month' => 'required|max:255',
                'year' => 'required|max:255',
                'id_hr_company' => 'required'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            //Check Working Days for the month selected
            $month_work_day = MonthDay::where('month', $request->month)->where('year', $request->year)->where('active', 1)->where('advance_company_id', auth()->user()->company->id)->where('advance_company_uri', auth()->user()->company->uri)->first();
            if (!$month_work_day) {
                return back()->with('error', 'No work days recorded for ' . $request->month . ', ' . $request->year)->withInput();
            }
            $payrolls = Utilities::generate_payroll($request->month, $request->year, $request->id_hr_company);
            if ($payrolls === false) {
                return back()->with('error', 'The Payroll for ' . $request->month . ', ' . $request->year . ' already exist and cannot be regenerated!')->withInput();
            }elseif(empty($payrolls) || is_null($payrolls)){
                return back()->with('error', 'Employee Remuneration not found! Please create remuneration for employees.');
            }
            $payrolls = json_decode($payrolls);
            
            $data = [];

            foreach ($payrolls as $payroll) {
                $employee_id = $payroll->employee_id;
                //company_id and company uri represents the advance authenticated company  
                $employee = DB::select("SELECT first_name, lastname FROM hr_employee WHERE id = '$employee_id' AND company_id = '$payroll->advance_company_id' AND company_uri = '$payroll->advance_company_uri'");
                $payroll->employee = $employee[0]->first_name . ', ' . $employee[0]->lastname;
                $data[] = $payroll;
            }

            if(is_null($data) || empty($data)){
                return back()->with('error', 'Employee remuneration not found for the subsidiary employees!');
            }

            DB::select("DELETE FROM hr_draft_payroll WHERE month = '$request->month' AND year = '$request->year' AND subsidiary_id = '$request->id_hr_company' AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_id'");
            $draft_payroll = new DraftPayroll();
            $draft_payroll->month = $request->month;
            $draft_payroll->year = $request->year;
            $draft_payroll->subsidiary_id = $request->id_hr_company;
            $draft_payroll->status = 1;
            $draft_payroll->created_by = Auth::user()->id;
            $draft_payroll->payroll_data = json_encode($data);
            $draft_payroll->advance_company_id = auth()->user()->company->id;
            $draft_payroll->advance_company_uri = auth()->user()->company->uri;
            $draft_payroll->save();
            //return auth user/company to the single employee generated payroll 
            return view('erp.hr.single_payroll')->with(['payrolls' => $data, 'month' => $request->month, 'year' => $request->year, 'company' => $request->id_hr_company]);
        } else {
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('erp.hr.generate_draft_payroll')->with(['months' => $months, 'years' => $years]);
        }
    }

    public function confirm_payroll($month, $year, $company_id, $advance_company_uri)
    {
        $adv_company_id = auth()->user()->company->id;
        $adv_company_uri = auth()->user()->company->uri;
        $checkduplicate = DB::table('payroll_summaries')
            ->where(['year' => $year, 'company_id' => $company_id, 'advance_company_id' => $adv_company_id, 'advance_company_uri' => $adv_company_uri, 'month' => $month])
            ->first();
        if (!empty($checkduplicate)) {
            return back()->with('error', 'Duplicate record found or Payroll for '.$month.' and '.$year.' has already been confirmed!');
        }

        if(is_null($company_id) || empty($company_id) || empty($advance_company_uri))
            return back()->with('error', 'Subsidiary not found!');
        $draft_payroll = DraftPayroll::where('month', $month)
                        ->where(['year' => $year,
                        'subsidiary_id' => $company_id,
                         'advance_company_id' => auth()->user()->company->id,
                         'advance_company_uri' => auth()->user()->company->uri
                         ])
        ->first();
        $payroll_data = json_decode($draft_payroll->payroll_data);
        if(is_null($payroll_data) || empty($payroll_data))
            return back()->with('info', 'Generated Payroll data not found!');
        $success = 0;
        $errors = 0;
        $total = 0;
        $payroll_reference = null;
        $payroll_tax = null;
        DB::select("DELETE FROM hr_payrolls WHERE month = '$month' AND year = '$year' AND company_id = '$company_id' AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_id'");
        foreach ($payroll_data as $payroll) {
            $pay = new EmployeePayroll();
            $pay->net_pay = $payroll->net_pay;
            $pay->payroll_reference = $payroll->payroll_reference;
            $pay->employee_id = $payroll->employee_id;
            $pay->salary = $payroll->salary;
            $pay->transport = $payroll->transport;
            $pay->leave = $payroll->leave;
            $pay->housing = $payroll->housing;
            $pay->month = $payroll->month;
            $pay->life_assurance = $payroll->life_assurance;
            $pay->cra = $payroll->cra;
            $pay->pension = $payroll->pension;
            $pay->nhf = $payroll->nhf;
            $pay->company_id = $payroll->company_id;
            $pay->year = $year;
            $pay->dependable_relative = $payroll->dependable_relative;
            $pay->children = $payroll->children;
            $pay->disability = $payroll->disability;
            $pay->fixed_cra = $payroll->fixed_cra;
            $pay->total_tax_reliefs = $payroll->total_tax_reliefs;
            $pay->taxable_pay = $payroll->taxable_pay;
            $pay->total_gross_payable = $payroll->total_gross_payable;
            $pay->meal = $payroll->meal;
            $pay->utility = $payroll->utility;
            $pay->entertainment = $payroll->entertainment;
            $pay->tax = $payroll->tax;
            $pay->department_id = $payroll->department_id;
            $pay->arrears = $payroll->arrears;
            $pay->salary_advance = $payroll->salary_advance;
            $pay->absence = $payroll->absence;
            $pay->other_deductions = $payroll->other_deductions;
            $pay->other_taxable_benefits = $payroll->other_taxable_benefits;
            $pay->other_non_taxable_benefits = $payroll->other_non_taxable_benefits;
            $pay->advance_company_id = $payroll->advance_company_id;
            $pay->advance_company_uri = $payroll->advance_company_uri;
            if ($pay->save()) {
                $total = $total + $payroll->net_pay;
                $payroll_reference = $payroll->payroll_reference;
                $payroll_tax = $payroll->tax;
                $success++;
            } else {
                $errors++;
            }
        }
        $payrol_summary = new PayrollSummary();
        $payrol_summary->payroll_reference = $payroll_reference;
        $payrol_summary->amount = $total;
        $payrol_summary->tax = $payroll_tax;
        $payrol_summary->month = $month;
        $payrol_summary->company_id = $company_id;
        $payrol_summary->status = "pending";
        $payrol_summary->year = $year;
        $payrol_summary->advance_company_id = auth()->user()->company->id;
        $payrol_summary->advance_company_uri = auth()->user()->company->uri;
        if($payrol_summary->save())
            session()->flash('success', 'Payroll Summary saved successfully');
        //update drafted payroll status for the authenticated advance user login
        DB::select("UPDATE hr_draft_payroll SET `status` = 0 WHERE id = '$draft_payroll->id' AND subsidiary_id = '$company_id' AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri'");
        $msg = $success . ' Records saved while ' . $errors . ' could not be saved!';
        return redirect(route('payrolls'))->with('success', $msg);
    }

    public function viewPayroll($id, $month, $year){
        $payrolls = EmployeePayroll::where('month', $month)->where('year', $year)->with(['employee'])->first();
        return view('erp.hr.view_employee_payroll', $payrolls);
    }

    public function payrolls(Request $request)
    {
        // $payrolls = EmployeePayroll::with('employee')->get();
        $method = $request->isMethod('post');
        if ($method) {
            $validator = Validator::make($request->all(), [
                'month' => 'required|max:255',
                'year' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $payrolls = EmployeePayroll::where('month', $request->month)->where(['year' => $request->year, 'advance_company_id' => auth()->user()->company->id])->with(['employee'])->get();
            $payroll_count = !empty($payrolls) ? count($payrolls) : null;
            if ($payroll_count <= 0) {
                return back()->with('info', 'Employee Payroll not available');
            }else{
                return view('erp.hr.view_payroll')->with(['payrolls' => $payrolls, 'month' => $request->month, 'year' => $request->year]);
            }

        } else {
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('erp.hr.payroll_histories')->with(['months' => $months, 'years' => $years]);
        }
    }

    public function payslip($employee_id, $month, $year, $advance_company_uri)
    {

        $payslip = EmployeePayroll::where('employee_id', $employee_id)->where('month', $month)->where('year', $year)->with(['employee'])->first();
        $basic_to_date = EmployeePayroll::basic_to_date($employee_id, $year);
        $transport_to_date = EmployeePayroll::transport_to_date($employee_id, $year);
        $leave_to_date = EmployeePayroll::leave_to_date($employee_id, $year);
        $housing_to_date = EmployeePayroll::housing_allowance_to_date($employee_id, $year);
        $netpay_to_date = EmployeePayroll::net_pay_to_date($employee_id, $year);
        $meal_to_date = EmployeePayroll::meal_allowance_to_date($employee_id, $year);
        $grosspay_to_date = EmployeePayroll::gross_payable_to_date($employee_id, $year);
        $tax_reliefs_to_date = EmployeePayroll::tax_relief_to_date($employee_id, $year);
        $taxable_pay_to_date = EmployeePayroll::taxable_pay_to_date($employee_id, $year);
        $tax_to_date = EmployeePayroll::tax_to_date($employee_id, $year);
        $entertainment_to_date = EmployeePayroll::entertainment_allowance_to_date($employee_id, $year);
        $pension_to_date = EmployeePayroll::pension_to_date($employee_id, $year);
        $nhf_to_date = EmployeePayroll::nhf_to_date($employee_id, $year);
        $salary_advance_to_date = EmployeePayroll::salary_advance_to_date($employee_id, $year);
        return view('employee.payslip', compact('payslip', 'basic_to_date', 'transport_to_date',
            'leave_to_date', 'housing_to_date', 'netpay_to_date', 'meal_to_date', 'grosspay_to_date',
            'tax_reliefs_to_date', 'taxable_pay_to_date', 'tax_to_date', 'entertainment_to_date', 'pension_to_date', 'salary_advance_to_date', 'nhf_to_date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function export_payroll($month, $year, $type, $subsidiary_id, $advance_company_uri)
    {
        $advance_company_uri = null;
        if ($type == 'draft') {
            $draft = DraftPayroll::where('month', $month)
                    ->where(['year' => $year,
                         'advance_company_id' => auth()->user()->company->id,
                         'subsidiary_id' => $subsidiary_id,
                         'advance_company_uri' => auth()->user()->company->uri
                         ])->first();
            $array = json_decode($draft->payroll_data);
            Excel::create('draft_payroll_' . $month . '_' . $year, function ($excel) use ($array, $month, $year) {
                $excel->sheet('Sheetname', function ($sheet) use ($array, $month, $year) {
                    $sheet->loadView('hr.elements.payroll_data')
                        ->with('payrolls', $array);
                });

            })->export('xls');
        } else {
            $draft = EmployeePayroll::where('month', $month)
                ->where(['year' => $year,
                'advance_company_id' => auth()->user()->company->id,
                'company_id' => $subsidiary_id,
                'advance_company_uri' => auth()->user()->company->uri
                ])->get();
            $array = $draft;
            Excel::create('confirmed_payroll_' . $month . '_' . $year, function ($excel) use ($array, $month, $year) {
                $excel->sheet('Sheetname', function ($sheet) use ($array, $month, $year) {
                    $sheet->loadView('hr.elements.payrolls_data')
                        ->with('payrolls', $array)
                        ->with('month', $month)
                        ->with('year', $year);
                });

            })->export('xls');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function download_payslip($employee_id, $month, $year)
    {
        $payslip = EmployeePayroll::where('employee_id', $employee_id)->where('month', $month)->where('year', $year)->with(['employee'])->first();
        $basic_to_date = EmployeePayroll::basic_to_date($employee_id, $year);
        $transport_to_date = EmployeePayroll::transport_to_date($employee_id, $year);
        $leave_to_date = EmployeePayroll::leave_to_date($employee_id, $year);
        $housing_to_date = EmployeePayroll::housing_allowance_to_date($employee_id, $year);
        $netpay_to_date = EmployeePayroll::net_pay_to_date($employee_id, $year);
        $meal_to_date = EmployeePayroll::meal_allowance_to_date($employee_id, $year);
        $grosspay_to_date = EmployeePayroll::gross_payable_to_date($employee_id, $year);
        $tax_reliefs_to_date = EmployeePayroll::tax_relief_to_date($employee_id, $year);
        $taxable_pay_to_date = EmployeePayroll::taxable_pay_to_date($employee_id, $year);
        $tax_to_date = EmployeePayroll::tax_to_date($employee_id, $year);
        $entertainment_to_date = EmployeePayroll::entertainment_allowance_to_date($employee_id, $year);
        $pension_to_date = EmployeePayroll::pension_to_date($employee_id, $year);
        $nhf_to_date = EmployeePayroll::nhf_to_date($employee_id, $year);
        $salary_advance_to_date = EmployeePayroll::salary_advance_to_date($employee_id, $year);
        $pdf = PDF::loadView('employee.payslip_pdf', compact('month', 'year', 'basic_to_date', 'transport_to_date',
            'leave_to_date', 'housing_to_date', 'netpay_to_date', 'meal_to_date', 'grosspay_to_date', 'tax_reliefs_to_date',
            'taxable_pay_to_date', 'tax_to_date', 'entertainment_to_date', 'pension_to_date', 'nhf_to_date', 'salary_advance_to_date', 'payslip'));
        return $pdf->download($payslip->employee->first_name.'_'.$payslip->employee->lastname.'_pay_slip_' . $month . '_' . $year . '.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_employee_deduction(Request $request)
    {
        $method = $request->isMethod('post');
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;
        $deductions = DB::select("SELECT 
                                    (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name,
                                     (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, 
                                        description, id, month, amount, year, created_at FROM hr_employee_deductions
                                         WHERE active = 1 AND advance_company_id = '$company_id' AND advance_company_uri = '$company_uri'");
        if ($method) {
            $validator = Validator::make($request->all(), [
                'employee_id' => 'required|max:255',
                'amount' => 'required|max:255',
                'description' => 'required|max:255',
                'month' => 'required|max:255',
                'year' => 'required|max:255'
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $bonus = new EmployeeDeduction();
            $bonus->employee_id = $request->employee_id;
            $bonus->amount = $request->amount;
            $bonus->description = $request->description;
            $bonus->month = $request->month;
            $bonus->year = $request->year;
            $bonus->created_by = Auth::user()->id;
            $bonus->advance_company_id = auth()->user()->company->id;
            $bonus->advance_company_uri = auth()->user()->company->uri;
            if ($bonus->save()) {
                return back()->with('success', 'Employee deduction added!');
            }
        } else {
            $months = Utilities::getMonths();
            $years = Utilities::getYears();
            return view('erp.hr.employee_deductions')->with(['months' => $months, 'years' => $years, 'deductions' => $deductions]);
        }
    }

    public function delete_record($id = null, $type = null)
    {
        if (!is_null($id)) {
            switch ($type) {
                case 'deduction';
                    $deleted = Utilities::soft_delete($id, 'hr_employee_deductions');
                    if ($deleted) {
                        return back()->with('success', 'Employee deduction deleted');
                    } else {
                        return back()->withErrors('Something went wrong! Please try again!');
                    }
                    break;
                case 'bonus';
                    $deleted = Utilities::soft_delete($id, 'hr_employee_bonuses');
                    if ($deleted) {
                        return back()->with('success', 'Employee bonus deleted');
                    } else {
                        return back()->withErrors('Something went wrong! Please try again!');
                    }
                    break;
                case 'absence';
                    $deleted = Utilities::soft_delete($id, 'hr_employee_absences');
                    if ($deleted) {
                        return back()->with('success', 'Employee absence deleted');
                    } else {
                        return back()->withErrors('Something went wrong! Please try again!');
                    }
                    break;
                case 'loan';
                    $deleted = Utilities::soft_delete($id, 'hr_employee_loans');
                    if ($deleted) {
                        return back()->with('success', 'Employee loan deleted');
                    } else {
                        return back()->withErrors('Something went wrong! Please try again!');
                    }
                    break;
                case 'arrears';
                    $deleted = Utilities::soft_delete($id, 'hr_employee_arrears');
                    if ($deleted) {
                        return back()->with('success', 'Employee arrear deleted');
                    } else {
                        return back()->withErrors('Something went wrong! Please try again!');
                    }
                    break;
                case 'days';
                    $deleted = Utilities::soft_delete($id, 'hr_employee_month_days');
                    if ($deleted) {
                        return back()->with('success', 'Recod deleted');
                    } else {
                        return back()->withErrors('Something went wrong! Please try again!');
                    }
                    break;
                case 'leave';
                    $deleted = Utilities::soft_delete($id, 'hr_leave_types');
                    if ($deleted) {
                        return back()->with('success', 'Record deleted');
                    } else {
                        return back()->withErrors('Something went wrong! Please try again!');
                    }
                    break;
                case 'employee-leave';
                    $deleted = Utilities::soft_delete($id, 'hr_employee_leaves');
                    if ($deleted) {
                        return back()->with('success', 'Record deleted');
                    } else {
                        return back()->withErrors('Something went wrong! Please try again!');
                    }
                    break;
                case 'client';
                    $deleted = Utilities::soft_delete($id, 'sys_clients');
                    if ($deleted) {
                        return back()->with('success', 'Record deleted');
                    } else {
                        return back()->withErrors('Something went wrong! Please try again!');
                    }
                    break;
                case 'vendor';
                    $deleted = Utilities::soft_delete($id, 'sys_vendors');
                    if ($deleted) {
                        return back()->with('success', 'Record deleted');
                    } else {
                        return back()->withErrors('Something went wrong! Please try again!');
                    }
                    break;
                default;
                    return back()->withErrors('Something went wrong! Please try again!');
                    break;
            }

        } else {
            return back()->withErrors('Something went wrong! Please try again!');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
