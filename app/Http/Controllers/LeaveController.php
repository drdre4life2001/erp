<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class LeaveController extends Controller
{
    public function create(Request $request){
        $method = $request->isMethod('post');

        switch($method){
            case true:
                try{
                    $validator = Validator::make($request->all(), [
                        'title' => 'required|max:255',
                        'description' => 'required',
                        'account_header_id' => 'required',
                        'amount' => 'required|numeric'
                    ]);

                    if ($validator->fails()) {
                        return back()->withErrors($validator)->withInput();
                    }
                    $a = $request->input('amount');
                    $b = str_replace( ',', '', $a );
                    $cleaned_amount = str_replace('₦', '', $b);

                    $expense = new OtherExpense();
                    $expense->title = $request->input('title');
                    $expense->description = $request->input('description');
                    $expense->account_header_id = $request->input('account_header_id');
                    $expense->created_by = auth()->user()->id;
                    $expense->requested_by = auth()->user()->id;
                    $expense->amount = $cleaned_amount;


                    $expense->save();
                    return back()->with('success', 'Leave request forward successfully');
                }catch (\Exception $ex){
                    return back()->withErrors($ex->getMessage())->withInput();
                }
            case false:
//                dd()
                return view('leave.create', compact('leave'));
        }
    }
}
