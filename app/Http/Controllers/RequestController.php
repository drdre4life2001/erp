<?php

namespace App\Http\Controllers;

use App\Libraries\Utilities;
use App\Models\Client;
use App\Models\RequestCategory;
use App\Models\RequestGroup;
use App\Models\RequestItem;
use App\Models\Salvage;
use App\Models\Vendor;
use App\Models\PrRequest;
use App\Models\RequestItems;
use App\Models\RequestItemsEdit;
use App\Models\Sysrfq;
use Illuminate\Http\Request;
use PDF;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Models\Organization;

class RequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $requests = \App\Models\Requests::with('item', 'requester')->get();
        dd($requests);
        return view('requests.index')->with(['requests' => $requests]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = RequestCategory::with('items')->get();
        $organizations = Organization::all();
        session(['categories' => $categories]);
        return view('requests.create', compact('organizations'));
    }
    //Insert new procurement request
    public function store(Request $request){
        
        $method = $request->isMethod('post');
        if($method){
            $validator = Validator::make($request->all(), [
                    'title' => 'required|max:256',
                    'category' => 'required|max:256',
                    'company_id' => 'required',
                    'subsidiary' => 'required'
            ]);
            if($validator->fails()){
                return back()->withErrors($validator)->withInput();
            }
            $user_id = Auth::user()->id_hr_employee;
            $line_manager = DB::select("select parent_employee_id from hr_line_manager where child_employee_id = $user_id");
            foreach ($line_manager as $line_manager) {
                $parent_employee_id = $line_manager->parent_employee_id;
            }
            $prRequest = new PrRequest; //An instance of the Procurement Request Model
            $prRequest->title = $request->title;
            $prRequest->category = $request->category;
            $prRequest->company_id = $request->company_id;
            $prRequest->sub_category_id = $request->subsidiary;
            $prRequest->user_id = $user_id;
            $prRequest->line_manager = $parent_employee_id;
            $prRequest->line_manager_remark = 1; //Pending
            $prRequest->c_level_remark = 1; //Pending
            $prRequest->state = 'active';
            if($prRequest->save()){
                $id = $prRequest->id;
                $route = route('add_requests',['id' => $id]);
                return redirect()->route('add_requests',['id' => $id]);
                /**return redirect()->action(
                    'RequestController@new_requests', ['id' => $id]
                );
                **/
            }

        }
    }
    public function new_requests($id = null){
        // return back()->with('success', 'Request Item Trashed!');
        $id = $id['id'];
        $detail = DB::select("SELECT * FROM pr_request WHERE id = '$id' and state = 'active'");
        $items = DB::select("SELECT * FROM request_items WHERE request_id = '$id' and state = 'active'");
        //Fetch salvages
        $salvages = Salvage::where('advance_company_id', auth()->user()->id)->get();
        return view('requests.add_requests')->with(['detail'=>$detail, 'item'=>$items, 'salvages' => $salvages]);
    }
    public function select_subsidiary(Request $request){
            $category = $request->category;
            $subsidiary = DB::select("SELECT id, name from (
            (SELECT * FROM capital_expenses)
            UNION
            (SELECT * FROM operating_expenses)
            ) as t where category_id = '$category'");
            return response()->json($subsidiary);
    }
    public function save_requests(Request $request){
        // $id = $id['id'];
        $detail = DB::select("SELECT * FROM pr_request WHERE state = 'active'");
        $items = DB::select("SELECT * FROM request_items WHERE state = 'active'");
        //Fetch salvages
        $salvages = Salvage::where('advance_company_id', auth()->user()->company->id)->get();

        $method = $request->isMethod('post');
        if($method){
            
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'description' => 'required|max:255',
                'price' => 'required|numeric',
                'quantity' => 'required|integer',
                'total' => 'required|numeric',
                'salvage' => 'required'
             ]);
            if($validator->fails()){
               return back()->withErrors($validator)->withInput();
            }
            //Get salvage total
            $salvage_id = $request->salvage;
            $quantity = $request->quantity;
            $salvages = DB::select("select * from salvages where id = '$salvage_id'");
            foreach ($salvages as $salvages){
                $salvage_value = $salvages->value;
            }
            $value = $salvage_value * $quantity;
            $requestItems = new RequestItems;
            $requestItems->request_id = $request->request_id;
            $requestItems->title = $request->title;
            $requestItems->description = $request->description;
            $requestItems->price = $request->price;
            $requestItems->quantity = $request->quantity;
            $requestItems->salvage_id = $request->salvage;
            $requestItems->salvage_value = $value;
            $requestItems->total = $request->total;
            $requestItems->line_manager_remark= 1;
            $requestItems->c_level_remark = 1;
            $requestItems->state = 'active';
            $requestItems->rfq_status = 0; //Not sent
            
            if($requestItems->save()){
                return back()->with('status', 'Item successfully added');
            } 
        }  
            else {
                return view('erp.procurement.requests')->with(['detail'=>$detail, 'item'=>$items, 'salvages' => $salvages]);
        }
    }
    public function showRequests(){ //Show all requests by current user
        $user_id = Auth::user()->id_hr_employee;
        $details = DB::select("SELECT a.created_at as createdAt, a.id as request_id, a.title as title, a.category as category, 
        c.first_name as firstname, c.lastname as lastname FROM pr_request AS a, sys_user AS b, hr_employee AS c WHERE 
        b.id_hr_employee = a.user_id AND b.id_hr_employee = c.id and a.state = 'active' order by 
        createdAt DESC");
        return view('requests.show_requests')->with('details', $details);
    }

    public function request_details($id){ //Fetch all items for this particular request
        $items  = DB::select("SELECT request_items.*, request_status_line_manager.definition AS line_manager_status, request_status_line_manager.id AS line_manager_status_id, request_status_c_level.definition AS c_level_status, request_status_c_level.id AS c_level_status_id FROM
                request_items INNER JOIN request_status AS request_status_line_manager ON request_items.line_manager_remark = request_status_line_manager.id INNER JOIN request_status AS request_status_c_level ON request_items.c_level_remark = request_status_c_level.id 
                 WHERE request_items.state = 'active' AND request_items.request_id = '$id'");
        //Fetch all vendors
        $vendors = DB::select("select id, name from sys_vendors");
        return view('requests.request_details')->with('items', $items)
                                                ->with('vendors', $vendors);
    }
    public function lineManagerRequests(){ //Fetch all pending requests for a line manager
        $user_id = Auth::user()->id_hr_employee;
        if(!is_null($user_id)){
            $details = DB::select("SELECT a.created_at as createdAt, a.id as request_id, a.title as title, a.category as category, c.first_name as firstname, c.lastname as lastname FROM pr_request AS a, sys_user AS b, hr_employee AS c WHERE b.id_hr_employee = a.user_id AND b.id_hr_employee = c.id and a.state = 'active' and a.line_manager =  $user_id and a.line_manager_remark = 1 order by createdAt DESC");
        }else{
            $details = null;
        }
        return view('erp.procurement.line_manager_request')->with('details', $details);
    }
    public function lineManagerRequestsDetails($id){ //Shows details of individual request items
        $items = DB::select("SELECT b.id as main_id, a.title as item_title, a.description as description, a.price as price, a.quantity as quantity, a.total as total, a.id as request_id, b.* FROM request_items as a,
        pr_request as b WHERE a.request_id = b.id and b.id = '$id' and a.state = 'active'");
        return view('requests.showLineManagerRequestDetails')->with('items', $items)
                                                            ->with('request_id', $id);
    }
    public function update_request_details($id, $request_id){
        $details = DB::select("select * from request_items where id = $id");
        return view('requests.update_request_details')->with('details', $details)
                                                    ->with('request_id', $request_id);

    }
    public function request_update(Request $request){ //Update a request item details (for only line manager and c-level staff)
        $method = $request->isMethod('post');
        //return $method;
        if($method){
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'description' => 'required',
                'price' => 'required|numeric',
                'quantity' => 'required|numeric',
                'total' => 'required|numeric' 
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)
                            ->withInput();
            }
            //Get salvage value from DB
            $item_id = $request->item_id;
            $quantity = $request->quantity;
            $salvage_id = $request->salvage_id;
            $query = DB::select("select * from salvages where id = '$salvage_id'");
            //Get salvage value
            foreach ($query as $query){
                $salvage_value = $query->value;
            }
            $value = $salvage_value * $quantity;
            //Update request_items table for this particular item and note who did the edit in request_items_edit table
            $update = DB::table('request_items')
                        ->where('id', $request->item_id)
                        ->update([
                            'title' => $request->title,
                            'description' => $request->description,
                            'price' => $request->price,
                            'quantity' => $request->quantity,
                            'total' => $request->total,
                            'salvage_value' => $value
                        ]);
           
            if($update){
                //Table updated
                $request_edits = new RequestItemsEdit;
                $request_edits->item_id = $request->item_id;
                $request_edits->user_id = Auth::user()->id_hr_employee;

                if($request_edits->save()){
                    return redirect()->route('lineManagerRequests', ['id' => $request->request_id]);
                }
            }else{
                //Nothing updated but still redirect
                return redirect()->route('lineManagerRequests', ['id' => $request->request_id]);
            }
            
        }
    }

    public function addSalvage(Request $request)
    {
        $validator = Validator::make($request->all(), [
               'salvage_name' => 'required',
               'period' => 'required|integer',
                'value' => 'required',
            ]);
            if($validator->fails()){
                return back()->withErrors($validator)->withInput();
            }
        $salvage = Salvage::create([
            'name' => $request->salvage_name,
            'period' => $request->period,
            'value' => $request->value,
            'advance_company_id' => auth()->user()->company->id,
            'advance_company_uri' => auth()->user()->company->uri,
             ]);
        if($salvage)
            return back()->with('success', 'Salvage item added!');
        return back()->with('info', 'Something went wrong!');
    }

    public function send_rfq(Request $request){

        //Check to ensure that rfq has not been sent already
        $count = DB::table('request_items')->where(['id' => $request->item_id, 'rfq_status' => 1])->get();
        if(count($count) == 0) { //Generate RFQ
            $validator = Validator::make($request->all(), [
               'vendor_id' => 'required',
               'response_date' => 'required',
                'send_option' => 'required'
            ]);
            if($validator->fails()){
                return back()->withErrors($validator)->withInput();
            }
            //Insert into sys_rfq table
            $request_date = Carbon::now();
            $response_date = date('Y-m-d', strtotime($request->response_date));
            $sys_rfq = new Sysrfq;
            $sys_rfq->item_id = $request->item_id;
            $sys_rfq->procurement_officer_id = Auth::user()->id_hr_employee;
            $sys_rfq->request_date = $request_date;
            $sys_rfq->response_date = $response_date;
            $sys_rfq->vendor_id = $request->vendor_id;
            $sys_rfq->rfq_status = 1; //Sent

            if ($sys_rfq->save()) {
                //Update the rfq status of this item on thee request_items table
                $update = DB::table('request_items')
                    ->where('id', $request->item_id)
                    ->update(['rfq_status' => 1]);
                if ($request->send_option == 1) { //Send email to vendor with attached RFQ
                    $vendor_id = $request->vendor_id;
                    $item_id = $request->item_id;
                    //Fetch details of this vendor
                    $vendor = DB::select("SELECT * from sys_vendors where id = $vendor_id");
                    foreach ($vendor as $vendor_details) {
                        $vendor_email = $vendor_details->email;
                        $la_user = $vendor_details->name;
                    }
                    $rfq_id = $sys_rfq->id;
                    //Fetch details  for this rfq
                    $request = DB::select("select a.id as rfq_id, a.item_id as item_id, b.first_name as first_name, b.lastname as lastname, a.request_date as request_date, a.response_date as response_date from sys_rfq as a, hr_employee as b where a.procurement_officer_id = b.id");
                    //Fetch details of the item
                    $item = DB::select("select * from request_items where id = $item_id");
                    $pdf = PDF::loadView('requests.send_rfq_pdf', compact('item', 'request'));
                    //Send an email attachment of the request
                    $send_email2 = Mail::send('requests.sendrfq_email', compact('vendor'), function ($message) use ($vendor_email, $la_user, $pdf) {

                        $message->to($vendor_email, $la_user);
                        $message->from('admin@taerp.com', 'TA Procurement');
                        $message->subject('New Request for Quote');
                        $message->attachData($pdf->output(), 'request_for_quote.pdf', []);
                    });

                    if ($send_email2) {
                        return back()
                            ->with('status', 'RFQ successfully sent to vendor');
                    }
                } else if ($request->send_option == 2) { //Generate excel sheet (of the rfq) for this vendor
                    $vendor_id = $_POST['vendor_id'];
                    $item_id = $_POST['item_id'];
                    //Fetch details of this vendor
                    $vendor = DB::select("SELECT * from sys_vendors where id = $vendor_id");
                    foreach ($vendor as $vendor_details) {
                        $vendor_email = $vendor_details->email;
                        $la_user = $vendor_details->name;
                    }
                    $rfq_id = $sys_rfq->id;
                    //Fetch details  for this rfq
                    $request = DB::select("select a.id as rfq_id, a.item_id as item_id, b.first_name as first_name, b.lastname as lastname, a.request_date as request_date, a.response_date as response_date from sys_rfq as a, hr_employee as b where a.procurement_officer_id = b.id");
                    //Fetch details of the item
                    $item = DB::select("select * from request_items where id = $item_id");
                    Excel::create('Request for Quote', function ($excel) use ($item, $request) {
                        $excel->sheet('RFQ 1', function ($sheet) use ($item, $request) {
                            $sheet->loadView('requests.send_rfq_pdf')
                                ->with('item', $item)
                                ->with('request', $request);
                        });
                    })->export('xls');
                    return back();

                }
            }
        }else{
            //Return a message that RFQ has already been sent
            return back()
                ->with('status', 'Error! RFQ already sent');
        }
        
    }
    public function lineManagerUpdateRequests(Request $request){
        //Update the status of the request items
        $request_id = $request->request_id;
        $validator = Validator::make($request->all(), 
            ['item_id' => 'required'], ['item_id.required' => 'Either approve or decline all items']
        );

        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        foreach ($request->input('item_id') as $id => $value) {
            $update = DB::table('request_items')
                        ->where('id', $id)
                        ->update(['line_manager_remark' => $value]);
       
        }
        if($update){ //If items have been updated, update the line_manager status of the main request to show that the request has been treated.
            $request_id = $request->request_id;
            $updateRequest = DB::table('pr_request')
                            ->where('id', $request_id)
                            ->update(['line_manager_remark' => 2]);
            if($updateRequest) {
                //Send a mail to cLevel staff for the approved items i.e. if any was approved. You got that?
                $stmt = DB::table('request_items')->where([
                    'request_id' => $request_id,
                    'line_manager_remark' => 2
                ])->get();
                $result = count($stmt);
                if ($result >= 1){ //If item(s) is approved
                    //Fetch details for this request
                    $request = DB::select("SELECT a.created_at as createdAt, a.id as request_id, a.title as title, 
                        a.category as category, c.first_name as firstname, c.lastname as lastname FROM pr_request AS a, sys_user AS b, 
                        hr_employee AS c WHERE b.id_hr_employee = a.user_id AND b.id_hr_employee = c.id and a.id = '$request_id' and 
                        a.state = 'active'");
                    //Fetch item details
                    $items = DB::select("SELECT * FROM request_items WHERE request_id = '$request_id' and line_manager_remark = 2 and state = 'active'");
                    //Load procurement request document
                    $pdf = PDF::loadView('requests.request_sendpdf', compact('items', 'request'));

                    //Send mail to all C level staff
                    $details = DB::select("select a.*, b.* from hr_employee as a,  hr_grade as b where
                    a.id_hr_grade = b.id and b.id = 4");
                    foreach ($details as $detail) { //Loop through all C level staff
                        $la_user = $detail->first_name.''.$detail->lastname;
                        $email = $detail->email;
                        $send_email2 = Mail::send('requests.request_sendmailclevel', compact('detail'), function ($message) use ($email, $la_user, $pdf){

                            $message->to($email, $la_user);
                            $message->from('admin@taerp.com', 'TA Procurement');
                            $message->subject('New Procurement Request');
                            $message->attachData($pdf->output(), 'procurement_request.pdf', []);
                        });
                    }
                    return redirect()->route('lineRequests');
                }

            }else{
                return 'false';
            }

        }else{
            return false;
        }    
       
    }
    public function cLevelRequests(){ //Fetch all pending requests for cLevel Manager
        //Get Hr grade to confirm this user is a cLevel Staff
        $id = Auth::user()->id_hr_employee;
        if(!is_null($id)){
            $db = DB::select("select id_hr_grade from hr_employee where id = $id");
            foreach ($db as $stmt) {
                $grade = $stmt->id_hr_grade; //Fetch the hr_grade for this user
            }
            if($grade == 4){ //Fetch details grade == 4 i.e. cLevel Manager  
                $details = DB::select("SELECT a.created_at as createdAt, a.id as request_id, a.title as title, a.category as category, 
                c.first_name as firstname, c.lastname as lastname FROM pr_request AS a, sys_user AS b, hr_employee AS c, request_items AS d WHERE 
                a.id = d.request_id and b.id_hr_employee = a.user_id AND b.id_hr_employee = c.id and a.state = 'active' 
                and a.c_level_remark = 1 and d.line_manager_remark = 2
                order by createdAt DESC");
                return view('erp.procurement.showcLevelRequests')->with('details', $details);
            }else{
                return view('erp.procurement.showcLevelRequests');
            }   
        }else{
            return view('erp.procurement.showcLevelRequests');
        } 
        
    }
    public function cLevelRequestsDetails($id){
        $items = DB::select("SELECT b.id as main_id, a.title as item_title, a.description as description, a.price as price, 
        a.quantity as quantity, a.total as total, a.id as request_id, b.* FROM request_items as a,
        pr_request as b WHERE a.request_id = b.id and b.id = '$id' and a.state = 'active' and a.line_manager_remark = 2");
        return view('requests.showcLevelRequestDetails')->with('items', $items)
                                                            ->with('request_id', $id);
    }
    public function clevel_update_request($id, $request_id){
        $details = DB::select("select * from request_items where id = $id");
        return view('requests.clevel_update_details')->with('details', $details)
                                                    ->with('request_id', $request_id);
    }
    public function clevel_request_update(Request $request){
        $method = $request->isMethod('post');
        if($method){
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'description' => 'required',
                'price' => 'required|numeric',
                'quantity' => 'required|numeric',
                'total' => 'required|numeric' 
            ]);
            $quantity = $request->quantity;
            $salvage_id = $request->salvage_id;
            $query = DB::select("select * from salvages where id = '$salvage_id'");
            //Get salvage value
            foreach ($query as $query){
                $salvage_value = $query->value;
            }
            $value = $salvage_value * $quantity;
            if ($validator->fails()) {
                return back()->withErrors($validator)
                            ->withInput();
            }

            //return $request->request_id;
            //Update request_items table for this particular item and note who did the edit in request_items_edit table
            $update = DB::table('request_items')
                        ->where('id', $request->item_id)
                        ->update([
                            'title' => $request->title,
                            'description' => $request->description,
                            'price' => $request->price,
                            'quantity' => $request->quantity,
                            'total' => $request->total,
                            'salvage_value' => $value
                        ]);
           
            if($update){
                //Table updated
                $request_edits = new RequestItemsEdit;
                $request_edits->item_id = $request->item_id;
                $request_edits->user_id = Auth::user()->id_hr_employee;

                if($request_edits->save()){
                    return redirect()->route('cLevelRequestsDetails', ['id' => $request->request_id]);
                }
            }else{
                //Nothing updated but still redirect
                return redirect()->route('cLevelRequestsDetails', ['id' => $request->request_id]);
            }
            

        }

    }
    public function cLevelUpdateRequests(Request $request){
        //Update the status of the request items
        $request_id = $request->request_id;
        
        foreach ($request->input('item_id') as $id => $value) {
            $update = DB::table('request_items')
                        ->where('id', $id)
                        ->update(['c_level_remark' => $value]);
       
        }
        if($update){ //If items have been updated, update the c_level status of the main request to show that the request has been treated.
            $request_id = $request->request_id;
            $updateRequest = DB::table('pr_request')
                            ->where('id', $request_id)
                            ->update(['c_level_remark' => 4]);
            if($updateRequest){
                //Send mail to finance team requesting for funds disbursement
                return redirect()->route('cLevelRequests');
            }else{
                return 'false';
            }

        }else{
            return 'false';
        }  
          
       
    }
    public function request_delete(Request $request){ //Soft delete a request + her items
        $request_id = $request->request_id;
        $update_request = DB::table('pr_request')
                            ->where('id', $request_id)
                            ->update(['state' => 'inactive']);
        $update_items = DB::table('request_items')
                            ->where('request_id', $request->id)
                            ->update(['state'=> 'inactive']);
            return back()->with('status', 'Request deleted');
    }
    public function request_send($id){ //Send email notifications to concerned parties
        $request = DB::select("SELECT a.created_at as createdAt, a.id as request_id, a.title as title, a.category as category, c.first_name as firstname, c.lastname as lastname FROM pr_request AS a, sys_user AS b, hr_employee AS c WHERE b.id_hr_employee = a.user_id AND b.id_hr_employee = c.id and a.id = '$id' and a.state = 'active'");
        $items = DB::select("SELECT * FROM request_items WHERE request_id = '$id' and state = 'active'");

        $pdf = PDF::loadView('requests.request_sendpdf', compact('items', 'request'));
        //Send mail to line manager

        $user_id = Auth::user()->id_hr_employee;
        if(!is_null($user_id)){
            $details = DB::select("select a.*, b.* from hr_employee as a,  hr_line_manager as b where
                a.id = b.parent_employee_id and child_employee_id = $user_id");

            $requestby = DB::select("select first_name, lastname from hr_employee where id = $user_id");
            $la_user = null;
            foreach ($details as $details) {
                $la_user = $details->first_name.''.$details->lastname;
            }
            $email = isset($details->email) ? $details->email : null;
            $send_email = Mail::send('requests.request_sendmail', compact('details'), function ($message) use ($email, $la_user, $pdf){
            
            $message->to($email, $la_user);
            $message->from('admin@taerp.com', 'TA Procurement');
            $message->subject('New Procurement Request');
            $message->attachData($pdf->output(), 'procurement_request.pdf', []);
            });
            if ($send_email) {
                return back()->with('success', 'Procurement Request sent successfully');
            }
        }else{
            return back()->with('error', 'Unable to send email');
        }
        
    }
    public function viewReports(Request $request){
        $method = $request->isMethod('post');
        if($method){

        }else{
            //Fetch all requests that are not pending i.e. line managers and clevel managers have responded
            $requests = DB::select("select a.title as title, a.category as category, a.created_at as created, b.first_name as firstname, b.lastname as lastname from 
                pr_request as a, hr_employee as b where 
                a.user_id = b.id and (a.line_manager_remark != 1 and a.c_level_remark != 1)");
            return view('erp.procurement.procurement_report')
                    ->with('requests', $requests);
        }
    }
    public function edit_item(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|integer',
            'total' => 'required|numeric',
            'salvage' => 'required'
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $id = $request->request_id;

        $salvage_id = $request->salvage;
        $quantity = $request->quantity;
        $salvages = DB::select("select * from salvages where id = '$salvage_id'");
        foreach ($salvages as $salvages){
            $salvage_value = $salvages->value;
        }
        $value = $salvage_value * $quantity;

        $update = DB::table('request_items')
            ->where('id', $request->item_id)
            ->update([
                'title' => $request->title,
                'description' => $request->description,
                'price' => $request->price,
                'quantity' => $request->quantity,
                'total' => $request->total,
                'salvage_id' => $salvage_id,
                'salvage_value' => $value
        ]);
        if($update){
            $route = route('add_requests',['id' => $id]);
            return back()->with('success', 'Item updated successfully!');
        }
    }
    public function delete_item(Request $request){
        
        $delete = DB::table('request_items')
            ->where('id', $request->item_id)
            ->update([
              'state' => 'inactive'  
            ]); 
        if($delete){
           return back()->with('info', 'Item de-activated successfully');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function clients()
    {
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $clients = DB::select("SELECT * FROM sys_clients WHERE active = 1 AND advance_company_id = '$company_id' AND advance_company_uri = '$company_uri'");
        $countries = self::getCountries();
        $countries = json_decode($countries);
        $countries = $countries->result;
        $countries = (array) $countries;
        $states = DB::table('states')->get();
        $cities = DB::table('cities')->get();
        return view('erp.finance.client', compact('countries', 'clients', 'states', 'cities'));
    }

    public function getCountryStates($country_id){
            $data = [];
            if($country_id){
                $search = $country_id;
                $data = Utilities::getStates($country_id);  
                $data = (array) $data['result']; 
            }
            return response()->json($data);
    }

     public function getStateCities($state_id){
            $data = [];
            if($state_id){
                $search = $state_id;
                $data = Utilities::getCities($state_id);  
                $data = (array) $data['result']; 
            }
            return response()->json($data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendors()
    {
        $vendors = DB::select('SELECT * FROM sys_vendors WHERE active = 1');
        return view('requests.vendors')->with(['vendors' => $vendors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_client(Request $request)
    {
        $method = $request->isMethod('post');
        if ($method) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'description' => 'required|max:255',
                'address' => 'required|max:255',
                'phone' => 'required|max:255',
                'country' => 'required',
                'state' => 'required',
                'city' => 'required',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $client = new Client();
            $client->name = $request->name;
            $client->description = $request->description;
            $client->address = $request->address;
            $client->phone = $request->phone;
            $client->country = $request->country;
            $client->state = $request->state;
            $client->city = $request->city;
            $client->created_by = Auth::user()->id;
            $client->tin = isset($request->tin) ? $request->tin : null;
            $client->advance_company_id = auth()->user()->company->id;
            $client->advance_company_uri = auth()->user()->company->uri;
            if ($client->save()) {
                return back()->with('success', 'Client added!');
            }else{
                return back()->with('error', 'All fields are required!');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_vendor(Request $request)
    {
        $adv_company_id = auth()->user()->company->uri;
        $adv_company_uri = auth()->user()->company->uri;

        $method = $request->isMethod('post');

        if ($method) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'description' => 'required|max:255',
                'address' => 'required|max:255',
                'phone' => 'required|max:255',
                'tin' => 'required|max:255',
                'country' => 'required|max:255',
                'state' => 'required|max:255',
                'email' => 'required|unique:sys_vendors'
            ]);

            if ($validator->fails()) {
                return back()->with('error', $validator)->withInput();
            }

            $chk_duplicate = Vendor::where('name', $request->name)->first();
            if ($chk_duplicate) {
                return back()->with('error', 'A Vendor with the name '.$request->name.' is already on record');
            }

            $client = new Vendor();
            $client->name = $request->name;
            $client->description = $request->description;
            $client->address = $request->address;
            $client->phone = $request->phone;
            $client->country = $request->country;
            $client->created_by = Auth::user()->id;
            $client->advance_company_id = $adv_company_id;
            $client->advance_company_uri = $adv_company_uri;
            $client->email = $request->email;
            $client->tin = isset($request->tin) ? $request->tin : null;
            if ($client->save()) {
                return back()->with('success', 'Vendor added!');
            }
        } else {
            $countries = Utilities::countries();
            $vendors = DB::select("SELECT * FROM sys_vendors WHERE active = 1 AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri'");
            $states = DB::table('states')->get();
            return view('erp.finance.vendor')->with(['countries' => $countries, 'vendors' => $vendors, 'states' => $states]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update_client(Request $request, $client_id = null)
    {
        $method = $request->isMethod('post');
        if ($method) {
            // dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'description' => 'required|max:255',
                'address' => 'required|max:255',
                'phone' => 'required|max:255',
                'country' => 'required|max:255',
                'city' => 'required|max:255',
                'state' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $update_client = DB::table('sys_clients')
                ->where('id', $request->client_id)
                ->update(
                    [
                        'name' => $request->name,
                        'description' => $request->description,
                        'address' => $request->address,
                        'phone' => $request->phone,
                        'country' => $request->country,
                        'city' => $request->city,
                        'state' => $request->state,
                        'updated_by' => Auth::user()->id,
                        'updated_at' => date('Y-m-d h:i:s')
                    ]
                );
            if ($update_client) {
                return back()->with('success', 'Client record updated!');
            }else{
                return back()->with('error', 'An error occured while updating client');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update_vendor(Request $request)
    {
        $method = $request->isMethod('post');
        if ($method) {
            // dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'description' => 'required|max:255',
                'address' => 'required|max:255',
                'phone' => 'required|max:255',
                'tin' => 'required|max:255',
                'country' => 'required|max:255',
                'state' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->with('error', $validator);
            }
            $update_client = DB::table('sys_vendors')
                ->where('id', $request->vendor_id)
                ->update(
                    [
                        'name' => $request->name,
                        'description' => $request->description,
                        'address' => $request->address,
                        'phone' => $request->phone,
                        'tin' => $request->tin,
                        'country' => $request->country,
                        'state' => $request->state,
                        'updated_by' => Auth::user()->id,
                        'updated_at' => date('Y-m-d h:i:s')
                    ]
                );
            if ($update_client) {
                return back()->with('success', 'Clients added!');
            }
        } else {
            $countries = Utilities::getCountries();
            $states = DB::table('states')->get();
            $cities = DB::table('cities')->get();
            $client = DB::select("SELECT * FROM sys_vendors WHERE id = '$client_id'");
            return view('requests.update_vendor')->with(['countries' => $countries, 'states' => $states, 'cities' => $cities, 'client' => $client[0]]);
        }
    }

    public function getCountries()
    {
        return json_encode(Utilities::getCountries());
    }

    public function getStates($country_id)
    {
        return json_encode(Utilities::getStates($country_id));
    }

    public function getCities($state_id)
    {
        return json_encode(Utilities::getCities($state_id));
    }

}
