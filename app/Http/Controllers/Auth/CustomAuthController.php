<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\HttpHelper;
use App\Libraries\Utilities;
use App\Models\Organization;
use App\Traits\Throttles;
use Exception;
use App\Models\Activity;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Company;
use Illuminate\Support\Facades\Cache;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Carbon\Carbon;
use Auth;

use App\Libraries\ApiServices;

class CustomAuthController extends Controller
{
    //implement App\Traits\Throttles;
    use ThrottlesLogins;

    private $httpHelper;

    protected $redirectTo = '/';
    /**
     * CustomAuthController constructor.
     */
    public function __construct() {
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Show the main login page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm() {
        return view("auth.login");
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);
    }
    /**
     * Authenticate against the  API
     * @param AuthenticationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function getCredentials(Request $request)
    {
        return $request->only($this->loginUsername(), 'password');
    }

    protected function getGuard()
    {
        return property_exists($this, 'guard') ? $this->guard : null;
    }

    public function authenticate(Request $request) {
        $authenticated = false;
        $this->validateLogin($request);

        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        $this->saveAttemptRecord($request, $authenticated);
        
        $data = [
            "email" => $request->email,
            "password" => $request->password
            ];
        //allow login with email or phone by replacing the email key with phone conditionally
        if(filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $data['email'] = $request->email;
        }else{
            $data['phone'] = $data['email'];
            unset($data['email']);
        }
        //attempt API AuthenticationRequest
        try {
            //other user (employee, business, procurement) login  details
            $other_user = null;
            if(filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                $other_user = User::where(['email' => $data['email']])->first();
            }else{
                $other_user = User::where(['advance_phone' => $data['phone']])->first();
            }

            if ($other_user && Auth::guard($this->getGuard())) {
                //verifying if its an employee login or others
                if(is_null($other_user->advance_username) && is_null($other_user->advance_email) || is_null($other_user)){
                        //authenticate user
                        //employee login successfull or other user role
                    if(filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                        //user sent their email so authorize and authenticate using email column
                       $authenticate_user = Auth::attempt(['email' => $data['email'], 'password' => $data['password'] ]);
                       if($authenticate_user){
                        $auth_user = Auth::attempt($credentials, $request->has('remember'));
                        $authenticated = true;
                        return $this->handleUserWasAuthenticated($request, $throttles);
                       }else{
                           return back()->with('error', 'Invalid login credentials');
                       }
                    }
                }elseif(!is_null($other_user->advance_username) && !is_null($other_user->advance_email)){
                    //try login user from advance endpoint because we assume he or she is an advance business user
                    $result = ApiServices::callAPI($data, '/authentication/customers', NULL, 'POST');
                    if($result['code'] == "200"){ //created
                        $advance_user = new User();
                        /* Will set any  user specific fields returned by the api request*/
                        $user = (object) $result['your_response'];
                        $existing_user = User::where('advance_email', $user->email)->where('advance_phone', $user->phone)->first();

                        //save user credentials on local db if its his first time login.
                        if(!$existing_user){
                            //save as employee as new user because we assume all users on the app is an employee.
                            $employee = Employee::create([
                            ]);
                            $advance_user->advance_username = $user->username;
                            $advance_user->advance_email = $user->email;
                            $advance_user->advance_password = \Hash::make($data['password']);
                            $advance_user->password = \Hash::make($data['password']);
                            $advance_user->advance_phone = $user->phone;
                            $advance_user->email = $user->email;
                            $advance_user->id_hr_employee = $employee->id;
                            $advance_user->role = "35"; //default as 35 for advance erp from sys_roles and sys_menus
                            $advance_user->save();
                            //create a company record.
                            $company = Company::create([
                                'user_id' => $advance_user->id,
                                'uri' => uniqid().rand(100, 1000),
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()
                            ]);
                            $update_user = \DB::SELECT("UPDATE sys_user SET `company_id` = '$company->id', `company_uri` = '$company->uri' WHERE id = '$advance_user->id'");
                        }
                        //store authenticated and user in session to be checked by authentication middleware

                        $request->session()->put('authenticated',true);
                        $request->session()->put('user',$user);
                        
                        if (Auth::guard($this->getGuard())) {
                            //authenticate user
                            if(filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                                //user sent their email so authorize and authenticate using email column
                                Auth::attempt(['email' => $data['email'], 'password' => $data['password'] ]);
                            } else {
                                //user sent their phone number so authorize and authenticate using advance_phone column
                                Auth::attempt(['advance_phone' => $data['phone'], 'password' => $data['password'] ]);
                            }
                            $auth_user = Auth::attempt($credentials, $request->has('remember'));
                            $authenticated = true;
                        }
                        $this->determineOrganizationUpdateAndCreate();
                        return $this->handleUserWasAuthenticated($request, $throttles);
                    }else{
                        return back()->with('error', $result['message']);
                    }
                }
            }elseif(is_null($other_user)){
                    //login user because we assume its his first time login on the erp platform and he is a business or corporate user
                    $result = ApiServices::callAPI($data, '/authentication/customers', NULL, 'POST');
                    if($result['code'] == "200"){ //created
                        $advance_user = new User();
                        /* Will set any  user specific fields returned by the api request*/
                        $user = (object) $result['your_response'];
                        $existing_user = User::where('advance_email', $user->email)->where('advance_phone', $user->phone)->first();

                        //save user credentials on local db if he its a first time login for n
                        if(!$existing_user){
                            //save as employee
                            $employee = Employee::create([
                            ]);
                            $advance_user->advance_username = $user->username;
                            $advance_user->advance_email = $user->email;
                            $advance_user->advance_password = \Hash::make($data['password']);
                            $advance_user->password = \Hash::make($data['password']);
                            $advance_user->advance_phone = $user->phone;
                            $advance_user->email = $user->email;
                            $advance_user->id_hr_employee = $employee->id;
                            $advance_user->role = "35"; //default as 35 for advance erp from sys_roles and sys_menus
                            $advance_user->save();
                            $company = Company::create([
                                'user_id' => $advance_user->id,
                                'uri' => uniqid().rand(100, 1000),
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()
                            ]);
                            $update_user = \DB::SELECT("UPDATE sys_user SET `company_id` = '$company->id', `company_uri` = '$company->uri' WHERE id = '$advance_user->id'");
                        }
                        //store authenticated and user in session to be checked by authentication middleware

                        $request->session()->put('authenticated',true);
                        $request->session()->put('user',$user);
                        
                        if (Auth::guard($this->getGuard())) {
                            //authenticate user
                            if(filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                                //user sent their email so authorize and authenticate using email column
                                Auth::attempt(['email' => $data['email'], 'password' => $data['password']]);
                            } else {

                                //user sent their phone number so authorize and authenticate using advance_phone column
                                Auth::attempt(['advance_phone' => $data['phone'], 'password' => $data['password'] ]);
                            }
                            $auth_user = Auth::attempt($credentials, $request->has('remember'));
                            $authenticated = true;
                        }
                        if(auth()->user()){
                            // dd("sdsd");
                            $this->determineOrganizationUpdateAndCreate();
                            return $this->handleUserWasAuthenticated($request, $throttles);
                        }else{
                            return back()->with('error', 'User credentials doesnt havea an E.R.P account');
                        }
                        
                    }else{
                        return back()->with('error', $result['message']);
                    }
            }
            //create user to store in session

        } catch(\GuzzleHttp\Exception\ClientException $e) {
            // dd($e);
            //track login attempt
            $this->incrementThrottleValue("login", $this->generateLoginThrottleHash($request));
            //remove user and authenticated from session
            $request->session()->forget('authenticated');
            $request->session()->forget('user');

            return redirect()->back()->with('error', 'The credentials do not match our records');
        }
        //login success - redirect to home page
        // $this->resetThrottleValue("login", $this->generateLoginThrottleHash($request));
        return redirect()->action("HomeController@home");
    }

    public function determineOrganizationUpdateAndCreate()
    {
        // try {
            $company_organizations = ApiServices::callApi(null, "/customer/".auth()->user()->advance_username."/organisations", null, 'GET');
            //pull exisiting adv user organizations
            $existing_organization = Utilities::companyOrganizations(auth()->user()->company->id, auth()->user()->company->uri);
            $organizations = (object) $company_organizations['your_response'];
            $res = [];
            foreach($organizations as $org){
                array_push($res, $org);
            }
            if(is_array($res)){
                $company_id = auth()->user()->company->id;
                $company_uri = auth()->user()->company->uri;
                //trash existing company organizations first
                foreach($res as $org){

                   $checkDuplicate = Utilities::checkExistingAdvanceOrganizationDuplicate($org['uid']);
                    //checking if the current organization already exist in our org local db
                    if($checkDuplicate){
                        //update local org row
                        $found_org = Organization::where(
                            ['uid' => $org['uid'], 'company_id' => auth()->user()->company->id, 'company_uri' => auth()->user()->company->uri]
                            )
                        ->firstOrFail();
                        $organization = $found_org->update([
                            'name' => $org['business_name'],
                            'registration_number' => $org['registration_number'],
                            'business_sector' => $org['business_sector'],
                            'business_type' => $org['business_type'],
                            'certificate_image' => $org['certificate_image'],
                            'date_incorporated' => $org['date_incorporated'],
                            'email' => $org['email'],
                            'phone' => $org['phone'],
                            'postal_address' => $org['postal_address'],
                            'tax_identification_number' => $org['tax_identification_number'],
                            'website' => $org['website'],
                            'address' => $org['address'],
                            'company_id' => $company_id,
                            'company_uri' => $company_uri,
                            'updated_by' => auth()->user()->id
                        ]);
                    }else{
                        //if the current organization index is a newly addded organization
                        //from advance mobile app then create a new org....
                        $organization = Organization::create([
                            'name' => $org['business_name'],
                            'registration_number' => $org['registration_number'],
                            'business_sector' => $org['business_sector'],
                            'business_type' => $org['business_type'],
                            'certificate_image' => $org['certificate_image'],
                            'date_incorporated' => $org['date_incorporated'],
                            'email' => $org['email'],
                            'phone' => $org['phone'],
                            'postal_address' => $org['postal_address'],
                            'tax_identification_number' => $org['tax_identification_number'],
                            'uid' => $org['uid'],
                            'website' => $org['website'],
                            'address' => $org['address'],
                            'code'=> $org['registration_number'],
                            'company_id' => $company_id,
                            'company_uri' => $company_uri,
                            'created_by' => auth()->user()->id,
                            'updated_by' => auth()->user()->id
                        ]);
                    }
                }
            }
        // }catch (\Exception $ex){
        //             return back()->with('error', $ex->getMessage());
        // }
    }

    /**
     * Log user out
     * @param Request $request
     * @return type
     */
    public function logout(Request $request) {
        //remove authenticated from session and redirect to login
        Auth::logout();
        $request->session()->forget('authenticated');
        $request->session()->forget('user');
        return redirect('login');
    }

    public function saveAttemptRecord(Request $request, $isAuthenticated){
        $activity = new Activity();
        $activity->username = $this->getUserName($request);
        $activity->ip_address = ip2long($request->ip());
        $activity->user_agent = $request->header('User-Agent');
        $activity->method = $request->method();
        $activity->url = $request->url();
        $activity->data = null;
        $activity->remarks = $isAuthenticated? "Successfully logged in" : "Failed log in attempt";
        $activity->save();
    }

    protected function isUsingThrottlesLoginsTrait()
    {
        return in_array(
            ThrottlesLogins::class, class_uses_recursive(static::class)
        );
    }

    public function loginUsername()
    {
        return property_exists($this, 'username') ? $this->username : 'email';
    }

    // Login throttling functions

    /**
     * @param AuthenticationRequest $request
     * @return string
     */
    private function generateLoginThrottleHash(Request $request) {
        return md5($request->email . "_" . $request->getClientIp());
    }

    private function getUserName(Request $request){
        return isset($request->only($this->loginUsername())[$this->loginUsername()])? $request->only($this->loginUsername())[$this->loginUsername()] : '';
    }

    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::guard($this->getGuard())->user());
        }
        return redirect()->intended($this->redirectPath());
    }

    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : "/home";
    }
}