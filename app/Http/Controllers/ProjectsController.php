<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Employee;
use App\Models\Project;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class ProjectsController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function issues(Request $request){
        //Fetch Jira credentials
        $user_id = Auth::user()->id_hr_employee;
        $credentials = DB::select("select username, password from jira_credentials where user_id = '$user_id'");
        foreach ($credentials as $credential){
            $username = $credential->username;
            $password = decrypt($credential->password);
        }
        $key = $request->key;
        $url = env('JIRA');
        $new_url = $url.'search?jql=project='.$key.'+order+by+duedate&fields=id,key';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8080",
            CURLOPT_URL => $new_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic ".base64_encode( $username . ":" . $password ),
                //"Authorization: Basic dWNoZWNodWt3dS5vYmlvaGE6QFJhZGlvYWN0aXZlMTE=",
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $issues = json_decode($response, true);
            $data  = $issues['issues'];
            $array = [];
            foreach ($data as $data){
                $url = $data['self'];
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_PORT => "8080",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "authorization: Basic ".base64_encode( $username . ":" . $password ),
                        "cache-control: no-cache",
                        "content-type: application/json"
                    ),
                ));

                $res = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
                $details = json_decode($res, true);
                    $issueType = $details['fields']['issuetype']['name'];
                    $description = $details['fields']['description'];
                    $summary = $details['fields']['summary'];
                    $creator = $details['fields']['creator']['displayName'];
                    $created = $details['fields']['created'];
                    $duedate = $details['fields']['duedate'];
                    $status = $details['fields']['status']['description'];
                    $progress = $details['fields']['status']['name'];
                    array_push($array, (object)['issueType' => $issueType, 'description' => $description, 'summary' => $summary,
                        'creator' => $creator, 'created' => $created, 'duedate' => $duedate, 'status' => $status, 'progress' => $progress]);
                }
            return view('projects.issues')->with(['issues' => $array]);
        }

    }
    public function projects(Request $request)
    {
        //Fetch all projects from JIRA using their endpoints
        //Projects fetched is dependent on the user logged on
        //I use cURL, no apologies
        $user_id = Auth::user()->id_hr_employee;
        $credentials = DB::select("select username, password from jira_credentials where user_id = '$user_id'");
        if(count($credentials) == 0){ //No jira record found for this user
            $request->session()->flash('message', 'No JIRA Credentials found! Click on your profile to add');
            return view('projects.projects');
        }else {
            //If found, fetch JIRA details
            foreach ($credentials as $credential){
                $username = $credential->username;
                $password = decrypt($credential->password);
            }

            $url = env('JIRA');
            $new_url = $url.'project';
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $new_url,
                CURLOPT_PORT => "8080",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Basic ".base64_encode( $username . ":" . $password ),
                    "cache-control: no-cache",
                    "content-type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                $request->session()->flash('message', 'Wrong JIRA Credentials! Please Update!');
                return view('projects.projects');

            } else {
                $projects = json_decode($response, true);
                if($projects == null){
                    $request->session()->flash('message', 'Wrong JIRA Credentials! Please Update!');
                    return view('projects.projects');
                }
                foreach ($projects as $project){
                    $geturl = $project['self']; //Get url
                    $getProject = $project['name'];
                    $getType = $project['projectTypeKey'];
                    $getKey = $project['key'];
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $geturl,
                        CURLOPT_PORT => "8080",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_HTTPHEADER => array(
                            "authorization: Basic ".base64_encode( $username . ":" . $password ),
                            "cache-control: no-cache",
                            "content-type: application/json"
                        ),
                    ));

                    $res = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    $details = json_decode($res, true);
                    $lead = $details['lead']['displayName'];
                    //Gather all data
                    $data[] = ['project' => $getProject, 'lead' => $lead, 'type' => $getType, 'key' => $getKey];

                }
                return view('projects.projects')->with(['projects' => $data]);
            }
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_project(Request $request)
    {
        $method = $request->isMethod('post');
        if ($method) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'client_id' => 'required|max:255',
                'description' => 'required|max:255',
                'manager' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $duplicate = Project::where('name', $request->name)->first();
            if ($duplicate) {
                return back()->withErrors('A Project with this name exists!')->withInput();
            }

            $project = new Project();
            $project->name = $request->name;
            $project->client_id = $request->client_id;
            $project->manager = $request->manager;
            $project->description = $request->description;
            $project->created_by = Auth::user()->id;
            if ($project->save()) {
                return redirect(route('projects'))->with('success', 'Project added!');
            }
        } else {
            $clients = DB::select('SELECT * FROM sys_clients WHERE active = 1');
            $employees = Employee::where('active',1)->get();
            return view('projects.add_project')->with(['clients' => $clients,'employees' => $employees]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update_project(Request $request, $id = null)
    {
        $method = $request->isMethod('post');
        if ($method) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'client_id' => 'required|max:255',
                'description' => 'required|max:255',
                'manager' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $project = Project::find($request->project_id);
            $project->name = $request->name;
            $project->client_id = $request->client_id;
            $project->description = $request->description;
            $project->manager = $request->manager;
            $project->created_by = Auth::user()->id;
            if ($project->save()) {
                return redirect(route('projects'))->with('success', 'Project added!');
            }
        } else {
            $project = Project::find($id);
            $clients = DB::select('SELECT * FROM sys_clients WHERE active = 1');
            $employees = Employee::where('active',1)->get();
            return view('projects.update_project')->with(['clients' => $clients, 'project' => $project,'employees' => $employees]);
        }
    }
}
