<?php

namespace App\Http\Controllers;

use App\Models\Permissions;
use App\Models\RolePermissions;
use App\Models\UserPermission;
use App\Models\Roles;
use Carbon\Carbon as Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Libraries\Utilities;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Roles::where(['advance_company_id' => auth()->user()->company->id, 'advance_company_uri' => auth()->user()->company->uri])->get();
        $sql = DB::select("SELECT * FROM sys_modules");
        return view('erp.system_settings.user.roles')->with(['roles'=>$roles, 'sql' => $sql]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sql = DB::select("SELECT * FROM sys_modules");
        return view('roles.create')->with('sql', $sql);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $advance_company_id = Auth::user()->company->id;
            $advance_company_uri = Auth::user()->company->uri;

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'description' => 'required',
                'resource' => 'required'
            ]);

            if ($validator->fails()) {
                return back()->with('error', $validator)->withInput();
            }
            $roles = null;
            $roles = Roles::create([
                'name' => $request->get('name'),
                'description'=> $request->get('description'),
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id,
                'advance_company_id'=>Auth::user()->company->id,
                'advance_company_uri'=>Auth::user()->company->uri

            ]);
            if($roles){ //If its inserted into roles table, then insert into permissions table   
                /**      
                foreach ($request->input('resources') as $id => $resources) { //nsert into the permission table
                    $role_id = $roles->id;
                    $permissions = implode(',', $resources);
                     UserPermission::create([
                    'role_id' => $role_id,
                    'resource_id' => $id,
                    'permissions' => $permissions
                    ]);
                }
                **/
                $permissions = 'c,r,u,d';
                foreach ($request->input('resource') as $id) {
                    $role_id = $roles->id;
                    UserPermission::create([
                    'role_id' => $role_id,
                    'resource_id' => $id,
                    'permissions' => $permissions,
                    'advance_company_id' => $advance_company_id,
                    'advance_company_uri' => $advance_company_uri
                    ]);
                }
                return back()->with('success', 'Role successfully added');
                
            }
            /**

            Roles::create([
                'name' => $request->get('name'),
                'slug'=> $request->get('slug'),
                'description'=> $request->get('description'),
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id
            ]);

            return redirect('roles');
            **/
        }catch (\Exception $ex){
            return back()->with('error', $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try{
            $permissions = Permissions::all();
            $role = Roles::find($id);

            if(empty($role)){
                return "Role resource not found";
            }
            $role->load(['rolePermissions.permission']);

            $rolePermissions = array(); //initialize empty array to keep all the permission for this role
            foreach($role->rolePermissions as $permission){
                $rolePermissions[$permission->id_sys_permissions] = $permission->level? $permission->level : '';
            }

            return view('roles.show')->with(['roles'=>$role, 'permissions' => $permissions, 'rolePermissions' => $rolePermissions]);
        }catch (\Exception $ex){
            return back()->withErrors($ex->getMessage())->withInput();
        }
    }

    public function editRole(Request $request, $id){
        $method = $request->isMethod('post');
        $sql = DB::select("SELECT * FROM sys_modules");

        $role = Roles::where('id', $id)->with('permissions')->first();
        $db_sel_val = [];
        foreach($role->permissions as $permission){
            array_push($db_sel_val, $permission->resource_id);
        }

        if($method) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'description' => 'required',
                'resource' => 'required'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $role = Roles::findOrFail($id);
            $role->update([
                'name' => $request->name,
                'description' => $request->description,
                'updated_at' => Carbon::now()
            ]);

            $permissions = 'c,r,u,d';
            $remove_resources_from_resource = DB::SELECT("DELETE FROM user_permissions WHERE role_id = '$id'");
            foreach ($request->input('resource') as $resource_id) {
                $role_id = $id;
                UserPermission::create([
                    'role_id' => $role_id,
                    'resource_id' => $resource_id,
                    'permissions' => $permissions
                ]);
            }
            return back()->with('success', 'Roles and Resources Updated Successfully!');
        }else{
            return view('erp.system_settings.user.edit_role', compact('sql', 'role', 'db_sel_val'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //get all request

        try{
            $role = Roles::find($id);

            if(empty($role)){
                return "Role resource not found";
            }
            $role->load(['rolePermissions.permission']);
            $assignedRolePermissions = array(); //initialize empty array to keep all the assigned permission for this role
            foreach($role->rolePermissions as $permission){
                $assignedRolePermissions[$permission->id_sys_permissions] =['level'=>$permission->level, 'id'=>$permission->id]; //saves the assigned permission id against the assigned level
            }
            $permissions = Permissions::all();//get all permissions

            foreach($permissions as $permission){ //run through all the permissions
                $selectedPermissionLevel = $request->get('level-'.$permission->id) ? $request->get('level-'.$permission->id) : null;
                if($request->get($permission->slug)){ //if permission was selected
                    //permission was selected
                    //create or update permission rule
                    //check if permission was already assigned
                    if(isset($assignedRolePermissions[$permission->id])){
                        //permission was assigned; we need to update the permission
                        if($assignedRolePermissions[$permission->id]['level'] != $selectedPermissionLevel){
                            //update the level
                            $rolePermissionUpdate = new RolePermissions();
                            $rolePermissionUpdate->exists = true;
                            $rolePermissionUpdate->id = $assignedRolePermissions[$permission->id]['id'];
                            $rolePermissionUpdate->level = $selectedPermissionLevel;
                            $rolePermissionUpdate->save();
                            //update the permissions level
                        }
                    }else{
                        //assign/create new permission
                        RolePermissions::create([
                            'id_sys_roles' => $role->id,
                            'id_sys_permissions' => $permission->id,
                            'level' => $selectedPermissionLevel ,
                            'created_by'=>Auth::user()->id,
                            'updated_by'=>Auth::user()->id
                        ]);
                    }
                    return back()->withErrors("Permissions updated successfully")->withInput();
                }else{
                    //permission was not selected
                    //check if permission was already assigned
                    if(isset($assignedRolePermissions[$permission->id])){
                        //permission was assigned, we need to delete the permission
                        RolePermissions::where('id', $assignedRolePermissions[$permission->id]['id'] )->delete();
                    }

                }
            }


        }catch(Exception $ex){
            echo $ex->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $role = Roles::find($id);
        $role->delete();
        return back()->with('success', 'Role was trashed successfully');
    }
}
