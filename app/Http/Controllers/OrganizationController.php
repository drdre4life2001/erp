<?php

namespace App\Http\Controllers;

use App\Libraries\Utilities;
use App\Models\Organization;
use App\Models\WorkAddress;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Libraries\ApiServices;
use Cloudder;



class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexErp(Request $request)
    {
        $method = $request->isMethod('post');
        switch ($method) {
            case true:
                //get parent organization
                $parentOrganization = $request->get('parent_id');

                try{
                    $validator = Validator::make($request->all(), [
                        'name' => 'required|max:255',
                        'id_hr_work_addresses' => 'required'
                    ]);

                    if ($validator->fails()) {
                        return back()->with('error', $validator);
                    }

                    $organization = Organization::create([
                        'name' => $request->get('name'),
                        'code'=> $request->get('code'),
                        'description'=> $request->get('description'),
                        'tin'=> $request->get('tin'),
                        'id_hr_work_addresses'=> $request->get('id_hr_work_addresses'),
                        'created_by'=>Auth::user()->id,
                        'updated_by'=>Auth::user()->id,
                        ''
                    ]);


                    //insert into organization tree
                    $sql = "SELECT `parent_id`, ".$organization->id.",
                    (`length` + 1) AS `length`, ".Auth::user()->id.",".Auth::user()->id." FROM `sys_organization_tree`
                    WHERE `child_id` = ". $parentOrganization."
                    UNION SELECT ".$organization->id.",".$organization->id.", 0, ".Auth::user()->id.",".Auth::user()->id." ";

                    $parentSql = "INSERT `sys_organization_tree` (`parent_id`, `child_id`, `length`, `created_by`, `updated_by`) ". $sql;

                    DB::insert(DB::raw($parentSql));

                    return back()->with('success', 'Subsidiary Created');

                }catch (\Exception $ex){
                    return back()->with('error', $ex->getMessage());
                }
                break;

                case false:
                $addresses = WorkAddress::orderBy('created_at', 'DESC')->get();
                $parentOrganization = Organization::orderBy('created_at', 'DESC')->get();
                $organizations = Organization::with('workAddress')->orderBy('created_at','DESC')->get();
                return view('erp.system_settings.company.company_subsidiaries')->with(['organizations'=>$organizations, 'addresses' => $addresses, 'parentOrganizations' => $parentOrganization]);
            default:
                break;
        }
        
    }

    public function organizationAjax(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $company = auth()->user()->company->id;
            $data = DB::SELECT("select * from sys_organizations where company_id = '$company' and `name` like '%$search%'");
        }

        return response()->json($data);
    }

    public function clientAjax(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $company = auth()->user()->company->id;
            $data = DB::SELECT("select * from sys_clients where advance_company_id = '$company' and `name` like '%$search%'");
        }
        
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //get work addresses
        $addresses = WorkAddress::orderBy('created_at', 'DESC')->get();
        $parentOrganization = Organization::orderBy('created_at', 'DESC')->get();
        //get organization as parent organization
        return view('organizations.create')
            ->with(
                ['addresses'=>$addresses,
                    'parentOrganizations'=>$parentOrganization
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //get parent organization
        $parentOrganization = $request->get('parent_id');

        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'id_hr_work_addresses' => 'required'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $organization = Organization::create([
                'name' => $request->get('name'),
                'code'=> $request->get('code'),
                'description'=> $request->get('description'),
                'tin'=> $request->get('tin'),
                'id_hr_work_addresses'=> $request->get('id_hr_work_addresses'),
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id
            ]);


            //insert into organization tree
            $sql = "SELECT `parent_id`, ".$organization->id.",
            (`length` + 1) AS `length`, ".Auth::user()->id.",".Auth::user()->id." FROM `sys_organization_tree`
            WHERE `child_id` = ". $parentOrganization."
            UNION SELECT ".$organization->id.",".$organization->id.", 0, ".Auth::user()->id.",".Auth::user()->id." ";

            $parentSql = "INSERT `sys_organization_tree` (`parent_id`, `child_id`, `length`, `created_by`, `updated_by`) ". $sql;

            DB::insert(DB::raw($parentSql));

            return redirect('organizations');

        }catch (\Exception $ex){
            return back()->withErrors($ex->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($org_uid){
        $response = ApiServices::callApi(null, "/organisations/$org_uid",  null, "DELETE");

        if($response == "204"){
            return back()->with('success', 'Subsidiary Trashed!');
        }else{
            return back()->with('error', 'Something went wrong!');
        }
    }

    public function index(Request $request){
        $method = $request->isMethod('post');
        switch ($method) {
            case true:
                //get parent organization
                $parentOrganization = $request->get('parent_id');

                try{
                    $validator = Validator::make($request->all(), [
                        'name' => 'required',
                        'registration_number' => 'required',
                        'phone_number' => 'required',
                        'email' => 'required',
                        'date_incorporated',
                    ]);

                    if ($validator->fails()) {
                        return back()->with('error', $validator);
                    }

                    //upload certificate picture to cloud
                    $image_url = Utilities::uploadImageToCloudinary($request->file('certificate_image'));
                    $data = [
                        'name' => $request->name,
                        'registration_number' => $request->registration_number,
                        'customer_uid' => auth()->user()->advance_username,
                        'phone' => $request->phone_number,
                        'email' => $request->email,
                        'date_incorporated' => $request->date_incorporated,
                        'business_type' => $request->business_type,
                        'certificate_image' => $image_url
                    ];
                    $response = ApiServices::callApi($data, "/organisations", null, "POST");
                    $created_org = (object) $response['your_response'];
                    if($response['code'] == "200"){ //created
                         $company_id = auth()->user()->company->id;
                         $company_uri = auth()->user()->company->uri;
                         $organization = Organization::create([
                            'name' => $request->name,
                            'registration_number' => $request->registration_number,
                            'business_type' => $request->business_type,
                            'certificate_image' => $image_url,
                            'date_incorporated' => $request->date_incorporated,
                            'email' => $request->email,
                            'uid' => $created_org->uid,
                            'phone' => $request->phone_number,
                            'code'=> $request->registration_number,
                            'company_id' => $company_id,
                            'company_uri' => $company_uri,
                            'created_by' => auth()->user()->id,
                            'updated_by' => auth()->user()->id
                         ]);
                        return back()->with('success', 'Subsidiary Created!');
                    }else{
                        return back()->with('error', $response['message']);
                    }

                }catch (\Exception $ex){
                    return back()->with('error', $ex->getMessage());
                }
                break;

            case false:
                $addresses = WorkAddress::orderBy('created_at', 'DESC')->get();
                $parentOrganization = Organization::orderBy('created_at', 'DESC')->get();
                $response = ApiServices::callApi(null, "/customer/".auth()->user()->advance_username."/organisations?expand=accounts", null, 'GET');
                $organizations = (object) $response['your_response'];

                return view('erp.system_settings.company.company_subsidiaries')->with(['organizations'=>$organizations, 'addresses' => $addresses, 'parentOrganizations' => $parentOrganization]);

            default:
                break;
        }
    }


}
