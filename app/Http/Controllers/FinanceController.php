<?php

namespace App\Http\Controllers;

use Excel;
use App\Models\AccountEffect;
use App\Models\AccountHeader;
use App\Models\AccountSubHeader;
use App\Models\BankAccount;
use App\Models\BankActivity;
use App\Models\Employee;

use App\Models\Ledger;
use App\Models\OperatingExpense;
use App\Models\MenuGroup;
use App\Models\Remittance;
use App\Models\Revenue;
use App\Models\RevenueStream;
use Illuminate\Support\Facades\Session;

use App\Models\Salvage;
use Illuminate\Http\Request;
use App\Models\CapitalExpense;
//use App\Models\BankAccount;
use App\Http\Requests;
use App\Models\User;
use App\Models\CompanyLoan;
use App\Models\CompanyBank;
use App\Models\PayrollSummary;
use App\Models\Organization;
use App\Models\LoanRepayment;
use App\Libraries\Utilities;
use Carbon\Carbon;
use App\Models\Module;

use App\Models\Bank;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Models\OtherExpense;
use Illuminate\Support\Facades\Input;
use File;
class FinanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function download_bulk_template(Request $request){
        //DOwnload bulk uploads template
        $name = "bulk_bank_uploads_template";
        $pathToFile = public_path()."/downloads/bulk_bank_accounts_upload.xlsx";
        return response()->download($pathToFile, $name);
    }

    public function add_bank_account(Request $request){
            //process form
            $validator = Validator::make($request->all(), [
                'account_name' => 'required',
                'account_number' => 'required|numeric'
            ]);
            if($validator->fails()){
                return back()->with('error', 'All fields are required');
            }
            //Check if record already exits for the selected date
            
                $insert = Bankz::create([
                    'account_name' => $request->account_name,
                    'account_number' => $request->account_number,
                    'user_id' => auth()->user()->id,
                    'advance_company_id' => auth()->user()->company->id,
                    'advance_company_uri' => auth()->user()->company->uri
                ]);
                $request->session()->flash('success', 'Bank account added!');
                return back();       
    }

    public function addBankAccount(Request $request){
        $response = array(
            'status' => 'success',
            'acc_number' => $request->account_number,
            'acc_name' => $request->account_name,
        );
        return response()->json($response); 
     }

    public function bulk_upload(Request $request){
        $method = $request->isMethod('post');

        if($method){ //process form
            $validator = Validator::make($request->all(), [
                'excel_file' => 'required'
            ],[
                'excel_file.required' => 'Please upload a file'
            ]);
            if($validator->fails()){
                return back()->with('success', 'Please choose a file to upload!');
            }
            $extension = File::extension($request->excel_file->getClientOriginalName());
            if ($extension == "xls" || $extension == "xlsx") {
                //'Your file is a valid xls or csv file'
                if($validator->fails()){

                }
                //Process form
                if (Input::hasFile('excel_file')) {
                    $path = Input::file('excel_file')->getRealPath();
                    $data = Excel::load($path, function ($reader) {
                    })->get();
                    $s = 0;
                    $f = 0;
                    foreach ($data as $key => $value) {
                        $retrieved_bank_id = DB::SELECT("SELECT id, name FROM sys_banks WHERE name = '$value->bank'");
                        //get all existing Banks
                        if(!empty($retrieved_bank_id)){
                            $retrieved_bank_id[0]->id;

                            $retrieved_bank_id = $retrieved_bank_id[0]->id;;
                        }else{
                            $retrieved_bank_id = null;
                        }

                        $retrieved_subsidiary_id = DB::SELECT("SELECT id, name FROM sys_organizations WHERE `name` = '$value->subsidiary'");
                        //get all existing Subsidiaries/Organizations
                        if(!empty($retrieved_subsidiary_id)){
                            $retrieved_subsidiary_id[0]->id;

                            $retrieved_subsidiary_id = $retrieved_subsidiary_id[0]->id;;
                        }else{
                            $retrieved_subsidiary_id = null;
                        }

                        if(!is_null($retrieved_bank_id) && !is_null($retrieved_subsidiary_id)){
                           $insert_bank_account = [
                                'bank_id' => $retrieved_bank_id,
                                'subsidiary_id' => $retrieved_subsidiary_id,
                                'account_number' => $value->bank_account,
                                'phone_relationship_manager' => $value->relations_phone,
                                'email_relationship_manager' => $value->relations_email,
                                'name_relationship_manager' => $value->relations_name,
                                'prevBalance' => $value->previous_balance,
                                'currentBalance' => $value->current_balance
                           ];

                            if(!empty($insert_bank_account)){
                                $each_save = DB::table('sys_bank_accounts')->insert($insert_bank_account);
                                $s++;
                            }

                        }else{
                            $f++;
                            continue;
                        }
                    }

                    if ($s > 0 && $f > 0) {

                        return back()->with('success',  $s . ' Bank account(s) was uploaded successfully. '.$f.' account(s) failed to upload due to some expected format...Try uploading them again.');

                    }elseif($s > 0 && $f <= 0){
                        return back()->with('success',  $s . ' Bank account(s) was uploaded successfully.');
                    }elseif ($s <= 0 && $f > 0){
                        return back()->with('success', $f.' account(s) failed to upload due to some expected format...Try uploading them again.');
                    }
                }

            }else{
                $ext_error = "File format be must .xls or .xlsx";
                return back()->withErrors($ext_error)->withInput();
            }

        }else{
            return view('finances.bulk_account_upload');
        }
    }
    public function remittance(Request $request){
        $method = $request->isMethod('post');
        $item = Remittance::all();
        if($method){
            //process form
            $validator = Validator::make($request->all(), [
                'date' => 'required|date',
                'amount' => 'required|numeric'
            ]);
            if($validator->fails()){
                return redirect()->withErrors($validator)->withInput();
            }
            //Check if record already exits for the selected date
            $count = DB::table('remittances')->where('remittance_date',$request->date)->count();
            if($count == 1){
                //Already exits
                $request->session()->flash('success', 'Remittance already exist for selected date');
                return back();
            }else{
                $insert = Remittance::create([
                    'remittance_date' => $request->date,
                    'amount' => $request->amount
                ]);
                $request->session()->flash('success', 'Remittance for '. $request->date. ' recorded');
                return back();
            }

        }else{
            //return form
            return view('erp.finance.remittance', compact('item'));
        }
    }
    public function download_remittance(){
        $pathToFile = public_path()."/files/remittance.xls";
        return response()->download($pathToFile);
    }
    public function upload_remittance(Request $request){
       $method = $request->isMethod('post');
       if($method){
           //Process excel sheet
           $path = Input::file('excel_file')->getRealPath();
           $data = Excel::load($path, function ($reader) {
           })->get();
           $i = 0;
           foreach ($data->toArray() as $key => $value) {
               foreach ($value as $v) {
                   $amount = $v['amount'];
                   $date = $v['date'];
                   $remit_date = date('d-m-Y', strtotime($date));
                   $count = DB::table('remittances')->where('remittance_date',$date)->count();
                   if($count == 1){
                       //Already exits
                       $request->session()->flash('success', 'Remittance already exist for '.$remit_date);
                       return back();
                   }
                   else{
                       $insert = Remittance::create([
                           'remittance_date' => $date,
                           'amount' => $amount
                       ]);
                       $request->session()->flash('success', 'Remittance has been recorded');
                       return back();
                   }
               }
           }

       }
    }
    public function update_remittance(Request $request){
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric'
        ]);
        if($validator->fails()){
            return redirect()->withErrors($validator)->withInput();
        }
        $remittance_id = $request->remittance_id;
        $update = DB::table('remittances')->where('id', $remittance_id)->update(
            ['amount' => $request->amount]
        );
        if($update){
            $request->session()->flash('success', 'Remittance updated');
            return back();
        }else{
            return back();
        }
    }
    public function revenue_update(Request $request){
        $stream_id = $request->stream_id;
        $revenue_id = $request->revenue_id;
        $date = $request->tdate;

        //Check revenue stream type
        if($stream_id == 5){ //busticketsas
            $data = Utilities::switch_db('bus')->select("
                SELECT DATE(`created_at`) as tdate, SUM(`final_cost`) AS sales, SUM(`passenger_count`) * 70 AS commission FROM bookings
                WHERE DATE(`created_at`) = '$date' 
                GROUP BY DATE(`created_at`)");
        }elseif ($stream_id == 6){ //Phedc
            $data = Utilities::switch_db('phedc')->select("
                SELECT DATE(`date`) as tdate, collection AS sales, commission AS commission FROM collections
                WHERE DATE(`date`) = '$date' 
                GROUP BY DATE(`date`)");
        }elseif ($stream_id == 7){ //Jedc
            $data = Utilities::switch_db('jedc')->select("
                SELECT DATE(`date`) as tdate, collection AS sales, commission AS commission FROM collections
                WHERE DATE(`date`) = '$date' 
                GROUP BY DATE(`date`)");
        }
        //Loop through data and update into revenue table
        foreach ($data as $data){
            //Check if record for a particular date already exist
            DB::table('revenues')->where('id', $revenue_id)->update(
                [
                    'total_collection' => $data->sales,
                    'total_commission' => $data->commission
                ]
            );
        }
        $request->session()->flash('success', 'Revenue updated');
        return back();
    }
    public function revenue(Request $request){
        $method = $request->isMethod('post');
        //Get list of all revenue streams
        $streams = RevenueStream::where('advance_company_id', auth()->user()->company->id)->get();
        if($method){
            $validator = Validator::make($request->all(), [
                'stream_id' => 'required',
                'from' => 'required|date',
                'to' => 'required|date'
            ]);
            if($validator->fails()){
                return back()->withErrors($validator)->withInput();
            }
            $from = $request->from;
            $to = $request->to;
            $stream_id = $request->stream_id;
            if($stream_id == 0){
                $items = DB::select("select a.id as id, a.revenue_stream_id as stream_id, a.total_collection as total_collection, a.total_commission as total_commission, DATE(a.date) as tdate,
                b.name as company, c.name as client, d.name as revenue_stream from revenues as a, sys_organizations as b,
                sys_clients as c, revenue_streams as d where 
                a.client_id = c.id and a.company_id = b.id and a.revenue_stream_id = d.id AND 
                DATE (a.date) BETWEEN '$from' and '$to' and
                a.deleted_at IS NULL ORDER BY a.id DESC");
            }else{
                $items = DB::select("select a.id as id, a.revenue_stream_id as stream_id, a.total_collection as total_collection, a.total_commission as total_commission, DATE(a.date) as tdate,
                b.name as company, c.name as client, d.name as revenue_stream from revenues as a, sys_organizations as b,
                sys_clients as c, revenue_streams as d where 
                a.client_id = c.id and a.company_id = b.id and a.revenue_stream_id = d.id AND 
                DATE (a.date) BETWEEN '$from' and '$to' and a.revenue_stream_id = '$stream_id' and
                a.deleted_at IS NULL ORDER BY a.id DESC");
            }
            return view('erp.finance.revenue', compact('items', 'streams'));
        }else{
            $items = DB::select("select a.id as id, a.revenue_stream_id as stream_id, a.total_collection as total_collection, a.total_commission as total_commission, DATE(a.date) as tdate,
            b.name as company, c.name as client, d.name as revenue_stream from revenues as a, sys_organizations as b,
            sys_clients as c, revenue_streams as d where 
            a.client_id = c.id and a.company_id = b.id and a.revenue_stream_id = d.id AND 
            a.deleted_at IS NULL ORDER BY a.id DESC");
            return view('erp.finance.revenue', compact('items', 'streams'));
        }
    }
    public function add_revenue(Request $request){
        $streams = RevenueStream::where('advance_company_id', auth()->user()->company->id)->get();
        $method = $request->isMethod('post');
        $auth_company_id = auth()->user()->company->id;
        if($method){
            //process form
            $validator = Validator::make($request->all(), [
                'stream_id' => 'required',
                'from' => 'required|date',
                'to' => 'required|date'
            ]);
            if($validator->fails()){
                return back()->withErrors($validator)->withInput();
            }
            $stream_id = $request->stream_id;
            // dd($stream_id);
            if($stream_id){
                //Fetch client_id and company_id for this stream
                $sql = DB::select("select client_id, company_id from revenue_streams where id = '$stream_id'");
                foreach ($sql as $sql){
                    $client_id = $sql->client_id;
                    $company_id = $sql->company_id;
                }
                //Fetch data for selected date range
                $startDate = $request->from;
                $endDate = $request->to;
                try {
                    $data = DB::SELECT("
                    SELECT DATE(`created_at`) as tdate, SUM(`total_collection`) AS sales, SUM(`total_commission`) AS commission FROM revenues
                    WHERE DATE(`created_at`) BETWEEN '$startDate' AND '$endDate'
                    GROUP BY DATE(`created_at`)");
                } catch (\Exception $e) {

                    return back()->with('error', 'An error occured '.$e->getMessage().' File :'. $e->getFile());
                }
            }else{
                //Do something asshole.
                $data = null;
            }
            if($data == null){
                return back()->withInput()->with('error', 'No revenue found for selected date range');
            }else{
                foreach ($data as $data){
                    //Check if record for a particular date already
                    $sql = DB::table('revenues')->where(['date' => $data->tdate, 'revenue_stream_id' => $stream_id])->get();
                    if(count($sql) >= 1){
                        //return 'Record already exists for this date:'.$data->tdate. 'Update instead';
                        return back()->withInput()->with('error', 'Record already exists for this date: '.$data->tdate.'.'. ' Update instead');
                    }else {
                        Revenue::create([
                            'revenue_stream_id' => $stream_id,
                            'client_id' => $client_id,
                            'company_id' => $company_id,
                            'date' => $data->tdate,
                            'total_collection' => $data->sales,
                            'total_commission' => $data->commission,
                            'advance_company_id' => auth()->user()->company->id,
                            'advance_company_uri' => auth()->user()->company->uri,

                        ]);
                    }
                }
                return back()->with('status', 'Revenue added!');
            }
            //return to revenues page
        }
    }

    public function streamAjax(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $company = auth()->user()->company->id;
            $data = DB::SELECT("select * from revenue_streams where advance_company_id = '$company' and `name` like '%$search%'");
        }
        

        return response()->json($data);
    }


    public function revenue_stream(){
        //Fetch all revenue streams
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $organizations = Organization::where(['company_id' => auth()->user()->company->id, 'company_uri' => auth()->user()->company->uri])->get(); //Fetch all company subsidiaries
        $clients = DB::select("SELECT * FROM sys_clients WHERE active = 1 AND advance_company_id = '$company_id'"); //Fetch all active clients
        $items = DB::select("select SUM(d.total_commission) as revenue, a.id  as id, a.client_id as client_id, a.company_id as company_id, a.name as name, b.name as client, c.name as company from
        revenue_streams as a, sys_clients as b, sys_organizations as c, revenues as d where a.client_id = b.id AND 
        a.company_id = c.id and d.revenue_stream_id = a.id and a.advance_company_id = '$company_id' and a.advance_company_uri = '$company_uri' and a.deleted_at IS NULL group by d.revenue_stream_id");

        return view('erp.finance.revenue_streams', compact('organizations', 'clients', 'items'));
    }
    public function revenue_stream_edit(Request $request){
        //Edit revenue stream
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:256',
            'client_id' => 'required',
            'company_id' => 'required'
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        DB::table('revenue_streams')->where('id', $request->item_id)
                                    ->update([
                                        'name' => $request->name,
                                        'client_id' => $request->client_id,
                                        'company_id' => $request->company_id
                                    ]);
        return back()->with('success', 'Updated!');
    }
    public function revenue_stream_delete($id){
        RevenueStream::find($id)->delete();
        return back()->with('info', 'Revenue Trashed!');
    }
    public function add_revenue_stream(Request $request){
        $method = $request->isMethod('post');
        $organizations = Organization::where('advance_company_id', auth()->user()->company->id)->get(); //Fetch all company subsidiaries
        if($method){
            //Process form
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:256',
                'client_id' => 'required',
                'company_id' => 'required'
            ]);
            if($validator->fails()){
                return back()->withErrors($validator)->withInput();
            }
            $revenue_stream = RevenueStream::create([
                'name' => $request->name,
                'client_id' => $request->client_id,
                'company_id' => $request->company_id,
                'advance_company_id' => auth()->user()->company->id,
                'advance_company_uri' => auth()->user()->company->uri,
            ]);
            //Once added, add a record to the revenue table
            $revenue_stream_id = $revenue_stream->id;
            Revenue::create([
                'revenue_stream_id' => $revenue_stream_id,
                'client_id' => $request->client_id,
                'company_id' =>  $request->company_id,
                'date' => Carbon::now(),
                'total_collection' => 0.00,
                'total_commission' => 0.00
            ]);
            //addition ends
            return back()->with('success','Revenue Stream Added!');
        }else{

            return view('revenue_stream.create', compact('organizations', 'clients'));
        }

    }
    public function quarter($ts) {
        return ceil(date('n', $ts)/3);
    }
    public function approved_procurement_items(){

        $adv_company_id = auth()->user()->company->id;
        $adv_company_uri = auth()->user()->company->uri;

        //Fetch all approved procurement items
        $items  = DB::select("SELECT request_items.*, request_status_line_manager.definition AS line_manager_status, 
                request_status_line_manager.id AS line_manager_status_id, request_status_c_level.definition AS c_level_status, 
                request_status_c_level.id AS c_level_status_id FROM
                request_items INNER JOIN request_status AS request_status_line_manager ON 
                request_items.line_manager_remark = request_status_line_manager.id INNER JOIN request_status AS request_status_c_level
                 ON request_items.c_level_remark = request_status_c_level.id 
                 WHERE request_items.state = 'active' AND advance_company_id = '$adv_company_id' AND advance_company_uri = '$adv_company_uri'");
        return view('erp.finance.procurement_disbursment', compact('items'));
    }
    public function get_bank_details(Request $request){
        $account_sub_header = $request->account_sub_header;
        $bank_details = DB::select("select a.id as id, a.currentBalance as currentBalance, a.account_number as account_number,
        b.name as bank_name from sys_bank_accounts as a, sys_banks as b, account_sub_headers as c  where
        a.bank_id = b.id and c.bank_account_id = a.id and c.id = '$account_sub_header'");

        return response()->json($bank_details);
    }
    public function get_balance(Request $request){
        $account_number = $request->bank_account;
        $balance = DB::select("select currentBalance from sys_bank_accounts where id = '$account_number'");
        return response()->json($balance);
    }

    public function get_sub_headers($header_id){
            $data = [];
            if($header_id){
                $search = $header_id;
                $data = DB::select("select id, name from account_sub_headers where header_id = '$header_id'
                        and deleted_at IS NULL");  
            }
            return response()->json($data);
    }

    public function disburse($id){
        $banks = DB::select("select a.id as id, a.account_number as account_number, b.name as name from sys_bank_accounts as 
        a, sys_banks as b where a.bank_id = b.id and a.deleted_at IS NULL");
        //Fetch the item amount
        $details = DB::select("select * from request_items where id = '$id'");
        $subheaders = AccountSubHeader::all();
        $item_id = $id;
        return view('finances.disburse', compact('banks', 'details', 'subheaders', 'item_id'));
    }
    public function processDisburse(Request $request){ //Process disbursement
        $method = $request->isMethod('post');
        if($method){
            $getCount = DB::table('request_items')
                        ->where(['id' => $request->item_id, 'rfq_status' => 2])
                        ->get();
            $count = count($getCount);
            if($count == 0) {
                $user_id = Auth::user()->id_hr_employee;
                $user = DB::select("select * from hr_employee as a,  hr_grade as b where
                    a.id_hr_grade = b.id and b.id = 4 and a.id = '$user_id'");
                if($user){ //If CLevel staff
                    $validator = Validator::make($request->all(), [
                        'bank_account' => 'required',
                        'balance' => 'required',
                        'account_sub_header' => 'required'
                    ]);
                    if ($validator->fails()) {
                        return back()->withErrors($validator)->withInput();
                    }
                    $amount = $request->amount;
                    $bank_account_id = $request->bank_account;
                    $current_balance = $request->balance;
                    $newBalance = $current_balance - $amount;
                }else{//Finance staff or whoever has access to finance module
                    $validator = Validator::make($request->all(), [
                        'bank_account_id' => 'required',
                        'balance' => 'required',
                        'account_sub_header' => 'required'
                    ]);
                    if ($validator->fails()) {
                        return back()->withErrors($validator)->withInput();
                    }
                    $amount = $request->amount;
                    $bank_account_id = $request->bank_account_id;
                    $current_balance = $request->balance;
                    $newBalance = $current_balance - $amount;
                }
                DB::table('request_items')
                    ->where('id', $request->item_id)
                    ->update(['rfq_status' => 2]); //Update = 2 i.e. funds disbursed
                $update_bank = DB::table('sys_bank_accounts')
                    ->where('id', $bank_account_id)
                    ->update([
                        'prevBalance' => $current_balance,
                        'currentBalance' => $newBalance
                    ]);
                if ($update_bank) { //Create record in bank activities table

                    //Insert into the Bank Activity table
                    Utilities::insBankActivity($bank_account_id, $amount, 0, $current_balance, $newBalance);
                    //Insert into the Ledger table
                    Utilities::insLedger($bank_account_id, $amount, $current_balance, $newBalance, $request->account_sub_header,
                        $request->item_id, 0, $user_id);
                    /**
                    Ledger::create([
                        'bank_id' => $bank_account_id,
                        'amount' => $amount,
                        'prevBalance' => $current_balance,
                        'currentBalance' => $newBalance,
                        'sub_header_id' => $request->account_sub_header,
                        'trans_id' => $request->item_id,
                        'type' => 0,
                        'user_id' => $user_id
                    ]);
                     * **/
                    return redirect()->route('ledger');
                }
            }else{
                return redirect()->route('disburse', [$request->item_id])->with('status', 'Funds already disbursed');
            }
        }
    }
    public function ledger(){
        $items = DB::select("select a.id as id, a.prevBalance as prevBalance, a.currentBalance as currentBalance, a.amount as amount, 
                c.name as bank_name, b.account_number as account_number, d.name as effect from ledgers as a, 
                sys_bank_accounts as b, sys_banks as c, account_effects as d
                where a.bank_id = b.id and b.bank_id = c.id and a.type = d.value order by a.created_at DESC");
            return view('finances.ledger', compact('items'));
    }
    public function banks(Request $request){
        $method = $request->isMethod('post');
        $banks = Bank::where(['user_id' => auth()->user()->id,
        'advance_company_id' => auth()->user()->company->id,
         'advance_company_uri' =>auth()->user()->company->uri])->orderBy('created_at', 'desc')->get();
        if ($method) {
            //Process form
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:256'
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            
            $query = new Bank();
            $query->name = $request->name;
            $query->user_id = auth()->user()->id;
            $query->advance_company_id = auth()->user()->company->id;
            $query->advance_company_uri = auth()->user()->company->uri;
            $query->created_at = Carbon::now();
            $query->updated_at = Carbon::now();
            $query->save();

            if ($query) {
                $request->session()->flash('success', 'Bank added');
                return back();
            }

        } else {
            return view('erp.finance.banks', compact('banks'));
        }
    }
    
    public function banks_edit(Request $request){

        if(empty($request->name))
            return back()->with('error', 'Please insert bank name');
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:256'
        ]);
        
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        
        DB::table('sys_banks')->where('id', $request->bank_id)->update(['name' => $request->name]);
        
        $request->session()->flash('success', 'Bank details updated!');
    
        return redirect()->route('banks');

    }

    public function banks_delete(Request $request, $id)
    {
        Bank::find($id)->delete();
        return back()->with('info', 'Bank trashed!');
    }
    public function ledger_delete(Request $request, $id)
    {
        Ledger::find($id)->delete();
        return back()->with('info', 'Ledger trashed!');
    }
    public function bank_accounts(Request $request){
        $method = $request->isMethod('post');

        $company_id = auth()->user()->company->id;
        
        $company_uri = auth()->user()->company->uri;

        $items = DB::select("select c.id as subsidiary_id, c.name as subsidiary, name_relationship_manager, phone_relationship_manager,email_relationship_manager,  a.prevBalance as prevBalance, 
        a.currentBalance as currentBalance, a.account_number  as account, b.name as bank, 
        a.id as id from sys_bank_accounts as a, sys_banks as b, sys_organizations as c where
        a.bank_id = b.id and a.advance_company_id = '$company_id' and a.advance_company_uri = '$company_uri' and a.subsidiary_id = c.id and a.deleted_at IS NULL order by a.created_at DESC");
        $banks = Bank::all();
        $organizations = Organization::all();
        if ($method) {
            //Process form
            $validator = Validator::make($request->all(), [
                'account_number' => 'required|digits:10|unique:sys_bank_accounts',
                'bank_id' => 'required',
                'account_balance' => 'required|numeric',
                'subsidiary_id' => 'required',
                'name_relationship_manager' => 'required',
                'phone_relationship_manager' => 'required',
                'account_name' => 'required',
                'email_relationship_manager' => 'required'
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $query = BankAccount::create([
                'account_number' => $request->account_number,
                'account_name' => $request->account_name,
                'bank_id' => $request->bank_id,
                'prevBalance' => $request->account_balance, //set prevBalance to current balance
                'currentBalance' => $request->account_balance,
                'subsidiary_id' => $request->subsidiary_id,
                'name_relationship_manager' => $request->name_relationship_manager,
                'phone_relationship_manager' => $request->phone_relationship_manager,
                'email_relationship_manager' => $request->email_relationship_manager,
                'advance_company_id' => $company_id,
                'advance_company_uri' => $company_uri
            ]);
            if ($query) {
                $request->session()->flash('success', 'Bank account details added for the selected organization');
                return back();
            }
        } else {
            return view('erp.finance.bank_accounts', compact('items', 'banks', 'organizations'));
        }
    }
    public function bank_accounts_edit(Request $request){
        $validator = Validator::make($request->all(), [
            'account' => 'required|digits:10|',
            'subsidiary_id' => 'required',
            'name_relationship_manager' => 'required',
            'email_relationship_manager' => 'required',
            'phone_relationship_manager' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        DB::table('sys_bank_accounts')->where('id', $request->item_id)
            ->update([
                'account_number' => $request->account,
                'subsidiary_id' => $request->subsidiary_id,
                'name_relationship_manager' => $request->name_relationship_manager,
                'email_relationship_manager' => $request->email_relationship_manager,
                'phone_relationship_manager' => $request->phone_relationship_manager
                ]);
        $request->session()->flash('success', 'Bank details updated!');
        return redirect()->route('bank_accounts');
    }
    public function bank_accounts_delete(Request $request)
    {
        BankAccount::find($request->item_id)->delete();
        return redirect()->route('bank_accounts');
    }
    
    public function bank_statement($id){
        $item = DB::select("SELECT  a.amount as amount, a.prevBalance as prevBalance, a.currentBalance as currentBalance, a.created_at as created_at
                                       FROM bank_activities as a WHERE
                                        a.bank_id = '$id'  order by created_at desc");
        $accounts = DB::select("select account_number from sys_bank_accounts where bank_id = '$id'");
        return view('finances.bank_statement', compact('item', 'accounts'));
    }

    public function account_header(Request $request){
        $company_id = auth()->user()->company->id;
        
        $company_uri = auth()->user()->company->uri;

        $method = $request->isMethod('post');
        $item = DB::select("select a.name as header, a.id as id, b.name as effect from account_headers as a, account_effects as b 
                where a.effect = b.id and a.advance_company_uri = '$company_uri' and a.advance_company_id = '$company_id' and a.deleted_at IS NULL order by a.created_at DESC");
        
        $effects = AccountEffect::where('advance_company_id', $company_id)->get();

        if ($method) {
            //Process form
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:256',
                'effect' => 'required'
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $query = AccountHeader::create([
                'name' => $request->name,
                'effect' => $request->effect,
                'advance_company_id' => $company_id,
                'advance_company_uri' => $company_uri
            ]);
            if ($query) {
                $request->session()->flash('success', 'Account header added!');
                return back();
            }
        } else {
            return view('erp.finance.account_headers', compact('item', 'effects'));
        }
    }
    public function account_header_edit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:256',
            'effect' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        DB::table('account_headers')->where('id', $request->item_id)
            ->update(['name' => $request->name, 'effect' => $request->effect]);
        $request->session()->flash('info', 'Header updated successfully!');
        return redirect()->route('account_header');
    }
    public function account_header_delete(Request $request, $id)
    {
        DB::table('account_headers')->where('id', $id)
            ->update(['deleted_at' => Carbon::now()]);
        //Soft delete sub-headers too
        DB::table('account_sub_headers')->where('header_id', $id)
            ->update(['deleted_at' => Carbon::now()]);
        return redirect()->route('account_header')->with('success', 'Account Header Trashed!');
    }
    public function account_sub_header(Request $request){
        $method = $request->isMethod('post');
        
        $company_id = auth()->user()->company->id;
        
        $company_uri = auth()->user()->company->uri;

        $item = DB::select("
        select a.parent_id as parent_id, a.header_id as header_id, a.bank_account_id as bank_id, a.id as id, a.name as sub_header, b.name as header, c.name as bank_name, d.account_number as account_number, 
        d.id as bank_account_id from account_headers b, account_sub_headers as a, sys_banks as c, sys_bank_accounts as d 
        WHERE
        c.id = d.bank_id and 
        a.bank_account_id = d.id and 
        a.header_id = b.id and a.deleted_at IS NULL order by a.created_at DESC
         ");
        $headers = AccountHeader::where('advance_company_id', $company_id)->get();

        $subheaders = AccountSubHeader::where('advance_company_id', $company_uri)->get();

        $banks = DB::select("select a.name as bank_name, b.id as id, b.account_number as account_number from sys_banks as a, sys_bank_accounts as b where a.id = b.bank_id and b.advance_company_id = '$company_id' and b.advance_company_uri = '$company_uri'");
        if ($method) {
            //Process form
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:256',
                'account_header' => 'required',
                'bank_account_id' => 'required'
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $query = AccountSubHeader::create([
                'name' => $request->name,
                'header_id' => $request->account_header,
                'bank_account_id' => $request->bank_account_id,
                'parent_id' => isset($request->parent_id) ? $request->parent_id : null
            ]);
            $sub_id = $query->id;
            if($request->account_sub_header == 0){ //If not applicable, set parent id to the sub-header's id
                $parent_id = $sub_id;
            }else{
                $parent_id = $request->account_sub_header;
            }
            //Update parent_id
            $update = DB::table('account_sub_headers')->where('id', $sub_id)->update(['parent_id' => $parent_id]);
            if ($update) {
                $request->session()->flash('success', 'Account sub-header added!');
                return back();
            }
        } else {
            return view('erp.finance.account_sub_headers', compact('item', 'headers', 'banks','subheaders'));
        }
    }
    public function account_sub_header_edit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:256',
            'account_header' => 'required',
            'bank_account_id' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        if($request->account_sub_header == 0){ //If not applicable, set parent id to the sub-header's id
            $parent_id = $request->item_id;
        }else{
            $parent_id = $request->account_sub_header;
        }
        DB::table('account_sub_headers')->where('id', $request->item_id)
            ->update(['name' => $request->name, 'header_id' => $request->account_header,
                'bank_account_id' => $request->bank_account_id, 'parent_id' => $parent_id
                ]);
        $request->session()->flash('success', 'Sub-header updated!');
        return redirect()->route('account_sub_header');
    }
    public function account_sub_header_delete($id)
    {
        // dd($id);
        AccountSubHeader::find($id)->delete();
        return back()->with('info', 'Subheader Trashed!');
    }
    public function operate(Request $request)
    {
        $method = $request->isMethod('post');
        $item = OperatingExpense::where('advance_company_id', auth()->user()->company->id)->orderBy('created_at', 'desc')->get();
        if ($method) {
            //Process form
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:256'
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $query = OperatingExpense::create([
                'name' => $request->name,
                'category_id' => 2, //id for operating expenses (fin_request_categories)
                'advance_company_id' => auth()->user()->company->id,
                'advance_company_uri' => auth()->user()->company->uri
                ]);
            if ($query) {
                $request->session()->flash('success', 'Expense item added');
                return back();
            }
        } else {
            return view('erp.finance.operating_expenses', compact('item'));
        }

    }



    public function operate_edit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:256|unique:operating_expenses'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        DB::table('operating_expenses')->where('id', $request->item_id)
            ->update(['name' => $request->name]);
        $request->session()->flash('success', 'item updated!');
        return redirect()->route('operating_expenses');
    }

    public function operate_delete(Request $request, $id)
    {
        OperatingExpense::find($id)->delete();
        return back()->with('info', 'Expense trashed!');
    }

    public function capital(Request $request)
    {
        $method = $request->isMethod('post');
        $item = CapitalExpense::where('advance_company_id', auth()->user()->company->id)->orderBy('created_at', 'desc')->get();
        if ($method) {
            //Process form
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:256'
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $query = CapitalExpense::create([
                'name' => $request->name,
                'category_id' => 1, //id for capital expenses (fin_request_categories),
                'advance_company_id' => auth()->user()->company->id,
                'advance_company_uri' => auth()->user()->company->uri,
            ]);
            if ($query) {
                $request->session()->flash('success', 'item added');
                return back();
            }
        } else {
            return view('erp.finance.capital_expenses', compact('item'));
        }

    }

    public function capital_edit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:256|unique:capital_expenses'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        DB::table('capital_expenses')->where('id', $request->item_id)
            ->update(['name' => $request->name]);
        $request->session()->flash('success', 'item updated!');
        return redirect()->route('capital_expenses');
    }

    public function capital_delete(Request $request, $id)
    {
        CapitalExpense::find($id)->delete();
        return back()->with('info', 'Expense trashed!');
    }

    public function salvage(Request $request)
    {
        $method = $request->isMethod('post');
        $item = Salvage::where('advance_company_id', auth()->user()->company->id)->orderBy('created_at', 'desc')->get();
        if ($method) {
            //Process form
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:256',
                'period' => 'required|integer',
                'value' => 'required|numeric'
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $query = Salvage::create([
                'name' => $request->name,
                'period' => $request->period,
                'value' => $request->value,
                'advance_company_id' => auth()->user()->company->id,
                'advance_company_uri' =>Auth::user()->company->uri
            ]);
            if ($query) {
                $request->session()->flash('success', 'Salvage added');
                return back();
            }
        } else {
            return view('erp.finance.salvage_items', compact('item'));
        }

    }

    public function salvage_edit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:256',
            'period' => 'required|integer',
            'value' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        DB::table('salvages')->where('id', $request->item_id)
            ->update([
                'name' => $request->name,
                'period' => $request->period,
                'value' => $request->value
            ]);
        $request->session()->flash('success', 'Salvage item updated!');
        return redirect()->route('salvage_items');
    }

    public function salvage_delete($id)
    {
        Salvage::find($id)->delete();
        return back()->with('info', 'Salvage Item Trashed!');
    }

    public function index()
    {
        //
        return view('finances.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createCompanyLoan(Request $request)
    {
        $adv_company_id = auth()->user()->company->id;
        $adv_company_uri = auth()->user()->company->uri;

        $method = $request->isMethod('post');
        $banks = Bank::all();
        $company_loans = \DB::select("SELECT id, company_id, amount, interest_rate, bank, description, period, 
                                        (SELECT name FROM sys_organizations WHERE id = company_id ) as company_name,
                                         created_at  FROM company_loans WHERE advance_company_id = '$adv_company_id'
                                            AND advance_company_uri = '$adv_company_uri'");
       switch ($method) {
            case true:
                try {
                    $validator = Validator::make($request->all(), [
                        'company_id' => 'required',
                        'amount' => 'required',
                        'interest_rate' => 'required',
                        'bank' => 'required',
                        'description' => 'required',
                        'period' => 'required'
                    ]);
                    if ($validator->fails()) {

                        return redirect()->back()->withErrors($validator)->withInput()->with(['error' => 'error']);
                    }
                    $amount = $request->input('amount');
                    $b = str_replace(',', '', $amount);
                    $amount = str_replace('₦', '', $b);
                    $amount = str_replace('n', '', $b);
                    $interest_rate = $request->input('interest_rate') / 100;
                    $loan = new CompanyLoan();
                    $loan->company_id = $request->input('company_id');
                    $loan->amount = $amount;
                    $loan->interest_rate = $interest_rate;
                    $loan->advance_company_id = $adv_company_id;
                    $loan->advance_company_uri = $adv_company_uri;
                    //set the initial balance of this loan to be the actual amount, so it get updated whenever they make loan repayment for this loan.
                    $loan->balance = $amount;
                    $loan->bank = $request->input('bank');
                    $loan->description = $request->input('description');
                    $loan->period = $request->input('period');
                    $loan->user_id = auth()->user()->id;
                    //save details collected after successful validation
                    if($loan->save()){
                        return back()->with('success','New loan added');
                    }
                } catch (\Exception $ex) {
                    return back()->with('error', $ex->getMessage())->withInput();
                }
                break;

            case false:
                return view('erp.finance.all_loans', compact('companies', 'company_loans', 'banks'));
                break;
        }
    }

    public function viewPaymentSchedule($loan_id)
    {
        $loan = CompanyLoan::where(['id' => $loan_id,
                             'advance_company_uri' => auth()->user()->company->uri,
                             'advance_company_id' => auth()->user()->company->id
                ])->first();
        $company_name = \DB::select('SELECT name FROM sys_organizations WHERE id = ' . $loan->company_id);
        $company_name = isset($company_name[0]->name) ? $company_name[0]->name : null;
        $bank = $loan->bank;
        $added_interest_value = $loan->interest_rate + $loan->amount;
        $monthly_payment = $added_interest_value / $loan->period;
        return view('erp.finance.payment_schedule', compact('loan', 'bank', 'added_interest_value', 'monthly_payment', 'company_name'));
    }

    public function addLoanRepayment(Request $request, $loan_id)
    {
        $loan = CompanyLoan::where(['id' => $loan_id,
                         'advance_company_id' => auth()->user()->company->id,
                         'advance_company_uri' => auth()->user()->company->uri
                         ])->first();
        $company_name = \DB::select('SELECT name FROM sys_organizations WHERE id = '.$loan->company_id);        
        $company_name = isset($company_name[0]->name) ? $company_name[0]->name : null;
        $total_resettlement = \DB::select('SELECT SUM(amount_paid) as total_resettlement FROM loan_repayments WHERE loan_id = '.$loan_id);
        $total_resettlement[0]->total_resettlement;
        $total_resettlement = $total_resettlement[0]->total_resettlement;
        if (empty($total_resettlement)) {
            $total_resettlement = 0;
        }
        $method = $request->isMethod('post');
        switch ($method) {
            case true:
                try{
                    $validator = Validator::make($request->all(), [
                        'amount_paid' => 'required',
                        'note'  => 'required'
                    ]);
                    if($validator->fails()){
                        return back()->withInput();
                    }
                    $amount = $request->input('amount_paid');
                    $b = str_replace( ',', '', $amount );
                    $amount = str_replace('₦', '', $b);
                    $amount_paid = str_replace('n', '', $b);
                    if ($amount_paid > $loan->balance) {
                        return back()->with('info', 'Sorry, Repayment Amount cant be greater than Loan Amount...Try again')->withInput();
                    }
                    $loan_repayment = new LoanRepayment();
                    $loan_repayment->loan_id = $loan->id;
                    $loan_repayment->loan_amount = $loan->amount;
                    $loan_repayment->total_resettlement = $total_resettlement;
                    $loan_repayment->amount_paid = $amount_paid;
                    $loan_repayment->balance = $loan->balance - $amount_paid;
                    $loan_repayment->note = $request->input('note');
                    $loan_repayment->date_paid = Carbon::now();
                    $loan_repayment->loan_id = $loan->id;
                    $loan_repayment->save();

                    $balance = \DB::table('company_loans')
                                    ->where('id', $loan->id)
                                    ->update(['balance' => $loan_repayment->balance]);
                    return back()->with('success', 'Repayment Loan created for '.$company_name);
                }catch (\Exception $ex){
                    return back()->with('error', $ex->getMessage())->withInput();
                }

                break;
            
            case false:
                $repayment_histories = \DB::select('SELECT * FROM loan_repayments WHERE loan_id = '.$loan_id);
                return view('erp.finance.add_loan_repayment', compact('loan', 'company_name', 'repayment_histories', 'total_resettlement'));
            break;
        }
    }


    public function bankBalance($bank_id){
        $data = null;
        if($bank_id){
            $data = BankAccount::where('bank_id', $bank_id)->get();
        }
        return response()->json($data);
    }

    public function accountSubHeader(Request $request, $account_header_id){
        $method = $request->isMethod('post');
        switch ($method){

            case true :
                $validator = Validator::make($request->all(), [
                    'amount' => 'required',
                    'bank_id'  => 'required',
                    'account_header_id'  => 'required',
                    'subheader_id'  => 'required'
                ]);

                if($validator->fails()){
                    return back()->withErrors($validator)->withInput()->with(['message'=>'error']);;
                }
                $bank = BankAccount::where('bank_id', $request->bank_id)->sum('currentBalance');
                $bank_prev_bal_from_bank_accounts_table = $bank;
                $current_balance = $bank_prev_bal_from_bank_accounts_table - $request->amount;
                if($bank_prev_bal_from_bank_accounts_table < $request->amount ){
                    return back()->withErrors('Insuficient fund for the selected bank account, please select other bank')->withInput();
                }
                $ledger = new Ledger();
                $ledger->bank_id = $request->bank_id;
                $ledger->amount = $request->amount;
                $ledger->prevBalance = $bank_prev_bal_from_bank_accounts_table;
                $ledger->currentBalance = $current_balance;
                $ledger->header_id = $request->account_header_id;
                $ledger->type = 0;
                $ledger->sub_header_id = $request->subheader_id;
                $ledger->user_id = auth()->user()->id;
                if($ledger->save()){
                    $update_bank = DB::table('sys_bank_accounts')
                        ->where('bank_id', $request->bank_id)
                        ->update([
                            'prevBalance' => $bank_prev_bal_from_bank_accounts_table,
                            'currentBalance' => $current_balance,
                            'user_id' => auth()->user()->id
                        ]);
                    //Create and keep track of record in bank activities table
                    $activity = BankActivity::create([
                                    'bank_id' => $request->bank_id,
                                    'amount' => $request->amount,
                                    'type' => 0, //Gotten from account_effects table 0- debit, 1-credit
                                    'prevBalance' => $bank_prev_bal_from_bank_accounts_table,
                                    'currentBalance' => $current_balance,
                                    'user_id' => auth()->user()->id
                                ]);
                    $payroll_summary = PayrollSummary::where('payroll_reference', $request->input('payroll_reference'))
                                                        ->update([
                                                                'status' => 'paid',
                                                            'salary_disbursed_by' => auth()->user()->id
                                                        ]);
                    return back()->withSuccess('Salary Paid Successfully!');

                }
            case false:
                $data = [];
                if($account_header_id){
                    $data = AccountSubHeader::where('header_id', $account_header_id)->get();
                }
                return response()->json($data);
            break;
        }
    }

    public function goBack()
    {
        return redirect()->back();
    }

    public function disbursementSalary(Request $request)
    {
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $payroll_summaries = DB::select("SELECT * FROM payroll_summaries WHERE advance_company_id = '$company_id' AND advance_company_uri = '$company_uri'");
        $banks = DB::select('SELECT id, name FROM sys_banks');
        $account_headers = DB::select("SELECT * FROM account_headers WHERE advance_company_id = '$company_id'");
        
        // $payroll_summaries = \App\Models\EmployeePayroll::
        //                         where(['advance_company_uri' => $company_id,
        //                                 'advance_company_id' => $company_uri
        //                         ])
        //                         ->with('employee')->get();
        // dd($payroll_summaries);
        // return response()->json(['data' => $payroll_summaries]);
        
        return view('erp.finance.salaries', compact('payroll_summaries', 'banks', 'account_headers'));
    }

    public function disburseOtherExpense(Request $request)
    {
        $method = $request->isMethod('post');

        switch ($method) {
            case true:
                $validator = Validator::make($request->all(), [
                    'amount' => 'required',
                    'bank_id'  => 'required',
                    'account_header_id'  => 'required',
                    'subheader_id'  => 'required'
                ]);
                if($validator->fails()){
                    return back()->withErrors($validator)->withInput()->with(['message'=>'error']);;
                }
                $bank = BankAccount::where('bank_id', $request->bank_id)->sum('currentBalance');
                $bank_prev_bal_from_bank_accounts_table = $bank;
                $current_balance = $bank_prev_bal_from_bank_accounts_table - $request->amount;
                if($bank_prev_bal_from_bank_accounts_table < $request->amount ){
                    return back()->withErrors('Insuficient fund for the selected bank account, please select other bank')->withInput();
                }
                $ledger = new Ledger();
                $ledger->bank_id = $request->bank_id;
                $ledger->amount = $request->amount;
                $ledger->prevBalance = $bank_prev_bal_from_bank_accounts_table;
                $ledger->currentBalance = $current_balance;
                $ledger->header_id = $request->account_header_id;
                $ledger->type = 0;
                $ledger->sub_header_id = $request->subheader_id;
                $ledger->user_id = auth()->user()->id;
                if($ledger->save()){
                    $update_bank = DB::table('sys_bank_accounts')
                        ->where('bank_id', $request->bank_id)
                        ->update([
                            'prevBalance' => $bank_prev_bal_from_bank_accounts_table,
                            'currentBalance' => $current_balance,
                            'user_id' => auth()->user()->id
                        ]);
                    $activity = BankActivity::create([
                                    'bank_id' => $request->bank_id,
                                    'amount' => $request->amount,
                                    'type' => 0, //Gotten from account_effects table 0- debit, 1-credit
                                    'prevBalance' => $bank_prev_bal_from_bank_accounts_table,
                                    'currentBalance' => $current_balance,
                                    'user_id' => auth()->user()->id
                                ]);
                    $other_expense = OtherExpense::where('id', $request->input('expense_id'))
                                                        ->update([
                                                                'disbursed' => 'true',
                                                            'expense_disbursed_by' => auth()->user()->id
                                                        ]);
                    return back()->withSuccess('Expense Disbursed Successfully!');

                }

                break;
            
            case false:
                $other_expenses = \DB::select('SELECT id, title, amount, disbursed, description, status, created_by, requested_by, approved_by,    created_at, updated_at, account_header_id,
                                              (SELECT name FROM account_headers WHERE id = account_header_id ) as category, 
                                              (SELECT first_name FROM hr_employee WHERE id = approved_by) as approved_by,
                                              (SELECT email FROM sys_user WHERE id = requested_by) as owner
                                                FROM other_expenses WHERE status = "approved"');
                $banks = DB::select('SELECT id, name FROM sys_banks');
                $account_headers = DB::select('SELECT * FROM account_headers');


                return view('finances.other_expenses', compact('other_expenses', 'banks', 'account_headers'));
                break;
        }


       
    }


    public function payTax(Request $request){
        $method = $request->isMethod('post');
        switch ($method){
            case true :
                $validator = Validator::make($request->all(), [
                    'tax' => 'required',
                    'bank_id'  => 'required',
                    'account_header_id'  => 'required',
                    'subheader_id'  => 'required'
                ]);

                if($validator->fails()){
                    return back()->withErrors($validator)->withInput()->with(['message'=>'error']);
                }
                $bank = BankAccount::where('bank_id', $request->bank_id)->sum('currentBalance');
                $bank_prev_bal_from_bank_accounts_table = $bank;
                $current_balance = $bank_prev_bal_from_bank_accounts_table - $request->tax;

                if($bank_prev_bal_from_bank_accounts_table < $request->tax ){
                    return back()->withErrors('Insuficient fund for the selected bank account, please select another bank')->withInput();
                }
                $ledger = new Ledger();
                $ledger->bank_id = $request->bank_id;
                $ledger->amount = $request->tax;
                $ledger->prevBalance = $bank_prev_bal_from_bank_accounts_table;
                $ledger->currentBalance = $current_balance;
                $ledger->header_id = $request->account_header_id;
                $ledger->type = 0;
                $ledger->sub_header_id = $request->subheader_id;
                $ledger->user_id = auth()->user()->id;
                if($ledger->save()){
                    $update_bank = DB::table('sys_bank_accounts')
                        ->where('bank_id', $request->bank_id)
                        ->update([
                            'prevBalance' => $bank_prev_bal_from_bank_accounts_table,
                            'currentBalance' => $current_balance,
                            'user_id' => auth()->user()->id
                        ]);
                    //Create and keep track of record in bank activities table
                    $activity = BankActivity::create([
                        'bank_id' => $request->bank_id,
                        'amount' => $request->tax,
                        'type' => 0, //Gotten from account_effects table 0- debit, 1-credit
                        'prevBalance' => $bank_prev_bal_from_bank_accounts_table,
                        'currentBalance' => $current_balance,
                        'user_id' => auth()->user()->id
                    ]);
                    $payroll_summary = PayrollSummary::where('payroll_reference', $request->input('payroll_reference'))
                        ->update([
                            'tax_status' => 'paid',
                            'tax_disbursed_by' => auth()->user()->id,
                        ]);

                    return back()->withSuccess('Tax Paid Successfully!');

                }
            case false:

                break;
        }
    }


    public function dashboard(){

        try{
            //get role and permissions
            $user = User::with('assignedRole.rolePermissions.permission')->where('id', Auth::user()->id)->first();
            $userPermissions = array();
            $userRole = isset($user->assignedRole->slug)? $user->assignedRole->slug : null;
            if(isset($user->assignedRole)){
                foreach($user->assignedRole->rolePermissions as $rolePermission){
                    //$userPermissions[$rolePermission->permission->id]['slug'] = $rolePermission->permission->slug;
                    $userPermissions[$rolePermission->permission->slug] = $rolePermission->level;
                }
            }
            //get accessible menus
            $menuGroups = MenuGroup::with('menus.permission')->get();
            $permittedMenuGroups = array();
            foreach($menuGroups as $menuGroup){
                $permittedMenuGroups[$menuGroup->id]['name'] = $menuGroup->name;
                $permittedMenus = array();
                foreach($menuGroup->menus as $menu){
                    if(isset($userPermissions[$menu->permission->slug])){
                        $permittedMenus[] = ['name'=>$menu->name, 'url'=>$menu->url];
                    }
                }
                $permittedMenuGroups[$menuGroup->id]['menus'] = $permittedMenus;
            }
            //get employee details: department, organization, name
            $userDepartments = array();
            $userOrganization = array();
            $userDetails = null;
            $employee = Employee::find(Auth::user()->id_hr_employee);

            if(!empty($employee)){
                $userDetails = $employee;
                $employee->load(['employeeDepartments' => function($query){
                    $query->where('end_date', null);
                    $query->with('department.organization');
                }]);

                foreach($employee->employeeDepartments as $employeeDepartment){
                    //get the department user belongs to
                    $userDepartments[$employeeDepartment->id] = isset($employeeDepartment->department->name)? $employeeDepartment->department->name : null;
                    //get corresponding organization user belongs to
                    if(isset($employeeDepartment->department->id_sys_organizations)){
                        $userOrganization[$employeeDepartment->department->id_sys_organizations] = isset($employeeDepartment->department->organization->name) ? $employeeDepartment->department->organization->name : null;
                    }
                }
            }

            //set ability and role
            Session::put('abilities', $userPermissions);
            Session::put('role', $userRole);
            Session::put('menuGroups',$permittedMenuGroups);//set accessible menus
            Session::put('department', $userDepartments);//sets the department(s) the user belongs to
            Session::put('organization', $userOrganization);//set the organization(s) the user belongs to
            Session::put('userInfo', $userDetails); //sets user employee details
            $qtr = self::quarter(strtotime(date('Y-m-d H:i:s')));
            $utilities = new Utilities();
            $quarter = $utilities->createFullWordOrdinal($qtr);
            $expense = OtherExpense::where('deleted_at', NULL)->paginate(5);
            //Get menu links for this user
            $id_hr_employee= Auth::user()->id_hr_employee;
            $modules = Module::orderBy('created_at')->get();
            //return $id_hr_employee;
            $utilities = new Utilities();
            $total_payables =  $utilities->totalPayables();
            $total_expenses =  $utilities->totalExpenses();
            auth()->user()->role != "7" ? $other_expenses = Utilities::myExpenses() : $other_expenses = Utilities::allExpenses();
            return view('dashboard.finance_dashboard')->with(['modules'=>$modules, 'quarter' => $quarter, 'expense' => $expense, 'total_payables' => $total_payables, 'total_expenses' => $total_expenses, 'other_expenses' => $other_expenses]);
        }catch(\Exception $ex){
            echo $ex->getMessage();
        }
    }

}
