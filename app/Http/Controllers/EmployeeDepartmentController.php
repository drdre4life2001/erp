<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\EmployeeDepartment;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EmployeeDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $validator = Validator::make($request->all(), [
                'id_sys_departments' => 'required|max:255',
                'employee_id' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $time = new Carbon();
            $time = $time->timestamp;

            $employee = Employee::find($request->get('employee_id'));
            if(empty($employee)){
                return "Employee Resource not found";
            }

            EmployeeDepartment::create([
                'start_date'=>$time ,
                'note'=> $request->get('department_note'),
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id,
                'id_hr_employee'=>$request->get('employee_id'),
                'id_sys_departments'=>$request->get('id_sys_departments')
            ]);

            return back()->withErrors('Job changed succesfully');

        }catch (\Exception $ex){
            return back()->withErrors($ex->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
