<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lead;

use DB;

class LeadsController extends Controller
{
    //
    public function __construct()
    {

    }


    public function index(Request $request)
	{
	    return view('leads.index', []);
	}

	public function create(Request $request)
	{
	    return view('leads.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$lead = Lead::findOrFail($id);
	    return view('leads.add', [
	        'model' => $lead	    ]);
	}

	public function show(Request $request, $id)
	{
		$lead = Lead::findOrFail($id);
	    return view('leads.show', [
	        'model' => $lead	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM leads a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE status LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
		$lead = null;
		if($request->id > 0) { $lead = Lead::findOrFail($request->id); }
		else { 
			$lead = new Lead;
		}
	    

	    		
			    $lead->id = $request->id?:0;
				
	    		
					    $lead->status = $request->status;
		
	    		
					    $lead->person_name = $request->person_name;
		
	    		
					    $lead->gender = $request->gender;
		
	    		
					    $lead->organization_name = $request->organization_name;
		
	    		
					    $lead->source = $request->source;
		
	    		
					    $lead->email_address = $request->email_address;
		
	    		
					    $lead->lead_owner = $request->lead_owner;
		
	    		
					    $lead->next_contact_date = $request->next_contact_date;
		
	    		
					    $lead->next_contact_by = $request->next_contact_by;
		
	    		
					    $lead->phone = $request->phone;
		
	    		
					    $lead->salutation = $request->salutation;
		
	    		
					    $lead->mobile_no = $request->mobile_no;
		
	    		
					    $lead->fax = $request->fax;
		
	    		
					    $lead->website = $request->website;
		
	    		
					    $lead->territory = $request->territory;
		
	    		
					    $lead->lead_type = $request->lead_type;
		
	    		
					    $lead->market_segment = $request->market_segment;
		
	    		
					    $lead->industry = $request->industry;
		
	    		
					    $lead->request_type = $request->request_type;
	    $lead->save();

	    return redirect('/leads');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$lead = Lead::findOrFail($id);

		$lead->delete();
		return "OK";
	    
	}

	
}