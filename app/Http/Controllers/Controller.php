<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public $company_uri;

    public $company_id;

    public function __construct()
    {
        $this->company_uri = self::companyUri();
        $this->company_id = self::companyId();
    }

    public function companyUri(){
        return auth()->user()->company->uri;
    }

    public function companyId(){
        return auth()->user()->company->id;
    }
}
