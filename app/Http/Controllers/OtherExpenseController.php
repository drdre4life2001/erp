<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\OtherExpense;
use App\Models\AccountHeader;
use DB;
use Illuminate\Support\Facades\Validator;
use App\Libraries\Utilities;


class OtherExpenseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $method = $request->isMethod('post');
        $categories = AccountHeader::all();
        $other_expenses = \DB::select('SELECT id, title, amount, description, status, created_by, approved_by, created_at, updated_at, account_header_id,
                                      (SELECT name FROM account_headers WHERE id = account_header_id ) as category, 
                                      (SELECT first_name FROM hr_employee WHERE id = approved_by) as approved_by  FROM other_expenses WHERE created_by = '.auth()->user()->id);
        switch($method){
            case true:
                try{
                    $validator = Validator::make($request->all(), [
                        'title' => 'required|max:255',
                        'description' => 'required',
                        'account_header_id' => 'required',
                        'amount' => 'required|numeric'

                    ]);

                    if ($validator->fails()) {
                        return back()->withErrors($validator)->withInput();
                    }
                    $a = $request->input('amount');
                    $b = str_replace( ',', '', $a );
                    $cleaned_amount = str_replace('₦', '', $b);

                    $expense = new OtherExpense();
                    $expense->title = $request->input('title');
                    $expense->description = $request->input('description');
                    $expense->account_header_id = $request->input('account_header_id');
                    $expense->created_by = auth()->user()->id;
                    $expense->requested_by = auth()->user()->id;
                    $expense->amount = $cleaned_amount;

                    $expense->save();
                    return redirect('other-expenses')->with('success', 'Expense forwarded successfully to the Level for approval, Check back soon for its status!');
                }catch (\Exception $ex){
                    return back()->withErrors($ex->getMessage())->withInput();
                }
            case false:
                return view('erp.expenses.all_expenses', compact('other_expenses', 'categories'));
        }
    }


    public function allExpenses(Request $request){
        $method = $request->isMethod('post');
        $categories = AccountHeader::all();
        $other_expenses = Utilities::getOtherExpenses();
        switch($method){
            case true:
                 try{
                    $validator = Validator::make($request->all(), [
                        'title' => 'required|max:255',
                        'description' => 'required',
                        'account_header_id' => 'required',
                        'amount' => 'required|numeric'
                    ]);

                    if ($validator->fails()) {
                        return back()->withErrors($validator)->withInput();
                    }
                    $a = $request->input('amount');
                    $b = str_replace( ',', '', $a );
                    $cleaned_amount = str_replace('₦', '', $b);

                    $expense = new OtherExpense();
                    $expense->title = $request->input('title');
                    $expense->description = $request->input('description');
                    $expense->account_header_id = $request->input('account_header_id');
                    $expense->created_by = auth()->user()->id;
                    $expense->requested_by = auth()->user()->id;
                    $expense->amount = $cleaned_amount;

                    $expense->save();
                    return redirect('other-expenses')->with('success', 'Expense forwarded successfully to the Level for approval, Check back soon for its status!');
                }catch (\Exception $ex){
                    return back()->withErrors($ex->getMessage())->withInput();
                }
            case false:
                return view('erp.all_expenses', compact('other_expenses', 'categories'));
        }
    }

    public function approveExpense($expense_id)
    {
        $expense = OtherExpense::where('id', $expense_id)->first();
        $expense->update([
                        'status' => "approved",
                        'approved_by' => auth()->user()->id,
                        ]);
        return back()->with('success', 'Expense was approved successfully!');
    }

    public function declineExpense($expense_id)
    {
        $expense = OtherExpense::where('id', $expense_id)->first();
        $expense->update([
                        'status' => "declined"
                        ]);
        return back()->with('success', 'Expense declined successfully!');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'title' => 'required',
                'account_header_id' => 'required',
                'description' => 'required',
            ]);

        $expense = OtherExpense::findOrFail($id);
        $a = $request->input('amount');
        $b = str_replace( ',', '', $a );
        $cleaned_amount = str_replace('₦', '', $b);

        $expense->update([
            'title' => $request->input('title'),
            'account_header_id' => $request->account_header_id,
            'description' => $request->input('description'),
            'created_by' => auth()->user()->id,
            'requested_by' => auth()->user()->id,
            'amount' => $cleaned_amount

        ]);
        return back()->with('success', 'Expense was updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expense = Expense::find($id);
        $expense->delete();
        return back()->with('success', 'Expense was trashed successfully');
    }

//    public function approvedE
    public function approvedExpenses(Request $request){
        $method = $request->isMethod('post');
        $categories = AccountHeader::all();
        $other_expenses = \DB::select('SELECT id, title, amount, description, status, created_by, approved_by, created_at, updated_at, account_header_id,
                                      (SELECT name FROM account_headers WHERE id = account_header_id ) as category, 
                                      (SELECT first_name FROM hr_employee WHERE id = approved_by) as approved_by  FROM other_expenses WHERE status = "approved" AND created_by = '.auth()->user()->id);
        return view('erp.expenses.approved_expenses', compact('other_expenses', 'method', 'categories'));
    }
//done to disburse, submit expense request, new expense request
    public function declinedExpenses(Request $request){
        $other_expenses = \DB::select('SELECT id, title, amount, description, status, created_by, approved_by, created_at, updated_at, account_header_id,
                                      (SELECT name FROM account_headers WHERE id = account_header_id ) as category, 
                                      (SELECT first_name FROM hr_employee WHERE id = approved_by) as approved_by  FROM other_expenses WHERE status = "declined" AND created_by = '.auth()->user()->id);
        $categories = AccountHeader::all();
        return view('erp.expenses.declined_expenses', compact('other_expenses', 'method', 'categories'));
    }
}
