<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Employee;
use App\Models\Organization;
use Illuminate\Http\Request;
use App\Libraries\Utilities;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $company_id = auth()->user()->company->id;
        $company_uri = auth()->user()->company->uri;

        $departments = DB::select("SELECT `id`, `description`, `name`, `code`, `updated_by`, `parent_id`,
                                  (SELECT first_name FROM hr_employee WHERE id = id_hr_employee) as line_manager_name,
                                  (SELECT `name` FROM sys_organizations WHERE id = id_sys_organizations) as organization_name
                              
                                    FROM sys_departments as s WHERE advance_company_id = '$company_id' AND advance_company_uri = '$company_uri' AND s.deleted_at IS NULL");
       return view('departments.index')->with(['departments'=>$departments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $parentDepartments = Department::where(['advance_company_id' => auth()->user()->company->id, 'advance_company_id' => auth()->user()->company->uri])->select('id','name', 'code')->orderBy('created_at','DESC')->get();
        return view('departments.create')->with(['parentDepartments' => $parentDepartments]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //get parent department
        $parentDepartment = $request->get('parent_id');
        //store
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'id_sys_organizations' => 'required'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }


            $department = Department::create([
                'advance_company_id' => auth()->user()->company->id,
                'advance_company_uri' => auth()->user()->company->uri,
                'description' => $request->get('description'),
                'name' => $request->get('name'),
                'code' => $request->get('code'),
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id,
                'id_sys_organizations' => $request->get('id_sys_organizations'),
                'id_hr_employee' => $request->get('id_hr_employee'),
                'parent_id' => $request->parent_id
            ]);


            //insert into department tree
            $sql = "SELECT `parent_id`, ".$department->id.",
            (`length` + 1) AS `length`, ".Auth::user()->id.",".Auth::user()->id." FROM `sys_department_tree`
            WHERE `child_id` = ". $parentDepartment."
            UNION SELECT ".$department->id.",".$department->id.", 0, ".Auth::user()->id.",".Auth::user()->id." ";

            $parentSql = "INSERT `sys_department_tree` (`parent_id`, `child_id`, `length`, `created_by`, `updated_by`) ". $sql;

            //return;
            DB::insert(DB::raw($parentSql));

            //return;

            return redirect('departments')->with('success', 'Department Added!');

        }catch (\Exception $ex){
            return back()->withErrors($ex->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return view('departments.edit');
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dept = Department::where(['id' => $id,
                         'advance_company_id' => auth()->user()->company->id,
                          'advance_company_uri' => auth()->user()->company->uri
                          ])->first();
        $dept->delete();
        return back()->with('success', 'Department successfully deactivated');
    }

    public function editDepartment(Request $request, $id)
    {
        try{
            $method = $request->isMethod('post');

            $dept = Department::where(
                    ['advance_company_uri' => auth()->user()->company->uri,
                     'advance_company_id' => auth()->user()->company->id, 'id' => $id
                    ])->first();

            if(empty($dept)){
                return back()->with('error', 'Something went wrong!'); 
            }
            // //check if the authenticated company has access to this request model
            // Utilities::checkCompanyAuthorizationForARequest($dept->advance_company_id, $dept->advance_company_uri);
            
            $organizations = Organization::all();
            $employees = Employee::all();
            $parentDepartments =Department::where(
                                        ['advance_company_id' => auth()->user()->company->id,
                                            'advance_company_uri' => auth()->user()->company->uri,
                                            ])->get();
    
            switch ($method){
                case true :
                    $dept->update([
                                    'name' => $request->name,
                                    'id_sys_organizations' => $request->id_sys_organizations,
                                    'code' => $request->code,
                                    'parent_id' => $request->parent_id,
                                    'id_hr_employee' => $request->id_hr_employee,
                                    'description'=>$request->description
                    ]);
                    return redirect()->route('departments.index')->with('success', 'The information has been updated!');
    
                case false:
                    return view('departments.edit', compact('dept', 'organizations', 'employees', 'parentDepartments'));
            }
    
        } catch (\Exception $e){
            return back()->with('error', 'Something went wrong!');
        }

        //
    }
}
