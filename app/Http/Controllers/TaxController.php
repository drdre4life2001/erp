<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Tax;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use DB;

class TaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        return $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $adv_company_id = auth()->user()->company->id;
        $adv_company_uri = auth()->user()->company->uri;

        //all tax rates
        $items = Tax::where([
            'advance_company_id' => $adv_company_id,
            'advance_company_uri' => $adv_company_uri])
            ->orderBy('created_at', 'DESC')->get();

        $method = $request->isMethod('post');
        switch ($method){
            case true :
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'rate'  => 'required'
                ]);

                if($validator->fails()){
                    return back()->with('error', $validator);
                }

                $tax = new Tax();
                $tax->name = $request->name;
                $tax->rate = $request->rate;
                $tax->advance_company_id = $adv_company_id;
                $tax->advance_company_uri = $adv_company_uri;
                // dd($tax);
                if($tax->save()){
                    return back()->with('success', 'Tax Added!');
                }

            case false:
                return view('erp.finance.tax', compact('items'));
        }
    }

    public function updateTax(Request $request)
    {
        $method = $request->isMethod('post');
        if ($method) {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'rate' => 'required'
            ]);

            if ($validator->fails()) {
                return back()->with('error', $validator);
            }
            $update_tax = DB::table('sys_tax')
                ->where('id', $request->tax_id)
                ->update(
                    [
                        'name' => $request->name,
                        'rate' => $request->rate,
                        'updated_at' => date('Y-m-d h:i:s')
                    ]
                );
            if ($update_tax) {
                return back()->with('success', 'Tax updated!');
            }else{
                return back()->with('error', 'An error occured while updating tax');
            }
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('tax.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'rate' => 'required|numeric'
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $tax = new Tax();
        $tax->name = $request->name;
        $tax->rate = $request->rate;
        $tax->save();

        return redirect('finance/tax');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Fetch details for this particular request id
        $data = Tax::find($id);
        return view ('tax.edit', compact( 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'rate' => 'required|numeric'
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        DB::table('sys_tax')->where('id', $id)->update([
            'name' => $request->name,
            'rate' => $request->rate
        ]);
        return redirect('finance/tax');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tax::find($id)->delete();
        return redirect('finance/tax')->with('info', 'Tax trashed!');
    }
}
