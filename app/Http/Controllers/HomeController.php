<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Libraries\Utilities;
use App\Models\Employee;
use App\Models\EmployeeLeave;
use App\Models\MenuGroup;
use App\Models\Module;
use App\Models\User;
use App\Models\OtherExpense;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
// use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function home(Request $request){
        $company_id = $this->companyId();
        $company_uri = $this->companyUri();
        $employees = DB::SELECT("SELECT COUNT(DISTINCT id) as count FROM hr_employee WHERE company_id = '$company_id' AND company_uri = '$company_uri'");
        $users = DB::SELECT("SELECT COUNT(DISTINCT id) as count FROM sys_user WHERE company_id = $company_id");
        //Total loans
        $loans = DB::table('hr_employee_loans')->where('company_id', $company_id)->where('company_uri', $company_uri)->sum('total_payable');
        $loan = number_format($loans);
        //Latest Loan Request
        $leave_requests = DB::SELECT("SELECT COUNT(DISTINCT id) as count FROM hr_employee_leaves WHERE company_id = '$company_id' AND company_uri = '$company_uri' AND deleted_at IS NULL");

        $total_salary = Utilities::totalSalary();
        $total_revenue =  Utilities::totalRevenue();
        $total_payable = Utilities::totalPayable();

        $total_expense = Utilities::totalExpense();

        $url = $request->path();
        //expense/request, leave and letters
        $leaves = DB::select("SELECT id, (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name, 
                                            (SELECT first_name FROM hr_employee WHERE id = employee_id) as first_name,
                                              (SELECT picture FROM hr_employee WHERE id = employee_id) as picture,
                                                (SELECT lastname FROM hr_employee WHERE id = employee_id) as lastname, 
                                                  (SELECT `name` FROM hr_leave_types WHERE id = type) as type,
                                                    start, `end`, days, description, status, created_at FROM hr_employee_leaves WHERE deleted_at IS NULL AND company_id = '$company_id' AND company_uri = '$company_uri'");
        $departments = DB::SELECT("SELECT `name`, id FROM sys_departments WHERE deleted_at IS NULL and advance_company_id = '$company_id' and advance_company_uri = '$company_uri' ORDER BY id");
        $result = [];
        foreach ($departments as $dept) {
            $employee_count_per_department = DB::SELECT("select COUNT(id_sys_departments) as numb_of_emp FROM hr_employee_departments WHERE id_sys_departments = '$dept->id' and advance_company_id = '$company_id' and advance_company_uri = '$company_uri'");
            $result[] = [
                'name' => $dept->name,
                'y' => $employee_count_per_department[0]->numb_of_emp,
            ];
        }
        $all_data = json_encode($result);
        $ledgers = DB::select("select a.id as id, a.prevBalance as prevBalance, a.currentBalance as currentBalance, a.amount as amount, 
                c.name as bank_name, b.account_number as account_number, d.name as effect, a.advance_company_uri as company_uri, a.advance_company_id as company_id from ledgers as a, 
                sys_bank_accounts as b, sys_banks as c, account_effects as d
                where a.bank_id = b.id and b.bank_id = c.id and a.type = d.value and a.advance_company_uri = '$company_uri' and a.advance_company_id = '$company_id' order by a.created_at DESC");

        $total_prospects = \App\Libraries\Utilities::totalProspects();

        $total_inline =\App\Libraries\Utilities::totaInLineProspects();

        $total_close = \App\Libraries\Utilities::totalClosedProspects();

        $total_deactivated = \App\Libraries\Utilities::totalDeactivatedProspects();


        $data =\App\Libraries\Utilities::employeeByDepartment();
        $pipeline_status = Utilities::crmPipeline();
        $other_expenses = Utilities::otherExpenses();
        $item = Utilities::crm_prospects();
        $status = Utilities::crmStatus();
        $approved_expenses =  Utilities::otherExpenses("approved");
        $declined_expenses =  Utilities::otherExpenses("declined");
        $pending_expenses =  Utilities::otherExpenses("pending");
        return view('erp.dashboard.index', compact('url', 'item', 'status', 'approved_expenses', 'pending_expenses', 'declined_expenses', 'pipeline_status', 'total_prospects', 'total_inline', 'total_close', 'total_deactivated', 'all_data', 'other_expenses', 'employees', 'users', 'data', 'loan', 'leave_requests', 'ledgers', 'total_salary', 'total_revenue', 'total_payable', 'total_expense', 'expenses', 'leaves'));
    }
    public function hr_new(Request $request){
        $url = $request->path();
        return view('erp.dashboard.index', compact('url'));
    }
    public function procure_new(Request $request){
        $url = $request->path();
        return view('erp.dashboard.index', compact('url'));
    }
    public function crm(Request $request){
        $url = $request->path();
        return view('erp.dashboard.index', compact('url'));
    }
    public function finance_new(Request $request){
        $url = $request->path();
        return view('erp.dashboard.index', compact('url'));
    }
    public function quarter($ts) {
        return ceil(date('n', $ts)/3);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            //get role and permissions
            $user = User::with('assignedRole.rolePermissions.permission')->where('id', Auth::user()->id)->first();
            $userPermissions = array();
            $userRole = isset($user->assignedRole->slug)? $user->assignedRole->slug : null;
            if(isset($user->assignedRole)){
                foreach($user->assignedRole->rolePermissions as $rolePermission){
                    //$userPermissions[$rolePermission->permission->id]['slug'] = $rolePermission->permission->slug;
                    $userPermissions[$rolePermission->permission->slug] = $rolePermission->level;
                }
            }
            //get accessible menus
            $menuGroups = MenuGroup::with('menus.permission')->get();
            $permittedMenuGroups = array();
            foreach($menuGroups as $menuGroup){
                $permittedMenuGroups[$menuGroup->id]['name'] = $menuGroup->name;
                $permittedMenus = array();
                foreach($menuGroup->menus as $menu){
                    if(isset($userPermissions[$menu->permission->slug])){
                        $permittedMenus[] = ['name'=>$menu->name, 'url'=>$menu->url];
                    }
                }
                $permittedMenuGroups[$menuGroup->id]['menus'] = $permittedMenus;
            }
            //get employee details: department, organization, name
            $userDepartments = array();
            $userOrganization = array();
            $userDetails = null;
            $employee = Employee::find(Auth::user()->id_hr_employee);

            if(!empty($employee)){
                $userDetails = $employee;
                $employee->load(['employeeDepartments' => function($query){
                    $query->where('end_date', null);
                    $query->with('department.organization');
                }]);

                foreach($employee->employeeDepartments as $employeeDepartment){
                    //get the department user belongs to
                    $userDepartments[$employeeDepartment->id] = isset($employeeDepartment->department->name)? $employeeDepartment->department->name : null;
                    //get corresponding organization user belongs to
                    if(isset($employeeDepartment->department->id_sys_organizations)){
                        $userOrganization[$employeeDepartment->department->id_sys_organizations] = isset($employeeDepartment->department->organization->name) ? $employeeDepartment->department->organization->name : null;
                    }
                }
            }

            //set ability and role
            Session::put('abilities', $userPermissions);
            Session::put('role', $userRole);
            Session::put('menuGroups',$permittedMenuGroups);//set accessible menus
            Session::put('department', $userDepartments);//sets the department(s) the user belongs to
            Session::put('organization', $userOrganization);//set the organization(s) the user belongs to
            Session::put('userInfo', $userDetails); //sets user employee details
            $qtr = self::quarter(strtotime(date('Y-m-d H:i:s')));
            $utilities = new Utilities();
            $quarter = $utilities->createFullWordOrdinal($qtr);
            $expense = OtherExpense::where('deleted_at', NULL)->paginate(5);
            //Get menu links for this user
            $id_hr_employee= Auth::user()->id_hr_employee;
            $modules = Module::orderBy('created_at')->get();
            //return $id_hr_employee;
            return view('dashboard')->with(['modules'=>$modules, 'quarter' => $quarter, 'expense' => $expense]);
        }catch(\Exception $ex){
            echo $ex->getMessage();
        }
    }

    public function hr()
    {

        try{
            //get role and permissions
            $user = User::with('assignedRole.rolePermissions.permission')->where('id', Auth::user()->id)->first();
            $userPermissions = array();
            $userRole = isset($user->assignedRole->slug)? $user->assignedRole->slug : null;
            if(isset($user->assignedRole)){
                foreach($user->assignedRole->rolePermissions as $rolePermission){
                    //$userPermissions[$rolePermission->permission->id]['slug'] = $rolePermission->permission->slug;
                    $userPermissions[$rolePermission->permission->slug] = $rolePermission->level;
                }
            }
            //get accessible menus
            $menuGroups = MenuGroup::with('menus.permission')->get();
            $permittedMenuGroups = array();
            foreach($menuGroups as $menuGroup){
                $permittedMenuGroups[$menuGroup->id]['name'] = $menuGroup->name;
                $permittedMenus = array();
                foreach($menuGroup->menus as $menu){
                    if(isset($userPermissions[$menu->permission->slug])){
                        $permittedMenus[] = ['name'=>$menu->name, 'url'=>$menu->url];
                    }
                }
                $permittedMenuGroups[$menuGroup->id]['menus'] = $permittedMenus;
            }
            //get employee details: department, organization, name
            $userDepartments = array();
            $userOrganization = array();
            $userDetails = null;
            $employee = Employee::find(Auth::user()->id_hr_employee);

            if(!empty($employee)){
                $userDetails = $employee;
                $employee->load(['employeeDepartments' => function($query){
                    $query->where('end_date', null);
                    $query->with('department.organization');
                }]);

                foreach($employee->employeeDepartments as $employeeDepartment){
                    //get the department user belongs to
                    $userDepartments[$employeeDepartment->id] = isset($employeeDepartment->department->name)? $employeeDepartment->department->name : null;
                    //get corresponding organization user belongs to
                    if(isset($employeeDepartment->department->id_sys_organizations)){
                        $userOrganization[$employeeDepartment->department->id_sys_organizations] = isset($employeeDepartment->department->organization->name) ? $employeeDepartment->department->organization->name : null;
                    }
                }
            }

            //set ability and role
            Session::put('abilities', $userPermissions);
            Session::put('role', $userRole);
            Session::put('menuGroups',$permittedMenuGroups);//set accessible menus
            Session::put('department', $userDepartments);//sets the department(s) the user belongs to
            Session::put('organization', $userOrganization);//set the organization(s) the user belongs to
            Session::put('userInfo', $userDetails); //sets user employee details
            $qtr = self::quarter(strtotime(date('Y-m-d H:i:s')));
            $utilities = new Utilities();
            $quarter = $utilities->createFullWordOrdinal($qtr);

            //Get menu links for this user
            $id_hr_employee= Auth::user()->id_hr_employee;
            $role_id = Utilities::getRoleId();
            $links = DB::select("select a.link as links from sys_menu_links as a, sys_modules as b, user_permissions as c
            where 
            a.module_id = b.id and 
            c.resource_id = a.module_id and 
            c.role_id = $role_id
            ");
            Session::put('links', $links);
            $modules = Module::orderBy('created_at')->get();

            //show total number of employees
            $employee =  DB::table('hr_employee')->count();


            //Total loans
            $loans = DB::table('hr_employee_loans')->sum('total_payable');
            $loan = number_format($loans);
            //Latest Loan Requests
            $loan_requests = DB::table('hr_employee_loans')-> orderBy('id', 'desc')->take(5)->get();

            //Latest leave requests
            $leave_requests = DB::table('hr_employee_leaves')->orderBy('id', 'desc')->take(5)->get();


            //Total Salaries
            $total_salaries = DB::table('hr_employee_renumerations')->sum('basic_pay');
            $total_salary = number_format($total_salaries);
            $netpay_department = DB::select(DB::raw("select count(department_id) as employees, sum(net_pay) as total_netpay, 
         (select name from sys_departments where id = department_id) AS department from hr_payrolls group by department_id "));
            return view('hr_dashboard')->with(['modules'=>$modules, 'quarter' => $quarter, 'employee' => $employee,
                'loan' => $loan, 'loan_requests'=>$loan_requests, 'leave_requests'=>$leave_requests,
                'total_salary'=>$total_salary, 'netpay_department' =>$netpay_department]);
        }catch(\Exception $ex){
            echo $ex->getMessage();
        }
    }

    public function procurement()
    {
        try{
            //get role and permissions
            $user = User::with('assignedRole.rolePermissions.permission')->where('id', Auth::user()->id)->first();
            $userPermissions = array();
            $userRole = isset($user->assignedRole->slug)? $user->assignedRole->slug : null;
            if(isset($user->assignedRole)){
                foreach($user->assignedRole->rolePermissions as $rolePermission){
                    //$userPermissions[$rolePermission->permission->id]['slug'] = $rolePermission->permission->slug;
                    $userPermissions[$rolePermission->permission->slug] = $rolePermission->level;
                }
            }
            //get accessible menus
            $menuGroups = MenuGroup::with('menus.permission')->get();
            $permittedMenuGroups = array();
            foreach($menuGroups as $menuGroup){
                $permittedMenuGroups[$menuGroup->id]['name'] = $menuGroup->name;
                $permittedMenus = array();
                foreach($menuGroup->menus as $menu){
                    if(isset($userPermissions[$menu->permission->slug])){
                        $permittedMenus[] = ['name'=>$menu->name, 'url'=>$menu->url];
                    }
                }
                $permittedMenuGroups[$menuGroup->id]['menus'] = $permittedMenus;
            }
            //get employee details: department, organization, name
            $userDepartments = array();
            $userOrganization = array();
            $userDetails = null;
            $employee = Employee::find(Auth::user()->id_hr_employee);

            if(!empty($employee)){
                $userDetails = $employee;
                $employee->load(['employeeDepartments' => function($query){
                    $query->where('end_date', null);
                    $query->with('department.organization');
                }]);

                foreach($employee->employeeDepartments as $employeeDepartment){
                    //get the department user belongs to
                    $userDepartments[$employeeDepartment->id] = isset($employeeDepartment->department->name)? $employeeDepartment->department->name : null;
                    //get corresponding organization user belongs to
                    if(isset($employeeDepartment->department->id_sys_organizations)){
                        $userOrganization[$employeeDepartment->department->id_sys_organizations] = isset($employeeDepartment->department->organization->name) ? $employeeDepartment->department->organization->name : null;
                    }
                }
            }

            //set ability and role
            Session::put('abilities', $userPermissions);
            Session::put('role', $userRole);
            Session::put('menuGroups',$permittedMenuGroups);//set accessible menus
            Session::put('department', $userDepartments);//sets the department(s) the user belongs to
            Session::put('organization', $userOrganization);//set the organization(s) the user belongs to
            Session::put('userInfo', $userDetails); //sets user employee details
            $qtr = self::quarter(strtotime(date('Y-m-d H:i:s')));
            $utilities = new Utilities();
            $quarter = $utilities->createFullWordOrdinal($qtr);

            //Get menu links for this user
            $id_hr_employee= Auth::user()->id_hr_employee;

            $links = DB::select("select a.link as links from sys_menu_links as a, sys_modules as b, user_permissions as c
            where 
            a.module_id = b.id and 
            c.resource_id = a.module_id and 
            c.role_id = (SELECT role from sys_user where id_hr_employee = $id_hr_employee)
            ");
            Session::put('links', $links);
            $modules = Module::orderBy('created_at')->get();

            //total request
            $total_requests =  DB::table('request_items')->sum('total');
            $total_request = number_format($total_requests);

            //total approved
            $total_apr =  DB::table('fin_requests')->sum('procurement_total_price');
            $total_approved = number_format( $total_apr);

            //Unapproved cash
            $unapprovedCash =  number_format($total_requests -  $total_apr);

            //return $id_hr_employee;
            $procurement_requests = DB::table('request_items')-> orderBy('id', 'desc')->take(5)->get();
            //Approved requests
            $approved_request = DB::select(DB::raw("select sum( line_manager_remark and c_level_remark) as approved  from request_items where line_manager_remark = 2 and c_level_remark = 4"));

            //unapproved request
            $unapproved_request = DB::select(DB::raw("select sum( line_manager_remark and c_level_remark) as unapproved  from request_items where line_manager_remark != 2 and c_level_remark != 4"));



            return view('procurement_dashboard')->with(['modules'=>$modules, 'quarter' => $quarter,
                'procurement_requests' => $procurement_requests, 'total_request' =>$total_request, 'total_approved' =>$total_approved,
                'approved_request' =>$approved_request, 'unapproved_request'=>$unapproved_request, 'unapprovedCash'=>$unapprovedCash]);
        }catch(\Exception $ex){
            echo $ex->getMessage();
        }
    }
    public function project()
    {
        try{
            //get role and permissions
            $user = User::with('assignedRole.rolePermissions.permission')->where('id', Auth::user()->id)->first();
            $userPermissions = array();
            $userRole = isset($user->assignedRole->slug)? $user->assignedRole->slug : null;
            if(isset($user->assignedRole)){
                foreach($user->assignedRole->rolePermissions as $rolePermission){
                    //$userPermissions[$rolePermission->permission->id]['slug'] = $rolePermission->permission->slug;
                    $userPermissions[$rolePermission->permission->slug] = $rolePermission->level;
                }
            }
            //get accessible menus
            $menuGroups = MenuGroup::with('menus.permission')->get();
            $permittedMenuGroups = array();
            foreach($menuGroups as $menuGroup){
                $permittedMenuGroups[$menuGroup->id]['name'] = $menuGroup->name;
                $permittedMenus = array();
                foreach($menuGroup->menus as $menu){
                    if(isset($userPermissions[$menu->permission->slug])){
                        $permittedMenus[] = ['name'=>$menu->name, 'url'=>$menu->url];
                    }
                }
                $permittedMenuGroups[$menuGroup->id]['menus'] = $permittedMenus;
            }
            //get employee details: department, organization, name
            $userDepartments = array();
            $userOrganization = array();
            $userDetails = null;
            $employee = Employee::find(Auth::user()->id_hr_employee);

            if(!empty($employee)){
                $userDetails = $employee;
                $employee->load(['employeeDepartments' => function($query){
                    $query->where('end_date', null);
                    $query->with('department.organization');
                }]);

                foreach($employee->employeeDepartments as $employeeDepartment){
                    //get the department user belongs to
                    $userDepartments[$employeeDepartment->id] = isset($employeeDepartment->department->name)? $employeeDepartment->department->name : null;
                    //get corresponding organization user belongs to
                    if(isset($employeeDepartment->department->id_sys_organizations)){
                        $userOrganization[$employeeDepartment->department->id_sys_organizations] = isset($employeeDepartment->department->organization->name) ? $employeeDepartment->department->organization->name : null;
                    }
                }
            }

            //set ability and role
            Session::put('abilities', $userPermissions);
            Session::put('role', $userRole);
            Session::put('menuGroups',$permittedMenuGroups);//set accessible menus
            Session::put('department', $userDepartments);//sets the department(s) the user belongs to
            Session::put('organization', $userOrganization);//set the organization(s) the user belongs to
            Session::put('userInfo', $userDetails); //sets user employee details
            $qtr = self::quarter(strtotime(date('Y-m-d H:i:s')));
            $utilities = new Utilities();
            $quarter = $utilities->createFullWordOrdinal($qtr);

            //Get menu links for this user
            $id_hr_employee= Auth::user()->id_hr_employee;

            $links = DB::select("select a.link as links from sys_menu_links as a, sys_modules as b, user_permissions as c
            where 
            a.module_id = b.id and 
            c.resource_id = a.module_id and 
            c.role_id = (SELECT role from sys_user where id_hr_employee = $id_hr_employee)
            ");
            Session::put('links', $links);
            $modules = Module::orderBy('created_at')->get();
            return view('project_dashboard')->with(['modules'=>$modules, 'quarter' => $quarter]);
        }catch(\Exception $ex){
            echo $ex->getMessage();
        }
    }


    public function employee()
    {
        try{
            //get role and permissions
            $user = User::with('assignedRole.rolePermissions.permission')->where('id', Auth::user()->id)->first();
            $userPermissions = array();
            $userRole = isset($user->assignedRole->slug)? $user->assignedRole->slug : null;
            if(isset($user->assignedRole)){
                foreach($user->assignedRole->rolePermissions as $rolePermission){
                    //$userPermissions[$rolePermission->permission->id]['slug'] = $rolePermission->permission->slug;
                    $userPermissions[$rolePermission->permission->slug] = $rolePermission->level;
                }
            }
            //get accessible menus
            $menuGroups = MenuGroup::with('menus.permission')->get();
            $permittedMenuGroups = array();
            foreach($menuGroups as $menuGroup){
                $permittedMenuGroups[$menuGroup->id]['name'] = $menuGroup->name;
                $permittedMenus = array();
                foreach($menuGroup->menus as $menu){
                    if(isset($userPermissions[$menu->permission->slug])){
                        $permittedMenus[] = ['name'=>$menu->name, 'url'=>$menu->url];
                    }
                }
                $permittedMenuGroups[$menuGroup->id]['menus'] = $permittedMenus;
            }
            //get employee details: department, organization, name
            $userDepartments = array();
            $userOrganization = array();
            $userDetails = null;
            $employee = Employee::find(Auth::user()->id_hr_employee);

            if(!empty($employee)){
                $userDetails = $employee;
                $employee->load(['employeeDepartments' => function($query){
                    $query->where('end_date', null);
                    $query->with('department.organization');
                }]);

                foreach($employee->employeeDepartments as $employeeDepartment){
                    //get the department user belongs to
                    $userDepartments[$employeeDepartment->id] = isset($employeeDepartment->department->name)? $employeeDepartment->department->name : null;
                    //get corresponding organization user belongs to
                    if(isset($employeeDepartment->department->id_sys_organizations)){
                        $userOrganization[$employeeDepartment->department->id_sys_organizations] = isset($employeeDepartment->department->organization->name) ? $employeeDepartment->department->organization->name : null;
                    }
                }
            }

            //set ability and role
            Session::put('abilities', $userPermissions);
            Session::put('role', $userRole);
            Session::put('menuGroups',$permittedMenuGroups);//set accessible menus
            Session::put('department', $userDepartments);//sets the department(s) the user belongs to
            Session::put('organization', $userOrganization);//set the organization(s) the user belongs to
            Session::put('userInfo', $userDetails); //sets user employee details
            $qtr = self::quarter(strtotime(date('Y-m-d H:i:s')));
            $utilities = new Utilities();
            $quarter = $utilities->createFullWordOrdinal($qtr);

            //Get menu links for this user
            $id_hr_employee= Auth::user()->id_hr_employee;

            $links = DB::select("select a.link as links from sys_menu_links as a, sys_modules as b, user_permissions as c
            where 
            a.module_id = b.id and 
            c.resource_id = a.module_id and 
            c.role_id = (SELECT role from sys_user where id_hr_employee = $id_hr_employee)
            ");
            Session::put('links', $links);
            $modules = Module::orderBy('created_at')->get();
            return view('employees_dashboard')->with(['modules'=>$modules, 'quarter' => $quarter]);
        }catch(\Exception $ex){
            echo $ex->getMessage();
        }
    }
}