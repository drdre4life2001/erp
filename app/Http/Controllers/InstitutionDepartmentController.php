<?php

namespace App\Http\Controllers;

use App\Models\InstitutionDepartment;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class InstitutionDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $institutionDepartment = InstitutionDepartment::orderBy('created_at', 'DESC')->get();
        return view('institution_departments.index')->with(['institutionDepartments'=>$institutionDepartment]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('institution_departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            InstitutionDepartment::create([
                'name' => $request->get('name'),
                'description'=> $request->get('description'),
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id
            ]);

            return redirect('institutionDepartments');
        }catch (\Exception $ex){
            return back()->withErrors($ex->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function editInstitutionDepartment(Request $request, $id)
    {
        $method = $request->isMethod('post');
        $institutionDepartment = InstitutionDepartment::findOrFail($id);
//        dd($request->all());

        switch ($method){
            case true :
                $institutionDepartment->update([
                    'name' => $request->name,
                    'description' => $request->description,


                ]);

                return redirect()->route('institutionDepartments.index')->with('success', 'The information has been updated!');

            case false:
                return view('institution_departments.edit', compact('institutionDepartment'));
        }

        //
    }
}
