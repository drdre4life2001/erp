<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Tax;

class ItemController extends Controller
{
    //
    public function __construct(){

        return $this->middleware('auth');
    }
    public function totalItem(Request $request)
    {
        //
        //$input_items = request('item');
        $input_items = $request->item;

        $mydata = new \stdClass;

        $sub_total = 0;
        $tax_total = 0;
        $items = array();

        if ($input_items) {
            foreach ($input_items as $key => $item) {
                $item_tax_total = 0;
                $price = isset($item['price']) ? $item['price'] : 0;
                $qty = isset($item['quantity']) ? $item['quantity'] : 0;
                $item_sub_total = ($price * $qty);
                if (isset($item['tax_id'])) {
                    $tax = Tax::find($item['tax_id']);
                    $rate = isset($tax->rate) ? $tax->rate : 0;
                    $item_tax_total = (($price * $qty) / 100) * $rate;
                }

                $sub_total += $item_sub_total;
                $tax_total += $item_tax_total;
                $total = $item_sub_total + $item_tax_total;
                $items[$key] = $total;
            }

        }

        $mydata->items = $items;

        $mydata->sub_total = $sub_total; //money($sub_total, $currency_code, true)->format();

        $mydata->tax_total = $tax_total; //money($tax_total, $currency_code, true)->format();

        $grand_total = $sub_total + $tax_total;

        $mydata->grand_total = $grand_total; //money($grand_total, $currency_code, true)->format();

        return response()->json(['mydata' => $mydata]);
    }

    public function totalItemAjax(Request $request)
    {
        if ($request->ajax()) {
            //
            //$input_items = request('item');
            $input_items = $request->item;

            $mydata = new \stdClass;

            $sub_total = 0;
            $tax_total = 0;
            $items = array();

            if ($input_items) {
                foreach ($input_items as $key => $item) {
                    $item_tax_total = 0;
                    $price = isset($item['price']) ? $item['price'] : 0;
                    $qty = isset($item['quantity']) ? $item['quantity'] : 0;
                    $item_sub_total = ($price * $qty);
                    if (isset($item['tax_id'])) {
                        $tax = Tax::find($item['tax_id']);
                        $rate = isset($tax->rate) ? $tax->rate : 0;
                        $item_tax_total = (($price * $qty) / 100) * $rate;
                    }

                    $sub_total += $item_sub_total;
                    $tax_total += $item_tax_total;
                    $total = $item_sub_total + $item_tax_total;
                    $items[$key] = $total;
                }

            }

            $mydata->items = $items;

            $mydata->sub_total = $sub_total; //money($sub_total, $currency_code, true)->format();

            $mydata->tax_total = $tax_total; //money($tax_total, $currency_code, true)->format();

            $grand_total = $sub_total + $tax_total;

            $mydata->grand_total = $grand_total; //money($grand_total, $currency_code, true)->format();

            return response()->json(['mydata' => $mydata]);
        
        }else{
            return;
        }
    }

}

