<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Libraries\ApiServices;

use App\Libraries\Utilities;

use App\Http\Requests;

use Carbon\Carbon;

use Illuminate\Support\Facades\DB;

use App\Models\AccountEffect;

use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    public function createAccount(Request $request)
    {
        $accounts = ApiServices::getAccounts(auth()->user()->advance_username);

        if($request->isMethod('post')){

            $validator = Validator::make($request->all(), [
                'account_category' => 'required',
                'name' => 'required'
            ]);
            
            $data = [
                    //for organizations or business default will be set to 2 because we are dealing with business
                      "account_type_id" => "2", 
                      "account_category_id" => $request->account_category,
                      "customer_uid" => auth()->user()->advance_username,
                      "name" => $request->name
                    ];

            $response = ApiServices::callApi($data, "/accounts", null, "POST");

            if($response['code'] == "201" && $response['status'] == "SUCCESS"){ //created
                return back()->with('success', 'Account created successfully!');
            }else{
                return back()->with('error', $response['message']);
            }

        }else{
            return view('erp.finance.account.create', compact('customer_accounts', 'accounts'));
        }
    }

    public function viewAccountBudget($account_number){
        
        $income_sources = ApiServices::incomeSources();
        $f_expenses = ApiServices::expenses("FIXED");
        $v_expenses = ApiServices::expenses("VARIABLE");

        $response = ApiServices::callApi(null, "/accounts/$account_number/budgets",  null, "GET");
        $budget = null;
        if($response['code'] == "200"){
            if(empty($response['your_response'])){
                return redirect()->back()->with('error', "Budget not found for this account!");
            }
            $budget = $response['your_response'];
            $budget = $budget[0];
            if(isset($budget->income_sources)){
                foreach ($budget->income_sources as $key => $value) {
                    $budget->income_sources[$key]['name'] = ApiServices::incomeSources($value['id']);
                }

                foreach ($budget->fixed_expenses as $key => $value) {
                    $budget->fixed_expenses[$key]['name'] = ApiServices::expenses("FIXED", $value['id']);
                }

                foreach ($budget->variable_expenses as $key => $value) {
                    $budget->variable_expenses[$key]['name'] = ApiServices::expenses("VARIABLE", $value['id']);
                }
            }
        }else{
            return back()->with('error', $response['message']);
        }

        return view('erp.finance.account.view_budget', compact('budget', 'income_sources', 'f_expenses', 'v_expenses'));
    }

    public function deleteAccount($account_number){
        $response = ApiServices::callApi(null, "/accounts/$account_number",  null, "DELETE");
        if($response == "204"){
            return back()->with('success', 'Account Deleted!');
        }else{
            return back()->with('error', 'Something went wrong!');
        }
    }

    public function createEffect(Request $request){

        $effects = AccountEffect::where('advance_company_uri', auth()->user()->company->uri)->get();
      
        if($request->isMethod('post')){

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'value' => 'required'
            ]);
            
            $data = AccountEffect::create([
                'name' => $request->name,
                'value' => $request->value,
                'advance_company_uri' => auth()->user()->company->uri,
                'advance_company_id' => auth()->user()->company->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            if($data){ //created
                return back()->with('success', 'Account created successfully!');
            }else{
                return back()->with('error', 'Something went wrong!');
            }

        }else{
            return view('erp.finance.account_effects', compact('customer_accounts', 'effects'));
        }

    }

    public function effects_delete(Request $request, $id)
    {
        AccountEffect::find($id)->delete();
        return back()->with('info', 'Account Effect trashed!');
    }

    public function effects_edit(Request $request){

        if(empty($request->name))
            return back()->with('error', 'Please insert effect name');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'value' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        DB::table('account_effects')->where('id', $request->bank_id)
            ->update(['name' => $request->name, 'value' => $request->value]);

        $request->session()->flash('success', 'Account Effects details updated!');

        return back()->with('success', 'Account Effect Updated');
    }

}
