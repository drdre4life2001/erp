<?php

namespace App\Http\Controllers;

use App\Libraries\ApiServices;
use Illuminate\Http\Request;
use App\Libraries\Utilities;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class BudgetController extends Controller
{
    public function create(Request $request)
    {
        $income_sources = ApiServices::incomeSources();
        $f_expenses = ApiServices::expenses("FIXED");
        $v_expenses = ApiServices::expenses("VARIABLE");
        $accounts = ApiServices::getAccounts(auth()->user()->advance_username);
        
        if($request->isMethod('post')){
            $data = self::budgetData($request);
            $response = ApiServices::callApi($data, "/accounts/".$request->account_number."/budgets", null, "POST");

                if($response['code'] == "200"){ //created
                    return back()->with('success', 'Budget created successfully!');
                }else{
                    return back()->with('error', $response['message']);
                }
    	}else{
    	    return view('erp.finance.budget.create', compact('organizations', 'accounts', 'item', 'pipeline_status', 'status', 'income_sources', 'f_expenses', 'v_expenses'));
    	}
    }

    public function stdClass()
    {
        return new \StdClass();
    }

    public function viewBudget($account_number, $budget_uid){
        $response = ApiServices::callApi(null, "/accounts/$account_number/budgets/$budget_uid",  null, "GET");
        $budget = null;
        if($response['code']== "200"){
            $budget = (object) $response['your_response'];
            if(isset($budget->income_sources)){
                foreach ($budget->income_sources as $key => $value) {
                    $budget->income_sources[$key]['name'] = ApiServices::incomeSources($value['name']);
                }

                foreach ($budget->fixed_expenses as $key => $value) {
                    $budget->fixed_expenses[$key]['name'] = ApiServices::expenses("FIXED", $value['name']);
                }

                foreach ($budget->variable_expenses as $key => $value) {
                    $budget->variable_expenses[$key]['name'] = ApiServices::expenses("VARIABLE", $value['name']);
                }
            }
        }else{
            return back()->with('error', $response['message']);
        }

        return view('erp.finance.budget.view_budget', compact('budget'));
    }


    public function deleteBudget($account_number, $budget_uid){
        $response = ApiServices::callApi(null, "/accounts/$account_number/budgets/$budget_uid",  null, "DELETE");
        if($response == "204"){
            return redirect()->route('create-budget')->with('success', 'Budget Trashed!');
        }else{
            return back()->with('error', 'something went wrong!');
        }
    }

    public function updateBudget(Request $request, $acc_number, $budget_uid){
        if($request->isMethod('patch')) {
            $data = self::budgetData($request);
            $response = ApiServices::callApi($data, "/accounts/$acc_number/budgets/$budget_uid", null, "PATCH");
            if ($response['code'] == "200") {
                return back()->with('success', 'Budget Updated!');
            } else {
                return back()->with('error', 'something went wrong! Date : '. $response['message']);
            }
        }
    }

    public static function budgetData(Request $request){
        $validator = Validator::make($request->all(), [
            'account_number' => 'required|max:255',
            'name' => 'required',
            'income' => 'required',
            'income_name' => 'required',
            'income_amount' => 'required',
            'f_name' => 'required',
            'f_amount' => 'required',
            'v_name' => 'required',
            'v_amount' => 'required'
        ]);
        $values = Input::only('account_number', 'name', 'income', 'income_name', 'income_amount', 'f_name', 'f_amount', 'v_name', 'v_amount', 'start_date', 'end_date');
        $account_number = $values['account_number'];
        $name = $values['name'];
        $start_date = $values['start_date'];
        $end_date = $values['end_date'];
        $income = $values['income'];
        $income_name = $values['income_name'];
        $income_amount = $values['income_amount'];
        $f_name = $values['f_name'];
        $f_amount = $values['f_amount'];
        $v_amount = $values['f_name'];
        $v_name = $values['v_name'];
        $v_amount = $values['v_amount'];

        $income_sources_arr = array();
        $fixed_expense_arr = array();
        $variable_expense_arr = array();

        if (is_array($income_name)) {
            //income object
            foreach ($income_name as $key => $val) {
                $income_sources_arr[] = (object)array('id' => $income_name[$key], 'amount' => Utilities::cleanData($income_amount[$key]));
            }
        }
        if (is_array($f_name)) {
            // fixed expenses object
            foreach ($f_name as $key => $val) {
                $fixed_expense_arr[] = (object)array('id' => $f_name[$key], 'amount' => Utilities::cleanData($f_amount[$key]));
            }
        }
        if (is_array($v_name)) {
            // variable expenses object
            foreach ($v_name as $key => $val) {
                $variable_expense_arr[] = (object)array('id' => $v_name[$key], 'amount' => Utilities::cleanData($v_amount[$key]));
            }
        }

        $income_sources = $income_sources_arr;
        $fixed_expenses = $fixed_expense_arr;
        $variable_expenses = $variable_expense_arr;
        $data = [
            'name' => $name,
            'income' => $income,
            'income_sources' => $income_sources,
            'fixed_expenses' => $fixed_expenses,
            'variable_expenses' => $variable_expenses,
            'start_date' => $start_date,
            'end_date' => $end_date
        ];
        return $data;
    }
}
