<?php

namespace App\Http\Controllers;

use App\Models\LineManager;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LineManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $validator = Validator::make($request->all(), [
                'manager_id' => 'required|max:255',
                'employee_id' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $time = new Carbon();
            $time = $time->timestamp;
            $existing = LineManager::where('child_employee_id',$request->employee_id)->get();
            //A user is his default manager on creation with length 0.
            //insert into manager tree
            $sql = "SELECT `parent_employee_id`, ".$request->get('employee_id').",
            (`length` + 1) AS `length`, ".Auth::user()->id.",".Auth::user()->id." , NOW() , NOW()  FROM `hr_line_manager`
            WHERE `child_employee_id` = ". $request->get('manager_id')."";

            $parentSql = "INSERT `hr_line_manager` (`parent_employee_id`, `child_employee_id`, `length`, `created_by`,
             `updated_by`, created_at, updated_at )
            ". $sql;
            $save = DB::insert(DB::raw($parentSql));
            if($save){
                if($existing->count() > 0){
                    foreach ($existing as $exists){
                        $line_mgr = LineManager::find($exists->id);
                        $line_mgr->active = 0;
                        $line_mgr->save();
                    }
                }
                return back()->with('success','Manager changed succesfully');
            }else{
                return back()->withErrors('Manager changed succesfully');
            }

        }catch (\Exception $ex){
            return $ex->getMessage();
            return back()->withErrors($ex)->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
