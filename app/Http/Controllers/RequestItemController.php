<?php

namespace App\Http\Controllers;

use App\Models\RequestCategory;
use App\Models\RequestItem;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RequestItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requestItems = RequestItem::with('category')->orderBy('created_at', 'DESC')->get();
        return view('request_items.index')->with(['requestItems'=>$requestItems]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = RequestCategory::select('id', 'name')->get();
        return view('request_items.create')->with(['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'id_fin_request_category' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            RequestItem::create([
                'name' => $request->get('name'),
                'id_fin_request_category' => $request->get('id_fin_request_category'),
                'description' => $request->get('description'),
                'cost_type'  => $request->get('cost_type'),
                'unit_price'  => $request->get('unit_price'),
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id
            ]);

            return redirect('requestItems');

        }catch (\Exception $ex){
            return back()->withErrors($ex)->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
