<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lead;

use DB;

class LeadController extends Controller
{
    public function __construct()
    {
    }
    public function index(Request $request)
	{
        $leads = DB::SELECT("SELECT id,
                status,
                person_name ,
                gender,
                organization_name,
                source,
                email_address,
                lead_owner,
                next_contact_date,
                next_contact_by,
                phone,
                salutation,
                mobile_no,
                fax,
                website,
                territory,
                lead_type,
                market_segment,
                industry,
                request_type,
                (SELECT email FROM hr_employee WHERE id = lead_owner) as lead_owner FROM leads");
        // dd($leads);
	    return view('erp.crm.leads', compact('leads'));
	}

	public function create(Request $request)
	{
	    $lead_owners = DB::select("SELECT * FROM hr_employee");
	    return view('erp.crm.add_lead', [[]], compact('lead_owners'));

	}

	public function edit(Request $request, $id)
	{
		$lead = Lead::findOrFail($id);
        $lead_owners = DB::select("SELECT * FROM hr_employee");

        return view('erp.crm.add_lead', [
	        'model' => $lead, 'lead_owners' => $lead_owners]);
	}

	public function show(Request $request, $id)
	{
        $lead = DB::SELECT("SELECT id,
                status,
                person_name ,
                gender,
                organization_name,
                source,
                email_address,
                lead_owner,
                next_contact_date,
                next_contact_by,
                phone,
                salutation,
                mobile_no,
                fax,
                website,
                territory,
                lead_type,
                market_segment,
                industry,
                request_type,
                (SELECT email FROM hr_employee WHERE id = lead_owner) as lead_owner FROM leads WHERE id =".$id);
	    return view('lead.show', [
	        'model' => $lead	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM leads a ";
		if($_GET['search']['value']) {
			$presql .= " WHERE status LIKE '%".$_GET['search']['value']."%' ";
		}

		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    $this->validate($request, [
	        'status' => 'required',
	        'person_name' => 'required',
	        'gender' => 'required',
	        'organization_name' => 'required',
	        'source' => 'required',
	        'email_address' => 'required',
	        'lead_owner' => 'required',
	        'next_contact_by' => 'required',
	        'lead_type' => 'required',
	        'industry' => 'required',
	        'request_type' => 'required'
	    ]);
		$lead = null;
		if($request->id > 0) {

		    $lead = Lead::findOrFail($request->id);
		    $lead->update([
                'status' => $request->status,
                'person_name' => $request->person_name,
                'gender' => $request->input('gender'),
                'organization_name' => $request->organization_name,
                'source' => $request->source,
                'email_address' => $request->email_address,
                'lead_owner' => $request->lead_owner,
                'next_contact_date' => $request->next_contact_date,
                'next_contact_by' => $request->next_contact_by,
                'phone' => $request->phone,
                'salutation' => $request->salutation,
                'mobile_no' => $request->mobile_no,
                'fax' => $request->fax,
                'website' => $request->website,
                'territory' => $request->territory,
                'lead_type' => $request->lead_type,
                'market_segment' => $request->market_segment,
                'industry' => $request->industry,
                'request_type' => $request->request_type,
            ]);
		    return back()->with('success', 'Lead Updated Successfully');
		}
		else { 
			$lead = new Lead;
		}
			    $lead->id = $request->id?:0;
				
	    		
					    $lead->status = $request->status;
		
	    		
					    $lead->person_name = $request->person_name;
		
	    		
					    $lead->gender = $request->gender;
		
	    		
					    $lead->organization_name = $request->organization_name;
		
	    		
					    $lead->source = $request->source;
		
	    		
					    $lead->email_address = $request->email_address;
		
	    		
					    $lead->lead_owner = $request->lead_owner;
		
	    		
					    $lead->next_contact_date = $request->next_contact_date;
		
	    		
					    $lead->next_contact_by = $request->next_contact_by;
		
	    		
					    $lead->phone = $request->phone;
		
	    		
					    $lead->salutation = $request->salutation;
		
	    		
					    $lead->mobile_no = $request->mobile_no;
		
	    		
					    $lead->fax = $request->fax;
		
	    		
					    $lead->website = $request->website;
		
	    		
					    $lead->territory = $request->territory;
		
	    		
					    $lead->lead_type = $request->lead_type;
		
	    		
					    $lead->market_segment = $request->market_segment;
		
	    		
					    $lead->industry = $request->industry;
		
	    		
					    $lead->request_type = $request->request_type;
	    $lead->save();

	    return redirect('/lead')->with('success', 'New Lead Added');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$lead = Lead::findOrFail($id);

		$lead->delete();
		return back()->with('success', 'Lead Trashed Successfully!');
	}

	
}