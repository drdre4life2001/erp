<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Country;
use App\Models\Department;
use App\Models\Employee;
use App\Models\EmployeeAttachment;
use App\Models\EmployeeDepartment;
use App\Models\EmployeeExperience;
use App\Models\EmployeeJob;
use App\Models\EmployeeReferee;
use App\Models\Gender;
use App\Models\Grade;
use App\Models\Institution;
use App\Models\InstitutionDepartment;
use App\Models\Job;
use App\Models\LineManager;
use App\Models\Organization;
use App\Models\Roles;
use App\Models\MenuGroup;
use Illuminate\Support\Facades\Session;
use App\Models\WorkAddress;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Libraries\Utilities;
use App\Models\Module;
use App\Models\OtherExpense;



class EmployeeController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    //employee done
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function quarter($ts) {
        return ceil(date('n', $ts)/3);
    }

    public function index()
    {
        $employee = Employee::where('company_id', auth()->user()->company->id)->with('workAddress', 'grade', 'gender')->orderBy('created_at', 'DESC')->get();
        return view('employee.index')->with(['employees' => $employee]);
    }

    public function employeeAjax(Request $request)
    {
        $data = [];
        $employees = [];


        if($request->has('q')){
            $search = $request->q;
            $company = auth()->user()->company->id;
            $data = DB::SELECT("SELECT o.name as organization, o.id as o_id, e.first_name as first_name, e.id as id,
                e.lastname as lastname, e.company_id as parent_company_id from sys_organizations as o,
                hr_employee as e where o.id = e.id_hr_company AND e.company_id = '$company' AND e.first_name LIKE '%$search%'");
        }

        return response()->json($data);
    }

    public function workAddressAjax(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $company = auth()->user()->company->id;
            $data = DB::table("hr_work_addresses")
                ->select("id","name", "website")
                ->where('company_id', "$company")
                ->where('name','LIKE',"%$search%")
                ->get();
        }

        return response()->json($data);
    }

    public function jobLocationAjax(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $company = auth()->user()->company->id;
            $data = DB::SELECT("select id, `name`, `code` from hr_job_title where advance_company_id = '3' and name like '%$search%'");
        }

        return response()->json($data);
    }


    public function update_bio_data(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'account_no' => 'required|max:255',
                'date_of_birth' => 'required|max:255',
                'home_address' => 'required|max:255',
                'first_name' => 'required|max:255',
                'lastname' => 'required|max:255',
                'date_first_hired' => 'required|max:255',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $employee = Employee::find($request->employee_id);
            $employee->identification_number = $request->get('identification_number');
            $employee->account_no = $request->get('account_no');
            $employee->date_of_birth = $request->get('date_of_birth');
            $employee->home_address = $request->get('home_address');
            $employee->first_name = $request->get('first_name');
            $employee->email = $request->get('email');
            $employee->phone = $request->get('phone');
            $employee->lastname = $request->get('lastname');
            $employee->other_name = $request->get('other_name');
            $employee->passport_no = $request->get('passport_no');
            $employee->city_of_birth = $request->get('city_of_birth');
            $employee->disability = $request->get('disability');
            $employee->permanent_address = $request->get('permanent_address');
            $employee->date_first_hired = $request->get('date_first_hired');
            $employee->updated_by = Auth::user()->id;
            $employee->save();
            return back()->with('success', 'Data Uploaded!');

        } catch (\Exception $ex) {
            return back()->with('error', 'An error occured : '.$ex->getMessage())->withInput();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //get jobs
        $jobs = Job::select('id', 'name', 'code')->where(['advance_company_id' => auth()->user()->company->id, 'advance_company_uri' => auth()->user()->company->uri])->get();//get jobs
        $departments = Department::select('id', 'name', 'code')->where('advance_company_id' , auth()->user()->company->id)->get();//get departments
        $banks = Bank::select('id', 'name')->get();
        $grades = Grade::select('id', 'name', 'code')->get();//get grades
        $gender = Gender::select('id', 'name')->get();//get gender
        $countries = Country::select('id', 'name')->get();//get countries
        $workAddresses = WorkAddress::where(['company_id' => auth()->user()->company->id, 'company_uri' => auth()->user()->company->uri])->select('id', 'name')->get();
        $employees = Employee::select('id', 'first_name', 'lastname')->get();
        $roles = Roles::where(['advance_company_id' => auth()->user()->company->id, 'advance_company_uri' => auth()->user()->company->uri])->select('id', 'name')->get();
        $organizations = Organization::select('id', 'name')->get();


        return view('employee.create')->with(['jobs' => $jobs,
            'departments' => $departments, 'banks' => $banks,
            'grades' => $grades, 'gender' => $gender,
            'countries' => $countries, 'workAddresses' => $workAddresses, 'employees' => $employees, 'roles' => $roles,
            'organizations' => $organizations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        try {
           //
            $validator = Validator::make($request->all(), [
                'identification_number' => 'required|max:255|unique:hr_employee',
                'account_no' => 'required|max:255',
                'date_of_birth' => 'required|max:255',
                'home_address' => 'required|max:255',
                'email' => 'required|max:255|unique:hr_employee',
                'phone' => 'required|max:255',
                'first_name' => 'required|max:255',
                'lastname' => 'required|max:255',
                'date_first_hired' => 'required|max:255',
                'id_hr_work_addresses' => 'required|max:255',
                'code' => 'required|max:255',
                'id_hr_grade' => 'required|max:255',
                'code_sys_country' => 'required|max:255',
                'id_sys_departments' => 'required|max:255',
                'id_hr_job_title' => 'required|max:255',
                'role' => 'required',
                'id_hr_company' => 'required'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $employee = Employee::create([
                'identification_number' => $request->get('identification_number'),
                'account_no' => $request->get('account_no'),
                'date_of_birth' => $request->get('date_of_birth'),
                'home_address' => $request->get('home_address'),
                'first_name' => $request->get('first_name'),
                'lastname' => $request->get('lastname'),
                'other_name' => $request->get('other_name'),
                'phone' => $request->get('phone'),
                'email' => $request->get('email'),
                'passport_no' => $request->get('passport_no'),
                'city_of_birth' => $request->get('city_of_birth'),
                'disability' => $request->get('disability'),
                'permanent_address' => $request->get('permanent_address'),
                'no_of_children' => $request->get('no_of_children'),
                'date_first_hired' => $request->get('date_first_hired'),
                'id_hr_work_addresses' => $request->get('id_hr_work_addresses'),
                'id_sys_banks' => $request->get('id_sys_banks'),
                'code' => $request->get('code'),
                'id_hr_grade' => $request->get('id_hr_grade'),
                'code_sys_country' => $request->get('code_sys_country'),
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
                'id_hr_company' => $request->get('id_hr_company'),
                'company_uri' => auth()->user()->company->uri,
                'company_id' => auth()->user()->company->id
            ]);


            $time = new Carbon();
            $time = $time->timestamp;

            //assign department
            EmployeeDepartment::create([
                'start_date' => $time,
                'note' => $request->get('department_note'),
                'id_hr_employee' => $employee->id,
                'id_sys_departments' => $request->get('id_sys_departments'),
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
                'company_id' => Auth::user()->company->id,
                'company_uri' => Auth::user()->company->uri,
            ]);

            //assign  job
            EmployeeJob::create([
                'start_date' => $time,
                'note' => $request->get('job_note'),
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
                'id_hr_employee' => $employee->id,
                'id_hr_job_title' => $request->get('id_hr_job_title'),
                'company_id' => Auth::user()->company->id,
                'company_uri' => Auth::user()->company->uri,
            ]);

            if(isset($request->line_manager)){
                //assign him as a line manager
                LineManager::create([
                    'parent_employee_id' => $request->get('line_manager'), //line manager
                    'child_employee_id' => $employee->id,
                    'length' => 0,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id,
                    'company_id' => Auth::user()->company->id,
                    'company_uri' => Auth::user()->company->uri,
                ]);
            }else{
                //assign him as a manager of himself
                LineManager::create([
                    'parent_employee_id' => $employee->id, //line manager
                    'child_employee_id' => $employee->id,
                    'length' => 0,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id,
                    'company_id' => Auth::user()->company->id,
                    'company_uri' => Auth::user()->company->uri,
                ]);
            }

            //Create user account and send email to notify him
            $generatedPassword  = Str::random('10');
            $hashedToken = bcrypt($generatedPassword);
            $time = new Carbon();
            $time = $time->timestamp;

            $email = $request->get('email');
            $la_user = $employee->lastname.' '.$employee->first_name;

            User::create([
                'email' => $request->get('email'),
                'id_hr_employee' => $employee->id,
                'role' => $request->get('role'),
                'confirmation_code'=>bcrypt($generatedPassword),
                'code_generation_time' => $time,
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id,
                'company_id' => auth()->user()->company->id,
                'company_uri' => auth()->user()->company->uri
            ]);

            Mail::send('auth.emails.user_confirmation', ['confirmation_code' => $generatedPassword, 'email'=>$email], function ($message) use ($email,$la_user){
                $message->to($email, $la_user);
                $message->from('admin@taerp.com', 'Admin');
                $message->subject('Employee Account Details');
            });
            return redirect('employee')->with('success', 'Employee Created!');

        } catch (\Exception $ex) {
            return back()->with('error', 'An error occured : '.$ex->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $company_uri)
    {
        try {
            $company = Utilities::checkCompanyExist($company_uri);
            $employee = Employee::find($id);
            if (empty($employee)) {
                return "Requested resource not found";
            }

            $employee->load(['bank', 'country', 'grade', 'workAddress', 'gender', 'employeeReferees' => function ($query) {
                $query->where('deleted', 0);
            }, 'employeeExperiences' => function ($query) {
                $query->where('deleted', 0);
            },
                'employeeAttachments' => function ($query) {
                    $query->where('deleted', 0);
                },
                'jobTitles'=> function ($query) {
                    $query->where('active', 1);
                }, 'employeeDepartments.department', 'managers' => function ($query) {
                    $query->where('length', 1);
                    $query->where('active', 1);
                    $query->with('managerInformation');
                },
                'employeeEducation' => function ($query) {
                    $query->with('institution');
                    $query->with('institutionDepartment');
                }
            ]);
            $managers = DB::select("select a.*, b.* from hr_line_manager as a, hr_employee as b where a.parent_employee_id = b.id and child_employee_id = $id");
            $allManagers = DB::select("select * from hr_employee where id != $id");
            $companies = DB::select("select a.name as company from sys_organizations as a, hr_employee as b where a.id = b.id_hr_company
            and b.id = '$id'");
            //get Jobs
            $jobs = Job::select('id', 'name', 'code')->orderBy('created_at')->get();//get jobs
            $departments = Department::select('id', 'name', 'code')->get();//get departments

            $institutions = Institution::select('id', 'name')->get();
            $institutionDepartments = InstitutionDepartment::select('id', 'name')->get();
            $banks = Bank::select('id', 'name')->get();
            $grades = Grade::select('id', 'name', 'code')->get();//get grades
            $gender = Gender::select('id', 'name')->get();//get gender
            $countries = Country::select('id', 'name')->get();//get countries
            $workAddresses = WorkAddress::select('id', 'name')->get();
            return view('employee.show')->with(['employee' => $employee, 'jobs' => $jobs,
                'departments' => $departments, 'managers' => $managers, 'allManagers' => $allManagers, 'banks' => $banks, 'grades' => $grades,
                'gender' => $gender, 'countries' => $countries, 'workAddresses' => $workAddresses,
                'institutions' => $institutions, 'institutionDepartments' => $institutionDepartments, 'companies' => $companies
            ]);
        } catch (\Exception $ex) {
            return back()->with('error', 'An error occured : '.$ex->getMessage())->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function assignLineManager(Request $request)
    {
        $line_mgr = LineManager::where('child_employee_id', $request->employee_id)->get();
        $mgr = LineManager::find($line_mgr->id);
        $mgr->parent_employee_id = $request->parent_employee_id;
        if($mgr->save()){
            return back()->with('success','Line Manager Assigned');
        }else{
            return back()->with('error', 'Could not assign Line Manager');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function change_picture(Request $request)
    {
        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            $file_extension = $file->getClientOriginalExtension();
            $file_size = $file->getSize();
            if ($file_size < 200000 && $file_extension === 'png' || $file_extension === 'jpg' || $file_extension === 'jpeg') {
                $new_name = base64_encode(date('Y-m-d H:i:s') . $file->getClientOriginalName()) . '.' . $file_extension;
                $destinationPath = 'uploads/employee_pictures';
                $file->move($destinationPath, $new_name);
                $employee = Employee::find($request->employee_id);
                if (isset($employee->picture)) {
                    Storage::delete($employee->picture);
                }
                $employee->picture = $destinationPath . '/' . $new_name;
                $employee->save();
                return back()->with('success', 'Employee Picture Updated!');
            } else {
                return back()->with('error', 'Error! It is either your File is too large! Your file must not be bigger then 100KB or You are uploading the wrong file exception');
            }
        } else {
            return back()->withErrors('Please upload a file');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function download_attachment($id)
    {
        $file = EmployeeAttachment::find($id);
        $name = $file->url;
        return response()->download($name);
    }

    public function save_attachment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'attachment' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
            $file_name = $file->getClientOriginalName();
            $file_extension = $file->getClientOriginalExtension();
            $file_path = $file->getRealPath();
            $file_size = $file->getSize();
            $file_type = $file->getMimeType();
            if ($file_size < 200000) {
                $new_name = base64_encode(date('Y-m-d H:i:s') . $file->getClientOriginalName()) . '.' . $file_extension;
                $destinationPath = 'uploads/attachments';
                $file->move($destinationPath, $new_name);
                $attachment = new EmployeeAttachment();
                $attachment->name = $request->title;
                $attachment->description = $request->description;
                $attachment->url = $destinationPath . '/' . $new_name;
                $attachment->id_hr_employee = $request->id_hr_employee;
                $attachment->save();
                return back()->with('success', 'File uploaded successfully');
            } else {
                return back()->with('error', 'File is too large! Your file must not be bigger then 100KB');
            }
        } else {
            return back()->with('error', 'Please upload a file');
        }
    }

    public function delete_experience($id)
    {
        $experience = EmployeeExperience::find($id);
        $experience->deleted = 1;
        $experience->deleted_at = date('Y-m-d H:i:s');
        $experience->save();
        return back()->with('success', 'Record Deleted');
    }

    public function delete_referee($id)
    {
        $experience = EmployeeReferee::find($id);
        $experience->deleted = 1;
        $experience->deleted_at = date('Y-m-d H:i:s');
        $experience->save();
        return back()->with('success', 'Record Deleted');
    }

    public function delete_attachment($id)
    {
        $experience = EmployeeAttachment::find($id);
        $experience->deleted = 1;
        $experience->deleted_at = date('Y-m-d H:i:s');
        $experience->save();
        return back()->with('success', 'Record Deleted');
    }

    public function save_experience(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'organization_name' => 'required|max:255',
                'last_manager' => 'required|max:255',
                'designation' => 'required|max:255',
                'department' => 'required|max:255',
                'work_start_date' => 'required|max:255',
                'work_end_date' => 'required|max:255',
                'id_hr_employee' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $time = new Carbon();
            $time = $time->timestamp;

            $employee = Employee::find($request->get('id_hr_employee'));
            if (empty($employee)) {
                return back()->withErrors("Data not saved! Employee Record not found")->withInput();
            }

            EmployeeExperience::create([
                'organization_name' => $request->get('organization_name'),
                'last_manager' => $request->get('last_manager'),
                'designation' => $request->get('designation'),
                'department' => $request->get('department'),
                'id_hr_employee' => $request->get('id_hr_employee'),
                'work_start_date' => $request->get('work_start_date'),
                'work_end_date' => $request->get('work_end_date'),
                'created_by' => Auth::user()->id
            ]);

            return back()->with('success', 'Employee record changed succesfully');

        } catch (\Exception $ex) {
            return back()->withErrors($ex->getMessage())->withInput();
        }
    }

    public function save_referee(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'firstname' => 'required|max:255',
                'lastname' => 'required|max:255',
                'address' => 'required|max:255',
                'phone' => 'required|max:255',
                'relationship' => 'required|max:255',
                'id_hr_employee' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $time = new Carbon();
            $time = $time->timestamp;

            $employee = Employee::find($request->get('id_hr_employee'));
            if (empty($employee)) {
                return back()->withErrors("Data not saved! Employee Record not found")->withInput();
            }

            EmployeeReferee::create([
                'firstname' => $request->get('firstname'),
                'lastname' => $request->get('lastname'),
                'address' => $request->get('address'),
                'phone' => $request->get('phone'),
                'relationship' => $request->get('relationship'),
                'id_hr_employee' => $request->get('id_hr_employee'),
                'created_by' => Auth::user()->id
            ]);

            return back()->with('success', 'Employee record changed succesfully');

        } catch (\Exception $ex) {
            return back()->withErrors($ex->getMessage())->withInput();
        }
    }

     public function dashboard(){

        try{
            //get role and permissions
            $user = User::with('assignedRole.rolePermissions.permission')->where('id', Auth::user()->id)->first();
            $userPermissions = array();
            $userRole = isset($user->assignedRole->slug) ? $user->assignedRole->slug : null;
            if(isset($user->assignedRole)){
                foreach($user->assignedRole->rolePermissions as $rolePermission){
                    $userPermissions[$rolePermission->permission->slug] = $rolePermission->level;
                }
            }
            //get accessible menus
            $menuGroups = MenuGroup::with('menus.permission')->get();
            $permittedMenuGroups = array();
            foreach($menuGroups as $menuGroup){
                $permittedMenuGroups[$menuGroup->id]['name'] = $menuGroup->name;
                $permittedMenus = array();
                foreach($menuGroup->menus as $menu){
                    if(isset($userPermissions[$menu->permission->slug])){
                        $permittedMenus[] = ['name'=>$menu->name, 'url'=>$menu->url];
                    }
                }
                $permittedMenuGroups[$menuGroup->id]['menus'] = $permittedMenus;
            }
            //get employee details: department, organization, name
            $userDepartments = array();
            $userOrganization = array();
            $userDetails = null;
            $employee = Employee::find(Auth::user()->id_hr_employee);

            if(!empty($employee)){
                $userDetails = $employee;
                $employee->load(['employeeDepartments' => function($query){
                    $query->where('end_date', null);
                    $query->with('department.organization');
                }]);

                foreach($employee->employeeDepartments as $employeeDepartment){
                    //get the department user belongs to
                    $userDepartments[$employeeDepartment->id] = isset($employeeDepartment->department->name)? $employeeDepartment->department->name : null;
                    //get corresponding organization user belongs to
                    if(isset($employeeDepartment->department->id_sys_organizations)){
                        $userOrganization[$employeeDepartment->department->id_sys_organizations] = isset($employeeDepartment->department->organization->name) ? $employeeDepartment->department->organization->name : null;
                    }
                }
            }

            //set ability and role
            Session::put('abilities', $userPermissions);
            Session::put('role', $userRole);
            Session::put('menuGroups',$permittedMenuGroups);//set accessible menus
            Session::put('department', $userDepartments);//sets the department(s) the user belongs to
            Session::put('organization', $userOrganization);//set the organization(s) the user belongs to
            Session::put('userInfo', $userDetails); //sets user employee details
            $qtr = self::quarter(strtotime(date('Y-m-d H:i:s')));
            $utilities = new Utilities();
            $quarter = $utilities->createFullWordOrdinal($qtr);
            $expense = OtherExpense::where('deleted_at', NULL)->paginate(5);

            //Get menu links for this user
            $id_hr_employee= Auth::user()->id_hr_employee;
            $modules = Module::orderBy('created_at')->get();
            $utilities = new Utilities();
            $total_payables =  $utilities->totalPayables();
            $total_expenses =  $utilities->totalExpenses();
            auth()->user()->role != "7" ? $other_expenses = Utilities::myExpenses() : $other_expenses = Utilities::allExpenses();
            return view('dashboard.employee_dashboard')->with(['modules'=>$modules, 'quarter' => $quarter, 'expense' => $expense, 'total_payables' => $total_payables, 'total_expenses' => $total_expenses, 'other_expenses' => $other_expenses]);
        }catch(\Exception $ex){
            return back()->with('error', 'An error occured : '.$ex->getMessage())->withInput();
        }
    }

}
