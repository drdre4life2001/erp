<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Organization;
use App\Models\Pipeline;
use App\Models\Prospect;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Libraries\Utilities;

class CRMController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth');

    }
    public function index(){
        $user_id = Auth::user()->id_hr_employee;
        //Fetch prospects' statuses
        $status = DB::select("select * from prospects_status where id != 1"); //Don't pick 'initiated' since prospect is already initiated during creation
        //If user is a c level staff
        $grade = DB::select("select * from hr_employee where id_hr_grade = 4 and id = '$user_id'");
        $pipeline_status = Utilities::crmPipeline();
        if(count($grade) == 1){
            //if c level, let him see all prospects and remove 'add activity' button
            $item = DB::select("select contact_person, contact_position, p.phone as contact_phone, p.email as contact_email, p.revenue as contact_revenue, p.id as id, p.name as prospect, p.created_at as created_at, b.id as status_id,  b.definition as status, 
            c.first_name as firstname, c.lastname as lastname, d.name as subsidiary from prospects as p, prospects_status as b, 
            hr_employee as c, sys_organizations as d  where p.subsidiary_id = d.id and p.status = b.id and p.created_by = 
            c.id order by p.created_at desc");
        }else{
            //else only show prospects created by this user
            $item = DB::select("select contact_person, contact_position, p.phone as contact_phone, p.email as contact_email, p.revenue as contact_revenue, p.id as id, p.name as prospect, p.created_at as created_at, b.id as status_id, b.definition as status, 
            c.first_name as firstname, c.lastname as lastname, d.name as subsidiary from prospects as p, prospects_status as b, 
            hr_employee as c, sys_organizations as d where p.subsidiary_id = d.id and p.status = b.id and p.created_by = 
            c.id and p.created_by = '$user_id' order by p.created_at desc");
        }
        return view('crm.index', compact('item', 'status', 'pipeline_status'));
    }
    public function create(Request $request){
        $organizations = Organization::all();
        $user_id = Auth::user()->id_hr_employee;
        $method = $request->isMethod('post');
        $status = DB::select("select * from prospects_status where id != 1"); //Don't pick 'initiated' since prospect is already initiated during creation
        $pipeline_status = DB::select("select * from pipeline_status where id != 1");
        $item = Utilities::crm_prospects();
        if($method){
            // dd($request->all());
            //Process the creation of prospects
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'address' => 'required',
                'phone' => 'required',
                'code' => 'required',
                'subsidiary' => 'required',
                'contact_person' => 'required',
                'contact_position' => 'required',
                'revenue' => 'required'

            ]);
            if($validator->fails()){
                return back()->with('error', $validator);
            }
            $create = Prospect::create([
               'name' => $request->name,
               'address' => $request->address,
               'email' => $request->email,
               'phone' => $request->phone,
               'code' =>  $request->code,
                'contact_person' => $request->contact_person,
                'contact_position' => $request->contact_position,
                'revenue' => $request->revenue,
                'subsidiary_id' => $request->subsidiary,
                'created_by' => $user_id,
                'status' => 1 //Default value for newly created prospect (Get value from prospects_status table)
            ]);
            if($create){
                return back()->with('success', 'Prospect Added!');
            }else{
            }
        }else{
            return view('erp.crm.prospects', compact('organizations', 'item', 'pipeline_status', 'status'));
        }
    }
    public function store_activity(Request $request){
        //Add activity of this prospect to the pipelines table
        $validator = Validator::make($request->all(), [
            'prospect_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'activity_date' => 'required|date',
            'status' => 'required'
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $activity_date = date('Y-m-d', strtotime($request->activity_date));
        $user_id = Auth::user()->id_hr_employee;
            //Add to pipelines table
            $pipeline = Pipeline::create([
                'prospect_id' => $request->prospect_id,
                'title' => $request->title,
                'description' => $request->description,
                'user_id' => $user_id,
                'activity_date' => $activity_date,
                'status' => $request->status
            ]);
            if ($pipeline){
                return redirect()->route('view_activity', ['id' => $request->prospect_id]);
            }


    }
    public  function view_activity($id){
        //Show activity log for this prospect
        //Fetch Pipeline status
        $pipeline_status = DB::select("select * from pipeline_status where (id != 1 and id !=2)");
        //Select start date for this prospect (prospects table) + pipelines for this prospect (pipelines table)
        $pipelines = DB::select("select b.status as pipeline_status, b.prospect_id as prospect_id, b.id as id, b.title as title, b.description as description,
        b.activity_date as activity_date, c.definition as status from pipelines as b, pipeline_status as c
        where b.status = c.id and b.prospect_id = '$id' order by activity_date asc");
        if(empty($pipelines))
            return back()->with('error', 'No activity found for this Prospect!');
        return view('erp.crm.view_activity', compact('pipelines', 'pipeline_status'));
    }

    public function pipelines(){
        $pipelines =DB::select("select a.name as prospect, b.title as title, b.description as description, b.activity_date as 
        activity_date, c.definition as status from prospects as a, pipelines as b, prospects_status as c where
        a.id = b.prospect_id and b.status = c.id 
        ORDER by b.prospect_id");
        // dd($item);
        return view('erp.crm.pipelines', compact('pipelines'));
    }
    public function by_user(){
        $items = DB::select("select count(a.id) as num, b.first_name as firstname, b.lastname as lastname from 
        prospects as a, hr_employee as b where a.created_by = b.id group by a.created_by");
        foreach ($items as $item){
            $employee[] = $item->firstname.' '.$item->lastname;
            $count[] = $item->num;
        }
        $data = ['employee' => $employee, 'count' => $count];
        return view('erp.crm.report_by_user')->with('data', json_encode($data));
    }
    public function update_prospect(Request $request){
        //Update status for this prospect
        $status = $request->status;
        $item_id = $request->item_id;
        $update = DB::table('prospects')->where('id', $item_id)->update(['status' => $status]);
        //If status = 8 i.e. executed agreement, add as client
        if($status == 9){ //If status == closed (3), add prospect as client
            //fetch all details for this prospect
            $sql = DB::select("select * from prospects where id = '$request->item_id'");
            foreach ($sql as $sql){
                $name = $sql->name;
                $address = $sql->address;
                $phone = $sql->phone;
                $created_by = $sql->created_by;
            }

            $client = new Client();
            $client->prospect_id = $request->item_id;
            $client->name = $name;
            $client->address = $address;
            $client->phone = $phone;
            $client->created_by = $created_by;
            $client->save();

            return back()->with('success', 'Congratulations! Prospect is now a client!');
        }else{
            return back()->with('success', 'Prospect status updated!');
        }
    }
    public function pipeline_update(Request $request){
        //Update status of pipeline to completed
        $pipeline_id = $request->pipeline_id;
        $prospect_id = $request->prospect_id;

        $update = DB::table('pipelines')->where('id', $pipeline_id)->update(['status' => 3]);
        return redirect()->route('view_activity', ['id' => $prospect_id]);

    }

}
