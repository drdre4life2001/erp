<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\MenuGroup;
use App\Models\Permissions;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menus = Menu::with('menuGroup', 'permission')->orderBy('created_at', 'DESC')->get();
        return view('menus.index')->with(['menus'=>$menus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $menuGroups = MenuGroup::select('id', 'name')->orderBy('name')->get(); //get menu groups
        $permissions = Permissions::select('id', 'name', 'slug')->get(); //get all permissions
        $levels = ['all' => 'All', 'organization' => 'Organization', 'department' => 'Department', 'self' => 'Individual'];
        return view('menus.create')->with(['menuGroups'=>$menuGroups, 'permissions'=>$permissions, 'levels'=>$levels]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'url'=> 'required|max:255',
                'menu_group'=> 'required',
                'permission'=> 'required',
                'level' => 'required',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            Menu::create([
                'name' => $request->get('name'),
                'url'=> $request->get('url'),
                'id_system_menu_group'=> $request->get('menu_group'),
                'id_system_permission'=> $request->get('permission'),
                'level'=> $request->get('level'),
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id
            ]);

            return redirect('menus');

        }catch (\Exception $ex){
            //return $ex->getMessage();
            return back()->withErrors($ex->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
