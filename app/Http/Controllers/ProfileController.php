<?php

namespace App\Http\Controllers;

use App\Models\JiraCredential;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Hash;

class ProfileController extends Controller
{
    //
    public function __construct(){
        return $this->middleware('auth');
    }

    public function index(){
        $user_id = Auth::user()->id_hr_employee;
        $items = DB::select("select email, date_of_birth, date_first_hired, b.name as department, first_name, lastname, permanent_address from hr_employee as a,
         sys_departments as b, hr_employee_departments as c where a.id = c.id_hr_employee and b.id = c.id_hr_employee and 
        a.id = '$user_id'");
        $details = DB::select("select id, username from jira_credentials where user_id = '$user_id'");
        return view('profile', compact('details', 'items'));
    }
    public function add_jira(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $user_id = Auth::user()->id_hr_employee;
        //Insert into jira credentials table
        $details = DB::select("select id, username from jira_credentials where user_id = '$user_id'");
        if(count($details == 0)){
            $create = JiraCredential::create([
                'user_id' => $user_id,
                'username' => $request->username,
                'password' => encrypt($request->password)
            ]);
            if($create){
                return redirect()->route('profile')->with('status', 'Jira Credentials Added');
            }
        }else{
            return back();
        }
    }
    public function viewProfile(Request $request){

        $method = $request->isMethod('post');
        switch ($method) {
            case true:
                $user = User::where('id', auth()->user()->id)->first();
                $this->validate($request, [
                    'password' => 'required|min:6',
                    'confirm_password' => 'required|min:6'
                ]);
                if($request->password !== $request->confirm_password)
                    return back()->with('error', 'Confirm password and password not match!');
                $update = DB::table('sys_user')->where('id', auth()->user()->id)
                    ->update([
                        'password' => Hash::make($request->password)
                    ]);
                if($update){
                    return back()->with('success', 'Password updated!');
                }
                break;

            case false:
                return view('users.profile');
                break;

            default:
                break;
        }
    }
    //Update JIRA credentials
    public function update_jira(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
                $update = DB::table('jira_credentials')->where('id', $request->user_id)
                    ->update([
                        'username' => $request->username,
                        'password' => encrypt($request->password)
                    ]);
                if($update){
                    return redirect()->route('profile')->with('status', 'Jira Credentials Updated');
                }
    }
}
