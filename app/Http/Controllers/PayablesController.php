<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\RequestCategory;
use App\Models\RequestGroup;
use App\Models\RequestItem;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PayablesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requestGroups = RequestGroup::with('employee')->orderBy('created_at', 'desc')->get();
        return view('request_groups.index')->with(['requestGroups'=>$requestGroups]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees =  Employee::get();
        //should pull for only the user depending on the permission
        $categories = RequestCategory::with('items')->get();
        $items = DB::select("select * from fin_request_item");
        session(['categories' => $categories]);
        session(['items' => $items]);
        return view('request_groups.create')->with(['employees'=>$employees, 'items'=>$items]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $validator = Validator::make($request->all(), [
                'item' => 'required|max:255',
                'qty'=> 'required|max:255',
                'unit_price'=> 'required',
                'start_date'=>'required',
                'end_date'=>'required',
                'id_hr_employee' => 'required'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            //get employee
            $employeeId = $request->get('id_hr_employee');
            $employee = Employee::where('id',$employeeId)->first();

            if(empty($employee)){
                return back()->withErrors('Employee resource not found');
            }

            //get line manager
            $manager = $employee->load(['managers'=>function($query){
                    $query->where('length', 1);
                    $query->orderBy('created_at', 'DESC');
                   $query->limit(1);
                }]);

            if(empty($employee->managers)){
                echo "You have no line manager assigned. Contact your HR to assign a line manager";
                return;
            }

            if(!isset($employee->managers[0])){
                echo "You have no line manager assigned. Contact your HR to assign a line manager";
                return;
            }

            //get request item
            $requestItem = RequestItem::find($request->get('item'));

            if(empty($requestItem)){
                //item does not exist
                return "Item resource not found";
            }

            $qty = $request->get('qty');
            $statedPrice =  $request->get('unit_price');
            $totalStatedPrice = $statedPrice * $qty;
            $procurementPrice = $requestItem->unit_price;
            $calculatedTotalProcurementPrice = $procurementPrice * $qty;

            $requestGroup = RequestGroup::create([
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'id_hr_employee' => $employeeId,
                'expected_approval_user' => $employee->managers[0]->parent_employee_id,
                'remarks' => $request->get('remarks'),
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
                'validation_date'  => Auth::user()->id,
                'approving_date'  => Auth::user()->id,
                'total_amount_requesting' => 0,
                'status' => 0, //in progress
                'total_amount_paid' => 0,
                'total_amount_requesting' => $totalStatedPrice,
                'approving_user'
            ]);

            \App\Models\Requests::create([
                'id_fin_request_item' => $request->get('item'),
                'id_fin_request_group' => $requestGroup->id,
                'qty'  => $request->get('qty'),
                'unit_price' => $request->get('unit_price'),
                'total_price' => $totalStatedPrice,
                'procurement_unit_price' => $procurementPrice,
                'procurement_total_price' => $calculatedTotalProcurementPrice,
                'info' => $request->get('info'),
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id
            ]);

            return redirect('payables');

        }catch (\Exception $ex){
            return back()->withErrors($ex->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try{
            $requestGroup = RequestGroup::find($id);

            $requestGroup->load(['requests.item', 'employee', 'expectedApprovalUser']);

            if(empty($requestGroup)){
                return "Requested resource not found";
            }

            $items = RequestItem::all();
            return view('request_groups.show')->with(['requestGroup'=>$requestGroup, 'items'=>$items]);
        }catch (\Exception $ex){
            return $ex->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //submit request
    public function submitRequest(Request $request, $requestGroupId){
        try{
            //get $request

            $requestGroup = RequestGroup::find($requestGroupId);

            if(empty($requestGroup)){
                return "Request resource not found";
            }


            if($requestGroup->created_by != Auth::user()->id){
                return "You are not permitted to access this resource";
            }

            //check the status
            if($requestGroup->status != 0){
                return "This request has already been processed.";
            }

            $requestGroup->load(['requests.item', 'employee', 'expectedApprovalUser']);
            // send the line manager an email
            $lineManager = $requestGroup->expected_approval_user;
            $managerInfo = Employee::find($lineManager);

            if(!empty($managerInfo)){
                //get the managers email
                $managerAccount = User::where('id_hr_employee',$managerInfo->id)->first();
                //send the manager an email and notification
                if(!empty($managerAccount)){
                    //send user email
                    $managerEmail = $managerAccount->email;
                    $la_user = $managerInfo->lastname.' '.$managerInfo->first_name;
                    echo($managerEmail);
                    try{
                        Mail::send('auth.emails.approval_request', ['requestGroup' => $requestGroup], function ($message) use ($managerEmail,$la_user){
                            $message->to($managerEmail, $la_user);
                            $message->from('admin@taerp.com', 'Request Approval');
                            $message->subject('Request Approval');
                        });
                    }catch(\Exception $ex){
                        //unable to send the mail
                        return $ex->getMessage();
                    }
                }
            }

            $time = new Carbon();
            $time = $time->timestamp;

            //change the status from pending to submitted
            $requestGroup->status = 1; //submitted
            $requestGroup->validation_date = $time;
            $requestGroup->save();

            //redirect
            return back()->withErrors("Request Submitted successfully")->withInput();

            //check
        }catch (\Exception $ex){
            return back()->withErrors($ex->getMessage())->withInput();
        }
    }

    public function approveRequest(Request $request, $requestGroupId){
        try{
            //validate the person is  the expected user to approve
            //validate it hasn't been approved


        }catch(\Exception $ex){

        }
    }

    public function denyRequest(Request $request, $requestGroupId){

    }
}
