<?php

namespace App\Http\Controllers;

use App\Models\BankAccount;
use App\Models\Client;
use App\Models\Invoice;
use App\Models\InvoiceLog;
use App\Models\Organization;
use App\Models\RevenueStream;
use App\Models\Tax;
use App\Models\Bank;
use Illuminate\Http\Request;
use App\Models\Item;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\Facade as PDF;
use App\Models\Revenue;
use Illuminate\Support\Facades\Auth;
use App\Models\CompanyBank;
use App\Libraries\Utilities;
use Illuminate\Support\Facades\Mail;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
        $company_id = auth()->user()->company->id;
        //Fetch all invoices
        $details = DB::select("select e.name as status, a.id as id, c.name as client, due_date, d.name as revenue_stream_name, invoice_date, invoice_number, order_number, grand_total
        from sys_invoices as a, sys_clients as c, revenue_streams as d, sys_invoices_status as e
        where e.id = a.status and a.revenue_stream_id = d.id and d.client_id = c.id and a.advance_company_id = '$company_id' and a.deleted_at IS NULL order by a.created_at DESC");
                
        return view('invoice.index', compact('details'));
    }

    /** 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banks = Bank::all();
        $taxes = Tax::where(
            ['advance_company_id' => auth()->user()->company->id,
            'advance_company_uri' => auth()->user()->company->uri
            ])->get(); //Get all taxes
        $revenue_streams = RevenueStream::where(
                ['advance_company_id' => auth()->user()->company->id,
                'advance_company_uri' => auth()->user()->company->uri
                ])->get(); //get revenue streams

        $company_bank_account = BankAccount::where('advance_company_id' , auth()->user()->company->id)->first();
        return view('invoice.create', compact('revenue_streams', 'taxes', 'banks', 'company_bank_account'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //Do some validation
        $validator = Validator::make($request->all(), [
            'revenue_stream' => 'required',
            'invoice_date' => 'required|date',
            'due_date' => 'required|date',
            'invoice_number' => 'required',
            'order_number' => 'required'
        ]);

        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        //Do some validation for the items
        if($request->item){
            $input_items = $request->item;
            foreach ($input_items as $key => $item) {
                $price = $item['price'];
                $quantity =  $item['quantity'];
                $tax_id = $item['tax_id'];
                $name = $item['name'];
                if(!preg_match('/^\d*(\.\d{2})?$/', $price)){
                    return back()->with('error', 'Price must be in the right format')->withInput();
                }
                if(!is_numeric($quantity)){
                    return back()->with('error', 'Quantity must be numeric')->withInput();
                }
                if($tax_id == ""){
                    return back()->with('error','Tax is required')->withInput();
                }
                if($name == ""){
                    return back()->with('error','item name is required')->withInput();
                }
            }
        }
        $invoice_date = date('Y-m-d', strtotime($request->invoice_date));
        $due_date = date('Y-m-d', strtotime($request->due_date));
        //First insert into invoices table
        $invoice = Invoice::create([
            'revenue_stream_id' => $request->revenue_stream,
            'status' => 1, //default value = 1 i.e. pending
            'invoice_date' => $invoice_date,
            'due_date' => $due_date,
            'invoice_number' => $request->invoice_number,
            'order_number' => $request->order_number,
            'notes' => $request->notes,
            'advance_company_id' => auth()->user()->company->id,
            'advance_company_uri' => auth()->user()->company->uri
        ]);
        //Insert into items table and then update the sub-total, tax and grand-total for the invoice
        $sub_total = 0;
        $tax_total = 0;
        if($request->item) {
            $input_items = $request->item;
            $invoice_id = $invoice->id;
            foreach ($input_items as $key => $item) {
                $price = $item['price'];
                $quantity =  $item['quantity'];
                $tax_id = $item['tax_id'];
                $name = $item['name'];
                $item_sub_total = $quantity * $price;
                if (isset($item['tax_id'])) {
                    $tax = Tax::find($item['tax_id']);
                    $rate = isset($tax->rate) ? $tax->rate : 0;
                    $item_tax_total = (($price * $quantity) / 100) * $rate;
                }
                //Insert into items table
                Item::create([
                    'name' => $name,
                    'price' => $price,
                    'quantity' => $quantity,
                    'total' => $quantity * $price,
                    'tax_id' => $tax_id,
                    'invoice_id' => $invoice_id,
                    'advance_company_id' => auth()->user()->company->id,
                    'advance_company_uri' => auth()->user()->company->uri
                ]);
                $sub_total += $item_sub_total;
                $tax_total += $item_tax_total;
                $total = $item_sub_total + $item_tax_total;
            }
        }
        //Update sub-total, tax and grand-total for selected invoice
            DB::table('sys_invoices')->where('id', $invoice_id)
            ->update([
                'sub_total' => $sub_total,
                'tax' => $tax_total,
                'grand_total' => $tax_total + $sub_total,
                'balance' => $tax_total + $sub_total,
                'paid' => 0 //default value is zero
            ]);
        //Show generated invoice

        $company_bank_account = CompanyBank::where('advance_company_id' , auth()->user()->company->id)->first();
        return redirect()->route('finance.invoice.show', ['id' => $invoice->id, 'company_bank_account' => $company_bank_account])->with('success', 'New invoice has been generated for the revenue stream');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company_id = auth()->user()->company->id;

        //Get subsidiary, client, item details and invoice details.
        $banks = DB::select("select a.id as id, a.account_number as account_number, account_name, b.name as name from sys_bank_accounts as 
        a, sys_banks as b where a.advance_company_id = '$company_id' and a.bank_id = b.id and a.deleted_at IS NULL");
        // $details = DB::select("select a.revenue_stream_id, d.client_id, d.company_id, e.name as status, a.status as status_id, a.id as id, b.name as subsidiary, c.name as client, due_date, invoice_date, invoice_number, order_number,
        // sub_total, tax, grand_total, notes, balance, paid from sys_invoices as a, sys_organizations as b, sys_clients as c, revenue_streams as d, sys_invoices_status as e
        // where e.id = a.status and a.revenue_stream_id = d.id and d.client_id = c.id and d.company_id = b.id and a.id = '$id'");

        $details = DB::select("select e.name as status, a.revenue_stream_id, d.client_id, d.company_id, a.id as id, c.name as client,a.status as status_id, b.name as subsidiary, d.name as revenue_stream_name, invoice_date, invoice_number, order_number, sub_total, grand_total, due_date, tax, paid, balance, notes
        from sys_invoices as a, sys_clients as c, revenue_streams as d, sys_organizations as b, sys_invoices_status as e
        where e.id = a.status and a.revenue_stream_id = d.id and d.client_id = c.id and a.advance_company_id = '$company_id' and a.id = '$id' and a.deleted_at IS NULL order by a.created_at DESC limit 1");
        
        // dd($details);
        //Get items
        $items = DB::select("select * from sys_items where invoice_id = '$id'");
        $company_bank_account = BankAccount::with('bank')->where('advance_company_id' , auth()->user()->company->id)->first();
        return view('invoice.show', compact('details', 'items', 'banks', 'company_bank_account'));
    }
    public function add_payment(Request $request){
        //Validate all fields
    try{
        // dd($request->all());
        
        $balance = $request->balance;
        $amount = $request->amount;
        $client_id = $request->client_id;
        $revenue_stream_id = $request->revenue_stream_id;
        $subsidiary_id = $request->subsidiary_id;
        $payment_date = date('Y-m-d', strtotime($request->payment_date));
        $invoice_id = $request->invoice_id;
        $user_id = Auth::user()->id_hr_employee;

        $validation = Validator::make($request->all(), [
            'payment_date' => 'required|date',
            'amount' => 'required',
            'bank_account' => 'required'
        ]);
        if ($validation->fails()){
            return back()->withErrors($validation);
        }
        /**
        if(!preg_match('/^\d*(\.\d{2})?$/', $request->amount)){
            return back()->withErrors('Amount must be in the right format');
        }
         * **/
        if($amount > $balance){
            return back()->with('error', 'Amount can not be greater than the balance');
        }

        //Add to revenue table
        Revenue::create([
            'revenue_stream_id' => $revenue_stream_id,
            'client_id' => $client_id,
            'company_id' => $subsidiary_id,
            'date' => $payment_date,
            'total_collection' => 0,
            'total_commission' => $amount,
            'advance_company_uri' => auth()->user()->company->uri,
            'advance_company_id' => auth()->user()->company->id
        ]);
        //Update invoices table - paid, balance and status
        //First get balance and paid
        $invoice = Invoice::where('id', $invoice_id)->first();
        //$balance = $invoice->balance;
        $paid = $invoice->paid;
        $grand_total = $invoice->grand_total;
        $paid_update = $amount + $paid;
        $balance_now = $grand_total-$paid_update;
        //Get status
        if($balance_now != 0){
            $status = 2;
        }else{
            $status = 3;
        }
        DB::table('sys_invoices')->where('id', $invoice_id)
            ->update([
                'balance' => $balance_now,
                'paid' => $paid_update, //default value is zero
                'status' => $status,
                'advance_company_uri' => auth()->user()->company->uri,
                'advance_company_id' => auth()->user()->company->id
            ]);
        //Update bank account details
        //Get current balance and previous balance
        $bank_account = BankAccount::where('id', $request->bank_account)->first();
        //$prevBalance = $bank_account->prevBalance;
        $currentBalance = $bank_account->currentBalance;
        $currentBalanceNow = $currentBalance + $amount;

        $update_bank = DB::table('sys_bank_accounts')
            ->where(['id' => $request->bank_account,
             'advance_company_id' => auth()->user()->company->id,
              'advance_company_uri' =>auth()->user()->company->uri])
            ->update([
                'prevBalance' => $currentBalance,
                'currentBalance' => $currentBalanceNow
            ]);
        if($update_bank) {
            //Insert into the Bank Activity table
            Utilities::insBankActivity($request->bank_account, $amount, 1, $currentBalance, $currentBalanceNow);
            //1 == credit
            //Insert into the Ledger table
            Utilities::insLedger($request->bank_account, $amount, $currentBalance, $currentBalanceNow, 0,
                $request->invoice_id, 1, $user_id);
            //Insert into invoice log table
            InvoiceLog::create([
                'user_id' => $user_id,
                'invoice_id' => $invoice_id,
                'amount' => $amount,
                'prevBalance' => $currentBalance,
                'currentBalance' => $currentBalanceNow,
                'description' => $request->description
            ]);
            return back()->with('success', 'Invoice payment made successfully!');
        } else {
            return back()->with('error', 'Payment couldnt be completed due to bank account selected!');
        }  
        // return back()->with('error', 'Something went wrong payment was not successful!');
    } catch(\Exception $e) {
        // dd($e->getMessage());
        return back()->with('error', 'Something went wrong!');
    }

    }

    public function printInvoice($id, $company_uri){
 
            $details = $this->invoiceDetails($id);

            $items = $this->invoiceItems($id);

            $pdf = PDF::loadView('invoice.invoice', compact('details', 'items'));

            $date = new \DateTime('now', new \DateTimeZone('Africa/Lagos'));

            return $pdf->download('invoice_'.$company_uri.'/'.$date->format('d-m-Y H:i:s').'.pdf');

    }

    public function invoiceDetails($invoice_id){

        $details = DB::select("select a.id as id, c.name as client, due_date, invoice_date, invoice_number, order_number,
            sub_total, tax, grand_total, notes from sys_invoices as a,  revenue_streams as c
            where a.revenue_stream_id = c.id and a.id = '$invoice_id'");

        return $details;
    }

    public function invoiceItems($invoice_id){

        $items = DB::select("select * from sys_items where invoice_id = '$invoice_id'");

        return $items;
    }



    public function sendPdf(Request $request, $id){
        //Send mail to the client with invoice as an attachment
        $method = $request->isMethod('post');
        if($method){
            //Validate the email field
            $validator = Validator::make($request->all(), [
                'email' => 'required|email'
            ]);
            if($validator->fails()){
                return back()->with('error', 'Invalid email address.')->withInput();
            }
            //If email is valid, send mail to designated email.
            $details = DB::select("select a.id as id, c.name as client, due_date, invoice_date, invoice_number, order_number,
            sub_total, tax, grand_total, notes from sys_invoices as a,  revenue_streams as c
            where a.revenue_stream_id = c.id and a.id = '$id'");

            $details = $this->invoiceDetails($id);
            // dd($details);
                //Get items
            $items  = $this->invoiceItems($id);

            $pdf = PDF::loadView('invoice.invoice', compact('details', 'items'));
            //Send an email attachment of the request
            $vendor_email = $request->email;
            $send_email2 = Mail::send('invoice.send_email', compact('details'), function ($message) use ($vendor_email, $pdf) {
            $message->to($vendor_email);
            $message->from('admin@taerp.com', 'TA Procurement');
            $message->subject('Invoice');
            $message->attachData($pdf->output(), 'invoice.pdf', []);
         });
        }

        return redirect()->route('finance.invoice.show', ['id' => $id])->with('success','Invoice sent successfully!');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Get invoice details + items details
        $taxes = Tax::all(); //Get all taxes
        $revenue_streams = RevenueStream::all(); //get revenue streams
        $details = DB::select("select revenue_stream_id, notes, a.id as id, due_date, invoice_date, invoice_number, order_number, grand_total
        from sys_invoices as a where  a.id = '$id'");
        $items = DB::select("select * from sys_items where invoice_id = '$id'");
        return view('invoice.edit', compact('taxes', 'revenue_streams', 'details', 'items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateInvoice(Request $request, $id)
    {
        //Update edited invoice + items in it.
        //Do some validation
        $validator = Validator::make($request->all(), [
            'revenue_stream' => 'required',
            'invoice_date' => 'required|date',
            'due_date' => 'required|date',
            'invoice_number' => 'required',
            'order_number' => 'required'
        ]);

        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        //Do some validation for the items
        if($request->item){
            $input_items = $request->item;
            foreach ($input_items as $key => $item) {
                $price = $item['price'];
                $quantity =  $item['quantity'];
                $tax_id = $item['tax_id'];
                $name = $item['name'];
                if(!preg_match('/^\d*(\.\d{2})?$/', $price)){
                    return back()->withErrors('Price must be in the right format')->withInput();
                }
                if(!is_numeric($quantity)){
                    return back()->withErrors('Quantity must be numeric')->withInput();
                }
                if($tax_id == ""){
                    return back()->withErrors('Tax is required')->withInput();
                }
                if($name == ""){
                    return back()->withErrors('item name is required')->withInput();
                }
            }
        }
        //Update the invoice
        $invoice_date = date('Y-m-d', strtotime($request->invoice_date));
        $due_date = date('Y-m-d', strtotime($request->due_date));
        DB::table('sys_invoices')
            ->where('id', $id)
            ->update([
                'revenue_stream_id' => $request->revenue_stream,
                'invoice_date' => $invoice_date,
                'due_date' => $due_date,
                'invoice_number' => $request->invoice_number,
                'order_number' => $request->order_number,
                'notes' => $request->notes
            ]);
        //Update items for this invoice
        $sub_total = 0;
        $tax_total = 0;
        if($request->item) {
            $input_items = $request->item;
            $invoice_id = $id;
            foreach ($input_items as $key => $item) {

                $price = $item['price'];
                $quantity =  $item['quantity'];
                $tax_id = $item['tax_id'];
                $name = $item['name'];
                $item_sub_total = $quantity * $price;
                if (isset($item['tax_id'])) {
                    $tax = Tax::find($item['tax_id']);
                    $rate = isset($tax->rate) ? $tax->rate : 0;
                    $item_tax_total = (($price * $quantity) / 100) * $rate;
                }
                //Update the items table
                if($item['item_id'] != 0){
                    DB::table('sys_items')
                        ->where('invoice_id', $id)
                        ->where('id', $item['item_id'])
                        ->update([
                            'name' => $name,
                            'price' => $price,
                            'quantity' => $quantity,
                            'total' => $quantity * $price,
                            'tax_id' => $tax_id
                        ]);
                }else{
                    Item::create([
                        'name' => $name,
                        'price' => $price,
                        'quantity' => $quantity,
                        'total' => $quantity * $price,
                        'tax_id' => $tax_id,
                        'invoice_id' => $id
                    ]);
                }

                $sub_total += $item_sub_total;
                $tax_total += $item_tax_total;
                $total = $item_sub_total + $item_tax_total;
            }
        }
        //Update sub-total, tax and grand-total for selected invoice
        DB::table('sys_invoices')->where('id', $id)
            ->update([
                'sub_total' => $sub_total,
                'tax' => $tax_total,
                'grand_total' => $tax_total + $sub_total,
                'balance' => $tax_total + $sub_total,
            ]);
        //Show generated invoice
        return redirect()->route('finance.invoice.show', ['id' => $id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Invoice::find($id)->delete();
        return redirect('finance/invoice');
    }

}
