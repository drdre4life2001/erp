<?php

namespace App\Http\Controllers;

use App\Models\WorkAddress;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WorkAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $method = $request->isMethod('post');
        switch ($method) {
            case true:
                try{
                    $validator = Validator::make($request->all(), [
                        'name' => 'required|max:255',
                        'phone'=>  'required|max:255',
                        'email'=>  'required|max:255',
                        'street'=>  'required'
                    ]);

                    if ($validator->fails()) {
                        return back()->with('error', $validator);
                    }

                    //return "here";

                    WorkAddress::create([
                        'name' => $request->get('name'),
                        'website'=> $request->get('website'),
                        'mobile'=> $request->get('mobile'),
                        'phone'=> $request->get('phone'),
                        'email'=> $request->get('email'),
                        'fax'=> $request->get('fax'),
                        'longitude'=> $request->get('longitude'),
                        'latitutde'=> $request->get('latitude'),
                        'street'=> $request->get('street'),
                        'code'=> $request->get('code'),
                        'created_by'=>Auth::user()->id,
                        'updated_by'=>Auth::user()->id,
                        'company_id' => auth()->user()->company->id,
                        'company_uri' => auth()->user()->company->uri,
                    ]);

                    return back()->with('success', 'WorkAddress Created Successfully!');

                }catch (\Exception $ex){
                    return back()->withErrors($ex->getMessage())->withInput();
                }
                break;
            
            case false:
                $addresses = WorkAddress::where('company_id', auth()->user()->company->id)->where('company_uri', auth()->user()->company->uri)->orderBy('created_at', 'DESC')->get();
                return view('erp.system_settings.company.company_locations')->with(['workAddresses'=>$addresses]);
            default:
                
                break;
        }
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('work_addresses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateWorkAddress(Request $request)
    {
        $method = $request->isMethod('post');
        if ($method) {
            // dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required',
                'street' => 'required',
                'phone' => 'required'
            ]);

            if ($validator->fails()) {
                return back()->with('error', $validator);
            }
            $update_work_address = \DB::table('hr_work_addresses')
                ->where('id', $request->work_address_id)
                ->update(
                    [
                        'name' => $request->get('name'),
                        'website'=> $request->get('website'),
                        'mobile'=> $request->get('mobile'),
                        'phone'=> $request->get('phone'),
                        'email'=> $request->get('email'),
                        'fax'=> $request->get('fax'),
                        'longitude'=> $request->get('longitude'),
                        'latitutde'=> $request->get('latitude'),
                        'street'=> $request->get('street'),
                        'code'=> $request->get('code'),
                        'updated_by'=>Auth::user()->id,
                        'updated_at' => date('Y-m-d h:i:s')
                    ]
                );
            if ($update_work_address) {
                return back()->with('success', 'WorkAddress updated!');
            }else{
                return back()->with('error', 'An error occured while updating!');
            }
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        WorkAddress::find($id)->delete();
        return back()->with('info', 'Work Address trashed!');
    }
}
