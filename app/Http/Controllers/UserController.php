<?php
namespace App\Http\Controllers;
use App\Models\Employee;
use App\Models\Roles;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles=Roles::all();
        $users = DB::SELECT("SELECT u.email as email, u.deleted_at as deleted_at, u.id as id, u.role as role, r.name as role_name FROM sys_user as u, sys_roles as r WHERE r.id = u.role");
        return view('users.index')->with(['users'=>$users, 'roles'=>$roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $users = User::where('company_id', auth()->user()->company->id)->with('roles')->get();
        
        $advance_company_id = auth()->user()->company->id;
        $advance_company_uri = auth()->user()->company->uri;

        $roles = Roles::where([
                        'advance_company_id' => $advance_company_id,
                        'advance_company_uri' => $advance_company_uri
                        ])->get();
        $method = $request->isMethod('post');
        switch ($method) {
            case true:
                    try{
                        $validator = Validator::make($request->all(), [
                            'email' => 'required|email|max:255|unique:sys_user',
                            'user_role' => 'required'
                        ]);

                        if ($validator->fails()) {
                            return redirect()->back()->with('error', $validator);
                        }

                        $confirmation_code  = Str::random('10');
                        $hashedToken = bcrypt($confirmation_code);
                        $time =  Carbon::now();
                        $email = $request->get('email');
                        User::create([
                            'email' => $email,
                            'role' => $request->get('user_role'),
                            'confirmation_code'=>bcrypt($hashedToken),
                            'code_generation_time' => $time,
                            'created_by'=>Auth::user()->id,
                            'updated_by'=>Auth::user()->id,
                            'company_id'=>Auth::user()->company->id,
                            'company_uri'=>Auth::user()->company->uri
                        ]);
                        Mail::send('auth.emails.user_confirmation', ['confirmation_code' => $confirmation_code, 'email'=>$email], function ($message) use ($email){
                            $message->to($email);
                            $message->from('admin@taerp.com', 'Tech-advance Admin');
                            $message->subject('User Account Details');
                        });

                        return back()->with('success', 'Account created successfully');
                    }catch (\Exception $ex){
                        return back()->with('error', $ex->getMessage());
                    }
                break;
            case false:
                    return view('erp.system_settings.user.index')
                    ->with(['users'=>$users,'roles' => $roles]);
                break;
            default:
                # code...
                break;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //create user
        //sends user link to email
        //then user activates account
        try{
            $advance_company_uri = auth()->user()->company->id;
            $advance_company_uri = auth()->user()->company->uri;

            $validator = Validator::make($request->all(), [
                'id_hr_employee' => 'required|max:255|unique:sys_user',
                'email' => 'required|email|max:255|unique:sys_user',
                'user_role' => 'required'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput()->with(['message'=>'error']);
            }

            $employee = Employee::where('id',$request->get('id_hr_employee'))->first();

                if(empty($employee)){
                    return "Employee not found";
                }

            $generatedPassword  = Str::random('10');
            $hashedToken = bcrypt($generatedPassword);
            $time = new Carbon();
            $time = $time->timestamp;

            $email = $request->get('email');
            $la_user = $employee->lastname.' '.$employee->first_name;

            User::create([
                'email' => $request->get('email'),
                'id_hr_employee' => $request->get('id_hr_employee'),
                'role' => $request->get('user_role'),
                'confirmation_code'=>Hash::make($generatedPassword),
                'code_generation_time' => $time,
                'created_by'=>Auth::user()->id,
                'updated_by'=>Auth::user()->id,
                'company_id' => $advance_company_id,
                'company_uri' => $advance_company_uri
            ]);

            Mail::send('auth.emails.user_confirmation', ['confirmation_code' => $generatedPassword, 'email'=>$email], function ($message) use ($email,$la_user){
                $message->to($email, $la_user);
                $message->from('admin@taerp.com', 'Admin');
                $message->subject('Employee Account Details');
            });

            return back()->withErrors('Account created successfully, please check the email provided for account activation!');
        }catch (\Exception $ex){
            return back()->withErrors($ex->getMessage())->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if($request->role == "" || is_null($request->role)){
            return back()->with('error', 'An error occured, please select a valid role');
        }

        if($user->email == $request->user_email){
            $validator = Validator::make([
                'role' => $request->role], [
                'role' => 'required'
            ]);

            if($validator->fails()){
                return back()->with('success', 'Please select user role.');
            }

            $user->update([
                'role' => $request->get('role')
            ]);
        }else{
            $validator = Validator::make([
                'email' => $request->user_email, 'role' => $request->role], [
                'email' => 'required|unique:sys_user|max:255',
                'role' => 'required'
            ]);

            if($validator->fails()){
                return back()->with('success', 'All fields are required.');
            }
            $user->update([
                'email' => $request->get('user_email'),
                'role' => $request->get('role')
            ]);
        }
        return back()->with('success', 'User was updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return back()->with('success', 'User was deactivated successfully');
    }
    public function activate($advance_company_id, $advance_company_uri, $user_id)
    {
        DB::SELECT("UPDATE sys_user SET status = 'active' WHERE company_id = '$advance_company_id' AND company_uri = '$advance_company_uri' AND id = '$user_id'");
        return back()->with('success', 'User activated successfully');
    }

    public function deactivate($advance_company_id, $advance_company_uri, $user_id)
    {
        DB::SELECT("UPDATE sys_user SET status = 'inactive' WHERE company_id = '$advance_company_id' AND company_uri = '$advance_company_uri' AND id = '$user_id'");
        return back()->with('success', 'User successfully deactivated');
    }
    public function createUserAccountForEmployee(Request $request, $employeeId){

        try{
            $employee = Employee::select('id','first_name', 'lastname', 'other_name', 'identification_number')
                ->where('id',$employeeId)->get();

            if(empty($employee)){
                return back()->withErrors('Employee resource not found');
            }

            //get user roles
            $roles = Roles::select('id', 'name')->get();
            return view('users.create')->with(['employees'=>$employee, 'roles'=> $roles]);
        }catch (\Exception $ex){
            return back()->withErrors($ex->getMessage())->withInput();
        }
    }

    public function verify(Request $request, $email, $token){
        try{

            $validator = Validator::make($request->all(), [
                'password' => 'required|min:6|confirmed',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput()->with(['message'=>'error']);
            }

            $user = User::where('email',$email)->first();

            if(empty($user)){
                return redirect()->back()->withErrors( "User not found");
            }

            if($user->confirmed == 1){
                //implies user is already confirmed
                return redirect()->back()->withErrors( "User already confirmed");
            }

            //change password, //set to confirmed
            $user->password = bcrypt($request->get('password'));
            $user->confirmed = 1;
            $user->save();//

            //redirect to login
            return redirect()->to('/');

        }catch (\Exception $ex){
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    public function changePassword(Request $request, $email, $token){
        return view('auth.change_password');
    }

}
