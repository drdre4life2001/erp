<?php

namespace App\Http\Middleware;
use App\Libraries\Utilities;
use Illuminate\Support\Facades\Auth;
use Closure;
use Illuminate\Support\Facades\DB;
class checkPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $resource)
    {
        $id_hr_employee= Auth::user()->id_hr_employee;
        $role_id = Utilities::getRoleId();
        //applications system roles and permission
            $sql = DB::select("select * from user_permissions where resource_id = '$resource'
            and role_id = $role_id
            ");
        if(count($sql) >= 1){
            return $next($request);
        }
    //return 'Not allowed';
        return response()->view('errors.403');
    }
}
