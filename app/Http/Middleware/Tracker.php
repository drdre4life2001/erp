<?php

namespace App\Http\Middleware;

use App\Models\Activity;
use Closure;
use Illuminate\Support\Facades\Auth;

class Tracker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $activity = new Activity();
        $activity->username = isset(Auth::user()->email)? Auth::user()->email : '';
        $activity->ip_address = ip2long($request->ip());
        $activity->user_agent = $request->header('User-Agent');
        $activity->method = $request->method();
        $activity->url = $request->url();
        $activity->data = json_encode($request->all());
        $activity->remarks = "";
        $activity->save();
        return $next($request);
    }
}
