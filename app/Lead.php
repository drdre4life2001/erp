<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    //
    protected $fillable = ['status', 'person_name', 'gender', 'organiztion_name', 'source', 'email_address',
                            'lead_owner', 'next_contact_date', 'next_contact_by', 'phone', 'salutation', 'mobile_no',
                            'fax', 'website', 'territory', 'lead_type', 'market_segment', 'industry', 'request_type'];
    public $timestamps = false;

}