<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrRequest extends Model
{
    //
    protected $table = 'pr_request';

    public $fillable = [
    'title',
    'category',
    'user_id',
    'line_manager',
    'line_manager_remark',
    'c_level_remark',
    'created_by',
    'updated_by',
        'company_id',
        'sub_category_id'
    ];
}
