<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoanRepayment extends Model
{
    //
    public $table = 'loan_repayments';

     protected $dates = ['deleted_at'];

}
