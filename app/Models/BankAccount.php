<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccount extends Model
{
    //
    use SoftDeletes;

    public $table = 'sys_bank_accounts';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'bank_id',
        'account_number',
        'account_name',
        'prevBalance',
        'currentBalance',
        'book_id',
        'subsidiary_id',
        'name_relationship_manager',
        'phone_relationship_manager',
        'email_relationship_manager',
        'advance_company_id',
        'advance_company_uri'
    ];

    public function bank(){
        return $this->belongsTo('App\Models\Bank');
    }
}
