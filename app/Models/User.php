<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @SWG\Definition(
 *      definition="User",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_employee",
 *          description="id_hr_employee",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="password",
 *          description="password",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="remember_token",
 *          description="remember_token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class User extends Authenticatable
{
    use SoftDeletes;

    public $table = 'sys_user';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_hr_employee',
        'email',
        'password',
        'confirmed',
        'confirmation_code',
        'code_generation_time',
        'remember_token',
        'created_by',
        'updated_by',
        'role',
        'company_id',
        'company_uri'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_hr_employee' => 'integer',
        'email' => 'string',
        'password' => 'string',
        'confirmed' => 'boolean',
        'confirmation_code' => 'string',
        'remember_token' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'role' => 'integer',
        'company_id',
        'company_uri'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function employee()
    {
        return $this->belongsTo(\App\Models\Employee::class);
    }

    public function emp()
    {
        return $this->hasOne('\App\Models\Employee', 'id_hr_employee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function assignedRole()
    {
        return $this->belongsTo(\App\Models\Roles::class, 'role');
    }

    public function abilities()
    {
        return $this->hasManyThrough( \App\Models\RolePermissions::class, \App\Models\Roles::class, 'id', 'id_sys_roles', 'role' );
        //return $this->hasMany(\App\Models\RolePermissions::class, 'id_sys_roles');
    }

    public function roles()
    {
        return $this->belongsTo('\App\Models\Roles', 'role');
    }

    public function company(){
        return $this->belongsTo(\App\Models\Company::class);
    }
}
