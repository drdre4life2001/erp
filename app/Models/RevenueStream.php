<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RevenueStream extends Model
{
    //
    use SoftDeletes;

    public $table = 'revenue_streams';

    protected $fillable = [
        'name',
        'client_id',
        'company_id',
        'advance_company_id',
        'advance_company_uri'

    ];

    protected $dates = ['deleted_at'];
    
    
}
