<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyBank extends Model
{
    //
    public $table = 'company_banks';

    protected $fillable = ['account_name', 'advance_company_id', 'advance_company_uri', 'created_at', 'user_id', 'updated_at', 'account_number'];
}
