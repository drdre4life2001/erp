<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyLoan extends Model
{
    //
    public $table = 'company_loans';

     protected $dates = ['deleted_at'];

}
