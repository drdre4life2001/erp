<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ledger extends Model
{
    //
    public $table = 'ledgers';

    protected $fillable = [
       'bank_id',
       'amount',
       'prevBalance',
       'currentBalance',
       'header_id',
       'sub_header_id',
       'book_number',
       'trans_id',
        'user_id',
       'type'
    ];
}
