<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sysrfq extends Model
{
    //
    public $table = 'sys_rfq';
   
    public $fillable = [
    'item_id',
    'procurement_officer_id',
    'request_date',
    'response_date',
    'vendor_id',
    'rfq_status'
    ];

  
}
