<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['name', 'registration_number', 'advance_username', 'user_id', 'uri'];

    public function user(){
        return $this->hasOne(\App\Models\User::class);
    }
}
