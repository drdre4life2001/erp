<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OperatingExpense extends Model
{
    //
    use SoftDeletes;

    public $table = 'operating_expenses';

    protected $fillable = [
        'name', 'category_id', 'advance_company_id', 'advance_company_uri'];

    protected $dates = ['deleted_at'];
}
