<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceLog extends Model
{
    //
    public $table = "sys_invoices_log";


    protected $fillable = [
        'user_id',
        'invoice_id',
        'amount',
        'prevBalance',
        'currentBalance',
        'description'
    ];
}
