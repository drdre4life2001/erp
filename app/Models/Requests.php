<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Requests",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_fin_request_item",
 *          description="id_fin_request_item",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="qty",
 *          description="qty",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="unit_price",
 *          description="unit_price",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="total_price",
 *          description="total_price",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="procurement_unit_price",
 *          description="procurement_unit_price",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="procurement_total_price",
 *          description="procurement_total_price",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="info",
 *          description="info",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Requests extends Model
{
    use SoftDeletes;

    public $table = 'fin_requests';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_fin_request_item',
        'id_fin_request_group',
        'qty',
        'unit_price',
        'total_price',
        'procurement_unit_price',
        'procurement_total_price',
        'info',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_fin_request_item' => 'integer',
        'id_fin_request_group'  => 'integer',
        'qty' => 'float',
        'unit_price' => 'float',
        'total_price' => 'float',
        'procurement_unit_price' => 'float',
        'procurement_total_price' => 'float',
        'info' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function item(){
        return $this->belongsTo(\App\Models\RequestItem::class, 'id_fin_request_item');
    }

    public function requester(){
        return $this->belongsTo(\App\Models\User::class, 'created_by');
    }
}
