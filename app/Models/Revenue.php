<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Revenue extends Model
{
    //
    use SoftDeletes;
    public $table = 'revenues';

    protected $fillable = [
        'client_id',
        'company_id',
        'revenue_stream_id',
        'total_collection',
        'total_commission',
        'date',
        'advance_company_id',
        'advance_company_uri'
    ];

    protected $dates =['deleted_at'];
}
