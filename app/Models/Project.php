<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Employee",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="identification_number",
 *          description="identification_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="account_no",
 *          description="account_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="date_of_birth",
 *          description="date_of_birth",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="home_address",
 *          description="home_address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="first_name",
 *          description="first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="lastname",
 *          description="lastname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="other_name",
 *          description="other_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="passport_no",
 *          description="passport_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="picture",
 *          description="picture",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="city_of_birth",
 *          description="city_of_birth",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="disability",
 *          description="disability",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="permanent_address",
 *          description="permanent_address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_of_children",
 *          description="no_of_children",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="date_first_hired",
 *          description="date_first_hired",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_work_addresses",
 *          description="id_hr_work_addresses",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_sys_banks",
 *          description="id_sys_banks",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_grade",
 *          description="id_hr_grade",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code_sys_country",
 *          description="code_sys_country",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Project extends Model
{
    use SoftDeletes;

    public $table = 'sys_projects';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

/*    public $fillable = [
        'identification_number',
        'account_no',
        'date_of_birth',
        'home_address',
        'first_name',
        'lastname',
        'other_name',
        'passport_no',
        'picture',
        'city_of_birth',
        'disability',
        'permanent_address',
        'no_of_children',
        'date_first_hired',
        'id_hr_work_addresses',
        'id_sys_banks',
        'code',
        'id_hr_grade',
        'code_sys_country',
        'created_by',
        'updated_by'
    ];*/

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'employee_id' => 'integer',
        'bonus_type' => 'string',
        'month' => 'string',
        'year' => 'string',
        'amount' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function created_by()
    {
        return $this->belongsTo(\App\Models\User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function updated_by()
    {
        return $this->belongsTo(\App\Models\User::class, 'updated_by');
    }
}
