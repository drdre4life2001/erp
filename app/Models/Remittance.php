<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Remittance extends Model
{
    //
    public $table = 'remittances';

    protected $fillable = [
        'remittance_date',
        'amount'
    ];
}
