<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Employee",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="identification_number",
 *          description="identification_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="account_no",
 *          description="account_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="date_of_birth",
 *          description="date_of_birth",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="home_address",
 *          description="home_address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="first_name",
 *          description="first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="lastname",
 *          description="lastname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="other_name",
 *          description="other_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="passport_no",
 *          description="passport_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="picture",
 *          description="picture",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="city_of_birth",
 *          description="city_of_birth",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="disability",
 *          description="disability",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="permanent_address",
 *          description="permanent_address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_of_children",
 *          description="no_of_children",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="date_first_hired",
 *          description="date_first_hired",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_work_addresses",
 *          description="id_hr_work_addresses",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_sys_banks",
 *          description="id_sys_banks",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_grade",
 *          description="id_hr_grade",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code_sys_country",
 *          description="code_sys_country",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class EmployeeRenumeration extends Model
{
    use SoftDeletes;

    public $table = 'hr_employee_renumerations';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

/*    public $fillable = [
        'identification_number',
        'account_no',
        'date_of_birth',
        'home_address',
        'first_name',
        'lastname',
        'other_name',
        'passport_no',
        'picture',
        'city_of_birth',
        'disability',
        'permanent_address',
        'no_of_children',
        'date_first_hired',
        'id_hr_work_addresses',
        'id_sys_banks',
        'code',
        'id_hr_grade',
        'code_sys_country',
        'created_by',
        'updated_by'
    ];*/

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     * `basic_pay` double NOT NULL,
    `transport` double NOT NULL,
    `leave` double NOT NULL,
    `housing` double NOT NULL,
    `others` double NOT NULL,
    `meal` double NOT NULL,
    `entertainment` double NOT NULL,
    `utility` double NOT NULL,
    `nhf` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `life_assurance` double NOT NULL,
    `cra` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `fixed_cra` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `pension` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `dependable_relative` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `children` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `disability` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
     */
    protected $casts = [
        'id' => 'integer',
        'employee_id' => 'integer',
        'basic_pay' => 'string',
        'transport' => 'string',
        'leave' => 'string',
        'housing' => 'string',
        'others' => 'string',
        'meal' => 'string',
        'entertainment' => 'string',
        'utility' => 'string',
        'nhf' => 'string',
        'life_assurance' => 'string',
        'cra' => 'string',
        'fixed_cra' => 'string',
        'pension' => 'string',
        'dependable_relative' => 'string',
        'children' => 'string',
        'disability' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'company_id' => 'integer',
        'company_uri' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function employee()
    {
        return $this->belongsTo(\App\Models\Employee::class, 'employee_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function created_by()
    {
        return $this->belongsTo(\App\Models\User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function updated_by()
    {
        return $this->belongsTo(\App\Models\User::class, 'updated_by');
    }
}
