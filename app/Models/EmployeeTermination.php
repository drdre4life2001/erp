<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="EmployeeTermination",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="new_employer",
 *          description="new_employer",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="reason",
 *          description="reason",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="date_of_notification",
 *          description="date_of_notification",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="projected_last_date",
 *          description="projected_last_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="actual_last_date",
 *          description="actual_last_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="accepted_date",
 *          description="accepted_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="accepted_by_employee",
 *          description="accepted_by_employee",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_employee_education",
 *          description="id_hr_employee_education",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class EmployeeTermination extends Model
{
    use SoftDeletes;

    public $table = 'hr_employee_termination';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'new_employer',
        'reason',
        'status',
        'date_of_notification',
        'projected_last_date',
        'actual_last_date',
        'accepted_date',
        'accepted_by_employee',
        'created_by',
        'updated_by',
        'id_hr_employee_education'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'new_employer' => 'string',
        'reason' => 'string',
        'status' => 'integer',
        'date_of_notification' => 'date',
        'projected_last_date' => 'date',
        'actual_last_date' => 'date',
        'accepted_date' => 'date',
        'accepted_by_employee' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'id_hr_employee_education' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function employeeEducation()
    {
        return $this->belongsTo(\App\Models\EmployeeEducation::class);
    }
    
}
