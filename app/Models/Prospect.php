<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prospect extends Model
{
    //
    public $table = 'prospects';

    protected $fillable = [
       'name',
       'address',
       'email',
       'phone',
       'code',
       'subsidiary_id',
        'created_by',
        'status',
        'contact_person',
        'contact_position',
        'revenue'
    ];
}
