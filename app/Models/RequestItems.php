<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestItems extends Model
{
    //
    //
    protected $table = 'request_items';

    public $fillable = [
    'request_id',
    'title',
    'description',
    'price',
    'quantity',
    'total',
        'salvage_id',
        'salvage_value',
    'line manager remark',
    'c level remark'
    ];
}
