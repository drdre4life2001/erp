<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    //
    use SoftDeletes;

    public $table = 'sys_invoices';

    protected $fillable = [
        'revenue_stream_id',
        'status',
        'invoice_date',
        'due_date',
        'invoice_number',
        'order_number',
        'sub_total',
        'tax',
        'grand_total',
        'notes',
        'balance',
        'paid',
        'advance_company_id',
        'advance_company_uri',
    ];
}
