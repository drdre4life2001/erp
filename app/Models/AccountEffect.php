<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountEffect extends Model
{
    //
    public $table = 'account_effects';

    protected $fillable = ['name', 'advance_company_id', 'advance_company_uri', 'created_at', 'updated_at'];
}
