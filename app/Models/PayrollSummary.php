<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PayrollSummary extends Model
{
	use SoftDeletes;

    public $table = 'payroll_summaries';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'parent_id',
        'child_id',
        'length',
        'created_by',
        'updated_by',
        'advance_company_id',
        'advance_company_uri',
    ];

    public function employees(){
        return $this->belongsTo('App\Models\Employee');
    }
}
