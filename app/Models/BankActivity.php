<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankActivity extends Model
{
    //
    public $table = 'bank_activities';

    protected $fillable = [
        'bank_id',
        'amount',
        'type',
        'prevBalance',
        'currentBalance',
        'user_id'
    ];
}
