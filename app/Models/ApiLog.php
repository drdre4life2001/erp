<?php

namespace App\Models;

use Eloquent as Model;

class ApiLog extends Model
{
    protected $fillable = ['id', 'request', 'response', 'status_code', 'message', 'api_ref', 'method', 'created_at', 'updated_at', 'end_point'];
}
