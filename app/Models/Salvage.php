<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Salvage extends Model
{
    //
    use SoftDeletes;
    public $table = 'salvages';

    protected $fillable = [
        'name',
        'period',
        'value',
        'advance_company_id',
        'advance_company_uri'
    ];

    protected $dates = ['deleted_at'];
}
