<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JiraCredential extends Model
{
    //
    public $table = 'jira_credentials';

    protected $fillable = [
        'user_id',
        'username',
        'password'
    ];
}
