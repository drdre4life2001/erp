<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="EmployeeEducation",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="specialization",
 *          description="specialization",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="notes",
 *          description="notes",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="startDate",
 *          description="startDate",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="endDate",
 *          description="endDate",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="mode_of_education",
 *          description="mode_of_education",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="grade",
 *          description="grade",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="comments",
 *          description="comments",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_institutions",
 *          description="id_hr_institutions",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_institution_departments",
 *          description="id_hr_institution_departments",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_employee",
 *          description="id_hr_employee",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class EmployeeEducation extends Model
{
    use SoftDeletes;

    public $table = 'hr_employee_education';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'specialization',
        'notes',
        'startDate',
        'endDate',
        'mode_of_education',
        'grade',
        'comments',
        'created_by',
        'updated_by',
        'id_hr_institutions',
        'id_hr_institution_departments',
        'id_hr_employee'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'specialization' => 'string',
        'notes' => 'string',
        'startDate' => 'date',
        'endDate' => 'date',
        'mode_of_education' => 'string',
        'grade' => 'string',
        'comments' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'id_hr_institutions' => 'integer',
        'id_hr_institution_departments' => 'integer',
        'id_hr_employee' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function employee()
    {
        return $this->belongsTo(\App\Models\Employee::class, 'id_hr_employee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function institutionDepartment()
    {
        return $this->belongsTo(\App\Models\InstitutionDepartment::class, 'id_hr_institution_departments');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function institution()
    {
        return $this->belongsTo(\App\Models\Institution::class, 'id_hr_institutions');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function employeeTerminations()
    {
        return $this->hasMany(\App\Models\EmployeeTermination::class);
    }
}
