<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model
{
    //
    public $table = "user_permissions";

    public $fillable = [
    	'role_id',
    	'resource_id',
        'permissions',
        'advance_company_id',
        'advance_company_uri'
    ];
}
