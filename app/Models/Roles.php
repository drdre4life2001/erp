<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Roles",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="slug",
 *          description="slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Roles extends Model
{
    use SoftDeletes;

    public $table = 'sys_roles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'slug',
        'description',
        'created_by',
        'updated_by',
        'advance_company_id',
        'advance_company_uri'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'advance_company_id' => 'integer',
        'advance_company_uri' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function rolePermissions()
    {
        //return $this->hasManyThrough(\App\Models\Permissions::class, \App\Models\RolePermissions::class, \App\Models\RolePermissions::class, 'id_sys_roles', 'id_sys_permissions' , 'id');
         return $this->hasMany(\App\Models\RolePermissions::class, 'id_sys_roles');
    }

    public function permissions(){
        return $this->hasMany(\App\Models\UserPermission::class, 'role_id');
    }
    
    // public function user()
    // {
    //     return $this->belongsTo('App\User');
    // }

    public function users()
    {
        return $this->hasOne('\App\Models\User'); //a role have atleast one user
    }
}
