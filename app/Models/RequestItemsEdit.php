<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestItemsEdit extends Model
{
    //
    public $table = 'request_items_edits';

    public $fillable = [
    	'user_id',
    	'item_id'
    ];
}
