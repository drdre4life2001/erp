<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class OtherExpense extends Model
{
    use SoftDeletes;

    public $table = 'other_expenses';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'content',
        'status',
        'description',
        'updated_by',
        'created_at',
        'updated_at',
        'description',
        'approoved_by',
        'account_header_id',
        'amount'
    ];

//    public function account_header(){
//        return $this->belongsTo(\App\Models\AccountHeader::class, 'created_by');
//    }
}
