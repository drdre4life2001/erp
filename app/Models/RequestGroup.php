<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="RequestGroup",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="start_date",
 *          description="start_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="end_date",
 *          description="end_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_employee",
 *          description="id_hr_employee",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="expected_approval_user",
 *          description="expected_approval_user",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="remarks",
 *          description="remarks",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="validation_date",
 *          description="validation_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="approving_date",
 *          description="approving_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="total_amount_requesting",
 *          description="total_amount_requesting",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="total_amount_paid",
 *          description="total_amount_paid",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="approving_user",
 *          description="approving_user",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class RequestGroup extends Model
{
    use SoftDeletes;

    public $table = 'fin_request_group';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'start_date',
        'end_date',
        'id_hr_employee',
        'expected_approval_user',
        'remarks',
        'created_by',
        'updated_by',
        'validation_date',
        'approving_date',
        'total_amount_requesting',
        'status',
        'total_amount_paid',
        'approving_user'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'start_date' => 'date',
        'end_date' => 'date',
        'id_hr_employee' => 'integer',
        'expected_approval_user' => 'integer',
        'remarks' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'validation_date' => 'date',
        'approving_date' => 'date',
        'total_amount_requesting' => 'float',
        'status' => 'integer',
        'total_amount_paid' => 'float',
        'approving_user' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function employee()
    {
        return $this->belongsTo(\App\Models\Employee::class, 'id_hr_employee');
    }

    public function expectedApprovalUser()
    {
        return $this->belongsTo(\App\Models\Employee::class, 'expected_approval_user');
    }

    public function requests()
    {
        return $this->hasMany(\App\Models\Requests::class, 'id_fin_request_group');
    }
    
}
