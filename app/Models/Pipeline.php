<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pipeline extends Model
{
    //
    public $table = 'pipelines';

    protected $fillable = [
        'prospect_id',
        'title',
        'description',
        'activity_date',
        'user_id',
        'status'
    ];
}
