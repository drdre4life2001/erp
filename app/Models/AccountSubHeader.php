<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountSubHeader extends Model
{
    //
    use SoftDeletes;

    public $table = 'account_sub_headers';

    protected $fillable = [
        'header_id',
        'parent_id',
        'bank_account_id',
        'name',
        'book_number'
    ];
}
