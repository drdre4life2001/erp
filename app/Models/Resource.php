<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    //
    public $table = 'sys_resources';

    public $fillable = [
    	'name'
    ];
}
