<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Organization",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tin",
 *          description="tin",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_work_addresses",
 *          description="id_hr_work_addresses",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */


class Organization extends Model
{
    use SoftDeletes;

    public $table = 'sys_organizations';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'code',
        'description',
        'tin',
        'created_by',
        'updated_by',
        'id_hr_work_addresses',
        'company_id',
        'company_uri',
        'registration_number',
        'business_sector',
        'business_type',
        'certificate_image',
        'date_incorporated',
        'email',
        'phone',
        'postal_address',
        'tax_identification_number',
        'uid',
        'website',
        'address',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'description' => 'string',
        'tin' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'id_hr_work_addresses' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function workAddress()
    {
        return $this->belongsTo(\App\Models\WorkAddress::class, 'id_hr_work_addresses');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function Departments()
    {
        return $this->hasMany(\App\Models\Department::class);
    }

    public function employees()
    {
        return $this->hasMany(\App\Models\Employee::class, 'id_hr_company');
    }
    
}
