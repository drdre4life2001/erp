<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
    public $table = "sys_items";

    protected $fillable = [
        'name',
        'price',
        'quantity',
        'total',
        'tax_id',
        'invoice_id',
        'advance_company_id',
        'advance_company_uri'
    ];
}
