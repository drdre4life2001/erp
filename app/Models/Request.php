<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    //
    protected $table = 'pr_request';

    public $fillable = [
    'title',
    'category',
    'user_id',
    'state',
    'created_by',
    'updated_by'
    ];
}
