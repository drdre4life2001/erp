<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CapitalExpense extends Model
{
    //
    use SoftDeletes;

    public $table = 'capital_expenses';

    protected $fillable = ['category_id', 'name', 'advance_company_id', 'advance_company_uri'];

    protected $dates = ['deleted_at'];
}
