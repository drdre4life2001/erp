<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AccountHeader extends Model
{
    //
    use SoftDeletes;

    public $table = 'account_headers';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'effect',
        'book_number',
        'advance_company_id',
        'advance_company_uri'

    ];

}
