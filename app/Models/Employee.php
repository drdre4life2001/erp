<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Employee",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="identification_number",
 *          description="identification_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="account_no",
 *          description="account_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="date_of_birth",
 *          description="date_of_birth",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="home_address",
 *          description="home_address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="first_name",
 *          description="first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="lastname",
 *          description="lastname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="other_name",
 *          description="other_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="passport_no",
 *          description="passport_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="picture",
 *          description="picture",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="city_of_birth",
 *          description="city_of_birth",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="disability",
 *          description="disability",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="permanent_address",
 *          description="permanent_address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_of_children",
 *          description="no_of_children",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="date_first_hired",
 *          description="date_first_hired",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_work_addresses",
 *          description="id_hr_work_addresses",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_sys_banks",
 *          description="id_sys_banks",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_grade",
 *          description="id_hr_grade",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code_sys_country",
 *          description="code_sys_country",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Employee extends Model
{
    use SoftDeletes;

    public $table = 'hr_employee';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'identification_number',
        'account_no',
        'date_of_birth',
        'home_address',
        'first_name',
        'lastname',
        'other_name',
        'email',
        'phone',
        'passport_no',
        'picture',
        'city_of_birth',
        'disability',
        'permanent_address',
        'no_of_children',
        'date_first_hired',
        'id_hr_work_addresses',
        'id_sys_banks',
        'code',
        'id_hr_grade',
        'code_sys_country',
        'created_by',
        'updated_by',
        'id_hr_company',
        'company_uri',
        'company_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'identification_number' => 'string',
        'account_no' => 'string',
        'date_of_birth' => 'date',
        'home_address' => 'string',
        'first_name' => 'string',
        'lastname' => 'string',
        'other_name' => 'string',
        'passport_no' => 'string',
        'picture' => 'string',
        'city_of_birth' => 'string',
        'disability' => 'string',
        'permanent_address' => 'string',
        'no_of_children' => 'integer',
        'date_first_hired' => 'date',
        'id_hr_work_addresses' => 'integer',
        'id_sys_banks' => 'integer',
        'code' => 'string',
        'id_hr_grade' => 'integer',
        'code_sys_country' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'company_id' => 'integer',
        'company_uri' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function country()
    {
        return $this->belongsTo(\App\Models\Country::class, 'code_sys_country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function gender()
    {
        return $this->belongsTo(\App\Models\Gender::class, 'code');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function grade()
    {
        return $this->belongsTo(\App\Models\Grade::class, 'id_hr_grade');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function workAddress()
    {
        return $this->belongsTo(\App\Models\WorkAddress::class, 'id_hr_work_addresses');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function bank()
    {
        return $this->belongsTo(\App\Models\Bank::class, 'id_sys_banks');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function employeeAttachments()
    {
        return $this->hasMany(\App\Models\EmployeeAttachment::class, 'id_hr_employee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function employeeDepartments()
    {
        return $this->hasMany(\App\Models\EmployeeDepartment::class, 'id_hr_employee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function employeeEducation()
    {
        return $this->hasMany(\App\Models\EmployeeEducation::class, 'id_hr_employee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function employeeBonus()
    {
        return $this->hasMany(\App\Models\EmployeeBonus::class, 'employee_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function employeeExperiences()
    {
        return $this->hasMany(\App\Models\EmployeeExperience::class, 'id_hr_employee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function jobTitles()
    {
        //return $this->hasManyThrough(\App\Models\Job::class, \App\Models\EmployeeJob::class, 'id_hr_employee', 'id', 'id' );
        return $this->hasMany(\App\Models\EmployeeJob::class, 'id_hr_employee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function employeeReferees()
    {
        return $this->hasMany(\App\Models\EmployeeReferee::class, 'id_hr_employee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function departments()
    {
        return $this->hasMany(\App\Models\Department::class, 'id_hr_employee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function users()
    {
        return $this->hasMany(\App\Models\User::class);
    }

    public function user()
    {
        return $this->belongsTo('\App\Models\User', 'id_hr_employee');
    }

    public function managers()
    {
        return $this->hasMany(\App\Models\LineManager::class, 'child_employee_id');
    }

    public function payrollSummaries(){
        return $this->hasMany('App\Models\PayrollSummary');
    }

    public function organization()
    {
        return $this->belongsTo(\App\Models\Organization::class);
    }
}
