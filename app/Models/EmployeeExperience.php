<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="EmployeeExperience",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="organization_name",
 *          description="organization_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="designation",
 *          description="designation",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="employee_number",
 *          description="employee_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="department",
 *          description="department",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="work_start_date",
 *          description="work_start_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="work_end_date",
 *          description="work_end_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="last_manager",
 *          description="last_manager",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="last_drawn_salary",
 *          description="last_drawn_salary",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="communication_details",
 *          description="communication_details",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="project_details",
 *          description="project_details",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="remarks",
 *          description="remarks",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_hr_employee",
 *          description="id_hr_employee",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class EmployeeExperience extends Model
{
    use SoftDeletes;

    public $table = 'hr_employee_experience';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'organization_name',
        'designation',
        'employee_number',
        'department',
        'work_start_date',
        'work_end_date',
        'last_manager',
        'last_drawn_salary',
        'communication_details',
        'project_details',
        'remarks',
        'created_by',
        'updated_by',
        'id_hr_employee'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'organization_name' => 'string',
        'designation' => 'string',
        'employee_number' => 'string',
        'department' => 'string',
        'work_start_date' => 'date',
        'work_end_date' => 'date',
        'last_manager' => 'string',
        'last_drawn_salary' => 'float',
        'communication_details' => 'string',
        'project_details' => 'string',
        'remarks' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'id_hr_employee' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function employee()
    {
        return $this->belongsTo(\App\Models\Employee::class);
    }
}
