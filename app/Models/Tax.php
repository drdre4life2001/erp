<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tax extends Model
{
    //
    use SoftDeletes;

    public $table = "sys_tax";

    protected $fillable = [
        'name',
        'rate',
        'advance_company_id',
        'advance_company_uri'
    ];
}
