<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Activity",
 *      required={""},
 *      @SWG\Property(
 *          property="username",
 *          description="username",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ip_address",
 *          description="ip_address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="user_agent",
 *          description="user_agent",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="method",
 *          description="method",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="data",
 *          description="data",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="remarks",
 *          description="remarks",
 *          type="string"
 *      )
 * )
 */
class Activity extends Model
{
    use SoftDeletes;

    public $table = 'sys_activities';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'username',
        'ip_address',
        'user_agent',
        'method',
        'url',
        'data',
        'remarks'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'username' => 'string',
        'ip_address' => 'string',
        'user_agent' => 'string',
        'method' => 'string',
        'url' => 'string',
        'data' => 'string',
        'remarks' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
